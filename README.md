### Biggest changes

`Http` changed to the new `HttpClient`. Change all `imports` and declarations. Main changes done in `common-http.module.ts`. Use as reference.

Also see Notes further down for changes from RxJS 5 to 6.

### Upgrade form Angular 5 to 6
Make sure global `@angular/cli` is version 6.2.9 (probably not needed, but makes it more safe to upgrade).

- Delete `node-modules` folder
- Delete `package-lock.json` file

In the root folder of project, run:
~~~~bash
npm install @angular/cli@^6.0.0
ng update @angular/cli
~~~~
This should convert the `.angular-cli.json` file to a new `angular.json` file.

Update these verison numbers in `package.json` file to fix dependencies (remove `"@angular/http": "^5.2.10"` as its `@deprecated`):

~~~~json
{
    "dependencies": {
        "@angular/animations": "^6.1.10",
        "@angular/cli": "^6.2.9",
        "@angular/common": "^6.1.10",
        "@angular/compiler": "^6.1.10",
        "@angular/core": "^6.1.10",
        "@angular/forms": "^6.1.10",
        "@angular/platform-browser": "^6.1.10",
        "@angular/platform-browser-dynamic": "^6.1.10",
        "@angular/platform-server": "^6.1.10",
        "@angular/router": "^6.1.10",
        "@ng-bootstrap/ng-bootstrap": "^2.2.2",
        "@ngx-config/core": "^6.0.0",
        "@ngx-config/http-loader": "^6.0.0",
        "@ngx-translate/core": "^10.0.0",
        "@ngx-translate/http-loader": "^2.0.0",
        "@ngx-translate/i18n-polyfill": "^1.0.0",
        "rxjs": "^6.5.2"
    },
    "devDependencies": {
        "@angular/compiler-cli": "^6.1.10",
        "codelyzer": "^4.5.0",
        "typescript": "^2.9.2"
    }
}
~~~~
Then run
~~~~bash
npm install
ng serve
~~~~
You should now get loads of errors, but the errors are all about import failures and basic syntax error in code. This is when you know you've succeeded.

#### More info

- https://stackoverflow.com/questions/50232874/angular-6-migration-angular-cli-json-to-angular-json#52969831
- https://update.angular.io/#5.2:6.0

---

##### Compile/serve errors
**Error**

`Local workspace file ('angular.json') could not be found.`

**Fix**

Check if you have an `angular.json` file in the root folder. If you do, use Google as I haven't experienced this. If you do not, then `ng update` did not do its job. Create a new `angular.json` file and paste this code in
~~~~json
{
  "$schema": "./node_modules/@angular/cli/lib/config/schema.json",
  "version": 1,
  "newProjectRoot": "projects",
  "projects": {
    "us-exceline-html": {
      "root": "",
      "sourceRoot": "src",
      "projectType": "application",
      "architect": {
        "build": {
          "builder": "@angular-devkit/build-angular:browser",
          "options": {
            "outputPath": "dist",
            "index": "src/index.html",
            "main": "src/main.ts",
            "tsConfig": "src/tsconfig.app.json",
            "polyfills": "src/polyfills.ts",
            "assets": [
              "src/assets",
              "src/favicon.ico"
            ],
            "styles": [
              "node_modules/intl-tel-input/build/css/intlTelInput.css",
              "src/styles.scss"
            ],
            "scripts": [
              "node_modules/core-js/client/shim.min.js",
              "node_modules/zone.js/dist/zone.min.js",
              "node_modules/jquery/dist/jquery.min.js",
              "node_modules/tether/dist/js/tether.js",
              "node_modules/popper.js/dist/umd/popper.min.js",
              "node_modules/bootstrap/dist/js/bootstrap.min.js",
              "node_modules/moment/min/moment.min.js",
              "src/assets/js/ckeditor.js",
              "src/assets/js/utils.js",
              "src/assets/js/fullcalendar-old.js",
              "src/assets/js/scheduler-old.js",
              "src/assets/js/fullcalendar-rightclick.js"
            ]
          },
          "configurations": {
            "production": {
              "optimization": true,
              "outputHashing": "all",
              "sourceMap": false,
              "extractCss": true,
              "namedChunks": false,
              "aot": true,
              "extractLicenses": true,
              "vendorChunk": false,
              "buildOptimizer": true,
              "fileReplacements": [
                {
                  "replace": "src/environments/environment.ts",
                  "with": "src/environments/environment.prod.ts"
                }
              ]
            }
          }
        },
        "serve": {
          "builder": "@angular-devkit/build-angular:dev-server",
          "options": {
            "browserTarget": "us-exceline-html:build",
            "port": 3000
          },
          "configurations": {
            "production": {
              "browserTarget": "us-exceline-html:build:production"
            }
          }
        },
        "extract-i18n": {
          "builder": "@angular-devkit/build-angular:extract-i18n",
          "options": {
            "browserTarget": "us-exceline-html:build"
          }
        },
        "test": {
          "builder": "@angular-devkit/build-angular:karma",
          "options": {
            "main": "src/test.ts",
            "karmaConfig": "./karma.conf.js",
            "polyfills": "src/polyfills.ts",
            "tsConfig": "src/tsconfig.spec.json",
            "scripts": [
              "node_modules/core-js/client/shim.min.js",
              "node_modules/zone.js/dist/zone.min.js",
              "node_modules/jquery/dist/jquery.min.js",
              "node_modules/tether/dist/js/tether.js",
              "node_modules/popper.js/dist/umd/popper.min.js",
              "node_modules/bootstrap/dist/js/bootstrap.min.js",
              "node_modules/moment/min/moment.min.js",
              "src/assets/js/ckeditor.js",
              "src/assets/js/utils.js",
              "src/assets/js/fullcalendar-old.js",
              "src/assets/js/scheduler-old.js",
              "src/assets/js/fullcalendar-rightclick.js"
            ],
            "styles": [
              "node_modules/intl-tel-input/build/css/intlTelInput.css",
              "src/styles.scss"
            ],
            "assets": [
              "src/assets",
              "src/favicon.ico"
            ]
          }
        },
        "lint": {
          "builder": "@angular-devkit/build-angular:tslint",
          "options": {
            "tsConfig": [
              "src/tsconfig.app.json",
              "src/tsconfig.spec.json"
            ],
            "exclude": []
          }
        }
      }
    },
    "us-exceline-html-e2e": {
      "root": "e2e",
      "sourceRoot": "e2e",
      "projectType": "application",
      "architect": {
        "e2e": {
          "builder": "@angular-devkit/build-angular:protractor",
          "options": {
            "protractorConfig": "./protractor.conf.js",
            "devServerTarget": "us-exceline-html:serve"
          }
        },
        "lint": {
          "builder": "@angular-devkit/build-angular:tslint",
          "options": {
            "tsConfig": [
              "e2e/tsconfig.e2e.json"
            ],
            "exclude": []
          }
        }
      }
    }
  },
  "defaultProject": "us-exceline-html",
  "schematics": {
    "@schematics/angular:component": {
      "prefix": "app",
      "styleext": "scss"
    },
    "@schematics/angular:directive": {
      "prefix": "app"
    }
  }
}
~~~~
Try `ng serve` now.

---

##### Runtime errors
**Error**

`Uncaught ReferenceError: global is not defined` 

**Fix**[^1]

~~~~html
<script>
  var global = global || window;
</script>
~~~~
in `index.html` `<head>`-tag.

**Error**

`angular-split` parsing errors

**Fix**[^2]

Run
~~~~bash
npm uninstall angular-split --save
npm install angular-split-ng6
~~~~
Change `import`statement in 

- `app.module.ts`
- `admin.module.ts`
- `reporting.module.ts`
- `shop.module.ts`

from
~~~~javascript
import { AngularSplitModule } from 'angular-split';
~~~~
to
~~~~javascript
import { AngularSplitModule } from 'angular-split-ng6';
~~~~

This is just a tempfix for `Angular 6`. Will be reverted to normal when updating to `Angular 7`.

### Notes
`Renderer` has been moved to `Renderer2` and some method-names are changed. 

- `Renderer.setElementStyle(element: any, style: string, value: any)` -> `Renderer2.setStyle(element: any, style: string, value: any)`
- `Renderer.setElementClass(renderElement: any, className: string, isAdd: boolean)` -> `Renderer2.addClass(el: any, name: string)`
- `Renderer.invokeElementMethod(renderElement: any, methodName: any, args?: any[])` -> `Renderer2.selectRootElement(selectorOrNode: any)`
- `Renderer.setElementProperty(renderElement: any, propertyName: string, propertyValue: any)` -> `Renderer2.setProperty(el: any, name: string, value, any)`

To use `Renderer2.selectRootElement()` with focus you just do:
~~~~ts
Renderer2.selectRootElement(selectorOrNode).focus();
~~~~

---

With the new RxJS version the `imports` have changed. `Observable`, `Subject`, `Subscription` etc. has been moved to the root folder of RxJS. Eg.
~~~~ts
import { Observable, Subject, Subscription } from 'rxjs';
~~~~
All operators on `Observables` has been moved to `rxjs/operators`. E.g.
~~~~ts
import { takeUntil, map, mergeMap, concatMap, first } from 'rxjs/operators';
~~~~
These needs to be changed accordingly to get Angular/RxJs 6 to compile.

---

`of`, `timer`, `from` and `throw` is no longer defined in `Observable()`[^3]. These needs to be importet from `rxjs` and used alone. E.g:
~~~~js
import { of, timer } from 'rxjs';

of(...)
~~~~
instead of
~~~~js
Observable.of(...)
~~~~
---
Operators on `Observables` has to be inside a `.pipe()` from RxJS ^6. You can no longer do:
~~~~ts
Observable.mergeMap().mergeMap().subscribe();
~~~~
Do this instead:
~~~~ts
import { Observable } from 'rxjs';
import { mergeMap, takeUntil } from 'rxjs/operators';

Observable.pipe(
  mergeMap(),
  mergeMap(),
  takeUntil()
).subscribe();
~~~~

---

`HttpHeaders` and `HttpParams` needs to be initialized this way:
~~~~js
const headers = new HttpHeaders()
    .append('key1', 'value1')
    .append('key2', 'value2');
    
const options = new HttpParams()
    .append('key1', 'value1')
    .append('key2', 'value2');
~~~~
If enteties needs to be added later do this:
~~~~js
headers = headers.append('key3', 'value3');
options = options.append('key3', 'value3');
~~~~

---

`HttpClient.request()` needs the request method (POST, GET etc) as first argument.

More information on the new HttpClient can be found [here](https://www.techiediaries.com/angular-httpclient/)


[^1]: https://stackoverflow.com/questions/50356408/upgrading-to-angular-6-x-gives-uncaught-referenceerror-global-is-not-defined#answer-50356546
[^2]: https://www.npmjs.com/package/angular-split-ng6
[^3]: https://www.learnrxjs.io/operators/creation/