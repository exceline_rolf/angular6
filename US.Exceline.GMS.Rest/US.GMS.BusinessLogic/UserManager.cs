﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.ResultNotifications;
using US.GMS.Data.DataAdapters;

namespace US.GMS.BusinessLogic
{
    public class UserManager
    {
        public static OperationResult<bool> UpdateSettingForUserRoutine(string key, string value, string user, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = UserFacade.UpdateSettingForUserRoutine(key, value, user, gymCode);
            }
            catch (Exception)
            {
                result.CreateMessage("Error on updating user routinr", MessageTypes.ERROR);
            }
            return result;
        }
    }
}
