﻿using System;
using System.Collections.Generic;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.SystemObjects;
using US.GMS.Data.DataAdapters;

namespace US.GMS.BusinessLogic
{
    public class ArticleManager
    {

        public static OperationResult<int> SaveArticle(ArticleDC articleDc, string user, int branchID, int activityCategoryId, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ArticleFacade.SaveArticle(articleDc, user, branchID, activityCategoryId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Saving Article" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }




        public static OperationResult<bool> ImportArticleList(List<ArticleDC> articleList, string user, int branchID, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ArticleFacade.ImportArticleList(articleList,  user,  branchID,  gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in get the Article" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ArticleDC>> GetArticles(int branchId, string user, ArticleTypes articleType, string keyWord, CategoryDC category, int activityId, bool? isObsalate, bool activeState, string gymCode, bool isFilterByBranch)
        {
            OperationResult<List<ArticleDC>> result = new OperationResult<List<ArticleDC>>();
            try
            {
                result.OperationReturnValue = ArticleFacade.GetArticles(-1, branchId, user, articleType, keyWord, category, activityId, isObsalate, activeState, gymCode, isFilterByBranch);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in get the Article" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ArticleDC>> GetArticlesSearch(int branchId, string keyWord, string gymCode)
        {
            OperationResult<List<ArticleDC>> result = new OperationResult<List<ArticleDC>>();
            try
            {
                result.OperationReturnValue = ArticleFacade.GetArticlesSearch(branchId, keyWord, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in get the Articles on search" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<ArticleDC> GetArticleById(int articleID, string gymCode)
        {
            OperationResult<ArticleDC> result = new OperationResult<ArticleDC>();
            try
            {
                List<ArticleDC> articles = new List<ArticleDC>();
                articles = ArticleFacade.GetArticles(articleID, -1, string.Empty, ArticleTypes.ALL, string.Empty, null, -1, false, true, gymCode, false);
                if (articles.Count > 0)
                    result.OperationReturnValue = articles[0];
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in get the Article" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<ArticleDC> GetInvoiceFee(int branchId, string gymCode)
        {
            OperationResult<ArticleDC> result = new OperationResult<ArticleDC>();
            try
            {
                result.OperationReturnValue = ArticleFacade.GetInvoiceFee(branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting invoicefee Article" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> UploadArticle(List<ArticleDC> articleList, string user, int branchId, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ArticleFacade.UploadArticle(articleList, user,branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in uploading Article" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
    }
}
