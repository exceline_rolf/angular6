﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "5/10/2012 11:17:16 AM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.ResultNotifications;
using US.GMS.Data.DataAdapters;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ScheduleManagement;

namespace US.GMS.BusinessLogic
{
    public class CommonBookingManager
    {
        public static OperationResult<bool> CheckCommonBookingAvailability(int activeTimeId, int entityId, int branchId, string entityType, DateTime startDateTime, DateTime endDateTime, string user, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = CommonBookingFacade.CheckCommonBookingAvailability(activeTimeId, entityId, branchId, entityType, startDateTime, endDateTime,  gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in check booking availability" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }


        public static OperationResult<bool> SaveCommonBookingDetails(CommonBookingDC commonBookingDC, ScheduleDC scheduleDC, int branchId, string user, string scheduleCategoryType, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = CommonBookingFacade.SaveCommonBookingDetails(commonBookingDC,scheduleDC, branchId, user,scheduleCategoryType,  gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in saving booking details" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
    }
}
