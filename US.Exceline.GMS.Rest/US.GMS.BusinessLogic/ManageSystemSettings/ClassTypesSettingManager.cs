﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageClassSettings;
using US.GMS.Core.ResultNotifications;
using US.GMS.Data.DataAdapters.ManageSystemSettings;

namespace US.GMS.BusinessLogic.ManageSystemSettings
{
    public class ClassTypesSettingManager
    {
        public static OperationResult<List<ExceClassTypeDC>> GetClassTypes(string gymCode)
        {
            OperationResult<List<ExceClassTypeDC>> result = new OperationResult<List<ExceClassTypeDC>>();
            try
            {
                result.OperationReturnValue = ClassSettingsFacade.GetClassTypes(gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting Class types" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ExceClassTypeDC>> GetClassTypesByBranch(int branchId, string gymCode)
        {
            OperationResult<List<ExceClassTypeDC>> result = new OperationResult<List<ExceClassTypeDC>>();
            try
            {
                result.OperationReturnValue = ClassSettingsFacade.GetClassTypeByBranch(branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting class types by branch" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }




        public static OperationResult<bool> AddEditClassType(ExceClassTypeDC classType, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ClassSettingsFacade.AddEditClassType(classType, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Adding Class Type" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> DeleteClassType(int classTypeId, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ClassSettingsFacade.DeleteClassType(classTypeId, gymCode);
            }
            catch(Exception ex)
            {
                result.CreateMessage("Error in deleting class type" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        
        
        }



    }
}
