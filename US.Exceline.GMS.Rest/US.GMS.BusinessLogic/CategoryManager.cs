﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "4/19/2012 3:22:49 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Data.DataAdapters;
using US.GMS.Core.DomainObjects.ManageContracts;

namespace US.GMS.BusinessLogic
{
    public class CategoryManager
    {
        public static OperationResult<List<CategoryTypeDC>> GetCategoryTypes(string name, string user, int branchId, string gymCode)
        {
            OperationResult<List<CategoryTypeDC>> result = new OperationResult<List<CategoryTypeDC>>();
            try
            {
                result.OperationReturnValue = CategoryFacade.GetCategoryTypes(name, user, branchId,  gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Category Type Getting" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> SaveCategory(CategoryDC category, string user, int branchId, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = CategoryFacade.SaveCategory(category, user, branchId,  gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Category Saving" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> SaveRegion(RegionDC region, string user, int branchId, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = CategoryFacade.SaveRegion(region, user, branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Region Saving" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<CategoryDC>> GetCategoriesByType(string type, string user, string gymCode)
        {
            OperationResult<List<CategoryDC>> result = new OperationResult<List<CategoryDC>>();
            try
            {
                result.OperationReturnValue = CategoryFacade.GetCategoriesByType(type,user,gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Categories" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> UpdateCategory(CategoryDC category, string user, int branchId, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = CategoryFacade.UpdateCategory(category, user, branchId,  gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Category Updating" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> DeleteCategory(int categoryId, string user, int branchId, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = CategoryFacade.DeleteCategory(categoryId, user, branchId,  gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Category Deleting" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<CategoryDC>> SearchCategories(string searchText, string searchType, string user, int branchId, string gymCode)
        {
            OperationResult<List<CategoryDC>> result = new OperationResult<List<CategoryDC>>();
            try
            {
                result.OperationReturnValue = CategoryFacade.SearchCategories(searchText,searchType, user, branchId,  gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Categories" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<RegionDC>> GetRegions(int branchId, string gymCode,string countryId)
        {
            OperationResult<List<RegionDC>> result = new OperationResult<List<RegionDC>>();
            try
            {
                result.OperationReturnValue = CategoryFacade.GetRegions(branchId, gymCode, countryId);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Regions" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
    }
}
