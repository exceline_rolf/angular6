﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.SystemObjects;
using US.GMS.Data.DataAdapters;

namespace US.GMS.BusinessLogic
{
    public class SystemManager
    {
        public static OperationResult<List<ExceAccessProfileDC>> GetAccessProfiles(string gymCode, Gender gender)
        {
            OperationResult<List<ExceAccessProfileDC>> result = new OperationResult<List<ExceAccessProfileDC>>();
            try
            {
                result.OperationReturnValue = SystemFacade.GetAccessProfiles(gymCode, gender);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Access Profiles" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> UpdateAccessProfiles(ExceAccessProfileDC AccessProfile, string user, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = SystemFacade.UpdateAccessProfiles(AccessProfile, user, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Access Profiles" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<String>> ActGetFileData(string gymCode)
        {
            OperationResult<List<String>> result = new OperationResult<List<String>>();
            try
            {
                result.OperationReturnValue = SystemFacade.ActGetFileData(gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting FileData" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> SaveTaskStatus(string taskName, string description, int branchId, int status, string gymCode)
        {

            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = SystemFacade.SaveTaskStatus(taskName, description, branchId, status, gymCode);
            }
            catch (Exception)
            {
                result.CreateMessage("Error on Saving task status", MessageTypes.ERROR);
            }
            return result;
        }


        public static OperationResult<bool> ValidateEmail(string email, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = SystemFacade.ValidateEmail(email, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in validating email" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> ValidateMemberCard(string memberCard, int memberId, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = SystemFacade.ValidateMemberCard(memberCard, memberId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in validating member card" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> ValidateGatCardNo(string gatCardNo, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = SystemFacade.ValidateGatCardNo(gatCardNo, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in validating gat card" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<Tuple<int, string>> ValidateMobile(string mobile, string mobilePrefix, string gymCode)
        {
            OperationResult<Tuple<int, string>> result = new OperationResult<Tuple<int, string>>();
            try
            {
                result.OperationReturnValue = SystemFacade.ValidateMobile(mobile, mobilePrefix, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in validating mobile" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
    }
}
