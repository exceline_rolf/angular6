﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.ResultNotifications;
using US.GMS.Data.DataAdapters;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;

namespace US.GMS.BusinessLogic
{
    public class UtilityManager
    {
        public static OperationResult<string> GetCityForPostalCode(string postalCode, string gymCode)
        {
            OperationResult<string> result = new OperationResult<string>();
            try
            {
                result.OperationReturnValue = UtilityFacade.GetCityForPostalCode(postalCode, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting city data " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<CalendarHoliday>> GetHolidays(DateTime calendarStartDate, int branchId, string gymCode)
        {
            OperationResult<List<CalendarHoliday>> result = new OperationResult<List<CalendarHoliday>>();
            try
            {
                result.OperationReturnValue = UtilityFacade.GetHolidays(calendarStartDate, branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting holi days " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
    }
}
