﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Data.DataAdapters;
using US.GMS.Core.SystemObjects;

namespace US.GMS.BusinessLogic
{
    public class ScheduleManager
    {
        public static OperationResult<bool> UpdateActiveTime(EntityActiveTimeDC activeTime, bool isDelete,int branchId,string user, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ScheduleFacade.UpdateActiveTime(activeTime, isDelete,branchId,user, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in updating the active time" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> UpdateCalenderActiveTimes(List<EntityActiveTimeDC> activeTimeList,int branchId,string user, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ScheduleFacade.UpdateCalenderActiveTimes(activeTimeList,branchId,user, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in updating calender active times" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> UpdateSheduleItems(ScheduleDC schedule, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ScheduleFacade.UpdateSheduleItems(schedule, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in updating the active time" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<EntityActiveTimeDC>> GetEntityActiveTimes(int branchId, DateTime startDate, DateTime endDate, List<int> entityList, string entityType, string gymCode, string user)
        {
            OperationResult<List<EntityActiveTimeDC>> result = new OperationResult<List<EntityActiveTimeDC>>();
            try
            {
                result.OperationReturnValue = ScheduleFacade.GetEntityActiveTimes(branchId, startDate, endDate, entityList, entityType, gymCode, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting active times" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }


        public static OperationResult<ScheduleDC> GetEntitySchedule(int entityId, string entityRoleType, string user, string gymCode)
        {
            OperationResult <ScheduleDC> result = new OperationResult <ScheduleDC>();
            try
            {
                result.OperationReturnValue = ScheduleFacade.GetEntitySchedule(entityId, entityRoleType, user, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting active times" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> DeleteScheduleItem(int scheduleItemId, bool activeStatus, string user, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ScheduleFacade.DeleteScheduleItem(scheduleItemId,activeStatus, user,  gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Delete Schedule Item" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> SaveShedule(int id, string name, string roleId, int branchId, ScheduleDC sheduleDc, string user, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ScheduleFacade.SaveShedule(id, name, roleId, branchId, sheduleDc, user,  gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Save Shedule" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ScheduleItemDC>> GetScheduleItemsByScheduleId(int scheduleId, string gymCode)
        {
            OperationResult<List<ScheduleItemDC>> result = new OperationResult<List<ScheduleItemDC>>();
            try
            {
                result.OperationReturnValue = ScheduleFacade.GetScheduleItemsByScheduleId(scheduleId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Save Shedule" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> CheckScheduleOverlapForEmployee(ScheduleItemDC scheduleItem, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ScheduleFacade.CheckScheduleOverlapForEmployee(scheduleItem, gymCode);
            }
            catch(Exception ex)
            {
                result.CreateMessage("Error in check overlap schedule" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
    }
}
