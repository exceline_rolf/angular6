﻿using System;
using System.Collections.Generic;
using US.GMS.Core.DomainObjects.Admin;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.SystemObjects;
using US.GMS.Data;

namespace US.GMS.BusinessLogic
{
    public class GymDetailHandler
    {
        public static OperationResult<string> GetGymConnection(string gymCode)
        {
            OperationResult<string> result = new OperationResult<string>();
            try
            {
                result.OperationReturnValue = GymDetailFacade.GetGymConnection(gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting Gym Connection" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> GetGymCompanyId(string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = GymDetailFacade.GetGymCompanyId(gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting Gym id" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<System.Collections.Generic.List<Core.SystemObjects.BranchDetailsDC>> GetBranchDetails()
        {
            OperationResult<List<BranchDetailsDC>> result = new OperationResult<List<BranchDetailsDC>>();
            try
            {
                result.OperationReturnValue = GymDetailFacade.GetBranchDetails();
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting branch Details" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<string>> GetGymCodes()
        {
            OperationResult<List<string>> result = new OperationResult<List<string>>();
            try
            {
                result.OperationReturnValue = GymDetailFacade.GetGymCodes();
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting gym codes" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<BranchDetailsDC>> GetBranches(string gymCode)
        {
            OperationResult<List<BranchDetailsDC>> result = new OperationResult<List<BranchDetailsDC>>();
            try
            {
                result.OperationReturnValue = GymDetailFacade.GetBranches(gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting branches " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ExceUserBranchDC>> GetGymsforAccountNumber(string accountNumber, string gymCode)
        {
            OperationResult<List<ExceUserBranchDC>> result = new OperationResult<List<ExceUserBranchDC>>();
            try
            {
                result.OperationReturnValue = GymDetailFacade.GetGymsforAccountNumber(accountNumber, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting branches for account number " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ExcelineRoleDc>> GetUserRole(string gymCode)
        {
            var result = new OperationResult<List<ExcelineRoleDc>>();
            try
            {
                result.OperationReturnValue = GymDetailFacade.GetUserRole(gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting user role " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
    }
}
