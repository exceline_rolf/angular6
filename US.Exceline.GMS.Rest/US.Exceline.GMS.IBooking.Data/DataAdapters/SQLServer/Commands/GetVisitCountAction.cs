﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using US.GMS.Core.DomainObjects.IBooking;
using US_DataAccess;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class GetVisitCountAction : USDBActionBase<List<ExceIBookingVisitCount>>
    {
        private readonly int _branchId;
        private readonly int _year;
        private readonly int _companyId;

        public GetVisitCountAction(int branchId, int year, int systemId)
        {
            _branchId = branchId;
            _year = year;
            _companyId = systemId;
        }

        protected override List<ExceIBookingVisitCount> Body(System.Data.Common.DbConnection connection)
        {
            List<ExceIBookingVisitCount> visitList = new List<ExceIBookingVisitCount>();
            string spName = "USExceGMSIBookingGetVisitsCount";
            DbDataReader reader = null;
            try
            {
                var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@branchId", System.Data.DbType.Int32, _branchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@year", System.Data.DbType.Int32, _year));
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ExceIBookingVisitCount visit = new ExceIBookingVisitCount();
                    visit.Year = Convert.ToInt32(reader["Year"]);
                    visit.Month = Convert.ToInt32(reader["Month"]);
                    visit.Count = Convert.ToInt32(reader["Count"]);                   

                    visitList.Add(visit);

                }

                if (reader != null)
                    reader.Close();

                return visitList;
            }
            catch (Exception)
            {
                throw;
            }

        }

    }
}
