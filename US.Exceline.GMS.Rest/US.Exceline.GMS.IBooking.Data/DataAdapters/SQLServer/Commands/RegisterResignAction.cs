﻿using System;
using System.Data.SqlClient;
using US_DataAccess;
using System.Data.Common;
using US.GMS.Core.DomainObjects.IBooking;
using System.Data;
using System.Globalization;
using System.Collections.Generic;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class RegisterResignAction : USDBActionBase<int>
    {
        private readonly ExceIBookingNewResign _resignItem;       

        public RegisterResignAction(ExceIBookingNewResign resign)
        {
            _resignItem = resign;
        }

        protected override int Body(DbConnection connection)
        {
            const string storedProcedureName = "USExceGMSIBookingSaveResignation";
            
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MemberContractId", DbType.Int32, _resignItem.MemberContractId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ResignDate", DbType.DateTime, DateTime.ParseExact(_resignItem.ResignedDate, "yyyy.MM.dd", CultureInfo.InvariantCulture)));                                             
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ResignCategoryId", DbType.Int32, _resignItem.ResignCategoryId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MemberFeeArticleID", DbType.Int32, _resignItem.MemberFeeArticleID));


                cmd.ExecuteScalar();   
                return 1;

            }
            catch (Exception ex)
            {
                return -1;
                throw ex;
            }
     
        }
    }
}
