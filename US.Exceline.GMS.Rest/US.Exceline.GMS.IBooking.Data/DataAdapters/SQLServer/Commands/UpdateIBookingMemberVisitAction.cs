﻿using System;
using System.Data.SqlClient;
using US_DataAccess;
using System.Data.Common;
using US.GMS.Core.DomainObjects.IBooking;
using System.Data;
using US.GMS.Core.DomainObjects.ManageMemberships;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateIBookingMemberVisitAction : USDBActionBase<int>
    {
        private readonly EntityVisitDC _memberVisit;

        public UpdateIBookingMemberVisitAction(EntityVisitDC visit)
        {
            _memberVisit = visit;
        }

        protected override int Body(DbConnection connection)
        {
            const string storedProcedureName = "USExceGMSIBookingInsertMemberVisits";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CustomerNo", DbType.String, _memberVisit.CutomerNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _memberVisit.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InTime", DbType.DateTime, _memberVisit.InTime));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@VisitDate", DbType.DateTime, _memberVisit.VisitDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CountVisit", DbType.Boolean, _memberVisit.CountVist));
                if (_memberVisit.MemberContractId != -1 && _memberVisit.MemberContractId != 0)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ContractID", DbType.Int32, _memberVisit.MemberContractId));

                if (_memberVisit.ArticleID != -1 && _memberVisit.ArticleID != 0)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ArticleId", DbType.Int32, _memberVisit.ArticleID));

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@VisitType", DbType.String, _memberVisit.VisitType));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Description", DbType.String, _memberVisit.ItemName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ActivityId", DbType.Int32, _memberVisit.ActivityId));
                cmd.ExecuteNonQuery();
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
