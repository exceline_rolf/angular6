﻿using System;
using System.Collections.Generic;
using US_DataAccess;
using US.GMS.Core.DomainObjects.IBooking;
using System.Data.Common;
using System.Data;
using System.Collections;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    class GetBrisIdActiveFreezedAction : USDBActionBase<List<int?>>
    {

        public GetBrisIdActiveFreezedAction()
        {

        }

        protected override List<int?> Body(DbConnection connection)
        {

            List<int?> userIds = new List<int?>();

            const string storedProcedure = "GetBRISUserIdsActiveFreezedfromAPI";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    userIds.Add(Convert.ToInt32(reader["BrisID"]));


                    ArrayList list = new ArrayList();



                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return userIds;
        }

    }
}
