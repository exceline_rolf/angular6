﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.IBooking.Core;
using US_DataAccess;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class AddClassVisitAction : USDBActionBase<int>
    {
        private int _gymID = -1;
        private int _classID = -1;
        private List<MemberVisitStatus> _visits;

        public AddClassVisitAction(string gymID, string classID, List<MemberVisitStatus> Visits)
        {
            _gymID = Convert.ToInt32(gymID);
            _classID = Convert.ToInt32(classID);
            _visits = Visits;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSIBookingAddClassVisit";
            try
            {
                DataTable visits = GetDataTable(_visits);
                DbCommand command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _gymID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@classId", DbType.Int32, _classID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Visits", SqlDbType.Structured, visits));
                command.ExecuteNonQuery();
                return 1;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private DataTable GetDataTable(List<MemberVisitStatus> visits)
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn("CustomerID", typeof(int)));
            dataTable.Columns.Add(new DataColumn("VisitStatus", typeof(bool)));

            foreach (MemberVisitStatus item in visits)
            {
                DataRow newRow = dataTable.NewRow();
                newRow["CustomerID"] = Convert.ToInt32(item.CustomerID);
                newRow["VisitStatus"] = Convert.ToBoolean(item.Status);
                dataTable.Rows.Add(newRow);
            }
            return dataTable;
        }
    }
}
