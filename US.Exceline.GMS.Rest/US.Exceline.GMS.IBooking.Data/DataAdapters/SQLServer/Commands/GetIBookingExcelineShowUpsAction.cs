﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.IBooking;
using US_DataAccess;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class GetIBookingExcelineShowUpsAction : USDBActionBase<List<ExceIBookingShowUp>>
    {
        private readonly int _branchId;
        private readonly DateTime _fromDate;
        private readonly DateTime _toDate;
        public GetIBookingExcelineShowUpsAction(int branchId, DateTime fromDate, DateTime toDate)
        {
            _branchId = branchId;
            _fromDate = fromDate;
            _toDate = toDate;
        }

        protected override List<ExceIBookingShowUp> Body(DbConnection connection)
        {
            List<ExceIBookingShowUp> showUpList = new List<ExceIBookingShowUp>();
            const string storedProcedure = "USExceGMSIBookingGetShowUps";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@fromDate", DbType.Date, _fromDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@toDate", DbType.Date, _toDate));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ExceIBookingShowUp exceShowUp = new ExceIBookingShowUp();
                    exceShowUp.CustomerNo = Convert.ToInt32(reader["CustomerId"]);
                    exceShowUp.ShowupDate = Convert.ToString(reader["ShowUpDate"]);
                    exceShowUp.ShowupTime = Convert.ToString(reader["ShowUpTime"]);
                    exceShowUp.ClassActivityType = Convert.ToInt32(reader["ClassActivityType"]);
                    showUpList.Add(exceShowUp);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return showUpList;
        }
    }
}
