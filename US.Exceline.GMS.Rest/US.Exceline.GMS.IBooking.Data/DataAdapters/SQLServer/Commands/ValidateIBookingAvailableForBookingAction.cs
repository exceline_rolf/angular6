﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US_DataAccess;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
   public class ValidateIBookingAvailableForBookingAction : USDBActionBase<Dictionary<int,int>>
   {
       private readonly List<int> _resourceIdList;
       private readonly DateTime _startDateTime;
       private readonly DateTime _endDateTime;
       public ValidateIBookingAvailableForBookingAction(List<int> resourceIdList,DateTime startDateTime,DateTime endDateTime)
       {
           _resourceIdList = resourceIdList;
           _startDateTime = startDateTime;            
           _endDateTime = endDateTime;
       }
       protected override Dictionary<int, int> Body(DbConnection connection)
        {
           const string storedProcedureName = "USExceGMSIBookingAvailableForBooking";
           Dictionary<int, int> result = new Dictionary<int, int>();
            try
            {
                

                foreach (var resourceId in _resourceIdList)
                {
                    DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@Resourceid", DbType.Int32, resourceId));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@Startdatetime", DbType.DateTime, _startDateTime));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@Enddatetime", DbType.DateTime, _endDateTime));

                    DbParameter output = new SqlParameter();
                    output.DbType = DbType.Int32;
                    output.ParameterName = "@Outid";
                    output.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(output);
                    cmd.ExecuteNonQuery();
                   int outPutValue  = Convert.ToInt32(output.Value);
                   if (outPutValue == -1)
                   {
                       result.Add(resourceId, outPutValue);
                       break;
                   }
                   result.Add(resourceId, outPutValue);
                   cmd.Parameters.Clear();
                }
                return result;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
