﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using US.GMS.Core.DomainObjects.IBooking;
using US_DataAccess;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class GetIBookingResourceBookingAction : USDBActionBase<List<ExceIBookingResourceBooking>>
    {
        private readonly int _branchId;
        private readonly DateTime _fromDate;
        private readonly DateTime _toDate;
        public GetIBookingResourceBookingAction(int branchId, DateTime fromDate, DateTime toDate)
        {
            _branchId = branchId;
            _fromDate = fromDate;
            _toDate = toDate;
        }
        protected override List<ExceIBookingResourceBooking> Body(DbConnection connection)
        {
            var exceIBookingResourceBookingList = new List<ExceIBookingResourceBooking>();
            const string storedProcedure = "USExceGMSIBookingGetResourceBookings";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@fromDate", DbType.DateTime, _fromDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@endDate", DbType.DateTime, _toDate));

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var exceIBooking = new ExceIBookingResourceBooking();

                    exceIBooking.Id = Convert.ToInt32(reader["BookingId"]);
                    string memberIdList = string.Empty;
                    if(reader["CustomerIds"] != null)
                     memberIdList = reader["CustomerIds"].ToString();

                    var memberList = new List<int>();
                    foreach (var memberId in memberIdList.Split(',').ToList())
                    {
                     int id;
                        if (Int32.TryParse(memberId, out id))
                            memberList.Add(id);
                    }

                    exceIBooking.CustomerIdList = memberList;
                    exceIBooking.ResourceId = Convert.ToInt32(reader["ResId"]);
                    exceIBooking.FromDateTime = Convert.ToString(reader["FromDateTime"]);
                    exceIBooking.ToDateTime = Convert.ToString(reader["EndDateTime"]);

                    if (reader["BookingCategoryId"] != DBNull.Value)
                    exceIBooking.BookingCategoryId = Convert.ToInt32(reader["BookingCategoryId"]);

                    exceIBooking.ArrivalStatus = Convert.ToBoolean(reader["ArrivalStatus"]);
                    exceIBooking.Paid = Convert.ToBoolean(reader["Paid"]);

                    exceIBookingResourceBookingList.Add(exceIBooking);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return exceIBookingResourceBookingList;
        }
    }
}
