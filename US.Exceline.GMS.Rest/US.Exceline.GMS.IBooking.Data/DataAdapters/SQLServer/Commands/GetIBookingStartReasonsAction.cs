﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.IBooking;
using US_DataAccess;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class GetIBookingStartReasonsAction : USDBActionBase<List<ExceIBookingStartReason>>
    {

        protected override List<ExceIBookingStartReason> Body(DbConnection connection)
        {
            List<ExceIBookingStartReason> startReasonList = new List<ExceIBookingStartReason>();
            const string storedProcedure = "USExceGMSIBookingGetStartReasons";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ExceIBookingStartReason exceStartReason = new ExceIBookingStartReason();
                    exceStartReason.StartReasonId = Convert.ToInt32(reader["StartReasonId"]);
                    exceStartReason.StartReasonName = Convert.ToString(reader["StartReasonName"]);
                    startReasonList.Add(exceStartReason);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return startReasonList;
        }
    }
}
