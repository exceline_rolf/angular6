﻿using System;
using System.Data.SqlClient;
using US_DataAccess;
using System.Data.Common;
using US.GMS.Core.DomainObjects.IBooking;
using System.Data;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateIBookingMemberClassVisitAction : USDBActionBase<int>
    {
        private readonly ExceIBookingMemberClassVisit _visit;

        public UpdateIBookingMemberClassVisitAction(ExceIBookingMemberClassVisit visit)
        {
            _visit = visit;
        }

        protected override int Body(DbConnection connection)
        {
            const string storedProcedureName = "USExceGMSIBookingUpdateMemberClassVisit";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _visit.GymId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@customerId", DbType.Int32, _visit.CustomerId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@classId", DbType.Int32, _visit.ClassId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@visitStatus", DbType.Boolean,_visit.VisitStatus));
                DbParameter output = new SqlParameter();
                output.DbType = DbType.Int32;
                output.ParameterName = "@OutID";
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);
                cmd.ExecuteNonQuery();
                return Convert.ToInt32(output.Value); 
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
