﻿using System;
using System.Data;
using US_DataAccess;
using US.GMS.Core.DomainObjects.IBooking;
using System.Data.Common;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class GetIBookingSystemDetailsAction : USDBActionBase<ExceIBookingSystem>
    {
        private int _systemId;
        public GetIBookingSystemDetailsAction(int systemId)
        {
            _systemId = systemId;
        }

        protected override ExceIBookingSystem Body(DbConnection connection)
        {
            ExceIBookingSystem company = new ExceIBookingSystem();
            const string storedProcedureName = "USExceGMSIBookingGetCompanyDetails";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@systemId", DbType.Int32, _systemId));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    company.SystemName = Convert.ToString(reader["GymCompanyName"]);
                    company.Address1 = Convert.ToString(reader["Address1"]);
                    company.Address2 = Convert.ToString(reader["Address2"]);
                    company.Address3 = Convert.ToString(reader["Address3"]);
                    company.PostCode = Convert.ToString(reader["PostalCode"]);
                    company.PostPlace = Convert.ToString(reader["PostalPlace"]);
                    company.ContactPerson = Convert.ToString(reader["ContactPerson"]);
                    company.Email = Convert.ToString(reader["Email"]);
                    company.Mobile = Convert.ToString(reader["Mobile"]).Trim();
                    company.AccountNumber = Convert.ToString(reader["AccountNo"]).Trim();
                    company.ContractConditions = Convert.ToString(reader["ContractConditions"]);
                }
                return company;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
