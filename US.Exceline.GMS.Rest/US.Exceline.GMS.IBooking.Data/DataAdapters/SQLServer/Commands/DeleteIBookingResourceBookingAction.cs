﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US_DataAccess;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
   public class DeleteIBookingResourceBookingAction : USDBActionBase<int>
   {
       private readonly int _bookingId;
       private readonly int _branchId;
       public DeleteIBookingResourceBookingAction(int bookingId, int branchId)
       {
           _bookingId = bookingId;
           _branchId = branchId;
       }
        protected override int Body(DbConnection connection)
        {
            const string storedProcedureName = "USExceGMSIBookingDeleteBooking";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Bookingid", DbType.Int32, _bookingId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                DbParameter output = new SqlParameter();
                output.DbType = DbType.Int32;
                output.ParameterName = "@Outid";
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);
                cmd.ExecuteNonQuery();
                return Convert.ToInt32(output.Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
