﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.IBooking;
using US_DataAccess;

namespace US.Exceline.GMS.IBooking.Data.DataAdapters.SQLServer.Commands
{
    public class GetIBookingPaymentsAction : USDBActionBase<List<ExceIBookingPayment>>
    {
        private readonly int _branchId;
        private readonly DateTime _fromDate;
        private readonly DateTime _toDate;
        public GetIBookingPaymentsAction(int branchId, DateTime fromDate, DateTime toDate)
        {
            _branchId = branchId;
            _fromDate = fromDate;
            _toDate = toDate;
        }
        protected override List<ExceIBookingPayment> Body(DbConnection connection)
        {
            List<ExceIBookingPayment> paymentList = new List<ExceIBookingPayment>();
            const string storedProcedure = "USExceGMSIBookingGetPayments";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@fromDate", DbType.Date, _fromDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@toDate", DbType.Date, _toDate));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ExceIBookingPayment excePayment = new ExceIBookingPayment();
                    excePayment.CustomerNo = Convert.ToInt32(reader["CustomerId"]);
                    excePayment.ContractNumber = reader["ContractNumber"].ToString();
                    excePayment.ClassActivityType =  Convert.ToInt32(reader["ClassActivityType"]);
                    excePayment.DueDate = Convert.ToString(reader["DueDate"]);
                    excePayment.PaidDate = Convert.ToString(reader["PaidDate"]);
                    excePayment.PaidDate = Convert.ToString(reader["PaidDate"]);
                    if (reader["OfferingId"] != DBNull.Value)
                    {
                        excePayment.OfferingId = Convert.ToInt32(reader["OfferingId"]);
                    }
                    if (reader["PaymentNumber"] != DBNull.Value)
                    {
                        excePayment.PaymentNumber = Convert.ToInt32(reader["PaymentNumber"]);
                    }
                    if (reader["InvoiceAmount"] != DBNull.Value)
                    {
                        excePayment.InvoiceAmount = Convert.ToDecimal(reader["InvoiceAmount"]);
                    }
                    excePayment.InvoiceText = Convert.ToString(reader["InvoiceText"]);
                    paymentList.Add(excePayment);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return paymentList;
        }
    }
}
