﻿using SessionManager;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using US.Common.Logging.API;
using US.Common.Notification.API;
using US.Common.Notification.Core.DomainObjects;
using US.Common.Notification.Core.Enums;
using US.Common.USSAdmin.API;
using US.Common.Web.UI.Core.SystemObjects;
using US.Communication.API;
using US.Communication.Core.SystemCore.DomainObjects;
using US.Exceline.GMS.Modules.Admin.API.ManageEmployees;
using US.Exceline.GMS.Modules.Admin.API.ManageResources;
using US.Exceline.GMS.Modules.Admin.API.ManageSystemSettings;
using US.Exceline.GMS.Modules.Login.API;
using US.Exceline.GMS.Modules.ManageMembership.API.ManageMembership;
using US.Exceline.GMS.Modules.ManageMembership.RestApi.Models;
using US.GMS.API;
using US.GMS.API.ManageSystemSettings;
using US.GMS.Core.DomainObjects.Admin;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.Notification;
using US.GMS.Core.DomainObjects.Payments;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.Utils;
using US.Payment.Core.Enums;
using US.Payment.Core.ResultNotifications;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Globalization;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Threading;
using US.Exceline.GMS.Modules.ManageMembership.RestApi.BRIS;
using System.Xml;
using WebSupergoo.ABCpdf8;
using US.GMS.Core.DomainObjects.Economy;
using System.Diagnostics;
using Newtonsoft.Json.Linq;

namespace US.Exceline.GMS.Modules.ManageMembership.RestApi.Controllers
{
    [RoutePrefix("api/Member")]
    public class MemberController : ApiController
    {
        [HttpGet]
        [Route("Test")]
        public HttpResponseMessage Test(int branchID, string searchText, int statuse, MemberSearchType searchType)
        {
            //string user = Request.Headers.GetValues("UserName").First();
            //return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(null, ApiResponseStatus.OK));

            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.GetMembers(1, "", 1, MemberSearchType.SEARCH, MemberRole.MEM, user, ExceConnectionManager.GetGymCode(user), 1, true, true, "");
               // var result = GMSManageMembership.GetExceMember();
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExcelineMemberDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExcelineMemberDC>(), ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpGet]
        [Route("GetMemberDetails")]
        [Authorize]
        public HttpResponseMessage GetMemberDetailsByMemberId(int branchID, int memberID, string memberRole)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();

                OperationResult<OrdinaryMemberDC> selectedMember = GMSManageMembership.GetMemberDetailsByMemberId(branchID, user, memberID, memberRole, ExceConnectionManager.GetGymCode(user));
                if (selectedMember.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in selectedMember.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    //selectedMember.OperationReturnValue.ImageURLDomain = ConfigurationManager.AppSettings["ImageUrl"]+'/'+
                    //    selectedMember.OperationReturnValue.ImageURLDomain+'/'+selectedMember.OperationReturnValue.CustId+ ".jpg";
                    //string imagePath = selectedMember.OperationReturnValue.ImagePath;
                

                    selectedMember.OperationReturnValue.IsBrisIntegrated = IsBrisIntegration();
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(selectedMember.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpPost]
        [Route("AddEventlogForImangeChange")]
        [Authorize]
        public HttpResponseMessage AddEventlogForImangeChange(USNotificationTree notification)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                OperationResult<bool> result = GMSManageMembership.ImageChangedAddEvent(notification, ExceConnectionManager.GetGymCode(user), user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)

                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse("-1", ApiResponseStatus.ERROR, errorMsg));

                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse("-1", ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpPost]
        [Route("UpdateMember")]
        [Authorize]
        public HttpResponseMessage UpdateMember(OrdinaryMemberDC member)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                OperationResult<string> result = GMSManageMembership.UpdateMember(member, GetConfigurations(user), ExceConnectionManager.GetGymCode(user), user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)

                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse("-1", ApiResponseStatus.ERROR, errorMsg));

                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse("-1", ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpGet]
        [Route("GetCategories")]
        [Authorize]
        public HttpResponseMessage GetCategories(string type)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                OperationResult<List<CategoryDC>> result = GMSCategory.GetCategoriesByType(type, user, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<CategoryDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<CategoryDC>(), ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [NonAction]
        public bool IsBrisIntegration()
        {
            try
            {
                string brisIntegration = ConfigurationManager.AppSettings["BrisIntegration"];
                if (brisIntegration == "1")
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        [HttpGet]
        [Route("GetCountries")]
        [Authorize]
        public HttpResponseMessage GetCountryDetails()
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                OperationResult<List<CountryDC>> result = GMSMember.GetCountryDetails(ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<CountryDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<CountryDC>(), ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpGet]
        [Route("ValidateMemberStatus")]
        [Authorize]
        public HttpResponseMessage ValidateMemberWithStatus(int memberId, int statusId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.ValidateMemberWithStatus(memberId, statusId, user, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (var message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(0, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(0, ApiResponseStatus.ERROR, errorMsg));
            }
        }



        [HttpPost]
        [Route("UpdateUserRoutine")]
        [Authorize]
        public HttpResponseMessage UpdateSettingForUserRoutine(string key, string value)
        {

            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSUser.UpdateSettingForUserRoutine(key, value, user, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpGet]
        [Route("GetVisitedMembers")]
        [Authorize]
        public HttpResponseMessage GetVisitedMembers()
        {

            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.GetVisitedMembers(user, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExcelineMemberDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExcelineMemberDC>(), ApiResponseStatus.ERROR, errorMsg));
            }
        }



        [HttpPost]
        [Route("MembercardVisit")]
        [Authorize]
        public HttpResponseMessage MembercardVisit(int memberID)
        {

            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.MembercardVisit(memberID, user, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExcelineMemberDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExcelineMemberDC>(), ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpPost]
        [Route("GetMemberList")]
        [Authorize]
        public HttpResponseMessage GetMemberList(FilterMemberList parameters)
        {

            try
            {   
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.GetMemberList(parameters, user, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<MemberForMemberlist>(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExcelineMemberDC>(), ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpGet]
        [Route("GetMemberCount")]
        [Authorize]
        public HttpResponseMessage GetMemberCount(int branchId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.GetMemberCount(ExceConnectionManager.GetGymCode(user), branchId);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExcelineMemberDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExcelineMemberDC>(), ApiResponseStatus.ERROR, errorMsg));
            }
        }



        [HttpGet]
        [Route("GetMembers")]
        [Authorize]
        public HttpResponseMessage GetMembers(int branchId, string searchText, int statuse, MemberSearchType searchType, MemberRole memberRole, int hit, bool isHeaderClick, bool isAscending, string sortName)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.GetMembers(branchId, searchText, statuse, searchType, memberRole, user, ExceConnectionManager.GetGymCode(user), hit, isHeaderClick, isAscending, sortName);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExcelineMemberDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExcelineMemberDC>(), ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpGet]
        [Route("GetUserSelectedBranches")]
        [Authorize]
        public HttpResponseMessage GetUserSelectedBranches()
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = ManageExcelineLogin.GetUserSelectedBranches(user, user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (USNotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExceUserBranchDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExceUserBranchDC>(), ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpGet]
        [Route("GetMemberStatus")]
        [Authorize]
        public HttpResponseMessage GetMemberStatus(bool getAllStatuses)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.GetMemberStatus(ExceConnectionManager.GetGymCode(user), getAllStatuses);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<MemberStatus>(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    List<MemberStatusDetails> statusDetailsList = new List<MemberStatusDetails>();
                    foreach(KeyValuePair<int, string> status in result.OperationReturnValue)
                    {
                        MemberStatusDetails statusDetails = new MemberStatusDetails()
                        {
                            StatusID = status.Key,
                            StatusName = status.Value
                        };
                        statusDetailsList.Add(statusDetails);
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(statusDetailsList, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<MemberStatus>(), ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpGet]
        [Route("GetMemberSearchCategory")]
        [Authorize]
        public HttpResponseMessage GetMemberSearchCategory(int branchId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.GetMemberSearchCategory(branchId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(string.Empty, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(string.Empty, ApiResponseStatus.ERROR, errorMsg));
            }
        }



        [HttpPost]
        [Route("SavePostalArea")]
        [Authorize]
        public HttpResponseMessage SavePostalArea(PostalData postalData)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSMember.SavePostalArea(user, postalData.postalCode, postalData.postalName, postalData.population, postalData.houseHold, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpGet]
        [Route("GetMemberBasicInfo")]
        [Authorize]
        public HttpResponseMessage GetMemberBasicInfo(int memberId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.GetMemberBasicInfo(memberId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new CustomerBasicInfoDC(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new CustomerBasicInfoDC(), ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpPost]
        [Route("SaveMember")]
        [Authorize]
        public HttpResponseMessage SaveMember(SaveMemberModel memberModel)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();

                if (IsBrisIntegration())
                {
                    var brisIntegrationApiKey = ConfigurationManager.AppSettings["Bris_Integration_ApiKey"];
                    //get the agresso ID If it's not found 
                    var brisServiceIntegration = new BrisServiceIntegration();
                    try
                    {
                        if (memberModel.Member.UserId > 0)
                        {
                            if (!(memberModel.Member.AgressoId > 0))
                            {
                                memberModel.Member.AgressoId = brisServiceIntegration.AddAgressoID(memberModel.Member.UserId, brisIntegrationApiKey);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        GMSManageMembership.UpdateBRISStatus(memberModel.Member.UserId, "Getting agresso ID", ex.Message, "ERROR", ExceConnectionManager.GetGymCode(user));
                    }
                }

                var result = GMSManageMembership.SaveMember(memberModel.Member, GetConfigurations(user), ExceConnectionManager.GetGymCode(user), user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse("-1", ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    if (memberModel.Member != null)
                    {
                        if (memberModel.Member.IntroducedById > 0)
                        {
                            try
                            {
                                USImportResult<NotificationTemplateText> notificationResult = NotificationAPI.GetNotificationTextByType(NotifyMethodType.POPUP, TextTemplateEnum.INTRODUCEDBY, user, memberModel.BranchId);
                                string textTemplate = string.Empty;
                                if (!notificationResult.ErrorOccured && notificationResult.MethodReturnValue != null)
                                {
                                    textTemplate = notificationResult.MethodReturnValue.Text;
                                }
                                List<TemplateField> templateFieldList = new List<TemplateField>();
                                templateFieldList.Add(new TemplateField { Key = TemplateFieldEnum.MEMBERNO, Value = memberModel.Member.CustId });
                                templateFieldList.Add(new TemplateField { Key = TemplateFieldEnum.MEMBERNAME, Value = memberModel.Member.FirstName + " " + memberModel.Member.LastName });
                                templateFieldList.Add(new TemplateField { Key = TemplateFieldEnum.INTRODUCEDNO, Value = memberModel.Member.IntroducedByCustId });
                                templateFieldList.Add(new TemplateField { Key = TemplateFieldEnum.INTRODUCEDNAME, Value = memberModel.Member.IntroducedByName });
                                templateFieldList.Add(new TemplateField { Key = TemplateFieldEnum.DATE, Value = DateTime.Now.ToShortDateString() });
                                templateFieldList.Add(new TemplateField { Key = TemplateFieldEnum.CREATEDUSER, Value = user });
                                string text = NotificationAPI.ConvertTemplateToText(textTemplate, templateFieldList);

                                if (!string.IsNullOrEmpty(text.Trim()))
                                {
                                    USNotificationTree noitificationTree = new USNotificationTree();
                                    noitificationTree.BranchId = memberModel.Member.BranchId;
                                    noitificationTree.MemberId = memberModel.Member.IntroducedById;
                                    USNotification notification = new USNotification();
                                    notification.Module = "Manage MemberShip";
                                    if (string.IsNullOrEmpty(memberModel.NotificationTitle.Trim()))
                                    {
                                        memberModel.NotificationTitle = "Intoduced By member";
                                    }
                                    notification.Title = memberModel.NotificationTitle;
                                    notification.Description = text;
                                    notification.CreatedDate = DateTime.Now;
                                    notification.CreatedUser = user;
                                    notification.TypeId = 3;
                                    notification.SeverityId = 3;
                                    notification.CreatedUserRoleId = "MEM";
                                    notification.DueDate = DateTime.Now.AddDays(30);
                                    notification.StatusId = 1;
                                    noitificationTree.Notification = notification;
                                    USNotificationAction action = new USNotificationAction();
                                    action.Comment = "Notification was saved automatically";
                                    action.AssgnId = user;
                                    noitificationTree.Action = action;
                                    List<USNotificationChannel> channelList = new List<USNotificationChannel>();
                                    USNotificationChannel channel = new USNotificationChannel();
                                    channel.ChannelDueDate = DateTime.Now.AddDays(30);
                                    channel.MessageType = NotifyMethodType.POPUP.ToString();
                                    channel.OccuranceType = OccuranceType.DAILY.ToString();
                                    channel.ChannelMessage = "New Message";
                                    channel.ChannelAssign = user;
                                    channel.ChannelStatus = true;
                                    channelList.Add(channel);
                                    noitificationTree.NotificationMethods = channelList;
                                    NotificationAPI.AddNotification(noitificationTree);
                                }
                            }
                            catch
                            {
                            }
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse("-1", ApiResponseStatus.ERROR, errorMsg));
            }
        }

        public bool ValidateMemberBrisId(int brisId, string user)
        {
            OperationResult<bool> result = GMSManageMembership.ValidateMemberBrisId(brisId, ExceConnectionManager.GetGymCode(user));

            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        [HttpGet]
        [Route("GetBranches")]
        [Authorize]
        public HttpResponseMessage GetBranches(int branchId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSSystemSettings.GetBranches(user, ExceConnectionManager.GetGymCode(user), branchId);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExcelineBranchDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExcelineBranchDC>(), ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpGet]
        [Route("GetGymEmployees")]
        [Authorize]
        public HttpResponseMessage GetGymEmployees(int branchId, string searchText, int roleId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSGymEmployee.GetGymEmployees(branchId, searchText, true, roleId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<GymEmployeeDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<GymEmployeeDC>(), ApiResponseStatus.ERROR, errorMsg));
            }
        }


        [HttpGet]
        [Route("GetInstructors")]
        [Authorize]
        public HttpResponseMessage GetInstructors(int branchId, string searchText, bool isActive)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var instructorId = -1;
                var result = GMSInstructor.GetInstructors(branchId, searchText, isActive, instructorId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<InstructorDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<InstructorDC>(), ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpGet]
        [Route("GetUserRole")]
        [Authorize]
        public HttpResponseMessage GetUserRole()
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSGymDetail.GetUserRole(ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExcelineRoleDc>(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExcelineRoleDc>(), ApiResponseStatus.ERROR, errorMsg));
            }
        }


        [HttpGet]
        [Route("GetSelectedGymSettings")]
        [Authorize]
        public HttpResponseMessage GetSelectedGymSettings(List<string> gymSetColNames, bool isGymSettings, int branchId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageGymSetting.GetSelectedGymSettings(gymSetColNames, isGymSettings, branchId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new Dictionary<string, object>(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new Dictionary<string, object>(), ApiResponseStatus.ERROR, errorMsg));
            }
        }



        [HttpGet]
        [Route("GetCityForPostalCode")]
        [Authorize]
        public HttpResponseMessage GetCityForPostalCode(string postalCode)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSUtility.GetCityByPostalCode(postalCode, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(string.Empty, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(string.Empty, ApiResponseStatus.ERROR, errorMsg));
            }
        }


        [HttpGet]
        [Route("GetMemberPaymentsHistory")]
        [Authorize]
        public HttpResponseMessage GetMemberPaymentsHistory(int branchId, int hit, int memberId, string paymentType)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.GetMemberPaymentsHistory(memberId, paymentType, ExceConnectionManager.GetGymCode(user), hit, user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExcePaymentInfoDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExcePaymentInfoDC>(), ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpGet]
        [Route("GetSMSNotificationTemplateByEntity")]
        [Authorize]
        public HttpResponseMessage GetSMSNotificationTemplateByEntity()
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = NotificationAPI.GetSMSNotificationTemplateByEntity("MEM", ExceConnectionManager.GetGymCode(user));

                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (var mgs in result.NotificationMessages.Where(mgs => mgs.ErrorLevel == ErrorLevels.Error))
                    {
                        USLogError.WriteToFile(mgs.Message, new Exception(), user);
                        errorMsg.Add(mgs.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExceNotificationTepmlateDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.MethodReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExceNotificationTepmlateDC>(), ApiResponseStatus.ERROR, errorMsg));
            }
        }

        private readonly static string reservedCharacters = "\"#%&,./;:<=>?¡£¤¥§ÄÅàäåÆÇÉèéìÑñòöØÖÜùüß";

        public static string UrlEncode(string value)
        {
            if (String.IsNullOrEmpty(value))
                return String.Empty;

            var sb = new StringBuilder();

            foreach (char @char in value)
            {   
                if (@char == '\n')
                {
                    sb.Append("%0A");
                }
                else if (@char == '\r')
                {
                    sb.Append("%0A");
                }
                else if (char.IsWhiteSpace(@char)) {
                    sb.Append("%20");
                }
                else if (reservedCharacters.IndexOf(@char) == -1)
                {
                    sb.Append(@char);
                }
                else
                {
                    sb.AppendFormat("%{0:X2}", (int)@char);
                }
            }
            
            return sb.ToString();
        }

        [HttpPost]
        [Route("SendSMSFromMemberCard")]
        [Authorize]
        public HttpResponseMessage SendSMSFromMemberCard(MemberSMS memberSMS)
        {
            try
            {
                var message = UrlEncode(memberSMS.message);
                List<string> errorMsg = new List<string>();
                string user = Request.Headers.GetValues("UserName").First();
                var result = NotificationAPI.AddSMSNotification("MEMBER CARD SMS", message, memberSMS.mobileNo, "MEM", memberSMS.memberID, memberSMS.memberID, memberSMS.branchId, user, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    foreach (var mgs in result.NotificationMessages.Where(mgs => mgs.ErrorLevel == ErrorLevels.Error))
                    {
                        USLogError.WriteToFile(mgs.Message, new Exception(), user);
                        errorMsg.Add(mgs.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {

                    var application = ConfigurationManager.AppSettings["USC_Notification_SMS"].ToString();
                    const string outputFormat = "SMS";

                    // send SMS through the USC API
                    var smsStatus = USCAPI.ExecuteTemplate(application, outputFormat, ExceConnectionManager.GetGymCode(user), result.MethodReturnValue.ToString());
                    var status = NotificationStatusEnum.Attended;
                    if (smsStatus != null)
                        status = (smsStatus.Messages.FirstOrDefault(X => X.Header == "StatusCode").Message.ToString() == "1") ? NotificationStatusEnum.Success : NotificationStatusEnum.Fail;
                    var statusMessage = smsStatus.Messages.FirstOrDefault(x => x.Header == "StatusMessage").Message;

                    // update status in notification table
                    var updateResult = NotificationAPI.UpdateNotificationStatus(result.MethodReturnValue, user, status, statusMessage);

                    if (updateResult.ErrorOccured)
                    {
                        foreach (var mgs in updateResult.NotificationMessages.Where(mgs => mgs.ErrorLevel == ErrorLevels.Error))
                        {
                            USLogError.WriteToFile(mgs.Message, new Exception(), user);
                            errorMsg.Add(mgs.Message);
                        }
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(true, ApiResponseStatus.OK));
                    }
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
            }
        }


        [HttpPost]
        [Route("SendEmailFromMemberCard")]
        [Authorize]
        public HttpResponseMessage SendEmailFromMemberCard(MemberEmail memberEmail)
        {
            try
            {
                List<string> errorMsg = new List<string>();
                string user = Request.Headers.GetValues("UserName").First();

                // create common notification
                USCommonNotificationDC commonNotification = new USCommonNotificationDC();
                commonNotification.IsTemplateNotification = false;
                commonNotification.Description = memberEmail.message;
                commonNotification.BranchID = memberEmail.branchId;
                commonNotification.CreatedUser = user;
                commonNotification.Role = "MEM";
                commonNotification.NotificationType = NotificationTypeEnum.Messages;
                commonNotification.Severity = NotificationSeverityEnum.Minor;
                commonNotification.MemberID = memberEmail.memberID;
                commonNotification.Status = NotificationStatusEnum.New;
                commonNotification.Method = NotificationMethodType.EMAIL;
                commonNotification.Title = memberEmail.subject;
                commonNotification.SenderDescription = memberEmail.email;

                USImportResult<int> result = NotificationAPI.AddNotification(commonNotification);

                if(result.ErrorOccured)
                {
                    foreach(USNotificationMessage mgs in result.NotificationMessages)
                    {
                        if(mgs.ErrorLevel == ErrorLevels.Error)
                        {
                            USLogError.WriteToFile(mgs.Message, new Exception(), user);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    string application = ConfigurationManager.AppSettings["USC_Notification_Email"].ToString();
                    const string outputFormat = "EMAIL";

                    // send Email through the USC API
                    IProcessingStatus emailStatus = USCAPI.ExecuteTemplate(application, outputFormat, ExceConnectionManager.GetGymCode(user), result.MethodReturnValue.ToString());
                    NotificationStatusEnum status = NotificationStatusEnum.Attended;

                    string statusMessage = string.Empty;
                    if(emailStatus != null)
                    {
                        status = (emailStatus.Messages.FirstOrDefault(X => X.Header == "Status").Message.ToString() == "1") ? NotificationStatusEnum.Success : NotificationStatusEnum.Fail;
                        statusMessage = emailStatus.Messages.FirstOrDefault(x => x.Header == "StatusMessage").Message;
                    }

                    // update status in notification table
                    USImportResult<bool> updateResult = NotificationAPI.UpdateNotificationStatus(result.MethodReturnValue, user, status, statusMessage);

                    if(updateResult.ErrorOccured)
                    {

                        foreach(USNotificationMessage mgs in updateResult.NotificationMessages)
                        {
                            if(mgs.ErrorLevel == ErrorLevels.Error)
                            {

                                USLogError.WriteToFile(mgs.Message, new Exception(), user);
                            }
                        }
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(true, ApiResponseStatus.OK));
                    }
                }
            }
            catch(Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
            }
        }


        [NonAction]
        private string GetConfigurations(string user)
        {
            var imageSavepath = string.Empty;
            try
            {
                imageSavepath = ConfigurationSettings.AppSettings["ImageFolderPath"].ToString();
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile("Error in getting Image folder path " + ex.Message, new Exception(), user);
            }
            return imageSavepath;
        }



        //booking

        [HttpGet]
        [Route("GetResources")]
        [Authorize]
        public HttpResponseMessage GetResources(int branchId, string searchText, int categoryId, int activityId, bool isActive)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSResources.GetResources(branchId, searchText, -1, activityId, 2, true, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(string.Empty, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(string.Empty, ApiResponseStatus.ERROR, errorMsg));
            }
        }




        [HttpGet]
        [Route("GetMemberBooking")]
        [Authorize]
        public HttpResponseMessage GetMemberBooking(int memberId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.GetMemberBookings(memberId, ExceConnectionManager.GetGymCode(user), user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(string.Empty, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(string.Empty, ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpGet]
        [Route("GetCategories")]
        [Authorize]
        public HttpResponseMessage GetCategories(string type, int branchId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSCategory.GetCategoriesByType(type, user, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(string.Empty, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(string.Empty, ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpGet]
        [Route("GetOtherMembersForBooking")]
        [Authorize]
        public HttpResponseMessage GetOtherMembersForBooking(int scheduleId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.GetOtherMembersForBooking(scheduleId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(string.Empty, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(string.Empty, ApiResponseStatus.ERROR, errorMsg));
            }
        }


        private void SetPDFFileParth(int id, string fileParth, string flag, int branchId, string user)
        {
            GMSManageMembership.SetPDFFileParth(id, fileParth, flag, branchId, ExceConnectionManager.GetGymCode(user), user);
        }


        [HttpGet]
        [Route("PrintMemberBooking")]
        [Authorize]
        public HttpResponseMessage PrintMemberBooking(int memberId, int branchId, string fromDate, string toDate)
        {
            PDFPrintResultDC result = new PDFPrintResultDC();
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var appUrl = ConfigurationManager.AppSettings["USC_MemberBooking_Print_App_Path"];
                var dataDictionary = new Dictionary<string, string>() { { "MemberID", memberId.ToString() }, { "fromDate", fromDate}, { "toDate", toDate }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                result.FileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);
                SetPDFFileParth(memberId, result.FileParth, "MBO", branchId, user);

                // OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                var isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                if (isVisibel.ErrorOccured)
                {
                    foreach (NotificationMessage message in isVisibel.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                    return null;
                }
                result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new PDFPrintResultDC(), ApiResponseStatus.ERROR, errorMsg));
            }
        }

        private List<IProcessingStatus> ExcecuteWithUSC(string application, string outputPlugging, Dictionary<string, string> dataDictionary, string gymCode, string user)
        {
            List<IProcessingStatus> lists = null;
            try
            {
                lists = TemplateManager.ExecuteTemplate(dataDictionary, application, outputPlugging, gymCode);
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile(ex.Message, new Exception(), user);
                throw ex;
            }
            return lists;
        }

        private string GetFilePathForPdf(string application, string outputPlugging, string user, Dictionary<string, string> dataDictionary)
        {
            var filePath = string.Empty;
            string pdfViewerURL = ConfigurationManager.AppSettings["PDFViewerURL"];
            try
            {
                var list = ExcecuteWithUSC(application, outputPlugging, dataDictionary, ExceConnectionManager.GetGymCode(user), user);
                foreach (var item in list)
                {
                    if (item == null) continue;
                    var message = item.Messages.First(mssge => mssge.Header.ToLower().Equals("filepath"));
                    if (message != null)
                    {
                        //manipulate to get the PDF path
                        filePath = message.Message;
                        int indexOFRoot = filePath.IndexOf("PDF");
                        string restOfPath = filePath.Substring(indexOFRoot +  4, filePath.Length - (indexOFRoot + 4));
                        string[] pathItems = restOfPath.Split(new char[] {'\\'});
                        string path = string.Empty;
                        
                        if(pathItems.Length > 0)
                        {
                            pathItems.ToList<string>().ForEach(X => path += "/" + X);
                        }
                        filePath = pdfViewerURL + path;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile(ex.Message, new Exception(), user);
            }
            return filePath;
        }


        public List<MemberBookingDC> GetMemberBooking(int memberId, string user)
        {
            OperationResult<List<MemberBookingDC>> result = GMSManageMembership.GetMemberBookings(memberId, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<MemberBookingDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<CategoryDC> GetCategories(string type, string user, int branchId)
        {
            var result = GMSCategory.GetCategoriesByType(type, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<CategoryDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ExcelineMemberDC> GetOtherMembersForBooking(int scheduleId, string user)
        {
            OperationResult<List<ExcelineMemberDC>> result = GMSManageMembership.GetOtherMembersForBooking(scheduleId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExcelineMemberDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        [HttpGet]
        [Route("GetClassByMemberId")]
        [Authorize]
        public HttpResponseMessage GetClassByMemberId(int branchId, int memberId, DateTime? classDate)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.GetClassByMemberId(branchId, memberId, classDate, user, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(string.Empty, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(string.Empty, ApiResponseStatus.ERROR, errorMsg));
            }
        }
        [HttpPost]
        [Route("GetSelectedGymSettings")]
        [Authorize]
        public HttpResponseMessage GetSelectedGymSettings(SelectedGymSettingsParameters parameters)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                OperationResult<Dictionary<string, object>> result = GMSManageGymSetting.GetSelectedGymSettings(parameters.settingNames, parameters.isGymSetting, parameters.branchId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpGet]
        [Route("PrintClassListByMember")]
        [Authorize]
        public HttpResponseMessage PrintClassListByMember(int branchId, int memberId, string fromDate, string toDate)
        {
            PDFPrintResultDC result = new PDFPrintResultDC();
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var appUrl = ConfigurationManager.AppSettings["USC_MemberClass_Print_App_Path"];
                var dataDictionary = new Dictionary<string, string>() { { "MemberID", memberId.ToString() }, { "fromDate", fromDate }, { "toDate", toDate }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                result.FileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);
                var isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                if (isVisibel.ErrorOccured)
                {
                    foreach (NotificationMessage message in isVisibel.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                    return null;
                }
                result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpPost]
        [Route("CancelPayment")]
        [Authorize]
        public HttpResponseMessage CancelPayment(DeletePayment deletePayment)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSPayment.CancelPayment(deletePayment.PaymentId, deletePayment.KeepPayment, user, deletePayment.Comment, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpPost]
        [Route("UpdateUserRoutine")]
        [Authorize]
        public HttpResponseMessage UpdateSettingForUserRoutine(PaymentUserRoutine userRoutine)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSUser.UpdateSettingForUserRoutine(userRoutine.Key, userRoutine.Value, user, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpGet]
        [Route("GetClaimExportStatusForMember")]
        [Authorize]
        public HttpResponseMessage GetClaimExportStatusForMember(int memberID, int hit, string type, DateTime? transferDate)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSCcx.GetClaimExportStatusForMember(ExceConnectionManager.GetGymCode(user), memberID, hit, transferDate, type, user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(string.Empty, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(string.Empty, ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpGet]
        [Route("GetIntroducedMembers")]
        [Authorize]
        public HttpResponseMessage GetIntroducedMembers(int memberId, int branchId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.GetIntroducedMembers(memberId, branchId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExcelineMemberDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExcelineMemberDC>(), ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpGet]
        [Route("GetNotificationByNotifiyMethod")]
        [Authorize]
        public HttpResponseMessage GetNotificationByNotifiyMethod(int memberId, int branchId, DateTime fromDate, DateTime toDate, NotifyMethodType notifyMethod)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = NotificationAPI.GetNotificationByNotifiyMethod(memberId, user, branchId, fromDate, toDate, notifyMethod);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (USNotificationMessage message in result.NotificationMessages)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(string.Empty, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.MethodReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(string.Empty, ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpPost]
        [Route("UpdateIntroducedMembers")]
        [Authorize]
        public HttpResponseMessage UpdateIntroducedMembers(UpdateIntroducedMembers updateIntroducedMembers)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.UpdateIntroducedMembers(updateIntroducedMembers.memberId, updateIntroducedMembers.creditedDate, updateIntroducedMembers.creditedText, updateIntroducedMembers.isDelete, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpPost]
        [Route("UpdateNotificationStatus")]
        [Authorize]
        public HttpResponseMessage UpdateNotificationStatus(UpdateNotificationStatus updateNotificationStatus)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = NotificationAPI.UpdateNotificationStatus(updateNotificationStatus.selectedIds, updateNotificationStatus.status, user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (USNotificationMessage message in result.NotificationMessages)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.MethodReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpPost]
        [Route("AddCountryDetails")]
        [Authorize]
        public HttpResponseMessage AddCountryDetails(CountryDC country)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSMember.AddCountryDetails(country, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpGet]
        [Route("GetSessionValue")]
       
        public HttpResponseMessage GetSessionValue(string sessionKey)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = SessionManager.Session.GetSession(sessionKey, ExceConnectionManager.GetGymCode(user));
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(string.Empty, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpGet]
        [Route("GetEmployeeCategoryBySponsorId")]
        [Authorize]
        public HttpResponseMessage GetEmployeeCategoryBySponsorId(int branchId, int sponsorId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.GetEmployeeCategoryBySponsorId(branchId, ExceConnectionManager.GetGymCode(user), sponsorId);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new OrdinaryMemberDC(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new OrdinaryMemberDC(), ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpGet]
        [Route("GetInterestCategoryByMember")]
        [Authorize]
        public HttpResponseMessage GetInterestCategoryByMember(int memberId, int branchId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.GetInterestCategoryByMember(memberId, branchId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpPost]
        [Route("SaveInterestCategoryByMember")]
        [Authorize]
        public HttpResponseMessage SaveInterestCategoryByMember(SaveInterestCategoryByMember saveInterestCategoryByMember)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.SaveInterestCategoryByMember(saveInterestCategoryByMember.memberId, saveInterestCategoryByMember.branchId, saveInterestCategoryByMember.interestCategoryList, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpGet]
        [Route("GetCategoryTypes")]
        [Authorize]
        public HttpResponseMessage GetCategoryTypes(string name, int branchId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSCategory.GetCategoryTypes(name, user, branchId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<CategoryTypeDC>(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<CategoryTypeDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpPost]
        [Route("SaveCategory")]
        [Authorize]
        public HttpResponseMessage SaveCategory(CategoryDC category)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSCategory.SaveCategory(category, user, category.BranchId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();

                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                if (result.OperationReturnValue == -2)
                {
                    List<string> waMsg = new List<string>();
                    waMsg.Add("duplicate");
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.WARNING, waMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpGet]
        [Route("GetContractSummaries")]
        public HttpResponseMessage GetContractSummaries(int memberId, int branchId, bool isFreezDetailNeeded, bool isFreezdView)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.GetContractSummaries(memberId, branchId, ExceConnectionManager.GetGymCode(user), isFreezDetailNeeded, user, isFreezdView);
                if (result.ErrorOccured)
                {
                    List<string> exceptionMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        exceptionMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<PackageDC>(), ApiResponseStatus.ERROR, exceptionMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<PackageDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }
        [HttpGet]
        [Route("GetMemberVisits")]
        public HttpResponseMessage GetMemberVisits(int memberId, DateTime selectedDate, int branchId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.GetMemberVisits(memberId, selectedDate, branchId, ExceConnectionManager.GetGymCode(user), user);
                if (result.ErrorOccured)
                {
                    List<string> exceptionMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        exceptionMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<PackageDC>(), ApiResponseStatus.ERROR, exceptionMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<PackageDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpGet]
        [Route("GetVisitCount")]
        public HttpResponseMessage GetVisitCount(int memberId, DateTime startDate, DateTime endDate)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.GetVisitCount(memberId, startDate, endDate, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> exceptionMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        exceptionMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<PackageDC>(), ApiResponseStatus.ERROR, exceptionMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<PackageDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpGet]
        [Route("PrintVisitsListByMember")]
        [Authorize]
        public HttpResponseMessage PrintVisitsListByMember(int branchId, int memberId, string fromDate, string toDate)
        {
            PDFPrintResultDC result = new PDFPrintResultDC();
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var appUrl = ConfigurationManager.AppSettings["USC_MemberVisits_Print_App_Path"];
                var dataDictionary = new Dictionary<string, string>() { { "MemberID", memberId.ToString() }, { "fromDate", fromDate }, { "toDate", toDate }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                result.FileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);
                SetPDFFileParth(memberId, result.FileParth, "MBO", branchId, user);

                // OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                var isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                if (isVisibel.ErrorOccured)
                {
                    foreach (NotificationMessage message in isVisibel.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                    return null;
                }
                result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new PDFPrintResultDC() , ApiResponseStatus.ERROR, errorMsg));
            }
        }
        [HttpPost]
        [Route("DeleteMemberVisit")]
        [Authorize]
        public HttpResponseMessage DeleteMemberVisit(DeleteVisit deleteVisit)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.DeleteMemberVisit(deleteVisit.memberVisitId, deleteVisit.memberContractId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpPost]
        [Route("SaveMemberVisit")]
        [Authorize]
        public HttpResponseMessage SaveMemberVisit(EntityVisitDC memberVisit)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.SaveMemberVisit(memberVisit, ExceConnectionManager.GetGymCode(user), user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
            }
        }



        [HttpPost]
        [Route("SaveDocumentData")]
        [Authorize]
        public HttpResponseMessage SaveDocumentData(SaveDocument document)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();

                var result = GMSManageMembership.SaveDocumentData(document.Document, ExceConnectionManager.GetGymCode(user), document.BranchId);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpPost]
        [Route("DeleteDocumentData")]
        [Authorize]
        public HttpResponseMessage DeleteDocumentData(SaveDocument document)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.DeleteDocumentData(document.DocId, ExceConnectionManager.GetGymCode(user), document.BranchId);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpGet]
        [Route("GetDocumentData")]
        public HttpResponseMessage GetDocumentData(bool isActive, string custId, string searchText, int documentType, int branchId)
        {
            try
            {
                if (searchText == null || searchText == "")
                {
                    searchText = "";
                }
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.GetDocumentData(isActive, custId, searchText, documentType, ExceConnectionManager.GetGymCode(user), branchId);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
            }
        }


        public List<DocumentDC> GetDocumentData(bool isActive, string custId, string searchText, int documentType, string username, int branchId)
        {
            OperationResult<List<DocumentDC>> result = GMSManageMembership.GetDocumentData(isActive, custId, searchText, documentType, ExceConnectionManager.GetGymCode(username), branchId);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), username);
                }
                return new List<DocumentDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        [HttpGet]
        [Route("ValidateEmail")]
        [Authorize]
        public HttpResponseMessage ValidateEmail(string email)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSSystem.ValidateEmail(email, ExceConnectionManager.GetGymCode(user));

                if (result.ErrorOccured)
                {
                    List<string> exceptionMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        exceptionMsg.Add(message.Message);
                    }


                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));

                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);

                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<PackageDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpPost]
        [Route("AddDocument")]
        public HttpResponseMessage AddDocument()
        {
            HttpResponseMessage response = new HttpResponseMessage();
            var abc = Request.Properties.Values;
            var httpRequest = HttpContext.Current.Request;
            var fileCount = httpRequest.Files.Count;
            if (httpRequest.Files.Count > 0)
            {
                for (int i = 0; i < fileCount; i++)
                {
                    var postedFile = httpRequest.Files[i];
                    var filePath = HttpContext.Current.Server.MapPath("~/UploadFile/" + postedFile.FileName);
                    postedFile.SaveAs(filePath);
                }
            }
            return response;
        }

        [HttpPost]
        [Route("AddFile")]
        public HttpResponseMessage addFile(HttpPostedFileBase CourseFile)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            var abc = Request.Properties.Values;
            var httpRequest = HttpContext.Current.Request;
            var fileCount = httpRequest.Files.Count;
            if (httpRequest.Files.Count > 0)
            {
                for (int i = 0; i < fileCount; i++)
                {
                    var postedFile = httpRequest.Files[i];
                    var filePath = HttpContext.Current.Server.MapPath("~/UploadFile/" + postedFile.FileName);
                    postedFile.SaveAs(filePath);
                }
            }
            return response;
        }

        [HttpGet]
        [Route("GetMemberIntegrationSettings")]
        [Authorize]
        public HttpResponseMessage GetMemberIntegrationSettings(int branchId, int memberId)
        //{
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.GetMemberIntegrationSettings(branchId, memberId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<CategoryDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<CategoryDC>(), ApiResponseStatus.ERROR, errorMsg));
            }
        }


        [HttpGet]
        [Route("GetExorLiveClientAppUrl")]
        [Authorize]
        public HttpResponseMessage GetExorLiveClientAppUrl()
        //{
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = ConfigurationSettings.AppSettings["ExorLiveClientAppUrl"].ToString(CultureInfo.InvariantCulture);


                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<CategoryDC>(), ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpGet]
        [Route("GetEncryptKey")]
        [Authorize]
        public HttpResponseMessage GetEncryptKey(string custId)
        //{
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = new Dictionary<string, string> { { "gymCode", Encrypt(ExceConnectionManager.GetGymCode(user)) }, { "custId", Encrypt(custId) } };


                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<CategoryDC>(), ApiResponseStatus.ERROR, errorMsg));
            }
        }

        private string Encrypt(string text)
        {
            const string encryptionKey = "MAKV2SPBNI99212";
            byte[] clearBytes = Encoding.Unicode.GetBytes(text);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(encryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    text = Convert.ToBase64String(ms.ToArray());
                }
            }
            return text;
        }


        [HttpPost]
        [Route("UploadFileApi")]
        public HttpResponseMessage UploadFileApi()
        {
            try
            {
                HttpResponseMessage response = new HttpResponseMessage();
                var httpRequest = HttpContext.Current.Request;

                if (httpRequest.Files.Count > 0)
                {
                    var filePath = "";
                    foreach (string file in httpRequest.Files)
                    {
                        string rootpath = ConfigurationSettings.AppSettings["UploadScanDocRoot"];
                        string BranchId = httpRequest.Headers["BranchId"].ToString();
                        string CustId = httpRequest.Headers["CustId"].ToString();
                        string UserName = httpRequest.Headers["UserName"].ToString();
                        string gymCode = ExceConnectionManager.GetGymCode(UserName).ToString();
                        string fileFolder = rootpath + @"\" + gymCode + @"\" + BranchId + @"\" + CustId + @"\";
                        if (!Directory.Exists(fileFolder))
                        {
                            Directory.CreateDirectory(fileFolder);
                        }
                        var postedFile = httpRequest.Files[file];
                        filePath = fileFolder + postedFile.FileName;
                        postedFile.SaveAs(filePath);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(filePath, ApiResponseStatus.OK));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
            }


        }


        [HttpPost]
        [Route("AddSessionValue")]
        [Authorize]
        public HttpResponseMessage AddSessionValue(Models.Session session)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();

                var result = SessionManager.Session.AddSessionValue(session.SessionKey, session.Value, ExceConnectionManager.GetGymCode(user));

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
            }
        }


        [HttpPost]
        [Route("GetSessionValue")]
        [Authorize]
        public HttpResponseMessage GetSessionValue(Models.Session session)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();

                var result = SessionManager.Session.GetSession(session.SessionKey, ExceConnectionManager.GetGymCode(user));

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
            }
        }




        [HttpPost]
        [Route("DeleteInstallment")]
        [Authorize]
        public HttpResponseMessage DeleteInstallment(DeleteInstallment deleteInstallment)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.DeleteInstallment(deleteInstallment.installmentIdList, deleteInstallment.memberContractId, ExceConnectionManager.GetGymCode(user), user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
            }
        }






        [HttpGet]
        [Route("ValidateMemberCard")]
        public HttpResponseMessage ValidateMemberCard(string memberCard, int memberId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSSystem.ValidateMemberCard(memberCard, memberId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> exceptionMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        exceptionMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpGet]
        [Route("ValidateGatCardNo")]
        public HttpResponseMessage ValidateGatCardNo(string gatCardNo)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSSystem.ValidateGatCardNo(gatCardNo, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> exceptionMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        exceptionMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpGet]
        [Route("ValidateMobile")]
        public HttpResponseMessage ValidateMobile(string mobile, string mobilePrefix)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSSystem.ValidateMobile(mobile, mobilePrefix, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> exceptionMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        exceptionMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(string.Empty, ApiResponseStatus.ERROR, exceptionMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(string.Empty, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpGet]
        [Route("GetFollowUpTemplates")]
        [Authorize]
        public HttpResponseMessage GetFollowUpTemplates(int branchId)
        {

            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSSystemSettings.GetFollowupTemplates(ExceConnectionManager.GetGymCode(user), branchId);
                if (result.ErrorOccured)
                {
                    List<string> exceptionMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        exceptionMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<FollowUpTemplateDC>(), ApiResponseStatus.ERROR, exceptionMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<FollowUpTemplateDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpGet]
        [Route("GetFollowUpTaskByTemplateIds")]
        [Authorize]
        public HttpResponseMessage GetFollowUpTaskByTemplateId(int followUpTemplateId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSSystemSettings.GetFollowUpTaskByTemplateId(followUpTemplateId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> exceptionMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        exceptionMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<FollowUpTemplateTaskDC>(), ApiResponseStatus.ERROR, exceptionMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<FollowUpTemplateTaskDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpGet]
        [Route("GetEmpRole")]
        [Authorize]
        public HttpResponseMessage GetEmpRole()
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = AdminUserSettings.GetUSPRoles("", true, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> exceptionMsg = new List<string>();
                    foreach (USNotificationMessage message in result.NotificationMessages)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        exceptionMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<USPRole>(), ApiResponseStatus.ERROR, exceptionMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.MethodReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<FollowUpTemplateTaskDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpPost]
        [Route("SaveFollowUp")]
        [Authorize]
        public HttpResponseMessage SaveFollowUp(List<FollowUpDC> followUpList)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.SaveFollowUp(followUpList, ExceConnectionManager.GetGymCode(user), user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
            }
        }


        [HttpGet]
        [Route("GetMemberOrders")]
        [Authorize]
        public HttpResponseMessage GetMemberOrders(int memberId, string type)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.GetMemberOrders(memberId, type, ExceConnectionManager.GetGymCode(user), user);
                if (result.ErrorOccured)
                {
                    List<string> exceptionMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        exceptionMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<InstallmentDC>(), ApiResponseStatus.ERROR, exceptionMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<InstallmentDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpPost]
        [Route("WellnessIntegrationOperation")]
        [Authorize]
        public HttpResponseMessage WellnessIntegrationOperation(WellnessIntegration wellnessIntegration)
        //{
        {
            try
            {

                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.WellnessIntegrationOperation(wellnessIntegration.Member, wellnessIntegration.WellnessIntegrationSetting, wellnessIntegration.WellnessOperationType, ExceConnectionManager.GetGymCode(user), wellnessIntegration.BranchId, user);
                if (result.ErrorOccured)
                {
                    List<string> exceptionMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        exceptionMsg.Add(message.Message);
                    }


                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));

                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }


            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
            }
        }



        [HttpPost]
        [Route("InfoGymRegisterMember")]
        [Authorize]
        public HttpResponseMessage InfoGymRegisterMember(InfoGym infoGym)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var host = ConfigurationManager.AppSettings["InfoGym_ApiEndpoint"];
                var org = ConfigurationManager.AppSettings["InfoGym_Org"];
                var url = string.Format("https://{0}/orgs/{1}/members", host, org);
                var scope = string.Format("orgs:{0}:members:write", org);

                var accessToken = InfoGymGetAccessToken(scope, user);
                var infoId = GMSManageMembership.InfoGymRegisterMember(infoGym.Member, url, host, accessToken);

                GMSManageMembership.InfoGymUpdateExceMember(infoId, infoGym.MemberID, user);

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(infoId, ApiResponseStatus.OK));



            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
            }
        }

        public string InfoGymGetAccessToken(string scope, string user)
        {
            try
            {
                var clientId = ConfigurationManager.AppSettings["InfoGym_ClientId"];
                var clientSecret = ConfigurationManager.AppSettings["InfoGym_ClientSecret"];
                var host = ConfigurationManager.AppSettings["InfoGym_TokenEndpoint"];

                var grantType = ConfigurationManager.AppSettings["InfoGym_GrantType"];
                var org = ConfigurationManager.AppSettings["InfoGym_Org"];
                var data = string.Format("grant_type={0}&org={1}&scope={2}", grantType, org, scope);

                return GMSManageMembership.InfoGymGetAccessToken(clientId, clientSecret, host, data);
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile(ex.Message, ex, user);
                return string.Empty;
            }
        }


        [HttpPost]
        [Route("InfoGymUpdateMember")]
        [Authorize]
        public HttpResponseMessage InfoGymUpdateMember(InfoGym infoGym)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var host = ConfigurationManager.AppSettings["InfoGym_ApiEndpoint"];
                var org = ConfigurationManager.AppSettings["InfoGym_Org"];
                var url = string.Format("https://{0}/orgs/{1}/members/{2}", host, org, infoGym.InfoGymId);
                var scope = string.Format("orgs:{0}:members:write", org);

                var accessToken = InfoGymGetAccessToken(scope, user);

                var infoId = GMSManageMembership.InfoGymUpdateMember(infoGym.Member, url, host, accessToken);

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(infoId, ApiResponseStatus.OK));



            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
            }
        }


        [HttpPost]
        [Route("GenerateInvoice")]
        [Authorize]
        public HttpResponseMessage GenerateInvoice(GenerateInvoice generateInvoice)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                PDFPrintResultDC printResult = new PDFPrintResultDC();
                var result = GMSPayment.GenerateInvoice(generateInvoice.invoiceBranchId, user, generateInvoice.installment, generateInvoice.paymentDetail, generateInvoice.invoiceType, ExceConnectionManager.GetGymCode(user), generateInvoice.memberBranchID);

                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                    result.OperationReturnValue.AritemNo = -1;
                    result.OperationReturnValue.SaleStatus = SaleErrors.ERROR;
                    result.OperationReturnValue.PrintResult = printResult;

                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    if (result.OperationReturnValue.AritemNo > 0)
                    {
                        if (generateInvoice.invoiceType == "SMS" || generateInvoice.invoiceType == "EmailSMS")
                        {
                            try
                            {
                                #region Save Notification
                                USImportResult<NotificationTemplateText> notificationResult = NotificationAPI.GetNotificationTextByType(NotifyMethodType.SMS, TextTemplateEnum.SMSINVOICE, user, generateInvoice.invoiceBranchId);
                                string textTemplate = string.Empty;
                                NotificationTemplateText notificationTemplateText = new NotificationTemplateText();
                                if (!notificationResult.ErrorOccured && notificationResult.MethodReturnValue != null)
                                {
                                    notificationTemplateText = notificationResult.MethodReturnValue;
                                    textTemplate = notificationTemplateText.Text;
                                }
                                ExcelineInvoiceDetailDC invoiceDetails = new ExcelineInvoiceDetailDC();
                                var invoiceDetailsVar = GMSInvoice.GetInvoiceDetails(result.OperationReturnValue.AritemNo, generateInvoice.invoiceBranchId, ExceConnectionManager.GetGymCode(user));
                                if (!invoiceDetailsVar.ErrorOccured && invoiceDetailsVar != null)
                                {
                                    invoiceDetails = invoiceDetailsVar.OperationReturnValue;
                                }
                                List<TemplateField> templateFieldList = new List<TemplateField>();
                                try
                                {
                                    templateFieldList.Add(new TemplateField { Key = TemplateFieldEnum.AMOUNT, Value = invoiceDetails.InvoiceAmount.ToString() });
                                    templateFieldList.Add(new TemplateField { Key = TemplateFieldEnum.KID, Value = invoiceDetails.BasicDetails.KID });
                                    templateFieldList.Add(new TemplateField { Key = TemplateFieldEnum.DUEDATE, Value = invoiceDetails.BasicDetails.DueDate.ToString() });
                                    templateFieldList.Add(new TemplateField { Key = TemplateFieldEnum.INVOICENO, Value = invoiceDetails.InvoiceNo });
                                    templateFieldList.Add(new TemplateField { Key = TemplateFieldEnum.GYMACCOUNT, Value = invoiceDetails.OtherDetails.BranchAccountNo });
                                    templateFieldList.Add(new TemplateField { Key = TemplateFieldEnum.DATE, Value = DateTime.Now.ToShortDateString() });
                                    templateFieldList.Add(new TemplateField { Key = TemplateFieldEnum.CREATEDUSER, Value = user });
                                    templateFieldList.Add(new TemplateField { Key = TemplateFieldEnum.MEMBERNAME, Value = invoiceDetails.CustomerName });
                                }
                                catch
                                {
                                }
                                string text = NotificationAPI.ConvertTemplateToText(textTemplate, templateFieldList);
                                if (!string.IsNullOrEmpty(text))
                                {
                                    USNotificationTree noitificationTree = new USNotificationTree();
                                    noitificationTree.BranchId = generateInvoice.invoiceBranchId;
                                    noitificationTree.MemberId = generateInvoice.installment.MemberId;
                                    USNotification notification = new USNotification();
                                    notification.Module = "Manage MemberShip";
                                    notification.Title = generateInvoice.notificationTitle;
                                    notification.Description = text;
                                    notification.CreatedDate = DateTime.Now;
                                    notification.CreatedUser = user;
                                    notification.TypeId = 3;
                                    notification.SeverityId = 3;
                                    notification.CreatedUserRoleId = "MEM";
                                    notification.DueDate = DateTime.Now.AddDays(30);
                                    notification.StatusId = 1;
                                    noitificationTree.Notification = notification;
                                    USNotificationAction action = new USNotificationAction();
                                    action.Comment = "Notification was saved automatically";
                                    action.AssgnId = user;
                                    noitificationTree.Action = action;
                                    List<USNotificationChannel> channelList = new List<USNotificationChannel>();
                                    USNotificationChannel channel = new USNotificationChannel();
                                    channel.ChannelDueDate = DateTime.Now.AddDays(30);
                                    channel.MessageType = NotifyMethodType.SMS.ToString();
                                    channel.OccuranceType = OccuranceType.ONCE.ToString();
                                    channel.ChannelMessage = "New Message";
                                    channel.ChannelAssign = user;
                                    channel.ChannelStatus = true;
                                    channelList.Add(channel);
                                    noitificationTree.NotificationMethods = channelList;
                                    NotificationAPI.AddNotification(noitificationTree);
                                }
                                #endregion
                            }
                            catch
                            {
                            }
                        }

                        if (generateInvoice.invoiceType == "SMS")
                        {

                            SendSMSInvoice(generateInvoice.invoiceBranchId, user, result.OperationReturnValue.AritemNo);
                            printResult.FileParth = "done";
                            result.OperationReturnValue.PrintResult = printResult;
                        }
                        else if (generateInvoice.invoiceType == "EmailSMS")
                        {
                            SendSMSInvoice(generateInvoice.invoiceBranchId, user, result.OperationReturnValue.AritemNo);
                            printResult.FileParth = "done";
                            result.OperationReturnValue.PrintResult = printResult;
                        }
                        else if (generateInvoice.invoiceType == "PRINT")
                        {
                            printResult = ViewInvoiceDetails(generateInvoice.invoiceBranchId, user, result.OperationReturnValue.AritemNo);
                            if (string.IsNullOrEmpty(printResult.FileParth))
                                printResult.FileParth = "INV";
                            result.OperationReturnValue.PrintResult = printResult;
                        }
                    }
                    else
                    {
                        result.OperationReturnValue.AritemNo = -1;
                        result.OperationReturnValue.SaleStatus = SaleErrors.ERROR;
                        result.OperationReturnValue.PrintResult = printResult;
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
            }
        }


        [NonAction]
        public int SendSMSInvoice(int brnachId, string user, int arItemNo)
        {
            var appUrl = ConfigurationManager.AppSettings["USC_Invoice_Print_App_Path"];
            return SendSMS(appUrl, "SMS", arItemNo.ToString(), user);
        }

        [NonAction]
        private int SendSMS(string application, string outputPlugging, string dataId, string user)
        {
            int result = 0;
            try
            {
                var dataDictionary = new Dictionary<string, string>() { { "itemID", dataId } };
                var list = ExcecuteWithUSC(application, outputPlugging, dataDictionary, ExceConnectionManager.GetGymCode(user), user);
                if (list[0].Messages[2] != null)
                    result = Convert.ToInt32(list[0].Messages[2].Message);
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile(ex.Message, new Exception(), user);
            }
            return result;
        }

        [NonAction]
        public PDFPrintResultDC ViewInvoiceDetails(int branchId, string user, int arItemNo)
        {
            PDFPrintResultDC result = new PDFPrintResultDC();
            try
            {
                var uggabugga = IsBrisIntegration();
                if (true)
                {
                    var appUrl = ConfigurationManager.AppSettings["USC_Invoice_Print_App_Path"];
                    var dataDictionary = new Dictionary<string, string>() { { "itemID", arItemNo.ToString() }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                    result.FileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);
                    SetPDFFileParth(arItemNo, result.FileParth, "INV", branchId, user);

                    OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                    if (isVisibel.ErrorOccured)
                    {
                        foreach (NotificationMessage message in isVisibel.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), user);
                        }
                        return null;
                    }
                    result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
                }
                else
                {
                    string invoicePath = string.Empty;
                    OperationResult<string> invoicePathResult = GMSManageMembership.GetInvoicePathByARitemNo(arItemNo, ExceConnectionManager.GetGymCode(user));
                    if (invoicePathResult.ErrorOccured)
                    {
                        foreach (NotificationMessage message in invoicePathResult.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), user);
                        }
                        return null;
                    }
                    else
                    {
                        invoicePath = invoicePathResult.OperationReturnValue;
                    }

                    OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                    if (isVisibel.ErrorOccured)
                    {
                        foreach (NotificationMessage message in isVisibel.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), user);
                        }
                        return null;
                    }
                    result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
                    result.FileParth = invoicePath;
                }
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }

        public NotificationTemplateText GetNotificationTextByType(NotifyMethodType method, TextTemplateEnum type, string user, int branchId)
        {
            USImportResult<NotificationTemplateText> result = NotificationAPI.GetNotificationTextByType(method, type, user, branchId);

            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.NotificationMessages)
                {
                    if (message.ErrorLevel == ErrorLevels.Error)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                }
                return new NotificationTemplateText();
            }
            else
            {
                return result.MethodReturnValue;
            }
        }

        [HttpGet]
        [Route("GetInstallments")]
        [Authorize]
        public HttpResponseMessage GetInstallments(int branchId, int memberContractId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.GetInstallments(memberContractId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> exceptionMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        exceptionMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<InstallmentDC>(), ApiResponseStatus.ERROR, exceptionMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<InstallmentDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpGet]
        [Route("GetInstallmentsToFreeze")]
        [Authorize]
        public HttpResponseMessage GetInstallmentsToFreeze(int memberContractId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.GetInstallmentsToFreeze(memberContractId, user, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> exceptionMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        exceptionMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<InstallmentDC>(), ApiResponseStatus.ERROR, exceptionMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<InstallmentDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpPost]
        [Route("GenerateAndSendInvoiceSMSANDEmail")]
        [Authorize]
        public HttpResponseMessage GenerateAndSendInvoiceSMSANDEmail(GenerateAndSendInvoiceSMSANDEmail generateAndSendInvoiceSMSANDEmail)
        {
            try
            {
                string invoiceType = string.Empty;
                if (generateAndSendInvoiceSMSANDEmail.isSelected["SMS"])
                    invoiceType = "SMS";
                else if (generateAndSendInvoiceSMSANDEmail.isSelected["EMAIL"])
                    invoiceType = "EMAIL";
                else
                    invoiceType = "SMS";

                string user = Request.Headers.GetValues("UserName").First();
                // Generate Invoice
                var newInvoice = GMSPayment.GenerateInvoice(generateAndSendInvoiceSMSANDEmail.invoiceBranchId, user, generateAndSendInvoiceSMSANDEmail.selectedOrder, generateAndSendInvoiceSMSANDEmail.paymentDetails, invoiceType, ExceConnectionManager.GetGymCode(user), generateAndSendInvoiceSMSANDEmail.memberBranchID);

                if (newInvoice.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in newInvoice.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    SendInvoiceSMSANDEmail sendInvoiceSmsAndEmail = new SendInvoiceSMSANDEmail()
                    {
                        branchId = generateAndSendInvoiceSMSANDEmail.memberBranchID,
                        arItemNo = newInvoice.OperationReturnValue.AritemNo,
                        memberID = generateAndSendInvoiceSMSANDEmail.selectedOrder.MemberId,
                        guardianId = generateAndSendInvoiceSMSANDEmail.selectedOrder.PayerId,
                        isSelected = generateAndSendInvoiceSMSANDEmail.isSelected,
                        text = generateAndSendInvoiceSMSANDEmail.text,
                        senderDetail = generateAndSendInvoiceSMSANDEmail.senderDetail,
                        invoiceRef = newInvoice.OperationReturnValue.InvoiceNo
                    };

                    return SendInvoiceSMSANDEmail(sendInvoiceSmsAndEmail);
                }

            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
            }
        }


        [HttpPost]
        [Route("SendInvoiceSMSANDEmail")]
        [Authorize]
        public HttpResponseMessage SendInvoiceSMSANDEmail(SendInvoiceSMSANDEmail sendInvoiceSMSANDEmail)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                ExcelineInvoiceDetailDC InvoiceDetails = GetInvoiceDetails(sendInvoiceSMSANDEmail.arItemNo, sendInvoiceSMSANDEmail.branchId, user);
                if (InvoiceDetails != null)
                {
                    // create parameter string for notification
                    Dictionary<TemplateFieldEnum, string> paraList = new Dictionary<TemplateFieldEnum, string>();
                    paraList.Add(TemplateFieldEnum.INVOICENO, InvoiceDetails.InvoiceNo.ToString());
                    paraList.Add(TemplateFieldEnum.DUEDATE, InvoiceDetails.InvoiceDueDate.Value.ToShortDateString());
                    paraList.Add(TemplateFieldEnum.AMOUNT, InvoiceDetails.InvoiceBalance.ToString());
                    paraList.Add(TemplateFieldEnum.BALANCE, InvoiceDetails.InvoiceBalance.ToString());
                    paraList.Add(TemplateFieldEnum.KID, InvoiceDetails.BasicDetails.KID.ToString());
                    paraList.Add(TemplateFieldEnum.GYMACCOUNT, InvoiceDetails.OtherDetails.BranchAccountNo);
                    paraList.Add(TemplateFieldEnum.GYMNAME, InvoiceDetails.OtherDetails.BranchName.ToString());
                    paraList.Add(TemplateFieldEnum.CREATEDUSER, user.ToString());
                    paraList.Add(TemplateFieldEnum.DATE, DateTime.Now.ToShortDateString());
                    paraList.Add(TemplateFieldEnum.MEMBERNAME, InvoiceDetails.CustomerName);

                    // create common notification
                    USCommonNotificationDC commonNotification = new USCommonNotificationDC();
                    //commonNotification.IsTemplateNotification = true;
                    commonNotification.ParameterString = paraList;
                    commonNotification.BranchID = sendInvoiceSMSANDEmail.branchId;
                    commonNotification.CreatedUser = user;
                    commonNotification.Role = "MEM";
                    commonNotification.NotificationType = NotificationTypeEnum.Messages;
                    commonNotification.Severity = NotificationSeverityEnum.Minor;
                    if (sendInvoiceSMSANDEmail.guardianId > 0)
                        commonNotification.MemberID = sendInvoiceSMSANDEmail.guardianId;
                    else
                        commonNotification.MemberID = sendInvoiceSMSANDEmail.memberID;

                    commonNotification.Status = NotificationStatusEnum.New;

                    bool sendedresult = true;

                    // if wants to send sms
                    if (sendInvoiceSMSANDEmail.isSelected["SMS"])
                    {
                        commonNotification.Type = TextTemplateEnum.SMSINVOICE;
                        commonNotification.Method = NotificationMethodType.SMS;
                        commonNotification.Title = "INVOICE SMS";
                        commonNotification.TempateText = sendInvoiceSMSANDEmail.text["SMS"];
                        commonNotification.SenderDescription = sendInvoiceSMSANDEmail.senderDetail["SMS"];

                        //add SMS notification into notification tables
                        USImportResult<int> result = NotificationAPI.AddNotification(commonNotification);

                        if (result.ErrorOccured)
                        {
                            foreach (USNotificationMessage message in result.NotificationMessages)
                            {
                                if (message.ErrorLevel == ErrorLevels.Error)
                                {
                                    USLogError.WriteToFile(message.Message, new Exception(), user);
                                }
                            }
                            sendedresult = false;
                        }
                        else
                        {
                            string application = ConfigurationManager.AppSettings["USC_Notification_SMS"].ToString();
                            string outputFormat = "SMS";

                            try
                            {
                                // send SMS through the USC API
                                IProcessingStatus smsStatus = USCAPI.ExecuteTemplate(application, outputFormat, ExceConnectionManager.GetGymCode(user), result.MethodReturnValue.ToString());

                                NotificationStatusEnum status = NotificationStatusEnum.Attended;
                                string statusMessage = string.Empty;
                                if (smsStatus != null)
                                {
                                    status = (smsStatus.Messages.FirstOrDefault(X => X.Header == "StatusCode").Message.ToString() == "1") ? NotificationStatusEnum.Success : NotificationStatusEnum.Fail;
                                    statusMessage = smsStatus.Messages.FirstOrDefault(x => x.Header == "StatusMessage").Message;
                                }
                                // update status in notification table
                                USImportResult<bool> updateResult = NotificationAPI.UpdateNotificationStatus(result.MethodReturnValue, user, status, statusMessage);

                                if (updateResult.ErrorOccured)
                                {
                                    foreach (USNotificationMessage message in updateResult.NotificationMessages)
                                    {
                                        if (message.ErrorLevel == ErrorLevels.Error)
                                        {
                                            USLogError.WriteToFile(message.Message, new Exception(), user);
                                        }
                                    }
                                    sendedresult = false;
                                }
                                else
                                {
                                    sendedresult = true;
                                }
                            }
                            catch (Exception e)
                            {
                                USLogError.WriteToFile(e.Message, new Exception(), user);
                                sendedresult = false;
                            }
                        }
                    }

                    // if wants to send email
                    if (sendInvoiceSMSANDEmail.isSelected["EMAIL"])
                    {
                        var filePath = ViewInvoiceDetails(sendInvoiceSMSANDEmail.branchId, user, sendInvoiceSMSANDEmail.arItemNo); // to print invoice pdf useing USC
                        String[] test = { "Greetings", "Robin", "this" ,"is", " the" , "data", "you", "requested", string.Empty};
                        test[8] = @filePath.FileParth;
                        File.WriteAllLines(@"D:\Exceline\DocumentStore\Dagbok4Robin.txt", test );
                        byte[] pdfBytes = null;
                        Economy.BusinessLogic.ExcelineInvoiceGenerator Gen = new Economy.BusinessLogic.ExcelineInvoiceGenerator(sendInvoiceSMSANDEmail.invoiceRef, sendInvoiceSMSANDEmail.branchId, ExceConnectionManager.GetGymCode(user));
                         pdfBytes = Gen.GetResult();
                         File.Delete("@filePath.FileParth");
                         File.WriteAllBytes(@filePath.FileParth, pdfBytes);

                        commonNotification.Type = TextTemplateEnum.EMAILINVOICE;
                        commonNotification.Method = NotificationMethodType.EMAIL;
                        commonNotification.AttachmentFileParth = filePath.FileParth;
                        commonNotification.Title = "INVOICE EMAIL";
                        commonNotification.TempateText = sendInvoiceSMSANDEmail.text["EMAIL"];
                        commonNotification.SenderDescription = sendInvoiceSMSANDEmail.senderDetail["EMAIL"];

                        //add EMAIL notification into notification tables
                        USImportResult<int> result = NotificationAPI.AddNotification(commonNotification);

                        if (result.ErrorOccured)
                        {
                            foreach (USNotificationMessage message in result.NotificationMessages)
                            {
                                if (message.ErrorLevel == ErrorLevels.Error)
                                {
                                    USLogError.WriteToFile(message.Message, new Exception(), user);
                                }
                            }
                            sendedresult = false;
                        }
                        else
                        {
                            string application = ConfigurationManager.AppSettings["USC_Notification_Email"].ToString();
                            string outputFormat = "EMAIL";

                            try
                            {
                                // send EMAIL through the USC API
                                IProcessingStatus emailStatus = USCAPI.ExecuteTemplate(application, outputFormat, ExceConnectionManager.GetGymCode(user), result.MethodReturnValue.ToString());
                                NotificationStatusEnum status = NotificationStatusEnum.Attended;
                                string statusMessage = string.Empty;
                                if (emailStatus != null)
                                {
                                    status = (emailStatus.Messages.FirstOrDefault(X => X.Header == "Status").Message.ToString() == "1") ? NotificationStatusEnum.Success : NotificationStatusEnum.Fail;
                                    statusMessage = emailStatus.Messages.FirstOrDefault(x => x.Header == "StatusMessage").Message;
                                }

                                // update status in notification table
                                USImportResult<bool> updateResult = NotificationAPI.UpdateNotificationStatus(result.MethodReturnValue, user, status, statusMessage);

                                if (updateResult.ErrorOccured)
                                {
                                    foreach (USNotificationMessage message in updateResult.NotificationMessages)
                                    {
                                        if (message.ErrorLevel == ErrorLevels.Error)
                                        {
                                            USLogError.WriteToFile(message.Message, new Exception(), user);
                                        }
                                    }
                                    sendedresult = false;
                                }
                                else
                                {
                                    if (!sendedresult)
                                        sendedresult = false;
                                }
                            }
                            catch (Exception e)
                            {
                                USLogError.WriteToFile(e.Message, new Exception(), user);
                                sendedresult = false;
                            }
                        }
                    }

                    // return sendedresult;
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(sendedresult, ApiResponseStatus.OK));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(false, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
            }
        }

        public ExcelineInvoiceDetailDC GetInvoiceDetails(int invoiceID, int branchId, string user)
        {
            var result = GMSInvoice.GetInvoiceDetails(invoiceID, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }

                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        [HttpGet]
        [Route("GetNotificationTemplateListByMethod")]
        [Authorize]
        public HttpResponseMessage GetNotificationTemplateListByMethod(NotificationMethodType method, TextTemplateEnum temp, int branchId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                USImportResult<List<ExceNotificationTepmlateDC>> result = NotificationAPI.GetNotificationTemplateListByMethod(method, temp, user, branchId);

                if (result.ErrorOccured)
                {
                    List<string> exceptionMsg = new List<string>();
                    foreach (USNotificationMessage message in result.NotificationMessages)
                    {
                        if (message.ErrorLevel == ErrorLevels.Error)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), user);
                        }
                    }

                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExceNotificationTepmlateDC>(), ApiResponseStatus.ERROR, exceptionMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.MethodReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExceNotificationTepmlateDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpGet]
        [Route("SaveMembersFile")]
        [Authorize]
        public HttpResponseMessage SaveMembersFile()
        {
            try
            {
                string _path = "C:\\Users\\unicorniba\\Desktop\\employment_verification_letter.doc";
                System.IO.FileInfo _file = new System.IO.FileInfo(_path);
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + _file.Name);
                HttpContext.Current.Response.AddHeader("Content-Length", _file.Length.ToString());
                HttpContext.Current.Response.ContentType = "application/octet-stream";
                HttpContext.Current.Response.WriteFile(_file.FullName);
                HttpContext.Current.Response.End();
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<InstallmentDC>(), ApiResponseStatus.ERROR, new List<string>()));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<InstallmentDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }
        //-----------------------Member-Freeze-----------------------------


        [HttpGet]
        [Route("GetContractFreezeItems")]
        [Authorize]
        public HttpResponseMessage GetContractFreezeItems(int memberContractId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.GetContractFreezeItems(memberContractId, user, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                                {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(string.Empty, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(string.Empty, ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpPost]
        [Route("SaveContractFreezeItem")]
        [Authorize]
        public HttpResponseMessage SaveContractFreezeItem(FreezeContract fContract)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.SaveContractFreezeItem(fContract.freezeItem, fContract.freezeInstallments, user, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    //manage the notification 
                    string notificationResult = SendSMSForFreeze(fContract.freezeItem.MemberContractNo, fContract.freezeItem.MemberId, fContract.branchId, user, fContract.senderDescription, fContract.notifyMethod, fContract.freezeItem, fContract.gymName);
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(notificationResult, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
            }
        }

        public string SendSMSForFreeze(string contractNo, int memberID, int branchId, string user, string senderDescription, string notifyMethod, ContractFreezeItemDC freezeItem, string gymName)
        {
            // create parameter string for notification
            string application = string.Empty;
            string outputFormat = string.Empty;
            NotificationMethodType nMethod = NotificationMethodType.NONE;
            if (notifyMethod == "SMS")
            {
                nMethod = NotificationMethodType.SMS;
                application = ConfigurationManager.AppSettings["USC_Notification_SMS"];
                outputFormat = "SMS";
            }
            else if (notifyMethod == "EMAIL")
            {
                nMethod = NotificationMethodType.EMAIL;
                application = ConfigurationManager.AppSettings["USC_Notification_Email"];
                outputFormat = "EMAIL";
            }

            var paraList = new Dictionary<TemplateFieldEnum, string>
                {
                    {TemplateFieldEnum.CONTRACTNO, contractNo},
                    {TemplateFieldEnum.STARTDATE, freezeItem.FromDate.ToShortDateString()},
                    {TemplateFieldEnum.ENDDATE, freezeItem.ToDate.ToShortDateString()},
                    {TemplateFieldEnum.GYMNAME, gymName.Trim()},
                };

            // create common notification
            var commonNotification = new USCommonNotificationDC
            {
                ParameterString = paraList,
                BranchID = branchId,
                CreatedUser = user,
                Role = "MEM",
                NotificationType = NotificationTypeEnum.Messages,
                Severity = NotificationSeverityEnum.Minor,
                MemberID = memberID,
                Status = NotificationStatusEnum.New,
                Method = nMethod,
                Type = TextTemplateEnum.FREEZESMS,
                Title = "FREEZE CONTRACT",
                SenderDescription = senderDescription,
                IsTemplateNotification = true
            };

            var result = NotificationAPI.AddNotification(commonNotification);
            if (result.ErrorOccured)
            {
                foreach (var mgs in result.NotificationMessages.Where(mgs => mgs.ErrorLevel == ErrorLevels.Error))
                {
                    USLogError.WriteToFile(mgs.Message, new Exception(), user);
                }
                return "NERROR";
            }

            try
            {
                if (notifyMethod == "SMS")
                {
                    // send SMS through the USC API
                    var smsStatus = USCAPI.ExecuteTemplate(application, outputFormat,
                                                           ExceConnectionManager.GetGymCode(user),
                                                           result.MethodReturnValue.ToString());
                    var status = NotificationStatusEnum.Attended;
                    string statusMessage = string.Empty;
                    if (smsStatus != null)
                    {
                        status = (smsStatus.Messages.FirstOrDefault(X => X.Header == "StatusCode").Message.ToString() ==
                                  "1")
                                     ? NotificationStatusEnum.Success
                                     : NotificationStatusEnum.Fail;
                        statusMessage = smsStatus.Messages.FirstOrDefault(x => x.Header == "StatusMessage").Message;
                    }

                    // update status in notification table
                    USImportResult<bool> updateResult =
                        NotificationAPI.UpdateNotificationStatus(result.MethodReturnValue, user, status, statusMessage);

                    if (updateResult.ErrorOccured)
                    {
                        foreach (USNotificationMessage message in updateResult.NotificationMessages)
                        {
                            if (message.ErrorLevel == ErrorLevels.Error)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), user);
                            }
                        }
                        return "NERROR";
                    }
                    return "SUCCESS";
                }
                if (notifyMethod == "EMAIL")
                {
                    // send EMAIL through the USC API
                    IProcessingStatus emailStatus = USCAPI.ExecuteTemplate(application, outputFormat, ExceConnectionManager.GetGymCode(user), result.MethodReturnValue.ToString());
                    NotificationStatusEnum status = NotificationStatusEnum.Attended;
                    string statusMessage = string.Empty;
                    if (emailStatus != null)
                    {
                        status = (emailStatus.Messages.FirstOrDefault(X => X.Header == "Status").Message.ToString() ==
                                  "1")
                                     ? NotificationStatusEnum.Success
                                     : NotificationStatusEnum.Fail;
                        statusMessage = emailStatus.Messages.FirstOrDefault(x => x.Header == "StatusMessage").Message;
                    }

                    // update status in notification table
                    USImportResult<bool> updateResult = NotificationAPI.UpdateNotificationStatus(result.MethodReturnValue, user, status, statusMessage);

                    if (updateResult.ErrorOccured)
                    {
                        foreach (USNotificationMessage message in updateResult.NotificationMessages)
                        {
                            if (message.ErrorLevel == ErrorLevels.Error)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), user);
                            }
                        }
                        return "NERROR";
                    }
                    return "SUCCESS";
                }
                if (notifyMethod == "PRINT")
                {
                    var memnberContractId = freezeItem.MemberContractid.ToString();
                    var startDate = freezeItem.FromDate.ToString("dd.MM.yyyy");
                    var endDate = freezeItem.ToDate.ToString("dd.MM.yyyy");

                    var appUrl = ConfigurationManager.AppSettings["USC_Freez_Print_App_Path"];
                    var dataDictionary = new Dictionary<string, string>() { { "MemberContractID", memnberContractId }, { "StartDate", startDate }, { "EndDate", endDate }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                    var pdfFileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);

                    if (!string.IsNullOrEmpty(pdfFileParth))
                        return pdfFileParth;
                    return "PDFERROR";
                }
            }
            catch (Exception)
            {
                return "NERROR";
            }
            return "SUCCESS";
        }

        [HttpGet]
        [Route("GetContractFreezeInstallments")]
        [Authorize]
        public HttpResponseMessage GetContractFreezeInstallments(int freezeItemId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.GetContractFreezeInstallments(freezeItemId, user, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(string.Empty, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(string.Empty, ApiResponseStatus.ERROR, errorMsg));
            }
        }



        [HttpGet]
        [Route("GetFollowUps")]
        [Authorize]
        public HttpResponseMessage GetFollowUps(int memberId, int followUpId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.GetFollowUps(memberId, followUpId, ExceConnectionManager.GetGymCode(user), user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<FollowUpDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<FollowUpDC>(), ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpPost]
        [Route("ExtendContractFreezeItem")]
        [Authorize]
        public HttpResponseMessage ExtendContractFreezeItem(FreezeContract fContract)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.ExtendContractFreezeItem(fContract.freezeItem, fContract.freezeInstallments, user, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    //manage the notification 
                    string notificationResult = SendSMSForFreeze(fContract.freezeItem.MemberContractNo, fContract.freezeItem.MemberId, fContract.branchId, user, fContract.senderDescription, fContract.notifyMethod, fContract.freezeItem, fContract.gymName);
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(notificationResult, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpGet]
        [Route("GetGroupMembers")]
        [Authorize]
        public HttpResponseMessage GetGroupMembers(int groupId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.GetGroupMembers(groupId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExcelineMemberDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExcelineMemberDC>(), ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpGet]
        [Route("GetFamilyMembers")]
        [Authorize]
        public HttpResponseMessage GetFamilyMembers(int memberId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.GetFamilyMembers(memberId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExcelineMemberDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExcelineMemberDC>(), ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpPost]
        [Route("UnfreezeContract")]
        [Authorize]
        public HttpResponseMessage UnfreezeContract(FreezeContract fContract)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.UnfreezeContract(fContract.freezeItem, fContract.freezeInstallments, user, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    try
                    {
                        USImportResult<NotificationTemplateText> notificationResult = NotificationAPI.GetNotificationTextByType(NotifyMethodType.EVENTLOG, TextTemplateEnum.UNFREEZCONTRACT, user, fContract.branchId);
                        string textTemplate = string.Empty;
                        NotificationTemplateText notificationTemplateText = new NotificationTemplateText();
                        if (!notificationResult.ErrorOccured && notificationResult.MethodReturnValue != null)
                        {
                            notificationTemplateText = notificationResult.MethodReturnValue;
                            textTemplate = notificationTemplateText.Text;
                        }
                        List<TemplateField> templateFieldList = new List<TemplateField>();
                        string memberNo = string.Empty;
                        try
                        {
                            OrdinaryMemberDC memberSummary = (GMSMember.GetMembersById(fContract.branchId, fContract.freezeItem.MemberId, ExceConnectionManager.GetGymCode(user))).OperationReturnValue;
                            memberNo = memberSummary.CustId;
                        }
                        catch
                        {
                        }
                        try
                        {
                            templateFieldList.Add(new TemplateField { Key = TemplateFieldEnum.MEMBERNO, Value = memberNo });
                            templateFieldList.Add(new TemplateField { Key = TemplateFieldEnum.CONTRACTNO, Value = fContract.freezeItem.MemberContractNo });
                            templateFieldList.Add(new TemplateField { Key = TemplateFieldEnum.DATE, Value = DateTime.Now.ToShortDateString() });
                            templateFieldList.Add(new TemplateField { Key = TemplateFieldEnum.CREATEDUSER, Value = user });
                        }
                        catch
                        {
                        }

                        string text = NotificationAPI.ConvertTemplateToText(textTemplate, templateFieldList);
                        if (!string.IsNullOrEmpty(text.Trim()))
                        {
                            USNotificationTree noitificationTree = new USNotificationTree();
                            noitificationTree.BranchId = fContract.branchId;
                            noitificationTree.MemberId = fContract.freezeItem.MemberId;
                            USNotification notification = new USNotification();
                            notification.Module = "Manage MemberShip";
                            notification.Title = fContract.notificationTitle;
                            notification.Description = text;
                            notification.CreatedDate = DateTime.Now;
                            notification.CreatedUser = user;
                            notification.TypeId = 3;
                            notification.SeverityId = 3;
                            notification.StatusId = 1;
                            notification.CreatedUserRoleId = "MEM";
                            noitificationTree.Notification = notification;
                            USNotificationAction action = new USNotificationAction();
                            action.Comment = "Notification was saved automatically";
                            action.AssgnId = user;
                            noitificationTree.Action = action;
                            List<USNotificationChannel> channelList = new List<USNotificationChannel>();
                            USNotificationChannel channel = new USNotificationChannel();
                            channel.ChannelDueDate = DateTime.Now.AddDays(30);
                            channel.MessageType = NotifyMethodType.EVENTLOG.ToString();
                            channel.OccuranceType = OccuranceType.ONCE.ToString();

                            channel.ChannelMessage = "New Message";
                            channel.ChannelAssign = user;
                            channel.ChannelStatus = true;
                            channelList.Add(channel);
                            noitificationTree.NotificationMethods = channelList;
                            NotificationAPI.AddNotification(noitificationTree);
                        }
                    }
                    catch
                    {
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
            }
        }


        [HttpGet]
        [Route("GetFollowUpTask")]
        [Authorize]
        public HttpResponseMessage GetFollowUpTask()
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.GetFollowUpTask(ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> exceptionMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        exceptionMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<FollowUpTemplateTaskDC>(), ApiResponseStatus.ERROR, exceptionMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<FollowUpTemplateTaskDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpGet]
        [Route("GetMemberInfoWithNotes")]
        [Authorize]
        public HttpResponseMessage GetMemberInfoWithNotes(int branchId, int memberId)
        {

            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.GetMemberInfoWithNotes(branchId, memberId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> exceptionMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        exceptionMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new OrdinaryMemberDC(), ApiResponseStatus.ERROR, exceptionMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new OrdinaryMemberDC(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpGet]
        [Route("ValidateChangeGymInMember")]
        [Authorize]
        public HttpResponseMessage ValidateChangeGymInMember(int currentBranchID, int newBranchID, int branchID, int memberID)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.ValidateChangeGymInMember(currentBranchID, newBranchID, branchID, memberID, user, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> exceptionMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        exceptionMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(0, ApiResponseStatus.ERROR, exceptionMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(0, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }
      
        [HttpPost]
        [Route("SaveMemberInfoWithNotes")]
        [Authorize]
        public HttpResponseMessage SaveMemberInfoWithNotes(OrdinaryMemberDC member)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.SaveMemberInfoWithNotes(member, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(string.Empty, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(string.Empty, ApiResponseStatus.ERROR, errorMsg));
            }

        }

        [HttpGet]
        [Route("GetGymsforAccountNumber")]
        [Authorize]
        public HttpResponseMessage GetGymsforAccountNumber(string accountNumber)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSGymDetail.GetGymsforAccountNumber(accountNumber, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExceUserBranchDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExceUserBranchDC>(), ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpPost]
        [Route("ChangePrePaidAccount")]
        [Authorize]
        public HttpResponseMessage ChangePrePaidAccount(string increase, float amount, int memberId, int branchId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.ChangePrePaidAccount(increase, amount, memberId, branchId, ExceConnectionManager.GetGymCode(user), user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
            }
        }


        [HttpGet]
        [Route("GetEconomyDetails")]
        [Authorize]
        public HttpResponseMessage GetEconomyDetails(int memberId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.GetEconomyDetails(memberId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new OrdinaryMemberDC(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new OrdinaryMemberDC(), ApiResponseStatus.ERROR, errorMsg));
            }
        }


       

        [HttpPost]
        [Route("SaveEconomyDetails")]
        [Authorize]
        public HttpResponseMessage SaveEconomyDetails(OrdinaryMemberDC member)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.SaveEconomyDetails(member, ExceConnectionManager.GetGymCode(user), user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpGet]
        [Route("GetListOfGuardian")]
        [Authorize]
        public HttpResponseMessage GetListOfGuardian(int guardianId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.GetListOfGuardian(guardianId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> exceptionMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        exceptionMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExcelineMemberDC>(), ApiResponseStatus.ERROR, exceptionMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExcelineMemberDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpGet]
        [Route("ViewInvoiceDetails")]
        [Authorize]
        public HttpResponseMessage ViewInvoiceDetails(int branchId, int arItemNo)
        {
            PDFPrintResultDC result = new PDFPrintResultDC();
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                if (!IsBrisIntegration())
                {
                    var appUrl = ConfigurationManager.AppSettings["USC_Invoice_Print_App_Path"];
                    var dataDictionary = new Dictionary<string, string>() { { "itemID", arItemNo.ToString() }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                    result.FileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);
                    SetPDFFileParth(arItemNo, result.FileParth, "INV", branchId, user);

                    OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                    if (isVisibel.ErrorOccured)
                    {
                        List<string> exceptionMsg = new List<string>();
                        foreach (NotificationMessage message in isVisibel.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), user);
                            exceptionMsg.Add(message.Message);
                        }
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new PDFPrintResultDC(), ApiResponseStatus.ERROR, exceptionMsg));
                    }
                    else
                    {
                        result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
                        return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
                    }
                }
                else
                {
                    string invoicePath = string.Empty;
                    OperationResult<string> invoicePathResult = GMSManageMembership.GetInvoicePathByARitemNo(arItemNo, ExceConnectionManager.GetGymCode(user));
                    if (invoicePathResult.ErrorOccured)
                    {
                        List<string> exceptionMsg = new List<string>();
                        foreach (NotificationMessage message in invoicePathResult.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), user);
                            exceptionMsg.Add(message.Message);
                        }
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new PDFPrintResultDC(), ApiResponseStatus.ERROR, exceptionMsg));
                    }
                    else
                    {

                        if (Path.GetExtension(invoicePathResult.OperationReturnValue.ToLower()) == ".xml")
                        {
                            string fileName = Path.GetFileNameWithoutExtension(invoicePathResult.OperationReturnValue);
                            string filePath = Path.GetDirectoryName(invoicePathResult.OperationReturnValue);
                            string saveFilePath = filePath + "\\" + fileName + ".pdf";

                            var xmlDic = ReadXmlToPdf(invoicePathResult.OperationReturnValue);
                            string htmlContent = Convert.ToString(ReadHtmlFile());
                            foreach (var item in xmlDic)
                            {
                                htmlContent = htmlContent.Replace(item.Key, item.Value);
                            }

                            string tableHtml = string.Empty;
                            var tList = ReadXmlToPdfTable(invoicePathResult.OperationReturnValue);
                            int i = 0;
                            foreach (var item in tList)
                            {
                                string htmlTableBodyu = "<tr class=\"item font-m\">";
                                htmlTableBodyu = htmlTableBodyu + "<td>" + item["[[UNITDESCR]]"] + "</td>";
                                htmlTableBodyu = htmlTableBodyu + "<td class=\"value-td \">" + item["[[QUANTITY]]"] + "</td>";
                                htmlTableBodyu = htmlTableBodyu + "<td class=\"value-td \">" + item["[[PRICE]]"] + "</td>";
                                htmlTableBodyu = htmlTableBodyu + "<td class=\"value-td \">" + item["[[BELOP]]"] + "</td>";
                                //foreach (var y in item)
                                //{
                                //    htmlTableBodyu = htmlTableBodyu + "<td>" + y.Value + "</td>";
                                //}
                                htmlTableBodyu = htmlTableBodyu + "</tr>";
                                tableHtml = tableHtml + htmlTableBodyu;
                            }
                            htmlContent = htmlContent.Replace("[[tBody]]", tableHtml);
                            GeneratePDF(htmlContent, saveFilePath);

                            OperationResult<bool> savePdfPathResult =new OperationResult<bool>();// GMSManageMembership.UpdateInvoicePdfPath(arItemNo, saveFilePath, ExceConnectionManager.GetGymCode(user));
                            if (savePdfPathResult.ErrorOccured)
                            {
                                List<string> exceptionMsg = new List<string>();
                                foreach (NotificationMessage message in invoicePathResult.Notifications)
                                {
                                    USLogError.WriteToFile(message.Message, new Exception(), user);
                                    exceptionMsg.Add(message.Message);
                                }
                                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, exceptionMsg));
                            }
                            if (savePdfPathResult.OperationReturnValue)
                                invoicePath = saveFilePath;
                        }
                        else
                        {
                            invoicePath = invoicePathResult.OperationReturnValue;
                        }
                    }

                    OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                    if (isVisibel.ErrorOccured)
                    {
                        List<string> exceptionMsg = new List<string>();
                        foreach (NotificationMessage message in isVisibel.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), user);
                            exceptionMsg.Add(message.Message);
                        }
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, exceptionMsg));
                    }
                    result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
                    result.FileParth = invoicePath;
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(result, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        public static Dictionary<string, string> ReadXmlToPdf(string xmlPath)
        {
            var xmlDetails = new Dictionary<string, string>();

            XmlDocument doc = new XmlDocument();
            //doc.Load(@"e:\\5908044.xml");
            doc.Load(xmlPath);

            foreach (XmlNode row in doc.SelectNodes("//Invoice"))
            {
                var selectedVal = row.SelectSingleNode("InvoiceNo").InnerText;
                xmlDetails.Add("[[INVOICENO]]", selectedVal);
            }
            foreach (XmlNode row in doc.SelectNodes("//Header"))
            {
                var selectedVal = row.SelectSingleNode("DueDate").InnerText;
                xmlDetails.Add("[[DUEDATE]]", selectedVal);

            }
            foreach (XmlNode row in doc.SelectNodes("//Header"))
            {
                var selectedVal = row.SelectSingleNode("InvoiceDate").InnerText;
                xmlDetails.Add("[[INVOICEDATE]]", selectedVal);

            }
            foreach (XmlNode row in doc.SelectNodes("//Header/Buyer"))
            {
                var buyerNo = row.SelectSingleNode("BuyerNo").InnerText;
                xmlDetails.Add("[[BUYERNO]]", buyerNo);
            }
            foreach (XmlNode row in doc.SelectNodes("//Header/PaymentInfo"))
            {
                var accountNumber = row.SelectSingleNode("AccountNumber").InnerText;
                xmlDetails.Add("[[ACCOUNTNUMBER]]", accountNumber);

                var bacsId = row.SelectSingleNode("BacsId").InnerText;
                xmlDetails.Add("[[BACSID]]", bacsId);
            }
            foreach (XmlNode row in doc.SelectNodes("//Header/BillTo"))
            {
                var billToName = row.SelectSingleNode("Name").InnerText;
                xmlDetails.Add("[[BILLTO_NAME]]", billToName);
            }
            foreach (XmlNode row in doc.SelectNodes("//Header/BillTo/AddressInfo"))
            {
                var address = row.SelectSingleNode("Address").InnerText;
                xmlDetails.Add("[[ADDRESS]]", address);
                var place = row.SelectSingleNode("Place").InnerText;
                xmlDetails.Add("[[PLACE]]", place);
                var zipCode = row.SelectSingleNode("ZipCode").InnerText;
                xmlDetails.Add("[[ZIPCODE]]", zipCode);
                //var country = row.SelectSingleNode("Country").InnerText;
                //xmlDetails.Add("[[COUNTRY]]", country);

            }
            foreach (XmlNode row in doc.SelectNodes("//Summary"))
            {
                var totalInclTax = row.SelectSingleNode("TotalInclTax").InnerText;
                xmlDetails.Add("[[TOTALINCLTAX]]", totalInclTax);
            }

            //foreach (XmlNode row in doc.SelectNodes("//Invoice"))
            //{
            //    var selectedVal = row.SelectSingleNode("InvoiceNo").InnerText;
            //    xmlDetails.Add("[[INVOICENO]]", selectedVal);
            //}

            //foreach (XmlNode row in doc.SelectNodes("//Header"))
            //{
            //    var selectedVal = row.SelectSingleNode("InvoiceDate").InnerText;
            //    xmlDetails.Add("[[INVOICEDATE]]", selectedVal);

            //}
            //foreach (XmlNode row in doc.SelectNodes("//Header/Seller/AddressInfo"))
            //{
            //    var address = row.SelectSingleNode("Address").InnerText;
            //    xmlDetails.Add("[[ADDRESS]]", address);
            //    var place = row.SelectSingleNode("Place").InnerText;
            //    xmlDetails.Add("[[PLACE]]", place);
            //    var zipCode = row.SelectSingleNode("ZipCode").InnerText;
            //    xmlDetails.Add("[[ZIPCODE]]", zipCode);
            //    var country = row.SelectSingleNode("Country").InnerText;
            //    xmlDetails.Add("[[COUNTRY]]", country);

            //}
            return xmlDetails;
        }

        public static List<Dictionary<string, string>> ReadXmlToPdfTable(string xmlPath)
        {
            var returnDetails = new List<Dictionary<string, string>>();
            XmlDocument doc = new XmlDocument();
            //doc.Load(@"e:\\5908044.xml");
            doc.Load(xmlPath);

            var detailNodeList = doc.SelectNodes("//Details/Detail");
            var productNodeList = doc.SelectNodes("//Details/Detail/Products");

            for (int i = 0; i < detailNodeList.Count; i++)
            {
                var itemDic = new Dictionary<string, string>();
                var detailItem = detailNodeList[i];
                var lineTotInclTax = detailItem.SelectSingleNode("LineTotInclTax").InnerText;
                itemDic.Add("[[BELOP]]", lineTotInclTax);
                var productItem = productNodeList[i];
                var unitDescr = productItem.SelectSingleNode("SellerProductDescr").InnerText;
                itemDic.Add("[[UNITDESCR]]", unitDescr);
                var quantity = productItem.SelectSingleNode("Quantity").InnerText;
                itemDic.Add("[[QUANTITY]]", quantity);
                var price = productItem.SelectSingleNode("Price").InnerText;
                itemDic.Add("[[PRICE]]", price);

                returnDetails.Add(itemDic);

            }
            //foreach (XmlNode row in doc.SelectNodes("//Details/Detail/Products"))
            //{
            //    var item = new Dictionary<string, string>();
            //    var unitDescr = row.SelectSingleNode("UnitDescr").InnerText;
            //    item.Add("[[UNITDESCR]]", unitDescr);
            //    var quantity = row.SelectSingleNode("Quantity").InnerText;
            //    item.Add("[[QUANTITY]]", quantity);
            //    var price = row.SelectSingleNode("Price").InnerText;
            //    item.Add("[[PRICE]]", price);
            //    var belop = row.SelectSingleNode("Price").InnerText;
            //    item.Add("[[BELOP]]", belop);
            //    returnDetails.Add(item);

            //}
            return returnDetails;
        }

        private static void GeneratePDF(string htmlcontent, string saveFilePath)
        {
            try
            {
                Doc theDoc = new Doc();
                theDoc.AddImageHtml(htmlcontent);
                // theDoc.AddImageUrl("file://" + @"E:\testpage.html");
                theDoc.HtmlOptions.ContentCount = 10;
                theDoc.HtmlOptions.RetryCount = 50;
                theDoc.HtmlOptions.Timeout = 20000000;
                theDoc.HtmlOptions.AutoTruncate = true;
                theDoc.HtmlOptions.BrowserWidth = 827; //795
                theDoc.HtmlOptions.FontEmbed = true;
                theDoc.HtmlOptions.FontProtection = false;
                theDoc.HtmlOptions.FontSubstitute = false;
                theDoc.Rendering.DotsPerInch = 36;
                theDoc.Rendering.SaveQuality = 75;
                theDoc.MediaBox.String = "A4";
                theDoc.Rect.String = "A4";

                //theDoc.Save(@"E:\samplepdf.pdf");
                theDoc.Save(saveFilePath);
                theDoc.HtmlOptions.PageCacheClear();
                theDoc.Clear();
            }
            catch (Exception ex)
            {
                File.WriteAllText(@"C:\Exceline\Logs\Error\error.txt", ex.Message);
                USLogError.WriteToFile(ex.Message, new Exception(), "");
            }

        }

        public static System.Text.StringBuilder ReadHtmlFile()
        {
            System.Text.StringBuilder htmlContent = new System.Text.StringBuilder();
            string line;
            try
            {
                var htmlTemplateFilePath = ConfigurationManager.AppSettings["Html_TemplateFile_Path"];
                //using (System.IO.StreamReader htmlReader = new System.IO.StreamReader(@"e:\testpage.html"))
                using (System.IO.StreamReader htmlReader = new System.IO.StreamReader(htmlTemplateFilePath))
                {

                    while ((line = htmlReader.ReadLine()) != null)
                    {
                        htmlContent.Append(line);
                    }
                }
            }
            catch (Exception objError)
            {
                throw objError;
            }

            return htmlContent;
        }

        [HttpGet]
        [Route("ViewCreditNoteDetails")]
        [Authorize]
        public HttpResponseMessage ViewCreditNoteDetails(int branchId, int CreditNoteID)
        {
            PDFPrintResultDC result = new PDFPrintResultDC();
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var appUrl = ConfigurationManager.AppSettings["USC_CreditNote_Print_App_Path"].ToString();
                var dataDictionary = new Dictionary<string, string>() { { "itemID", CreditNoteID.ToString() }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                result.FileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);
                SetPDFFileParth(CreditNoteID, result.FileParth, "CNT", branchId, user);

                OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                if (isVisibel.ErrorOccured)
                {
                    foreach (NotificationMessage message in isVisibel.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                    return null;
                }
                else
                {
                    result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(result, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpGet]
        [Route("ViewMemberATGContract")]
        [Authorize]
        public HttpResponseMessage ViewMemberATGContract(int branchId, int memberId)
        {
            PDFPrintResultDC result = new PDFPrintResultDC();
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var appUrl = ConfigurationManager.AppSettings["USC_Contract_ATG_App_Path"];
                var dataDictionary = new Dictionary<string, string>() { { "itemID", memberId.ToString() }, { "branchID", branchId.ToString() }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                result.FileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);
                // manipulate the path to get the PDF 

                SetPDFFileParth(memberId, result.FileParth, "ATG", branchId, user);
                USLogError.WriteToFile("got the path", new Exception(), user);
                OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                if (isVisibel.ErrorOccured)
                {
                    foreach (NotificationMessage message in isVisibel.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                    return null;
                }
                else
                {
                    result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(result, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpGet]
        [Route("GetMemberInvoices")]
        [Authorize]
        public HttpResponseMessage GetMemberInvoices(int memberId, int hit)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSInvoice.GetMemberInvoices(memberId, hit, ExceConnectionManager.GetGymCode(user), user);
                if (result.ErrorOccured)
                {
                    List<string> exceptionMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        exceptionMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<MemberInvoiceDC>(), ApiResponseStatus.ERROR, exceptionMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<MemberInvoiceDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpGet]
        [Route("GetInvoiceDetails")]
        [Authorize]
        public HttpResponseMessage GetInvoiceDetails(int invoiceID, int branchId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSInvoice.GetInvoiceDetails(invoiceID, branchId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> exceptionMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        exceptionMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, exceptionMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpPost]
        [Route("UpdateInvoice")]
        [Authorize]
        public HttpResponseMessage UpdateInvoice(UpdateInvoice updateInvoice)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSInvoice.UpdateInvoice(updateInvoice.ArItemNo, updateInvoice.Comment, updateInvoice.DueDate, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
            }
        }


        [HttpPost]
        [Route("AddCreditNote")]
        [Authorize]
        public HttpResponseMessage AddCreditNote(CreditNoteDC creditNote)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSInvoice.AddCreditNote(creditNote, creditNote.BranchId, false, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpGet]
        [Route("GetCreditNoteDetails")]
        [Authorize]
        public HttpResponseMessage GetCreditNoteDetails(int arItemno)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.GetCreditNoteDetails(arItemno, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> exceptionMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        exceptionMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, exceptionMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpGet]
        [Route("PrintGroupMemberListByMember")]
        [Authorize]
        public HttpResponseMessage PrintGroupMemberListByMember(int branchId, string memberID)
        {

            PDFPrintResultDC result = new PDFPrintResultDC();
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var appUrl = ConfigurationManager.AppSettings["USC_GroupMemberList_Print_App_Path"];
                var dataDictionary = new Dictionary<string, string>() { { "memberID", memberID }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                result.FileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);
                SetPDFFileParth(Convert.ToInt32(memberID), result.FileParth, "GML", branchId, user);

                OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                if (isVisibel.ErrorOccured)
                {
                    foreach (NotificationMessage message in isVisibel.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                    return null;
                }
                else
                {
                    result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(result, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpGet]
        [Route("PrintInvoicePaidReceipt")]
        [Authorize]
        public HttpResponseMessage PrintInvoicePaidReceipt(int branchId, int arItemNo)
        {

            PDFPrintResultDC result = new PDFPrintResultDC();
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var appUrl = ConfigurationManager.AppSettings["USC_Invoice_PrintReceipt_App_Path"];
                var dataDictionary = new Dictionary<string, string>() { { "itemID", arItemNo.ToString() }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                result.FileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);

                OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                if (isVisibel.ErrorOccured)
                {
                    foreach (NotificationMessage message in isVisibel.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                    return null;
                }
                else
                {
                    result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(result, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpGet]
        [Route("ViewSponsorInvoiceDetails")]
        [Authorize]
        public HttpResponseMessage ViewSponsorInvoiceDetails(int branchId, int arItemNo)
        {

            PDFPrintResultDC result = new PDFPrintResultDC();
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var appUrl = ConfigurationManager.AppSettings["USC_SponsorInvoiceDetails_Print_App_Path"];
                var dataDictionary = new Dictionary<string, string>() { { "itemID", arItemNo.ToString() }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                result.FileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);
                SetPDFFileParth(arItemNo, result.FileParth, "SPINV", branchId, user);

                OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                if (isVisibel.ErrorOccured)
                {
                    foreach (NotificationMessage message in isVisibel.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                    return null;
                }
                else
                {
                    result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(result, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpGet]
        [Route("GetSponsorSetting")]
        [Authorize]
        public HttpResponseMessage GetSponsorSetting(int branchId, int sponsorId)
        {

            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.GetSponsorSetting(branchId, user, sponsorId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> exceptionMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        exceptionMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new SponsorSettingDC(), ApiResponseStatus.ERROR, exceptionMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new SponsorSettingDC(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpGet]
        [Route("GetGroupDiscountByType")]
        [Authorize]
        public HttpResponseMessage GetGroupDiscountByType(int branchId, int discountTypeId, int sponsorId)
        {

            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.GetGroupDiscountByType(branchId, user, discountTypeId, ExceConnectionManager.GetGymCode(user), sponsorId);
                if (result.ErrorOccured)
                {
                    List<string> exceptionMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        exceptionMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<DiscountDC>(), ApiResponseStatus.ERROR, exceptionMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<DiscountDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpGet]
        [Route("DeleteDiscountCategory")]
        [Authorize]
        public HttpResponseMessage DeleteDiscountCategory(int categoryId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var discountList = new List<int>();
                discountList.Add(categoryId);
                var result = GMSManageMembership.DeleteDiscount(discountList, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> exceptionMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        exceptionMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpGet]
        [Route("DeleteEmployeeCategory")]
        [Authorize]
        public HttpResponseMessage DeleteEmployeeCategory(int categoryId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.DeleteEmployeeCategory(categoryId, ExceConnectionManager.GetGymCode(user)); ;
                if (result.ErrorOccured)
                {
                    List<string> exceptionMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        exceptionMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpPost]
        [Route("GenerateMemberFee")]
        [Authorize]
        public HttpResponseMessage GenerateMemberFee(MemberFeeGenerationDetails memberFeegenerationDetails)
        {
            string user = Request.Headers.GetValues("UserName").First();
            OperationResult<MemberFeeGenerationResultDC> result = GMSMember.GenerateMemberFee(memberFeegenerationDetails.gyms, memberFeegenerationDetails.month, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                List<string> exceptionMsg = new List<string>();
                foreach (var message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                    exceptionMsg.Add(message.Message);
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new MemberFeeGenerationResultDC() { Status = false }, ApiResponseStatus.ERROR, exceptionMsg));
            }
            return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
        }

        [HttpPost]
        [Route("MemberFeeConfirmation")]
        [Authorize]
        public HttpResponseMessage GetConfirmationDetailForMemberFee(MemberFeeGenerationDetails memberFeegenerationDetails)
        {
            string user = Request.Headers.GetValues("UserName").First();
            OperationResult<List<string>> result = GMSMember.ConfirmationDetailForGenerateMemberFee(memberFeegenerationDetails.gyms, memberFeegenerationDetails.month, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                List<string> exceptionMsg = new List<string>();
                foreach (var message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                    exceptionMsg.Add(message.Message);
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<string>(), ApiResponseStatus.ERROR, exceptionMsg));
            }
            return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
        }

        [HttpPost]
        [Route("ValidateMemberFee")]
        [Authorize]
        public HttpResponseMessage ValidateGenerateMemberFeeWithMemberFeeMonth(MemberFeeGenerationDetails memberFeegenerationDetails)
        {
            string user = Request.Headers.GetValues("UserName").First();
            OperationResult<bool> result = GMSMember.ValidateGenerateMemberFeeWithMemberFeeMonth(memberFeegenerationDetails.gyms, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                List<string> exceptionMsg = new List<string>();
                foreach (var message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                    exceptionMsg.Add(message.Message);
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
            return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
        }

        //[HttpGet]
        //[Route("DeleteEmployeeCategory")]
        //[Authorize]
        //public HttpResponseMessage DeleteEmployeeCategory(int categoryId)
        //{
        //    try
        //    {
        //        string user = Request.Headers.GetValues("UserName").First();
        //        var result = GMSManageMembership.DeleteEmployeeCategory(categoryId, ExceConnectionManager.GetGymCode(user)); ;
        //        if (result.ErrorOccured)
        //        {
        //            List<string> exceptionMsg = new List<string>();
        //            foreach (NotificationMessage message in result.Notifications)
        //            {
        //                USLogError.WriteToFile(message.Message, new Exception(), user);
        //                exceptionMsg.Add(message.Message);
        //            }
        //            return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
        //        }
        //        else
        //        {
        //            return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        List<string> exceptionMsg = new List<string>();
        //        exceptionMsg.Add(ex.Message);
        //        return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
        //    }
        //}

        [HttpGet]
        [Route("BrisIntegration")]
        [Authorize]
        public HttpResponseMessage BrisIntegration()
        {
            try
            {
                var brisIntegration = ConfigurationManager.AppSettings["BrisIntegration"];
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(brisIntegration == "1", ApiResponseStatus.OK));
            } catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
           
        }

        [HttpGet]
        [Route("ViewMemberContractDetail")]
        [Authorize]
        public HttpResponseMessage ViewMemberContractDetail(int branchId, int contractId)
        {
            PDFPrintResultDC result = new PDFPrintResultDC();
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var appUrl = ConfigurationManager.AppSettings["USC_MemberContract_Print_App_Path"];
                var dataDictionary = new Dictionary<string, string>() { { "itemID", contractId.ToString() }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                result.FileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);
                SetPDFFileParth(contractId, result.FileParth, "CONT", branchId, user);

                OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                if (isVisibel.ErrorOccured)
                {
                    foreach (NotificationMessage message in isVisibel.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                    return null;
                }
                result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new PDFPrintResultDC(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpGet]
        [Route("PrintGroupContractMemberList")]
        [Authorize]
        public HttpResponseMessage PrintGroupContractMemberList(int branchId, string contractID, string memberType)
        {
            PDFPrintResultDC result = new PDFPrintResultDC();
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var appUrl = ConfigurationManager.AppSettings["USC_GroupContractMemberList_Print_App_Path"];
                var dataDictionary = new Dictionary<string, string>() { { "groupContractID", contractID }, { "memberType", contractID }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                result.FileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);
                SetPDFFileParth(Convert.ToInt32(contractID), result.FileParth, "GCM", branchId, user);

                OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                if (isVisibel.ErrorOccured)
                {
                    foreach (NotificationMessage message in isVisibel.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                    return null;
                }
                result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new PDFPrintResultDC(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpGet]
        [Route("PrintOrder")]
        [Authorize]
        public HttpResponseMessage PrintGroupContractMemberList(int orderId, int branchId)
        {
            PDFPrintResultDC result = new PDFPrintResultDC();
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var appUrl = ConfigurationManager.AppSettings["USC_Order_Print_App_Path"];
                var dataDictionary = new Dictionary<string, string>() { { "itemID", orderId.ToString() }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                result.FileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);

                OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                if (isVisibel.ErrorOccured)
                {
                    foreach (NotificationMessage message in isVisibel.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                    return null;
                }
                result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new PDFPrintResultDC(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpGet]
        [Route("PrintPaymentHistory")]
        [Authorize]
        public HttpResponseMessage PrintPaymentHistory(int branchId,  int memberId, string fromDate, string toDate)
        {
            PDFPrintResultDC result = new PDFPrintResultDC();
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var appUrl = ConfigurationManager.AppSettings["USC_MemberPaymentHistory_Print_App_Path"];
                var dataDictionary = new Dictionary<string, string>() { { "MemberID", memberId.ToString() }, { "fromDate", fromDate}, { "toDate", toDate}, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                result.FileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);
                SetPDFFileParth(memberId, result.FileParth, "PHP", branchId, user);

                OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                if (isVisibel.ErrorOccured)
                {
                    foreach (NotificationMessage message in isVisibel.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                    return null;
                }
                result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new PDFPrintResultDC(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpGet]
        [Route("PrintPurchaseHistory")]
        [Authorize]
        public HttpResponseMessage PrintMemberSales(int memberID, string fromDate, string toDate,  int branchID)
        {
            PDFPrintResultDC result = new PDFPrintResultDC();
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var appUrl = ConfigurationManager.AppSettings["USC_MemberSales_Print_App_Path"];
                var dataDictionary = new Dictionary<string, string>() { { "MemberID", memberID.ToString() }, { "fromDate", fromDate }, { "toDate", toDate }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                result.FileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);
                SetPDFFileParth(memberID, result.FileParth, "MSE", branchID, user);

                OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchID, ExceConnectionManager.GetGymCode(user));
                if (isVisibel.ErrorOccured)
                {
                    foreach (NotificationMessage message in isVisibel.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                    return null;
                }
                result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new PDFPrintResultDC(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpGet]
        [Route("DeleteCreditNote")]
        [Authorize]
        public HttpResponseMessage DeleteCreditNote(int creditNoteId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.DeleteCreditNote(creditNoteId, ExceConnectionManager.GetGymCode(user));
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
            }

            catch(Exception exceptionmsg)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(exceptionmsg.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
            
        }

        [HttpPost]
        [Route("RegisterChangeOfInvoiceDueDate")]
        [Authorize]
        public HttpResponseMessage RegisterChangeOfInvoiceDueDate(AlterationInDueDateData changeData)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.RegisterChangeOfInvoiceDueDate(changeData, user, ExceConnectionManager.GetGymCode(user));
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(true, ApiResponseStatus.OK));
            }

            catch (Exception exceptionmsg)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(exceptionmsg.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

    }
}
        



