﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml;
using US.Common.Logging.API;
using US.Common.Notification.API;
using US.Common.Notification.Core.DomainObjects;
using US.Common.Notification.Core.Enums;
using US.Communication.API;
using US.Communication.Core.SystemCore.DomainObjects;
using US.Exceline.GMS.Modules.Admin.API.ManageMembers;
using US.Exceline.GMS.Modules.ManageMembership.API.ManageMembership;
using US.Exceline.GMS.Modules.ManageMembership.RestApi.Models;
using US.Exceline.GMS.Modules.ManageMembership.BusinessLogic.ManageMembership;
using US.GMS.API;
using US.GMS.API.ManageSystemSettings;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.Utils;
using US.Payment.Core.Enums;
using US.Payment.Core.ResultNotifications;
using WebSupergoo.ABCpdf8;
using US.Exceline.GMS.Modules.Economy.BusinessLogic;

namespace US.Exceline.GMS.Modules.ManageMembership.RestApi.Controllers
{
    [RoutePrefix("api/Member/Contract")]
    public class ContractController : ApiController
    {
        [HttpGet]
        [Route("GetContractSummeries")]
        [Authorize]
        public HttpResponseMessage GetContractSummaries(int memberId, int branchId, bool isFreezDetailNeeded, bool isFreezdView)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                OperationResult<List<ContractSummaryDC>> result = GMSManageMembership.GetContractSummaries(memberId, branchId, ExceConnectionManager.GetGymCode(user), isFreezDetailNeeded, user, isFreezdView);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ContractSummaryDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ContractSummaryDC>(), ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpGet]
        [Route("GetContractDetails")]
        [Authorize]
        public HttpResponseMessage GetContractDetails(int contractId, int branchId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                OperationResult<MemberContractDC> result = GMSManageMembership.GetMemberContractDetails(contractId, branchId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpGet]
        [Route("GetInstallments")]
        [Authorize]
        public HttpResponseMessage GetInstallments(int branchId, int memberContractId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                OperationResult<List<InstallmentDC>> result = GMSManageMembership.GetInstallments(memberContractId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<InstallmentDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<InstallmentDC>(), ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpGet]
        [Route("GetAccessProfiles")]
        [Authorize]
        public HttpResponseMessage GetAccessProfiles(Gender gender)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                OperationResult<List<ExceAccessProfileDC>> result = GMSSystem.GetAccessProfiles(ExceConnectionManager.GetGymCode(user), gender);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExceAccessProfileDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch(Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExceAccessProfileDC>(), ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpGet]
        [Route("GetContractTemplates")]
        [Authorize]
        public HttpResponseMessage GetContractTemplates(int branchId, int contractType)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.GetContracts(branchId, user, contractType, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> exceptionMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        exceptionMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<PackageDC>(), ApiResponseStatus.ERROR, exceptionMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<PackageDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }

        [HttpPost]
        [Route("UpdateMemberInstallment")]
        [Authorize]
        public HttpResponseMessage UpdateMemberInstallment(InstallmentDC installment)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.UpdateMemberInstallment(installment, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [Route("RegisterInstallmentPayment")]
        [Authorize]
        public HttpResponseMessage RegisterInstallmentPayment(RegisterInstallmentPaymentReq request)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                
                var result = GMSPayment.RegisterInstallmentPayment(
                    request.MemberBranchID, request.LoggedbranchID, user, request.Installment, request.PaymentDetail, 
                    ExceConnectionManager.GetGymCode(user), request.SalePointID, request.SalesDetails
                    );


                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
            }
        }


        [HttpPost]
        [Route("ResignContract")]
        [Authorize]
        public HttpResponseMessage ResignContract(ResignContractRequest request)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSManageMembership.ResignContract(request.ResignDetail, ExceConnectionManager.GetGymCode(user), user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    if (request.NotificationMethod == "EVENTLOG")
                    {
                        PrintResignDocument(request.ResignDetail, user, request.BranchId);
                        return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                    }
                    if (request.NotificationMethod == "PRINT")
                    {
                        var re = PrintResignDocument(request.ResignDetail, user, request.BranchId);
                        return Request.CreateResponse(re == "PRINTERROR" ? HttpStatusCode.InternalServerError : HttpStatusCode.OK, ApiUtils.CreateApiResponse(re, ApiResponseStatus.OK));
                    }
                    else
                    {
                        var re = PrintResignDocument(request.ResignDetail, user, request.BranchId);
                        return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(re, ApiResponseStatus.OK));
                    }
                    SendResignNotification(request.ResignDetail, request.BranchId, user, request.SenderDescription, request.NotificationMethod);

                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
            }
        }

        private string PrintResignDocument(ContractResignDetailsDC resignDetails, string user, int branchId)
        {
            var memnberContractID = resignDetails.MemberContractId.ToString();
            var contractEndDate = resignDetails.ContractEndDate.Value.ToString("dd.MM.yyyy");
            var numberofOrder = "0";
            var totalOrderAmount = "0";
            var lastDueDate = "";

            if (resignDetails.RemainingInstallments.Count > 0)
            {
                numberofOrder = resignDetails.RemainingInstallments.Count.ToString();
                totalOrderAmount = resignDetails.RemainingInstallments.Sum(X => X.Amount).ToString("N");
                lastDueDate = resignDetails.RemainingInstallments.Max(X => X.DueDate).ToString("dd.MM.yyyy");
            }

            var appUrl = ConfigurationManager.AppSettings["USC_Resign_Print_App_Path"];
            var dataDictionary = new Dictionary<string, string>() { { "MemberContractID", memnberContractID }, { "ContractEndDate", contractEndDate }, { "NumberOfOrder", numberofOrder }, { "TotalOrderAmount", totalOrderAmount }, { "LastDueDate", lastDueDate }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
            var pdfFileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);
            SetPDFFileParth(resignDetails.MemberContractId, pdfFileParth, "RESG", branchId, user);

            if (!string.IsNullOrEmpty(pdfFileParth))
                return pdfFileParth;
            return "PRINTERROR";
        }

        private string GetFilePathForPdf(string application, string outputPlugging, string user, Dictionary<string, string> dataDictionary)
        {
            var filePath = string.Empty;
            try
            {
                var list = ExcecuteWithUSC(application, outputPlugging, dataDictionary, ExceConnectionManager.GetGymCode(user), user);
                foreach (var item in list)
                {
                    if (item == null) continue;
                    var message = item.Messages.First(mssge => mssge.Header.ToLower().Equals("filepath"));
                    if (message != null)
                    {
                        filePath = message.Message;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile(ex.Message, new Exception(), user);
            }
            return filePath;
        }

        private void SetPDFFileParth(int id, string fileParth, string flag, int branchId, string user)
        {
            GMSManageMembership.SetPDFFileParth(id, fileParth, flag, branchId, ExceConnectionManager.GetGymCode(user), user);
        }

        private List<IProcessingStatus> ExcecuteWithUSC(string application, string outputPlugging, Dictionary<string, string> dataDictionary, string gymCode, string user)
        {
            List<IProcessingStatus> lists = null;
            try
            {
                lists = TemplateManager.ExecuteTemplate(dataDictionary, application, outputPlugging, gymCode);
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile(ex.Message, new Exception(), user);
                throw ex;
            }
            return lists;
        }


        private int SendResignNotification(ContractResignDetailsDC resignDetails, int branchID, string user, string senderDescription, string notifyMethod)
        {
            try
            {
                // create parameter string for notification
                var application = string.Empty;
                var outputFormat = string.Empty;

                USCommonNotificationDC commonNotification = null;
                var nMethod = NotificationMethodType.NONE;
                switch (notifyMethod)
                {
                    case "SMS":
                        nMethod = NotificationMethodType.SMS;
                        application = ConfigurationManager.AppSettings["USC_Notification_SMS"];
                        outputFormat = "SMS";
                        break;
                    case "EMAIL":
                        nMethod = NotificationMethodType.EMAIL;
                        application = ConfigurationManager.AppSettings["USC_Notification_Email"];
                        outputFormat = "EMAIL";
                        break;
                }

                var numberofOrder = resignDetails.RemainingInstallments.Count;
                DateTime? lastDueDate = null;
                if (resignDetails.RemainingInstallments.Count > 0)
                {
                    decimal totalOrderAmount = resignDetails.RemainingInstallments.Sum(X => X.Amount);
                    lastDueDate = resignDetails.RemainingInstallments.Max(X => X.DueDate);

                    var paraList = new Dictionary<TemplateFieldEnum, string>
                    {
                        {TemplateFieldEnum.CONTRACTNO, resignDetails.MemberContractNo},
                        {TemplateFieldEnum.CONTRACTENDDATE, resignDetails.ContractEndDate.Value.ToShortDateString()},
                        {TemplateFieldEnum.NOOFORDERS, numberofOrder.ToString()},
                        {TemplateFieldEnum.TOTALORDERAMOUNT, totalOrderAmount.ToString()},
                        {TemplateFieldEnum.DUEDATEFORLASTORDER, lastDueDate.Value.ToShortDateString()}
                    };

                    // create common notification
                    commonNotification = new USCommonNotificationDC
                    {
                        ParameterString = paraList,
                        BranchID = branchID,
                        CreatedUser = user,
                        Role = "MEM",
                        NotificationType = NotificationTypeEnum.Messages,
                        Severity = NotificationSeverityEnum.Minor,
                        MemberID = resignDetails.MemberId,
                        Status = NotificationStatusEnum.New,
                        Method = nMethod,
                        Type = TextTemplateEnum.RESIGNSMS,
                        Title = "RESIGN CONTRACT",
                        SenderDescription = senderDescription,
                        IsTemplateNotification = true
                    };
                }
                else
                {
                    var paraList = new Dictionary<TemplateFieldEnum, string>
                    {
                        {TemplateFieldEnum.CONTRACTNO, resignDetails.MemberContractNo},
                        {TemplateFieldEnum.CONTRACTENDDATE, resignDetails.ContractEndDate.Value.ToShortDateString()},
                    };

                    // create common notification
                    commonNotification = new USCommonNotificationDC
                    {
                        ParameterString = paraList,
                        BranchID = branchID,
                        CreatedUser = user,
                        Role = "MEM",
                        NotificationType = NotificationTypeEnum.Messages,
                        Severity = NotificationSeverityEnum.Minor,
                        MemberID = resignDetails.MemberId,
                        Status = NotificationStatusEnum.New,
                        Method = nMethod,
                        Type = TextTemplateEnum.RESIGNSMSPAID,
                        Title = "RESIGN CONTRACT",
                        SenderDescription = senderDescription,
                        IsTemplateNotification = true
                    };
                }


                var result = NotificationAPI.AddNotification(commonNotification);
                if (result.ErrorOccured)
                {
                    foreach (var mgs in result.NotificationMessages.Where(mgs => mgs.ErrorLevel == ErrorLevels.Error))
                    {
                        USLogError.WriteToFile(mgs.Message, new Exception(), user);
                    }
                    return 0;
                }

                if (notifyMethod == "SMS")
                {
                    try
                    {
                        // send SMS through the USC API
                        var smsStatus = USCAPI.ExecuteTemplate(application, outputFormat, ExceConnectionManager.GetGymCode(user), result.MethodReturnValue.ToString());
                        var status = NotificationStatusEnum.Attended;
                        string statusMessage = string.Empty;
                        if (smsStatus != null)
                        {
                            status = (smsStatus.Messages.FirstOrDefault(X => X.Header == "StatusCode").Message.ToString() == "1") ? NotificationStatusEnum.Success : NotificationStatusEnum.Fail;
                            statusMessage = smsStatus.Messages.FirstOrDefault(x => x.Header == "StatusMessage").Message;
                        }

                        // update status in notification table
                        USImportResult<bool> updateResult = NotificationAPI.UpdateNotificationStatus(result.MethodReturnValue, user, status, statusMessage);

                        if (updateResult.ErrorOccured)
                        {
                            foreach (USNotificationMessage message in updateResult.NotificationMessages)
                            {
                                if (message.ErrorLevel == ErrorLevels.Error)
                                {
                                    USLogError.WriteToFile(message.Message, new Exception(), user);
                                }
                            }
                            return 0;
                        }
                    }
                    catch
                    {
                        return 0;
                    }
                }
                else if (notifyMethod == "EMAIL")
                {
                    // send EMAIL through the USC API
                    IProcessingStatus emailStatus = USCAPI.ExecuteTemplate(application, outputFormat, ExceConnectionManager.GetGymCode(user), result.MethodReturnValue.ToString());
                    NotificationStatusEnum status = NotificationStatusEnum.Attended;
                    string statusMessage = string.Empty;
                    if (emailStatus != null)
                    {
                        status = (emailStatus.Messages.FirstOrDefault(X => X.Header == "Status").Message.ToString() == "1") ? NotificationStatusEnum.Success : NotificationStatusEnum.Fail;
                        statusMessage = emailStatus.Messages.FirstOrDefault(x => x.Header == "StatusMessage").Message;
                    }

                    // update status in notification table
                    USImportResult<bool> updateResult = NotificationAPI.UpdateNotificationStatus(result.MethodReturnValue, user, status, statusMessage);

                    if (updateResult.ErrorOccured)
                    {
                        foreach (USNotificationMessage message in updateResult.NotificationMessages)
                        {
                            if (message.ErrorLevel == ErrorLevels.Error)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), user);
                            }
                        }
                        return 0;
                    }
                    return 1;
                }
                return 1;
            }
            catch (Exception e)
            {
                USLogError.WriteToFile(e.Message, new Exception(), user);
                throw e;
            }
        }

        [HttpPost]
        [Route("AddInvoicePayment")]
        [Authorize]
        public HttpResponseMessage AddInvoicePayment(RegisterInstallmentPaymentReq request)
        {
            string user = Request.Headers.GetValues("UserName").First();
            OperationResult<SaleResultDC> result = GMSPayment.RegisterInvoicePayment(request.PaymentDetail, ExceConnectionManager.GetGymCode(user), request.LoggedbranchID, request.SalesDetails, user);
            if (result.ErrorOccured)
            {
                var response = new SaleResultDC() { AritemNo = -1, InvoiceNo = string.Empty, SaleStatus = SaleErrors.ERROR };
                List<string> errorMsg = new List<string>();
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                    errorMsg.Add(message.Message);
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(response, ApiResponseStatus.ERROR, errorMsg));
               // return new SaleResultDC() { AritemNo = -1, InvoiceNo = string.Empty, SaleStatus = SaleErrors.ERROR };
            }

            foreach (var payMode in request.PaymentDetail.PayModes)
            {
                if (payMode.PaymentTypeCode == "PREPAIDBALANCE" || payMode.PaymentTypeCode == "INVOICE" || payMode.PaymentTypeCode == "ONACCOUNT" || payMode.PaymentTypeCode == "NEXTORDER")
                {
                    // create parameter string for notification
                    Dictionary<TemplateFieldEnum, string> paraList = new Dictionary<TemplateFieldEnum, string>();
                    paraList.Add(TemplateFieldEnum.DATE, DateTime.Now.ToString());
                    paraList.Add(TemplateFieldEnum.MEMBERNAME, "");
                    paraList.Add(TemplateFieldEnum.MEMBERNO, request.PaymentDetail.CustId.ToString());
                    paraList.Add(TemplateFieldEnum.AMOUNT, payMode.Amount.ToString());
                    paraList.Add(TemplateFieldEnum.PAYMENTMETHOD, payMode.PaymentType.ToString());


                    // create common notification
                    USCommonNotificationDC commonNotification = new USCommonNotificationDC();
                    commonNotification.IsTemplateNotification = true;
                    commonNotification.ParameterString = paraList;
                    commonNotification.BranchID = request.LoggedbranchID;// brnachID;
                    commonNotification.CreatedUser = user;
                    commonNotification.Role = "MEM";
                    commonNotification.NotificationType = NotificationTypeEnum.Messages;
                    commonNotification.Severity = NotificationSeverityEnum.Minor;
                    commonNotification.MemberID = request.PaymentDetail.PaidMemberId;
                    commonNotification.Status = NotificationStatusEnum.New;

                    // FOR SMS **************************************************************
                    commonNotification.Type = TextTemplateEnum.PAYMENTNOTIFICATION;
                    commonNotification.Method = NotificationMethodType.SMS;
                    commonNotification.Title = "SHOP PAYMENT NOTIFICATION";

                    //add SMS notification into notification tables

                    USImportResult<int> notification = US.Common.Notification.API.NotificationAPI.AddNotification(commonNotification);

                    if (!result.ErrorOccured)
                    {
                        string application = ConfigurationManager.AppSettings["USC_Notification_SMS"].ToString();
                        string outputFormat = "SMS";

                        try
                        {
                            // send SMS through the USC API
                            IProcessingStatus smsStatus = USCAPI.ExecuteTemplate(application, outputFormat, ExceConnectionManager.GetGymCode(user), notification.MethodReturnValue.ToString());

                            NotificationStatusEnum status = NotificationStatusEnum.Attended;

                            if (smsStatus != null)
                                status = (smsStatus.Messages.FirstOrDefault(X => X.Header == "StatusCode").Message.ToString() == "1") ? NotificationStatusEnum.Success : NotificationStatusEnum.Fail;
                            var statusMessage = smsStatus.Messages.FirstOrDefault(x => x.Header == "StatusMessage").Message;

                            // update status in notification table
                            USImportResult<bool> updateResult = NotificationAPI.UpdateNotificationStatus(notification.MethodReturnValue, user, status, statusMessage);

                            if (updateResult.ErrorOccured)
                            {
                                foreach (USNotificationMessage message in updateResult.NotificationMessages)
                                {
                                    if (message.ErrorLevel == Payment.Core.Enums.ErrorLevels.Error)
                                    {
                                        USLogError.WriteToFile(message.Message, new Exception(), user);
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            USLogError.WriteToFile(e.Message, new Exception(), user);
                        }
                    }
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
        //    return result.OperationReturnValue;
        }

        [HttpPost]
        [Route("SaveContract")]
        [Authorize]
        public HttpResponseMessage SaveMemberContract(MemberContractDC memberContract)
        {
            string user = Request.Headers.GetValues("UserName").First();
            var result = GMSManageMembership.SaveMemberContract(memberContract, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                List<string> errorMsg = new List<string>();
                var response = new ContractSaveResultDC() { MemberContractId = -1, MemberContractNo = string.Empty, IsGymChanged = false };
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                    errorMsg.Add(message.Message);

                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(response, ApiResponseStatus.ERROR, errorMsg));
            }
            return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
        }


        [HttpGet]
        [Route("ContractBookings")]
        [Authorize]
        public HttpResponseMessage GetContractBookings(int branchId, int membercontractId)
        {
            string user = Request.Headers.GetValues("UserName").First();
            var result = GMSManageMembership.GetContractBookings(membercontractId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                List<string> errorMsg = new List<string>();
                var response = new List<ContractBookingDC>();
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                    errorMsg.Add(message.Message);

                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(response, ApiResponseStatus.ERROR, errorMsg));
            }
            return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
        }

        [HttpPost]
        [Route("AddBlancInstallment")]
        [Authorize]
        public HttpResponseMessage AddBlancInstallment(int memberId, int branchId)
        {
            string user = Request.Headers.GetValues("UserName").First();
            OperationResult<int> result = GMSManageMembership.AddBlancInstallment(memberId, branchId, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                List<string> errorMsg = new List<string>();
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                    errorMsg.Add(message.Message);
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
        }

        [HttpPost]
        [Route("AddInstallment")]
        [Authorize]
        public HttpResponseMessage AddInstallment(InstallmentDC installment)
        {
            string user = Request.Headers.GetValues("UserName").First();
            OperationResult<int> result = GMSManageMembership.AddInstallment(installment, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                List<string> errorMsg = new List<string>();
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                    errorMsg.Add(message.Message);
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
        }

        [HttpPost]
        [Route("MergeOrders")]
        [Authorize]
        public HttpResponseMessage MergeOrders(MergeDetails mergeDetails)
        {
            string user = Request.Headers.GetValues("UserName").First();
            OperationResult<List<InstallmentDC>> result = GMSManageMembership.MergeOrders(mergeDetails.updatedOrders, mergeDetails.removedOrder, mergeDetails.memberContractId, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                List<string> errorMsg = new List<string>();
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                    errorMsg.Add(message.Message);
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<InstallmentDC>(), ApiResponseStatus.ERROR, errorMsg));
            }
            else
            {
                var orderNoList = (mergeDetails.updatedOrders.Aggregate(string.Empty, (current, order) => current + order.OrderNo) + ", " + mergeDetails.removedOrderNo).Trim();

                // create parameter string for notification
                var paraList = new Dictionary<TemplateFieldEnum, string>
                {
                    {TemplateFieldEnum.CONTRACTNO, mergeDetails.updatedOrders.First().MemberContractNo},
                    {TemplateFieldEnum.NOOFORDERS, (mergeDetails.removedOrder.Count + 1).ToString()},
                    {TemplateFieldEnum.ORDERNOLIST, orderNoList},
                    {TemplateFieldEnum.CREATEDUSER, user},
                    {TemplateFieldEnum.DATE, DateTime.Now.ToShortDateString()}
                };

                // create common notification
                var commonNotification = new USCommonNotificationDC();
                commonNotification.IsTemplateNotification = true;
                commonNotification.ParameterString = paraList;
                commonNotification.BranchID = mergeDetails.branchID;
                commonNotification.CreatedUser = user;
                commonNotification.Role = "MEM";
                commonNotification.NotificationType = NotificationTypeEnum.Messages;
                commonNotification.Severity = NotificationSeverityEnum.Minor;
                commonNotification.MemberID = mergeDetails.updatedOrders.First().MemberId;
                commonNotification.Status = NotificationStatusEnum.New;
                commonNotification.Type = TextTemplateEnum.MERGEORDERS;
                commonNotification.Method = NotificationMethodType.EVENTLOG;
                commonNotification.Title = "MERGE ORDERS";
                commonNotification.TempateText = string.Empty;

                //add SMS notification into notification tables
                var Logresult = NotificationAPI.AddNotification(commonNotification);
                if (Logresult.ErrorOccured)
                {
                    foreach (USNotificationMessage message in Logresult.NotificationMessages)
                    {
                        if (message.ErrorLevel == ErrorLevels.Error)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), user);
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
        }


        [HttpPost]
        [Route("UpdateContractOrder")]
        [Authorize]
        public HttpResponseMessage UpdateContractOrder(OrderUpdateDetails orderUpdateDetails)
        {
            string user = Request.Headers.GetValues("UserName").First();
            OperationResult<bool> result = GMSManageMembership.UpdateContractOrder(orderUpdateDetails.installmentList, orderUpdateDetails.branchId, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                List<string> errorMsg = new List<string>();
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
        }

        [HttpGet]
        [Route("GetMemberInvoicesForContract")]
        [Authorize]
        public HttpResponseMessage GetMemberInvoicesForContract(int memberId, int contractId, int hit)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSInvoice.GetMemberInvoicesForContract(memberId, contractId, hit, ExceConnectionManager.GetGymCode(user), user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<MemberInvoiceDC>(), ApiResponseStatus.ERROR, errorMsg));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
                }
            }
            catch (Exception ex)
            {
                List<string> errorMsg = new List<string>();
                errorMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<MemberInvoiceDC>(), ApiResponseStatus.ERROR, errorMsg));
            }
        }

        [HttpPost]
        [Route("RemoveResign")]
        [Authorize]
        public HttpResponseMessage RemoveContractResign(ResignRemoveDetails removeDetails)
        {
            string user = Request.Headers.GetValues("UserName").First();
            OperationResult<bool> result = GMSManageMembership.RemoveContractResign(removeDetails.memberContractId, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                List<string> errorMsg = new List<string>();
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                    errorMsg.Add(message.Message);
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(true, ApiResponseStatus.OK));
            }
        }

        [HttpPost]
        [Route("GenerateOrderAndSMSANDEmail")]
        [Authorize]
        public HttpResponseMessage GenerateAndSendInvoiceSMSANDEmail(SMSEmailInvoiceDetails details)
        {
            string user = Request.Headers.GetValues("UserName").First();
            Dictionary<string, bool> isSelected = new Dictionary<string, bool>();
            if (details.isEmailSelected)
                isSelected.Add("EMAIL", true);
            else
                isSelected.Add("EMAIL", false);

            if (details.isSMSselected)
                isSelected.Add("SMS", true);
            else
                isSelected.Add("SMS", false);

            Dictionary<string, string> text = new Dictionary<string, string>();
            text.Add("SMS", details.SMStext);
            text.Add("EMAIL", details.EmailText);

            Dictionary<string, string> senderDetail = new Dictionary<string, string>();
            senderDetail.Add("SMS", details.SMSNo);
            senderDetail.Add("EMAIL", details.EmailAddress);


            string invoiceType = string.Empty;
            if (isSelected["SMS"])
                invoiceType = "SMS";
            else if (isSelected["EMAIL"])
                invoiceType = "EMAIL";
            else
                invoiceType = "SMS";

            // Generate Invoice
            var newInvoice = GMSPayment.GenerateInvoice(details.invoiceBranchId, user, details.selectedOrder, details.paymentDetails, invoiceType, ExceConnectionManager.GetGymCode(user), details.memberBranchID);
            if (newInvoice.ErrorOccured)
            {
                List<string> errorMsg = new List<string>();
                foreach (NotificationMessage message in newInvoice.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                    errorMsg.Add(message.Message);
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
            }
            bool result = SendInvoiceSMSANDEmail(details.invoiceBranchId, user, newInvoice.OperationReturnValue.AritemNo, details.selectedOrder.MemberId, details.selectedOrder.PayerId, isSelected, text, senderDetail);
            return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));
        }

        [NonAction]
        public bool SendInvoiceSMSANDEmail(int branchId, string user, int arItemNo, int memberID, int guardianId, Dictionary<string, bool> isSelected, Dictionary<string, string> text, Dictionary<string, string> senderDetail)
        {
            ExcelineInvoiceDetailDC InvoiceDetails = GetInvoiceDetails(arItemNo, branchId, user);
            if (InvoiceDetails != null)
            {
                // create parameter string for notification
                Dictionary<TemplateFieldEnum, string> paraList = new Dictionary<TemplateFieldEnum, string>();
                paraList.Add(TemplateFieldEnum.INVOICENO, InvoiceDetails.InvoiceNo.ToString());
                paraList.Add(TemplateFieldEnum.DUEDATE, InvoiceDetails.InvoiceDueDate.Value.ToShortDateString());
                paraList.Add(TemplateFieldEnum.AMOUNT, InvoiceDetails.InvoiceBalance.ToString());
                paraList.Add(TemplateFieldEnum.BALANCE, InvoiceDetails.InvoiceBalance.ToString());
                paraList.Add(TemplateFieldEnum.KID, InvoiceDetails.BasicDetails.KID.ToString());
                paraList.Add(TemplateFieldEnum.GYMACCOUNT, InvoiceDetails.OtherDetails.BranchAccountNo);
                paraList.Add(TemplateFieldEnum.GYMNAME, InvoiceDetails.OtherDetails.BranchName.ToString());
                paraList.Add(TemplateFieldEnum.CREATEDUSER, user.ToString());
                paraList.Add(TemplateFieldEnum.DATE, DateTime.Now.ToShortDateString());
                paraList.Add(TemplateFieldEnum.MEMBERNAME, InvoiceDetails.CustomerName);

                // create common notification
                USCommonNotificationDC commonNotification = new USCommonNotificationDC();
                //commonNotification.IsTemplateNotification = true;
                commonNotification.ParameterString = paraList;
                commonNotification.BranchID = branchId;
                commonNotification.CreatedUser = user;
                commonNotification.Role = "MEM";
                commonNotification.NotificationType = NotificationTypeEnum.Messages;
                commonNotification.Severity = NotificationSeverityEnum.Minor;
                if (guardianId > 0)
                    commonNotification.MemberID = guardianId;
                else
                    commonNotification.MemberID = memberID;

                commonNotification.Status = NotificationStatusEnum.New;

                bool sendedresult = true;

                // if wants to send sms
                if (isSelected["SMS"])
                {
                    commonNotification.Type = TextTemplateEnum.SMSINVOICE;
                    commonNotification.Method = NotificationMethodType.SMS;
                    commonNotification.Title = "INVOICE SMS";
                    commonNotification.TempateText = text["SMS"];
                    commonNotification.SenderDescription = senderDetail["SMS"];

                    //add SMS notification into notification tables
                    USImportResult<int> result = NotificationAPI.AddNotification(commonNotification);

                    if (result.ErrorOccured)
                    {
                        foreach (USNotificationMessage message in result.NotificationMessages)
                        {
                            if (message.ErrorLevel == ErrorLevels.Error)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), user);
                            }
                        }
                        sendedresult = false;
                    }
                    else
                    {
                        string application = ConfigurationManager.AppSettings["USC_Notification_SMS"].ToString();
                        string outputFormat = "SMS";

                        try
                        {
                            // send SMS through the USC API
                            IProcessingStatus smsStatus = USCAPI.ExecuteTemplate(application, outputFormat, ExceConnectionManager.GetGymCode(user), result.MethodReturnValue.ToString());

                            NotificationStatusEnum status = NotificationStatusEnum.Attended;
                            string statusMessage = string.Empty;
                            if (smsStatus != null)
                            {
                                status = (smsStatus.Messages.FirstOrDefault(X => X.Header == "StatusCode").Message.ToString() == "1") ? NotificationStatusEnum.Success : NotificationStatusEnum.Fail;
                                statusMessage = smsStatus.Messages.FirstOrDefault(x => x.Header == "StatusMessage").Message;
                            }
                            // update status in notification table
                            USImportResult<bool> updateResult = NotificationAPI.UpdateNotificationStatus(result.MethodReturnValue, user, status, statusMessage);

                            if (updateResult.ErrorOccured)
                            {
                                foreach (USNotificationMessage message in updateResult.NotificationMessages)
                                {
                                    if (message.ErrorLevel == ErrorLevels.Error)
                                    {
                                        USLogError.WriteToFile(message.Message, new Exception(), user);
                                    }
                                }
                                sendedresult = false;
                            }
                            else
                            {
                                sendedresult = true;
                            }
                        }
                        catch (Exception e)
                        {
                            USLogError.WriteToFile(e.Message, new Exception(), user);
                            sendedresult = false;
                        }
                    }
                }

                // if wants to send email
                if (isSelected["EMAIL"])
                {
                    var filePath = ViewInvoiceDetails(branchId, user, arItemNo); // to print invoice pdf useing USC

                    commonNotification.Type = TextTemplateEnum.EMAILINVOICE;
                    commonNotification.Method = NotificationMethodType.EMAIL;
                    commonNotification.AttachmentFileParth = filePath.FileParth;
                    commonNotification.Title = "INVOICE EMAIL";
                    commonNotification.TempateText = text["EMAIL"];
                    commonNotification.SenderDescription = senderDetail["EMAIL"];

                    //add EMAIL notification into notification tables
                    USImportResult<int> result = NotificationAPI.AddNotification(commonNotification);

                    if (result.ErrorOccured)
                    {
                        foreach (USNotificationMessage message in result.NotificationMessages)
                        {
                            if (message.ErrorLevel == ErrorLevels.Error)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), user);
                            }
                        }
                        sendedresult = false;
                    }
                    else
                    {
                        string application = ConfigurationManager.AppSettings["USC_Notification_Email"].ToString();
                        string outputFormat = "EMAIL";

                        try
                        {
                            // send EMAIL through the USC API
                            IProcessingStatus emailStatus = USCAPI.ExecuteTemplate(application, outputFormat, ExceConnectionManager.GetGymCode(user), result.MethodReturnValue.ToString());
                            NotificationStatusEnum status = NotificationStatusEnum.Attended;
                            string statusMessage = string.Empty;
                            if (emailStatus != null)
                            {
                                status = (emailStatus.Messages.FirstOrDefault(X => X.Header == "Status").Message.ToString() == "1") ? NotificationStatusEnum.Success : NotificationStatusEnum.Fail;
                                statusMessage = emailStatus.Messages.FirstOrDefault(x => x.Header == "StatusMessage").Message;
                            }

                            // update status in notification table
                            USImportResult<bool> updateResult = NotificationAPI.UpdateNotificationStatus(result.MethodReturnValue, user, status, statusMessage);

                            if (updateResult.ErrorOccured)
                            {
                                foreach (USNotificationMessage message in updateResult.NotificationMessages)
                                {
                                    if (message.ErrorLevel == ErrorLevels.Error)
                                    {
                                        USLogError.WriteToFile(message.Message, new Exception(), user);
                                    }
                                }
                                sendedresult = false;
                            }
                            else
                            {
                                if (!sendedresult)
                                    sendedresult = false;
                            }
                        }
                        catch (Exception e)
                        {
                            USLogError.WriteToFile(e.Message, new Exception(), user);
                            sendedresult = false;
                        }
                    }
                }

                return sendedresult;
            }
            return false;
        }

        [NonAction]
        public ExcelineInvoiceDetailDC GetInvoiceDetails(int invoiceID, int branchId, string user)
        {
            var result = GMSInvoice.GetInvoiceDetails(invoiceID, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }

                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        [NonAction]
        public PDFPrintResultDC ViewInvoiceDetails(int branchId, string user, int arItemNo)
        {
            PDFPrintResultDC result = new PDFPrintResultDC();
            try
            {
                if (!IsBrisIntegration())
                {
                    var appUrl = ConfigurationManager.AppSettings["USC_Invoice_Print_App_Path"];
                    var dataDictionary = new Dictionary<string, string>() { { "itemID", arItemNo.ToString() }, { "gymCode", ExceConnectionManager.GetGymCode(user) } };
                    result.FileParth = GetFilePathForPdf(appUrl, "PDF", user, dataDictionary);
                    SetPDFFileParth(arItemNo, result.FileParth, "INV", branchId, user);

                    OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                    if (isVisibel.ErrorOccured)
                    {
                        foreach (NotificationMessage message in isVisibel.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), user);
                        }
                        return null;
                    }
                    result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
                }
                else
                {
                    string invoicePath = string.Empty;
                    OperationResult<string> invoicePathResult = GMSManageMembership.GetInvoicePathByARitemNo(arItemNo, ExceConnectionManager.GetGymCode(user));
                    if (invoicePathResult.ErrorOccured)
                    {
                        foreach (NotificationMessage message in invoicePathResult.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), user);
                        }
                        return null;
                    }
                    else
                    {
                        if (Path.GetExtension(invoicePathResult.OperationReturnValue.ToLower()) == ".xml")
                        {
                            string fileName = Path.GetFileNameWithoutExtension(invoicePathResult.OperationReturnValue);
                            string filePath = Path.GetDirectoryName(invoicePathResult.OperationReturnValue);
                            string saveFilePath = filePath + "\\" + fileName + ".pdf";

                            var xmlDic = ReadXmlToPdf(invoicePathResult.OperationReturnValue);
                            string htmlContent = Convert.ToString(ReadHtmlFile());
                            foreach (var item in xmlDic)
                            {
                                htmlContent = htmlContent.Replace(item.Key, item.Value);
                            }

                            string tableHtml = string.Empty;
                            var tList = ReadXmlToPdfTable(invoicePathResult.OperationReturnValue);

                            foreach (var item in tList)
                            {
                                string htmlTableBodyu = "<tr class=\"item font-m\">";
                                htmlTableBodyu = htmlTableBodyu + "<td>" + item["[[UNITDESCR]]"] + "</td>";
                                htmlTableBodyu = htmlTableBodyu + "<td class=\"value-td \">" + item["[[QUANTITY]]"] + "</td>";
                                htmlTableBodyu = htmlTableBodyu + "<td class=\"value-td \">" + item["[[PRICE]]"] + "</td>";
                                htmlTableBodyu = htmlTableBodyu + "<td class=\"value-td \">" + item["[[BELOP]]"] + "</td>";
                                htmlTableBodyu = htmlTableBodyu + "</tr>";
                                tableHtml = tableHtml + htmlTableBodyu;
                            }
                            htmlContent = htmlContent.Replace("[[tBody]]", tableHtml);
                            GeneratePDF(htmlContent, saveFilePath);

                            OperationResult<bool> savePdfPathResult = GMSManageMembership.UpdateInvoicePdfPath(arItemNo, saveFilePath, ExceConnectionManager.GetGymCode(user));
                            if (savePdfPathResult.ErrorOccured)
                            {
                                foreach (NotificationMessage message in invoicePathResult.Notifications)
                                {
                                    USLogError.WriteToFile(message.Message, new Exception(), user);
                                }
                                return null;
                            }
                            if (savePdfPathResult.OperationReturnValue)
                                invoicePath = saveFilePath;
                        }
                        else
                        {
                            invoicePath = invoicePathResult.OperationReturnValue;
                        }
                    }

                    OperationResult<Dictionary<string, object>> isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                    if (isVisibel.ErrorOccured)
                    {
                        foreach (NotificationMessage message in isVisibel.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), user);
                        }
                        return null;
                    }
                    result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
                    result.FileParth = invoicePath;
                }
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }

        [NonAction]
        private static void GeneratePDF(string htmlcontent, string saveFilePath)
        {
            try
            {
                Doc theDoc = new Doc();
                theDoc.AddImageHtml(htmlcontent);
                // theDoc.AddImageUrl("file://" + @"E:\testpage.html");
                theDoc.HtmlOptions.ContentCount = 10;
                theDoc.HtmlOptions.RetryCount = 50;
                theDoc.HtmlOptions.Timeout = 20000000;
                theDoc.HtmlOptions.AutoTruncate = true;
                theDoc.HtmlOptions.BrowserWidth = 827; //795
                theDoc.HtmlOptions.FontEmbed = true;
                theDoc.HtmlOptions.FontProtection = false;
                theDoc.HtmlOptions.FontSubstitute = false;
                theDoc.Rendering.DotsPerInch = 36;
                theDoc.Rendering.SaveQuality = 75;
                theDoc.MediaBox.String = "A4";
                theDoc.Rect.String = "A4";
                theDoc.Save(saveFilePath);
                theDoc.HtmlOptions.PageCacheClear();
                theDoc.Clear();
            }
            catch (Exception ex)
            {
            }
        }

        [NonAction]
        public bool IsBrisIntegration()
        {
            var brisIntegration = ConfigurationManager.AppSettings["BrisIntegration"];
            return brisIntegration == "1";
        }

        [NonAction]
        public static System.Text.StringBuilder ReadHtmlFile()
        {
            System.Text.StringBuilder htmlContent = new System.Text.StringBuilder();
            string line;
            try
            {
                var htmlTemplateFilePath = ConfigurationManager.AppSettings["Html_TemplateFile_Path"];
                //using (System.IO.StreamReader htmlReader = new System.IO.StreamReader(@"e:\testpage.html"))
                using (System.IO.StreamReader htmlReader = new System.IO.StreamReader(htmlTemplateFilePath))
                {

                    while ((line = htmlReader.ReadLine()) != null)
                    {
                        htmlContent.Append(line);
                    }
                }
            }
            catch (Exception objError)
            {
                throw objError;
            }

            return htmlContent;
        }


        [NonAction]
        public static Dictionary<string, string> ReadXmlToPdf(string xmlPath)
        {
            var xmlDetails = new Dictionary<string, string>();

            XmlDocument doc = new XmlDocument();
            //doc.Load(@"e:\\5908044.xml");
            doc.Load(xmlPath);

            foreach (XmlNode row in doc.SelectNodes("//Invoice"))
            {
                var selectedVal = row.SelectSingleNode("InvoiceNo").InnerText;
                xmlDetails.Add("[[INVOICENO]]", selectedVal);
            }
            foreach (XmlNode row in doc.SelectNodes("//Header"))
            {
                var selectedVal = row.SelectSingleNode("DueDate").InnerText;
                xmlDetails.Add("[[DUEDATE]]", selectedVal);

            }
            foreach (XmlNode row in doc.SelectNodes("//Header"))
            {
                var selectedVal = row.SelectSingleNode("InvoiceDate").InnerText;
                xmlDetails.Add("[[INVOICEDATE]]", selectedVal);

            }
            foreach (XmlNode row in doc.SelectNodes("//Header/Buyer"))
            {
                var buyerNo = row.SelectSingleNode("BuyerNo").InnerText;
                xmlDetails.Add("[[BUYERNO]]", buyerNo);
            }
            foreach (XmlNode row in doc.SelectNodes("//Header/PaymentInfo"))
            {
                var accountNumber = row.SelectSingleNode("AccountNumber").InnerText;
                xmlDetails.Add("[[ACCOUNTNUMBER]]", accountNumber);

                var bacsId = row.SelectSingleNode("BacsId").InnerText;
                xmlDetails.Add("[[BACSID]]", bacsId);
            }
            foreach (XmlNode row in doc.SelectNodes("//Header/BillTo"))
            {
                var billToName = row.SelectSingleNode("Name").InnerText;
                xmlDetails.Add("[[BILLTO_NAME]]", billToName);
            }
            foreach (XmlNode row in doc.SelectNodes("//Header/BillTo/AddressInfo"))
            {
                var address = row.SelectSingleNode("Address").InnerText;
                xmlDetails.Add("[[ADDRESS]]", address);
                var place = row.SelectSingleNode("Place").InnerText;
                xmlDetails.Add("[[PLACE]]", place);
                var zipCode = row.SelectSingleNode("ZipCode").InnerText;
                xmlDetails.Add("[[ZIPCODE]]", zipCode);
                
            }
            foreach (XmlNode row in doc.SelectNodes("//Summary"))
            {
                var totalInclTax = row.SelectSingleNode("TotalInclTax").InnerText;
                xmlDetails.Add("[[TOTALINCLTAX]]", totalInclTax);
            }
            return xmlDetails;
        }

        [NonAction]
        public static List<Dictionary<string, string>> ReadXmlToPdfTable(string xmlPath)
        {
            var returnDetails = new List<Dictionary<string, string>>();
            XmlDocument doc = new XmlDocument();
            doc.Load(xmlPath);

            var detailNodeList = doc.SelectNodes("//Details/Detail");
            var productNodeList = doc.SelectNodes("//Details/Detail/Products");

            for (int i = 0; i < detailNodeList.Count; i++)
            {
                var itemDic = new Dictionary<string, string>();
                var detailItem = detailNodeList[i];
                var lineTotInclTax = detailItem.SelectSingleNode("LineTotInclTax").InnerText;
                itemDic.Add("[[BELOP]]", lineTotInclTax);
                var productItem = productNodeList[i];
                var unitDescr = productItem.SelectSingleNode("SellerProductDescr").InnerText;
                itemDic.Add("[[UNITDESCR]]", unitDescr);
                var quantity = productItem.SelectSingleNode("Quantity").InnerText;
                itemDic.Add("[[QUANTITY]]", quantity);
                var price = productItem.SelectSingleNode("Price").InnerText;
                itemDic.Add("[[PRICE]]", price);

                returnDetails.Add(itemDic);

            }
            
            return returnDetails;
        }

        [HttpPost]
        [Route("UpdateContract")]
        [Authorize]
        public HttpResponseMessage UpdateContract(MemberContractDC memberContract)
        {
            string user = Request.Headers.GetValues("UserName").First();
            var result = GMSManageMembership.UpdateMemberContract(memberContract, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                List<string> errorMsg = new List<string>();
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                    errorMsg.Add(message.Message);
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
            }else
            {
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
        }

        [HttpPost]
        [Route("DeleteOrder")]
        [Authorize]
        public HttpResponseMessage DeleteInstallment(DeleteOrderDetails orderDeleteDetails) 
        {
            string user = Request.Headers.GetValues("UserName").First();
            var result = GMSManageMembership.DeleteInstallment(orderDeleteDetails.installmentIdList, orderDeleteDetails.memberContractId, ExceConnectionManager.GetGymCode(user), user);
            if (result.ErrorOccured)
            {
                List<string> errorMsg = new List<string>();
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                    errorMsg.Add(message.Message);
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(true, ApiResponseStatus.OK));
            }
        }

        [HttpGet]
        [Route("GetLastInstallment")]
        [Authorize]
        public HttpResponseMessage GetMemberContractLastInstallment(int memberContractId)
        {
            string user = Request.Headers.GetValues("UserName").First();
            var result = GMSManageMembership.GetMemberContractLastInstallment(memberContractId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                List<string> errorMsg = new List<string>();
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                    errorMsg.Add(message.Message);
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
        }

        [HttpGet]
        [Route("GetRenewTemplate")]
        [Authorize]
        public HttpResponseMessage GetRenewTemplate(int templateID, string filterType)
        {
            string user = Request.Headers.GetValues("UserName").First();
            OperationResult<PackageDC> result = GMSManageMembership.GetContractTemplateDetails(filterType, templateID, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                List<string> errorMsg = new List<string>();
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                    errorMsg.Add(message.Message);
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
        }

        [HttpPost]
        [Route("RenewMemberContract")]
        [Authorize]
        public HttpResponseMessage RenewMemberContract(ContractRenewDetails contractRenewDetails)                       
        {
            string user = Request.Headers.GetValues("UserName").First();
            var result = GMSManageMembership.RenewMemberContract(contractRenewDetails.renewedContract, contractRenewDetails.installmentList, ExceConnectionManager.GetGymCode(user), contractRenewDetails.branchId, false, user);
            if (result.ErrorOccured)
            {
                List<string> errorMsg = new List<string>();
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                    errorMsg.Add(message.Message);
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
        }

        [HttpPost]
        [Route("PriceChangeContractCount")]
        [Authorize]
        public HttpResponseMessage GetPriceChangeContractCount(PriceChangeDetails priceChangeDetails)
        {
            string user = Request.Headers.GetValues("UserName").First();
            var result = GMSManageMembership.GetPriceChangeContractCount(priceChangeDetails.dueDate, priceChangeDetails.lockInPeriod, priceChangeDetails.priceGuarantyPeriod, priceChangeDetails.minimumAmount, priceChangeDetails.templateID, priceChangeDetails.gyms, ExceConnectionManager.GetGymCode(user), priceChangeDetails.changeToNextTemplate);
            if (result.ErrorOccured)
            {
                List<string> errorMsg = new List<string>();
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                    errorMsg.Add(message.Message);
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
        }


        [HttpPost]
        [Route("UpdatePrice")]
        [Authorize]
        public HttpResponseMessage UpdatePrice(PriceChangeDetails priceChangeDetails)
        {
            string user = Request.Headers.GetValues("UserName").First();
            var result = GMSManageMembership.UpdatePrice(priceChangeDetails.dueDate, priceChangeDetails.lockInPeriod, priceChangeDetails.priceGuarantyPeriod, priceChangeDetails.minimumAmount, priceChangeDetails.templateID, priceChangeDetails.gyms, ExceConnectionManager.GetGymCode(user), priceChangeDetails.changeToNextTemplate, user);
            if (result.ErrorOccured)
            {
                List<string> errorMsg = new List<string>();
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                    errorMsg.Add(message.Message);
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
        }

        [HttpPost]
        [Route("PriceUpdateContracts")]
        [Authorize]
        public HttpResponseMessage GetPriceUpdateContracts(PriceChangeDetails priceChangeDetails)
        {
            string user = Request.Headers.GetValues("UserName").First();
            var result = GMSManageMembership.GetPriceUpdateContracts(priceChangeDetails.dueDate, priceChangeDetails.lockInPeriod, priceChangeDetails.priceGuarantyPeriod, priceChangeDetails.minimumAmount, priceChangeDetails.templateID, priceChangeDetails.gyms, ExceConnectionManager.GetGymCode(user), priceChangeDetails.changeToNextTemplate, priceChangeDetails.hit);
            if (result.ErrorOccured)
            {
                List<string> errorMsg = new List<string>();
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                    errorMsg.Add(message.Message);
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExcelineContractDC>(), ApiResponseStatus.ERROR, errorMsg));
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
        }

        [HttpGet]
        [Route("GetSponsors")]
        [Authorize]
        public HttpResponseMessage GetSponsorsforSponsoring(int branchId, string startDueDate, string endDueDate)
        {
            string user = Request.Headers.GetValues("UserName").First();
            OperationResult<List<ExcelineMemberDC>> result = GMSMembers.GetSponsorsforSponsoring(branchId, Convert.ToDateTime(startDueDate), Convert.ToDateTime(endDueDate), ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                List<string> errorMsg = new List<string>();
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                    errorMsg.Add(message.Message);
                }
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExcelineMemberDC>(), ApiResponseStatus.ERROR, errorMsg));
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
        }

        [HttpGet]
        [Route("PrintOfMemberContract")]
        [Authorize]
        public HttpResponseMessage PrintOfMemberContract( int contractId)
        {
            string user = Request.Headers.GetValues("UserName").First();
            byte[] pdfBytes = null;
            MemberContractPrint ContractPrint = new MemberContractPrint(contractId, ExceConnectionManager.GetGymCode(user));
            pdfBytes = ContractPrint.GetResult();
            if (pdfBytes == null)
            {

                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR));
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(pdfBytes, ApiResponseStatus.OK));
            }
        }


    }
}
