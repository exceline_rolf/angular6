﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Exceline.GMS.Modules.ManageMembership.RestApi.Models
{
    public class PostalData
    {
        public string postalCode { get; set; }
        public string postalName { get; set; }
        public long population { get; set; }
        public long houseHold { get; set; }
    }
}