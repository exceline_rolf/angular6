﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using US.GMS.Core.DomainObjects.ManageMemberships;

namespace US.Exceline.GMS.Modules.ManageMembership.RestApi.Models
{
    public class ContractRenewDetails
    {
        public MemberContractDC renewedContract { get; set; }
        public List<InstallmentDC> installmentList { get; set; }
        public int branchId { get; set; }
    }
}