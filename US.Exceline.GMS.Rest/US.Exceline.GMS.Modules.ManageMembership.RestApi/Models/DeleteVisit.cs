﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Exceline.GMS.Modules.ManageMembership.RestApi.Models
{
    public class DeleteVisit
    {
        public int memberVisitId { get; set; }
        public int memberContractId { get; set; }
    
    }
}