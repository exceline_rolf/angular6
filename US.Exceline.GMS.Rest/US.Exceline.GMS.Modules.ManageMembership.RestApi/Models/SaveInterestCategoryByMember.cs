﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.ManageMembership.RestApi.Models
{
    public class SaveInterestCategoryByMember
    {
        public int memberId { get; set; }
        public int branchId { get; set; }
        public List<CategoryDC> interestCategoryList { get; set; }
    }
}