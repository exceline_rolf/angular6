﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.ManageMembership.RestApi.Models
{
    public class SaveDocument
    {
        public DocumentDC Document { get; set; }
        public int BranchId { get; set; }

        public int DocId { get; set; }


    }
}