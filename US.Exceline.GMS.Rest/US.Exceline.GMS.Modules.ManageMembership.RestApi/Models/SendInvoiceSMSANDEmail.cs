﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Exceline.GMS.Modules.ManageMembership.RestApi.Models
{
    public class SendInvoiceSMSANDEmail
    {
        public int branchId { get; set; }
        public int arItemNo { get; set; }
        public int memberID { get; set; }
        public int guardianId { get; set; }
        public string invoiceRef { get; set; }
        public Dictionary<string, bool> isSelected { get; set; }
        public Dictionary<string, string> text { get; set; }
        public Dictionary<string, string> senderDetail { get; set; }
    }
}