﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using US.GMS.Core.DomainObjects.ManageMemberships;

namespace US.Exceline.GMS.Modules.ManageMembership.RestApi.Models
{
    public class MergeDetails
    {
        public List<InstallmentDC> updatedOrders { get; set; }
        public List<int> removedOrder { get; set; }
        public string removedOrderNo { get; set; }
        public int branchID { get; set; }
        public int memberContractId { get; set; }

    }
}