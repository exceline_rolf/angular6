﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Exceline.GMS.Modules.ManageMembership.RestApi.Models
{
    public class MemberEmail
    {
        public int memberID { get; set; }
        public int branchId { get; set; }
        public string message { get; set; }
        public string email { get; set; }
        public string subject { get; set; }

    }
}