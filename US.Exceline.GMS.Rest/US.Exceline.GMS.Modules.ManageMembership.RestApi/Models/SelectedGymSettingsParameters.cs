﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Exceline.GMS.Modules.ManageMembership.RestApi.Models
{
    public class SelectedGymSettingsParameters
    {
        public List<string> settingNames { get; set; }
        public bool isGymSetting { get; set; }
        public int branchId { get; set; }
    }
}