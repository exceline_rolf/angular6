﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using US.GMS.Core.DomainObjects.ManageMemberships;

namespace US.Exceline.GMS.Modules.ManageMembership.RestApi.Models
{
    public class FreezeContract
    {
        
        public ContractFreezeItemDC freezeItem { get; set; }
        public List<ContractFreezeInstallmentDC> freezeInstallments { get; set; }
        public int branchId { get; set; }
        public string notificationTitle { get; set; }
        public string notifyMethod { get; set; }
        public string senderDescription { get; set; }
        public string gymName { get; set; }


    }
}