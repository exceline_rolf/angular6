﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Exceline.GMS.Modules.ManageMembership.RestApi.Models
{
    public class MemberStatusDetails
    {
        public int StatusID { get; set; }
        public string StatusName { get; set; }
    }
}