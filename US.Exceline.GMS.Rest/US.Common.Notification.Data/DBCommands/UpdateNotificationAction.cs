﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US.Common.Notification.Core.DomainObjects;
using US_DataAccess;

namespace US.Common.Notification.Data.DBCommands
{
    class UpdateNotificationAction : USDBActionBase<bool>
    {
        private USNotification _notification = new USNotification();
        private string _loggedUser = string.Empty;
        public UpdateNotificationAction(USNotification notification)
        {
            _notification = notification;
            if (_notification != null)
            {
                _loggedUser = _notification.CreatedUser;
            }
            OverwriteUser(_loggedUser);
        }

        protected override bool Body(DbConnection connection)
        {
            bool status;
            try
            {
                string storedProcedure = "dbo.US_UpdateNotification";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@NotificationId", DbType.String, _notification.NotificationId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ModuleID", DbType.String, _notification.Module));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@NotificationSummary", DbType.String, _notification.Title));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreatedDate", DbType.Double, _notification.CreatedDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreatedUser", DbType.String, _notification.CreatedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Role", DbType.Int32, _notification.CreatedUserRoleId));

                DbParameter para3 = new SqlParameter();
                para3.DbType = DbType.Int32;
                para3.ParameterName = "@NotificationId";
                para3.Direction = ParameterDirection.Output;

                cmd.Parameters.Add(para3);

                cmd.ExecuteNonQuery();
                status = true;
            }
            catch (Exception)
            {
                status = false;
            }
            return status;
        }
    }
}
