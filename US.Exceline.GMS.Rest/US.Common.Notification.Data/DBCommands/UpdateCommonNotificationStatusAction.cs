﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US.Common.Notification.Core.Enums;
using US_DataAccess;

namespace US.Common.Notification.Data.DBCommands
{
    class UpdateCommonNotificationStatusAction : USDBActionBase<bool>
    {
        private int _notificationID = -1; 
        private string _user = string.Empty;
        private NotificationStatusEnum _status = NotificationStatusEnum.Attended;
        private string _statusMessage = string.Empty;

        public UpdateCommonNotificationStatusAction(int notificationID, string user, NotificationStatusEnum status, string statusMessage)
        {
            _notificationID = notificationID;
            _user = user;
            _status = status;
            _statusMessage = statusMessage;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {

            try
            {
                string storedProcedure = "US_NotificationCommonStatusUpdate";

                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@notificationID", DbType.Int32, _notificationID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@status", DbType.String, _status));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@statusMessage", DbType.String, _statusMessage));

                DbParameter para = new SqlParameter();
                para.DbType = DbType.Boolean;
                para.ParameterName = "@outPut";
                para.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(para);
                cmd.ExecuteNonQuery();

                return Convert.ToBoolean(para.Value);
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
