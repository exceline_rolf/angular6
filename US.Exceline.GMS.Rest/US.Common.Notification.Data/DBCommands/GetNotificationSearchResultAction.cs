﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Xml.Linq;
using US.Common.Notification.Core.DomainObjects;
using US_DataAccess;

namespace US.Common.Notification.Data.DBCommands
{
    public class GetNotificationSearchResultAction : USDBActionBase<List<USNotificationSummaryInfo>>
    {
        private string _title;
        private string _assignTo;
        private DateTime? _receiveDateFrom;
        private DateTime? _receiveDateTo;
        private DateTime? _dueDateFrom;
        private DateTime? _dueDateTo;
        private int _branchId = -1;

        private List<int> _severityIdList;
        private List<int> _typeIdList;

        private int _statusId;
        private string _loggedUser = string.Empty;

        public GetNotificationSearchResultAction(int branchId, string user, string title, string assignTo, DateTime? receiveDateFrom, DateTime? receiveDateTo, DateTime? dueDateFrom, DateTime? dueDateTo, List<int> severityIdList, List<int> typeIdList, int statusId)
        {
            this._title = title;
            _branchId = branchId;
            this._assignTo = assignTo;
            this._receiveDateFrom = receiveDateFrom;
            this._receiveDateTo = receiveDateTo;
            this._dueDateFrom = dueDateFrom;
            this._dueDateTo = dueDateTo;
            this._severityIdList = severityIdList;
            this._typeIdList = typeIdList;
            this._statusId = statusId;

            _loggedUser = user;
            OverwriteUser(_loggedUser);

        }

        protected override List<USNotificationSummaryInfo> Body(System.Data.Common.DbConnection connection)
        {
            List<USNotificationSummaryInfo> notificationSummaryList = new List<USNotificationSummaryInfo>();

            try
            {
                string storedProcedureName = "dbo.US_GetNotificationSearchResult";
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@title", DbType.String, _title));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@assignTo", DbType.String, _assignTo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@receiveDateFrom", DbType.Date, _receiveDateFrom));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@receiveDateTo", DbType.Date, _receiveDateTo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@dueDateFrom", DbType.Date, _dueDateFrom));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@dueDateTo", DbType.Date, _dueDateTo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@statusId", DbType.Int32, _statusId));

                DataTable severityIdListTable = new DataTable();
                DataColumn severityCol = null;
                severityCol = new DataColumn("Id", typeof(Int32));
                severityIdListTable.Columns.Add(severityCol);

                foreach (var severiteId in _severityIdList)
                {
                    severityIdListTable.Rows.Add(severiteId);
                }

                SqlParameter severityParameter = new SqlParameter();
                severityParameter.ParameterName = "@severityIdList";
                severityParameter.SqlDbType = System.Data.SqlDbType.Structured;
                severityParameter.Value = severityIdListTable;
                cmd.Parameters.Add(severityParameter);

                DataTable typeIdListTable = new DataTable();
                DataColumn typeCol = null;
                typeCol = new DataColumn("Id", typeof(Int32));
                typeIdListTable.Columns.Add(typeCol);

                foreach (var typeId in _typeIdList)
                {
                    typeIdListTable.Rows.Add(typeId);
                }

                SqlParameter typeParameter = new SqlParameter();
                typeParameter.ParameterName = "@typeIdList";
                typeParameter.SqlDbType = System.Data.SqlDbType.Structured;
                typeParameter.Value = typeIdListTable;
                cmd.Parameters.Add(typeParameter);


                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var notificationSummary = new USNotificationSummaryInfo();

                    notificationSummary.Id = Convert.ToInt32(reader["NotificationId"]);
                    notificationSummary.SeverityId = Convert.ToInt32(reader["SeverityId"]);
                    notificationSummary.Severity = reader["Severity"].ToString();
                    notificationSummary.TypeId = Convert.ToInt32(reader["TypeId"]);
                    notificationSummary.Type = reader["Type"].ToString();
                    notificationSummary.Title = GetTitleWithoutXML(reader["Title"].ToString());
                    notificationSummary.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
                    notificationSummary.DueDate = (reader["DueDate"] != DBNull.Value) ? Convert.ToDateTime(reader["DueDate"]) : (DateTime?)null;
                    notificationSummary.AssignTo = reader["AssignTo"].ToString();
                    notificationSummary.LastAttendDate = (reader["LastAttendDate"] != DBNull.Value) ? Convert.ToDateTime(reader["LastAttendDate"]) : (DateTime?)null;
                    notificationSummary.StatusId = Convert.ToInt32(reader["StatusId"]);
                    notificationSummary.Status = reader["Status"].ToString();

                    switch (notificationSummary.Type)
                    {
                        case "Errors":
                            notificationSummary.ErrorsIconVisibility = "Visible";
                            break;
                        case "Warning":
                            notificationSummary.WarningIconVisibility = "Visible";
                            break;
                        case "TODO":
                            notificationSummary.TodoIconVisibility = "Visible";
                            break;
                        case "Messages":
                            notificationSummary.MessagesIconVisibility = "Visible";
                            break;
                    }

                    switch (notificationSummary.Severity)
                    {
                        case "Critical":
                            notificationSummary.CriticalIconVisibility = "Visible";
                            break;
                        case "Moderate":
                            notificationSummary.ModerateIconVisibility = "Visible";
                            break;
                        case "Minor":
                            notificationSummary.MinorIconVisibility = "Visible";
                            break;
                    }

                    notificationSummaryList.Add(notificationSummary);
                }
            }
            catch
            {
                throw;
            }
            return notificationSummaryList;
        }

        private string GetTitleWithoutXML(string title)
        {
            try
            {
                string newtitle = string.Empty;
                bool hasLinks = false;

                if (title.Contains("NotificationLink"))
                {
                    hasLinks = true;
                    var checkString = title;
                    while (hasLinks)
                    {
                        var firstIndex = checkString.IndexOf("<NotificationLink>");
                        var lastIndex = checkString.IndexOf("</NotificationLink>");

                        if (firstIndex != 0)
                        {
                            var normalText = checkString.Substring(0, firstIndex);

                            newtitle += normalText;
                        }

                        var linkData = checkString.Substring(firstIndex, (lastIndex - firstIndex) + 19);
                        newtitle += GetLinkText(linkData);

                        checkString = checkString.Substring(lastIndex + 19);

                        if (!checkString.Contains("NotificationLink"))
                        {
                            hasLinks = false;

                            newtitle += checkString;
                        }
                    }

                    return newtitle;
                }
                else
                {
                    return title;
                }
            }
            catch
            {
                throw;
            }
        }

        private string GetLinkText(string xmlContent)
        {

            var doc = XDocument.Parse(xmlContent);
            var result = doc.Descendants("NotificationLink").FirstOrDefault();
            var linkText = result.Element("LinkText").Value;
            return linkText;
        }
    }
}
