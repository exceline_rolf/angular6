﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.Common.Notification.Core.DomainObjects;
using US.Common.Notification.Core.Enums;
using US_DataAccess;

namespace US.Common.Notification.Data
{
    class GetNotificationTemplateListByMethodAction : USDBActionBase<List<ExceNotificationTepmlateDC>>
    {
        private NotificationMethodType _method = new NotificationMethodType();
        private TextTemplateEnum _temp = new TextTemplateEnum();
        private string _user = string.Empty;
        private int _branchId = -1;

        public GetNotificationTemplateListByMethodAction(NotificationMethodType method, TextTemplateEnum temp, string user, int branchId)
        {
            _method     = method;
            _user       = user;
            _branchId   = branchId;
            _temp       = temp;
        }

        protected override List<ExceNotificationTepmlateDC> Body(DbConnection connection)
        {
            List<ExceNotificationTepmlateDC> notificationTemplateList = new List<ExceNotificationTepmlateDC>();

            try
            {
                string storedProcedureName = "dbo.US_GetnotificationTemplateListByMethod";

                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@method", DbType.String, _method.ToString()));
                if(!string.IsNullOrEmpty(_temp.ToString()))
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@template", DbType.String, _temp.ToString()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));

                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ExceNotificationTepmlateDC notificationTepmlate = new ExceNotificationTepmlateDC();

                    notificationTepmlate.ID         = Convert.ToInt32(reader["ID"]);
                    notificationTepmlate.Name       = Convert.ToString(reader["Name"]);
                    notificationTepmlate.TypeID     = Convert.ToInt32(reader["TypeID"]);
                    notificationTepmlate.Text       = Convert.ToString(reader["Text"]);
                    notificationTepmlate.MethodID   = Convert.ToInt32(reader["MethodID"]);

                    notificationTemplateList.Add(notificationTepmlate);
                }
            }
            catch
            {
                throw;
            }
            return notificationTemplateList;
        }
    }
}
