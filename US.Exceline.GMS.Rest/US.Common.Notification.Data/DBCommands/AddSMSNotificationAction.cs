﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US_DataAccess;

namespace US.Common.Notification.Data.DBCommands
{
    public class AddSMSNotificationAction : USDBActionBase<int>
    {
        private string _subject = string.Empty;
        private string _body = string.Empty;
        private string _mobileNo = string.Empty;
        private string _entity = string.Empty;
        private int _entityId = -1;
        private int _memberId = -1;
        private int _branchaId = -1;
        private string _user = string.Empty;

        public AddSMSNotificationAction(string subject, string body, string mobileNo, string entity, int entityId, int memberId, int branchId, string user)
        {
            _subject = subject;
            _body = body;
            _mobileNo = mobileNo;
            _entity = entity;
            _entityId = entityId;
            _memberId = memberId;
            _branchaId = branchId;
            _user = user;
        }

        protected override int Body(DbConnection connection)
        {
            try
            {
                const string storedProcedureName = "US_NotificationAddSMS";
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Subject", DbType.String, _subject));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Body", DbType.String, _body));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MobileNo", DbType.String, _mobileNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Entity", DbType.String, _entity));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EntityId", DbType.Int32, _entityId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MemberId", DbType.Int32, _memberId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchaId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _user));

                DbParameter para = new SqlParameter();
                para.DbType = DbType.Int32;
                para.ParameterName = "@notificationId";
                para.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(para);
                cmd.ExecuteNonQuery();

                return Convert.ToInt32(para.Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
