﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.Common.Notification.Core.DomainObjects;
using US_DataAccess;

namespace US.Common.Notification.Data.DBCommands
{
    class GetNotificationEmailToSendAction : USDBActionBase<List<NotificationEmail>>
    {
        private readonly int _notificationID = -1;
        private readonly List<NotificationEmail> _emailList = new List<NotificationEmail>();

        public GetNotificationEmailToSendAction(int notificationID)
        {
            _notificationID = notificationID;
        }

        protected override List<NotificationEmail> Body(DbConnection connection)
        {
            DbDataReader reader = null;
            try
            {
                const string storedProcedure = "US_GetNotificationEmailDetails";
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@notificationID", DbType.Int32, _notificationID));

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var email = new NotificationEmail
                        {
                            NotificationID  = Convert.ToInt32(reader["NotificationID"]),
                            Body            = Convert.ToString(reader["Body"]),
                            Subject         = Convert.ToString(reader["Subject"]),
                            Attachment      = Convert.ToString(reader["Attachment"]),
                            Sender          = Convert.ToString(reader["Sender"]),
                            Receiver        = Convert.ToString(reader["Receiver"]),
                            StatusCode      = "Attended"
                        };
                    _emailList.Add(email);
                }
                return _emailList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
        }
    }
}
