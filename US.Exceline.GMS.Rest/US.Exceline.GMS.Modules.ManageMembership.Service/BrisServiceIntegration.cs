﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IO.Swagger.Api;
using IO.Swagger.Model;
using Newtonsoft.Json;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.ManageMembership.Service
{
    public class BrisServiceIntegration
    {
        private BrukereOgBrukerskApi _brisInstance;
        private SkrivBrukerdataApi _skrivBrukerdataApi;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "item")]
        public List<OrdinaryMemberDC> SearchBrisMember(string apiKey, string userId, string firstName, string lastName, string email, string mobile, string birthDay, string cardNumber, string ssn)
        {
           
            _brisInstance = new BrukereOgBrukerskApi();
            var body = new BrukerSokBeanDto //search files are ok 
            {
                Brukerid = userId,
                Fornavn = firstName,
                Etternavn = lastName,
                Epostadresse = email,
                Mobiltelefon = mobile,
                Fodselsdato = birthDay,
                Kortnummer = cardNumber,
                Personnummer = ssn
            };

            var response = _brisInstance.Search(apiKey, body);
            var root = JsonConvert.DeserializeObject<RootObject>(response.ToJson());
            List<BrukerDto> result = root.Results.ToList();
            List<OrdinaryMemberDC> memberList = ConvertMemberListToOrdinaryMemberDc(result);

            return memberList;
           
            


            //List<OrdinaryMemberDC> itemList = new List<OrdinaryMemberDC>();
            //OrdinaryMemberDC item = new OrdinaryMemberDC();
            //item.FirstName = "nuwan";
            //item.LastName = "sanjeewa";
            //item.Name = item.FirstName + " " + item.LastName;
            //item.Mobile = "0712203672";
            //item.MobilePrefix = "+47";
            //item.Email = "nuwan@gmail.com";
            //item.UserId =12;
            //item.BirthDate = DateTime.Now;
            //item.Gender = Gender.MALE;
            //item.Ssn = "08576883";
            //item.ActiveStatus =true;
            //item.Language = "no";

            //OrdinaryMemberDC item1 = new OrdinaryMemberDC();
            //item1.FirstName = "nuwan1";
            //item1.LastName = "sanjeewa1";
            //item1.Mobile = "07122036721";
            //item1.MobilePrefix = "+47";
            //item1.Email = "nuwan11@gmail.com";
            //item1.UserId = 121;
            //item1.BirthDate = DateTime.Now;
            //item1.Gender = Gender.MALE;
            //item1.Ssn = "0857688311";
            //item1.ActiveStatus = true;
            //item1.Language = "no";

            //itemList.Add(item);
            //itemList.Add(item1);

            //return itemList; 

        }


        public OrdinaryMemberDC SaveBrisMember(string apiKey, OrdinaryMemberDC member)
        {
          //TODO
            var brukerDto = ConvertOrdinaryMemberDcToBrisMember(member);
            var brukerKlientRequestDto = new BrukerKlientRequestDto(brukerDto);

            var response = _skrivBrukerdataApi.UpdateBruker(apiKey, brukerKlientRequestDto);
            var root = JsonConvert.DeserializeObject<BrukerDto>(response.ToJson());
            var result = ConvertBrisMemberToOrdinaryMemberDc(root);

            return result;
        }

        private List<OrdinaryMemberDC> ConvertMemberListToOrdinaryMemberDc(List<BrukerDto> brukerDtos)
        {
            var itemList = new List<OrdinaryMemberDC>();
            foreach (var brukerDto in brukerDtos)
            {
                var item = new OrdinaryMemberDC();
                item.FirstName = brukerDto.Fornavn;
                item.LastName = brukerDto.Etternavn;
                item.Name = item.FirstName + " " + item.LastName;
                item.Mobile = brukerDto.Mobiltelefon;
                item.MobilePrefix = brukerDto.Mobilprefiks;
                item.Email = brukerDto.Epostadresse;
                item.UserId = brukerDto.BrukerId != null ? (int)brukerDto.BrukerId : -1;
                if (brukerDto.Fodselsdato != null)
                    item.BirthDate = (DateTime)brukerDto.Fodselsdato;
                item.Gender = brukerDto.Kjonn == 1 ? Gender.FEMALE : Gender.MALE;
                item.MobilePrefix = brukerDto.Mobilprefiks;
                item.Ssn = brukerDto.Fnr;
                if (brukerDto.Aktiv != null) item.ActiveStatus = (bool)brukerDto.Aktiv;
                item.Language = brukerDto.Sprak;
                item.School = brukerDto.StudiestedNavn;
                item.LastPaidSemesterFee = brukerDto.SemesteravgiftKode;
                item.AgressoId = brukerDto.AgressoId;
                itemList.Add(item);
            }

            return itemList;
        }

        private OrdinaryMemberDC ConvertBrisMemberToOrdinaryMemberDc(BrukerDto brukerDtos)
        {
            var item = new OrdinaryMemberDC();
            item.FirstName = brukerDtos.Fornavn;
            item.LastName = brukerDtos.Etternavn;
            item.Name = item.FirstName + " " + item.LastName;
            item.Mobile = brukerDtos.Mobiltelefon;
            item.MobilePrefix = brukerDtos.Mobilprefiks;
            item.Email = brukerDtos.Epostadresse;
            item.UserId = brukerDtos.BrukerId != null ? (int)brukerDtos.BrukerId : -1;
            if (brukerDtos.Fodselsdato != null)
                item.BirthDate = (DateTime)brukerDtos.Fodselsdato;
            item.Gender = brukerDtos.Kjonn == 1 ? Gender.FEMALE : Gender.MALE;
            item.MobilePrefix = brukerDtos.Mobilprefiks;
            item.Ssn = brukerDtos.Fnr;
            if (brukerDtos.Aktiv != null) item.ActiveStatus = (bool)brukerDtos.Aktiv;
            item.Language = brukerDtos.Sprak;
            item.School = brukerDtos.StudiestedNavn;
            item.LastPaidSemesterFee = brukerDtos.SemesteravgiftKode;
            item.AgressoId = brukerDtos.AgressoId;
           
            return item;
        }

        private BrukerDto ConvertOrdinaryMemberDcToBrisMember(OrdinaryMemberDC member)
        {
            var brukerDto = new BrukerDto();

            brukerDto.Fornavn = member.FirstName;
            brukerDto.Etternavn = member.LastName;
            brukerDto.Mobiltelefon = member.Mobile;
            brukerDto.Mobilprefiks = member.MobilePrefix;
            brukerDto.Kjonn = member.Gender == Gender.FEMALE ? 1 : 0;
            brukerDto.Fodselsdato = member.BirthDate;
            brukerDto.StudiestedNavn = member.School;
            brukerDto.Epostadresse = member.Email; // TODO
            brukerDto.Aktiv = member.ActiveStatus;
            brukerDto.SemesteravgiftKode = member.LastPaidSemesterFee;
            brukerDto.Fnr = member.Ssn;
            brukerDto.AgressoId = member.AgressoId;

            return brukerDto;
        }
    }
}
