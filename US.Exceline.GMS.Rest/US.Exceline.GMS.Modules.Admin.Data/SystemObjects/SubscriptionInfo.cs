﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.Exceline.GMS.Modules.Admin.Data.SystemObjects
{
    [DataContract]
    public class SubscriptionInfo
    {
        private int _moduleId;
        [DataMember]
        public int ModuleId
        {
            get { return _moduleId; }
            set { _moduleId = value; }
        }

        private int _featureId;
        [DataMember]
        public int FeatureId
        {
            get { return _featureId; }
            set { _featureId = value; }
        }

        private int _operationId;
        [DataMember]
        public int OperationId
        {
            get { return _operationId; }
            set { _operationId = value; }
        }

        private int _subOperationId;
        [DataMember]
        public int SubOperationId
        {
            get { return _subOperationId; }
            set { _subOperationId = value; }
        }

        private string _displayName;
        [DataMember]
        public string DisplayName
        {
            get { return _displayName; }
            set { _displayName = value; }
        }

        public DateTime _firstSubbedDate;
        [DataMember]
        public DateTime FirstSubbedDate
        {
            get { return _firstSubbedDate; }
            set { _firstSubbedDate = value; }
        }

        public bool _isActive;
        [DataMember]
        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }

        public bool _isBasic;
        [DataMember]
        public bool IsBasic
        {
            get { return _isBasic; }
            set { _isBasic = value; }
        }

        public bool _adminOnly;
        [DataMember]
        public bool AdminOnly
        {
            get { return _adminOnly; }
            set { _adminOnly = value; }
        }

    }
}