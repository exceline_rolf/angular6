﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : DKA
// Created Timestamp : "10/20/2013 8:25:25 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US.Exceline.GMS.Modules.Admin.Data.SystemObjects;
using US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.DomainObjects.Common;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.ManageJobs
{
    public class JobFacade
    {
        private static IAdminDataAdapter GetDataAdapter()
        {
            return new SQLServerAdminDataAdapter();
        }

        public static int SaveExcelineJob(ScheduleItemDC scheduleItem, int branchId, string gymCode)
        {
            return GetDataAdapter().SaveExcelineJob(scheduleItem,branchId,gymCode);
        }

        public static List<ScheduleItemDC> GetJobScheduleItems(int branchId, string gymCode)
        {
            return GetDataAdapter().GetJobScheduleItems(branchId, gymCode);
        }

        public static int DeleteScheduleItem(List<int> scheduleItemIdList, string gymCode)
        {
            return GetDataAdapter().DeleteScheduleItem(scheduleItemIdList, gymCode);
        }

        public static List<ExcelineCommonTaskDC> GetAllTasksByEmployeeId(int branchId,int employeeId, string gymCode, string user)
        {
            return GetDataAdapter().GetAllTasksByEmployeeId(branchId, employeeId, gymCode,user);
        }


        public static List<ExcelineCommonTaskDC> GetFollowUpByEmployeeId(int branchId, int employeeId, string gymCode, string user, int hit)
        {
            return GetDataAdapter().GetFollowUpByEmployeeId(branchId, employeeId, gymCode, user, hit);
        }



        public static List<ExcelineCommonTaskDC> GetJobsByEmployeeId(int branchId, int employeeId, string gymCode, string user, int hit)
        {
            return GetDataAdapter().GetJobsByEmployeeId(branchId, employeeId, gymCode, user, hit);
        }


        public static int AssignTaskToEmployee(ExcelineCommonTaskDC commonTask, int employeeId, string gymCode, string user)
        {
            return GetDataAdapter().AssignTaskToEmployee(commonTask, employeeId, gymCode, user);
        }

    }
}
