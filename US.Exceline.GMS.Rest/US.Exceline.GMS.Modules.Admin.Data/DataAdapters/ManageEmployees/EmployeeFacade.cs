﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US.Exceline.GMS.Modules.Admin.Data.SystemObjects;
using US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.ManageEmployees
{
    public class EmployeeFacade
    {
        private static IAdminDataAdapter GetDataAdapter()
        {
            return new SQLServerAdminDataAdapter();
        }

        public static string UpdateEmployees(EmployeeDC employeeDc, string user, string gymCode)
        {
            return GetDataAdapter().UpdateEmployees(employeeDc, user, gymCode);
        }

        public static string UpdateMemeberActiveness(int trainerId, string rolId, int branchId, string user, bool isActive, string gymCode)
        {
            return GetDataAdapter().UpdateMemeberActiveness(trainerId, rolId, branchId, user, isActive, gymCode);
        }

        public static List<ExcelineTaskDC> GetEntityTasks(int employeeId, int branchId, string entityRoleType, string gymCode)
        {
            return GetDataAdapter().GetEntityTasks(employeeId, branchId, entityRoleType, gymCode);
        }

        public static bool SaveEntityTimeEntry(TimeEntryDC timeEntry, string gymCode)
        {
            return GetDataAdapter().SaveEntityTimeEntry(timeEntry, gymCode);
        }

        public static List<TimeEntryDC> GetEntityTimeEntries(DateTime startDate, DateTime endDate, int entityId, string entityRoleType, string gymCode)
        {
            return GetDataAdapter().GetEntityTimeEntries(startDate, endDate, entityId, entityRoleType, gymCode);
        }

        public static bool DeleteEntityTimeEntry(int entityTimeEntryId, int taskId, decimal timeSpent, string gymCode)
        {
            return GetDataAdapter().DeleteEntityTimeEntry(entityTimeEntryId, taskId, timeSpent, gymCode);
        }

        public static List<CategoryDC> GetTaskStatusCategories(int branchId, string gymCode)
        {
            return GetDataAdapter().GetTaskStatusCategories(branchId,gymCode);
        }

        public static bool SaveEmployeesRoles(EmployeeDC employeeDc, string gymCode)
        {
            return GetDataAdapter().SaveEmployeesRoles(employeeDc, gymCode);
        }

        public static List<RoleActivityDC> GetActivitiesForRoles(int memberId, string gymCode)
        {
            return GetDataAdapter().GetActivitiesForRoles(memberId, gymCode);
        }

        public static bool DeleteGymEmployee(int memberId,int assignEmpId, string user, string gymCode)
        {
            return GetDataAdapter().DeleteGymEmployee(memberId, assignEmpId,user, gymCode);
        }
        public static List<EmployeeClass> GetEmployeeClasses(int empId, string gymCode)
        {
            return GetDataAdapter().GetEmployeeClasses(empId, gymCode);
        }

        public static List<DateTime> GetClassDateByEmployee(int scheduleItemId, string gymCode)
        {
            return GetDataAdapter().GetClassDateByEmployee(scheduleItemId, gymCode);
        }


        public static int UpdateApproveStatus(bool IsApproved, int EntityActiveTimeID, string user)
        {
            return GetDataAdapter().UpdateApproveStatus(IsApproved, EntityActiveTimeID, user);
        }

        public static List<ExcelineRoleDc> GetGymEmployeeRolesById(int employeeId, int branchId, string user)
        {
            return GetDataAdapter().GetGymEmployeeRolesById(employeeId, branchId, user);
        }
    }
}
