﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetFollowUpCategoryTasksByTemplateIdAction : USDBActionBase<List<FollowUpTemplateTaskDC>>
    {
        private readonly int _followUpTemplateId;

        public GetFollowUpCategoryTasksByTemplateIdAction(int followUpTemplateId)
        {
            _followUpTemplateId = followUpTemplateId;
        }

        protected override List<FollowUpTemplateTaskDC> Body(DbConnection connection)
        {
            List<FollowUpTemplateTaskDC> excilineTaskCategoryList = new List<FollowUpTemplateTaskDC>();

            const string spName2 = "USExceGMSAdminGetFollowUpTasks";
            try
            {
                DbCommand command = CreateCommand(CommandType.StoredProcedure, spName2);
                command.Parameters.Add(DataAcessUtils.CreateParam("@followupTemplateId", DbType.String, _followUpTemplateId));
                DbDataReader reader2 = command.ExecuteReader();

                while (reader2.Read())
                {
                    FollowUpTemplateTaskDC followUpTask = new FollowUpTemplateTaskDC();
                    followUpTask.FollowUpTemplateId = Convert.ToInt32(reader2["FollowUpTemplateId"]);
                    followUpTask.TaskCategoryId = Convert.ToInt32(reader2["TaskCategoryId"]);
                    followUpTask.NumberOfDays = Convert.ToInt32(reader2["NumberOfDays"]);
                    followUpTask.IsNextFollowUpValue = Convert.ToBoolean(reader2["IsNextFollowUpValue"]);
                    followUpTask.Text = Convert.ToString(reader2["Text"]);
                    followUpTask.ExcelineTaskCategory = new ExcelineTaskCategoryDC();
                    followUpTask.ExcelineTaskCategory.Id = Convert.ToInt32(reader2["ID"]);
                    followUpTask.ExcelineTaskCategory.Name = Convert.ToString(reader2["Name"]);
                    followUpTask.ExcelineTaskCategory.IsAssignToEmp = Convert.ToBoolean(reader2["IsAssignToEmp"]);
                    followUpTask.ExcelineTaskCategory.IsAssignToRole = Convert.ToBoolean(reader2["IsAssignToRole"]);
                    followUpTask.ExcelineTaskCategory.IsStartDate = Convert.ToBoolean(reader2["IsStartDate"]);
                    followUpTask.ExcelineTaskCategory.IsEndDate = Convert.ToBoolean(reader2["IsEndDate"]);
                    followUpTask.ExcelineTaskCategory.IsDueTime = Convert.ToBoolean(reader2["IsDueTime"]);
                    followUpTask.ExcelineTaskCategory.IsStartTime = Convert.ToBoolean(reader2["IsStartTime"]);
                    followUpTask.ExcelineTaskCategory.IsEndTime = Convert.ToBoolean(reader2["IsEndTime"]);
                    followUpTask.ExcelineTaskCategory.IsFoundDate = Convert.ToBoolean(reader2["IsFoundDate"]);
                    followUpTask.ExcelineTaskCategory.IsReturnDate = Convert.ToBoolean(reader2["IsReturnDate"]);
                    followUpTask.ExcelineTaskCategory.IsDueDate = Convert.ToBoolean(reader2["IsDueDate"]);
                    followUpTask.ExcelineTaskCategory.IsNoOfDays = Convert.ToBoolean(reader2["IsNoOfDays"]);
                    followUpTask.ExcelineTaskCategory.IsPhoneNo = Convert.ToBoolean(reader2["IsPhoneNo"]);
                    followUpTask.ExcelineTaskCategory.IsNextFollowUp = Convert.ToBoolean(reader2["IsNextFollowUp"]);
                    followUpTask.ExcelineTaskCategory.IsAutomatedSMS = Convert.ToBoolean(reader2["IsAutomatedSMS"]);
                    followUpTask.ExcelineTaskCategory.IsAutomatedEmail = Convert.ToBoolean(reader2["IsAutomatedEmail"]);
                    followUpTask.ExcelineTaskCategory.CreatedDate = Convert.ToDateTime(reader2["CreatedDate"]);
                    followUpTask.ExcelineTaskCategory.LastModifiedDate = Convert.ToDateTime(reader2["LastModifiedDate"]);
                    followUpTask.ExcelineTaskCategory.CreatedUser = Convert.ToString(reader2["CreatedUser"]);
                    followUpTask.ExcelineTaskCategory.LastModifiedUser = Convert.ToString(reader2["LastModifiedUser"]);
                    followUpTask.ExcelineTaskCategory.IsDefault = Convert.ToBoolean(reader2["IsDefault"]);
                    excilineTaskCategoryList.Add(followUpTask);
                }
                reader2.Close();
                foreach (var item in excilineTaskCategoryList)
                {
                    item.ExcelineTaskCategory.ExtendedFieldsList = GetTaskTemplateExtendedFieldsAction(item.TaskCategoryId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return excilineTaskCategoryList;
        }

        public List<ExtendedFieldDC> GetTaskTemplateExtendedFieldsAction(int taskCategoryId)
        {
            List<ExtendedFieldDC> extFieldTemplateList = new List<ExtendedFieldDC>();
            const string storedProcedureName = "USExceGMSAdminGetTaskCategoryExtFields";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CategoryId", DbType.Int32, taskCategoryId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CategoryType", DbType.String, "FOLLOWUP"));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedFieldDC extTaskTemplate = new ExtendedFieldDC();
                    extTaskTemplate.Id = Convert.ToInt32(reader["Id"]);
                    extTaskTemplate.Title = reader["Name"].ToString();
                    extTaskTemplate.FieldType = (CommonUITypes)Enum.Parse(typeof(CommonUITypes), reader["FieldType"].ToString(), true);
                    extFieldTemplateList.Add(extTaskTemplate);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return extFieldTemplateList;
        }
    }
}
