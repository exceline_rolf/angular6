﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    class GetClassDateByEmployeeAction : USDBActionBase<List<DateTime>>
    {
        private int _scheduleItemId = -1;

        public GetClassDateByEmployeeAction(int scheduleItemId)
        {
            _scheduleItemId = scheduleItemId;
        }
        protected override List<DateTime> Body(System.Data.Common.DbConnection connection)
        {
            List<DateTime> dateList = new List<DateTime>();
            const string storedProcedureName = "GetClassDateByEmployee";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleItemId", DbType.Int32, _scheduleItemId));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    dateList.Add(Convert.ToDateTime(reader["StartDateTime"]));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dateList;
        }
    }
}
