﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer
{
    class GetSystemSettingEconomySettingAction : USDBActionBase<SystemSettingEconomySettingDC>
    {
        public GetSystemSettingEconomySettingAction() { }

        protected override SystemSettingEconomySettingDC Body(DbConnection connection)
        {
            SystemSettingEconomySettingDC SystemSettingEconomySetting = new SystemSettingEconomySettingDC();
            try
            {
                const string storedProcedure = "USExceGMSAdminGetSystemSettingEconomySetting";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    SystemSettingEconomySetting.ID = Convert.ToInt32(reader["ID"]);
                    SystemSettingEconomySetting.CreditPeriod = Convert.ToString(reader["CreditPeriod"]);
                    SystemSettingEconomySetting.OneDueDateAtDate = Convert.ToBoolean(reader["OneDueDateAtDate"]);
                    SystemSettingEconomySetting.FixedDueDay = Convert.ToString(reader["FixedDueDay"]);
                    SystemSettingEconomySetting.SponsoringDay = Convert.ToString(reader["SponsoringDay"]);
                    SystemSettingEconomySetting.SponsoredDueDate = Convert.ToString(reader["SponsoredDueDate"]);
                    if (reader["SponsorDueDay"] != DBNull.Value)
                    {
                        SystemSettingEconomySetting.SponsorDueDay = Convert.ToInt32(reader["SponsorDueDay"]);
                    }
                    else
                    {
                      
                    }
                    SystemSettingEconomySetting.SponsorInvoiceText = Convert.ToString(reader["SponsorInvoiceText"]);
                    SystemSettingEconomySetting.ContractAutoRenewalDays = Convert.ToString(reader["ContractAutoRenewalDays"]);
                    SystemSettingEconomySetting.IsSMSInvoice = Convert.ToBoolean(reader["IsSMSInvoice"]);
                    SystemSettingEconomySetting.DeviationFollowup = Convert.ToBoolean(reader["DeviationFollowUp"]);
                    SystemSettingEconomySetting.DeviationFollowupCompleted = Convert.ToBoolean(reader["DeviationFollowUpCompleted"]);
                    SystemSettingEconomySetting.ReminderDueDays = Convert.ToInt32(reader["ReminderDueDays"]);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return SystemSettingEconomySetting;
        }
    }
}
