﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.AccessControl.ARX;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class SaveArxSettingDetailAction : USDBActionBase<int>
    {
        private ExceArxFormatDetail _arxFormatDetail;
        private readonly int _branchId;
        private readonly string _user = string.Empty;
        private readonly int _tempCodeDeleteDate;
        private readonly string _accessControlTypes;
        public SaveArxSettingDetailAction(ExceArxFormatDetail arxFormatDetail, int tempCodeDeleteDate, int branchId, string user, string accessControlTypes)
        {
            _arxFormatDetail = arxFormatDetail;
            _branchId = branchId;
            _tempCodeDeleteDate = tempCodeDeleteDate;
            _user = user;
            _accessControlTypes = accessControlTypes;

        }

        protected override int Body(DbConnection connection)
        {
            int result = -1;
            const string storedProcedureName = "USExceGMSSaveArxSettingDetail";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                if (_arxFormatDetail != null)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, _arxFormatDetail.Id));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@FormatTypeId", DbType.String, _arxFormatDetail.FormatTypeId));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@MinLength", DbType.Int32, _arxFormatDetail.MinLength));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@MaxLength", DbType.Int32, _arxFormatDetail.MaxLength));
                }

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TempCodeDeleteDate", DbType.Int32, _tempCodeDeleteDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreatedUser", DbType.String, _user));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AccessControlTypes", DbType.String, _accessControlTypes));


                DbParameter output = new SqlParameter();
                output.DbType = DbType.Int32;
                output.ParameterName = "@outId";
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);

                cmd.ExecuteNonQuery();
                result = Convert.ToInt32(output.Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}
