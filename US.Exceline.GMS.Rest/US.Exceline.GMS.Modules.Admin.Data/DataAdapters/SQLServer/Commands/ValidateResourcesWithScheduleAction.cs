﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    class ValidateResourcesWithScheduleAction : USDBActionBase<string>
    {
        private string _roleType = string.Empty;
        private AvailableResourcesDC _resource = new AvailableResourcesDC();
        private int _branchId = -1;

        public ValidateResourcesWithScheduleAction(string roleType, AvailableResourcesDC resource, int branchId)
        {
            _roleType = roleType;
            _resource = resource;
            _branchId = branchId;
        }

        protected override string Body(DbConnection connection)
        {
            string storedProcedureName = "USExceGMSAdminValidateResourcesWithSchedule";
            string scheduleId = string.Empty;

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@resourceId", DbType.Int32, _resource.ResourID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@day", DbType.String, _resource.Day));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@fromTime", DbType.DateTime, _resource.FromTime));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@toTime", DbType.DateTime, _resource.ToTime));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@roleType", DbType.String, _roleType));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    scheduleId = Convert.ToString(reader["ScheduleId"]); break;
                }
            }
            catch (Exception)
            {
            }
            return scheduleId;
        }
    }
}
