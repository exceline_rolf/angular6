﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Common;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetFollowUpTemplatesAction : USDBActionBase<List<FollowUpTemplateDC>>
    {
        private readonly int _branchId;
        public GetFollowUpTemplatesAction(int branchId)
        {
            _branchId = branchId;
        }

        protected override List<FollowUpTemplateDC> Body(DbConnection connection)
        {
            List<FollowUpTemplateDC> followUpTemplatesList = new List<FollowUpTemplateDC>();
            const string spName = "USExceGMSAdminGetExceFollowUpTemplates";
            try
            {
                DbCommand command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                DbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    FollowUpTemplateDC followUpTemplate = new FollowUpTemplateDC();
                    followUpTemplate.Id = Convert.ToInt32(reader["Id"]);
                    followUpTemplate.Name = Convert.ToString(reader["Name"]);
                    followUpTemplate.IsDefault = Convert.ToBoolean(reader["IsDefault"]);
                    followUpTemplate.Category = new CategoryDC();
                    followUpTemplate.Category.Id = Convert.ToInt32(reader["CatId"]);
                    followUpTemplate.Category.Name = Convert.ToString(reader["CatName"]);
                    if (reader["CreatedUser"] != DBNull.Value)
                        followUpTemplate.CreatedUser = Convert.ToString(reader["CreatedUser"]);
                    if (reader["CreatedDate"] != DBNull.Value)
                        followUpTemplate.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
                    followUpTemplatesList.Add(followUpTemplate);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //List<FollowUpTemplateTaskDC> followUpTasksList = new List<FollowUpTemplateTaskDC>();
            //const string spName2 = "USExceGMSAdminGetFollowUpTasks";
            //try
            //{
            //    DbCommand command2 = CreateCommand(System.Data.CommandType.StoredProcedure, spName2);
            //    DbDataReader reader2 = command2.ExecuteReader();

            //    while (reader2.Read())
            //    {
            //        FollowUpTemplateTaskDC followUpTask = new FollowUpTemplateTaskDC();
            //        followUpTask.FollowUpTemplateId = Convert.ToInt32(reader2["FollowUpTemplateId"]);
            //        followUpTask.TaskCategoryId = Convert.ToInt32(reader2["TaskCategoryId"]);
            //        followUpTask.NumberOfDays = Convert.ToInt32(reader2["NumberOfDays"]);
            //        followUpTask.IsNextFollowUpValue = Convert.ToBoolean(reader2["IsNextFollowUp"]);
            //        followUpTask.ExcelineTaskCategory = new ExcelineTaskCategoryDC();
            //        followUpTask.ExcelineTaskCategory.Id = Convert.ToInt32(reader2["ID"]);
            //        followUpTask.ExcelineTaskCategory.Name = Convert.ToString(reader2["Name"]);
            //        followUpTask.ExcelineTaskCategory.IsAssignTo = Convert.ToBoolean(reader2["IsAssignTo"]);
            //        followUpTask.ExcelineTaskCategory.IsStartDate = Convert.ToBoolean(reader2["IsStartDate"]);
            //        followUpTask.ExcelineTaskCategory.IsEndDate = Convert.ToBoolean(reader2["IsEndDate"]);
            //        followUpTask.ExcelineTaskCategory.IsDueTime = Convert.ToBoolean(reader2["IsDueTime"]);
            //        followUpTask.ExcelineTaskCategory.IsStartTime = Convert.ToBoolean(reader2["IsStartTime"]);
            //        followUpTask.ExcelineTaskCategory.IsEndTime = Convert.ToBoolean(reader2["IsEndTime"]);
            //        followUpTask.ExcelineTaskCategory.IsFoundDate = Convert.ToBoolean(reader2["IsFoundDate"]);
            //        followUpTask.ExcelineTaskCategory.IsReturnDate = Convert.ToBoolean(reader2["IsReturnDate"]);
            //        followUpTask.ExcelineTaskCategory.IsDueDate = Convert.ToBoolean(reader2["IsDueDate"]);
            //        followUpTask.ExcelineTaskCategory.IsNoOfDays = Convert.ToBoolean(reader2["IsNoOfDays"]);
            //        followUpTask.ExcelineTaskCategory.IsPhoneNo = Convert.ToBoolean(reader2["IsPhoneNo"]);
            //        followUpTask.ExcelineTaskCategory.IsSms = Convert.ToBoolean(reader2["IsSms"]);
            //        followUpTask.ExcelineTaskCategory.IsFollowupMember = Convert.ToBoolean(reader2["IsFollowupMember"]);
            //        followUpTask.ExcelineTaskCategory.IsBelongsToMember = Convert.ToBoolean(reader2["IsBelongsToMember"]);
            //        followUpTask.ExcelineTaskCategory.IsNextFollowUp = Convert.ToBoolean(reader2["IsNextFollowUp"]);
            //        followUpTask.ExcelineTaskCategory.IsAutomatedSMS = Convert.ToBoolean(reader2["IsAutomatedSMS"]);
            //        followUpTask.ExcelineTaskCategory.IsAutomatedEmail = Convert.ToBoolean(reader2["IsAutomatedEmail"]);
            //        followUpTask.ExcelineTaskCategory.CreatedDate = Convert.ToDateTime(reader2["CreatedDate"]);
            //        followUpTask.ExcelineTaskCategory.LastModifiedDate = Convert.ToDateTime(reader2["LastModifiedDate"]);
            //        followUpTask.ExcelineTaskCategory.CreatedUser = Convert.ToString(reader2["CreatedUser"]);
            //        followUpTask.ExcelineTaskCategory.LastModifiedUser = Convert.ToString(reader2["LastModifiedUser"]);
            //        followUpTask.ExcelineTaskCategory.IsDefault = Convert.ToBoolean(reader2["IsDefault"]);
            //        followUpTasksList.Add(followUpTask);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}

            //foreach (FollowUpTemplateDC template in followUpTemplatesList)
            //{
            //    foreach (FollowUpTemplateTaskDC task in followUpTasksList)
            //    {
            //        if (template.Id == task.FollowUpTemplateId)
            //        {
            //            template.FollowUpTaskList.Add(task);
            //        }
            //    }
            //}
            return followUpTemplatesList;
        }
    }
}
