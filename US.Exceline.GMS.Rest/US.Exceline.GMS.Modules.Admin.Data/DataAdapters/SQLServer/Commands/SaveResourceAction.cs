﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US.GMS.Core.DomainObjects.Common;
using US_DataAccess;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using System.Data.SqlClient;
using US.GMS.Core.DomainObjects.ManageContracts;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class SaveResourceAction : USDBActionBase<int>
    {
        private ResourceDC _resource;
        private DataTable _dataTable = null;
        public SaveResourceAction(ResourceDC resource)
        {
            _resource = resource;
            _resource = resource;
            if (resource.EmpList != null && resource.EmpList.Count > 0)
                _dataTable = GetEmpList(_resource.EmpList);
        }

        private DataTable GetEmpList(Dictionary<int,string> empLst)
        {
            _dataTable = new DataTable();
            _dataTable.Columns.Add(new DataColumn("ID", Type.GetType("System.Int32")));
            foreach (var item in empLst)
            {
                DataRow dataTableRow = _dataTable.NewRow();
                dataTableRow["ID"] = item.Key;
                _dataTable.Rows.Add(dataTableRow);
            }
            return _dataTable;
        }

        //private DataTable GetActivityItemLst(List<ActivityDC> activityList)
        //{
        //    _dataTable = new DataTable();
        //    _dataTable.Columns.Add(new DataColumn("ActivityId", Type.GetType("System.Int32")));
        //    foreach (ActivityDC activityItem in activityList)
        //    {
        //        DataRow _dataTableRow = _dataTable.NewRow();
        //        _dataTableRow["ActivityId"] = activityItem.Id;
        //        _dataTable.Rows.Add(_dataTableRow);
        //    }
        //    return _dataTable;
        //}

        protected override int Body(DbConnection connection)
        {
            int resourceId = -1;
            string StoredProcedureName = "USExceGMSAdminAddResource";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, _resource.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchid", DbType.Int32, _resource.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@name", DbType.String, _resource.Name));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@description", DbType.String, _resource.Description));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@createduser", DbType.String, _resource.CreatedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@lastmodifieduser", DbType.String, _resource.LastModifiedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@activestate", DbType.Boolean, _resource.ActiveStatus));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ArticleId", DbType.Int32, _resource.ArticleId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ArticleSettingId", DbType.Int32, _resource.ArticleSettingId));
                if (!_resource.PurchasedDate.Equals(DateTime.MinValue))
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@purchasedDate", DbType.DateTime, _resource.PurchasedDate));
                if (!_resource.MaintenanceDate.Equals(DateTime.MinValue))
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@maintenanceDate", DbType.DateTime, _resource.MaintenanceDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@categoryId", DbType.Int32, _resource.ResourceCategory.Id));
                if (_resource.TimeCategory != null)
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TimeCategoryId", DbType.Int32, _resource.TimeCategory.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SalaryPerBooking", DbType.Int32, _resource.IsSalaryPerBooking));
                if (_resource.ActivityId > 0)
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ActivityId", DbType.Int32, _resource.ActivityId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EquipmentId", DbType.Int32, _resource.EquipmentId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ImagePath", DbType.String, _resource.ImagePath));
                if (_resource.IsSwitchRes && _resource.SwitchResId > 0)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@SwitchResId", DbType.Int32, _resource.SwitchResId));
                else if (_resource.CancelBooking)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@CancelBooking", DbType.Boolean, _resource.CancelBooking));
                //if (_resource.ActivityList != null)
                //{
                //    cmd.Parameters.Add(DataAcessUtils.CreateParam("@activityItemList", SqlDbType.Structured, _dataTable));
                //}
                //else
                //{
                //    cmd.Parameters.Add(DataAcessUtils.CreateParam("@activityItemList", SqlDbType.Structured, null));
                //}

                cmd.Parameters.Add(_resource.EmpList != null
                                       ? DataAcessUtils.CreateParam("@EmpList", SqlDbType.Structured,
                                                                    _dataTable)
                                       : DataAcessUtils.CreateParam("@EmpList", SqlDbType.Structured, null));

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@resourceId";
                param.SqlDbType = SqlDbType.Int;
                param.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(param);
                cmd.ExecuteNonQuery();
                resourceId = Convert.ToInt32(param.Value);
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return resourceId;
        }
    }
}
