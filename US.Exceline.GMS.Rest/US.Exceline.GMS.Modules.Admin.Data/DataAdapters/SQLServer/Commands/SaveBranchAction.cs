﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "5/29/2012 14:52:37
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using US_DataAccess;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Admin;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class SaveBranchAction : USDBActionBase<int>
    {
        private ExcelineBranchDC _branch;

        public SaveBranchAction(ExcelineBranchDC branch)
        {
            this._branch = branch;
        }

        protected override int Body(DbConnection connection)
        {
            DbTransaction transaction = null;

            ExcelineCreditorDC creditor = new ExcelineCreditorDC
            {
                CreditorEntNo = 0,
                CreditorGroupId = 1, //add Default group
                FirstName = _branch.BranchName,
                BornDate = _branch.RegisteredDate,
                Addr1 = (!String.IsNullOrEmpty(_branch.Addr1) ? _branch.Addr1 : String.Empty),
                Addr2 = (!String.IsNullOrEmpty(_branch.Addr2) ? _branch.Addr2 : String.Empty),
                Addr3 = (!String.IsNullOrEmpty(_branch.Addr3) ? _branch.Addr3 : String.Empty),
                ZipCode = (!String.IsNullOrEmpty(_branch.ZipCode) ? _branch.ZipCode : String.Empty),
                ZipName = (!String.IsNullOrEmpty(_branch.ZipName) ? _branch.ZipName : String.Empty),
                CreditorInkassoId = _branch.CreditorCollectionId,
                CountryId = _branch.CountryId,
                Region = _branch.Region,
                TelWork = (!String.IsNullOrEmpty(_branch.TelWork) ? _branch.TelWork : String.Empty),
                Fax = (!String.IsNullOrEmpty(_branch.Fax) ? _branch.Fax : String.Empty),
                Email = (!String.IsNullOrEmpty(_branch.Email) ? _branch.Email : String.Empty),
                AccountNo = (!String.IsNullOrEmpty(_branch.BankAccountNo) ? _branch.BankAccountNo : String.Empty),
                KidSwapAccountNo = (!String.IsNullOrEmpty(_branch.KidSwapAccountNo) ? _branch.KidSwapAccountNo : String.Empty),
                CompanyId = (!String.IsNullOrEmpty(_branch.RegisteredNo) ? _branch.RegisteredNo : String.Empty),
                ReorderSmsReceiver = _branch.ReorderSmsReceiver
            };

            try
            {
                transaction = connection.BeginTransaction();
                SaveCreditorAction saveCreditorAction = new SaveCreditorAction(creditor);
                int creditorEntNo = saveCreditorAction.SaveCreditor(transaction);
                if (creditorEntNo > 0)
                {
                    int branchId = SaveBranch(transaction, creditorEntNo, creditor.CreditorGroupId, _branch.IsExpressGym, _branch.Web, _branch.OfficialName, _branch.CountryMobilePrefix,_branch.OtherAdminMobile,_branch.OtherAdminEmail);
                    if (branchId > 0)
                    {
                        transaction.Commit();
                        return branchId;
                    }
                    else
                    {
                        transaction.Rollback();
                        return -1;
                    }
                }
                else
                {
                    transaction.Rollback();
                    return -1;
                }
            }
            catch(Exception ex)
            {
               
                transaction.Rollback();
                return -1;
            }
        }

        public int SaveBranch(DbTransaction transaction, int entNo, int groupId, bool isExpressGym, string web, string officialName, string mobileCountryPrefix, string otherAdminMobile, string otherAdminEmail)
        {
            try
            {
                string storedProcedureName = "USExceGMSAdminAddEditBranchDetails";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Connection = transaction.Connection;
                cmd.Transaction = transaction;

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, 0));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entNo", DbType.Int32, entNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@groupId", DbType.Int32, groupId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Web", DbType.String, web));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsExpressGym", DbType.String, isExpressGym));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@OfficialName", DbType.String, officialName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MobileCountryPrefix", DbType.String, mobileCountryPrefix));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@OtherAdminMobile", DbType.String, otherAdminMobile));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@OtherAdminEmail", DbType.String, otherAdminEmail));
                if (_branch.CompanyIdSalary > 0)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@CompanyIdSalary", DbType.Int32, _branch.CompanyIdSalary));
                SqlParameter outputParam = new SqlParameter();
                outputParam.ParameterName = "@outputId";
                outputParam.SqlDbType = SqlDbType.Int;
                outputParam.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outputParam);
                cmd.ExecuteScalar();
                int branchId = Convert.ToInt32(outputParam.Value);
                return branchId;
            }
            catch (Exception ex)
            {
                throw ex;
                return -1;
            }
        }
    }
}
