﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.Common;
using System.IO;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.ResultNotifications;
using US.GMS.API;
using US.Common.Logging.API;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetInstructorDetailsByInstructorIdAction : USDBActionBase<InstructorDC>
    {
        private int _branchId;
        private string _user = string.Empty;
        private int _instructorId;
        private string _gymCode = string.Empty;
        public GetInstructorDetailsByInstructorIdAction(int branchId, string user, int instructorId, string gymCode)
        {
            _branchId = branchId;
            _user = user;
            _instructorId = instructorId;
            _gymCode = gymCode;
        }

        protected override InstructorDC Body(DbConnection connection)
        {

            InstructorDC instructer = new InstructorDC();
            string StoredProcedureName = "USExceGMSAdminGetInstructorsByInstructorId";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@instructorId", DbType.Int32, _instructorId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {

                    instructer.Id = Convert.ToInt32(reader["InstructorId"]);
                    instructer.CustId = Convert.ToString(reader["CustId"]);
                    instructer.EntNo = Convert.ToInt32(reader["EntNo"]);
                    instructer.BranchId = Convert.ToInt32(reader["BranchId"]);
                    instructer.FirstName = reader["FirstName"].ToString();
                    instructer.LastName = reader["LastName"].ToString();
                    instructer.Name = instructer.FirstName + " " + instructer.LastName;
                    if (!string.IsNullOrEmpty(reader["BirthDate"].ToString()))
                    {
                        instructer.BirthDay = Convert.ToDateTime(reader["BirthDate"]);
                    }
                    if (!string.IsNullOrEmpty(reader["Address"].ToString()))
                    {
                        instructer.Address1 = Convert.ToString(reader["Address"]);
                    }
                    if (!string.IsNullOrEmpty(reader["Address2"].ToString()))
                    {
                        instructer.Address2 = Convert.ToString(reader["Address2"]);
                    }
                    if (!string.IsNullOrEmpty(reader["Address3"].ToString()))
                    {
                        instructer.Address3 = Convert.ToString(reader["Address3"]);
                    }
                    if (!string.IsNullOrEmpty(reader["ZipCode"].ToString()))
                    {
                        instructer.PostCode = Convert.ToString(reader["ZipCode"]);
                    }
                    if (!string.IsNullOrEmpty(reader["ZipName"].ToString()))
                    {
                        instructer.PostPlace = Convert.ToString(reader["ZipName"]);
                    }
                    if (!string.IsNullOrEmpty(reader["TelMobile"].ToString()))
                    {
                        instructer.Mobile = Convert.ToString(reader["TelMobile"]).Trim();
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["TelWork"])))
                    {
                        instructer.Work = Convert.ToString(reader["TelWork"]).Trim();
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(reader["TelHome"])))
                    {
                        instructer.Private = Convert.ToString(reader["TelHome"]).Trim();
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["Description"])))
                    {
                        instructer.Description = Convert.ToString(reader["Description"]);
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(reader["Gender"])))
                    {
                        instructer.Gender = Convert.ToString(reader["Gender"]);
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(reader["Email"])))
                    {
                        instructer.Email = Convert.ToString(reader["Email"]);
                    }
                    instructer.ImagePath = reader["ImagePath"].ToString();
                    instructer.ActiveStatus = Convert.ToBoolean(reader["ActiveStatus"]);
                    instructer.EntityRoleType = "INS";
                    instructer.ImagePath = Convert.ToString(reader["ImagePath"]);

                    CategoryDC categoryDC = new CategoryDC
                    {
                        Id = Convert.ToInt32(reader["CategoryId"]),
                        Code = reader["CategoryCode"].ToString(),
                        Name = reader["CategoryDescription"].ToString()
                    };
                    instructer.Category = categoryDC;
                    instructer.RoleId = reader["RoleId"].ToString();
                    instructer.EntityRoleType = "INS";
                    instructer.IsHasTask = Convert.ToBoolean(reader["IsHasTask"]);
                    instructer.AssignedEntityId = Convert.ToInt32(reader["AssignedEntityId"]);
                    instructer.CommentForInactive = Convert.ToString(reader["Comment"]);
                    instructer.SourceEntity = Convert.ToInt32(reader["SourceEntity"]);
                    GetActivitiesForEmployeeAction action = new GetActivitiesForEmployeeAction(instructer.BranchId, instructer.Id, instructer.RoleId);
                    try
                    {
                        instructer.ActivityList = action.Execute(EnumDatabase.Exceline, _gymCode);
                    }
                    catch
                    {
                        throw;
                    }

                    if (!string.IsNullOrEmpty(instructer.ImagePath))
                    {
                        instructer.ProfilePicture = GetMemberProfilePicture(instructer.ImagePath);
                    }

                    try
                    {
                        instructer.Schedule = GetEntitySchedule(instructer.Id, instructer.RoleId, _user, _gymCode);
                    }
                    catch
                    {
                    }

                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return instructer;
        }

        private byte[] GetMemberProfilePicture(string p)
        {
            if (File.Exists(p))
            {
                byte[] byteArray = System.IO.File.ReadAllBytes(p);
                return byteArray;
            }
            return new byte[0];
        }

        public ScheduleDC GetEntitySchedule(int entityId, string entityRoleType, string user, string gymCode)
        {
            OperationResult<ScheduleDC> result = GMSSchedule.GetEntitySchedule(entityId, entityRoleType, user, gymCode);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new ScheduleDC();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }
    }
}