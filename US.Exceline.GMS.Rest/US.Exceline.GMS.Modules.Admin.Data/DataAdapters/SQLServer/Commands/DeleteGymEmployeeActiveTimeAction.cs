﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public  class DeleteGymEmployeeActiveTimeAction:  USDBActionBase<bool>
    {
        private int _activeTimeId;
 

        public DeleteGymEmployeeActiveTimeAction(int activeTimeId)
        {
            _activeTimeId = activeTimeId;
        }

        protected override bool Body(DbConnection connection)
        {
            bool result = false;
            string StoredProcedureName = "USExceGMSAdminDeleteGymEmployeeWorkActiveTime";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ActiveTimeId", DbType.Int32, _activeTimeId));
                cmd.ExecuteScalar();
                result = true;
            }
            catch
            {
                throw;
            }
            return result;
        }



    }
}
