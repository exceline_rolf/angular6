﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetEmployeeTimeEntriesAction : USDBActionBase<List<EmployeeTimeEntryDC>>
    {
        private int _employeeId;

        public GetEmployeeTimeEntriesAction(int employeeId)
        {
            _employeeId = employeeId;
        }

        protected override List<EmployeeTimeEntryDC> Body(System.Data.Common.DbConnection connection)
        {
            List<EmployeeTimeEntryDC> employeeTimeList = new List<EmployeeTimeEntryDC>();
            string storedProcedure = "USExceGMSAdminGetEmployeeTimeEntry";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EmployeeId", DbType.Int32, _employeeId ));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    EmployeeTimeEntryDC  timeEntry = new EmployeeTimeEntryDC();
                    timeEntry.Id = Convert.ToInt32(reader["ID"]);
                    timeEntry.EmployeeId = Convert.ToInt32(reader["EmployeeId"]);
                    timeEntry.WorkDate = Convert.ToDateTime(reader["WorkDate"]);
                    //timeEntry.StartTimeTicks = Convert.ToInt32 ( reader["StartTime"]);
                    //timeEntry.EndTimeTicks = Convert.ToInt32 (reader["EndTime"]);
                    timeEntry.StartTime = (TimeSpan)(reader["StartTime"]);                                            //TimeSpan.FromMinutes( timeEntry.StartTimeTicks);
                    timeEntry.EndTime = (TimeSpan)(reader["EndTime"]);                   /////TimeSpan.FromMinutes( timeEntry.EndTimeTicks);
                    timeEntry.TimeInMinutes = Convert.ToInt32(reader["TimeInMinutes"]);
                    timeEntry.TimeCategoryId = Convert.ToInt32(reader["TimeCategoryId"]);
                    timeEntry.TimeCategoryName = Convert.ToString(reader["Name"]);
                    timeEntry.Comment = Convert.ToString(reader["Comment"]);
                    timeEntry.BranchId = Convert.ToInt32(reader["BranchId"]);
                    timeEntry.BranchName = reader["BranchName"].ToString();

                    if (reader["IsApproved"] != DBNull.Value)
                        timeEntry.IsApproved = Convert.ToBoolean(reader["IsApproved"]);
                    timeEntry.CreatedUser = Convert.ToString(reader["CreatedUser"]);
                    timeEntry.CreatedDate = Convert.ToString(reader["CreatedDate"]);
                    employeeTimeList.Add(timeEntry);
                   
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return employeeTimeList;
        }


    }
}
