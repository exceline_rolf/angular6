﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Admin.HolydayManagement;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class AddNewHolidayAction : USDBActionBase<bool>, IDisposable
    {
        private int _year;
        private string _holidayType;
        private DateTime _startDate;
        private DateTime _endDate;
        private string _day;
        private string _name;
        private int _branchId;
        private bool _activeStatus;
        private List<DateTime> _datesList = new List<DateTime>();
        private DataTable _dataTable = null;

        public AddNewHolidayAction(HolidayScheduleDC newHoliday)
        {
            this._year = newHoliday.Year;
            this._holidayType = newHoliday.HolidayCategoryName;
            this._startDate = newHoliday.StartDate;
            this._endDate = newHoliday.EndDate;
            this._day = newHoliday.Day;
            this._name = newHoliday.Name;
            this._branchId = newHoliday.BranchId;
            this._activeStatus = newHoliday.ActiveStatus;
            this._datesList = newHoliday.DatesList;
            _dataTable = new DataTable();
            _dataTable.Columns.Add(new DataColumn("Holiday", Type.GetType("System.DateTime")));

            foreach (DateTime date in _datesList)
            {
                DataRow _dataTableRow = _dataTable.NewRow();
                _dataTableRow["Holiday"] = date.Date;
                _dataTable.Rows.Add(_dataTableRow);
            }
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string storedProcedure = "USExceGMSAdminAddHolidaySchedule";
            bool returnValue = false;
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Year", System.Data.DbType.Int32, _year));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@HolidayType", System.Data.DbType.String, _holidayType.Replace(" ", string.Empty)));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartDate", System.Data.DbType.DateTime, _startDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndDate", System.Data.DbType.DateTime, _endDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Day", System.Data.DbType.String, _day));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Name", System.Data.DbType.String, _name));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", System.Data.DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ActiveStatus", System.Data.DbType.Boolean, _activeStatus));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@DatesList", SqlDbType.Structured, _dataTable));
                cmd.ExecuteNonQuery();
                returnValue = true;
            }
            catch
            {
                throw;
            }
            return returnValue;
        }

        public void Dispose()
        {
            this.Dispose();
            GC.SuppressFinalize(true);
        }
    }
}
