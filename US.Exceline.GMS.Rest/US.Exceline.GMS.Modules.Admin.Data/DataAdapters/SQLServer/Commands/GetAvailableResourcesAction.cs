﻿using System;
using System.Collections.Generic;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.Admin.ManageResources;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    class GetAvailableResourcesAction : USDBActionBase<List<AvailableResourcesDC>>
    {
        private int _branchId = -1;
        private int _resId = -1;

        public GetAvailableResourcesAction(int branchId,int resId)
        {
            _branchId = branchId;
            _resId = resId;
        }

        protected override List<AvailableResourcesDC> Body(DbConnection connection)
        {
            List<AvailableResourcesDC> resourcesList = new List<AvailableResourcesDC>();
            string storedProcedureName = "USExceGMSAdminGetAvailableResources";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                if(_resId > 0)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@resId", DbType.Int32, _resId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    AvailableResourcesDC AvailableResources = new AvailableResourcesDC();

                    AvailableResources.Id           = Convert.ToInt32(reader["Id"]);
                    AvailableResources.ResourID     = Convert.ToInt32(reader["ResourceID"]);
                    AvailableResources.ResourName   = Convert.ToString(reader["ResourceName"]);
                    AvailableResources.Day          = Convert.ToString(reader["Day"]);
                    if (reader["FromTime"] != DBNull.Value)
                    AvailableResources.FromTime     = Convert.ToDateTime(reader["FromTime"]);
                    if (reader["ToTime"] != DBNull.Value)
                    AvailableResources.ToTime       = Convert.ToDateTime(reader["ToTime"]);
                    if (reader["ActivityId"] != DBNull.Value)
                    AvailableResources.ActivityId   = Convert.ToInt32(reader["ActivityId"]);
                    if (reader["ActivityName"] != DBNull.Value)
                    AvailableResources.ActivityName = Convert.ToString(reader["ActivityName"]);

                    resourcesList.Add(AvailableResources);
                }
            }
            catch (Exception ex)
            {                
                throw ex;
            }
            return resourcesList;
        }
    }
}
