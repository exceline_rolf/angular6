﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class ValidateContractConditionWithTemplateAction : USDBActionBase<List<ContractTemplateDC>>
    {
        private readonly int _contractConditionId = -1;

        public ValidateContractConditionWithTemplateAction(int contractConditionId)
        {
            _contractConditionId = contractConditionId;
        }

        protected override List<ContractTemplateDC> Body(DbConnection connection)
        {
            var contractTemplateList = new List<ContractTemplateDC>();
            try
            {
                const string storedProcedure = "USExceGMSValidateContractConditionWithTemplate";
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ContractConditionId", DbType.Int32, _contractConditionId));
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var template = new ContractTemplateDC
                    {
                        TemplateID = Convert.ToInt32(reader["TemplateID"]),
                        TemplateNo = Convert.ToInt32(reader["TemplateNo"]),
                        TemplateName = Convert.ToString(reader["TemplateName"])
                    };
                    contractTemplateList.Add(template);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return contractTemplateList;
        }
    }
}
