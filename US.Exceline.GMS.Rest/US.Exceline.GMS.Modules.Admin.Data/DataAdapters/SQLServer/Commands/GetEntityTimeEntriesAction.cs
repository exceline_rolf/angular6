﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS.Modules.Admin.Data
// Coding Standard   : US Coding Standards
// Author            : NHE
// Created Timestamp : "5/19/2012 5:37:44 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetEntityTimeEntriesAction : USDBActionBase<List<TimeEntryDC>>
    {
        private DateTime _startDate;
        private DateTime _endDate;
        private int _entityId;
        private string _entityRoleType;

        public GetEntityTimeEntriesAction(DateTime startDate, DateTime endDate, int entityId, string entityRoleType)
        {
           _startDate = startDate; 
           _endDate = endDate;
           _entityId = entityId;
           _entityRoleType = entityRoleType;
        }

        protected override List<TimeEntryDC> Body(DbConnection connection)
        {
            List<TimeEntryDC> entityTimeEntryList = new List<TimeEntryDC>();
            string storedProcedureName = "USExceGMSAdminGetEntityTaskTimes";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@startDate", DbType.DateTime, _startDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@endDate", DbType.DateTime, _endDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityId", DbType.Int32 , _entityId ));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityRoleType", DbType.String , _entityRoleType));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    TimeEntryDC timeEntry = new TimeEntryDC();
                    if (reader["ID"] != DBNull.Value)
                        timeEntry.Id = Convert.ToInt32(reader["ID"]);
                    if (reader["TaskId"] != DBNull.Value)
                        timeEntry.TaskId = Convert.ToInt32(reader["TaskId"]);
                    if (reader["Name"] != DBNull.Value)
                       timeEntry.TaskName = Convert.ToString(reader["Name"]);
                    if (reader["Comment"] != DBNull.Value)
                        timeEntry.Comment = Convert.ToString(reader["Comment"]);
                    if (reader["SpendTime"] != DBNull.Value)
                        timeEntry.SpendTime = Convert.ToDecimal(reader["SpendTime"]);
                    if (reader["ExecutedDate"] != DBNull.Value)
                        timeEntry.ExecutedDate  = Convert.ToDateTime(reader["ExecutedDate"]);
                    entityTimeEntryList.Add(timeEntry);
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return entityTimeEntryList;
        }

    }
}
