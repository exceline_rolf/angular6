﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US_DataAccess;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;


namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
   public class CheckActiveTimeOverlapWithBookingAction : USDBActionBase<string>
    {
        private EntityActiveTimeDC _activeTime;
        private DataTable _dataTable = null;
        public CheckActiveTimeOverlapWithBookingAction(EntityActiveTimeDC activeTime)
        {
            _activeTime = activeTime;
            List<int> resList = new List<int>();
            resList.Add(activeTime.EntityId);
            _activeTime.BookingResourceList.ToList().ForEach(x => resList.Add(x.Id));
            if (resList != null && resList.Count > 0)
                _dataTable = GetResList(resList);
        }

        private DataTable GetResList(List<int> resList)
        {
            _dataTable = new DataTable();
            _dataTable.Columns.Add(new DataColumn("ID", Type.GetType("System.Int32")));
            foreach (var item in resList)
            {
                DataRow dataTableRow = _dataTable.NewRow();
                dataTableRow["ID"] = item;
                _dataTable.Rows.Add(dataTableRow);
            }
            return _dataTable;
        }
        protected override string Body(System.Data.Common.DbConnection connection)
        {
            string result = string.Empty;
            string storedProcedure = "USExceGMSAdminCheckActiveTimeOverlapWithBooking";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@activeTimeId", DbType.Int32, _activeTime.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@startTime", DbType.DateTime, _activeTime.StartDateTime));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@endTime", DbType.DateTime, _activeTime.EndDateTime));
                cmd.Parameters.Add(_dataTable != null
                                          ? DataAcessUtils.CreateParam("@resList", SqlDbType.Structured,
                                                                       _dataTable)
                                          : DataAcessUtils.CreateParam("@resList", SqlDbType.Structured, null));

                DbParameter output = new SqlParameter();
                output.DbType = DbType.String;
                output.ParameterName = "@outPutVal";
                 output.Size = 100;
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);
                cmd.ExecuteNonQuery();
                result = Convert.ToString(output.Value);
               
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}
