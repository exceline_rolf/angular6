﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 27/4/2012 3:31:44 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageContracts;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    class EditContractItemAction : USDBActionBase<bool>
    {
        private ContractItemDC _contractItem = new ContractItemDC();
        private string _user = string.Empty;
        private int _branchId = -1;

        public EditContractItemAction(ContractItemDC contractItem, string user, int branchId)
        {
            _contractItem = contractItem;
            _user = user;
            _branchId = branchId;
        }

        protected override bool Body(DbConnection connection)
        {
            bool result = false;
            try
            {
                string storedProcedureName = "USExceGMSAdminEditContractItem";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Name", DbType.String, _contractItem.ItemName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@UnitPrice", DbType.Decimal, _contractItem.UnitPrice));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ItemId", DbType.Int32, _contractItem.ItemId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CategoryId", DbType.Int32, _contractItem.CategoryId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ContractItemID", DbType.Int32, _contractItem.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId ", DbType.Int32, _branchId));
                cmd.ExecuteNonQuery();
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result; ;
        }
    }
}
