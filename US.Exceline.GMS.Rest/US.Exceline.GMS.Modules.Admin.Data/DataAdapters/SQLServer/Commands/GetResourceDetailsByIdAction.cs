﻿using System;
using System.Data;
using System.Data.Common;
using System.Linq;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US.GMS.Core.DomainObjects.Common;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetResourceDetailsByIdAction : USDBActionBase<ResourceDC>
    {
        private readonly string _gymCode = string.Empty;
        private readonly int _resourceId;
        public GetResourceDetailsByIdAction(int resourceId,string gymCode)
        {
            _resourceId = resourceId;
            _gymCode = gymCode;
        }
        protected override ResourceDC Body(DbConnection connection)
        {
            var resource = new ResourceDC();
            const string storedProcedureName = "USExceGMSAdminGetResourceDetailsByResourceId";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ResourceId", DbType.Int32, _resourceId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    resource.Id = Convert.ToInt32(reader["ResourceId"]);
                    resource.BranchId = Convert.ToInt32(reader["BranchId"]);
                    resource.Name = reader["Name"].ToString();
                    resource.Description = reader["Description"].ToString();
                    resource.CreatedDate = Convert.ToDateTime(reader["CreatedTime"]);
                    resource.CreatedUser = reader["CreatedUser"].ToString();
                    resource.ModifiedDate = Convert.ToDateTime(reader["LastModifiedDate"]);
                    resource.LastModifiedUser = reader["ModifiedUser"].ToString();
                    resource.ActiveStatus = Convert.ToBoolean(reader["ActiveStatus"]);
                    resource.ResId = Convert.ToString(reader["ResId"]);
                    var categoryDc = new CategoryDC
                    {
                        Id = Convert.ToInt32(reader["CategoryId"]),
                        Code = reader["CategoryCode"].ToString(),
                        Name = reader["CategoryDescription"].ToString()
                    };
                    resource.ResourceCategory = categoryDc;

                    var timeCategoryDc = new CategoryDC
                    {
                        Id = Convert.ToInt32(reader["TimeCategoryId"]),
                        Code = reader["TimeCategoryCode"].ToString(),
                        Name = reader["TimeCategoryName"].ToString()
                    };
                    resource.TimeCategory = timeCategoryDc;

                    if (reader["SalaryPerBooking"] != DBNull.Value)
                        resource.IsSalaryPerBooking = Convert.ToBoolean(reader["SalaryPerBooking"]);

                    if (reader["PurchasedDate"] != DBNull.Value)
                        resource.PurchasedDate = Convert.ToDateTime(reader["PurchasedDate"]);

                    if (reader["ServiceDate"] != DBNull.Value)
                        resource.MaintenanceDate = Convert.ToDateTime(reader["ServiceDate"]);

                    resource.ActivityId = Convert.ToInt32(reader["ActivityId"]);
                    resource.ActivityName = reader["ActivityName"].ToString();
                    resource.DaysForBookingPerActivity = Convert.ToInt32(reader["DaysForBooking"]);
                    resource.EquipmentId = Convert.ToInt32(reader["EquipmentId"]);
                    resource.ActivitySmsReminder = Convert.ToBoolean(reader["SmsReminder"]);
                    resource.IsEquipment = resource.EquipmentId != 0;
                    resource.ArticleId = Convert.ToInt32(reader["ArticleId"]);
                    resource.ArticleSettingId = Convert.ToInt32(reader["ArticleSettingId"]);
                    resource.ArticleName = Convert.ToString(reader["ArticleName"]);

                    var action = new GetEmpolyeeForResourceAction(resource.Id);
                    resource.EmpList = action.Execute(EnumDatabase.Exceline, _gymCode);

                    var actionSchedule = new GetResourceScheduleItemsAction(resource.Id, "RES", _gymCode);
                    resource.Schedule = actionSchedule.Execute(EnumDatabase.Exceline, _gymCode);

                    //if (resource.ActivityId > 0)
                    //{
                    //    GetActivityUnavailableTimeAction actionActivityTime = new GetActivityUnavailableTimeAction(_branchId, resource.ActivityId);
                    //    resource.ActivityTimeLst = actionActivityTime.Execute(EnumDatabase.Exceline, _gymCode);
                    //}

                    if (resource.EmpList != null && resource.EmpList.ToList().Any())
                    {
                        resource.EmpCount = resource.EmpList.Count();
                        resource.IsHasEmp = true;
                    }

                }

            }

            catch (Exception ex)
            {
                throw ex;
            }

            return resource;
        }
    }
}
