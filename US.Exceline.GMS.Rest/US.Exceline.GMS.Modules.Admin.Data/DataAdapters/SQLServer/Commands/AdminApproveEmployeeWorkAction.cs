﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public  class AdminApproveEmployeeWorkAction:  USDBActionBase<int>
    {
        private GymEmployeeWorkDC _work = new GymEmployeeWorkDC();
        private bool _isApproved;
        private string _user;

        public AdminApproveEmployeeWorkAction(GymEmployeeWorkDC work, bool isApproved, string user)
        {
            _work = work;
            _isApproved = isApproved;
            _user = user;
        }

        protected override int Body(DbConnection connection)
        {
            int outputId = -1;
            string StoredProcedureName = "USExceGMSAdminApproveEmployeeWork";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EmployeeId", DbType.Int32, _work.EmployeeId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleItemId", DbType.Int32, _work.ScheduleItemId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Date", DbType.DateTime, _work.Date));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartTime", DbType.DateTime, _work.StartTime));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndTime", DbType.DateTime, _work.EndTime));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ApprovalId", DbType.Int32, _work.ApprovalId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsApproved", DbType.Boolean, _isApproved));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _user));
                SqlParameter output = new SqlParameter("@OutPut", SqlDbType.Int);
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);
                cmd.ExecuteNonQuery();
                outputId = Convert.ToInt32(output.Value);
            }
            catch
            {
                throw;
            }
            return outputId;
        }



    }
}
