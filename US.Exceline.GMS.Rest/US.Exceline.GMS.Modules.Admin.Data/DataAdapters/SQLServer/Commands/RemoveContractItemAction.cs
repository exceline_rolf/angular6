﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 4/27/2012 3:37:04 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    class RemoveContractItemAction : USDBActionBase<bool>
    {
        private int _contractItemID = -1;

        public RemoveContractItemAction(int contractItemId, string user, int branchId)
        {
            _contractItemID = contractItemId;
        }
        protected override bool Body(DbConnection connection)
        {
            bool result = false;
            try
            {
                string storedProcedureName = "USExceGMSAdminRemoveContractItem";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ContractItemID", DbType.Int32, _contractItemID));
                cmd.ExecuteNonQuery();
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result; ;
        }
    }
}
