﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US_DataAccess;
using System.Diagnostics;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class SaveResourceActiveTimePunchardAction : USDBActionBase<int>
    {
        private EntityActiveTimeDC _activeTime;
        private readonly bool _isDelete;
        private DataTable _dataTable;
        private readonly int _branchId;
        private readonly string _user = string.Empty;
        private int _contractId;

        public SaveResourceActiveTimePunchardAction(EntityActiveTimeDC activeTime, bool isDelete, int branchId, string user, int contractId)
        {
            _activeTime = activeTime;
            _isDelete = isDelete;
            _branchId = branchId;
            _user = user;
            _contractId = contractId;
            if ((activeTime.BookingMemberList != null && activeTime.BookingMemberList.Any()) || (activeTime.BookingResourceList != null && activeTime.BookingResourceList.Any()))
                _dataTable = GetMemberLst(activeTime.BookingMemberList, activeTime.BookingResourceList, activeTime.Id);
        }


        private DataTable GetMemberLst(List<BookingEntityDc> memLst, List<BookingEntityDc> resLst, int activeTimeId)
        {
            _dataTable = new DataTable();
            _dataTable.Columns.Add(new DataColumn("ActiveTimeId", typeof(Int32)));
            _dataTable.Columns.Add(new DataColumn("EntityId", typeof(Int32)));
            _dataTable.Columns.Add(new DataColumn("RoleId", typeof(String)));
            _dataTable.Columns.Add(new DataColumn("ContractId", typeof(Int32)));
            _dataTable.Columns.Add(new DataColumn("Deleted", typeof(Boolean)));
            _dataTable.Columns.Add(new DataColumn("CreditBack", typeof(Boolean)));
            if (memLst != null && memLst.Any())
                foreach (var item in memLst)
                {
                    DataRow dataTableRow = _dataTable.NewRow();
                    dataTableRow["ActiveTimeId"] = activeTimeId;
                    dataTableRow["EntityId"] = item.Id;
                    dataTableRow["RoleId"] = "MEM";
                    dataTableRow["ContractId"] = DBNull.Value;
                    Debug.WriteLine(DBNull.Value);
                    dataTableRow["Deleted"] = item.IsDeleted;
                    dataTableRow["CreditBack"] = item.IsBookingCreditback;
                    _dataTable.Rows.Add(dataTableRow);
                }

            if (resLst != null && resLst.Any())
                foreach (var item in resLst)
                {
                    DataRow dataTableRow = _dataTable.NewRow();
                    dataTableRow["ActiveTimeId"] = activeTimeId;
                    dataTableRow["EntityId"] = item.Id;
                    dataTableRow["RoleId"] = "RES";
                    dataTableRow["ContractId"] = DBNull.Value;
                    dataTableRow["Deleted"] = item.IsDeleted;
                    dataTableRow["CreditBack"] = DBNull.Value;
                    _dataTable.Rows.Add(dataTableRow);
                }

            return _dataTable;
        }
        protected override int Body(DbConnection connection)
        {
            Debug.WriteLine("SQL next");
            int activeTimeId = -1;
            string spName = "USExceGMSAdminPunchcardSaveResourceScheduleItemActiveTime";
            try
            {
                DbCommand command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleItemId", DbType.Int32, _activeTime.SheduleItemId));
                if (_activeTime.Id > 0)
                    command.Parameters.Add(DataAcessUtils.CreateParam("@ActiveTimeId", DbType.Int32, _activeTime.Id));
                command.Parameters.Add(DataAcessUtils.CreateParam("@StartTime", DbType.DateTime, _activeTime.StartDateTime));
                command.Parameters.Add(DataAcessUtils.CreateParam("@EndTime", DbType.DateTime, _activeTime.EndDateTime));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Comment", DbType.String, _activeTime.Comment));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ArticleId", DbType.Int32, _activeTime.ArticleId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@SMSRemindered", DbType.Boolean, _activeTime.IsSmsReminder));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ArrivalDateTime", DbType.DateTime, _activeTime.ArrivedDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@IsPaid", DbType.Boolean, _activeTime.IsPaid));
                command.Parameters.Add(DataAcessUtils.CreateParam("@aRItemNo", DbType.Int32, _activeTime.ArItemNo));
                command.Parameters.Add(DataAcessUtils.CreateParam("@amount", DbType.Decimal, _activeTime.TotalPaid));
                command.Parameters.Add(DataAcessUtils.CreateParam("@IsDeleted", DbType.Boolean, _isDelete));
                command.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));
                command.Parameters.Add(DataAcessUtils.CreateParam("@task", DbType.Boolean, _activeTime.IsTask));
                // command.Parameters.Add(DataAcessUtils.CreateParam("@IsCreditback", DbType.Boolean, _activeTime.IsBookingCreditback));
                command.Parameters.Add(DataAcessUtils.CreateParam("@IsExtraResChange", DbType.Boolean, _activeTime.IsExtraResChange));
                command.Parameters.Add(DataAcessUtils.CreateParam("@BookingCategoryId", DbType.Int32, _activeTime.BookingCategoryId));
                if (_activeTime.RoleType != null)
                {
                    command.Parameters.Add(DataAcessUtils.CreateParam("@RoleType", DbType.String, _activeTime.RoleType.ToUpper().Trim()));
                }
                if (_branchId > 0)
                { 
                    command.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                    command.Parameters.Add(_dataTable != null
                        ? DataAcessUtils.CreateParam("@ExceMemberBoking", SqlDbType.Structured, _dataTable)
                        : DataAcessUtils.CreateParam("@ExceMemberBoking", SqlDbType.Structured, null));
                }

                command.Parameters.Add(DataAcessUtils.CreateParam("@ContractId", DbType.Int32, _contractId));

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@output";
                param.SqlDbType = SqlDbType.Int;
                param.Direction = ParameterDirection.Output;
                command.Parameters.Add(param);
                command.ExecuteNonQuery();
                activeTimeId = Convert.ToInt32(param.Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return activeTimeId;
        }
    }
}
