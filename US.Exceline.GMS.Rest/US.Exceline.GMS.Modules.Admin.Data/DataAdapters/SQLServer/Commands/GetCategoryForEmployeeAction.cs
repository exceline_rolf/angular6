﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Common;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetCategoryForEmployeeAction : USDBActionBase<List<CategoryDC>>
    {
        private int _branchId = -1;
        private int _employeeId = -1;

        public GetCategoryForEmployeeAction(int branchId, int employeeId)
        {
            _branchId = branchId;
            _employeeId = employeeId;
        }

        protected override List<CategoryDC> Body(System.Data.Common.DbConnection connection)
        {
            string StoredProcedureName = "USExceGMSAdminGetCategotyForEmployee";
            List<CategoryDC> categoryList = new List<CategoryDC>();
            try
            {

                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@employeeId", DbType.Int32, _employeeId));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    CategoryDC categoryItem = new CategoryDC();
                    categoryItem.Id = Convert.ToInt32(reader["CategoryID"]);
                    categoryItem.Name = Convert.ToString(reader["CategoryName"]);
                    categoryItem.Code = Convert.ToString(reader["CategoryCode"]);
                    categoryList.Add(categoryItem);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return categoryList;
        }
    }
}
