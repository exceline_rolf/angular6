﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Admin.HolydayManagement;
using System.Data.Common;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetHolidayScheduleAction : USDBActionBase<List<HolidayScheduleDC>>
    {
        private int _branchId;
        private string _holidayType;
        private DateTime _startDate;
        private DateTime _endDate;


        public GetHolidayScheduleAction()
        {
        }

        public GetHolidayScheduleAction(int branchId, string holidayType, DateTime startDate, DateTime endDate)
        {
            this._branchId = branchId;
            this._holidayType = holidayType;
            this._startDate = startDate;
            this._endDate = endDate;
        }

        protected override List<HolidayScheduleDC> Body(System.Data.Common.DbConnection connection)
        {
            List<HolidayScheduleDC> holidayList = new List<HolidayScheduleDC>();
            string storedProcedure = "USExceGMSAdminGetHolidayShedule";
            string storedProcedure2 = "USExceGMSAdminGetHolidayList";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", System.Data.DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@HolidayType", System.Data.DbType.String, _holidayType));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartDate", System.Data.DbType.DateTime, _startDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndDate", System.Data.DbType.DateTime, _endDate));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    HolidayScheduleDC holidaySchedule = new HolidayScheduleDC();
                    holidaySchedule.Id = Convert.ToInt32(reader["Id"].ToString());
                    holidaySchedule.Year = Convert.ToInt32(reader["Year"].ToString());
                    holidaySchedule.ActiveStatus = Convert.ToBoolean(reader["ActiveStatus"]);
                    holidaySchedule.CategoryId = Convert.ToInt32(reader["CategoryId"].ToString());
                    holidaySchedule.StartDate = Convert.ToDateTime(reader["StartDate"].ToString());
                    holidaySchedule.EndDate = Convert.ToDateTime(reader["EndDate"].ToString());
                    holidaySchedule.Day = reader["Day"].ToString();
                    holidaySchedule.Name = reader["Name"].ToString();
                    holidaySchedule.BranchId = Convert.ToInt16(reader["BranchId"].ToString());
                    holidayList.Add(holidaySchedule);
                }

                reader.Close();

                foreach (HolidayScheduleDC temp in holidayList)
                {
                    int holidayScheduleId = temp.Id;
                    DbCommand cmd2 = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure2);
                    cmd2.Parameters.Add(DataAcessUtils.CreateParam("@HolidayScheduleId", System.Data.DbType.Int32, holidayScheduleId));
                    DbDataReader reader2 = cmd2.ExecuteReader();
                    List<DateTime> days = new List<DateTime>();
                    while (reader2.Read())
                    {
                        DateTime day = new DateTime();
                        day = Convert.ToDateTime(reader2["Holiday"].ToString());
                        days.Add(day);
                    }
                    reader2.Close();
                    temp.DatesList = days;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return holidayList;
        }

    }
}
