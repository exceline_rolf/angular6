﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US.GMS.Core.SystemObjects;
using US_DataAccess;
using System.Data.Common;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetGymCompanyClassKeywordAction : USDBActionBase<List<string>>
    {
        public GetGymCompanyClassKeywordAction()
        {
        }
        protected override List<string> Body(System.Data.Common.DbConnection connection)
        {
            List<string> classKeywordList;
            try
            {
                string storedProcedure = "USExceGMSAdminGetGymCompanyClassKeyword";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                DbDataReader reader = cmd.ExecuteReader();
                string classKeyword = string.Empty;
                while (reader.Read())
                {
                    classKeyword = reader["ClassKeywords"].ToString();
                }
                classKeywordList = new List<string>(classKeyword.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return classKeywordList;
        }
    }
}