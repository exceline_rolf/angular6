﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
   public class ValidateEmployeeFollowUpAction : USDBActionBase<bool>
   {
       private readonly int _employeeId;
       private readonly DateTime _endDate;
       public  ValidateEmployeeFollowUpAction(int employeeId,DateTime endDate)
       {
           _employeeId = employeeId;
           _endDate = endDate;
       }

        protected override bool Body(DbConnection connection)
        {
            bool result ;
            string spName = "USExceGMSAdminValidateEmployeeFollowUp";
            try
            {
                DbCommand command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@EmployeeId", DbType.Int32, _employeeId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@EndDate", DbType.DateTime, _endDate));

                DbParameter output = new SqlParameter();
                output.DbType = DbType.Boolean;
                output.ParameterName = "@OutPut";
                output.Direction = ParameterDirection.Output;
                command.Parameters.Add(output);
                command.ExecuteNonQuery();
                result = Convert.ToBoolean(output.Value);
               
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}
