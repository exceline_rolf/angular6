﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class ValidateContractConditionWithContractsAction : USDBActionBase<int>
    {
        private readonly int _contractConditionId = -1;

        public ValidateContractConditionWithContractsAction(int contractConditionId)
        {
            _contractConditionId = contractConditionId;
        }
        protected override int Body(DbConnection connection)
        {
            var outputId = 0;
            const string storedProcedureName = "USExceGMSAdminValidateContractConditionWithContracts";
            try
            {
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ContractConditionId", DbType.Int32, _contractConditionId));

                DbParameter para1 = new SqlParameter();
                para1.DbType = DbType.Int32;
                para1.ParameterName = "@outPutID";
                para1.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(para1);
                cmd.ExecuteNonQuery();
                outputId = Convert.ToInt32(para1.Value);
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return outputId;
        }
    }
}
