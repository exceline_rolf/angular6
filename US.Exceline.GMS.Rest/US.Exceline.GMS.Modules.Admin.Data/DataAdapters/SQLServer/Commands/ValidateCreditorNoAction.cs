﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class ValidateCreditorNoAction : USDBActionBase<bool>
    {
        private readonly int _branchId;
        private readonly string _gymCode = string.Empty;
        private readonly string _creditorNo = string.Empty;
        public ValidateCreditorNoAction(int branchId,string gymCode,string creditorNo)
        {
            _branchId = branchId;
            _gymCode = gymCode;
            _creditorNo = creditorNo;
        }
        protected override bool Body(DbConnection connection)
        {
            bool result;
            string spName = "ExceWorkStationValidateCreditorNo";
            try
            {
                DbCommand command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@GymCode", DbType.String, _gymCode));
                command.Parameters.Add(DataAcessUtils.CreateParam("@CreditorNo", DbType.String, _creditorNo));
                

                DbParameter output = new SqlParameter();
                output.DbType = DbType.Boolean;
                output.ParameterName = "@OutPut";
                output.Direction = ParameterDirection.Output;
                command.Parameters.Add(output);
                command.ExecuteNonQuery();
                result = Convert.ToBoolean(output.Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}
