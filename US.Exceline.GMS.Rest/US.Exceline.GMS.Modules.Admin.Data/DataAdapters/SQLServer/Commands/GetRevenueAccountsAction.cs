﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Payments;
using System.Data.Common;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetRevenueAccountsAction : USDBActionBase<List<AccountDC>>
    {
        public GetRevenueAccountsAction()
        {
        }

        protected override List<AccountDC> Body(System.Data.Common.DbConnection connection)
        {
            List<AccountDC> accountList = new List<AccountDC>();
            string spName = "USExceGMSAdminGetRevenueAccounts";
            DbDataReader reader = null;
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    AccountDC item = new AccountDC();
                    item.ID = Convert.ToInt32(reader["AccountID"]);
                    item.AccountNo = reader["AccountNo"].ToString();
                    item.Name = reader["Name"].ToString();                  
                    accountList.Add(item);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
            return accountList;
        }
    }
}
