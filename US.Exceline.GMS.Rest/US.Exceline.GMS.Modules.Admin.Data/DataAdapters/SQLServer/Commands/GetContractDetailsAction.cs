﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : JNI
// Created Timestamp : "4/1/2012 10:39:15 AM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageContracts;
using System.Data.Common;
using System.Data;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    class GetContractDetailsAction : USDBActionBase<List<PackageDC>>
    {
        private int _branchId = 0;
        private string _searchText = string.Empty;
        private string _searchCriteria = string.Empty;
        private int _searchType;

        public GetContractDetailsAction(int branchId, string searchText, int searchType, string searchCriteria)
        {
            _branchId = branchId;
            _searchType = searchType;
            _searchText = searchText.ToUpper();
            if (!string.IsNullOrEmpty(searchCriteria))
            {
                _searchCriteria = searchCriteria.ToUpper();
            }
        }

        protected override List<PackageDC> Body(System.Data.Common.DbConnection connection)
        {
            List<PackageDC> packageList = new List<PackageDC>();
            string StoredProcedureName = "USExceGMSAdminGetContracts";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@searchText", DbType.String, _searchText));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@searchType", DbType.Int32, _searchType));
                if (!string.IsNullOrEmpty(_searchCriteria.Trim()))
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@SearchCriteria", DbType.String, _searchCriteria));
                }

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    PackageDC package = new PackageDC();
                    package.ContractTypeValue = new US.GMS.Core.DomainObjects.Common.CategoryDC();
                    package.PackageId = Convert.ToInt32(reader["Id"]);
                    package.TemplateNumber = Convert.ToString(reader["TemplateNo"]);
                    try
                    {
                        package.IntTemplateNumber = Convert.ToInt32(reader["TemplateNo"]);
                    }
                    catch { }
                    package.PackageName = reader["Name"].ToString();
                    package.BranchId = Convert.ToInt32(reader["BranchId"]);
                    package.ContractTypeValue.Code = Convert.ToString(reader["PackageType"].ToString());
                    package.ContractTypeValue.Id = Convert.ToInt32(reader["PackageTypeId"]);
                    package.ContractTypeValue.Name = Convert.ToString(reader["PackageTypeName"]);
                    if (reader["StartDate"] != DBNull.Value)
                        package.StartDate = Convert.ToDateTime(reader["StartDate"]);
                    if (reader["EndDate"] != DBNull.Value)
                        package.EndDate = Convert.ToDateTime(reader["EndDate"]);
                    package.NoOfMonths = Convert.ToInt32(reader["NoOfMonths"]);
                    package.LockInPeriodUntilDate = Convert.ToDateTime(reader["LockInPeriodUntilDate"]);
                    package.PackagePrice = Convert.ToDecimal(reader["PackagePrice"]);
                    package.NumOfInstallments = Convert.ToInt32(reader["NumberOfInstallments"]);
                    package.RatePerInstallment = Convert.ToDecimal(reader["RatePerInstallment"]);
                    package.EnrollmentFee = Convert.ToDecimal(reader["EnrollmentFee"]);
                    package.NumOfVisits = Convert.ToInt32(reader["NumberOfVisits"]);
                    package.LockInPeriod = Convert.ToInt32(reader["LockInPeriod"]);
                    package.PackageCategory = new US.GMS.Core.DomainObjects.Common.CategoryDC();
                    package.PackageCategory.Id = Convert.ToInt32(reader["ContractCategoryId"]);
                    package.ContractTypeId = Convert.ToInt32(reader["ContractCategoryId"]);
                    package.PriceGuaranty = Convert.ToInt32(reader["PriceGuaranty"]);
                    package.IsShownOnNet = Convert.ToBoolean(reader["IsShownOnTheNet"]);
                    package.RestPlusMonth = Convert.ToBoolean(reader["RestPlusMonth"]);
                    if (reader["FixDateOfContract"] != DBNull.Value)
                        package.FixDateOfContract = Convert.ToDateTime(reader["FixDateOfContract"]);
                    package.IsInvoiceDetail = Convert.ToBoolean(reader["InvoiceDetail"]);
                    package.AutoRenew = Convert.ToBoolean(reader["AutoRenew"]);
                    package.IsATG = Convert.ToBoolean(reader["IsATG"]);
                    package.CreditDueDays = Convert.ToInt32(reader["CreditDueDays"]);
                    package.NextTemplateId = Convert.ToInt32(reader["NextTemplateNo"]);
                    package.OrderPrice = Convert.ToDecimal(reader["OrderPrice"]);
                    package.StartUpItemPrice = Convert.ToDecimal(reader["StartupItemPrice"]);
                    package.EveryMonthItemsPrice = Convert.ToDecimal(reader["EveryMonthItemPrice"]);
                    package.NextContractTemplateName = Convert.ToString(reader["NextTemplateName"]);
                    package.NoOfMonths = Convert.ToInt32(reader["NoOfMonths"]);
                    package.MaxAge = Convert.ToInt32(reader["MaxAge"]);
                    if (reader["InStock"] != DBNull.Value)
                        package.InStock = Convert.ToInt32(reader["InStock"]);
                    if (reader["IsShownOnNet"] != DBNull.Value)
                    {
                        package.IsShownOnNet = Convert.ToBoolean(reader["IsShownOnNet"]);
                    }
                    package.ActivityName = Convert.ToString(reader["ActivityName"]);
                    package.ArticleId = Convert.ToInt32(reader["ArticleId"]);
                    if (reader["FirstDueDate"] != DBNull.Value)
                        package.FirstDueDate = Convert.ToDateTime(reader["FirstDueDate"]);
                    if (reader["SecondDueDate"] != DBNull.Value)
                        package.SecondDueDate = Convert.ToDateTime(reader["SecondDueDate"]);
                    if (reader["StartDateOfContract"] != DBNull.Value)
                        package.FixStartDateOfContract = Convert.ToDateTime(reader["StartDateOfContract"]);
                    package.SortingNo = Convert.ToInt32(reader["Sorting"]);
                    package.IsInStock = Convert.ToBoolean(reader["InStockVisible"]);
                    package.IsNoClasses = Convert.ToBoolean(reader["NoClasses"]);
                    if (Convert.ToInt32(reader["ContractType"]) == 0)
                    {
                        package.ContractType = "Member";
                        package.ContractTemplateType = ContractTemplateType.MEMBER;
                    }
                    else
                    {
                        package.ContractType = "Sponsor";
                        package.ContractTemplateType = ContractTemplateType.SPONSOR;
                    }
                    package.NoOfDays = Convert.ToInt32(reader["NoOfDays"]);
                    package.AccessProfileId = Convert.ToInt32(reader["AccessProfileId"]);
                    package.ATGStatus = Convert.ToInt32(reader["ATGStatus"]);
                    package.AmountForATG = Convert.ToDecimal(reader["AmountForATG"]);
                    package.ExpressAvailable = Convert.ToBoolean(reader["ExpressAvailable"]);
                    package.ActivityId = Convert.ToInt32(reader["ActivityId"]);
                    if (Convert.ToInt32(reader["MemberFeeArticleID"]) > 0)
                    {
                        ContractItemDC memberFeeArtilce = new ContractItemDC();
                        memberFeeArtilce.ArticleId = Convert.ToInt32(reader["MemberFeeArticleID"]);
                        memberFeeArtilce.Description = Convert.ToString(reader["MemberFeeArticleName"]);
                        memberFeeArtilce.ItemName = memberFeeArtilce.Description;
                        package.MemberFeeArticle = memberFeeArtilce;
                       
                    }
                    package.ServicePrice = Convert.ToDecimal(reader["ServiceAmount"]);
                    package.DiscountRate = (DiscountRateType)Enum.Parse(typeof(DiscountRateType), Convert.ToString(reader["DiscountRate"]));
                    package.ContractConditionId = Convert.ToInt32(reader["ContractConditionId"]);
                    package.ContractCondition = Convert.ToString(reader["ContractCondition"]);
                    packageList.Add(package);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return packageList;
        }

    }
}
