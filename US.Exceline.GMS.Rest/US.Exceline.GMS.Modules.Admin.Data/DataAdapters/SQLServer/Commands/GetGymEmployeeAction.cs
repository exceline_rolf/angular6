﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US_DataAccess;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using System.IO;
using US.GMS.Core.DomainObjects.ManageContracts;


namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetGymEmployeeAction : USDBActionBase<List<GymEmployeeDC>>
    {
        private readonly int _branchId = 0;
        private readonly string _searchText = string.Empty;
        private readonly bool _isActive = true;
        readonly List<GymEmployeeDC> _gymEmployeeList = new List<GymEmployeeDC>();
        private readonly int _roleId;
        public GetGymEmployeeAction(int branchId, string searchText, bool isActive,int roleId)
        {
            _branchId = branchId;
            _searchText = searchText;
            _isActive = isActive;
            _roleId = roleId;

          
        }

        protected override List<GymEmployeeDC> Body(DbConnection connection)
        {
            const string storedProcedureName = "USExceGMSAdminGetGymEmployees";          

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                if (!string.IsNullOrEmpty(_searchText))
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@searchText", DbType.String, _searchText));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@activeState", DbType.Boolean, _isActive));
                if (_roleId > 0)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@EmpRoleId", DbType.Int32, _roleId));
             
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var gymEmployee = new GymEmployeeDC();
                    gymEmployee.Id = Convert.ToInt32(reader["EmployeeId"]);
                    gymEmployee.EntNo = Convert.ToInt32(reader["EntNo"]);
                    gymEmployee.CustId = Convert.ToString(reader["CustId"]);
                    try
                    {
                        gymEmployee.IntCustId = Convert.ToInt32(reader["CustId"]);
                    }
                    catch { }
                    gymEmployee.FirstName = Convert.ToString(reader["FirstName"]);
                    gymEmployee.LastName = Convert.ToString(reader["LastName"]);
                    gymEmployee.Name = gymEmployee.FirstName + " " + gymEmployee.LastName;
                    gymEmployee.Email = reader["Email"].ToString();
                    gymEmployee.BirthDay = Convert.ToDateTime(reader["BirthDate"]);
                    if (!string.IsNullOrEmpty(reader["Address1"].ToString()))
                    {
                        gymEmployee.Address1 = Convert.ToString(reader["Address1"]);
                    }
                    if (!string.IsNullOrEmpty(reader["Address2"].ToString()))
                    {
                        gymEmployee.Address2 = Convert.ToString(reader["Address2"]);
                    }
                    if (!string.IsNullOrEmpty(reader["Address3"].ToString()))
                    {
                        gymEmployee.Address3 = Convert.ToString(reader["Address3"]);
                    }
                    if (!string.IsNullOrEmpty(reader["ZipCode"].ToString()))
                    {
                        gymEmployee.PostCode = Convert.ToString(reader["ZipCode"]);
                    }
                    if (!string.IsNullOrEmpty(reader["ZipName"].ToString()))
                    {
                        gymEmployee.PostPlace = Convert.ToString(reader["ZipName"]);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["TelMobile"])))
                    {
                        gymEmployee.Mobile = Convert.ToString(reader["TelMobile"]).Trim();
                    }
                    gymEmployee.MobilePrefix = Convert.ToString(reader["TelMobilePrefix"]).Trim();
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["TelWork"])))
                    {
                        gymEmployee.Work = Convert.ToString(reader["TelWork"]).Trim();
                    }
                    gymEmployee.WorkTeleNoPrefix = Convert.ToString(reader["TelWorkPrefix"]).Trim();
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["TelHome"])))
                    {
                        gymEmployee.Private = Convert.ToString(reader["TelHome"]).Trim();
                    }
                    gymEmployee.PrivateTeleNoPrefix = Convert.ToString(reader["TelHomePrefix"]).Trim();
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["Gender"])))
                    {
                        gymEmployee.Gender = Convert.ToString(reader["Gender"]);
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(reader["Description"])))
                    {
                        gymEmployee.Description = Convert.ToString(reader["Description"]);
                    }

                    gymEmployee.HomeBranchId = Convert.ToInt32(reader["HomeBranchId"]);
                    gymEmployee.HomeGymName = reader["HomeBranchName"].ToString();

                    gymEmployee.ActiveStatus = Convert.ToBoolean(reader["ActiveStatus"]);
                    gymEmployee.ImagePath = Convert.ToString(reader["ImagePath"]);
                    gymEmployee.EntityRoleType = "EMP";
                    gymEmployee.RoleId = reader["RoleId"].ToString();

                    if (reader["LinkMemberId"] != DBNull.Value)
                        gymEmployee.LinkMemberId = Convert.ToInt32(reader["LinkMemberId"]);
                    

                    if (!string.IsNullOrEmpty(gymEmployee.ImagePath))
                    {
                        gymEmployee.ProfilePicture = GetMemberProfilePicture(gymEmployee.ImagePath);
                    }
                    _gymEmployeeList.Add(gymEmployee);
                }
                reader.Close();
            }

            catch (Exception ex)
            {
                throw ex;
            }   
            return _gymEmployeeList;
        }

        



        private byte[] GetMemberProfilePicture(string p)
        {
            if (File.Exists(p))
            {
                byte[] byteArray = File.ReadAllBytes(p);
                return byteArray;
            }
            return new byte[0];
        }


        public GymEmployeeDC RunOnTransation(DbTransaction dbTransaction, GymEmployeeDC emp)
        {
            const string storedProcedureName = "USExceGMSAdminGetActivitiesForEmployee";

            try
            {

                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Connection = dbTransaction.Connection;
                cmd.Transaction = dbTransaction;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@id", DbType.Int32, emp.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@roleId", DbType.String, emp.RoleId));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var activityItem = new ActivityDC
                        {
                            Id = Convert.ToInt32(reader["ID"]),
                            Name = Convert.ToString(reader["Name"]),
                            BranchId = Convert.ToInt32(reader["BranchId"]),
                            IsActive = Convert.ToBoolean(reader["IsActive"])
                        };

                    emp.ActivityList.Add(activityItem);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return emp;

        }

    }
}
