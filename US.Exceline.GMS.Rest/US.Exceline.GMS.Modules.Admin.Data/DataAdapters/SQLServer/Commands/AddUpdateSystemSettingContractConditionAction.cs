﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer
{
    class AddUpdateSystemSettingContractConditionAction : USDBActionBase<int>
    {
        private readonly ContractCondition _contractCondition = new ContractCondition();

        public AddUpdateSystemSettingContractConditionAction(ContractCondition systemSettingContractCondition)
        {
            _contractCondition = systemSettingContractCondition;
        }

        protected override int Body(DbConnection connection)
        {
            var outputId = -1;
            const string storedProcedureName = "USExceGMSAdminAddUpdateSystemSettingContractCondition";
            
            try
            {
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, _contractCondition.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Name", DbType.String, _contractCondition.Name));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Condition", DbType.String, _contractCondition.Condition));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsDefault", DbType.Boolean, _contractCondition.IsDefault));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsDelete", DbType.Boolean, _contractCondition.IsDelete));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _contractCondition.User));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _contractCondition.BranchId));

                DbParameter para1 = new SqlParameter();
                para1.DbType = DbType.Int32;
                para1.ParameterName = "@outPutID";
                para1.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(para1);
                cmd.ExecuteNonQuery();
                outputId = Convert.ToInt32(para1.Value);
            }
            catch
            {
                throw;
            }
            return outputId;
        }
    }
}
