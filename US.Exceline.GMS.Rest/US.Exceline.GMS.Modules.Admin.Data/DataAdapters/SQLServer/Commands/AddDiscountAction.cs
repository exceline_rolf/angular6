﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Admin;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class AddDiscountAction : USDBActionBase<bool>
    {
        private ShopDiscountDC _newDiscount = new ShopDiscountDC();
        private bool _returnValue = false;

        public AddDiscountAction(ShopDiscountDC newDiscount)
        {
            this._newDiscount = newDiscount;
        }

        protected override bool Body(DbConnection connection)
        {
            string StoredProcedureNameAddDiscount = "[USExceGMSShopAddDiscount]";
            string StoredProcedureNameAddInventryDiscount = "USExceGMSShopAddInventoryDiscount";
            int outputId = -1;
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureNameAddDiscount);

                cmd.Parameters.Clear();
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Name", DbType.String, _newDiscount.Name));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Description", DbType.String, _newDiscount.Description));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Discount", DbType.Double, _newDiscount.Discount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartDate", DbType.DateTime, _newDiscount.StartDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndDate", DbType.DateTime, _newDiscount.EndDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _newDiscount.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsActive", DbType.Boolean, true));
                object obj = cmd.ExecuteScalar();
                outputId = Convert.ToInt32(obj);

                if (outputId != -1)
                {
                    DbCommand cmdItems = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureNameAddInventryDiscount);
                    foreach (int itemId in _newDiscount.ItemList)
                    {
                        cmdItems.Parameters.Clear();
                        cmdItems.Parameters.Add(DataAcessUtils.CreateParam("@ExceShopDiscountId", DbType.Int32, outputId));
                        cmdItems.Parameters.Add(DataAcessUtils.CreateParam("@ItemId", DbType.Int32, itemId));
                        cmdItems.ExecuteNonQuery();
                        _returnValue = true;
                    }
                }
            }
            catch
            {
                _returnValue = false;
            }

            return _returnValue;

        }
    }
}
