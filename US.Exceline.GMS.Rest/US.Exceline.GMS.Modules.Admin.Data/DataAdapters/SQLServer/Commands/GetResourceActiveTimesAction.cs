﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : JNI
// Created Timestamp : "3/29/2012 8:27:44 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.Admin.ManageResources;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    class GetResourceActiveTimesAction : USDBActionBase<List<ResourceActiveTimeDC>>
    {
        private int _branchId;
        private DateTime _startDate;
        private DateTime _endDate;
        private int _entNO;

        public GetResourceActiveTimesAction(int _branchId, DateTime _startDate, DateTime _endDate, int _entNO)
        {
            this._branchId = _branchId;
            this._startDate = _startDate;
            this._endDate = _endDate;
            this._entNO = _entNO;
        }

        protected override List<ResourceActiveTimeDC> Body(DbConnection connection)
        {
            List<ResourceActiveTimeDC> resourceActiveTimeList = new List<ResourceActiveTimeDC>();
            string storedProcedureName = "USExceGMSAdminGetResourceActiveTimes";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@startDate", DbType.DateTime, _startDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@endDate", DbType.DateTime, _endDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityNo", DbType.Int32, _entNO));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ResourceActiveTimeDC resourceActiveTime = new ResourceActiveTimeDC();
                    resourceActiveTime.Id = Convert.ToInt32(reader["ID"]);
                    resourceActiveTime.SheduleItemId = Convert.ToInt32(reader["ScheduleItemId"]);
                    if (reader["StartDateTime"] != DBNull.Value)
                        resourceActiveTime.StartDateTime = Convert.ToDateTime(reader["StartDateTime"]);
                    if (reader["EndDateTime"] != DBNull.Value)
                        resourceActiveTime.EndDateTime = Convert.ToDateTime(reader["EndDateTime"]);
                    resourceActiveTime.EntityId = Convert.ToInt32(reader["EntityId"]);
                    resourceActiveTime.EntityRoleType = Convert.ToString(reader["EntityRoleType"]);
                    if (!string.IsNullOrEmpty(reader["Name"].ToString()))
                    {
                        resourceActiveTime.Name = Convert.ToString(reader["Name"]);
                    }
                    resourceActiveTimeList.Add(resourceActiveTime);
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return resourceActiveTimeList;
        }
    }
}
