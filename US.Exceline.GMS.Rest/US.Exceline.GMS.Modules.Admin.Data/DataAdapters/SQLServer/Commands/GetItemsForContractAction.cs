﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS.Modules.Admin.Data
// Coding Standard   : US Coding Standards
// Author            : NHE
// Created Timestamp : "5/1/2012 5:37:44 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageContracts;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetItemsForContractAction : USDBActionBase<List<ContractItemDC>>
    {
        private int _contractId;
        private int _branchId;

        public GetItemsForContractAction(int contractId,int branchId)
        {
            _contractId = contractId;
            _branchId = branchId;
        }

        protected override List<ContractItemDC> Body(DbConnection connection)
        {
            List<ContractItemDC> contractItemList = new List<ContractItemDC>();
            string SPGetItems = "USExceGMSAdminGetItemsForContractTemplate";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, SPGetItems);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@contractId", DbType.Int32, _contractId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ContractItemDC item = new ContractItemDC();
                    item.CategoryId = Convert.ToInt32(reader["ID"]);
                    item.ItemName = Convert.ToString(reader["Name"]);
                    item.UnitPrice = Convert.ToDecimal(reader["UnitPrice"]);
                    item.ArticleId = Convert.ToInt32(reader["ArticleId"]);
                    item.ArticleNo = Convert.ToString(reader["ArticleNo"]);
                    item.CategoryId = Convert.ToInt32(reader["CategoryId"]);
                    item.IsStartUpItem = Convert.ToBoolean(reader["IsStartUpItem"]);
                    item.NumberOfOrders = Convert.ToInt32(reader["NumberOfOrders"]);
                    item.Quantity = Convert.ToInt32(reader["Units"]);
                    item.Price = Convert.ToDecimal(reader["Price"]);
                    item.IsActivityArticle = Convert.ToBoolean(reader["IsActivityArticle"]);
                    item.StartOrder = Convert.ToInt32(reader["StartOrder"]);
                    item.EndOrder = Convert.ToInt32(reader["EndOrder"]);
                    item.ActivityID = Convert.ToInt32(reader["ActivityId"]);
                //    item.SequenceID = Convert.ToInt32(reader["SequenceID"]);
                    contractItemList.Add(item);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return contractItemList;
        }
    }
}
