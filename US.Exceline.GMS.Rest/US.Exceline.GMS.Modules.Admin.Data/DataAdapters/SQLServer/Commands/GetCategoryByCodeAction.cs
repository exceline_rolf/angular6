﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Common;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetCategoryByCodeAction:USDBActionBase<CategoryDC>
    {
        private readonly string _categoryCode = string.Empty;

        public GetCategoryByCodeAction(string categoryCode)
        {
            _categoryCode = categoryCode;
        }

        protected override CategoryDC Body(DbConnection dbConnection)
        {
            var category = new CategoryDC();
            const string storedProcedureName = "USExceGMSGetCategoryByCode";
            var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
            cmd.Parameters.Add(DataAcessUtils.CreateParam("@CategoryCode", DbType.String, _categoryCode));

            var dbDataReader = cmd.ExecuteReader();
            try
            {
                while (dbDataReader.Read())
                {
                    category.Id = Convert.ToInt32(dbDataReader["ID"].ToString());
                    category.Code = dbDataReader["Code"].ToString();
                    category.Name = dbDataReader["Name"].ToString();
                    category.Color = dbDataReader["Color"].ToString();
                    category.Description = dbDataReader["Description"].ToString();
                    category.TypeId = Convert.ToInt32(dbDataReader["CategoryTypeId"].ToString());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return category;
        }
    }
}
