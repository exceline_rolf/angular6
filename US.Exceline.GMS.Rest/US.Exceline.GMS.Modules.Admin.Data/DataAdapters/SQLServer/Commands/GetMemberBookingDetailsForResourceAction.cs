﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
   public class GetMemberBookingDetailsForResourceAction : USDBActionBase<List<EntityActiveTimeDC>>
   {
       private int _scheduleItemId;
       private int _branchId;
       public GetMemberBookingDetailsForResourceAction(int branchId,int scheduleItemId)
       {
           _scheduleItemId = scheduleItemId;
           _branchId = branchId;

       }
        protected override List<EntityActiveTimeDC> Body(DbConnection connection)
        {
            List<EntityActiveTimeDC> activeTimesList = new List<EntityActiveTimeDC>();
            string spName = "USExceGMSAdminGetMemberBookingDetailsForResource";
            try
            {
               
                    DbCommand command = CreateCommand(CommandType.StoredProcedure, spName);
                    command.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@scheduleItemId", DbType.Int32, _scheduleItemId));
                    DbDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        EntityActiveTimeDC activeTime = new EntityActiveTimeDC();
                        activeTime.Id = (reader["ActiveTimeID"] != DBNull.Value) ? Convert.ToInt32(reader["ActiveTimeID"]) : -1;
                        activeTime.EntityId = Convert.ToInt32(reader["EntityId"]);
                        activeTime.EntityRoleType = Convert.ToString(reader["EntityRoleType"]);
                        activeTime.ScheduleParentId = Convert.ToInt32(reader["ParentId"]);
                        activeTime.EntityName = Convert.ToString(reader["EntityName"]);
                        if (reader["StartDateTime"] != DBNull.Value)
                            activeTime.StartDateTime = Convert.ToDateTime(reader["StartDateTime"]);
                        if (reader["EndDateTime"] != DBNull.Value)
                            activeTime.EndDateTime = Convert.ToDateTime(reader["EndDateTime"]);
                        if (reader["ScheduleItemId"] != DBNull.Value)
                            activeTime.SheduleItemId = Convert.ToInt32(reader["ScheduleItemId"]);
                        if (reader["ScheduleId"] != DBNull.Value)
                            activeTime.ScheduleId = Convert.ToInt32(reader["ScheduleId"]);
                        if (reader["ScheduleName"] != DBNull.Value)
                            activeTime.Name = Convert.ToString(reader["ScheduleName"]);
                        activeTime.MemberId = Convert.ToInt32(reader["MemberId"]);
                        
                        activeTimesList.Add(activeTime);
                    }
               
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return activeTimesList;

        }
    }
}
