﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetExtraResourcesForEmployeeAction : USDBActionBase<List<ResourceDC>>
    {
        private int _scheduleId;

        public  GetExtraResourcesForEmployeeAction(int schedulId)
        {
            _scheduleId = schedulId;
        }

        protected override List<ResourceDC> Body(System.Data.Common.DbConnection connection)
        {


            List<ResourceDC> resourcrList = new List<ResourceDC>();
            string storedProcedure2 = "USGMSAdminGetEmployeeOtherResources";

            try
            {
                DbCommand cmd2 = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure2);
                cmd2.Parameters.Add(DataAcessUtils.CreateParam("@EmployeeId", DbType.Boolean, _scheduleId));
                DbDataReader reader2 = cmd2.ExecuteReader();

                while (reader2.Read())
                {
                    ResourceDC resource = new ResourceDC();
                    resource.Id = Convert.ToInt32(reader2["ID"]);
                    resource.Name = Convert.ToString(reader2["Name"]);
                    resourcrList.Add(resource);
                }

            }
            catch
            {
                throw;
            }
            return resourcrList;
        }
    }
}
