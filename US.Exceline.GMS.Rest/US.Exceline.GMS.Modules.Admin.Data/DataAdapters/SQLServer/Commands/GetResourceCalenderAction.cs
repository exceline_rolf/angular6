﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US.GMS.Core.DomainObjects.Common;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetResourceCalenderAction : USDBActionBase<List<ResourceDC>>
    {
        private readonly int _branchId;
        private readonly int _hit;
        private readonly string _gymCode = string.Empty;

        public GetResourceCalenderAction(int branchId, string gymCode,int hit)
        {
            _branchId = branchId;
            _hit = hit;
            _gymCode = gymCode;
        }
        protected override List<ResourceDC> Body(DbConnection connection)
        {
            var resourceLst = new List<ResourceDC>();
            const string storedProcedureName = "USExceGMSAdminGetResourceCalender";
            DbDataReader reader = null;
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Hit", DbType.Int32, _hit));

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var resource = new ResourceDC();
                    resource.Id = Convert.ToInt32(reader["ResourceId"]);
                    resource.BranchId = Convert.ToInt32(reader["BranchId"]);
                    resource.Name = reader["Name"].ToString();
                    resource.Description = reader["Description"].ToString();
                    resource.CreatedDate = Convert.ToDateTime(reader["CreatedTime"]);
                    resource.CreatedUser = reader["CreatedUser"].ToString();
                    resource.ModifiedDate = Convert.ToDateTime(reader["LastModifiedDate"]);
                    resource.LastModifiedUser = reader["ModifiedUser"].ToString();
                    resource.ActiveStatus = Convert.ToBoolean(reader["ActiveStatus"]);
                    resource.ResId = Convert.ToString(reader["ResId"]);
                    var categoryDc = new CategoryDC
                        {
                            Id = Convert.ToInt32(reader["CategoryId"]),
                            Code = reader["CategoryCode"].ToString(),
                            Name = reader["CategoryDescription"].ToString()
                        };
                    resource.ResourceCategory = categoryDc;

                    var timeCategoryDc = new CategoryDC
                        {
                            Id = Convert.ToInt32(reader["TimeCategoryId"]),
                            Code = reader["TimeCategoryCode"].ToString(),
                            Name = reader["TimeCategoryName"].ToString()
                        };
                    resource.TimeCategory = timeCategoryDc;

                    if (reader["SalaryPerBooking"] != DBNull.Value)
                        resource.IsSalaryPerBooking = Convert.ToBoolean(reader["SalaryPerBooking"]);

                    if (reader["PurchasedDate"] != DBNull.Value)
                        resource.PurchasedDate = Convert.ToDateTime(reader["PurchasedDate"]);

                    if (reader["ServiceDate"] != DBNull.Value)
                        resource.MaintenanceDate = Convert.ToDateTime(reader["ServiceDate"]);

                    resource.ActivityId = Convert.ToInt32(reader["ActivityId"]);
                    resource.ActivityName = reader["ActivityName"].ToString();
                    resource.DaysForBookingPerActivity = Convert.ToInt32(reader["DaysForBooking"]);
                    resource.EquipmentId = Convert.ToInt32(reader["EquipmentId"]);
                    resource.ActivitySmsReminder = Convert.ToBoolean(reader["SmsReminder"]);
                    resource.IsEquipment = resource.EquipmentId != 0;

                    var action = new GetEmpolyeeForResourceAction(resource.Id);
                    resource.EmpList = action.Execute(EnumDatabase.Exceline, _gymCode);

                    // var actionSchedule = new GetResourceScheduleItemsAction(resource.Id, "RES", _gymCode);
                    // resource.Schedule = actionSchedule.Execute(EnumDatabase.Exceline, _gymCode);
                    if (resource.ActivityId > 0)
                    {
                        var actionActivityTime = new GetActivityUnavailableTimeAction(_branchId, resource.ActivityId);
                        resource.ActivityTimeLst = actionActivityTime.Execute(EnumDatabase.Exceline, _gymCode);
                    }


                    if (resource.EmpList != null && resource.EmpList.ToList().Any())
                    {
                        resource.EmpCount = resource.EmpList.Count();
                        resource.IsHasEmp = true;
                    }


                    resourceLst.Add(resource);
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }
           
            return resourceLst;
        }
    }
}
