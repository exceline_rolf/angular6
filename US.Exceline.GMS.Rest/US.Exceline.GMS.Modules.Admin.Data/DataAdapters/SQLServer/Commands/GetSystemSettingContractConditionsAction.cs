﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetSystemSettingContractConditionsAction : USDBActionBase<List<ContractCondition>>
    {
        protected override List<ContractCondition> Body(DbConnection connection)
        {
            var contractConditionList = new List<ContractCondition>();
            try
            {
                const string storedProcedure = "USExceGMSAdminGetSystemSettingContractConditions";
                var cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var condition = new ContractCondition
                    {
                        Id = Convert.ToInt32(reader["Id"]),
                        Name = Convert.ToString(reader["Name"]),
                        Condition = Convert.ToString(reader["Condition"]),
                        IsDefault = Convert.ToBoolean(reader["IsDefault"])
                    };
                    contractConditionList.Add(condition);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return contractConditionList;
        }
    }
}
