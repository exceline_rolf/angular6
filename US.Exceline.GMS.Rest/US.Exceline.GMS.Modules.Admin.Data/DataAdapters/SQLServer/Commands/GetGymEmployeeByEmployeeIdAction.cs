﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using US.GMS.Core.SystemObjects;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using System.Data.Common;
using System.Data;
using System.IO;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.ResultNotifications;
using US.Common.Logging.API;
using US.GMS.API;
using US.Common.Web.UI.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class GetGymEmployeeByEmployeeIdAction : USDBActionBase<GymEmployeeDC>
    {
        private int _branchId = 0;
        private string _user = string.Empty;
        private int _employeeId = -1;
        private string _gymCode = string.Empty;

        public GetGymEmployeeByEmployeeIdAction(int branchId, string user, int employeeId, string gymCode)
        {
            _branchId = branchId;
            _user = user;
            _employeeId = employeeId;
            _gymCode = gymCode;
        }

        protected override GymEmployeeDC Body(DbConnection connection)
        {
            string StoredProcedureName = "USExceGMSAdminGetGymEmployeeByGymEmployeeId";
            GymEmployeeDC gymEmployee = new GymEmployeeDC();
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@gymemployeeId", DbType.Int32, _employeeId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {

                    gymEmployee.Id = Convert.ToInt32(reader["EmployeeId"]);
                    gymEmployee.CustId = Convert.ToString(reader["CustId"]);
                    gymEmployee.EntNo = Convert.ToInt32(reader["EntNo"]);
                   // gymEmployee.BranchId = Convert.ToInt32(reader["BranchId"]);
                    gymEmployee.FirstName = Convert.ToString(reader["FirstName"]);
                    gymEmployee.LastName = Convert.ToString(reader["LastName"]);
                    gymEmployee.Name = gymEmployee.FirstName + " " + gymEmployee.LastName;
                    gymEmployee.Email = reader["Email"].ToString();
                    gymEmployee.BirthDay = Convert.ToDateTime(reader["BirthDate"]);
                    gymEmployee.Address1 = Convert.ToString(reader["Address"]);
                    gymEmployee.Address2 = Convert.ToString(reader["Address2"]);
                    gymEmployee.Address3 = Convert.ToString(reader["Address3"]);
                    gymEmployee.PostCode = Convert.ToString(reader["ZipCode"]);
                    gymEmployee.PostPlace = Convert.ToString(reader["ZipName"]);
                    gymEmployee.Mobile = Convert.ToString(reader["TelMobile"]).Trim();
                    gymEmployee.MobilePrefix = Convert.ToString(reader["TelMobilePrefix"]).Trim();
                    gymEmployee.Work = Convert.ToString(reader["TelWork"]).Trim();
                    gymEmployee.WorkTeleNoPrefix = Convert.ToString(reader["TelWorkPrefix"]).Trim();
                    gymEmployee.Private = Convert.ToString(reader["TelHome"]).Trim();
                    gymEmployee.PrivateTeleNoPrefix = Convert.ToString(reader["TelHomePrefix"]).Trim();
                    gymEmployee.EmployeeCardNo = Convert.ToString(reader["EmpCardNo"]);
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["Gender"])))
                    {
                        try
                        {
                            gymEmployee.Gender = Convert.ToString(reader["Gender"]);
                        }
                        catch
                        {
                        }
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(reader["Description"])))
                    {
                        gymEmployee.Description = Convert.ToString(reader["Description"]);
                    }
                    gymEmployee.ActiveStatus = Convert.ToBoolean(reader["ActiveStatus"]);
                    gymEmployee.ImagePath = Convert.ToString(reader["ImagePath"]);
                    gymEmployee.EntityRoleType = "EMP";
                    gymEmployee.RoleId = reader["RoleId"].ToString();
                    if (reader["TaskMinDate"] != DBNull.Value)
                    gymEmployee.TaskMinDate =  Convert.ToDateTime(reader["TaskMinDate"]);
                    if (reader["TaskMaxDate"] != DBNull.Value)
                    gymEmployee.TaskMaxDate =  Convert.ToDateTime(reader["TaskMaxDate"]);
                    gymEmployee.AssignedEntityId = Convert.ToInt32(reader["AssignedEntityId"]);
                    if (reader["Startdate"] != DBNull.Value)
                    gymEmployee.StartDate = Convert.ToDateTime(reader["Startdate"]);
                    if (reader["Enddate"] != DBNull.Value)
                    gymEmployee.EndDate = Convert.ToDateTime(reader["Enddate"]);
                    gymEmployee.SourceEntity = Convert.ToInt32(reader["SourceEntity"]);
                    gymEmployee.AssignedEntityName = reader["AssignedEntityName"].ToString();
                    gymEmployee.HomeBranchId = Convert.ToInt32(reader["HomeBranchId"]);
                    gymEmployee.HomeGymName = reader["HomeBranchName"].ToString();
                    gymEmployee.PayrollNumber = reader["PayrollNo"].ToString();
                    if (reader["MemberId"] != DBNull.Value)
                    gymEmployee.MemberId = Convert.ToInt32(reader["MemberId"]);

                    var action = new GetActivitiesForEmployeeAction(gymEmployee.BranchId, gymEmployee.Id, gymEmployee.RoleId);
                    try
                    {
                        gymEmployee.ActivityList = action.Execute(EnumDatabase.Exceline, _gymCode);
                    }
                    catch
                    {
                    }
                    var actionForCategory = new GetCategoryForEmployeeAction(gymEmployee.BranchId, gymEmployee.Id);
                    try
                    {
                        gymEmployee.CategoryList = actionForCategory.Execute(EnumDatabase.Exceline, _gymCode);
                    }
                    catch
                    {
                    }
                    if (!string.IsNullOrEmpty(gymEmployee.ImagePath))
                    {
                        gymEmployee.ProfilePicture = GetMemberProfilePicture(gymEmployee.ImagePath);
                    }

                    try
                    {
                        gymEmployee.Schedule = GetEntitySchedule(gymEmployee.Id, gymEmployee.RoleId, _user, _gymCode);

                    }
                    catch
                    {
                    }
                }
                reader.Close();

                try
                {
                    const string storedProcedureName2 = "USExceGMSAdminGetEmployeeRoles";
                    DbCommand cmd2 = CreateCommand(CommandType.StoredProcedure, storedProcedureName2);
                    cmd2.Parameters.Add(DataAcessUtils.CreateParam("@employeeId", DbType.Int32, gymEmployee.Id));
                    DbDataReader reader2 = cmd2.ExecuteReader();
                    while (reader2.Read())
                    {
                        var excelineRole = new ExcelineRoleDc();
                        excelineRole.RoleId = Convert.ToInt32(reader2["ID"]);
                        excelineRole.RoleName = Convert.ToString(reader2["RoleName"]);
                        excelineRole.IsActive = true;
                        if (reader2["BranchIds"] != DBNull.Value)
                            reader2["BranchIds"].ToString().Trim().Split(',').ToList().ForEach(x =>
                            {
                                if (excelineRole.BranchList == null) excelineRole.BranchList = new List<int>();
                                excelineRole.BranchList.Add(Convert.ToInt32(x));
                            });
                        gymEmployee.ExceEmpRoleList.Add(excelineRole);
                    }
                    reader2.Close();
                }
                catch
                {

                }
                     
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return gymEmployee;
        }

        private byte[] GetMemberProfilePicture(string p)
        {
            if (File.Exists(p))
            {
                byte[] byteArray = System.IO.File.ReadAllBytes(p);
                return byteArray;
            }
            return new byte[0];
        }


        public ScheduleDC GetEntitySchedule(int entityId, string entityRoleType, string user, string gymCode)
        {
            OperationResult<ScheduleDC> result = GMSSchedule.GetEntitySchedule(entityId, entityRoleType, user, gymCode);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new ScheduleDC();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }
    }
}

