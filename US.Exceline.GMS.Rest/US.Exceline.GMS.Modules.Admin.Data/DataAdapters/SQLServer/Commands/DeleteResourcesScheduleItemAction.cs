﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class DeleteResourcesScheduleItemAction : USDBActionBase<int>
    {
        private int _scheduleItemId;
        public DeleteResourcesScheduleItemAction(int scheduleItemId)
        {
            _scheduleItemId = scheduleItemId;
        }
        protected override int Body(DbConnection connection)
        {
            int result;
            const string storedProcedureName = "USExceGMSAdminDeleteResourcesScheduleItem";
            try
            {

                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleItemId", DbType.Int32, _scheduleItemId));
                DbParameter output = new SqlParameter();
                output.DbType = DbType.Int32;
                output.ParameterName = "@OutPutID";
                output.Size = 50;
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);
                cmd.ExecuteNonQuery();
                result = Convert.ToInt32(output.Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        
        }
    }
}
