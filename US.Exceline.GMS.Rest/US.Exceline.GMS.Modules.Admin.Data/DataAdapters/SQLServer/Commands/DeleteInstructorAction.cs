﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using US_DataAccess;
using System.Data.Common;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class DeleteInstructorAction : USDBActionBase<bool>
    {
        private int _instructorId;
        private bool _isActive;
        public DeleteInstructorAction(int instructorId, bool isActive)
        {
            _instructorId = instructorId;
            _isActive = isActive;
        }

        protected override bool Body(DbConnection connection)
        {
            bool result = false;
            string StoredProcedureName = "USExceGMSAdminDeleteInstructor";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@instructorId", System.Data.DbType.Int32, _instructorId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@activeState", System.Data.DbType.Int32, _isActive));
                cmd.ExecuteScalar();
                result = true;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException("", ex);
            }
            return result;

        }
    }
}
