﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS.Modules.Admin.Data
// Coding Standard   : US Coding Standards
// Author            : NHE
// Created Timestamp : "5/21/2012 5:37:44 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using US_DataAccess;
using System.Data.Common;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class DeleteEntityTimeEntryAction : USDBActionBase<bool>
    {
        private int _timeEntryId;
        private int _taskId;
        private decimal _timeSpent;

        public DeleteEntityTimeEntryAction(int timeEntryId, int taskId, decimal timeSpent)
        {
            _timeEntryId = timeEntryId;
            _taskId = taskId;
            _timeSpent = timeSpent;
        }

        protected override bool Body(DbConnection connection)
        {
            bool result = false;
            string StoredProcedureName = "USExceGMSAdminDeleteEntityTimeEntry";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@timeEntryId", System.Data.DbType.Int32, _timeEntryId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@taskId", System.Data.DbType.Int32, _taskId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@spendTime", System.Data.DbType.Decimal, _timeSpent));
                object obj = cmd.ExecuteScalar();

                int res = Convert.ToInt32(obj);
                if (res > 0)
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}
