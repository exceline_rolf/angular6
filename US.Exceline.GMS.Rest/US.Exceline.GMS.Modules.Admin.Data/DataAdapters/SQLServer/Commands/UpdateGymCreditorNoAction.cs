﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System.Data.Common;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateGymCreditorNoAction : USDBActionBase<bool>
    {
        private int _branchId = -1;
        private string _gymCode = string.Empty;
        private int _creditorNo = -1;
         private string _branchName = string.Empty;
        public UpdateGymCreditorNoAction(int branchId, string gymCode, int creditorNo,string branchName)
        {
            _branchId = branchId;
            _gymCode = gymCode;
            _creditorNo = creditorNo;
            _branchName = branchName;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string spName = "ExceWorkStationUpdateBranchCreditorNo";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", System.Data.DbType.Int32, _branchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchName", System.Data.DbType.String, _branchName));
                command.Parameters.Add(DataAcessUtils.CreateParam("@GymCode", System.Data.DbType.String, _gymCode));
                command.Parameters.Add(DataAcessUtils.CreateParam("@CreditorNo", System.Data.DbType.String, _creditorNo));

                command.ExecuteNonQuery();
                return true;
            }
            catch 
            {
                throw;
            }
        }
    }
}
