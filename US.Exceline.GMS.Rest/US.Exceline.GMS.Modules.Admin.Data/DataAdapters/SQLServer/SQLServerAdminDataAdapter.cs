﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US.Exceline.GMS.Modules.Admin.Data.SystemObjects;
using US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands;
using US.GMS.Core.DomainObjects.AccessControl;
using US.GMS.Core.DomainObjects.AccessControl.ARX;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.DomainObjects.Admin;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.Admin.HolydayManagement;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US.GMS.Core.DomainObjects.Payments;
using US.Payment.Data.Admin.EconomySettings.DBCommands;
using US.GMS.Core.Utils;
using System.Linq;
using US.GMS.Core.DomainObjects.Admin.ManageAnonymizing;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using System.Diagnostics;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer
{
    public class SQLServerAdminDataAdapter : IAdminDataAdapter
    {
        #region IAdminDataAdapter Instructors

        public List<InstructorDC> GetInstructors(int branchId, string user, bool isActive, int instructorId, string gymCode)
        {
            GetInstructorDetailsAction action = new GetInstructorDetailsAction(branchId, user, isActive, instructorId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
        #endregion

        #region IAdminDataAdapter Trainers


        public List<TrainerDC> GetTrainers(int branchId, string searchText, bool isActive, int _trainerId, string gymCode)
        {
            GetTrainerDetailsAction action = new GetTrainerDetailsAction(branchId, searchText, isActive, _trainerId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
        #endregion

        #region IAdminGymEmployee

        public string SaveGymEmployee(GymEmployeeDC employee, string gymCode)
        {
            SaveGymEmployeeAction action = new SaveGymEmployeeAction(employee);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int UpdateGymEmployee(GymEmployeeDC employee, int branchId, string user, string gymCode)
        {
            return 1;
        }

        //public bool DeleteGymEmployee(int gymEmployeeId, int branchId, string user, string gymCode)
        //{
        //    return false;
        //}

        public bool DeActivateGymEmployee(int gymEmployeeId, int branchId, string user, string gymCode)
        {
            throw new NotImplementedException();
        }

        public List<GymEmployeeDC> GetGymEmployees(int branchId, string searchText, bool isActive, int roleId, string gymCode)
        {
            GetGymEmployeeAction action = new GetGymEmployeeAction(branchId, searchText, isActive, roleId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public GymEmployeeDC GetGymEmployeeById(int branchId, int employeeId, string gymCode)
        {
            GetGymEmployeeByIdAction action = new GetGymEmployeeByIdAction(branchId, employeeId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public GymEmployeeDC GetGymEmployeeByEmployeeId(int branchId, string user, int employeeId, string gymCode)
        {
            GetGymEmployeeByEmployeeIdAction action = new GetGymEmployeeByEmployeeIdAction(branchId, user, employeeId, gymCode);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool ValidateEmployeeFollowUp(int employeeId,DateTime endDate, string gymCode)
        {
            var action = new ValidateEmployeeFollowUpAction(employeeId, endDate);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
        #endregion

        #region Resource

        public int ChangeResourceOnBooking(int scheduleItemId, int resourceId, string comment, string gymCode, string user, int branchId)
        {
            ChangeResourceOnBookingAction action = new ChangeResourceOnBookingAction(scheduleItemId, resourceId, comment, user, branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int UpdateContractVisits(int selectedContractId, int punchedContractId, int bookingId, string gymCode, string user)
        {
            UpdateContractVisitsAction action = new UpdateContractVisitsAction(selectedContractId, punchedContractId, bookingId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ResourceBookingElemnt> GetResourceBooking(int scheduleId, string _startDateTime, string _endDateTime, string gymCode, string user)
        {
            GetResourceBookingAction action = new GetResourceBookingAction(scheduleId, _startDateTime, _endDateTime);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ContractInfo> GetPunchcardContractsOnMember(int memberId, string activity, string gymCode, string user)
        {
            GetPunchcardContractsOnMemberAction action = new GetPunchcardContractsOnMemberAction(memberId, activity);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int IsArticlePunchcard(int articleId, string gymCode, string user)
        {
            IsArticlePunchcardAction action = new IsArticlePunchcardAction(articleId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int SaveResources(ResourceDC resource, string user, string gymCode)
        {
            SaveResourceAction action = new SaveResourceAction(resource);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool UpdateResource(ResourceDC resource, string user, string gymCode)
        {
            return false;
        }

        public bool DeleteResource(int branchId, string user, string gymCode)
        {
            return false;
        }

        public bool DeActivateResource(int resourceId, int branchId, string user, string gymCode)
        {
            DeActivateResourceAction action = new DeActivateResourceAction(resourceId, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool SwitchResource(int resourceTd1, int resourceId2, DateTime startDate, DateTime endDate, string comment, int branchId, string user, string gymCode)
        {
            SwitchResourceAction action = new SwitchResourceAction(resourceTd1, resourceId2, startDate, endDate, comment, branchId, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool SwitchResourceUndo(int resId, DateTime startDate, DateTime endDate, string user, int branchId,string gymCode)
        {
            SwitchResourceUndoAction action = new SwitchResourceUndoAction(resId,startDate,endDate,user,branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
            
        public bool ValidateScheduleWithResourceId(int resourceId, DateTime startDate, DateTime endDate, int branchId, string user, string gymCode)
        {
            ValidateScheduleWithResourceIdAction action = new ValidateScheduleWithResourceIdAction(resourceId, startDate, endDate, branchId, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool ValidateScheduleWithSwitchResource(int resourceId1, int resourceId2, DateTime startDate, DateTime endDate, int branchId, string user, string gymCode)
        {
            ValidateScheduleWithSwitchResourceAction action = new ValidateScheduleWithSwitchResourceAction(resourceId1, resourceId2, startDate, endDate, branchId, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ScheduleItemDC> GetBookingsOnDeletedScheduleItem(int resourceId, string gymCode)
        {
            GetBookingsOnDeletedScheduleItemAction action = new GetBookingsOnDeletedScheduleItemAction(resourceId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }



        public List<ResourceDC> GetResources(int branchId, string searchText, int categoryId, int activityId, int equipmentid, bool isActive, string gymCode)
        {
            GetResourceDetailsAction action = new GetResourceDetailsAction(branchId, searchText, categoryId, activityId, equipmentid, isActive, gymCode);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public ResourceDC GetResourceDetailsById(int resourceId, string gymCode)
        {
            var action = new GetResourceDetailsByIdAction(resourceId, gymCode);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ResourceDC> GetResourceCalender(int branchId, string gymCode, int hit)
        {
            var action = new GetResourceCalenderAction( branchId,  gymCode, hit);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        List<ResourceActiveTimeDC> IAdminDataAdapter.GetResourceActiveTimes(int branchId, DateTime startTime, DateTime endTime, int entNO, string gymCode)
        {
            GetResourceActiveTimesAction action = new GetResourceActiveTimesAction(branchId, startTime, endTime, entNO);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        List<AvailableResourcesDC> IAdminDataAdapter.GetAvailableResources(int branchId, int resId, string gymCode)
        {
            GetAvailableResourcesAction action = new GetAvailableResourcesAction(branchId, resId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<AvailableResourcesDC> GetResourceAvailableTimeById(int branchId, int resId, string gymCode)
        {
            GetResourceAvailableTimeByIdAction action = new GetResourceAvailableTimeByIdAction(branchId, resId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

      

        public List<string> ValidateResourcesWithSchedule(string roleType, List<AvailableResourcesDC> resourceList, int branchId, string gymCode)
        {
            List<string> result = new List<string>();

            foreach (var item in resourceList)
            {
                ValidateResourcesWithScheduleAction action = new ValidateResourcesWithScheduleAction(roleType, item, branchId);
                string temp = action.Execute(EnumDatabase.Exceline, gymCode);
                result.Add(temp);
            }
            return result;
        }

        public bool AddOrUpdateAvailableResources(List<AvailableResourcesDC> resourceList, int branchId, string user)
        {
            bool result = false;
            List<string> days = new List<string>() { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };
            
            foreach (var item in resourceList)
            {
                if (item.Day == "All")
                {
                    foreach (var day in days)
                    {
                        item.Day = day;
                        AddOrUpdateAvailableResourcesAction action = new AddOrUpdateAvailableResourcesAction(item, branchId, user);
                        bool temp = action.Execute(EnumDatabase.Exceline, ExceConnectionManager.GetGymCode(user));
                        result = (result && temp);
                    }
                }
                else
                {
                    AddOrUpdateAvailableResourcesAction action = new AddOrUpdateAvailableResourcesAction(item, branchId, user);
                    bool temp = action.Execute(EnumDatabase.Exceline, ExceConnectionManager.GetGymCode(user));
                    result = (result && temp);
                }
            }
            return result;
        }

       public List<int> GetEmpolyeeForResources(List<int> resIdList, string gymCode)
       {
            var empIdList = new List<int>();
            foreach (var item in resIdList)
            {
                var action = new GetEmpolyeeForResourceAction(item);
                Dictionary<int, string> empList = action.Execute(EnumDatabase.Exceline, gymCode);
                if (empList != null) empList.ToList().ForEach(x =>  empIdList.Add(x.Key));
            }
            return empIdList.Distinct().ToList();
       }

       public List<int> ValidateResWithSwitchDataRange(List<int> resourceIds,DateTime startDate,DateTime endDate,string gymCode)
        {
            var action = new ValidateResWithSwitchDataRangeAction(resourceIds, startDate, endDate);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        #endregion

        #region Contract
        public int SaveContract(PackageDC package, string user, string gymCode)
        {
            SaveContractAction action = new SaveContractAction(package,user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int UpdateContractPriority(Dictionary<int, int> packages, string user, string gymCode)
        {
            UpdateContractPriorityAction action = new UpdateContractPriorityAction(packages);
            action.Execute(EnumDatabase.Exceline, gymCode);
            return 1;
        }

        public bool UpdateContract(PackageDC package, string user)
        {
            return false;
        }

        public bool DeActivateContract(int resourceId, int branchId, string user)
        {
            return false;
        }

        public List<PackageDC> GetContracts(int branchId, string searchText, int searchType, string searchCriteria, string gymCode)
        {
            try
            {
                GetContractDetailsAction action = new GetContractDetailsAction(branchId, searchText, searchType, searchCriteria);
                List<PackageDC> packageList = action.Execute(EnumDatabase.Exceline, gymCode);
                foreach (PackageDC package in packageList)
                {
                    GetItemsForContractAction action2 = new GetItemsForContractAction(package.PackageId, branchId);
                    List<ContractItemDC> contractItemList = action2.Execute(EnumDatabase.Exceline, gymCode);
                    foreach (ContractItemDC contractItem in contractItemList)
                    {
                        contractItem.IsSelected = true;
                        if (contractItem.IsStartUpItem)
                        {
                            package.StartUpItemList.Add(contractItem);
                        }
                        else
                        {
                            package.EveryMonthItemList.Add(contractItem);
                        }
                    }

                    GetBranchesForTemplateAction action3 = new GetBranchesForTemplateAction(package.PackageId);
                    List<int> branchIdList = action3.Execute(EnumDatabase.Exceline, gymCode);
                    package.BranchIdList = branchIdList;

                    GetRegionsForTemplateAction action4 = new GetRegionsForTemplateAction(package.PackageId);
                    List<int> regionIdList = action4.Execute(EnumDatabase.Exceline, gymCode);
                    package.RegionIdList = regionIdList;

                    GetCountriesForTemplateAction action5 = new GetCountriesForTemplateAction(package.PackageId);
                    List<string> countryIdList = action5.Execute(EnumDatabase.Exceline, gymCode);
                    package.CountryIdList = countryIdList;
                }
                return packageList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetNextContractId(int branchId, string gymCode)
        {
            GetNextContractIdAction action = new GetNextContractIdAction(branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int DeleteContract(int contractId, string gymCode)
        {
            DeleteContractAction action = new DeleteContractAction(contractId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<PriceItemTypeDC> GetPriceItemTypes(int branchId, string gymCode)
        {
            GetPriceItemTypesAction action = new GetPriceItemTypesAction(branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        #endregion

        #region Contract item
        public List<ContractItemDC> GetContractItems(int branchId, string gymCode)
        {
            GetContractItemsAction action = new GetContractItemsAction(branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool AddContractItem(ContractItemDC contractItem, string user, int branchId, string gymCode)
        {
            SaveContractItemAction action = new SaveContractItemAction(contractItem, user, branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }


        public bool RemoveContractItem(int contractItemID, string user, int branchId, string gymCode)
        {
            RemoveContractItemAction action = new RemoveContractItemAction(contractItemID, user, branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        #endregion

        #region Employee
        public string UpdateEmployees(EmployeeDC employeeDc, string user, string gymCode)
        {
            GymEmployeeDC gymEmployee = (GymEmployeeDC)employeeDc;
            SaveGymEmployeeAction action = new SaveGymEmployeeAction(gymEmployee);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public string UpdateMemeberActiveness(int trainerId, string rolId, int branchId, string user, bool isActive, string gymCode)
        {

            UpdateEmployeesActivenesAction action = new UpdateEmployeesActivenesAction(trainerId, isActive, rolId);
            string result = action.Execute(EnumDatabase.Exceline, gymCode);
            return result;
        }

        public List<ExcelineTaskDC> GetEntityTasks(int entityId, int branchId, string roleType, string gymCode)
        {
            GetEntityTasksAction action = new GetEntityTasksAction(entityId, branchId, roleType);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool SaveEntityTimeEntry(TimeEntryDC timeEntry, string gymCode)
        {
            SaveEntityTimeEntry action = new SaveEntityTimeEntry(timeEntry);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<TimeEntryDC> GetEntityTimeEntries(DateTime startTime, DateTime endTime, int entityId, string entityRoleType, string gymCode)
        {
            GetEntityTimeEntriesAction action = new GetEntityTimeEntriesAction(startTime, endTime, entityId, entityRoleType);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool DeleteEntityTimeEntry(int entityTimeEntryId, int taskId, decimal timeSpent, string gymCode)
        {
            DeleteEntityTimeEntryAction action = new DeleteEntityTimeEntryAction(entityTimeEntryId, taskId, timeSpent);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<CategoryDC> GetTaskStatusCategories(int branchId, string gymCode)
        {
            GetTasksStatusCategoriesAction action = new GetTasksStatusCategoriesAction(branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }


        public bool SaveEmployeesRoles(EmployeeDC employeeDc, string gymCode)
        {
            SaveRolesAction action = new SaveRolesAction(employeeDc);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<RoleActivityDC> GetActivitiesForRoles(int memberId, string gymCode)
        {
            GetActivitiesForRoleAction action = new GetActivitiesForRoleAction(memberId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool DeleteGymEmployee(int memberId,int assignEmpId, string user, string gymCode)
        {
            DeleteGymEmployeeAction action = new DeleteGymEmployeeAction(memberId,assignEmpId, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<GymEmployeeRoleDurationDC> GetGymEmployeeRoleDuration(int employeeId, string gymCode)
        {
            GetGymEmployeeRoleDurationAction action = new GetGymEmployeeRoleDurationAction(employeeId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
        public List<GymEmployeeWorkPlanDC> GetGymEmployeeWorkPlan(int employeeId, string gymCode)
        {
            GetGymEmployeeWorkPlanAction action = new GetGymEmployeeWorkPlanAction(employeeId);
            List<GymEmployeeWorkPlanDC> workPlanList = action.Execute(EnumDatabase.Exceline, gymCode);

            foreach (GymEmployeeWorkPlanDC plan in workPlanList)
            {
                GetEmployeeBookingsByScheduleAction saction = new GetEmployeeBookingsByScheduleAction(employeeId, plan.Id);
                plan.BookingList = new List<EmployeeBookingDC>(saction.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode));
            }
            return workPlanList;
        }

        public List<GymEmployeeApprovalDC> GetGymEmployeeApprovals(int employeeId, string approvalType, string gymCode)
        {
            GetGymEmployeeApprovalsAction action = new GetGymEmployeeApprovalsAction(employeeId, approvalType);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        
        public bool AddEmployeeTimeEntry(EmployeeTimeEntryDC timeEntry, string gymCode)
        {
            AddEmployeeTimeEntryAction action = new AddEmployeeTimeEntryAction(timeEntry);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<EmployeeTimeEntryDC> GetEmployeeTimeEntries(int employeeId, string gymCode)
        {
            GetEmployeeTimeEntriesAction action = new GetEmployeeTimeEntriesAction(employeeId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool ApproveEmployeeTimes(int employeeId, bool isAllApproved, string empIdList, string gymCode)
        {
            ApproveEmployeeTimesAction action = new ApproveEmployeeTimesAction(employeeId, isAllApproved, empIdList);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<EmployeeClass> GetEmployeeClasses(int empId, string gymCode)
        {
            GetEmployeeClassesAction action = new GetEmployeeClassesAction(empId, gymCode);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<DateTime> GetClassDateByEmployee(int scheduleItemId, string gymCode)
        {
            GetClassDateByEmployeeAction action = new GetClassDateByEmployeeAction(scheduleItemId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
        public int UpdateApproveStatus(bool IsApproved, int EntityActiveTimeID, string user)
        {
            UpdateApproveStatusAction action = new UpdateApproveStatusAction(IsApproved, EntityActiveTimeID, user);
            return action.Execute(EnumDatabase.Exceline, user.Split('/')[0]);
        }

        public List<EmployeeBookingDC> GetEmployeeBookings(int empId, string gymCode)
        {
            GetEmployeeBookingAction action = new GetEmployeeBookingAction(empId);
            return action.Execute(EnumDatabase.Exceline, gymCode);        
        }         

        #endregion

        #region Branch

        public int SaveBranchDetails(ExcelineBranchDC branch, string gymCode)
        {
            SaveBranchAction action = new SaveBranchAction(branch);
            //return action.Execute(EnumDatabase.Exceline, gymCode);
            int branchId = action.Execute(EnumDatabase.Exceline, gymCode);

            if (branchId > 0)
            {
                UpdateGymCreditorNoAction upateAction = new UpdateGymCreditorNoAction(branchId, gymCode, branch.CreditorCollectionId, branch.BranchName);
                bool result = upateAction.Execute(EnumDatabase.WorkStation);

                if (result)
                {
                    
                }
            }
            return branchId;

        }

        public bool ValidateCreditorNo(int branchId, string gymCode, string creditorNo)
        {
            var action = new ValidateCreditorNoAction(branchId,gymCode,creditorNo);
            return action.Execute(EnumDatabase.WorkStation);
        }

        public List<ExcelineCreditorGroupDC> GetBranchGroups(string gymCode)
        {
            GetBranchGroupsAction action = new GetBranchGroupsAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ExcelineBranchDC> GetBranches(string user, string gymCode, int branchId)
        {
            GetBranchesAction action = new GetBranchesAction(branchId, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int UpdateBranchDetails(ExcelineBranchDC branch, string gymCode)
        {
            //Update the common database
            UpdateGymCreditorNoAction upateAction = new UpdateGymCreditorNoAction(branch.Id, gymCode, branch.CreditorCollectionId, branch.BranchName);
            upateAction.Execute(EnumDatabase.WorkStation);

            UpdateBranchAction action = new UpdateBranchAction(branch);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
        public int SaveGymEmployeeWork(ScheduleItemDC scheduleItem, int branchId, int resourceId, string gymCode)
        {
            SaveGymEmployeeWorkAction action = new SaveGymEmployeeWorkAction(scheduleItem, resourceId, branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int SaveWorkActiveTime(GymEmployeeWorkPlanDC work, int branchId, string gymCode)
        {
            SaveWorkActiveTimeAction action = new SaveWorkActiveTimeAction(work, branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int SaveWorkItem(GymEmployeeWorkDC work, int branchId, string gymCode)
        {
            SaveWorkItemAction action = new SaveWorkItemAction(work, branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int AdminApproveEmployeeWork(GymEmployeeWorkDC work, bool isApproved, string user, string gymCode)
        {
            AdminApproveEmployeeWorkAction action = new AdminApproveEmployeeWorkAction(work, isApproved, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
        public bool DeleteGymEmployeeActiveTime(int activeTimeId, string gymCode)
        {
            DeleteGymEmployeeActiveTimeAction action = new DeleteGymEmployeeActiveTimeAction(activeTimeId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }



        #endregion

        #region manage time entries

        public List<TimeEntryDC> GetTimeEntryForAdminView(DateTime startDate, DateTime endDate, string gymCode)
        {
            GetTimeEntriesForAdminViewAction action = new GetTimeEntriesForAdminViewAction(startDate, endDate);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        #endregion

        #region Member

        public List<EntityVisitDC> GetMemberVisits(DateTime startDate, DateTime endDate, int branchId, string gymCode)
        {
            GetMemberVisitsAction action = new GetMemberVisitsAction(startDate, endDate, branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<string> GetAccessPoints(string gymCode)
        {
            GetAccessPointsAction action = new GetAccessPointsAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
        #endregion

        #region Holidays
        public List<HolidayScheduleDC> GetHolidays(int branchId, string holidayType, DateTime startDate, DateTime endDate, string gymCode)
        {
            GetHolidayScheduleAction holidaySchedule = new GetHolidayScheduleAction(branchId, holidayType, startDate, endDate);
            return holidaySchedule.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool AddNewHoliday(HolidayScheduleDC newHoliday, string gymCode)
        {
            AddNewHolidayAction addHoliday = new AddNewHolidayAction(newHoliday);
            return addHoliday.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int GetHolidayCategoryId(string categoryName, string gymCode)
        {
            GetHolidayCategoryIdAction GetCategoryId = new GetHolidayCategoryIdAction(categoryName);
            return GetCategoryId.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool UpdateHolidayStatus(HolidayScheduleDC updateHoliday, string gymCode)
        {
            UpdateHolidayStatusAction update = new UpdateHolidayStatusAction(updateHoliday);
            return update.Execute(EnumDatabase.Exceline, gymCode);
        }
        #endregion

        #region Category
        public int AddActivitySettings(ActivitySettingDC activitySetting, string gymCode, string user)
        {
            AddActivitySettingsAction action = new AddActivitySettingsAction(activitySetting,user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        #endregion

        #region Manage Discount
        public bool AddDiscount(ShopDiscountDC newDiscount, string gymCode)
        {
            AddDiscountAction action = new AddDiscountAction(newDiscount);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ShopDiscountDC> GetDiscountsList(int branchId, bool isActive, string gymCode)
        {
            GetDiscountsListAction action = new GetDiscountsListAction(branchId, isActive);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool UpdateDiscount(ShopDiscountDC updateDiscount, string gymCode)
        {
            UpdateDiscountAction action = new UpdateDiscountAction(updateDiscount);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ArticleDC> GetInventoryList(int branchId, string gymCode)
        {
            GetInventoryItemsAction action = new GetInventoryItemsAction(branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool DeleteDiscount(int discountId, string type, string gymCode)
        {
            DeleteDiscountAction action = new DeleteDiscountAction(discountId, type);
            return action.Execute(EnumDatabase.Exceline, gymCode);        
        }

        #endregion

        #region System Settings

        public List<VatCodeDC> GetVatCode(string gymCode)
        {
            GetVatCodeAction action = new GetVatCodeAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int AddUpdateVatCodes(VatCodeDC vatCodeDetail, string user, string gymCode)
        {
            AddUpdateVatCodesAction action = new AddUpdateVatCodesAction(vatCodeDetail, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public SystemSettingOtherSettingDC GetOtherSettings(string gymCode)
        {
            GetGymCompanyOtherSettingsAction action = new GetGymCompanyOtherSettingsAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int AddUpdateOtherSettings(SystemSettingOtherSettingDC otherSettingsDetail, string user, string gymCode)
        {
            AddUpdateGymCompanyOtherSettingsAction action = new AddUpdateGymCompanyOtherSettingsAction(otherSettingsDetail, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<string> GetClassKeyword(string gymCode)
        {
            GetGymCompanyClassKeywordAction action = new GetGymCompanyClassKeywordAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
        
        public SystemSettingBasicInfoDC GetSystemSettingBasicInfo(string gymCode)
        {
            GetSystemSettingBasicInfoAction action = new GetSystemSettingBasicInfoAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
        
        public SystemSettingEconomySettingDC GetSystemSettingEconomySetting(string gymCode)
        {
            GetSystemSettingEconomySettingAction action = new GetSystemSettingEconomySettingAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
        
        public List<ContractCondition> GetSystemSettingContractConditions(string gymCode)
        {
            GetSystemSettingContractConditionsAction action = new GetSystemSettingContractConditionsAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int AddUpdateSystemSettingBasicInfo(SystemSettingBasicInfoDC SystemSettingBasicInfo, string user, string gymCode)
        {
            int result = -1;

            AddUpdateSystemSettingBasicInfoWorkStationAction actionWorkStation = new AddUpdateSystemSettingBasicInfoWorkStationAction(SystemSettingBasicInfo, user, gymCode);
            result = actionWorkStation.Execute(EnumDatabase.WorkStation);

            AddUpdateSystemSettingBasicInfoAction action = new AddUpdateSystemSettingBasicInfoAction(SystemSettingBasicInfo, user);
            result = action.Execute(EnumDatabase.Exceline, gymCode);

            return result;
        }

        public int AddUpdateSystemSettingEconomySetting(SystemSettingEconomySettingDC SystemSettingEconomySetting, string user, string gymCode)
        {
            AddUpdateSystemSettingEconomySettingAction action = new AddUpdateSystemSettingEconomySettingAction(SystemSettingEconomySetting, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int AddUpdateSystemSettingContractCondition(ContractCondition SystemSettingContractCondition, string user, string gymCode)
        {
            AddUpdateSystemSettingContractConditionAction action = new AddUpdateSystemSettingContractConditionAction(SystemSettingContractCondition);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
        #endregion

        #region Revenue Account
        public List<AccountDC> GetRevenueAccounts(string gymCode)
        {
            GetRevenueAccountsAction action = new GetRevenueAccountsAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int AddUpdateRevenueAccounts(AccountDC revenueAccountDetail, string user, string gymCode)
        {
            AddUpdateRevenueAccountsAction action = new AddUpdateRevenueAccountsAction(revenueAccountDetail, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int DeleteRevenueAccounts(AccountDC revenueAccountDetail, string user, string gymCode)
        {
            var action = new DeleteRevenueAccountsAction(revenueAccountDetail, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        } 
        #endregion

        public decimal SaveStock(ArticleDC article,int branchId, string gymCode, string user)
        {
            SaveStockAction action = new SaveStockAction(article,branchId, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int DeleteArticle(int articleId, int branchId, string gymCode, string user, bool isAdminUser)
        {
            DeleteArticleAction action = new DeleteArticleAction(articleId, branchId, user, isAdminUser);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ActivityTimeDC> GetActivityUnavailableTimes(int branchId, string gymCode)
        {
            GetActivityUnavailableTimeAction action = new GetActivityUnavailableTimeAction(branchId,-1);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool AddActivityUnavailableTimes(List<ActivityTimeDC> activityTimes, string gymCode)
        {
            AddActivityUnavailableTimesAction action = new AddActivityUnavailableTimesAction(activityTimes);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int SaveInventory(InventoryItemDC inventoryItem, string gymCode, string user)
        {
            SaveInventoryAction action = new SaveInventoryAction(inventoryItem, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<InventoryItemDC> GetInventory(int branchId, string gymCode)
        {
            GetInventoryAction action = new GetInventoryAction(branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int DeleteInventory(int inventoryId, string gymCode)
        {
            DeleteInventoryAction action = new DeleteInventoryAction(inventoryId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ArticleDC> GetInventoryDetail(int branchId, int inventoryId, string gymCode)
        {
            GetInventoryDetailAction action = new GetInventoryDetailAction(branchId, inventoryId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public decimal SaveInventoryDetail(List<ArticleDC> articleList, int inventoryId, string gymCode)
        {
            SaveInventoryDetailAction action = new SaveInventoryDetailAction(articleList, inventoryId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public Dictionary<decimal, decimal> GetNetValueForArticle(int inventoryId, int articleId, int counted, int branchId, string gymCode)
        {
            GetNetValueForArticleAction action = new GetNetValueForArticleAction(inventoryId, articleId, counted, branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int SaveScheduleItem(ScheduleItemDC scheduleItem, int resId, int branchId, string gymCode)
        {
            SaveScheduleItemAction action = new SaveScheduleItemAction(scheduleItem, resId, branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public ScheduleDC GetResourceScheduleItems(int resId, string roleTpye, string gymCode)
        {
            GetResourceScheduleItemsAction action = new GetResourceScheduleItemsAction(resId, roleTpye, gymCode);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }


        public List<ScheduleDC> GetResourceCalenderScheduleItems(List<int> resIdList, string roleTpye, string gymCode)
        {
            return resIdList.Select(resId => new GetResourceScheduleItemsAction(resId, roleTpye, gymCode)).Select(action => action.Execute(EnumDatabase.Exceline, gymCode)).ToList();
        }

        public List<EntityActiveTimeDC> GetMemberBookingDetails(int branchId, int scheduleItemId, string gymCode)
        {
            GetMemberBookingDetailsForResourceAction action = new GetMemberBookingDetailsForResourceAction(branchId, scheduleItemId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool ValidateScheduleWithPaidBooking(int scheduleItemId,int resourceId, string gymCode)
        {
            var action = new ValidateScheduleWithPaidBookingAction(scheduleItemId, resourceId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int DeleteResourcesScheduleItem(int scheduleItemId, string gymCode)
        {
            DeleteResourcesScheduleItemAction action = new DeleteResourcesScheduleItemAction(scheduleItemId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int CheckDuplicateResource(int sourceId, int destinationId, DateTime startDate, DateTime endDate, string gymCode)
        {
            CheckDuplicateResourceAction action = new CheckDuplicateResourceAction(sourceId, destinationId, startDate, endDate);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        #region followUp

        public bool AddFollowUpTemplate(FollowUpTemplateDC followUpTemplate, string gymCode, int branchId)
        {
            AddFollowUpTemplateAction action = new AddFollowUpTemplateAction(followUpTemplate, branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<FollowUpTemplateDC> GetFollowUpTemplates(string gymCode, int branchId)
        {
            GetFollowUpTemplatesAction action = new GetFollowUpTemplatesAction(branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool DeleteFollowUpTemplate(int followUpTemplate, string gymCode)
        {
            DeleteFollowUpTemplateAction action = new DeleteFollowUpTemplateAction(followUpTemplate);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<FollowUpTemplateTaskDC> GetTaskCategoryByFollowUpTemplateId(int templateId, string gymCode)
        { 
             GetFollowUpCategoryTasksByTemplateIdAction action = new GetFollowUpCategoryTasksByTemplateIdAction(templateId);
             return action.Execute(EnumDatabase.Exceline, gymCode);
        }



        #endregion

        #region Manage Jobs

        public int SaveExcelineJob(ScheduleItemDC scheduleItem, int branchId, string gymCode)
        {
            SaveExcelineJobAction action = new SaveExcelineJobAction(scheduleItem, branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ScheduleItemDC> GetJobScheduleItems(int branchId, string gymCode)
        {
            GetJobScheduleItemsAction action = new GetJobScheduleItemsAction(branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int DeleteScheduleItem(List<int> scheduleItemIdList, string gymCode)
        {
            int isDeleted = 0;
            foreach (int id in scheduleItemIdList)
            {
                DeleteScheduleItemAction action = new DeleteScheduleItemAction(id);
                isDeleted = action.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
            }
            return isDeleted;
        }

        public int AssignTaskToEmployee(ExcelineCommonTaskDC commonTask, int employeeId, string gymCode, string user)
        {
            AssignTaskToEmployeeAction action = new AssignTaskToEmployeeAction(commonTask, employeeId, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        #endregion

        public string CheckActiveTimeOverlap(EntityActiveTimeDC activeTime, string gymCode)
        {
            CheckActiveTimeOverlapWithBookingAction checkOverlap = new CheckActiveTimeOverlapWithBookingAction(activeTime);
            return checkOverlap.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
        }

        public int SaveResourceActiveTimePunchard(EntityActiveTimeDC activeTimeItem, int contractId, string gymCode, int branchId, string user)
        {
            SaveResourceActiveTimePunchardAction action = new SaveResourceActiveTimePunchardAction(activeTimeItem, activeTimeItem.IsDeleted, branchId, user, contractId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int SaveResourceActiveTimeRepeatablePunchcard(List<EntityActiveTimeDC> activeTimeItemList, int contractId, string gymCode, string user)
        {
            SaveResourceActiveTimeRepeatablePunchcardAction action = new SaveResourceActiveTimeRepeatablePunchcardAction(activeTimeItemList, contractId, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int SaveResourceActiveTimeRepeatable(List<EntityActiveTimeDC> activeTimeItemList, string gymCode, string user)
        {
            SaveResourceActiveTimeRepeatableAction action = new SaveResourceActiveTimeRepeatableAction(activeTimeItemList, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public int SaveResourceActiveTime(EntityActiveTimeDC activeTimeItem, string gymCode, int branchId, string user)
        {

            SaveResourceActiveTimeAction action = new SaveResourceActiveTimeAction(activeTimeItem, activeTimeItem.IsDeleted, branchId, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);

             //string result = string.Empty;
             //if (activeTimeItem.IsDeleted) // don't need to check overlap when delete
             //{
             //    SaveResourceActiveTimeAction action = new SaveResourceActiveTimeAction(activeTimeItem, activeTimeItem.IsDeleted, branchId, user);
             //    return action.Execute(EnumDatabase.Exceline, gymCode);
             //}
             //else
             //{
             //    //CheckActiveTimeOverlapWithBookingAction checkOverlap = new CheckActiveTimeOverlapWithBookingAction(activeTimeItem);
             //    //result = checkOverlap.Execute(US_DataAccess.EnumDatabase.Exceline, gymCode);
             //    //string[] sTextArray = result.Split(new char[] { ':' });
             //    //if (sTextArray.Length > 0)
             //    //{
             //    //    if (Convert.ToInt32(sTextArray[0]) > 0)
             //    //    {
             //    //        SaveResourceActiveTimeAction action = new SaveResourceActiveTimeAction(activeTimeItem, activeTimeItem.IsDeleted, branchId, user);
             //    //        return action.Execute(EnumDatabase.Exceline, gymCode);
             //    //    }
             //    //}
             //    //return result;
             //}
             //return string.Empty;
            
        }

        public int DeleteResourceActiveTime(int activeTimeId,string articleName,DateTime visitDateTime, string roleType, bool isArrived, bool isPaid, List<int> memberlist ,string gymCode)
        {
            DeleteResourceActiveTimeAction action = new DeleteResourceActiveTimeAction(activeTimeId, articleName, visitDateTime, roleType, isArrived, isPaid, memberlist);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ArticleDC> GetArticleForResouceBooking(int activityId,int branchId, string gymCode)
        {
            var action = new GetArticleForResouceBookingAction(activityId,branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ArticleDC> GetArticleForResouce(int activityId, int branchId, string gymCode)
        {
            var action = new GetArticleForResouceAction(activityId, branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }


        #region Manage Employee Calendar

        public List<ExcelineCommonTaskDC> GetAllTasksByEmployeeId(int branchId, int employeeId, string gymCode, string user)
        {
            var action = new GetAllTasksByEmployeeIdAction(branchId, employeeId, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ExcelineCommonTaskDC> GetFollowUpByEmployeeId(int branchId, int employeeId, string gymCode, string user, int hit)
        {
            var action = new GetFollowUpByEmployeeAction(branchId, employeeId, user, hit);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ExcelineCommonTaskDC> GetJobsByEmployeeId(int branchId, int employeeId, string gymCode, string user,int hit)
        {
            var action = new GetJobsByEmployeeAction(branchId, employeeId, user, hit);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
        public Dictionary<string, List<int>> CheckUnusedCancelBooking(int activityId,int branchId, string accountNo, List<int> memberIdList, DateTime bookingDate, string gymCode)
        {
            var result = new Dictionary<string, List<int>>();
            var action = new CheckUnusedCancelBookingAction(activityId, accountNo, memberIdList, bookingDate);
            List<int> memList = action.Execute(EnumDatabase.Exceline, gymCode);

            var actionWithAccount = new CheckUnusedCancelBookingWithAccountAction(activityId,branchId, accountNo, memberIdList, bookingDate);
            List<int> memListWithAccount = actionWithAccount.Execute(EnumDatabase.Exceline, gymCode);

            result.Add("validMem", memList);
            result.Add("validMemAcc", memListWithAccount);

            return result;
        }

        public int UnavailableResourceBooking(int scheduleItemId, int activeTimeId, DateTime startDate, DateTime endDate, string gymCode, string user)
        {
            var action = new UnavailableResourceBookingAction(scheduleItemId, activeTimeId, startDate, endDate, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        #endregion
        public List<EmployeeEventDC> GetEmployeeEvents(int employeeNumber, string gymCode)
        {
            GetEmployeeEventsAction action = new GetEmployeeEventsAction(employeeNumber);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<EmployeeJobDC> GetEmployeeJobs(int employeeNumber, string gymCode)
        {
            GetEmployeeJobsAction action = new GetEmployeeJobsAction(employeeNumber);
            return action.Execute(EnumDatabase.Exceline, gymCode);

        }

        public List<ExcelineMemberDC> GetSponsorsforSponsoring(int branchId, DateTime startDate, DateTime endDate, string gymCode)
        {
            GetSponsorsforSponsoringAction action = new GetSponsorsforSponsoringAction(branchId, startDate, endDate);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool GenerateSponsorOrders(SponsorShipGenerationDetailsDC sponsorDetails, string gymCode, string user)
        {
            GenerateSponsorOrdersAction action = new GenerateSponsorOrdersAction(sponsorDetails,  user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool GetSponsoringProcessStatus(string guiID, string gymCode, string user)
        {
            GetSponsoringProcessStatusAction action = new GetSponsoringProcessStatusAction( guiID, user);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public Dictionary<string, List<int>> ValidatePunchcardContractMember(int activityId,int branchId, string accountNo, List<int> memberIdList, DateTime startDate, string gymCode)
        {
            var result = new Dictionary<string, List<int>>();

            var action = new ValidatePunchcardContractMemberAction(activityId,memberIdList, startDate);
            var validPunchMemIdList = action.Execute(EnumDatabase.Exceline, gymCode);

            var actionWithAccount = new ValidatePunchcardContractMemberWithAccountAction(activityId, branchId,accountNo, memberIdList, startDate);
            var validPunchWithaccountMemIdList = actionWithAccount.Execute(EnumDatabase.Exceline, gymCode);

            result.Add("validMem", validPunchMemIdList);
            result.Add("validMemAcc", validPunchWithaccountMemIdList);

            return result;
        }

        public int SavePaymentBookingMember(int activetimeId, int articleId, int memberId, bool paid, int aritemNo, decimal amount, string paymentType, string gymCode)
        {
            var action = new SavePaymentBookingMemberAction(activetimeId, articleId, memberId, paid, aritemNo, amount, paymentType);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        } 


        public List<ExcelineRoleDc> GetGymEmployeeRolesById(int employeeId, int branchId, string gymCode)
        {
            var action = new GetGymEmployeeRolesAction(employeeId, branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        #region Manage Anonymizing

        public int DeleteAnonymizingData(ExceAnonymizingDC anonymizing,string user, int branchId, string gymCode)
        {
            DeleteAnonymizingDataAction action = new DeleteAnonymizingDataAction(anonymizing,user, branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ExceAnonymizingDC> GetAnonymizingData(string gymCode)
        {
            GetAnonymizingDataAction action = new GetAnonymizingDataAction();
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        #endregion


        public CategoryDC GetCategoryByCode(string categoryCode, string gymCode)
        {
            var action = new GetCategoryByCodeAction(categoryCode);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ExcACCAccessControl> GetExcAccessControlList(int branchId, string gymCode)
        {
            try
            {
                var action = new GetExcAccessControlListAction(branchId);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ExceACCMember GetLastAccMember(string gymCode, int branchId, int accTerminalId)
        {
            try
            {
                var action = new GetLastAccMemberAction(branchId, accTerminalId);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetResourceBookingViewMode(string user, string gymCode)
        {
            try
            {
                var action = new GetResourceBookingViewModeAction(user);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ExceArxFormatDetail> GetArxSettingDetail(int branchId, string gymCode)
        {
            try
            {
                var action = new GetArxSettingDetailAction(branchId);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<ExceArxFormatType> GetArxFormatType(string gymCode)
        {
            try
            {
                var action = new GetArxFormatTypeAction();
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int SaveArxSettingDetail(ExceArxFormatDetail arxFormatDetail, int tempCodeDeleteDate, int branchId, string user, string gymCode, string accessContolTypes)
        {
            try
            {
                var action = new SaveArxSettingDetailAction(arxFormatDetail, tempCodeDeleteDate, branchId, user, accessContolTypes);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public List<ContractTemplateDC> ValidateContractConditionWithTemplate(int contractConditionId, string gymCode)
        {
            try
            {
                var action = new ValidateContractConditionWithTemplateAction(contractConditionId);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ValidateContractConditionWithContracts(int contractConditionId, string gymCode)
        {
            try
            {
                var action = new ValidateContractConditionWithContractsAction(contractConditionId);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ValidateArticleByActivityId(int activityId, int articleId,string gymCode)
        {
            try
            {
                var action = new ValidateArticleByActivityIdAction(activityId, articleId);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

       
    }
}
