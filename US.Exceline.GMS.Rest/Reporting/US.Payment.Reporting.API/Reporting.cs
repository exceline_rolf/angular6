﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Payment.Core.ResultNotifications;
using US.Payment.Core.Enums;
using US.Payment.Reporting.Core.DomainObjects;
using US.Payment.Reporting.BL;
using System.Data;
using US.Payment.Reporting.Data;
using US.Payment.Core.Utils;
using US.Common.Logging.API;
using System.Configuration;
using System.IO;

using System.Net;
using System.Web.Services.Protocols;
using US.Payment.Core;
using US_DataAccess;
using US.Payment.Core.BusinessDomainObjects.Reporting;
using System.Threading;
using System.Xml.Serialization;
using System.Xml;
using System.Runtime.Serialization;
using US_DataAccess.Utill;
using US.Payment.Reporting.ReportingServices2005;

namespace US.Payment.Reporting.API
{
    public class Reporting
    {
        private static List<ReportSetting> _settingList = null;

        static string _userName = string.Empty;
        static string _password = string.Empty;
        static string _connectionStringForDataSourse = string.Empty;
        static string _userNameForDataSourse = string.Empty;
        static string _passwordForDataSourse = string.Empty;
        static string _urlForReportServer = string.Empty;
        static string _fileUploadHandlerPath = string.Empty;
        static string _recordCount = string.Empty;

        private static void GetReportSettings(string CoreSessionUserName)
        {
            //if (_settingList == null)
            //{
            try
            {
                _settingList = ReportingFacade.GetReportSettings(CoreSessionUserName);
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile("Report : Report Settings reading Failed : " + ex.Message, ex, _userName);
            }
            if (_settingList != null)
            {
                foreach (ReportSetting item in _settingList)
                {
                    switch (item.Key)
                    {
                        case "UserName":
                            {
                                _userName = item.KeyValue;
                                break;
                            }
                        case "Password":
                            {
                                _password = item.KeyValue;
                                break;
                            }
                        case "ReportServiceURL":
                            {
                                _urlForReportServer = item.KeyValue;
                                break;
                            }
                        case "RecordCount":
                            {
                                _recordCount = item.KeyValue;
                                break;
                            }
                        case "FileUploadHandlerPath":
                            {
                                _fileUploadHandlerPath = item.KeyValue;
                                break;
                            }
                    }
                }
            }
            _connectionStringForDataSourse = GenericDBFactory.GetConnectionString(EnumDatabase.Exceline, GetGymCode(CoreSessionUserName));

            foreach (string item in _connectionStringForDataSourse.Split(';').ToList())
            {
                if (item.ToUpper().Contains("USER ID"))
                {
                    _userNameForDataSourse = item.Split('=')[1];
                }
                else if (item.ToUpper().Contains("PASSWORD"))
                {
                    _passwordForDataSourse = item.Split('=')[1];
                }

            }
        }

        /// <summary>
        /// Get all report information
        /// </summary>
        /// <returns>USImportResult with List of ReportInfo </returns>
        public static USImportResult<List<ReportInfo>> GetReportInfoItemList(string CoreSessionUserName)
        {
            GetReportSettings(CoreSessionUserName);
            USImportResult<List<ReportInfo>> result = new USImportResult<List<ReportInfo>>();
            try
            {
                result.MethodReturnValue = US.Payment.Reporting.Data.ReportingFacade.GetReportInfoList();
                result.ErrorOccured = false;
            }
            catch (Exception ex)
            {
                USNotificationMessage message = new USNotificationMessage(ErrorLevels.Error, "Error in retrieving report items infomation." + ex.Message);
                result.ErrorOccured = true;
                result.AddUSNotificationMessage(message);
            }
            return result;
        }

        /// <summary>
        /// Get all Report Entities
        /// </summary>
        /// <returns></returns>
        public static List<ReportEntity> GetFilteredReportEntities(List<string> reportEntityList, string CoreSessionUserName)
        {
            GetReportSettings(CoreSessionUserName);
            return ReportingFacade.GetFilteredReportEntities(reportEntityList, CoreSessionUserName);
        }
        /// <summary>
        /// View all reports
        /// </summary>
        /// <returns></returns>
        public static List<USReport> ViewAllReports(string CoreSessionUserName)
        {
            GetReportSettings(CoreSessionUserName);
            return ReportingFacade.ViewAllReports(CoreSessionUserName);
        }

        public static List<ReportSchedule> GetScheduleListByReportId(int reportId, string CoreSessionUserName)
        {

            GetReportSettings(CoreSessionUserName);
            return ReportingFacade.GetScheduleListByReportId(reportId, CoreSessionUserName);
        }
        public static USReport GetReportById(int reportId, string CoreSessionUserName)
        {
            GetReportSettings(CoreSessionUserName);
            return ReportingFacade.GetReportById(reportId, CoreSessionUserName);
        }

        public static List<ReportEntity> GetAllEntityList(string CoreSessionUserName)
        {
            GetReportSettings(CoreSessionUserName);
            return ReportingFacade.GetAllEntityList(CoreSessionUserName);
        }


        public static List<string> GetAllReportCategories(string CoreSessionUserName)
        {
            GetReportSettings(CoreSessionUserName);
            return ReportingFacade.GetAllReportCategories(CoreSessionUserName);
        }
        public static List<UserRole> GetAllRoles(string CoreSessionUserName)
        {
            GetReportSettings(CoreSessionUserName);
            return ReportingFacade.GetAllRoles(CoreSessionUserName);
        }
        public static bool SaveCategory(string name, string CoreSessionUserName)
        {
            try
            {
                GetReportSettings(CoreSessionUserName);
                return ReportingFacade.SaveCategory(name, CoreSessionUserName);
            }
            catch
            {
                throw;
            }
        }

        public static bool UpdateCategory(string oldName, string newName, string CoreSessionUserName)
        {
            GetReportSettings(CoreSessionUserName);
            return ReportingFacade.UpdateCategory(oldName, newName, CoreSessionUserName);
        }
        public static string SaveReport(USReport reportToBeSaved, string CoreSessionUserName)
        {
            GetReportSettings(CoreSessionUserName);
            return ReportingFacade.SaveReport(reportToBeSaved, CoreSessionUserName);
        }

        public static bool SaveEditedReport(int ID, USReport reportToBeSaved, string CoreSessionUserName)
        {
            GetReportSettings(CoreSessionUserName);
            return ReportingFacade.SaveEditedReport(ID, reportToBeSaved, CoreSessionUserName);
        }


        public static List<JoinQuery> GetJoinQueryList(string CoreSessionUserName)
        {
            GetReportSettings(CoreSessionUserName);
            return ReportingFacade.GetJoinQueryList(CoreSessionUserName);
        }

        public static bool DeleteReport(int reportIdToBeDeleted, string CoreSessionUserName)
        {
            GetReportSettings(CoreSessionUserName);
            return ReportingFacade.DeleteReport(reportIdToBeDeleted, CoreSessionUserName);
        }

        public static string GetOldReportItem(int oldId, string CoreSessionUserName)
        {
            GetReportSettings(CoreSessionUserName);
            USReport oldReport = ReportingFacade.GetOldReportItem(oldId, CoreSessionUserName);
            return "/" + oldReport.Category + "/" + oldReport.Name;
        }

        public static List<ReportEntity> GetAllEntitiesWithProperties(string CoreSessionUserName)
        {
            GetReportSettings(CoreSessionUserName);
            return ReportingFacade.GetAllEntitiesWithProperties(CoreSessionUserName);
        }

        public static List<string> TestQuery(string query, string CoreSessionUserName)
        {
            GetReportSettings(CoreSessionUserName);
            return ReportingFacade.TestQuery(query, CoreSessionUserName);
        }
        public static List<string> FollowUpQuery(string query, string CoreSessionUserName)
        {
            GetReportSettings(CoreSessionUserName);
            return ReportingFacade.FollowUpQuery(query, CoreSessionUserName);
        }

        public static string SaveReportConfiguration(USReport report, string fileName, string CoreSessionUserName)
        {
            GetReportSettings(CoreSessionUserName);
            USReport reportToBeSaved = new USReport();
            reportToBeSaved = report;
            //reportToBeSaved = (USReport)DesrializeXMLToObject(report, new USReport().GetType());
            string rdlString = string.Empty;
            if (reportToBeSaved.FixReport)
            {
                try
                {
                    string reportFilePath = _fileUploadHandlerPath + @"\Reports\" + reportToBeSaved.Category + @"\" + fileName;
                    int count = 0;
                    while (true)
                    {
                        try
                        {
                            if (count < 30)
                            {
                                using (Stream stream = new FileStream(reportFilePath, FileMode.Open))
                                {
                                    USLogEvent.WriteToFile("Report : RDL file reading finished.", _userName);
                                    break;
                                }
                            }
                            else
                                break;
                        }
                        catch (Exception ee)
                        {
                            //check here why it failed and retry if the file is in use.
                            USLogError.WriteToFile("Report : Uploaded RDl file open for reding Failed : " + ee.Message, ee, _userName);
                            Thread.Sleep(1000);
                            count++;
                            continue;
                        }
                    }
                    USLogEvent.WriteToFile("Report : Uploaded RDL file reading initiated.", _userName);
                    rdlString = File.ReadAllText(reportFilePath);
                    File.Delete(reportFilePath);
                }
                catch (Exception ex)
                {
                    USLogError.WriteToFile("Report : Uploaded RDl file reading failed Failed : " + ex.Message, ex, _userName);
                    return ex.Message;
                }
            }
            else
            {
                //Generate Query
                List<JoinQuery> joinQueryList = GetJoinQueryList(CoreSessionUserName);
                string query = BLManager.GeneateSQLQuery(joinQueryList, reportToBeSaved, _recordCount, CoreSessionUserName);
                reportToBeSaved.QueryForReport = query;
                //Generate RDLC
                rdlString = BLManager.GenerateRdl(reportToBeSaved, query, "");
            }
            //publish report..
            string DeployReportReturn1 = DeployReport(rdlString, reportToBeSaved.Name, _userName, _password, reportToBeSaved.Category, _connectionStringForDataSourse, _userNameForDataSourse, _passwordForDataSourse, CoreSessionUserName);
            if (DeployReportReturn1 == "true")
            {
                return SaveReport(reportToBeSaved, CoreSessionUserName);
            }
            else
            {
                return DeployReportReturn1;
            }
        }
        public static object DesrializeXMLToObject(string xml, Type type)
        {
            DataContractSerializer dcs = new DataContractSerializer(type);
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(xml));
            return dcs.ReadObject(ms);
        }
        public static string SaveEditedReportConfiguration(USReport report, string fileName, string CoreSessionUserName)
        {
            GetReportSettings(CoreSessionUserName);
            USReport reportToBeSaved = new USReport();

            reportToBeSaved = report;
                //(USReport)DesrializeXMLToObject(report, new USReport().GetType());
            string rdlString = string.Empty;
            USLogEvent.WriteToFile("Report : SaveEditedReportConfiguration initiated.", _userName);
            if (reportToBeSaved.FixReport)
            {
                try
                {
                    string reportFilePath = _fileUploadHandlerPath + @"\Reports\" + reportToBeSaved.Category + @"\" + fileName;
                    while (true)
                    {
                        try
                        {
                            using (Stream stream = new FileStream(reportFilePath, FileMode.Open))
                            {
                                USLogEvent.WriteToFile("Report : RDL file reading finished.", _userName);
                                break;
                            }
                        }
                        catch (Exception ee)
                        {
                            //check here why it failed and retry if the file is in use.
                            USLogError.WriteToFile("Report : Uploaded RDl file open for reding Failed : " + ee.Message, ee, _userName);
                            continue;
                        }
                    }
                    USLogEvent.WriteToFile("Report : Uploaded RDL file reading initiated.", _userName);
                    rdlString = File.ReadAllText(reportFilePath);
                    File.Delete(reportFilePath);
                }
                catch (Exception ex)
                {
                    USLogError.WriteToFile("Report : Uploaded RDl file reading failed : " + ex.Message, ex, _userName);
                    return ex.Message;
                }
            }
            else
            {
                //Generate Query
                List<JoinQuery> joinQueryList = GetJoinQueryList(CoreSessionUserName);
                string query = BLManager.GeneateSQLQuery(joinQueryList, reportToBeSaved, _recordCount, CoreSessionUserName);
                reportToBeSaved.QueryForReport = query;
                rdlString = BLManager.GenerateRdl(reportToBeSaved, query, "");
            }
            //remove currunt report and publish the new one..
            string itemForDeleteReport = GetOldReportItem(reportToBeSaved.Id, CoreSessionUserName);
            DeleteReportFromServer(_userName, _password, itemForDeleteReport, CoreSessionUserName);
            string DeployReportReturn2 = DeployReport(rdlString, reportToBeSaved.Name, _userName, _password, reportToBeSaved.Category, _connectionStringForDataSourse, _userNameForDataSourse, _passwordForDataSourse, CoreSessionUserName);
            if (DeployReportReturn2 == "true")
            {
                SaveEditedReport(reportToBeSaved.Id, reportToBeSaved, CoreSessionUserName);
            }
            else
            {
                return DeployReportReturn2;
            }
            return "true";
        }
        public static bool SaveEditedReportQuery(string report, string CoreSessionUserName)
        {
            GetReportSettings(CoreSessionUserName);
            USReport reportToBeSaved = new USReport();
            reportToBeSaved = (USReport)DesrializeXMLToObject(report, new USReport().GetType());
            string rdlString = string.Empty;
            USLogEvent.WriteToFile("Report : SaveEditedReportConfiguration initiated.", _userName);

            rdlString = BLManager.GenerateRdl(reportToBeSaved, reportToBeSaved.QueryForReport, "");
            //remove currunt report and publish the new one..
            string itemForDeleteReport = GetOldReportItem(reportToBeSaved.Id, CoreSessionUserName);
            DeleteReportFromServer(_userName, _password, itemForDeleteReport, CoreSessionUserName);
            string DeployReportReturn3 = DeployReport(rdlString, reportToBeSaved.Name, _userName, _password, reportToBeSaved.Category, _connectionStringForDataSourse, _userNameForDataSourse, _passwordForDataSourse, CoreSessionUserName);
            if (DeployReportReturn3 == "true")
            {
                SaveEditedReport(reportToBeSaved.Id, reportToBeSaved, CoreSessionUserName);
            }
            else
            {
                return false;
            }
            return true;
        }
        public static bool DeleteReport(int reportIdToBeDeleted, string item, string CoreSessionUserName)
        {
            GetReportSettings(CoreSessionUserName);
            USLogEvent.WriteToFile("Report : DeleteReport initiated.", _userName);
            if (DeleteReportFromServer(_userName, _password, item, CoreSessionUserName))
            {
                //if report server is not loalhost,then "item" should be modified....
                API.Reporting.DeleteReport(reportIdToBeDeleted, CoreSessionUserName);
            }
            else
            {
                return false;
            }
            return true;
        }
        public static string DeployReport(string content, string reportName, string userName, string password, string category, string connectionStringForDataSourse, string userNameForDataSourse, string passwordForDataSourse, string CoreSessionUserName)
        {
            try
            {
                GetReportSettings(CoreSessionUserName);
                USLogEvent.WriteToFile("Report : DeployReport initiated.", userName);
                ReportingService2005 rs = new ReportingService2005(_urlForReportServer);
                rs.Credentials = new NetworkCredential(userName, password, "");
                //string subscriptionMethod = reportName + "_SubsMethod";
                string CreateDataSourceReturn = CreateDataSource(rs, category, connectionStringForDataSourse, userNameForDataSourse, passwordForDataSourse, CoreSessionUserName);
                if (CreateDataSourceReturn == "true")
                {
                    string CreateReportReturn = CreateReport(content, rs, reportName, category, CoreSessionUserName);
                    if (CreateReportReturn != "true")
                    {
                        return CreateReport(content, rs, reportName, category, CoreSessionUserName);
                    }
                }
                else
                {
                    return CreateDataSourceReturn;
                }
            }
            catch (Exception eee)
            {
                return eee.Message;
            }
            return "true";
        }

        private static string CreateDataSource(ReportingService2005 reportingService, string category, string connectionStringForDataSourse, string userName, string password, string CoreSessionUserName)
        {
            try
            {
                if (string.IsNullOrEmpty(_connectionStringForDataSourse))
                {
                    USLogEvent.WriteToFile("Report : connectionStringForDataSourse = Empty or null", _userName);
                    return "connectionStringForDataSourse = Empty or null";
                }
                else
                {
                    USLogEvent.WriteToFile("Report : connectionStringForDataSourse = " + _connectionStringForDataSourse, _userName);
                    GetReportSettings(CoreSessionUserName);
                    USLogEvent.WriteToFile("Report : CreateDataSource initiated.", _userName);
                    Property newProp = new Property();
                    newProp.Name = "USReportingModule";
                    newProp.Value = "USReport";
                    Property[] props = new Property[1];
                    props[0] = newProp;
                    int length = -1;
                    int childLength = -1;

                    bool gymCodeFolderExist = false;
                    bool folderExist = false;

                    string name = "DataSource1";

                    // Define the data source definition.
                    DataSourceDefinition definition = new DataSourceDefinition();
                    definition.CredentialRetrieval = CredentialRetrievalEnum.Store;
                    definition.ConnectString = connectionStringForDataSourse;
                    definition.Enabled = true;
                    definition.EnabledSpecified = true;
                    definition.Extension = "SQL";
                    definition.ImpersonateUserSpecified = false;
                    definition.Prompt = null;
                    definition.WindowsCredentials = false;
                    definition.UserName = userName;
                    definition.Password = password;

                    if (USPRunTimeVariables.GetConnectionSource() != "Config")
                    {
                        CatalogItem[] items = reportingService.ListChildren("/", false);
                        if (items != null)
                        {
                            length = items.Length;
                        }

                        if (length != (-1))
                        {
                            for (int i = 0; i < length; i++)
                            {
                                if (items[i].Name.ToUpper() == GetGymCode(CoreSessionUserName))
                                {
                                    gymCodeFolderExist = true;
                                    break;
                                }
                            }
                        }

                        if (!gymCodeFolderExist)
                        {
                            try
                            {
                                USLogEvent.WriteToFile("Report : CreateFolder started..", _userName);
                                reportingService.CreateFolder(GetGymCode(CoreSessionUserName), "/", props);
                                gymCodeFolderExist = true;
                            }
                            catch (SoapException e)
                            {
                                USLogError.WriteToFile("Report : Exception Occour while creating the Gym-Code folder " + e.Message, e, _userName);
                                return e.Message;
                            }
                        }
                        if (gymCodeFolderExist)
                        {
                            CatalogItem[] childItems = reportingService.ListChildren("/" + GetGymCode(CoreSessionUserName), false);
                            if (childItems != null)
                            {
                                childLength = childItems.Length;
                            }
                            if (childLength != (-1))
                            {
                                for (int i = 0; i < childLength; i++)
                                {
                                    if (childItems[i].Name == category)
                                    {
                                        folderExist = true;
                                        break;
                                    }
                                }
                            }
                            if (!folderExist)
                            {
                                try
                                {
                                    USLogEvent.WriteToFile("Report : CreateFolder started..", _userName);
                                    reportingService.CreateFolder(category, "/" + GetGymCode(CoreSessionUserName), props);
                                    string parent = "/" + GetGymCode(CoreSessionUserName) + "/" + category;
                                    USLogEvent.WriteToFile("Report : CreateDataSource started in "+category+".." + GetGymCode(CoreSessionUserName)+": Connection String- "+definition.ConnectString, _userName);
                                    reportingService.CreateDataSource(name, parent, false, definition, null);
                                }
                                catch (SoapException e)
                                {
                                    USLogError.WriteToFile("Report : Exception Occour while creating the Data Source " + e.Message, e, _userName);
                                    return e.Message;
                                }
                            }
                        }
                    }
                    else
                    {
                        CatalogItem[] items = reportingService.ListChildren("/", false);
                        if (items != null)
                        {
                            length = items.Length;
                        }
                        if (length != (-1))
                        {
                            for (int i = 0; i < length; i++)
                            {
                                if (items[i].Name == category)
                                {
                                    folderExist = true;
                                    break;
                                }
                            }
                        }

                        if (!folderExist)
                        {
                            try
                            {
                                USLogEvent.WriteToFile("Report : CreateFolder started..", _userName);
                                reportingService.CreateFolder(category, "/", props);
                                string parent = "/" + category;
                                USLogEvent.WriteToFile("Report : CreateDataSource started..", _userName);
                                reportingService.CreateDataSource(name, parent, false, definition, null);
                            }
                            catch (SoapException e)
                            {
                                USLogError.WriteToFile("Report : Exception Occour while creating the Data Source " + e.Message, e, _userName);
                                return e.Message;
                            }
                        }
                    }

                    return "true";
                }
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile("Report : Exception Occour while creating the Data Source " + ex.Message, ex, _userName);
                return ex.Message;
            }
        }

        private static string CreateReport(string content, ReportingService2005 rs, string reportName, string category, string CoreSessionUserName)
        {
            GetReportSettings(CoreSessionUserName);
            USLogEvent.WriteToFile("Report : CreateReport initiated.", _userName);
            byte[] definition = null;
            //Warning[] warnings = null;
            try
            {
                definition = Encoding.UTF8.GetBytes(content);
                MemoryStream stream = new MemoryStream(definition.Length);
                stream.Read(definition, 0, (int)stream.Length);
                stream.Close();
                // Create a custom property for the folder.
                //warnings = rs.CreateReport(reportName, @"/" + category, false, definition, null);
                if (USPRunTimeVariables.GetConnectionSource() != "Config")
                {
                    USLogEvent.WriteToFile("Report :" + reportName + ": CreateReport started..", _userName);
                    rs.CreateReport(reportName, @"/" + GetGymCode(CoreSessionUserName) + "/" + category, false, definition, null);
                }
                else
                {
                    USLogEvent.WriteToFile("Report :" + reportName + ": CreateReport started..", _userName);
                    rs.CreateReport(reportName, @"/" + category, false, definition, null);
                }
            }
            catch (SoapException e)
            {
                USLogError.WriteToFile("Report : Exception Occour while creating the report " + e.Message, e, _userName);
                return e.Message;
            }
            return "true";
        }

        //private static int CheckReportIsExist(ReportingService2005 reportingService, string reportName, string path, string coreSessionUserName)
        //{
        //    bool reportExistInReportServer = false;
        //    bool reportExistInDB = false;
        //    //check the report server
        //    CatalogItem[] items = reportingService.ListChildren(path, false);
        //    foreach (var item in items)
        //    {
        //        if (reportName == item.Name)
        //        {
        //            reportExistInReportServer = true;
        //        }
        //    }
        //    //check the DB
        //    GetReportSettings(coreSessionUserName);
        //    if (USPRunTimeVariables.GetConnectionSource() != "Config")
        //    {
        //        reportExistInDB = ReportingFacade.CheckReportIsExistInDB(reportName, path, coreSessionUserName);
        //    }
        //    else
        //    {
        //        reportExistInDB = ReportingFacade.CheckReportIsExistInDB(reportName, path.Split('/')[1], coreSessionUserName);
        //    }

        //    //return the value according to the 'reportExistInReportServer' and 'reportExistInDB' values
        //    if (!reportExistInReportServer && !reportExistInDB)
        //        return 0;
        //    else if (reportExistInReportServer && !reportExistInDB)
        //        return 1;
        //    else if (!reportExistInReportServer && reportExistInDB)
        //        return 2;
        //    else
        //        return 3;

        //}

        //private static void EnableDataSource(ReportingService2005 rs, string dsName, string CoreSessionUserName)
        //{
        //    try
        //    {
        //        GetReportSettings(CoreSessionUserName);
        //        rs.EnableDataSource(dsName);
        //    }
        //    catch (SoapException exp)
        //    {
        //        USLogError.WriteToFile("Exception Occour while EnableDataSource : " + exp.Message, exp, "User");
        //    }
        //}
        private static bool DeleteReportFromServer(string userName, string password, string item, string CoreSessionUserName)
        {
            GetReportSettings(CoreSessionUserName);
            USLogEvent.WriteToFile("Report : DeleteReportFromServer initiated.", _userName);
            ReportingService2005 rs = new ReportingService2005(_urlForReportServer);
            try
            {
                if (CoreSessionUserName.Contains('/'))
                {
                    item = "/" + CoreSessionUserName.Split('/')[0].ToUpper() + item;
                    rs.Credentials = new NetworkCredential(userName, password, "");
                    rs.DeleteItem(item);
                }
                else
                {
                    //wanna to remove the below line
                    //  item = "/UNI" + item;
                    rs.Credentials = new NetworkCredential(userName, password, "");
                    rs.DeleteItem(item);
                }
            }
            catch (Exception e)
            {
                USLogError.WriteToFile("Report : Delete Report Failed : " + e.Message, e, _userName);
                return false;
            }
            return true;
        }
        //private static bool SetItemDataSource(ReportingService2005 rs, string reportName, string CoreSessionUserName)
        //{
        //    try
        //    {
        //        GetReportSettings(CoreSessionUserName);
        //        DataSource[] dataSources = rs.GetItemDataSources("/" + reportName);
        //        for (int i = 0; i < dataSources.Length; i++)
        //        {
        //            string dsName = "DataSource1";
        //            DataSource serverDS = new DataSource();
        //            DataSourceReference serverDSRef = new DataSourceReference();
        //            serverDSRef.Reference = "/" + dsName;
        //            serverDS.Item = serverDSRef;
        //            serverDS.Name = dsName;
        //            DataSource[] serverDataSources = new DataSource[] { serverDS };
        //            rs.SetItemDataSources("/" + reportName, serverDataSources);
        //        }
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //    return true;
        //}
        //format of the input parameter should be 'GymCode/UserName'
        //private static string GetUserName(string coreSessionuserName)
        //{
        //    if (USPRunTimeVariables.GetConnectionSource() != "Config" && coreSessionuserName.Contains("/"))
        //    {
        //        return coreSessionuserName.Split('/')[1];
        //    }
        //    else
        //        return coreSessionuserName;
        //}
        private static string GetGymCode(string coreSessionuserName)
        {
            //wanna to remove the below line
            // coreSessionuserName = "UNI/" + coreSessionuserName;
            if (USPRunTimeVariables.GetConnectionSource() != "Config" && coreSessionuserName.Contains("/"))
            {
                return coreSessionuserName.Split('/')[0].ToUpper();
            }
            else
                return coreSessionuserName.ToUpper();
            //return string.Empty;
        }

        public static string IsQueryVisible()
        {
            string isQueryVisible = "";
            if (USPRunTimeVariables.GetConfigurationByName("QueryVisible").ToString() == "Enable")
            {

                isQueryVisible = "Visible";
            }
            else
            {
                isQueryVisible = "Collapsed";
            }
            return isQueryVisible;
        }

        public static List<ReportingEntityProperty> GetEntityPropertyInfo(int filterEntityId, ReportEntity reportEntity, string CoreSessionUserName)
        {
            GetReportSettings(CoreSessionUserName);
            return ReportingFacade.GetEntityPropertyInfo(filterEntityId, reportEntity, CoreSessionUserName);
        }

        public static int SendReportOutPut(bool considerationGDPR, string userName, string subject, string body, bool isSelected, int outputType, List<string> recipientList, string primaryColumn, string basedOn, bool reminder, bool addreminderFee, int branchID, DateTime? reminderDuedate)
        {
            GetReportSettings(userName);
            return ReportingFacade.SendReportOutPut(considerationGDPR, userName, subject, body, isSelected, outputType, recipientList, primaryColumn, basedOn, reminder, addreminderFee, branchID, reminderDuedate);
        }

        //Admin pannel implementations
        public static string SaveReportSetting(string userName, ReportConfigureSetting settingItem)
        {
            GetReportSettings(userName);
            USLogEvent.WriteToFile("Report : SaveReportSetting initiated.", _userName);
            return ReportingFacade.SaveReportSetting(userName, settingItem);
        }

        public static ReportConfigureSetting GetReportSetting(string userName)
        {
            //GetReportSettings(userName);
            USLogEvent.WriteToFile("Report : GetReportSetting initiated.", userName);
            return ReportingFacade.GetReportSetting(userName);
        }

        public static List<ReportScheduleTemplate> GetScheduleTemplateList(KeyValueObject category, string entity,string userName)
        {
            USLogEvent.WriteToFile("Report : GetScheduleTemplateList initiated.", userName);
            return ReportingFacade.GetScheduleTemplateList(category, entity, userName);
        }
        public static bool ExecuteReportScheduleTask(string GymCode)
        {
            try
            {
                USLogEvent.WriteToFile("Report : ExecuteReportScheduleTask initiated.", GymCode);
                return ReportingFacade.ExecuteReportScheduleTask(GymCode);
            }
            catch (Exception)
            {              
                throw;
            }

        }
        public static bool ExecuteReportScheduleTaskFollowUp(string GymCode)
        {
            try
            {
                USLogEvent.WriteToFile("Report : ExecuteReportScheduleTaskFollowUP initiated.", GymCode);
                return ReportingFacade.ExecuteReportScheduleTaskFollowUp(GymCode);
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        public static bool ExecuteReportSchedule_MoveToHistoryTask(string GymCode)
        {
            try
            {
                USLogEvent.WriteToFile("Report : ExecuteReportSchedule_MoveToHistoryTask initiated.", GymCode);
                return ReportingFacade.ExecuteReportSchedule_MoveToHistoryTask(GymCode);
            }
            catch (Exception)
            {
                throw;
            }

        }

        public static Dictionary<string, bool> GetEnbleEntitesByEntity(string EntityName, string userName)
        {
            try
            {
                USLogEvent.WriteToFile("Report : GetEnbleEntitesByEntity initiated.", userName);
                return ReportingFacade.GetEnbleEntitesByEntity(EntityName, userName);
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public static int SaveChangePropertyDisplayName(List<EntityPropertyForGrid> propertyList, string userName)
        {
            try
            {
                USLogEvent.WriteToFile("Report : SaveChangePropertyDisplayName initiated.", userName);
                return ReportingFacade.SaveChangePropertyDisplayName(propertyList, userName);
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public static List<EntityPropertyForGrid> GetEntityWithProperties(int entityId, string userName)
        {
            try
            {
                USLogEvent.WriteToFile("Report : GetEntityWithProperties initiated.", userName);
                return ReportingFacade.GetEntityWithProperties(entityId, userName);
            }
            catch (Exception ex)
            {                
                throw ex;
            }
        }

        public static List<GymUsage> GetGymUsageReportSetting(int branchId, string userName)
        {
            try
            {
                USLogEvent.WriteToFile("Report : GetGymUsageReportSetting initiated.", userName);
                return ReportingFacade.GetGymUsageReportSetting(branchId, userName);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static bool UpdateGymUsageReportSetting(List<GymUsage> settingList, int branchId, string userName)
        {
            try
            {
                USLogEvent.WriteToFile("Report : UpdateGymUsageReportSetting initiated.", userName);
                return ReportingFacade.UpdateGymUsageReportSetting(settingList, branchId, userName);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static List<UserRole> GetRoleListByReportId(int reportId, string user)
        {
            try
            {
                return ReportingFacade.GetRoleListByReportId(reportId, user);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int GetReminderDueDays(string user)
        {
            try
            {
                return ReportingFacade.GetReminderDueDays(user);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
