﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using US.Payment.Core.Utils;
using System.Data.SqlClient;
using US.Payment.Reporting.Core.DomainObjects;

namespace US.Payment.Reporting.Data.Commands
{
    public class SaveChangePropertyDisplayNameAction : USDBActionBase<int>
    {
        private List<EntityPropertyForGrid> _propertyList = new List<EntityPropertyForGrid>();
        private string _userName = string.Empty;

        public SaveChangePropertyDisplayNameAction(List<EntityPropertyForGrid> propertyList, string userName)
        {
            _propertyList = propertyList;
            _userName = userName;
        }
        protected override int Body(System.Data.Common.DbConnection connection)
        {
            foreach (var item in _propertyList)
            {
                try
                {
                    string storedProcedureName = "USRPT.SaveChangePropertyDisplayName";
                    DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@PropertyID", DbType.Int32,item.PropertyId));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@NewDisplayName", DbType.String,item.PropertyReturnName));

                    DbParameter propertyIDParameter = new SqlParameter();
                    propertyIDParameter.DbType = DbType.Int32;
                    propertyIDParameter.Direction = ParameterDirection.Output;
                    propertyIDParameter.ParameterName = "@ReturnNumber";
                    cmd.Parameters.Add(propertyIDParameter);

                    cmd.ExecuteNonQuery();
                    
                }
                catch (Exception)
                {

                    throw;
                }
            }
            return 1;
        }
    }
}
