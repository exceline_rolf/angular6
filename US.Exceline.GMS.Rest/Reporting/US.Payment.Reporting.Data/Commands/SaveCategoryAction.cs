﻿using System;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using US.Common.Logging.API;
using System.Configuration;

namespace US.Payment.Reporting.Data.Commands
{
    class SaveCategoryAction : USDBActionBase<bool>
    {
        string _userName = string.Empty;
        private string  _categoryName = string.Empty;

        public SaveCategoryAction(string categoryName, string CoreSessionUserName)
        {
            _categoryName = categoryName;
            _userName = CoreSessionUserName;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            try
            {
                string storedProcedureName = "USRPT.SaveNewReportCategory";
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@NewCategoryName", DbType.String, _categoryName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _userName));
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile("Report : Exception Occour while SaveCategoryAction " + ex.Message, ex, _userName);
                throw;
            }
            return true;
        }
    }
}