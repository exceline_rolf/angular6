﻿using System;
using System.Collections.Generic;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using System.Configuration;
using US.Common.Logging.API;
using US.Payment.Core.BusinessDomainObjects.Reporting;

namespace US.Payment.Reporting.Data.Commands
{
    class GetReportInfoItemListAction : USDBActionBase<List<ReportInfo>>
    {
        string _userName = ConfigurationManager.AppSettings["userName"];
        List<ReportInfo> _resultReportInfoList = new List<ReportInfo>();
        public GetReportInfoItemListAction()
        {
        }

        protected override List<ReportInfo> Body(System.Data.Common.DbConnection connection)
        {
            try
            {
                string storedProcedureName = "dbo.USP_RPT_GetReportInfoItemList";
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ReportInfo reportInfo = new ReportInfo();
                    reportInfo.Name = Convert.ToString(reader["Name"]);
                    reportInfo.Description = Convert.ToString(reader["Description"]);
                    reportInfo.Path = Convert.ToString(reader["Path"]);
                    _resultReportInfoList.Add(reportInfo);
                }

            }
            catch (Exception ex)
            {
                USLogError.WriteToFile("Report : Exception Occour while GetReportInfoItemListAction " + ex.Message, ex, _userName);
                throw;
            }
            return _resultReportInfoList;
        }

    }
}
