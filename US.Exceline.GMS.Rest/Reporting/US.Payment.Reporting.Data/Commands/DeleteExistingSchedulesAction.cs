﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Common.Logging.API;
using US_DataAccess;

namespace US.Payment.Reporting.Data.Commands
{
    public class DeleteExistingSchedulesAction : USDBActionBase<string>
    {
        private string _userName = string.Empty;
        private int _reportID = 0;

        public DeleteExistingSchedulesAction(string userName,int reportId)
        {
            _userName = userName;
            _reportID = reportId;
        }
        protected override string Body(System.Data.Common.DbConnection connection)
        {
            try
            {
                string storedProcedure = "USRPT.DeleteExistingSchedule";
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ReportId", DbType.Int32, _reportID));
                cmd.ExecuteNonQuery();
                return "true";
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile("Report : Exception Occour while DeleteExistingSchedulesAction " + ex.Message, ex, _userName);
                return ex.Message;
            }
        }
    }
}
