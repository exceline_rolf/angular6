﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using US.Common.Logging.API;
using US.Payment.Core.Utils;
using US.Payment.Reporting.Core.DomainObjects;
using US_DataAccess;

namespace US.Payment.Reporting.Data.Commands
{
    class SaveReportAction : USDBActionBase<string>
    {
        private readonly string _userName = string.Empty;
        private readonly USReport _reportToBeSaved;

        public SaveReportAction(USReport reportToBeSaved, string coreSessionUserName)
        {
            _reportToBeSaved = reportToBeSaved;
            _userName = coreSessionUserName;
        }
        protected override string Body(DbConnection connection)
        {
            try
            {
                const string storedProcedureName = "USRPT.SaveReport";
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Name", DbType.String, _reportToBeSaved.Name));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Category", DbType.String, _reportToBeSaved.Category));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@RoleId", DbType.String, string.Join(",", _reportToBeSaved.RoleList.Where(x => x.IsView).Select(role => role.Id))));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@RoleIdForEdit", DbType.String, string.Join(",", _reportToBeSaved.RoleList.Where(x => x.IsEditable).Select(role => role.Id))));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _userName));
                DbParameter reportIdParameter = new SqlParameter();
                reportIdParameter.DbType = DbType.Int32;
                reportIdParameter.Direction = ParameterDirection.Output;
                reportIdParameter.ParameterName = "@ReportID";
                cmd.Parameters.Add(reportIdParameter);

                var xmlString = XMLUtils.SerializeObject(_reportToBeSaved);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Report", DbType.Xml, xmlString.Replace("<?xml version=\"1.0\" encoding=\"utf-32\"?>", "")));
                if (_reportToBeSaved.Description == null)
                {
                    _reportToBeSaved.Description = "US Report Module Report";
                }
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Description", DbType.String, _reportToBeSaved.Description));
                cmd.ExecuteNonQuery();

                //Scheduling....
                var reportId = int.Parse(reportIdParameter.Value.ToString());
                //if (Report is a scheduling report),implement here to add data to US_NotificationSchedule table..

                if (_reportToBeSaved.IsScheduleReport)
                {
                    //implement here to add data to US_NotificationSchedule table..
                    ReportingFacade.SaveReportSchedule(false, _userName, reportId, _reportToBeSaved, _reportToBeSaved.ReportScheduleList);
                }
                return "true";

            }
            catch (Exception ex)
            {
                USLogError.WriteToFile("Report : Exception Occour while SaveReportAction " + ex.Message, ex, _userName);
                throw;
            }
        }
    }
}
