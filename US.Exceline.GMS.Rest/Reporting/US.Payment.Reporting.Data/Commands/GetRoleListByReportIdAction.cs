﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Payment.Reporting.Core.DomainObjects;
using US_DataAccess;

namespace US.Payment.Reporting.Data.Commands
{
    public class GetRoleListByReportIdAction : USDBActionBase<List<UserRole>>
    {
        private readonly int _reportId = -1;

        public GetRoleListByReportIdAction(int reportId)
        {
            _reportId = reportId;
        }

        protected override List<UserRole> Body(DbConnection connection)
        {
            try
            {
                var roleList = new List<UserRole>();
                const string storedProcedureName = "USRPT.GetRoleListByReportId";
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@reportId", DbType.Int32, _reportId));
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var role = new UserRole
                    {
                        Id = Convert.ToInt32(reader["Id"]),
                        RoleName = Convert.ToString(reader["RoleName"]),
                        IsView = Convert.ToBoolean(reader["IsView"]),
                        IsEditable = Convert.ToBoolean(reader["IsEditable"])
                    };
                    roleList.Add(role);
                }
                return roleList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
