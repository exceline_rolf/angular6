﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using US.Common.Logging.API;
using US.Payment.Reporting.Core.DomainObjects;
using US_DataAccess;

namespace US.Payment.Reporting.Data.Commands
{
    class GetJoinQueryListAction : USDBActionBase<List<JoinQuery>>
    {
        string _userName = string.Empty;

        public GetJoinQueryListAction(string CoreSessionUserName)
        {
            _userName = CoreSessionUserName;
        }

        protected override List<JoinQuery> Body(System.Data.Common.DbConnection connection)
        {
            List<JoinQuery> joinQueryList = new List<JoinQuery>();
            try
            {
                string storedProcedureName = "USRPT.GetAllJoinQueryList";
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    JoinQuery query = new JoinQuery();
                    query.FirstEntity = Convert.ToString(reader["FirstEntityName"]);
                    query.SencondEntity = Convert.ToString(reader["SecondEntityName"]);
                    query.JoinQueryText = Convert.ToString(reader["JoinQueryText"]);
                    joinQueryList.Add(query);
                }

            }
            catch (Exception ex)
            {
                USLogError.WriteToFile("Report : Exception Occour while GetJoinQueryListAction " + ex.Message, ex, _userName);
                throw;
            }
            return joinQueryList;
        }
    }
}
