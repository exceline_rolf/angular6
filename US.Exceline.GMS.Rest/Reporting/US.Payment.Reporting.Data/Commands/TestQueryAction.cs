﻿using System;
using System.Collections.Generic;
using US_DataAccess;
using System.Data.Common;
using US.Common.Logging.API;
using System.Configuration;

namespace US.Payment.Reporting.Data.Commands
{
    public class TestQueryAction : USDBActionBase<List<string >>
    {
        private string _userName = string.Empty;
        private string _query = string.Empty;

        public TestQueryAction(string query, string CoreSessionUserName)
        {
            _query = query;
            _userName = CoreSessionUserName;
        }
        protected override List<string> Body(DbConnection connection)
        {
            List<string> returnedDataStringList = new List<string>();
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.Text, _query);
                DbDataReader dataReader = cmd.ExecuteReader();

                string headerItem = string.Empty;
                string valueItem = string.Empty;
                bool FirstTime = true;

                while (dataReader.Read())
                {
                    valueItem = string.Empty;
                    if (FirstTime)
                    {
                        FirstTime = false;
                        for (int i = 0; i < dataReader.FieldCount; i++)
                        {
                            headerItem = headerItem + dataReader.GetName(i).ToString() + ";";
                        }
                        headerItem = headerItem.TrimEnd(';');
                        returnedDataStringList.Add(headerItem);
                    }


                    for (int i = 0; i < dataReader.FieldCount; i++)
                    {
                        string temp = string.Empty;
                        if (dataReader[i].GetType().FullName == "System.DateTime")
                        {
                            DateTime dt = Convert.ToDateTime(dataReader[i]);
                            temp = dt.ToString("d");
                        }
                        else
                            temp = Convert.ToString(dataReader[i]);
                        if (string.IsNullOrEmpty(temp))
                            temp = "-";
                        valueItem = valueItem + temp + ";";
                    }
                    valueItem = valueItem.TrimEnd(';');
                    returnedDataStringList.Add(valueItem);
                }

            }
            catch (Exception e)
            {
                USLogError.WriteToFile("Report : Exception Occour while TestQueryAction " + e.Message, e, _userName);
                return null;
            }
            return returnedDataStringList;
        }
    }
}

