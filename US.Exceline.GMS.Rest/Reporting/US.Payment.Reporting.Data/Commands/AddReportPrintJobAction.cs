﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Payment.Reporting.Data.Commands
{
    public class AddReportPrintJobAction : USDBActionBase<int>
    {
        private string _userName = string.Empty;
        private List<string> _recipientList = new List<string>();
        private bool _reminder = false;
        private bool _addreminderFee = false;
        private int _branchID = -1;
        private DateTime? _reminderDuedate;
             

        public AddReportPrintJobAction(string userName, List<string> recipientList, bool reminder, bool addreminderFee, int branchID, DateTime? reminderDueDate)
        {
            _userName = userName;
            _recipientList = recipientList;
            _reminder = reminder;
            _addreminderFee = addreminderFee;
            _branchID = branchID;
            _reminderDuedate = reminderDueDate;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExcGMSRptAddPrintJob";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Reminder", DbType.Boolean, _reminder));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AddReminderFee", DbType.Boolean, _addreminderFee));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _userName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchID", DbType.Int32, _branchID));
                if(_reminderDuedate.HasValue)
                   cmd.Parameters.Add(DataAcessUtils.CreateParam("@RmdDueDate", DbType.DateTime, _reminderDuedate));

                DataTable table = new DataTable();
                table.Columns.Add("ID", typeof(int));
                for (int i = 0; i < _recipientList.Count; i++)
                {
                    table.Rows.Add(Convert.ToInt32(_recipientList[i].Trim()));
                }
                SqlParameter parameter = new SqlParameter();
                parameter.ParameterName = "@list";
                parameter.SqlDbType = SqlDbType.Structured;
                parameter.Value = table;
                cmd.Parameters.Add(parameter);

                cmd.ExecuteNonQuery();
                return 1;
            }
            catch (Exception)
            {
                return -1;
            }
        }
    }
}
