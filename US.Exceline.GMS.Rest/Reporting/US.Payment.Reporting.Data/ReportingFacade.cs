﻿using System;
using System.Collections.Generic;
using US_DataAccess;
using US.Payment.Reporting.Data.Commands;
using US.Payment.Reporting.Core.DomainObjects;
using US.Payment.Core;
using US.Payment.Core.BusinessDomainObjects.Reporting;

namespace US.Payment.Reporting.Data
{
    public class ReportingFacade
    {
        public static List<ReportInfo> GetReportInfoList()
        {
            List<ReportInfo> reportInfoItemList = new List<ReportInfo>();
            {
                try
                {
                    GetReportInfoItemListAction reportInfoDataAction = new GetReportInfoItemListAction();
                    reportInfoItemList = reportInfoDataAction.Execute(EnumDatabase.Exceline);
                    return reportInfoItemList;
                }
                catch
                {
                    throw;
                }
            }
        }
        public static List<ReportingEntityProperty> GetEntityPropertyInfo(int _filterByID, ReportEntity reportEntity, string CoreSessionUserName)
        {
            List<ReportingEntityProperty> reportInfoItemList = new List<ReportingEntityProperty>();
            {
                try
                {
                    GetEntityPropertyInfoAction reportInfoDataAction = new GetEntityPropertyInfoAction(_filterByID, reportEntity);
                    reportInfoItemList = reportInfoDataAction.Execute(EnumDatabase.Exceline, GetGymCode(CoreSessionUserName));
                    return reportInfoItemList;
                }
                catch
                {
                    throw;
                }
            }
        }

        public static List<ReportEntity> GetFilteredReportEntities(List<string> reportEntities, string CoreSessionUserName)
        {
            List<ReportEntity> reportInfoItemList = new List<ReportEntity>();
            {
                try
                {
                    foreach (string entity in reportEntities)
                    {
                        GetFilteredReportEntitiesAction reportInfoDataAction = new GetFilteredReportEntitiesAction(entity, CoreSessionUserName);
                        reportInfoItemList.Add(reportInfoDataAction.Execute(EnumDatabase.Exceline, GetGymCode(CoreSessionUserName)));
                    }
                    return reportInfoItemList;
                }
                catch
                {
                    throw;
                }
            }
        }
        public static List<USReport> ViewAllReports(string CoreSessionUserName)
        {
            List<USReport> reportInfoItemList = new List<USReport>();
            {
                try
                {
                    ViewAllReportsAction reportInfoDataAction = new ViewAllReportsAction(CoreSessionUserName);
                    reportInfoItemList = reportInfoDataAction.Execute(EnumDatabase.Exceline, GetGymCode(CoreSessionUserName));
                    return reportInfoItemList;
                }
                catch
                {
                    throw;
                }
            }
        }
        public static List<ReportSchedule> GetScheduleListByReportId(int reportId, string CoreSessionUserName)
        {

            {
                try
                {

                    List<ReportSchedule> scheduleList = new GetScheduleListByReportIdAction(reportId, CoreSessionUserName).Execute(EnumDatabase.Exceline, CoreSessionUserName.Split('/')[0]);

                    return scheduleList;
                }
                catch
                {
                    throw;
                }
            }
        }
        public static USReport GetReportById(int Id, string CoreSessionUserName)
        {
            USReport report = new USReport();
            {
                try
                {
                    GetReportByIdAction reportInfoDataAction = new GetReportByIdAction(Id);
                    report = reportInfoDataAction.Execute(EnumDatabase.Exceline, GetGymCode(CoreSessionUserName));
                    return report;
                }
                catch
                {
                    throw;
                }
            }
        }
        //public static bool Updatereport(USReport report)
        //{
        //    return false;
        //}
        public static bool DeleteReport(int reportIdToBeDeleted, string CoreSessionUserName)
        {
            DeleteReportAction action = new DeleteReportAction(reportIdToBeDeleted, CoreSessionUserName);
            return action.Execute(EnumDatabase.Exceline, GetGymCode(CoreSessionUserName));
        }
        //public static DbParameter CreateParam(string paramName, DbType type, object value)
        //{
        //    DbParameter param = new DbParameter();
        //    param.ParameterName = paramName;
        //    param.DbType = type;
        //    param.Value = value;
        //    return param;
        //}


        public static List<ReportEntity> GetAllEntityList(string CoreSessionUserName)
        {
            var reportInfoItemList = new List<ReportEntity>();
            {
                try
                {
                    var reportInfoDataAction = new GetAllEntitiesAction(CoreSessionUserName);
                    reportInfoItemList = reportInfoDataAction.Execute(EnumDatabase.Exceline, GetGymCode(CoreSessionUserName));
                    return reportInfoItemList;
                }
                catch
                {
                    throw;
                }
            }
        }

        public static List<string> GetAllReportCategories(string CoreSessionUserName)
        {
            List<string> reportInfoItemList = new List<string>();
            {
                try
                {
                    GetAllReportCategoriesAction reportInfoDataAction = new GetAllReportCategoriesAction(CoreSessionUserName);
                    reportInfoItemList = reportInfoDataAction.Execute(EnumDatabase.Exceline, GetGymCode(CoreSessionUserName));
                    return reportInfoItemList;
                }
                catch
                {
                    throw;
                }
            }
        }
        public static List<UserRole> GetAllRoles(string CoreSessionUserName)
        {
            List<UserRole> reportInfoItemList = new List<UserRole>();
            {
                try
                {
                    GetAllRolesAction reportInfoDataAction = new GetAllRolesAction(CoreSessionUserName);
                    reportInfoItemList = reportInfoDataAction.Execute(EnumDatabase.Exceline, GetGymCode(CoreSessionUserName));
                    return reportInfoItemList;
                }
                catch
                {
                    throw;
                }
            }
        }
        public static bool SaveCategory(string name, string CoreSessionUserName)
        {
            bool reportInfoItemList = false;
            {
                try
                {
                    SaveCategoryAction reportInfoDataAction = new SaveCategoryAction(name, CoreSessionUserName);
                    reportInfoItemList = reportInfoDataAction.Execute(EnumDatabase.Exceline, GetGymCode(CoreSessionUserName));
                    return reportInfoItemList;
                }
                catch
                {
                    throw;
                }
            }
        }

        public static bool UpdateCategory(string oldName, string newName, string CoreSessionUserName)
        {
            bool reportInfoItemList = false;
            {
                try
                {
                    UpdateCategoryAction reportInfoDataAction = new UpdateCategoryAction(oldName, newName, CoreSessionUserName);
                    reportInfoItemList = reportInfoDataAction.Execute(EnumDatabase.Exceline, GetGymCode(CoreSessionUserName));
                    return reportInfoItemList;
                }
                catch
                {
                    throw;
                }
            }
        }
        public static string SaveReport(USReport reportToBeSaved, string CoreSessionUserName)
        {
            SaveReportAction action = new SaveReportAction(reportToBeSaved, CoreSessionUserName);
            return action.Execute(EnumDatabase.Exceline, GetGymCode(CoreSessionUserName));
        }

        public static bool SaveEditedReport(int ID, USReport reportToBeSaved, string CoreSessionUserName)
        {
            SaveEditedReportAction action = new SaveEditedReportAction(ID, reportToBeSaved, CoreSessionUserName);
            return action.Execute(EnumDatabase.Exceline, GetGymCode(CoreSessionUserName));
        }

        public static List<JoinQuery> GetJoinQueryList(string CoreSessionUserName)
        {
            GetJoinQueryListAction action = new GetJoinQueryListAction(CoreSessionUserName);
            return action.Execute(EnumDatabase.Exceline, GetGymCode(CoreSessionUserName));
        }

        public static USReport GetOldReportItem(int oldId, string CoreSessionUserName)
        {
            GetOldReportItemAction action = new GetOldReportItemAction(oldId, CoreSessionUserName);
            return action.Execute(EnumDatabase.Exceline, GetGymCode(CoreSessionUserName));
        }

        public static List<ReportEntity> GetAllEntitiesWithProperties(string CoreSessionUserName)
        {
            GetAllEntitiesWithPropertiesAction action = new GetAllEntitiesWithPropertiesAction(CoreSessionUserName);
            return action.Execute(EnumDatabase.Exceline, GetGymCode(CoreSessionUserName));
        }

        public static List<string> TestQuery(string query, string CoreSessionUserName)
        {
            TestQueryAction action = new TestQueryAction(query, CoreSessionUserName);
            return action.Execute(EnumDatabase.Exceline, GetGymCode(CoreSessionUserName));
        }

        public static List<string> FollowUpQuery(string query, string CoreSessionUserName)
        {
            FollowUpQueryAction action = new FollowUpQueryAction(query, CoreSessionUserName);
            return action.Execute(EnumDatabase.Exceline, GetGymCode(CoreSessionUserName));
        }

        public static List<ReportSetting> GetReportSettings(string CoreSessionUserName)
        {
            try
            {
                GetReportSettingsAction action = new GetReportSettingsAction();
                return action.Execute(EnumDatabase.Exceline, GetGymCode(CoreSessionUserName));
            }
            catch (System.Exception)
            {
                throw;
            }

        }
        //format of the input parameter should be 'GymCode/UserName'
        //private static string GetUserName(string coreSessionuserName)
        //{
        //    if (USPRunTimeVariables.GetConnectionSource() != "Config" && coreSessionuserName.Contains("/"))
        //    {
        //        return coreSessionuserName.Split('/')[1];
        //    }
        //    else
        //        return coreSessionuserName;
        //    //D:\USP_Build\Deployment\FileConvertorRoot\DataFiles
        //}
        private static string GetGymCode(string coreSessionuserName)
        {
            if (USPRunTimeVariables.GetConnectionSource() != "Config" && coreSessionuserName.Contains("/"))
            {
                return coreSessionuserName.Split('/')[0];
            }
            else
                //wanna to remove the below line
                // coreSessionuserName = "UNI";
                return coreSessionuserName;
        }

        public static int SendReportOutPut(bool considerationGDPR, string userName, string subject, string body, bool isSelected, int outputType, List<string> recipientList, string primaryColumn, string basedOn, bool reminder, bool addreminderFee, int branchID, DateTime? reminderDuedate)
        {
            if (outputType == 2) //print 
            {
                AddReportPrintJobAction action = new AddReportPrintJobAction(userName, recipientList, reminder, addreminderFee, branchID, reminderDuedate);
                return action.Execute(EnumDatabase.Exceline, GetGymCode(userName));
            }
            else //email or SMS
            {
                SendEmailOrSMSAction action = new SendEmailOrSMSAction(considerationGDPR, userName, subject, body, isSelected, outputType, recipientList, primaryColumn, basedOn, reminder, addreminderFee, reminderDuedate, branchID);
                return action.Execute(EnumDatabase.Exceline, GetGymCode(userName));
            }
        }

        public static bool CheckReportIsExistInDB(string reportName, string path, string coreSessionUserName)
        {
            CheckReportIsExistInDBAction action = new CheckReportIsExistInDBAction(reportName, path, coreSessionUserName);
            return action.Execute(EnumDatabase.Exceline, GetGymCode(coreSessionUserName));
        }

        public static string SaveReportSetting(string userName, ReportConfigureSetting settingItem)
        {
            SaveReportSettingAction action = new SaveReportSettingAction(userName, settingItem);
            return action.Execute(EnumDatabase.Exceline, GetGymCode(userName));
        }

        public static ReportConfigureSetting GetReportSetting(string _userName)
        {
            try
            {
                ReportConfigureSetting settingObject = new ReportConfigureSetting();
                GetReportSettingsAction action = new GetReportSettingsAction();
                settingObject.ReportSettingList = action.Execute(EnumDatabase.Exceline, GetGymCode(_userName));
                return settingObject;
            }
            catch (System.Exception)
            {
                return null;
            }
        }
        public static string SaveReportSchedule(bool isEditingReport, string userName, int reportId, USReport report, List<ReportSchedule> reportScheduleList)
        {
            try
            {
                SaveReportScheduleAction action = new SaveReportScheduleAction(isEditingReport, userName, reportId, report, reportScheduleList);
                action.Execute(EnumDatabase.Exceline, GetGymCode(userName));
                return "";
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static List<ReportScheduleTemplate> GetScheduleTemplateList(KeyValueObject category, string entity, string userName)
        {
            try
            {
                GetScheduleTemplateListAction action = new GetScheduleTemplateListAction(category, entity, userName);
                return action.Execute(EnumDatabase.Exceline, GetGymCode(userName));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static string DeleteExistingSchedules(string _userName, int _ID)
        {
            DeleteExistingSchedulesAction action = new DeleteExistingSchedulesAction(_userName, _ID);
            return action.Execute(EnumDatabase.Exceline, GetGymCode(_userName));
        }
        public static bool ExecuteReportScheduleTask(string GymCode)
        {
            try
            {
                ExecuteReportScheduleTaskAction action = new ExecuteReportScheduleTaskAction(GymCode);
                return action.Execute(EnumDatabase.Exceline, GymCode);
            }
            catch (Exception)
            {
                throw;
            }

        }
        public static bool ExecuteReportScheduleTaskFollowUp(string GymCode)
        {
            try
            {
                ExecuteReportScheduleTaskFollowUpAction action = new ExecuteReportScheduleTaskFollowUpAction(GymCode);
                return action.Execute(EnumDatabase.Exceline, GymCode);
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        public static bool ExecuteReportSchedule_MoveToHistoryTask(string GymCode)
        {
            try
            {
                ExecuteReportSchedule_MoveToHistoryTaskAction action = new ExecuteReportSchedule_MoveToHistoryTaskAction(GymCode);
                return action.Execute(EnumDatabase.Exceline, GymCode);
            }
            catch (Exception)
            {
                throw;
            }

        }

        public static Dictionary<string, bool> GetEnbleEntitesByEntity(string EntityName, string userName)
        {
            try
            {
                GetEnbleEntitesByEntityAction action = new GetEnbleEntitesByEntityAction(EntityName, userName);
                return action.Execute(EnumDatabase.Exceline, GetGymCode(userName));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static int SaveChangePropertyDisplayName(List<EntityPropertyForGrid> propertyList, string userName)
        {
            try
            {
                SaveChangePropertyDisplayNameAction action = new SaveChangePropertyDisplayNameAction(propertyList, userName);
                return action.Execute(EnumDatabase.Exceline, GetGymCode(userName));
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public static List<EntityPropertyForGrid> GetEntityWithProperties(int entityId, string userName)
        {
            try
            {
                var action = new GetEntityWithPropertiesAction(entityId, userName);
                return action.Execute(EnumDatabase.Exceline, GetGymCode(userName));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<GymUsage> GetGymUsageReportSetting(int branchId, string userName)
        {
            try
            {
                var action = new GetGymUsageReportSettingAction(branchId);
                return action.Execute(EnumDatabase.Exceline, GetGymCode(userName));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static bool UpdateGymUsageReportSetting(List<GymUsage> settingList, int branchId, string userName)
        {
            try
            {
                var action = new UpdateGymUsageReportSettingAction(settingList, branchId, userName);
                return action.Execute(EnumDatabase.Exceline, GetGymCode(userName));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static List<UserRole> GetRoleListByReportId(int reportId, string user)
        {
            try
            {
                var action = new GetRoleListByReportIdAction(reportId);
                return action.Execute(EnumDatabase.Exceline, GetGymCode(user));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int GetReminderDueDays(string user)
        {
            try
            {
                GetReminderDueDaysAction action = new GetReminderDueDaysAction();
                return action.Execute(EnumDatabase.Exceline, GetGymCode(user));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
