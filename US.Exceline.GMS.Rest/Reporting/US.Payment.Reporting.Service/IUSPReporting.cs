﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.IO;
using System.ServiceModel.Web;
using System.Data;
using US.Common.Notification.Core.DomainObjects;
using US.Payment.Reporting.Core.DomainObjects;
using US.Payment.Core.BusinessDomainObjects.Reporting;

namespace US.Payment.Reporting.Service
{
    [ServiceContract]
    public interface IPolicyRetriever
    {
        [OperationContract, WebGet(UriTemplate = "/clientaccesspolicy.xml")]
        Stream ProvidePolicyFile();
    }
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IUSPReporting
    {
        [OperationContract]
        List<ReportInfo> GetReportInfoList();
        [OperationContract]
        string GetReportServerRootPath();
        [OperationContract]
        List<ReportEntity> GetFilteredReportEntities(List<string> reportEntityList, string CoreSessionUserName);
        [OperationContract]
        string ViewAllReports(string CoreSessionUserName);
        [OperationContract]
        List<ReportSchedule> GetScheduleListByReportId(int reportId, string CoreSessionUserName);
        [OperationContract]
        string GetReportById(int reportId, string CoreSessionUserName);
     
        [OperationContract]
        List<ReportEntity> GetAllEntityList(string CoreSessionUserName);
        [OperationContract]
        string SaveReportConfiguration(string reportToBeSaved, string addedRDL, string CoreSessionUserName);
        [OperationContract]
        bool DeleteReport(int reportIdToBeDeleted, string item, string CoreSessionUserName);
        [OperationContract]
        string SaveEditedReportConfiguration(string reportToBeSaved, string fileName, string CoreSessionUserName);
        [OperationContract]
        string SaveAsReportConfiguration(string report,string addedRDL, string CoreSessionUserName);
        [OperationContract]
        List<string> GetAllReportCategories(string CoreSessionUserName);
        [OperationContract]
        List<UserRole> GetAllRoles(string CoreSessionUserName);
        [OperationContract]
        bool SaveCategory(string name, string CoreSessionUserName);
        [OperationContract]
        bool UpdateCategory(string oldName, string newName, string CoreSessionUserName);
        [OperationContract]
        List<ReportEntity> GetAllEntitiesWithProperties(string CoreSessionUserName);
        [OperationContract]
        bool SaveEditedReportQuery(string report, string CoreSessionUserName);
        [OperationContract]
        string TestQuery(string query, string CoreSessionUserName);
        [OperationContract]
        string FollowUpQuery(string query, string CoreSessionUserName);
        //[OperationContract]
        //WhereCondition getWhereCondition();
        [OperationContract]
        USReport getUSReport();
        // TODO: Add your service operations here

        [OperationContract]
        string IsQueryVisible();
        [OperationContract]
        List<ReportingEntityProperty> GetEntityPropertyInfo(int filterEntityId, ReportEntity reportEntity, string CoreSessionUserName);
        [OperationContract]
        string GetExcelineReportServerPath();
        [OperationContract]
        int SendReportOutPut(string userName, string subject, string body, bool isSelected, int outputType, List<string> recipientList, string primaryColumn, string basedOn, bool reminder, bool addreminderFee, int branchID, DateTime? reminderDueDate);
        [OperationContract]
        Dictionary<string, bool> GetEnbleEntitesByEntity(string EntityName, string userName);
        [OperationContract]
        int SaveChangePropertyDisplayName(List<EntityPropertyForGrid> propertyList, string userName);
    
        //Admin Pannel
        [OperationContract]
        string SaveReportSetting(string userName,ReportConfigureSetting settingItem);

        [OperationContract]
        ReportConfigureSetting GetReportSetting(string userName);

        //Scheduling...
        [OperationContract]
        List<ReportScheduleTemplate> GetScheduleTemplateList(KeyValueObject category, string entity,string userName);

        [OperationContract]
        List<EntityPropertyForGrid> GetEntityWithProperties(int entityId, string userName);

        [OperationContract]
        List<GymUsage> GetGymUsageReportSetting(int branchId, string userName);

        [OperationContract]
        bool UpdateGymUsageReportSetting(List<GymUsage> settingList, int branchId, string userName);

        [OperationContract]
        List<ExceNotificationTepmlateDC> GetSMSNotificationTemplateByEntity(string entity, string user);

        [OperationContract]
        List<UserRole> GetRoleListByReportId(int reportId, string user);

        [OperationContract]
        int GetReminderDueDays(string user);
    }
}
