﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Reporting.Core.DomainObjects
{
    public class PreFunction
    {
        private string _functionCode = string.Empty;

        public string FunctionCode
        {
            get { return _functionCode; }
            set { _functionCode = value; }
        }

        private string _functionDisplayName = string.Empty;

        public string FunctionDisplayName
        {
            get { return _functionDisplayName; }
            set { _functionDisplayName = value; }
        }

        private string _actingFunction = string.Empty;

        public string ActingFunction
        {
            get { return _actingFunction; }
            set { _actingFunction = value; }
        }

        private string _functionToolTip = string.Empty;

        public string FunctionToolTip
        {
            get { return _functionToolTip; }
            set { _functionToolTip = value; }
        }
    }
}
