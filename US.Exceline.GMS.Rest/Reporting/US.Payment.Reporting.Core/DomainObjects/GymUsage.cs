﻿using System;

namespace US.Payment.Reporting.Core.DomainObjects
{
    public class GymUsage
    {
        private int _id = -1;
        private DateTime _fromTime = new DateTime();
        private DateTime _toTime = new DateTime();
        private int _sunday = -1;
        private int _monday = -1;
        private int _tuesday = -1;
        private int _wednesday = -1;
        private int _thursday = -1;
        private int _friday = -1;
        private int _saturday = -1;
        private bool _isDelete = false;
        
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public DateTime FromTime
        {
            get { return _fromTime; }
            set { _fromTime = value; }
        }

        public DateTime ToTime
        {
            get { return _toTime; }
            set { _toTime = value; }
        }

        public int Sunday
        {
            get { return _sunday; }
            set { _sunday = value; }
        }

        public int Monday
        {
            get { return _monday; }
            set { _monday = value; }
        }

        public int Tuesday
        {
            get { return _tuesday; }
            set { _tuesday = value; }
        }

        public int Wednesday
        {
            get { return _wednesday; }
            set { _wednesday = value; }
        }

        public int Thursday
        {
            get { return _thursday; }
            set { _thursday = value; }
        }

        public int Friday
        {
            get { return _friday; }
            set { _friday = value; }
        }

        public int Saturday
        {
            get { return _saturday; }
            set { _saturday = value; }
        }

        public bool IsDelete
        {
            get { return _isDelete; }
            set { _isDelete = value; }
        }
    }
}
