﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Reporting.Core.DomainObjects
{

    public class ReportingEntityProperty
    {

        private string _id = string.Empty;

        public string Id
        {
            get { return _id; }
            set { _id = value; }
        }
        private string _displayName = string.Empty;

        public string DisplayName
        {
            get { return _displayName; }
            set { _displayName = value; }
        }
        private bool _selected = false;

        public bool Selected
        {
            get { return _selected; }
            set { _selected = value; }
        }
        private bool _aggregated = false;

        public bool Aggregated
        {
            get { return _aggregated; }
            set { _aggregated = value; }
        }
        private bool _grouped = false;

        public bool Grouped
        {
            get { return _grouped; }
            set { _grouped = value; }
        }

        private string _dataColumnName = string.Empty;

        public string DataColumnName
        {
            get { return _dataColumnName; }
            set { _dataColumnName = value; }
        }
        private string _dataColumnReturnName = string.Empty;

        public string DataColumnReturnName
        {
            get { return _dataColumnReturnName; }
            set { _dataColumnReturnName = value; }
        }
        private string _userParameterPromptNane = string.Empty;

        public string UserParameterPromptNane
        {
            get { return _userParameterPromptNane; }
            set { _userParameterPromptNane = value; }
        }
        private string _columnWidth = "4in";

        public string ColumnWidth
        {
            get { return _columnWidth; }
            set { _columnWidth = value; }
        }
        private string _dataType = "String";

        public string DataType
        {
            get { return _dataType; }
            set { _dataType = value; }
        }
        private string _description = string.Empty;

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private bool _inConditionFixValue = false;

        public bool InConditionFixValue
        {
            get { return _inConditionFixValue; }
            set { _inConditionFixValue = value; }
        }
        private string _conditionFixValue = string.Empty;

        public string ConditionFixValue
        {
            get { return _conditionFixValue; }
            set { _conditionFixValue = value; }
        }
        private bool _enableAggregate = false;

        public bool EnableAggregate
        {
            get { return _enableAggregate; }
            set { _enableAggregate = value; }
        }
        private bool _hideAggregate = false;

        public bool HideAggregate
        {
            get { return _hideAggregate; }
            set { _hideAggregate = value; }
        }
        private int _orderNo = -1;

        public int OrderNo
        {
            get { return _orderNo; }
            set { _orderNo = value; }
        }
        private bool _isPropertyEnable = true;

        public bool IsPropertyEnable
        {
            get { return _isPropertyEnable; }
            set { _isPropertyEnable = value; }
        }
    }
}
