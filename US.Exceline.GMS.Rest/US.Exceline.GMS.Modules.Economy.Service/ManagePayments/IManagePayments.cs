﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using US.Exceline.GMS.Modules.Economy.Core.DomainObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR;
using US.GMS.Core.SystemObjects;

//using US.Payment.Core.BusinessDomainObjects;
//using US.Payment.Modules.Economy.Core.DomainObjects;
//using US.Payment.Modules.Economy.Core.DomainObjects.ManagePayment;

namespace US.Exceline.GMS.Modules.Economy.Service.ManagePayments
{
    [ServiceContract(Name = "ManagePayments", Namespace = "US.Exceline.GMS.Modules.ExcEconomy.Service")]
    public interface IManagePayments
    {
        [OperationContract]
        List<US.Exceline.GMS.Modules.Economy.Core.DomainObjects.USPErrorPayment> GetErrorPaymentsByDateRange(DateTime from, DateTime to);

        [OperationContract]
        string GetErrorPaymentsByDateRangeAndErrorPaymentType(DateTime from, DateTime to, string errorPaymentType, string user,int branchId,string batchId=null);

        [OperationContract]
        ErrorPaymentMapping ErrorPaymentMapping();

        [OperationContract]
        USPErrorPaymentHistory USPErrorPaymentHistory();

        [OperationContract]
        List<OCRImportSummary> GetOCRImportHistory(DateTime? startDate, DateTime? endDate, string user);

        [OperationContract]
        bool RemovePayment(int paymentID, int arItemno,  string user, string type);

        [OperationContract]
        int MovePayment(int paymentID, int aritemno, string type, string user);

        [OperationContract]
        PDFPrintResultDC GenerateDeviationPrint(DateTime fromDate, DateTime toDate, int branchId, string user);

        [OperationContract]
        int AddSessionValue(string sessionKey, string value, string user);

        [OperationContract]
        List<PaymentImportLogDC> GetPaymentImportProcessLog(DateTime fromDate, DateTime toDate, string user);
    }
}
