﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using SessionManager;
using US.Common.Logging;
using US.Common.Logging.API;
using US.Communication.API;
using US.Exceline.GMS.Modules.Economy.API;
using US.Exceline.GMS.Modules.Economy.Core.DomainObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR;
using US.Exceline.GMS.Modules.Economy.Service.ManagePayments;
using US.GMS.API.ManageSystemSettings;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.Utils;

namespace US.Exceline.GMS.Modules.ExcEconomy.Service
{
    public partial class EconomyService : IManagePayments
    {
       
        public List<US.Exceline.GMS.Modules.Economy.Core.DomainObjects.USPErrorPayment> GetErrorPaymentsByDateRange(DateTime from, DateTime to)
        {
            //return USPPaymentManager.GetErrorPaymentsByDateRangeAndErrorPaymentType(null, from, to);
            return null;
        }

        public string GetErrorPaymentsByDateRangeAndErrorPaymentType(DateTime from, DateTime to, string errorPaymentType, string user, int branchId, string batchId=null)
        {
            OperationResult<List<US.Exceline.GMS.Modules.Economy.Core.DomainObjects.USPErrorPayment>> result = US.Exceline.GMS.Modules.Economy.API.DirectPayment.GetErrorPayments(from, to, errorPaymentType, ExceConnectionManager.GetGymCode(user), branchId, batchId);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return US.GMS.Core.Utils.XMLUtils.SerializeDataContractObjectToXML(new List<US.Exceline.GMS.Modules.Economy.Core.DomainObjects.USPErrorPayment>());
            }
            return US.GMS.Core.Utils.XMLUtils.SerializeDataContractObjectToXML(result.OperationReturnValue);
        }

        public ErrorPaymentMapping ErrorPaymentMapping()
        {
            return new ErrorPaymentMapping();
        }

        public USPErrorPaymentHistory USPErrorPaymentHistory()
        {
            return new USPErrorPaymentHistory();
        }

        public bool RemovePayment(int paymentID, int aritemNo, string user, string type)
        {
            OperationResult<bool> result = DirectPayment.RemovePayment(paymentID, aritemNo, user, ExceConnectionManager.GetGymCode(user), type);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            return true;
        }

        public List<OCRImportSummary> GetOCRImportHistory(DateTime? startDate, DateTime? endDate, string user)
        {
            OperationResult<List<OCRImportSummary>> result = OCR.GetOCRImportHistory(startDate, endDate, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<OCRImportSummary>();
            }
            return result.OperationReturnValue;
        }

        public int MovePayment(int paymentID, int aritemno, string type, string user)
        {
            OperationResult<int> result = DirectPayment.MovePayment(paymentID, aritemno, type, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            return result.OperationReturnValue;
        }

        public PDFPrintResultDC GenerateDeviationPrint(DateTime fromDate, DateTime toDate, int branchId, string user)
        {
            var result = new PDFPrintResultDC();
            try
            {
                var gymCode = ExceConnectionManager.GetGymCode(user);
                var template = ConfigurationManager.AppSettings["USC_DeviationLog_Print_App_Path"];

                var dataDictionary = new Dictionary<string, string>() { { "fromDate", fromDate.ToString("yyyy-MM-dd") }, { "toDate", toDate.ToString("yyyy-MM-dd") }, { "branchId", branchId.ToString() }, { "gymCode", gymCode } };
                var pdf = TemplateManager.ExecuteTemplate(dataDictionary, template, "PDF", gymCode).FirstOrDefault();
                if (pdf != null)
                {
                    result.FileParth = pdf.Messages.First(mssge => mssge.Header.ToLower().Equals("filepath")).Message;
                }

                var isVisibel = GMSManageGymSetting.GetSelectedGymSettings(new List<string> { "OtherIsPrieviewWhenPrint" }, true, branchId, ExceConnectionManager.GetGymCode(user));
                if (isVisibel.ErrorOccured)
                {
                    foreach (var message in isVisibel.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                    return null;
                }
                result.IsPriview = Convert.ToBoolean(isVisibel.OperationReturnValue["OtherIsPrieviewWhenPrint"]);
                return result;
            }
            catch (Exception ex)
            {
                USLogger.Log(ex.Message, new List<string> { "InvoicePrint", "Service" }, SeverityFilter.Error, null);
                return new PDFPrintResultDC();
            }
        }

        public int AddSessionValue(string sessionKey, string value, string user)
        {
            return Session.AddSessionValue(sessionKey, value, ExceConnectionManager.GetGymCode(user));
        }

        public List<PaymentImportLogDC> GetPaymentImportProcessLog(DateTime fromDate, DateTime toDate, string user)
        {
            OperationResult<List<PaymentImportLogDC>> result = OCR.GetPaymentImportProcessLog(fromDate, toDate, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (var message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<PaymentImportLogDC>();
            }
            return result.OperationReturnValue;
        }
    }
}

