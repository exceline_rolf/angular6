﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using US.Exceline.GMS.Modules.Economy.Core.DomainObjects;
using US.GMS.Core.DomainObjects.Admin;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.ExcEconomy.Service
{
    [ServiceContract]
    interface IEconomyService
    {
        [OperationContract]
        List<GymCompanyDC> GetGymCompanies(string user);

        [OperationContract]
        string GetClaimImportStatus(string user, string gymCode, int hit, int status, DateTime date, ImportStatusDC advancedData);

        [OperationContract]
        bool ReScheduleClaim(string user, string gymCode, int itemNo);

        [OperationContract]
        PaymentImportStatusDC GetPaymentImportStatus(string user, string gymCode, int hit);

        [OperationContract]
        string GetPaymentImportStatusXML(string user, string gymCode, int hit, DateTime regDate);

        [OperationContract]
        bool MoveToShopAccount(int statusId, bool isImportStatus, string user, string gymCode);

        [OperationContract]
        bool GetenerateATGFile(int branchID, int status, string user);

        [OperationContract]
        List<EconomyFileLogData> GetEconomyFileLogData(DateTime fromDate, DateTime toDate, string source, string filterBy, string user);

        [OperationContract]
        List<EconomyFileLogData> GetEconomyFileLogDetailData(EconomyLogFileHelper economyLogFileHelper);

        [OperationContract]
        int GetCcxEnabledStatus(int branchId, string user);

        [OperationContract]
        List<InvoiceDC> GetInvoiceList(string category, DateTime fromDate, DateTime toDate, int branchId, string user);

        [OperationContract]
        string GenerateInvoiceList(List<int> aritemList, int branchId, string user);

        [OperationContract]
        List<ExceUserBranchDC> GetUserSelectedBranches(string user);

        [OperationContract]
        bool UpdatePDFFileParth(int id, string fileParth, int branchId, string user);

        [OperationContract]
        List<HistoryPDFPrint> GetHistoryPDFPrintDetail(DateTime fromDate, DateTime toDate, string user);

        [OperationContract]
        int GenarateClassSalary(List<int> branchIdList, string user, List<int> employeeIdList, List<int> categoryIdList, DateTime fromDate, DateTime toDate);

        [OperationContract]
        List<ExcelineBranchDC> GetBranches(string user, int branchId);

        [OperationContract]
        List<GymEmployeeDC> GetEmployees(string user, int isActive);

        [OperationContract]
        List<CategoryDC> GetCategories(string type, string user);

        [OperationContract]
        List<ClassFileDetail> GetClassFileDetails(string user);

        [OperationContract]
        string GetClassSalaryFileDownloadUrl();

        [OperationContract]
        int GetGymCompanyId(string user);

        [OperationContract]
        ComboBoxMultipleItem getItem();

        [OperationContract]
        int UpdateBulkPDFPrintDetails(int id, string docType, string dataIdList, string pdfFilePath, int noOfDoc, int failCount, int branchId, string user);

        [OperationContract]
        List<BulkPDFPrint> GetBulkPDFPrintDetailList(string type, DateTime fromDate, DateTime toDate, int branchId, string user);

    }
}
