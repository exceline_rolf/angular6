﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using US.Common.Logging;
using US.Common.Logging.API;
using US.Communication.API;
using US.Exceline.GMS.Modules.Admin.API.ManageEmployees;
using US.Exceline.GMS.Modules.Admin.API.ManageSystemSettings;
using US.Exceline.GMS.Modules.Economy.API;
using US.Exceline.GMS.Modules.Economy.Core.DomainObjects;
using US.Exceline.GMS.Modules.Login.API;
using US.GMS.API;
using US.GMS.Core.DomainObjects.Admin;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.Utils;

namespace US.Exceline.GMS.Modules.ExcEconomy.Service
{
    public partial class EconomyService : IEconomyService
    {
        public List<GymCompanyDC> GetGymCompanies(string user)
        {
            OperationResult<List<GymCompanyDC>> result = GMSCcx.GetGymCompanies();
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<GymCompanyDC>();
            }
            return result.OperationReturnValue;
        }

        public string GetClaimImportStatus(string user, string gymCode, int hit, int status, DateTime date, ImportStatusDC advancedData)
        {
            OperationResult<string> result = GMSCcx.GetClaimStatus(gymCode, hit, status, date, advancedData);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return US.GMS.Core.Utils.XMLUtils.SerializeDataContractObjectToXML(new List<ImportStatusDC>());
            }
            return result.OperationReturnValue;
        }

        public bool ReScheduleClaim(string user, string gymCode, int itemNo)
        {
            OperationResult<bool> result = GMSCcx.ReScheduleClaim(gymCode, itemNo);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            return result.OperationReturnValue;
        }

        public PaymentImportStatusDC GetPaymentImportStatus(string user, string gymCode, int hit)
        {
            return new PaymentImportStatusDC();
        }

        public string GetPaymentImportStatusXML(string user, string gymCode, int hit, DateTime regDate)
        {
            OperationResult<string> result = GMSCcx.GetPaymentImportStatusXML(gymCode, hit, regDate);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return US.GMS.Core.Utils.XMLUtils.SerializeDataContractObjectToXML(new PaymentImportStatusDC());
            }
            return result.OperationReturnValue;
        }

        public bool MoveToShopAccount(int statusId, bool isImportStatus, string user, string gymCode)
        {
            OperationResult<bool> result = GMSCcx.MoveToShopAccount(statusId, isImportStatus, gymCode);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            return result.OperationReturnValue;
        }
        
        public bool GetenerateATGFile(int branchID, int status, string user)
        {
            //string folderPath = string.Empty;
            //OperationResult<List<IUSPCreditor>> result = null;
            //result = ATG.GetATGCrediors(ATGRecordType.NotifyByCrediCare, branchID, ExceConnectionManager.GetGymCode(user));

            //if (result.ErrorOccured)
            //{
            //    foreach (NotificationMessage message in result.Notifications)
            //    {
            //        USLogError.WriteToFile(message.Message, new Exception(), user);
            //    }
            //    return false;
            //}
            //else
            //{
            //    if (result.OperationReturnValue.Count == 0)
            //        return false;

            //    OperationResult<IFileLog> logg = ATG.GenerateFile(result.OperationReturnValue, status, folderPath, ExceConnectionManager.GetGymCode(user));
            //    foreach (string message in logg.OperationReturnValue.NotificationMessages)
            //    {
            //        USLogError.WriteToFile(message, new Exception(), user);
            //    }

            //    return true;
            //}
            return false;
        }
        
        public List<EconomyFileLogData> GetEconomyFileLogData(DateTime fromDate, DateTime toDate, string source, string filterBy, string user)
        {
            var result = EconomyFileLog.GetEconomyFileLogData(fromDate, toDate, source, filterBy,
                                                              ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (var message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
            }
            return result.OperationReturnValue;
        }

        public List<EconomyFileLogData> GetEconomyFileLogDetailData(EconomyLogFileHelper economyLogFileHelper)
        {
            var user = economyLogFileHelper.User;
            economyLogFileHelper.GymCode = ExceConnectionManager.GetGymCode(economyLogFileHelper.User);
            var result = EconomyFileLog.GetEconomyFileLogDetailData(economyLogFileHelper);
            if (result.ErrorOccured)
            {
                foreach (var message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
            }
            return result.OperationReturnValue;
        }

        public int GetCcxEnabledStatus(int branchId, string user)
        {
            var result = EconomyFileLog.GetCcxEnabledStatus(branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (var message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
            }
            return result.OperationReturnValue;
        }
        
        public List<InvoiceDC> GetInvoiceList(string category, DateTime fromDate, DateTime toDate, int branchId, string user)
        {
            OperationResult<List<InvoiceDC>> result = Invoice.GetInvoiceList(category, fromDate, toDate, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (var message in result.Notifications)
                {
                    USLogger.Log(message.Message, new List<string> { "InvoicePrint", "Service" }, SeverityFilter.Error, null);
                }
                return new List<InvoiceDC>();
            }
            return result.OperationReturnValue;
        }

        public string GenerateInvoiceList(List<int> arItemList, int branchId, string user)
        {
            var mergedPDFPath = string.Empty;
            var failCount = 0;
            try
            {
                var noOfDoc = arItemList.Count;
                var dataId = String.Join(",", arItemList);

                if (noOfDoc < 6)
                {
                    var historyId = UpdateHistoryPDFPrintProcess(-1, arItemList.Count, failCount, branchId, user, mergedPDFPath);
                    var gymCode = ExceConnectionManager.GetGymCode(user);
                    var template = ConfigurationManager.AppSettings["USC_Invoice_Print_App_Path"];

                    var bulkPDFPath = ConfigurationManager.AppSettings["BulkPDFFileFolder"] + string.Format(@"\{0}\{1}\INV_{2:yyyyMMdd_HHmmss}{3}", gymCode, branchId, DateTime.Now, ".pdf");
                    var pdfList = new List<string>();

                    foreach (var arItem in arItemList)
                    {
                        try
                        {
                            var dataDictionary = new Dictionary<string, string>() { { "itemID", arItem.ToString() }, { "gymCode", gymCode } };
                            var pdf = TemplateManager.ExecuteTemplate(dataDictionary, template, "PDF", gymCode).FirstOrDefault();
                            if (pdf != null)
                            {
                                var filePath = pdf.Messages.First(mssge => mssge.Header.ToLower().Equals("filepath")).Message;
                                if (string.IsNullOrEmpty(filePath))
                                    failCount++;
                                else
                                {
                                    UpdatePDFFileParth(arItem, filePath, branchId, user);
                                    pdfList.Add(filePath);
                                }
                            }
                            else
                                failCount++;
                        }
                        catch
                        {
                            failCount++;
                        }
                    }

                    mergedPDFPath = MergePDFFile(pdfList, bulkPDFPath);

                    UpdateHistoryPDFPrintProcess(historyId, noOfDoc, failCount, branchId, user, mergedPDFPath);
                }
                UpdateBulkPDFPrintDetails(-1, "INV", dataId, mergedPDFPath, noOfDoc, failCount, branchId, user);
                return mergedPDFPath;
            }
            catch (Exception ex)
            {
                USLogger.Log(ex.Message, new List<string> { "InvoicePrint", "Service" }, SeverityFilter.Error, null);
                return "ERROR";
            }
        }

        public List<ExceUserBranchDC> GetUserSelectedBranches(string user)
        {
            var result = ManageExcelineLogin.GetUserSelectedBranches(user, user);
            if (result.ErrorOccured)
            {
                foreach (var message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExceUserBranchDC>();
            }
            return result.OperationReturnValue;
        }

        public bool UpdatePDFFileParth(int id, string fileParth, int branchId, string user)
        {
            return Invoice.UpdatePDFFileParth(id, fileParth, branchId, ExceConnectionManager.GetGymCode(user), user);
        }

        private static string MergePDFFile(List<string> pdfPathList, string newPDFPath)
        {
            try
            {
                return Invoice.MergePDFFile(pdfPathList, newPDFPath);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int UpdateHistoryPDFPrintProcess(int id, int invoiceCount, int FailCount, int branchId, string user, string filePath)
        {
            try
            {
                return Invoice.UpdateHistoryPDFPrintProcess(id, invoiceCount, FailCount, branchId, user, ExceConnectionManager.GetGymCode(user), filePath);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int UpdateBulkPDFPrintDetails(int id, string docType, string dataIdList, string pdfFilePath, int noOfDoc, int failCount, int branchId, string user)
        {
            try
            {
                return Invoice.UpdateBulkPDFPrintDetails(id, docType, dataIdList, pdfFilePath, noOfDoc, failCount, branchId, user, ExceConnectionManager.GetGymCode(user));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<HistoryPDFPrint> GetHistoryPDFPrintDetail(DateTime fromDate, DateTime toDate, string user)
        {
            return Invoice.GetHistoryPDFPrintDetail(fromDate, toDate, ExceConnectionManager.GetGymCode(user));
        }

        public int GetGymCompanyId(string user)
        {
            OperationResult<int> result = GMSGymDetail.GetGymCompanyId(ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int GenarateClassSalary(List<int> branchIdList, string user, List<int> employeeIdList, List<int> categoryIdList, DateTime fromDate, DateTime toDate)
        {
            OperationResult<List<ClassDetail>> result = ClassApi.GetClassSalary(branchIdList, employeeIdList, categoryIdList, fromDate, toDate, ExceConnectionManager.GetGymCode(user));

            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                string text = string.Empty;
                if (result.OperationReturnValue.Any())
                {
                    foreach (var item in result.OperationReturnValue)
                    {
                        string timer = item.Timer.Replace(".", ",");
                        text = text + item.CompanyId + ';' + item.PayrollNumber + ';' + item.CategoryId + ';' + item.ArtNumber + ';' +
                               timer + ';' + String.Format("{0:dd.MM.yyyy}", item.StartDate) + Environment.NewLine;
                    }

                    OperationResult<int> gymId = GMSGymDetail.GetGymCompanyId(ExceConnectionManager.GetGymCode(user));
                    if (gymId.ErrorOccured)
                    {
                        foreach (NotificationMessage message in gymId.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), user);
                        }
                        return -1;
                    }

                    string fileName = fromDate.ToString("yyyyMMdd") + '-' + toDate.ToString("yyyyMMdd") + '-' + DateTime.Now.ToString("yyyyMMdd");
                    string filePath = ConfigurationSettings.AppSettings["ClassFileDownloadPath"] + gymId.OperationReturnValue.ToString() + '\\';
                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }

                    string file = GetClassFileName(filePath, fileName);
                    File.WriteAllText(file, text);



                    OperationResult<int> fileResult = ClassApi.SaveClassFileDetail(file.Split('\\').LastOrDefault(), filePath, user, ExceConnectionManager.GetGymCode(user));
                    if (fileResult.ErrorOccured)
                    {
                        foreach (NotificationMessage message in fileResult.Notifications)
                        {
                            USLogError.WriteToFile(message.Message, new Exception(), user);
                        }
                        return -1;
                    }
                    else
                    {
                        return fileResult.OperationReturnValue;
                    }
                }
                return -2;
            }
        }

        private string GetClassFileName(string filePath, string file)
        {

            string filename = string.Empty;
            string name = file;
            int i = 0;
            while (true)
            {
                filename = filePath + name + ".txt";
                if (File.Exists(filename))
                {
                    i++;
                    name = file + "_" + i;
                }
                else
                {
                    break;
                }
            }
            return filename;
        }

        public string GetClassSalaryFileDownloadUrl()
        {
            return ConfigurationSettings.AppSettings["ClassFileDownloadUrl"].ToString();
        }
        
        public List<ExcelineBranchDC> GetBranches(string user, int branchId)
        {
            OperationResult<List<ExcelineBranchDC>> result = GMSSystemSettings.GetBranches(user, ExceConnectionManager.GetGymCode(user), branchId);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExcelineBranchDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<GymEmployeeDC> GetEmployees(string user, int isActive)
        {
            OperationResult<List<GymEmployeeDC>> result = GMSMember.GetEmployee(isActive, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<GymEmployeeDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<CategoryDC> GetCategories(string type, string user)
        {
            OperationResult<List<CategoryDC>> result = GMSCategory.GetCategoriesByType(type, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<CategoryDC>(); ;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ClassFileDetail> GetClassFileDetails(string user)
        {
            OperationResult<List<ClassFileDetail>> result = ClassApi.GetClassFileDetails(ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ClassFileDetail>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public ComboBoxMultipleItem getItem()
        {
            return new ComboBoxMultipleItem();
        }

        public List<BulkPDFPrint> GetBulkPDFPrintDetailList(string type, DateTime fromDate, DateTime toDate, int branchId, string user)
        {
            try
            {
                return Invoice.GetBulkPDFPrintDetailList(type, fromDate, toDate, branchId, ExceConnectionManager.GetGymCode(user));
            }
            catch (Exception ex)
            {
                USLogger.Log(ex.Message, new List<string> { "InvoicePrint", "Service" }, SeverityFilter.Error, null);
                return new List<BulkPDFPrint>();
            }
        }
    }
}
