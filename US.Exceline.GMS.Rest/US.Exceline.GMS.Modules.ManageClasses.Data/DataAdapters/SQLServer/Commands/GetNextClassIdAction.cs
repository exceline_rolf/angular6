﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageClasses;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.ManageClasses.Data.DataAdapters.SQLServer.Commands
{
    public class GetNextClassIdAction : USDBActionBase<int>
    {
        public GetNextClassIdAction()
        {
        }

        protected override int Body(DbConnection connection)
        {
            int _classId = -1;
            string StoredProcedureName = "USExceGMSManageClassesGetNextClassId";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                object obj = cmd.ExecuteScalar();
                _classId = Convert.ToInt32(obj);
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return _classId;
        }
    }
}
