﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageClasses;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using System.Collections.ObjectModel;

namespace US.Exceline.GMS.Modules.ManageClasses.Data.DataAdapters.SQLServer.Commands
{
    public class AddClassDetailsAction : USDBActionBase<int>
    {
        ExcelineClassDC _exceineClass = new ExcelineClassDC();
        int _classScheduleId = 0;
        int _categoryId = 0;
        ObservableCollection<ScheduleItemDC> _scheduleItemList = new ObservableCollection<ScheduleItemDC>();

        public AddClassDetailsAction(ExcelineClassDC excelineClass)
        {
            _exceineClass = excelineClass;            
            if (_exceineClass.Schedule != null)
            {
                _scheduleItemList = _exceineClass.Schedule.SheduleItemList;
            }
            _categoryId = _exceineClass.ClassCategory.Id;
        }

        protected override int Body(DbConnection connection)
        {

            try
            {
                DbTransaction dbtran = connection.BeginTransaction();
                SaveClassAction saveAction = new SaveClassAction(_exceineClass);
                _classScheduleId = saveAction.RunOnTransation(dbtran);

                if (_classScheduleId == -1)
                {
                    dbtran.Rollback();
                    return -1;
                }

                else
                {
                    if (_exceineClass.Schedule != null)
                    {
                        AddEntityScheduleItemAction addscheduleItemForClass = new AddEntityScheduleItemAction(_classScheduleId, _scheduleItemList);
                        addscheduleItemForClass.ParentTransaction = dbtran;
                        int schItemId = addscheduleItemForClass.RunOnTransaction(dbtran);
                        if (schItemId == -1)
                        {
                            dbtran.Rollback();
                            return -1;
                        }

                        if (_exceineClass.TrainerIdList != null)
                        {
                            foreach (int Id in _exceineClass.TrainerIdList)
                            {
                                AddEntityScheduleAction addEntityScheduleAction = new AddEntityScheduleAction(_classScheduleId, Id, "TRA", _exceineClass.Name, _categoryId, _exceineClass.BranchId);
                                addEntityScheduleAction.ParentTransaction = dbtran;
                                int schduleId = addEntityScheduleAction.RunOnTransaction(dbtran);
                                if (schduleId == -1)
                                {
                                    dbtran.Rollback();
                                    return -1;
                                }

                                else
                                {
                                    AddEntityScheduleItemAction addEntityScheduleItemAction = new AddEntityScheduleItemAction(schduleId, _scheduleItemList);
                                    addEntityScheduleItemAction.ParentTransaction = dbtran;
                                    int scheduleItemId = addEntityScheduleItemAction.RunOnTransaction(dbtran);
                                    if (scheduleItemId == -1)
                                    {
                                        dbtran.Rollback();
                                        return -1;
                                    }
                                }
                            }
                        }

                        if (_exceineClass.InstructorIdList != null)
                        {
                            foreach (int Id in _exceineClass.InstructorIdList)
                            {
                                AddEntityScheduleAction addEntityScheduleAction = new AddEntityScheduleAction(_classScheduleId, Id, "INS", _exceineClass.Name, _categoryId, _exceineClass.BranchId);
                                addEntityScheduleAction.ParentTransaction = dbtran;
                                int schduleId = addEntityScheduleAction.RunOnTransaction(dbtran);
                                if (schduleId == -1)
                                {
                                    dbtran.Rollback();
                                    return -1;
                                }

                                else
                                {
                                    AddEntityScheduleItemAction addEntityScheduleItemAction = new AddEntityScheduleItemAction(schduleId, _scheduleItemList);
                                    addEntityScheduleItemAction.ParentTransaction = dbtran;
                                    int scheduleItemId = addEntityScheduleItemAction.RunOnTransaction(dbtran);
                                    if (scheduleItemId == -1)
                                    {
                                        dbtran.Rollback();
                                        return -1;
                                    }
                                }
                            }
                        }

                        if (_exceineClass.MemberIdList != null)
                        {
                            foreach (int Id in _exceineClass.MemberIdList)
                            {
                                AddEntityScheduleAction addEntityScheduleAction = new AddEntityScheduleAction(_classScheduleId, Id, "MEM", _exceineClass.Name, _categoryId, _exceineClass.BranchId);
                                addEntityScheduleAction.ParentTransaction = dbtran;
                                int schduleId = addEntityScheduleAction.RunOnTransaction(dbtran);
                                if (schduleId == -1)
                                {
                                    dbtran.Rollback();
                                    return -1;
                                }

                                else
                                {
                                    AddEntityScheduleItemAction addEntityScheduleItemAction = new AddEntityScheduleItemAction(schduleId, _scheduleItemList);
                                    addEntityScheduleItemAction.ParentTransaction = dbtran;
                                    int scheduleItemId = addEntityScheduleItemAction.RunOnTransaction(dbtran);
                                    if (scheduleItemId == -1)
                                    {
                                        dbtran.Rollback();
                                        return -1;
                                    }
                                }
                            }
                        }

                        if (_exceineClass.ResourceIdList != null)
                        {
                            foreach (int Id in _exceineClass.ResourceIdList)
                            {
                                AddEntityScheduleAction addEntityScheduleAction = new AddEntityScheduleAction(_classScheduleId, Id, "RES", _exceineClass.Name, _categoryId, _exceineClass.BranchId);
                                addEntityScheduleAction.ParentTransaction = dbtran;
                                int schduleId = addEntityScheduleAction.RunOnTransaction(dbtran);
                                if (schduleId == -1)
                                {
                                    dbtran.Rollback();
                                    return -1;
                                }

                                else
                                {
                                    AddEntityScheduleItemAction addEntityScheduleItemAction = new AddEntityScheduleItemAction(schduleId, _scheduleItemList);
                                    addEntityScheduleItemAction.ParentTransaction = dbtran;
                                    int scheduleItemId = addEntityScheduleItemAction.RunOnTransaction(dbtran);
                                    if (scheduleItemId == -1)
                                    {
                                        dbtran.Rollback();
                                        return -1;
                                    }
                                }
                            }
                        }
                    }
                }


                dbtran.Commit();
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return _classScheduleId;
        }
    }
}
