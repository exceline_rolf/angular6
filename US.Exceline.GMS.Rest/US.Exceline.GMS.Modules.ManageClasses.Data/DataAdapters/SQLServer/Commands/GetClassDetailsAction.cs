﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageClasses;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.Common;

namespace US.Exceline.GMS.Modules.ManageClasses.Data.DataAdapters.SQLServer.Commands
{
   public class GetClassDetailsAction : USDBActionBase<List<ExcelineClassDC>>
    {
       string _className = string.Empty;
       int _branchId = 0;

       public GetClassDetailsAction(string className, int branchId)
       {
           _className = className;
           _branchId = branchId;
       }

       protected override List<ExcelineClassDC> Body(DbConnection connection)
       {
           List<ExcelineClassDC> _excelineClassLst = new List<ExcelineClassDC>();
           string StoredProcedureName = "USExceGMSManageClassesGetClasses";

           try
           {
               DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
               cmd.Parameters.Add(DataAcessUtils.CreateParam("@classname", DbType.String, _className));
               cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
               DbDataReader reader = cmd.ExecuteReader();

               while (reader.Read())
               {
                   ExcelineClassDC excelineClass = new ExcelineClassDC();
                   excelineClass.ClassCategory = new CategoryDC();
                   excelineClass.Id = Convert.ToInt32(reader["ClassId"]);
                   excelineClass.Name = reader["ClassName"].ToString();
                   excelineClass.Description = reader["Description"].ToString();
                   excelineClass.BranchId = Convert.ToInt32(reader["BranchId"]);
                   excelineClass.ClassCategory.Id = Convert.ToInt32(reader["CatogeryId"]);
                   excelineClass.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
                   excelineClass.CreatedUser = reader["CreatedUser"].ToString();
                   excelineClass.ModifiedDate = Convert.ToDateTime(reader["ModifiedDate"]);
                   excelineClass.ModifiedUser = reader["ModifiedUser"].ToString();
                   excelineClass.MaxNumberOfMembers = Convert.ToInt32(reader["MaxNumberOfMembers"]);
                   excelineClass.ActiveStatus = Convert.ToBoolean(reader["ActiveStatus"]);
                   excelineClass.TotalClassFee = Convert.ToDecimal(reader["TotalClassFee"]);
                   excelineClass.ActiveTimeFee = Convert.ToDecimal(reader["ActiveTimeFee"]);
                   excelineClass.IsPayable = Convert.ToBoolean(reader["IsPayable"]);
                   excelineClass.ArticleId = Convert.ToInt32(reader["ArticleId"]);


                   excelineClass.ClassCategory.Code = reader["CatCode"].ToString();
                  
                   _excelineClassLst.Add(excelineClass);
               }              
           }

           catch(Exception ex)
           {
               throw ex;
           }

           return _excelineClassLst;
       }
    }
}
