﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using System.Data.Common;
using System.Data;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageClasses.Data.DataAdapters.SQLServer.Commands
{
    public class GetEntitySchedulesForClassUpdateAction : US_DataAccess.USDBActionBase<List<int>>
    {
        private List<int> _entitySchedulesIDs = new List<int>();
        private int _classScheduleId;

        public GetEntitySchedulesForClassUpdateAction(int classScheduleId)
        {
            this._classScheduleId = classScheduleId;
        }

        protected override List<int> Body(DbConnection connection)
        {
            string _storedProcedureName = "USExceGMSManageClassesInactivateEntitySchedule";

            try
            {
                DbCommand comd = CreateCommand(CommandType.StoredProcedure, _storedProcedureName);
                comd.Parameters.Add(DataAcessUtils.CreateParam("@classScheduleId", DbType.Int32, _classScheduleId));
                DbDataReader reader = comd.ExecuteReader();

                while (reader.Read())
                {
                    int scheduleId = Convert.ToInt32(reader["EntityScheduleId"]);
                    _entitySchedulesIDs.Add(scheduleId);
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return _entitySchedulesIDs;
        }

        public List<int> RunOnTransaction(DbTransaction Transaction)
        {
            string _storedProcedureName = "USExceGMSManageClassesInactivateEntitySchedule";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, _storedProcedureName);
                cmd.Connection = Transaction.Connection;
                cmd.Transaction = Transaction;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@classScheduleId", DbType.Int32, _classScheduleId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    int scheduleId = Convert.ToInt32(reader["EntityScheduleId"]);
                    _entitySchedulesIDs.Add(scheduleId);
                }
                reader.Close();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return _entitySchedulesIDs;
        }
    }
}
