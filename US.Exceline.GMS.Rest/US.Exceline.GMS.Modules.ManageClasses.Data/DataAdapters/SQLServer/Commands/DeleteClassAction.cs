﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.ManageClasses.Data.DataAdapters.SQLServer.Commands
{
    public class DeleteClassAction : USDBActionBase<bool>
    {
        private int _classId;       

        public DeleteClassAction(int classId)
        {
            // TODO: Complete member initialization
            _classId = classId;
        }

        protected override bool Body(DbConnection connection)
        {
            bool _isDeleted = false;
            int _id = 0;
            string StoredProcedureName = "USExceGMSManageClassesDeleteClass";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@classId", DbType.Int32, _classId)); 
                object obj = cmd.ExecuteScalar();
                _id = Convert.ToInt32(obj);
                if (_id > 0)
                    _isDeleted = true;
                else
                    _isDeleted = false;
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return _isDeleted;
        }
    }
}
