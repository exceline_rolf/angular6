﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageClasses;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using System.Collections.ObjectModel;

namespace US.Exceline.GMS.Modules.ManageClasses.Data.DataAdapters.SQLServer.Commands
{
    public class SaveClassAction : USDBActionBase<int>
    {
        private ExcelineClassDC _excelineClass;  

        public SaveClassAction(ExcelineClassDC excelineClass)
        {
            _excelineClass = excelineClass;
            _excelineClass.BranchId = 1;
        }
      

        protected override int Body(DbConnection connection)
        {
            int _classScheduleId = 0;
            string StoredProcedureName = "USExceGMSManageClassesAddClass";
            DbTransaction transaction = null;
            try
            {                
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Transaction = transaction;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@classId", DbType.Int32, _excelineClass.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Name", DbType.String, _excelineClass.Name));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _excelineClass.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CategoryId", DbType.Int32, _excelineClass.ClassCategory.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MaxNumberOfMembers", DbType.Int32, _excelineClass.MaxNumberOfMembers));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreatedUser", DbType.String, _excelineClass.CreatedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@description", DbType.String, _excelineClass.Description));                
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ModifiedUser", DbType.String, _excelineClass.ModifiedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsPayable", DbType.Boolean, _excelineClass.IsPayable));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AricleId", DbType.Int32, _excelineClass.ArticleId)); 
                if (_excelineClass.Schedule != null)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartDate", DbType.DateTime, _excelineClass.Schedule.StartDate));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndDate", DbType.DateTime, _excelineClass.Schedule.EndDate));
                }

                else
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartDate", DbType.DateTime, DBNull.Value));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndDate", DbType.DateTime, DBNull.Value));
                }
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Mode", DbType.String, _excelineClass.Mode));
                if (_excelineClass.Schedule == null)
                {
                    cmd.ExecuteNonQuery();
                    _classScheduleId = 1;
                }
                else
                {
                    object obj = cmd.ExecuteScalar();
                    if (obj != null)
                        _classScheduleId = Convert.ToInt32(obj);
                }
            }  

            catch (Exception ex)
            {
                throw ex;
            }
            return _classScheduleId;
        }

        public int RunOnTransation(DbTransaction dbTransaction)
        {
            int _classScheduleId = 0;
            string StoredProcedureName = "USExceGMSManageClassesAddClass";         
            try
            {  
              
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Connection = dbTransaction.Connection;
                cmd.Transaction = dbTransaction;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@classId", DbType.Int32, _excelineClass.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Name", DbType.String, _excelineClass.Name));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _excelineClass.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CategoryId", DbType.Int32, _excelineClass.ClassCategory.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MaxNumberOfMembers", DbType.Int32, _excelineClass.MaxNumberOfMembers));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreatedUser", DbType.String, _excelineClass.CreatedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@description", DbType.String, _excelineClass.Description));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ModifiedUser", DbType.String, _excelineClass.ModifiedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsPayable", DbType.Boolean, _excelineClass.IsPayable));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ArticleId", DbType.Int32, _excelineClass.ArticleId)); 
                if (_excelineClass.Schedule != null)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartDate", DbType.DateTime, _excelineClass.Schedule.StartDate));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndDate", DbType.DateTime, _excelineClass.Schedule.EndDate));
                }

                else
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartDate", DbType.DateTime, DBNull.Value));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndDate", DbType.DateTime, DBNull.Value));
                }
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Mode", DbType.String, _excelineClass.Mode));
                if (_excelineClass.Schedule == null)
                {
                    cmd.ExecuteNonQuery();
                    _classScheduleId = 1;
                }
                else
                {
                    object obj = cmd.ExecuteScalar();
                    if (obj != null)
                        _classScheduleId = Convert.ToInt32(obj);
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return _classScheduleId;
        }
    }
}
