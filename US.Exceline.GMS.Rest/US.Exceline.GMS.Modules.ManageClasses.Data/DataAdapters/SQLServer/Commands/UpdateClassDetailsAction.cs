﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageClasses;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using System.Collections.ObjectModel;

namespace US.Exceline.GMS.Modules.ManageClasses.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateClassDetailsAction : USDBActionBase<int>
    {
        ExcelineClassDC _exceineClass = new ExcelineClassDC();
        int _scheduleId;
        int _parentId;
        int _categoryId;
        string _name;
        List<ScheduleDC> entityScheduleList = new List<ScheduleDC>();
        private List<int> _entityScheduleIds = new List<int>();
        private bool _isInactivated = false;

        public UpdateClassDetailsAction(ExcelineClassDC excelineClass)
        {
            _exceineClass = excelineClass;
            _parentId = _exceineClass.Schedule.Id;
            _categoryId = _exceineClass.ClassCategory.Id;
            _name = _exceineClass.Name;

        }

        protected override int Body(DbConnection connection)
        {
            try
            {
                DbTransaction dbtran = connection.BeginTransaction();
                UpdateClassAction updateClassAction = new UpdateClassAction(_exceineClass);
                _scheduleId = updateClassAction.RunOnTransaction(dbtran);

                if (_scheduleId == -1)
                {
                    dbtran.Rollback();
                    return -1;
                }

                else
                {
                    if (_exceineClass.MemberIdList.Count != 0 || _exceineClass.ResourceIdList.Count != 0 || _exceineClass.TrainerIdList.Count != 0 || _exceineClass.InstructorIdList.Count != 0)
                    {
                        GetEntitySchedulesForClassUpdateAction getEntityScheduleIds = new GetEntitySchedulesForClassUpdateAction(_parentId);
                        getEntityScheduleIds.ParentTransaction = dbtran;
                        _entityScheduleIds = getEntityScheduleIds.RunOnTransaction(dbtran);
                        if (_exceineClass.Schedule.Id != -1)
                        {
                            if ((_entityScheduleIds.Count == 0 && _exceineClass.MemberIdList.Count == 0) && (_entityScheduleIds.Count == 0 && _exceineClass.InstructorIdList.Count == 0) && (_entityScheduleIds.Count == 0 && _exceineClass.TrainerIdList.Count == 0) && (_entityScheduleIds.Count == 0 && _exceineClass.ResourceIdList.Count == 0))
                            {
                                dbtran.Rollback();
                                return -1;
                            }
                        }
                    }
                    else
                    {
                        if (_exceineClass.MemberIdList.Count != 0 || _exceineClass.ResourceIdList.Count != 0 || _exceineClass.TrainerIdList.Count != 0 || _exceineClass.InstructorIdList.Count != 0)
                        {
                            foreach (int _entitySchId in _entityScheduleIds)
                            {
                                InactivateEntityScheduleItemAction inactivateEntityScheduleItem = new InactivateEntityScheduleItemAction(_entitySchId);
                                inactivateEntityScheduleItem.ParentTransaction = dbtran;
                                _isInactivated = inactivateEntityScheduleItem.RunOnTransaction(dbtran);
                                if (!_isInactivated)
                                {
                                    dbtran.Rollback();
                                    return -1;
                                }
                            }
                        }
                    }


                    if (_exceineClass.Schedule.Id == -1 || _exceineClass.Schedule.Id == 0)
                    {
                        _exceineClass.Schedule.Id = _scheduleId;
                        if (_parentId == 0 && _scheduleId != 0)
                            _parentId = _scheduleId;
                    }
                    if (_exceineClass.Schedule.SheduleItemList.Count > 0)
                    {
                        UpdateEntityScheduleItemsAction updateClassScheduleItems =
                            new UpdateEntityScheduleItemsAction(_exceineClass.Schedule.SheduleItemList, _exceineClass.Id,
                                                                "CLS", _exceineClass.Schedule.Id);
                        updateClassScheduleItems.ParentTransaction = dbtran;
                        int id = updateClassScheduleItems.RunOnTransaction(dbtran);
                        if (id == -1)
                        {
                            dbtran.Rollback();
                            return -1;
                        }
                    }

                    if (_exceineClass.MemberIdList.Count > 0)
                    {
                        foreach (int _memberId in _exceineClass.MemberIdList)
                        {
                            UpdateEntityScheduleAction updateEntityScheduleAction = new UpdateEntityScheduleAction(_parentId, "MEM", _categoryId, _name, _memberId, _exceineClass.BranchId);
                            updateEntityScheduleAction.ParentTransaction = dbtran;
                            _scheduleId = updateEntityScheduleAction.RunOnTransaction(dbtran);
                            if (_scheduleId == -1)
                            {
                                dbtran.Rollback();
                                return -1;
                            }
                            else
                            {
                                UpdateEntityScheduleItemsAction updateEntityScheduleItemAction = new UpdateEntityScheduleItemsAction(_exceineClass.Schedule.SheduleItemList, _memberId, "MEM", _scheduleId);
                                updateEntityScheduleItemAction.ParentTransaction = dbtran;
                                int _scheduleItemId = updateEntityScheduleItemAction.RunOnTransaction(dbtran);
                                if (_scheduleItemId == -1)
                                {
                                    dbtran.Rollback();
                                    return -1;
                                }
                            }

                        }
                    }

                    if (_exceineClass.TrainerIdList.Count > 0)
                    {
                        foreach (int _trainerId in _exceineClass.TrainerIdList)
                        {
                            UpdateEntityScheduleAction updateEntitySchedulAction = new UpdateEntityScheduleAction(_parentId, "TRA", _categoryId, _name, _trainerId, _exceineClass.BranchId);
                            updateEntitySchedulAction.ParentTransaction = dbtran;
                            _scheduleId = updateEntitySchedulAction.RunOnTransaction(dbtran);
                            if (_scheduleId == -1)
                            {
                                dbtran.Rollback();
                                return -1;
                            }
                            else
                            {
                                UpdateEntityScheduleItemsAction updateEntityScheduleItemAction = new UpdateEntityScheduleItemsAction(_exceineClass.Schedule.SheduleItemList, _trainerId, "TRA", _scheduleId);
                                updateEntityScheduleItemAction.ParentTransaction = dbtran;
                                int _scheduleItemId = updateEntityScheduleItemAction.RunOnTransaction(dbtran);
                                if (_scheduleItemId == -1)
                                {
                                    dbtran.Rollback();
                                    return -1;
                                }
                            }
                        }
                    }

                    if (_exceineClass.InstructorIdList.Count > 0)
                    {
                        foreach (int _instructorId in _exceineClass.InstructorIdList)
                        {
                            UpdateEntityScheduleAction updateEntityScheduleAction = new UpdateEntityScheduleAction(_parentId, "INS", _categoryId, _name, _instructorId, _exceineClass.BranchId);
                            updateEntityScheduleAction.ParentTransaction = dbtran;
                            _scheduleId = updateEntityScheduleAction.RunOnTransaction(dbtran);
                            if (_scheduleId == -1)
                            {
                                dbtran.Rollback();
                                return -1;
                            }
                            else
                            {
                                UpdateEntityScheduleItemsAction updateEntityScheduleItemAction = new UpdateEntityScheduleItemsAction(_exceineClass.Schedule.SheduleItemList, _instructorId, "INS", _scheduleId);
                                updateEntityScheduleItemAction.ParentTransaction = dbtran;
                                int _scheduleItemId = updateEntityScheduleItemAction.RunOnTransaction(dbtran);
                                if (_scheduleItemId == -1)
                                {
                                    dbtran.Rollback();
                                    return -1;
                                }
                            }
                        }
                    }

                    if (_exceineClass.ResourceIdList.Count > 0)
                    {
                        foreach (int _resourceId in _exceineClass.ResourceIdList)
                        {
                            UpdateEntityScheduleAction updateEntityScheduleAction = new UpdateEntityScheduleAction(_parentId, "RES", _categoryId, _name, _resourceId, _exceineClass.BranchId);
                            updateEntityScheduleAction.ParentTransaction = dbtran;
                            _scheduleId = updateEntityScheduleAction.RunOnTransaction(dbtran);
                            if (_scheduleId == -1)
                            {
                                dbtran.Rollback();
                                return -1;
                            }
                            else
                            {
                                UpdateEntityScheduleItemsAction updateEntityScheduleItemAction = new UpdateEntityScheduleItemsAction(_exceineClass.Schedule.SheduleItemList, _resourceId, "RES", _scheduleId);
                                updateEntityScheduleItemAction.ParentTransaction = dbtran;
                                int _scheduleItemId = updateEntityScheduleItemAction.RunOnTransaction(dbtran);
                                if (_scheduleItemId == -1)
                                {
                                    dbtran.Rollback();
                                    return -1;
                                }
                            }
                        }
                    }
                }



                dbtran.Commit();
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return _scheduleId;
        }

    }
}
