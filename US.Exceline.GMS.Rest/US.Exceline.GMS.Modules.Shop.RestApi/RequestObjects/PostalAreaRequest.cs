﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Exceline.GMS.Modules.Shop.RestApi.RequestObjects
{
    public class PostalAreaRequest
    {
        private string _postalCode;

        public string PostalCode
        {
            get { return _postalCode; }
            set { _postalCode = value; }
        }

        private string _postalArea;

        public string PostalArea
        {
            get { return _postalArea; }
            set { _postalArea = value; }
        }

        private long _population;

        public long Population
        {
            get { return _population; }
            set { _population = value; }
        }

        private long _houseHold;

        public long HouseHold
        {
            get { return _houseHold; }
            set { _houseHold = value; }
        }

        
    }
}