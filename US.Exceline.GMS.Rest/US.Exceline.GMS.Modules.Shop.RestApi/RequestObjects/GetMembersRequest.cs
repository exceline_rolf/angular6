﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.Shop.RestApi.RequestObjects
{
    public class GetMembersRequest
    {
        private int _branchId;

        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private string _searchText;

        public string SearchText
        {
            get { return _searchText; }
            set { _searchText = value; }
        }


        private int _status;

        public int Status
        {
            get { return _status; }
            set { _status = value; }
        }

        private MemberSearchType _searchType;

        public MemberSearchType SearchType
        {
            get { return _searchType; }
            set { _searchType = value; }
        }

        private MemberRole _memRole;

        public MemberRole MemRole
        {
            get { return _memRole; }
            set { _memRole = value; }
        }

        private int _hit;

        public int Hit
        {
            get { return _hit; }
            set { _hit = value; }
        }


    }
}