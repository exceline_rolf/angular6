﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using US.Common.Logging.API;
using US.Communication.API;
using US.Communication.Core.SystemCore.DomainObjects;
using US.Exceline.GMS.Modules.Shop.API;
using US.Exceline.GMS.Modules.Shop.API.InventoryManagement;
using US.Exceline.GMS.Modules.Shop.API.ManageShop;
using US.GMS.API;
using US.GMS.API.ManageSystemSettings;
using US.GMS.Core.DomainObjects.ManageShop;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.Utils;
namespace US.Exceline.GMS.Modules.Shop.RestApi.Controllers
{
    [RoutePrefix("api/Sales")]
    public class SalesController : ApiController
    {

        #region Sales

        [HttpGet]
        [Route("GetDailySales")]
        [Authorize]
        public HttpResponseMessage GetDailySales( DateTime salesDate, int branchId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSShopManager.GetDailySales(salesDate, branchId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<ShopSalesItemDC>(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ShopSalesItemDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpGet]
        [Route("GetSalesPaymentList")]
        [Authorize]
        public HttpResponseMessage GetSalesPaymentList()
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = new ShopSalesPaymentDC();
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new ShopSalesPaymentDC(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        #endregion

        [HttpGet]
        [Route("GetMemberEconomyBalances")]
        [Authorize]
        public HttpResponseMessage GetMemberEconomyBalances(int memberId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSShopManager.GetMemberEconomyBalances(ExceConnectionManager.GetGymCode(user), memberId);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new Dictionary<string, decimal>(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new Dictionary<string, decimal>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        #region Bill

        [HttpGet]
        [Route("GetShopBillSummary")]
        [Authorize]
        public HttpResponseMessage GetShopBillSummary(DateTime fromDate, DateTime toDate, int branchId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSShopManager.GetShopBillSummary(fromDate, toDate, branchId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<ShopSalesPaymentDC>(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ShopSalesPaymentDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpGet]
        [Route("GetShopSaleBillDetail")]
        [Authorize]
        public HttpResponseMessage GetShopSaleBillDetail(int saleId, int branchId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSShopManager.GetShopSaleBillDetail(saleId, branchId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new ShopBillDetailDC(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new ShopBillDetailDC(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


        #endregion

        #region SalesPoint
        [HttpGet]
        [Route("GetSalesPoint")]
        [Authorize]
        public HttpResponseMessage GetSalesPointList(int branchId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSShopManager.GetSalesPointList(branchId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<SalePointDC>(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<SalePointDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpGet]
        [Route("GetSalesPointByMachineName")]
        [Authorize]
        public HttpResponseMessage GetSalesPointByMachineName(int branchID, string machineName)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSShopManager.GetSalesPointByMachineName(branchID, machineName, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new SalePointDC(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new SalePointDC(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpPost]
        [Route("SavePointOfSale")]
        [Authorize]
        public HttpResponseMessage SavePointOfSale(SalePointDC pointOfSale)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSShopManager.SavePointOfSale(pointOfSale.BranchID, pointOfSale, user, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }
        #endregion


    }
}
