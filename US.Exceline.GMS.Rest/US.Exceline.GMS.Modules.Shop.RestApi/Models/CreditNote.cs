﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.Shop.RestApi.Models
{
    public class CreditNote
    {
        public int AritemNo { get; set; }
        public int MemberId { get; set; }
        public int ShopAccountId { get; set; }
        public int SalePointId { get; set; }
        public int LoggedBranchId { get; set; }
        public List<PayModeDC> PaymentModes { get; set; }
    }
}