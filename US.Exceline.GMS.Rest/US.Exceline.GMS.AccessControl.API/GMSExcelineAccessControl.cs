﻿using System.Collections.Generic;
using US.Exceline.GMS.AccessControl.BusinessLogic;
using US.GMS.Core.DomainObjects.AccessControl;
using US.GMS.Core.ResultNotifications;

namespace US.Exceline.GMS.AccessControl.API
{
    public class GMSExcelineAccessControl
    {
        public static OperationResult<ExceAccessControlAuthenticationDC> CheckAuthentication(int branchId, string cardNo, string gymCode, ExceAccessControlTerminalDetailDC terminalDetail, int terminalId, string user)
        {
            return ExcelineAccessControlManager.CheckAuthentication(branchId, cardNo, gymCode, terminalDetail, terminalId, user);
        }

        public static OperationResult<ExceAccessControlAuthenticationDC> CheckAuthenticationForGatPurchase(string cardNo, int branchId, int terminalId, string gymCode,string duration, string price)
        {
            return ExcelineAccessControlManager.CheckAuthenticationForGatPurchase(cardNo, branchId, terminalId, gymCode, duration,price);
        }

        public static OperationResult<List<ExceAccessControlTerminalDetailDC>> GetTerminalDetails(int branchId, string gymCode)
        {
            return ExcelineAccessControlManager.GetTerminalDetails(branchId, gymCode);
        }

        public static OperationResult<bool> UpdateTerminalStatus(string gymCode, ExceAccessControlUpdateTerminalStatusDC terminalStatusDetail)
        {
            return ExcelineAccessControlManager.UpdateTerminalStatus(gymCode, terminalStatusDetail);
        }

        public static OperationResult<bool> RegisterPurchase(string gymCode, ExceAccessControlRegisterPurchaseDC purchaseDetail)
        {
            return ExcelineAccessControlManager.RegisterPurchase(gymCode, purchaseDetail);
        }

        public static OperationResult<bool> RegisterSunBedPurchase(string gymCode, ExceAccessControlRegisterPurchaseDC purchaseDetail)
        {
            return ExcelineAccessControlManager.RegisterSunBedPurchase(gymCode, purchaseDetail);
        }

        public static OperationResult<bool> RegisterVendingEvent(string gymCode, ExceAccessControlVendingEventDC vendingEventDetail)
        {
            return ExcelineAccessControlManager.RegisterVendingEvent(gymCode, vendingEventDetail);
        }

        public static OperationResult<string> GetGymCodeByCompanyId(string companyId)
        {
            return ExcelineAccessControlManager.GetGymCodeByCompanyId(companyId);
        }

        public static OperationResult<ExceAccessControlTerminalDetailDC> GetTypeIdByTerminalId(int terminalId, string gymCode)
        {
            return ExcelineAccessControlManager.GetTypeIdByTerminalId(terminalId, gymCode);
        }

        public static OperationResult<bool> RegisterMemberVisit(string gymCode, ExceAccessControlRegisterMemberVisitDC memberVisit, string user)
        {
            return ExcelineAccessControlManager.RegisterMemberVisit(gymCode, memberVisit,user);
        }        
    }
}
