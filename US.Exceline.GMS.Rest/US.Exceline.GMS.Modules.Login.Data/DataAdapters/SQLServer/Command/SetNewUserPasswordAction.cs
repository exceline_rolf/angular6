﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Login;
using System.Data;
using System.Data.Common;

namespace US.Exceline.GMS.Modules.Login.Data.DataAdapters.SQLServer.Command
{
    public class SetNewUserPasswordAction : USDBActionBase<bool>
    {
        private String _UserName = String.Empty;
        private String _base = String.Empty;
        private String _password = String.Empty;
        public SetNewUserPasswordAction(ExcePWSetRecDC userData)
        {
            _UserName = userData.UserName;
            _base = userData.Base;
            _password = userData.Password;
        }


        protected override bool Body(DbConnection connection)
        {
            bool isSuccess = false;
            try
            {
                const string storedProcedure = "dbo.USExceGMSManageLoginSetNewUserPassword";
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@UserName", DbType.String, _UserName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@base", DbType.String, _base));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@password", DbType.String, _password));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    if (Convert.ToInt32(reader["returnCode"]) == 0)
                    {
                        isSuccess = true;
                    }
                    else
                    {
                        throw new ArgumentNullException();
                    }
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return isSuccess;
        }
    }


}
