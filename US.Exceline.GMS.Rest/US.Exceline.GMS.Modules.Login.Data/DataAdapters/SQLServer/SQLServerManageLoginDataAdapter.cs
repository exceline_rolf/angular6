﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Login.Data.DataAdapters.SQLServer.Command;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Login;

namespace US.Exceline.GMS.Modules.Login.Data.DataAdapters.SQLServer
{
    class SQLServerManageLoginDataAdapter : IManageLoginDataAdapter
    {
        public List<US.GMS.Core.DomainObjects.Admin.ExceUserBranchDC> GetUserSelectedBranches(string userName, string user)
        {
            try
            {
                GetUserSelectedBranchAction action = new GetUserSelectedBranchAction(userName, user);
                return action.Execute(EnumDatabase.USP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SetNewUserPassword(ExcePWSetRecDC userData)
        {
            try
            {
                var action = new SetNewUserPasswordAction(userData);
                return action.Execute(EnumDatabase.Exceline, userData.GymCode);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public ExcePWRecDC AnonymousUserPasswordReset(string gymCode, string potentialUserName)
        {
            try {            
                var action = new AnonymousUserPasswordResetAction(gymCode, potentialUserName);
                return action.Execute(EnumDatabase.Exceline, gymCode);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public List<ExecUserDC> GetLoggedUserDetails(string userName)
        {
            try
            {
                var action = new GetLoggedUserDetailsAction(userName);
                var userDetails = action.Execute(EnumDatabase.USP);
                return userDetails;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AddEditUserHardwareProfile(int userId, int hardwareProfileId,string user)
        {
            try
            {
                AddEditUserHardwareProfileAction action = new AddEditUserHardwareProfileAction(userId, hardwareProfileId,user);
                return action.Execute(EnumDatabase.USP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

      

    }
}
