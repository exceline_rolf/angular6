﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin;
using US.GMS.Core.DomainObjects.Login;

namespace US.Exceline.GMS.Modules.Login.Data
{
    interface IManageLoginDataAdapter
    {
        List<ExceUserBranchDC> GetUserSelectedBranches(string userName, string user);
        List<ExecUserDC> GetLoggedUserDetails(string userName);
        bool AddEditUserHardwareProfile(int userId, int hardwareProfileId, string user);
        ExcePWRecDC AnonymousUserPasswordReset(string gymCode, string potentialUserName);
        bool SetNewUserPassword(ExcePWSetRecDC userData);
    }
}
