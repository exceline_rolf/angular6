﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using US.GMS.Core.DomainObjects.Notification;
using US.GMS.Core.DomainObjects.Common;


namespace US.Exceline.GMS.Modules.Notifications.Service
{
    [ServiceContract]
    public interface INotificationsService
    {
        [OperationContract]
        string Test();
        [OperationContract]
        int SaveNotification(NotificationDC notification, string createdUser, int branchId);
        [OperationContract]
        List<NotificationMethodDC> GetNotifications(int branchId, string user );
        [OperationContract]
        List<CategoryDC> GetCategories(string type, string user, int branchId );
        [OperationContract]
        int UpdateIsNotified(int notificationMethodId, string user, int branchId );
        [OperationContract]
        bool DeleteNotificationMethod(int notificationMethodId, string user, int branchId );
    }
}
