﻿using System;
using System.Collections.Generic;
using US.Exceline.GMS.Modules.Economy.BusinessLogic;
using US.Exceline.GMS.Modules.Economy.Core.DomainObjects;
using US.GMS.Core.ResultNotifications;

namespace US.Exceline.GMS.Modules.Economy.API
{
    public class EconomyFileLog
    {
        public static OperationResult<List<EconomyFileLogData>> GetEconomyFileLogData(DateTime fromDate, DateTime toDate,
                                                                                      string source, string filterBy,
                                                                                      string gymCode)
        {
            return EconomyFileLogManager.GetEconomyFileLogData(fromDate, toDate, source, filterBy, gymCode);
        }

        public static OperationResult<List<EconomyFileLogData>> GetEconomyFileLogDetailData(EconomyLogFileHelper economyLogFileHelper)
        {
            return EconomyFileLogManager.GetEconomyFileLogDetailData(economyLogFileHelper);
        }
        public static OperationResult<int> GetCcxEnabledStatus(int branchId, string gymCode)
        {
            return EconomyFileLogManager.GetCcxEnabledStatus(branchId, gymCode);
        }
    }
}
