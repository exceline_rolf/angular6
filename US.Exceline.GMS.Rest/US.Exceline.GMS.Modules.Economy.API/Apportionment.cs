﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.BusinessLogic;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums;
using US.GMS.Core.ResultNotifications;

namespace US.Exceline.GMS.Modules.Economy.API
{
    public class Apportionment
    {
        public static OperationResult<List<USPApportionmentTransaction>> GetInitialApportionmentData(string KID, string creditorNo, decimal amount, int ARNo, int caseNO, int paymentID, string paymentType, string gymCode)
        {
            return ApportionmentManager.GetInitialApportionmentData(KID, creditorNo, amount, ARNo, caseNO, paymentID, paymentType, gymCode);
        }

        public static OperationResult<ReverseConditions> ReverseApportionments(int paymentId, int arItem, string actionStatus, string user, string dbCode)
        {
            return ApportionmentManager.ReverseApportionments(paymentId, arItem, actionStatus, user, dbCode);
        }

        public static OperationResult<int> AddNotApportionmentData(int ARNo, string KID, int errorPayment, string creditoNo, decimal mainAmount, int itemTypeID, List<USPApportionmentTransaction> transactionList, string user, string dbCode)
        {
            return ApportionmentManager.AddNotApportionmentTransactions(ARNo, KID, errorPayment, creditoNo, mainAmount, itemTypeID, transactionList, user, dbCode);
        }
    }
}
