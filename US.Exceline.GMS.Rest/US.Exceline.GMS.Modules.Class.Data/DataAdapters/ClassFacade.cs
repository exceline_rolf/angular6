﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer.Commands;
using US.Exceline.GMS.Modules.Class.Data.SystemObjects;
using US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer;
using US.GMS.Core.DomainObjects.ManageClasses;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.Class.Data.DataAdapters
{
    public class ClassFacade
    {
        private static IClassDataAdapter GetDataAdapter()
        {
            return new SQLServerClassDataAdapter();
        }
        public static List<ExcelineClassDC> SearchClass(string category, string searchText, string gymCode)
        {
            return GetDataAdapter().SearchClass(category, searchText, gymCode);
        }

        public static List<ExcelineClassDC> GetClasses(string className, int branchId, string gymCode)
        {
            return GetDataAdapter().GetClasses(className, branchId, gymCode);
        }

        public static List<ScheduleItemDC> GetScheduleItemsByClass(int classId, int branchId, string gymCode)
        {
            return GetDataAdapter().GetScheduleItemsByClass(classId, branchId, gymCode);
        }

        public static List<ScheduleItemDC> GetScheduleItemsByClassIds(List<int> classIds, int branchId, string gymCode)
        {
            return GetDataAdapter().GetScheduleItemsByClassIds(classIds, branchId, gymCode);
        }

        public static List<ExcelineClassDC> SearchClass(string category, string searchText, DateTime startDate, string gymCode)
        {
            return GetDataAdapter().SearchClass(category, searchText, startDate, gymCode);
        }

        public static List<ExcelineClassDC> SearchClass(string category, string searchText, DateTime startDate, DateTime endDate, string gymCode)
        {
            return GetDataAdapter().SearchClass(category, searchText, startDate, endDate, gymCode);
        }

        public static string SaveClass(ExcelineClassDC excelineClass,int branchId, string gymCode,string user)
        {
            return GetDataAdapter().SaveClass(excelineClass, branchId, gymCode, user);
        }

        public static bool UpdateClass(ExcelineClassDC excelineClass, string gymCode)
        {
            return GetDataAdapter().UpdateClass(excelineClass, gymCode);
        }

        public static bool DeleteClass(int classId, string gymCode)
        {
            return GetDataAdapter().DeleteClass(classId, gymCode);
        }

        public static List<ExcelineClassActiveTimeDC> GetExcelineClassActiveTimes(int branchId, DateTime startDate, DateTime endDate, int entNo, string gymCode)
        {
            return GetDataAdapter().GetClassActiveTimes(branchId, startDate, endDate, entNo, gymCode);
        }

        public static int GetNextClassId(string gymCode)
        {
            return GetDataAdapter().GetNextClassId(gymCode);
        }

        public static List<InstructorDC> GetInstructorsForClass(int classId, string gymCode)
        {
            return GetDataAdapter().GetInstructorsForClass(classId, gymCode);
        }

        public static List<ResourceDC> GetresourcesForClass(int classId, string gymCode)
        {
            return GetDataAdapter().GetResourcesForClass(classId, gymCode);
        }

        public static List<ExcelineClassMemberDC> GetMembersByActiveTimeId(int branchId, int activeTimeId, string gymCode)
        {
            return GetDataAdapter().GetMembersByActiveTimeId(branchId,activeTimeId,gymCode);
        }

        public static ScheduleDC GetClassSchedule(int classId, string gymCode)
        {
            return GetDataAdapter().GetClassSchedule(classId, gymCode);
        }

      
        public static int GetExcelineClassIdByName(string className, string gymCode)
        {
            return GetDataAdapter().GetExcelineClassIdByName(className, gymCode);
        }

        public static int UpdateTimeTableScheduleItems(List<ScheduleItemDC> scheduleItemList,int branchId, string gymCode,string user)
        {
            return GetDataAdapter().UpdateTimeTableScheduleItems(scheduleItemList, branchId, gymCode, user);
        }

        public static string CheckActiveTimeOverlapWithClass(ScheduleItemDC scheduleItem, string gymCode)
        {
            return GetDataAdapter().CheckActiveTimeOverlapWithClass(scheduleItem, gymCode);
        }

        public static int SaveScheduleItem(ScheduleItemDC scheduleItem, int branchId, string gymCode,string user)
        {
            return GetDataAdapter().SaveScheduleItem(scheduleItem, branchId, gymCode, user);
        }

        public static int SaveActiveTimes(List<ScheduleItemDC> scheduleItemList, string gymCode)
        {
            return GetDataAdapter().SaveActiveTimes(scheduleItemList, gymCode);
        }

        public static int UpdateClassCalendarActiveTime(EntityActiveTimeDC activeTime, string user, string gymCode)
        {
            return GetDataAdapter().UpdateClassCalendarActiveTime(activeTime,user, gymCode);
        }

        public static int DeleteScheduleItem(List<int> scheduleItemIdList, string gymCode, string user)
        {
            return GetDataAdapter().DeleteScheduleItem(scheduleItemIdList, gymCode, user);
        }

        public static bool DeleteSchedule(ExcelineClassDC exceClass, string gymCode, string user) 
        {
            return GetDataAdapter().DeleteSchedule(exceClass, gymCode, user);
        }

        public static int UpdateSeasonEndDateWithClassActiveTimes(int branchId, int seasonId, DateTime endDate, string gymCode)
        {
            return GetDataAdapter().UpdateSeasonEndDateWithClassActiveTimes(branchId, seasonId, endDate, gymCode);
        }

        public static bool DeleteClassActiveTime(int activeTimeID, string gymCode, string user)
        {
            return GetDataAdapter().DeleteClassActiveTime(activeTimeID, gymCode, user);
        }

        public static int UpdateClassActiveTime(UpdateClassActiveTimeHelperDC helperObj, string gymCode, string user)
        {
            return GetDataAdapter().UpdateClassActiveTime(helperObj, gymCode, user);
        }

        public static List<EntityActiveTimeDC> GetActiveTimesForClassScheduleItem(int scheduleItemId, string gymCode)
        {
            return GetDataAdapter().GetActiveTimesForClassScheduleItemAction(scheduleItemId,gymCode);
        }

        public static ScheduleData GetSeasonAndClassInfo(int branchId, int scheduleId, String gymCode)
        {
            return GetDataAdapter().GetSeasonAndClassInfo(branchId, scheduleId, gymCode);
        }

        public static string GetListOrCalendarInitialViewByUser(string user, string gymCode)
        {
            try
            {
                return GetDataAdapter().GetListOrCalendarInitialViewByUser(user, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
