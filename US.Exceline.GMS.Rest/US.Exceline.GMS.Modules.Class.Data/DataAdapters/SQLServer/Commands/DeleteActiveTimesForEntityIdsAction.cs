﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer.Commands
{
    public class DeleteActiveTimesForEntityIdsAction : USDBActionBase<bool>
    {
        private int _activeTimeId = -1;    

        public DeleteActiveTimesForEntityIdsAction(int activeTimeID)
        {
            _activeTimeId = activeTimeID; 

        }
        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            return false;
        }

        public bool RunOnTransaction(DbTransaction transaction)
        {
            bool _isDeleted = false;
            string storedProcedure = "USEXCEGMSDeleteEntityActiveTimesForUpdate";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Connection = transaction.Connection;
                cmd.Transaction = transaction;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ClassActiveTimeId", DbType.Int32, _activeTimeId));
                cmd.ExecuteNonQuery();
                _isDeleted = true;                
            }
            catch (Exception ex)
            {
                throw ex;
            }

        
            return _isDeleted;
        }
    }
}
