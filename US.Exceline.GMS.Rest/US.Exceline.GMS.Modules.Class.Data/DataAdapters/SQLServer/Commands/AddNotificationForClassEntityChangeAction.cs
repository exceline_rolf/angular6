﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer.Commands
{
    public class AddNotificationForClassEntityChangeAction : USDBActionBase<bool>
    {
        private readonly int _activeTimeId;
        private  DataTable _addedInsIdList;
        private  DataTable _deletedInsIdList;
        private  DataTable _addedResIdList;
        private  DataTable _deletedResIdList;
        private readonly string _user = string.Empty;

       public AddNotificationForClassEntityChangeAction(UpdateClassActiveTimeHelperDC updateClassActiveTimeObj,string user)
       {
           _activeTimeId = updateClassActiveTimeObj.ActiveTimeId;
           _addedInsIdList = GetEntityList(updateClassActiveTimeObj.AddedINSIDList);
           _deletedInsIdList = GetEntityList(updateClassActiveTimeObj.DeletedINSIDList);
           _addedResIdList = GetEntityList(updateClassActiveTimeObj.AddedRESIDList);
           _deletedResIdList = GetEntityList(updateClassActiveTimeObj.DeletedRESIDList);
           _user = user;
       }

       private DataTable GetEntityList(List<int> entityList)
       {
           DataTable dataTable = new DataTable();
           dataTable.Columns.Add(new DataColumn("ID", Type.GetType("System.Int32")));
           if (entityList != null && entityList.Any())
               foreach (var item in entityList)
               {
                   DataRow dataTableRow = dataTable.NewRow();
                   dataTableRow["ID"] = item;
                   dataTable.Rows.Add(dataTableRow);
               }
           return dataTable;
       }

       

       protected override bool Body(DbConnection connection)
       {
           return false;
       }

       public bool RunOnTransaction(DbTransaction transaction)
       {
           bool result;
           const string storedProcedureName = "USExceGMSSaveNotificationForClassEntityChange";
           try
           {
               DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
               cmd.Parameters.Add(DataAcessUtils.CreateParam("@ActiveTimeId", DbType.Int32, _activeTimeId));
               cmd.Parameters.Add(DataAcessUtils.CreateParam("@AddedInsIdList", SqlDbType.Structured, _addedInsIdList));
               cmd.Parameters.Add(DataAcessUtils.CreateParam("@AddedResIdList", SqlDbType.Structured, _addedResIdList));
               cmd.Parameters.Add(DataAcessUtils.CreateParam("@DeletedInsIdList", SqlDbType.Structured, _deletedInsIdList));
               cmd.Parameters.Add(DataAcessUtils.CreateParam("@DeletedResIdList", SqlDbType.Structured, _deletedResIdList));
               cmd.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _user));
               cmd.Connection = transaction.Connection;
               cmd.Transaction = transaction;

               cmd.ExecuteNonQuery();
               result = true;
           }
           catch (Exception)
           {
               throw;
           }
           return result;
       }
    }
}
