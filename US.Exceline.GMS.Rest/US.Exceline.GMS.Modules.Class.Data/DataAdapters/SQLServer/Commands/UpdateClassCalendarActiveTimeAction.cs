﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateClassCalendarActiveTimeAction : USDBActionBase<int>
    {
        private DataTable _dataTableEntity;
        private DataTable _dtActiveTimes;
        private EntityActiveTimeDC _activeTime;
        private string _user = string.Empty;
        public UpdateClassCalendarActiveTimeAction(EntityActiveTimeDC activeTime, string user)
        {
            _activeTime = activeTime;
            _user = user;
            _dataTableEntity = GetEntityList(activeTime.InstructorList.Select(x => x.Id), activeTime.ResourceLst.Select(x => x.Id));
        }

        private DataTable GetEntityList(IEnumerable<int> insList, IEnumerable<int> resList)
        {
            _dataTableEntity = new DataTable();
            _dataTableEntity.Columns.Add(new DataColumn("ScheduleItemId", typeof(int)));
            _dataTableEntity.Columns.Add(new DataColumn("EntityId", typeof(int)));
            _dataTableEntity.Columns.Add(new DataColumn("EntityRoleType", typeof(string)));

            if (insList != null)
            {
                var enumerable = insList as int[] ?? insList.ToArray();
                if (enumerable.Any())
                {
                    foreach (int id in enumerable)
                    {
                        DataRow dataTableRow = _dataTableEntity.NewRow();
                        dataTableRow["ScheduleItemId"] = _activeTime.Id;
                        dataTableRow["EntityId"] = id;
                        dataTableRow["EntityRoleType"] = "INS";
                        _dataTableEntity.Rows.Add(dataTableRow);
                    }
                }
            }

            if (resList != null)
            {
                var enumerable = resList as int[] ?? resList.ToArray();
                if (enumerable.Any())
                {
                    foreach (int id in enumerable)
                    {
                        DataRow dataTableRow = _dataTableEntity.NewRow();
                        dataTableRow["ScheduleItemId"] = _activeTime.Id;
                        dataTableRow["EntityId"] = id;
                        dataTableRow["EntityRoleType"] = "RES";
                        _dataTableEntity.Rows.Add(dataTableRow);
                    }
                }
            }
            return _dataTableEntity;
        }
     
        protected override int Body(DbConnection connection)
        {
            int outPutId = -1;
            const string storedProcedureName = "USExceGMSClassesUpdateEntityActiveTime";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ActiveTimeId", DbType.Int32, _activeTime.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EntityList", SqlDbType.Structured, _dataTableEntity));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@User", SqlDbType.VarChar, _user));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@OutPut", DbType.Int32, outPutId));
                object obj = cmd.ExecuteScalar();
                outPutId = Convert.ToInt32(obj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return outPutId;
        }
    }
}
