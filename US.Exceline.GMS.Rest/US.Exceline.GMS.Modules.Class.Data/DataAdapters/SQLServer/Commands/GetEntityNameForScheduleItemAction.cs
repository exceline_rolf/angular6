﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer.Commands
{
    public class GetEntityIdNameForScheduleItemAction : USDBActionBase<Dictionary<int,string>>
    {
        private int _scheduleItemId;
        private string _role = string.Empty;

        public GetEntityIdNameForScheduleItemAction(int scheduleItemId, string role)
        {
            _scheduleItemId = scheduleItemId;
            _role = role;
        }
        protected override Dictionary<int, string> Body(System.Data.Common.DbConnection connection)
        {
            var entityNameList = new Dictionary<int, string>();
            const string storedProcedureName = "GetEntityIdForScheduleItem";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleItemId", DbType.Int32, _scheduleItemId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Role", DbType.String, _role));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    int entityId = Convert.ToInt32(reader["EntityId"]);
                    string entityName = (reader["EntityName"].ToString());
                    entityNameList.Add(entityId,entityName);
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return entityNameList;
        }
    }
}
