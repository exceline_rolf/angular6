﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer.Commands
{
    public class DeleteAddedFixScheduleItemsAction : USDBActionBase<bool>
    {
        private int _activeTimeId = -1;
        private List<int> _deletedInsList = new List<int>();
        private List<int> _deletedResList = new List<int>();
        private DateTime _startDate;


        public DeleteAddedFixScheduleItemsAction(UpdateClassActiveTimeHelperDC updateObjectHelper)
        {
            _activeTimeId = updateObjectHelper.ActiveTimeId;
            _deletedResList = updateObjectHelper.DeletedRESIDList;
            _deletedInsList = updateObjectHelper.DeletedINSIDList;
            _startDate = updateObjectHelper.StartDateTime;
        }
        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            return false;
        }

        public bool RunOnTransaction(DbTransaction transaction)
        {
            bool _isDeleted = true;
            try
            {
                string spName = "USExceGMSDeleteAddedFixScheduleItemsForEntities";
                foreach (int insID in _deletedInsList)
                {
                    DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                    cmd.Connection = transaction.Connection;
                    cmd.Transaction = transaction;
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ClassActiveTimeID", DbType.Int32, _activeTimeId));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@EntityID", DbType.Int32, insID));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@EntityRoleType", DbType.String, "INS"));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartDateTime", DbType.DateTime, _startDate));

                    cmd.ExecuteNonQuery();
                    _isDeleted = true;
                }
                foreach (int resID in _deletedResList)
                {
                    DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                    cmd.Connection = transaction.Connection;
                    cmd.Transaction = transaction;
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ClassActiveTimeID", DbType.Int32, _activeTimeId));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@EntityID", DbType.Int32, resID));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@EntityRoleType", DbType.String, "RES"));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartDateTime", DbType.DateTime, _startDate));

                    cmd.ExecuteNonQuery();
                    _isDeleted = true;
                }

            }
            catch (Exception ex)
            {
                _isDeleted = false;
                throw ex;
            }

            return _isDeleted;
        }
    }
}
