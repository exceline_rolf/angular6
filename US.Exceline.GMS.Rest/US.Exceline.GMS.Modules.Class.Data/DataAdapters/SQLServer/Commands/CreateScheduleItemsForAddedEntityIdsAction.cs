﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer.Commands
{
    public class CreateScheduleItemsForAddedEntityIdsAction : USDBActionBase<bool>
    {
        private int _entityId = -1;
        private string _roleType = string.Empty;
        private int _activeTimeId = -1;
        private DateTime _startDateTime;
        private DateTime _endDateTime;

        public CreateScheduleItemsForAddedEntityIdsAction(int entityId, string roleType,int activeTimeId,DateTime startTime, DateTime endTime)
        {
            _entityId = entityId;
            _roleType = roleType;
            _activeTimeId = activeTimeId;
            _startDateTime = startTime;
            _endDateTime = endTime;
        }
        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            return false;
        }

        public bool RunOnTransaction(DbTransaction transaction)
        {
            bool _isAdded = false;
            string storedProcedure = "USEXCEGMSCreateScheduleItemsForAddedEntityIds";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EntityId", DbType.Int32, _entityId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EntityRoleType", DbType.String, _roleType));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ClassActiveTimeId", DbType.Int32, _activeTimeId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartTime", DbType.DateTime, _startDateTime));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndTime", DbType.DateTime, _endDateTime));

                cmd.Connection = transaction.Connection;
                cmd.Transaction = transaction;

                cmd.ExecuteNonQuery();
                _isAdded = true;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _isAdded;
        }
    }
}
