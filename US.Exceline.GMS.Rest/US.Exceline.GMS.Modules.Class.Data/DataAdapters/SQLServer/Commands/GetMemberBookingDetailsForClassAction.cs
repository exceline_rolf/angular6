﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageClasses;
using System.Data.Common;
using System.Data;
using System.IO;

namespace US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer.Commands
{
    public class GetMemberBookingDetailsForClassAction : USDBActionBase<List<MemberBookingDetailsDC>>
    {
        private int _classId = -1;
        private int _branchId = -1;

        public GetMemberBookingDetailsForClassAction(int classId, int branchId)
        {
            _classId = classId;
            _branchId = branchId;
        }

        protected override List<MemberBookingDetailsDC> Body(DbConnection connection)
        {
            List<MemberBookingDetailsDC> _bookingDetails = new List<MemberBookingDetailsDC>();
            string StoredProcedureName = "USExceGMSManageClassesGetMemberBookingDetailsForClass";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@classId", DbType.Int32, _classId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    MemberBookingDetailsDC bookingDetail = new MemberBookingDetailsDC();
                    bookingDetail.ScheduleId = Convert.ToInt32(reader["ScheduleId"]);
                    bookingDetail.ScheduleItemId = Convert.ToInt32(reader["ScheduleItemId"]);
                    bookingDetail.ClassName = reader["ClassName"].ToString();
                    bookingDetail.MemberId = Convert.ToInt32(reader["MemberId"]);
                    bookingDetail.MemberName = reader["MemberName"].ToString();
                    bookingDetail.MemberImagePath = reader["ImagePath"].ToString();
                    if (!String.IsNullOrEmpty(bookingDetail.MemberImagePath))
                    {
                        bookingDetail.ProfilePicture = GetMemberProfilePicture(bookingDetail.MemberImagePath);
                    }
                    bookingDetail.ActiveTimeId = Convert.ToInt32(reader["ActiveTimeId"]);
                    bookingDetail.NumberOfParticipants = Convert.ToInt32(reader["NumberOfParticipants"]);
                    bookingDetail.Paid = Convert.ToBoolean(reader["Paid"]);
                    bookingDetail.StartDateTime = reader["StartDateTime"] != DBNull.Value ? Convert.ToDateTime(reader["StartDateTime"]) : (Nullable<DateTime>)null;
                    bookingDetail.EndDateTime = reader["EndDateTime"] != DBNull.Value ? Convert.ToDateTime(reader["EndDateTime"]) : (Nullable<DateTime>)null;
                    _bookingDetails.Add(bookingDetail);
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return _bookingDetails;
        }

        private byte[] GetMemberProfilePicture(string p)
        {
            if (File.Exists(p))
            {
                byte[] byteArray = System.IO.File.ReadAllBytes(p);
                return byteArray;
            }
            return new byte[0];
        }
    }
}
