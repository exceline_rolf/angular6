﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer.Commands
{
    public class DeleteClassActiveTimeAction:USDBActionBase<bool>
    {
        private readonly int _activeTimeID = -1;
        private readonly string _user = string.Empty;

        public DeleteClassActiveTimeAction(int activeTimeID,string user)
        {
            this._activeTimeID = activeTimeID;
            _user = user;
        }

        protected override bool Body(DbConnection connection)
        {
            bool _isDeleted = false;
            string StoredProcedureName = "USExceGMSDeleteClassActiveTimes";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ActiveTimeID", DbType.Int32, this._activeTimeID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _user));
                cmd.ExecuteNonQuery();
                _isDeleted = true;
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return _isDeleted;
        }
    }
}
