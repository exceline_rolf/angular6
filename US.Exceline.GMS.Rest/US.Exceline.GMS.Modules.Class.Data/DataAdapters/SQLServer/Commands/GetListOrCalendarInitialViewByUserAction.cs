﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer.Commands
{
    public class GetListOrCalendarInitialViewByUserAction : USDBActionBase<string>
    {
        private readonly string _user = string.Empty;

        public GetListOrCalendarInitialViewByUserAction(string user)
        {
            _user = user;
        }

        protected override string Body(DbConnection connection)
        {
            try
            {
                const string spName = "USExceGMSManageClassesGetListOrCalendarInitialViewByUser";
                var cmd = CreateCommand(CommandType.StoredProcedure, spName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@userName", DbType.String, _user));
                var dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    return Convert.ToString(dataReader["item"]);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return "NEW";
        }
    }
}
