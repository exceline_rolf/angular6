﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer.Commands
{
    public class GetEntitySchedules:USDBActionBase<List<ScheduleDC>>
    {
        private int _parentId;
        private string _className;

        public GetEntitySchedules(int parentId, string classname)
        {
            _parentId = parentId;
            _className = classname;
        }

        protected override List<ScheduleDC> Body(DbConnection connection)
        {
            List<ScheduleDC> scheduleList = new List<ScheduleDC>();
            string StoredProcedureName = "USExceGMSManageClassesGetEntityScheduleForClass";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@classname", DbType.String, _className));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@parentId", DbType.Int32, _parentId));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ScheduleDC entityschedule = new ScheduleDC();
                    entityschedule.Id = Convert.ToInt32(reader["EntityScheduleId"]);
                    entityschedule.EntityId = Convert.ToInt32(reader["EntityId"]);
                    entityschedule.EntityRoleType = reader["EntityType"].ToString();
                    scheduleList.Add(entityschedule);
                }
                
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return scheduleList;
        }

        public List<ScheduleDC> RunOnTransaction(DbTransaction Transaction)
        {
            List<ScheduleDC> scheduleList = new List<ScheduleDC>();
            string StoredProcedureName = "USExceGMSManageClassesGetEntityScheduleForClass";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Connection = Transaction.Connection;
                cmd.Transaction = Transaction;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@classname", DbType.String, _className));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@parentId", DbType.Int32, _parentId));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ScheduleDC entityschedule = new ScheduleDC();
                    entityschedule.Id = Convert.ToInt32(reader["EntityScheduleId"]);
                    entityschedule.EntityId = Convert.ToInt32(reader["EntityId"]);
                    entityschedule.EntityRoleType = reader["EntityType"].ToString();
                    scheduleList.Add(entityschedule);
                }

                reader.Close();

            }

            catch (Exception ex)
            {
                throw ex;
            }
            return scheduleList;
        }
    }
}
