﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer.Commands
{
    public class GetScheduleItemIdsAction:USDBActionBase<List<int>>
    {
        private int _classScheduleId = -1;

        public GetScheduleItemIdsAction(int classScheduleId)
        {
            _classScheduleId = classScheduleId;
        }

        protected override List<int> Body(DbConnection connection)
        {
            List<int> _scheduleItemIdList = new List<int>();
            string StoredProcedureName = "USExceGMSManageClassesGetEntityScheduleIds";
            DbTransaction transaction = null;
            try
            {
                transaction = connection.BeginTransaction();
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Transaction = transaction;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@classScheduleId", DbType.Int32, _classScheduleId));
                DbDataReader reader = cmd.ExecuteReader();
                while(reader.Read())
                {
                    int scheduleItemId = Convert.ToInt32(reader["EntityScheduleId"]);
                    _scheduleItemIdList.Add(scheduleItemId);
                }
            }

            catch(Exception ex)
            {
            throw  ex;
            }
            return _scheduleItemIdList;
        }
    }
}
