﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageClasses;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer.Commands
{
    public class GetClassActiveTimesAction : USDBActionBase<List<ExcelineClassActiveTimeDC>>
    {
        private int _branchId;
        private DateTime _startDate;
        private DateTime _endDate;
        private int _entNO;

        public GetClassActiveTimesAction(int branchId, DateTime startDate, DateTime endDate, int entNo)
        {
            _branchId = branchId;
            _startDate = startDate;
            _endDate = endDate;
            _entNO = entNo;
        }

        protected override List<ExcelineClassActiveTimeDC> Body(DbConnection connection)
        {
            List<ExcelineClassActiveTimeDC> excelineClassActiveTimeList = new List<ExcelineClassActiveTimeDC>();
            string storedProcedureName = "USExceGMSManageClassesGetExcelineClassActiveTimes";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@startDate", DbType.DateTime, _startDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@endDate", DbType.DateTime, _endDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityNo", DbType.Int32, _entNO));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ExcelineClassActiveTimeDC excelineClassActiveTime = new ExcelineClassActiveTimeDC();
                    excelineClassActiveTime.Id = Convert.ToInt32(reader["ID"]);
                    excelineClassActiveTime.SheduleItemId = Convert.ToInt32(reader["ScheduleItemId"]);
                    if(reader["StartDateTime"] != DBNull.Value)
                    excelineClassActiveTime.StartDateTime = Convert.ToDateTime(reader["StartDateTime"]);
                    if (reader["EndDateTime"] != DBNull.Value)
                    excelineClassActiveTime.EndDateTime = Convert.ToDateTime(reader["EndDateTime"]);
                    excelineClassActiveTime.EntityId = Convert.ToInt32(reader["EntityId"]);
                    excelineClassActiveTime.EntityRoleType = Convert.ToString(reader["EntityRoleType"]);
                    if (!string.IsNullOrEmpty(reader["Name"].ToString()))
                    {
                        excelineClassActiveTime.Name = Convert.ToString(reader["Name"]);
                    }

                    excelineClassActiveTimeList.Add(excelineClassActiveTime);

                }
            }

            catch (Exception ex)
            {

                throw ex;
            }

            return excelineClassActiveTimeList;
        }


    }
}
