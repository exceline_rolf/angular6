﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageClasses;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer.Commands
{
    public class GetClassActiveTimesForBookingAction : USDBActionBase<List<ClassBookingActiveTimeDC>>
    {

        private int _branchId;
        private DateTime _startDate;
        private DateTime _endDate;
        private int _entNO;

        public GetClassActiveTimesForBookingAction(int branchId, DateTime startDate, DateTime endDate, int entNo)
        {
            _branchId = branchId;
            _startDate = startDate;
            _endDate = endDate;
            _entNO = entNo;
        }

        protected override List<ClassBookingActiveTimeDC> Body(DbConnection connection)
        {
            List<ClassBookingActiveTimeDC> excelineClassActiveTimeList = new List<ClassBookingActiveTimeDC>();
            string storedProcedureName = "USExceGMSManageClassesGetExcelineClassActiveTimesForBooking";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@startDate", DbType.DateTime, _startDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@endDate", DbType.DateTime, _endDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityNo", DbType.Int32, _entNO));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ClassBookingActiveTimeDC excelineClassActiveTime = new ClassBookingActiveTimeDC();
                    excelineClassActiveTime.Id = Convert.ToInt32(reader["ScheduleId"]);
                    excelineClassActiveTime.SheduleItemId = Convert.ToInt32(reader["ScheduleItemId"]);
                    if(reader["StartDateTime"] != DBNull.Value)
                    excelineClassActiveTime.StartDateTime = Convert.ToDateTime(reader["StartDateTime"]);
                    if (reader["EndDateTime"] != DBNull.Value)
                    excelineClassActiveTime.EndDateTime = Convert.ToDateTime(reader["EndDateTime"]);
                    excelineClassActiveTime.EntityId = Convert.ToInt32(reader["ClassId"]);
                    excelineClassActiveTime.EntityRoleType = Convert.ToString(reader["EntityRoleType"]);
                    if (!string.IsNullOrEmpty(reader["Name"].ToString()))
                    {
                        excelineClassActiveTime.Name = Convert.ToString(reader["Name"]);
                    }
                    excelineClassActiveTime.ActiveTimeId = Convert.ToInt32(reader["ActiveTimeId"]);
                    excelineClassActiveTimeList.Add(excelineClassActiveTime);

                }
            }

            catch (Exception ex)
            {

                throw ex;
            }

            return excelineClassActiveTimeList;
        }
    }
}
