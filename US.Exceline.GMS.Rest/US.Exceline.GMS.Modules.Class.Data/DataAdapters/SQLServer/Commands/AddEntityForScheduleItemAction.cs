﻿using System;
using System.Data;
using System.Data.Common;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer.Commands
{
  public class AddEntityForScheduleItemAction : USDBActionBase<int>
  {
      private int _scheduleItemId;
      private int _entityId;
      private string _entityType;

      public AddEntityForScheduleItemAction(int scheduleItemId, int entityId,string entityType)
      {
          _scheduleItemId = scheduleItemId;
          _entityId = entityId;
          _entityType = entityType;

      }

        protected override int Body(DbConnection connection)
        {
            throw new NotImplementedException();
        }

        public int RunOnTransaction(DbTransaction transaction)
        {
            int entityScheduleItemId = -1;
            const string storedProcedureName = "USExceGMSManageClassesAddEntityForScheduleItem";

            try
            {

                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Connection = transaction.Connection;
                cmd.Transaction = transaction;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@parentId", DbType.Int32, _scheduleItemId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityId", DbType.Int32, _entityId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityType", DbType.String, _entityType));

                object obj = cmd.ExecuteScalar();
                entityScheduleItemId = Convert.ToInt32(obj);

            }

            catch (Exception ex)
            {
                throw ex;
            }

            return entityScheduleItemId;
        }
    }
}
