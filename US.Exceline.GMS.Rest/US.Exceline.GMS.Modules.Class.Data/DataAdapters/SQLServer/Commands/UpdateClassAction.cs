﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using US.GMS.Core.DomainObjects.ManageClasses;
using System.Data;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using System.Collections.ObjectModel;

namespace US.Exceline.GMS.Modules.Class.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateClassAction : USDBActionBase<int>
    {
        private ExcelineClassDC _excelineClass;
        public UpdateClassAction(ExcelineClassDC excelineClass)
        {
            _excelineClass = excelineClass;         
        }      

        protected override int Body(DbConnection connection)
        {
            int _classScheduleId = -1;
            string StoredProcedureName = "USExceGMSManageClassesUpdateClass";
            DbTransaction transaction = null;
            try
            {
                transaction = connection.BeginTransaction();
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Transaction = transaction;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@classId",DbType.Int32,_excelineClass.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@scheduleId",DbType.Int32, _excelineClass.Schedule.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Name", DbType.String, _excelineClass.Name));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId",DbType.Int32,1));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartDate",DbType.DateTime, _excelineClass.Schedule.StartDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndDate",DbType.DateTime,_excelineClass.Schedule.EndDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ClassCategoryId", DbType.Int32, _excelineClass.ClassCategory.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MaxNumberOfMembers",DbType.Int32, _excelineClass.MaxNumberOfMembers));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@LastModifiedUser",DbType.String, _excelineClass.ModifiedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ActiveStatus",DbType.Boolean, true));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsPayable", DbType.Boolean, _excelineClass.IsPayable));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AricleId", DbType.Int32, _excelineClass.ArticleId)); 
                cmd.ExecuteNonQuery();
                _classScheduleId = _excelineClass.Schedule.Id;

            }

            catch (Exception ex)
            {
                throw ex;
            }        

            return _classScheduleId;
        }

        public int RunOnTransaction(DbTransaction Transaction)
        {
            int _classScheduleId = -1;
            string StoredProcedureName = "USExceGMSManageClassesUpdateClass";            
            try
            {
                
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Connection = Transaction.Connection;
                cmd.Transaction = Transaction;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@classId", DbType.Int32, _excelineClass.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@scheduleId", DbType.Int32, _excelineClass.Schedule.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Name", DbType.String, _excelineClass.Name));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _excelineClass.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartDate", DbType.DateTime, _excelineClass.Schedule.StartDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndDate", DbType.DateTime, _excelineClass.Schedule.EndDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ClassCategoryId", DbType.Int32, _excelineClass.ClassCategory.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MaxNumberOfMembers", DbType.Int32, _excelineClass.MaxNumberOfMembers));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@LastModifiedUser", DbType.String, _excelineClass.ModifiedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ActiveStatus", DbType.Boolean, true));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsPayable", DbType.Boolean, _excelineClass.IsPayable));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AricleId", DbType.Int32, _excelineClass.ArticleId));

                if (_excelineClass.Schedule.Id != -1 && _excelineClass.Schedule.Id != 0)
                {
                    cmd.ExecuteNonQuery();
                    _classScheduleId = _excelineClass.Schedule.Id;
                }
                else
                {
                    object obj = cmd.ExecuteScalar();
                    _classScheduleId = Convert.ToInt32(obj);
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }       
         
            return _classScheduleId;
        }
    }
}
