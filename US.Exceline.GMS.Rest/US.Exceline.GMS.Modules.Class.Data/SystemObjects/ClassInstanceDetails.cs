﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Class.Data.SystemObjects
{
    public class ClassInstanceDetails 
    {
        private  List<InstructorData> _classInstructorList;
        private  List<ResourceData> _classResourceList;

        public ClassInstanceDetails(List<InstructorData> ClassInstructorList, List<ResourceData> ClassResourceList)
        {
            _classInstructorList = ClassInstructorList;
            _classResourceList = ClassResourceList;
            AlterationSpecs = new AlteredClassData(_classInstructorList, _instanceInstructorList, _classResourceList, _instanceResourceList);
        }
        public int Id { get; set; } = -1;
        public DateTime? Date { get; set; } = null;
        public int DayOfTheWeek { get; set; } = -1;
        public DateTime? StartTime { get; set; } = null;
        public DateTime? EndTime { get; set; } = null;
        public bool Altered { get; set; } = false;
        public String Occurence { get; set; } = String.Empty;
        public int MaxNoOfBookings { get; set; } = -1;
        public DateTime? LastModifiedDateTime { get; set; } = null;
        public String LastModifiedUser { get; set; } = String.Empty;

        private  List<InstructorData> _instanceInstructorList = new List<InstructorData>();
        public List<InstructorData> InstanceInstructorList
        {
            get { return _instanceInstructorList; }
            set { _instanceInstructorList = value; }
        }

        private  List<ResourceData> _instanceResourceList = new List<ResourceData>();
        public List<ResourceData> InstanceResourceList
        {
            get { return _instanceResourceList; }
            set { _instanceResourceList = value; }
        }
        
        public AlteredClassData AlterationSpecs { get; set; } 
    }
}
