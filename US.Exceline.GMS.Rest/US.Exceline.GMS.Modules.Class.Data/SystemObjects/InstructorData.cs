﻿using System;

namespace US.Exceline.GMS.Modules.Class.Data.SystemObjects
{
    public class InstructorData : IEquatable<InstructorData>
    {
        private int _id = -1;
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _activeStatus = -1;
        public int ActiveStatus
        {
            get { return _activeStatus; }
            set { _activeStatus = value; }
        }

        private String _displayName = String.Empty;
        public String DisplayName
        {
            get { return _displayName; }
            set { _displayName = value; }
        }

        private String _description = String.Empty;
        public String Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public override bool Equals(object obj)
        {
            var other = obj as InstructorData;
            if (other == null) return false;

            return Equals(other);
        }

        public bool Equals(InstructorData other)
        {
            if(other == null)
            {
                return false;
            }

            return other.Id == _id;
        }

        public override int GetHashCode() => (Id).GetHashCode();
    
    }
}
