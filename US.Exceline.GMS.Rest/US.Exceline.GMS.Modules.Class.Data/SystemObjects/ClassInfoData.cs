﻿using System;

namespace US.Exceline.GMS.Modules.Class.Data.SystemObjects
{
    public class ClassInfoData
    {
        private int _id = -1;
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _groupId = -1;
        public int GroupId
        {
            get { return _groupId; }
            set { _groupId = value; }
        }

        private String _name = String.Empty;
        public String Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private String _groupName = String.Empty;
        public String GroupName
        {
            get { return _groupName; }
            set { _groupName = value; }
        }

        private int _classLevelId = -1;
        public int ClassLevelId
        {
            get { return _classLevelId; }
            set { _classLevelId = value; }
        }

        private String _classLevelName = String.Empty;
        public String ClassLevelName
        {
            get { return _classLevelName; }
            set { _classLevelName = value; }
        }

        private String _colour = String.Empty;
        public String Colour
        {
            get { return _colour; }
            set { _colour = value; }
        }

        private String _comment = String.Empty;
        public String Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        private int _maxNoOfBookings = -1;
        public int MaxNoOfBookings
        {
            get { return _maxNoOfBookings; }
            set { _maxNoOfBookings = value; }
        }
    }
}
