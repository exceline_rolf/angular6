﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageClasses;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.SystemObjects;


namespace US.Exceline.GMS.Modules.Class.Data.SystemObjects
{
    public interface IClassDataAdapter
    {
        List<ExcelineClassDC> SearchClass(string category, string searchText, string gymCode);
        List<ExcelineClassDC> SearchClass(string category, string searchText, DateTime startDate, string gymCode);
        List<ExcelineClassDC> SearchClass(string category, string searchText, DateTime startDate, DateTime endDate, string gymCode);
        string SaveClass(ExcelineClassDC excelineClass, int branchId, string gymCode,string user);
        bool UpdateClass(ExcelineClassDC excelineClass, string gymCode);
        bool DeleteClass(int classId, string gymCode);
        int GetNextClassId(string gymCode);
        List<ExcelineClassDC> GetClasses(string className, int branchId, string gymCode);
        List<ScheduleItemDC> GetScheduleItemsByClass(int classId, int branchId, string gymCode);
        List<ScheduleItemDC> GetScheduleItemsByClassIds(List<int> classIds, int branchId, string gymCode);
        List<ExcelineClassActiveTimeDC> GetClassActiveTimes(int branchId, DateTime startDate, DateTime endDate, int entNo, string gymCode);
        List<InstructorDC> GetInstructorsForClass(int classId, string gymCode);
        List<ResourceDC> GetResourcesForClass(int classId, string gymCode);
        ScheduleDC GetClassSchedule(int classId, string gymCode);
        List<ExcelineClassMemberDC> GetMembersByActiveTimeId(int branchId, int activeTimeId, string gymCode);
       // List<ArticleDC> GetArticlesForClass(string gymCode);
        int GetExcelineClassIdByName(string className, string gymCode);
        int UpdateTimeTableScheduleItems(List<ScheduleItemDC> scheduleItemList,int branchId, string gymCode,string user);
        int SaveActiveTimes(List<ScheduleItemDC> scheduleItemList, string gymCode);
        string CheckActiveTimeOverlapWithClass(ScheduleItemDC scheduleItem, string gymCode);
        int SaveScheduleItem(ScheduleItemDC scheduleItem, int branchId, string gymCode,string user);
        int UpdateClassCalendarActiveTime(EntityActiveTimeDC activeTime, string user, string gymCode);
        int DeleteScheduleItem(List<int> scheduleItemIds, string gymCode ,string user);
        bool DeleteSchedule(ExcelineClassDC exceClass, string gymCode, string user);
        int UpdateSeasonEndDateWithClassActiveTimes(int branchId, int seasonId, DateTime endDate, string gymCode);
        bool DeleteClassActiveTime(int activeTimeId, string gymCode, string user);
        int UpdateClassActiveTime(UpdateClassActiveTimeHelperDC helperObj, string gymCode, string user);
        List<EntityActiveTimeDC> GetActiveTimesForClassScheduleItemAction(int scheduleItemId, string gymCode);
        string GetListOrCalendarInitialViewByUser(string user, string gymCode);
        ScheduleData GetSeasonAndClassInfo(int branchId, int scheduleId, String gymCode);
    }
}
