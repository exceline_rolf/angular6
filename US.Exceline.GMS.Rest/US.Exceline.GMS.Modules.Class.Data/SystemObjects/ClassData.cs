﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Class.Data.SystemObjects
{
    public class ClassData
    {
        public ClassData()
        {
            _classInstanceList = new List<ClassInstanceDetails> { new ClassInstanceDetails(_instructorList, _resourceList) };
        }

        private ClassInfoData _classInfo = new ClassInfoData();
        public ClassInfoData ClassInfo
        {
            get { return _classInfo; }
            set { _classInfo = value; }
        }

        private int _scheduleItemId = -1;
        public int ScheduleItemId
        {
            get { return _scheduleItemId; }
            set { _scheduleItemId = value; }
        }

        private String _nameOfClass = String.Empty;
        public String NameOfClass
        {
            get { return _nameOfClass; }
            set { _nameOfClass = value; }
        }

        private int _maxNoOfBookings = -1;
        public int MaxNoOfBookings
        {
            get { return _maxNoOfBookings; }
            set { _maxNoOfBookings = value; }
        }

        private int _dayOfTheWeek = -1;
        public int DayOfTheWeek
        {
            get { return _dayOfTheWeek; }
            set { _dayOfTheWeek = value; }
        }

        private DateTime? _startTime = null;
        public DateTime? StartTime
        {
            get { return _startTime; }
            set { _startTime = value; }
        }

        private DateTime? _endTime = null;
        public DateTime? EndTime
        {
            get { return _endTime; }
            set { _endTime = value; }
        }

        private DateTime? _startDate = null;
        public DateTime? StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }

        private DateTime? _endDate = null;
        public DateTime? EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }

        
        private int _classTypeId = -1;
        public int ClassTypeId
        {
            get { return _classTypeId; }
            set { _classTypeId = value; }
        }

        private  List<InstructorData> _instructorList = new List<InstructorData>();
        public List<InstructorData> InstructorList
        {
            get { return _instructorList; }
            set { _instructorList = value; }
        }

        private  List<ResourceData> _resourceList = new List<ResourceData>();
        public List<ResourceData> ResourceList
        {
            get { return _resourceList; }
            set { _resourceList = value; }
        }

        private List<ClassInstanceDetails> _classInstanceList;
        public List<ClassInstanceDetails> ClassInstanceList
        {
          
            get { return _classInstanceList;}
            set { _classInstanceList = value; }
        }

        private List<DateTime> _originalInstanceTimes = new List<DateTime>();
        public List<DateTime> OriginalInstanceTimes
        {
            get { return _originalInstanceTimes; }
            set { _originalInstanceTimes = value; }
        }
    }
}


