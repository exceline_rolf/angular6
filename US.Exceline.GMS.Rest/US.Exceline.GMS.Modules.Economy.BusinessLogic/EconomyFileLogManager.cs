﻿using System;
using System.Collections.Generic;
using US.Exceline.GMS.Modules.Economy.Core.DomainObjects;
using US.Exceline.GMS.Modules.Economy.Data;
using US.GMS.Core.ResultNotifications;

namespace US.Exceline.GMS.Modules.Economy.BusinessLogic
{
    public class EconomyFileLogManager
    {
        public static OperationResult<List<EconomyFileLogData>> GetEconomyFileLogData(DateTime fromDate, DateTime toDate,
                                                                                      string source, string filterBy,
                                                                                      string gymCode)
        {
            var result = new OperationResult<List<EconomyFileLogData>>();
            try
            {
                result.OperationReturnValue = EconomyFacade.GetEconomyFileLogData(fromDate, toDate, source, filterBy,
                                                                                  gymCode);
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in Get Economy file log details. " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<EconomyFileLogData>> GetEconomyFileLogDetailData(EconomyLogFileHelper economyLogFileHelper)
        {
            var result = new OperationResult<List<EconomyFileLogData>>();
            try
            {
                result.OperationReturnValue = EconomyFacade.GetEconomyFileLogDetailData(economyLogFileHelper);
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in Get Economy file log detail data=. " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        public static OperationResult<int> GetCcxEnabledStatus(int branchId, string gymCode)
        {
            var result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = EconomyFacade.GetCcxEnabledStatus(branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error on Getting ccx status " + ex.Message, MessageTypes.ERROR);
                throw ex;
            }
            return result;
        }
    }
}
