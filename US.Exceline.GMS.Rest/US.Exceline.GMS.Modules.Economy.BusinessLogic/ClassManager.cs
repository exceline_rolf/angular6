﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.DomainObjects;
using US.Exceline.GMS.Modules.Economy.Data;
using US.GMS.Core.ResultNotifications;

namespace US.Exceline.GMS.Modules.Economy.BusinessLogic
{
    public class ClassManager
    {
        public static OperationResult<List<ClassDetail>> GetClassSalary(List<int> branchIdList, List<int> employeeIdList, List<int> categoryIdList, DateTime fromDate, DateTime toDate, string gymCode)
        {
            var result = new OperationResult<List<ClassDetail>>();
            try
            {
                result.OperationReturnValue = ClassFacade.GetClassSalary(branchIdList, employeeIdList, categoryIdList, fromDate, toDate, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Get Class List: " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ClassFileDetail>> GetClassFileDetails(string gymCode)
        {
            var result = new OperationResult<List<ClassFileDetail>>();
            try
            {
                result.OperationReturnValue = ClassFacade.GetClassFileDetails(gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Get Class file detail: " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> SaveClassFileDetail(string fileName, string filePath, string user, string gymCode)
        {
            var result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ClassFacade.SaveClassFileDetail(fileName, filePath, user, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in saving class file: " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
    }
}
