﻿using System;
using System.Collections.Generic;
using US.Exceline.GMS.Modules.Economy.Core.DomainObjects;
using US.Exceline.GMS.Modules.Economy.Data;
using US.GMS.Core.ResultNotifications;

namespace US.Exceline.GMS.Modules.Economy.BusinessLogic
{
    public class PaymentManager
    {
        public static OperationResult<List<USPErrorPayment>> GetErrorPaymentsByErrorType(DateTime from, DateTime to, string errorPaymentType, string gymCode, int branchid,string batchId=null)
        {
            var result = new OperationResult<List<USPErrorPayment>>();
            try
            {
                var errorPayments = PaymentFacade.GetErrorPaymentDetails(errorPaymentType, from, to, gymCode, branchid, batchId);
                result.OperationReturnValue = errorPayments;
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in Get Error Payments By Error Type. " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> RemovePayment(int paymentID, int arItemno,  string user, string gymCode, string type)
        {
            var result = new OperationResult<bool>();
            try
            {
                var removedStatus = PaymentFacade.RemovePayment(paymentID, arItemno, user, gymCode, type);
                result.OperationReturnValue = removedStatus;
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in removing Payments. " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> MovePayment(int paymentID, int aritemno, string type, string user, string gymCode)
        {
            var result = new OperationResult<int>();
            try
            {
                var removedStatus = PaymentFacade.MovePayment(paymentID, aritemno,type, user, gymCode);
                result.OperationReturnValue = removedStatus;
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in moving Payments. " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
    }
}
