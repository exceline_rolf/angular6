﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR;
using US.Exceline.GMS.Modules.Economy.Data;

namespace US.Exceline.GMS.Modules.Economy.BusinessLogic
{
    public class OCRValidationRecordCollectionByCreditorCollection
    {
        private string _gymCode = string.Empty;
        public OCRValidationRecordCollectionByCreditorCollection(string gymCode)
        {
            _gymCode = gymCode;
            RecordsCollectionByCreditorAccountNo = new List<OCRValidationRecordCollectionByCreditor>();
        }
        /// <summary>
        /// Count of lines in the file
        /// </summary>
        public int NumOfLinesInFile { get; set; }

        public List<OCRValidationRecordCollectionByCreditor> RecordsCollectionByCreditorAccountNo { get; set; }

        public void AddRecord(OCRValidationRecord validationRecord)
        {
            OCRValidationRecordCollectionByCreditor recordCollectionByCreditorAccountNo = GetRecordCollectionByCreditorAccountNo(validationRecord.CreditorInkassoID);
            if (recordCollectionByCreditorAccountNo == null)
            {
                recordCollectionByCreditorAccountNo = new OCRValidationRecordCollectionByCreditor(validationRecord.CreditorInkassoID, _gymCode);
                RecordsCollectionByCreditorAccountNo.Add(recordCollectionByCreditorAccountNo);
            }
            recordCollectionByCreditorAccountNo.AddRecord(validationRecord);
        }

        private OCRValidationRecordCollectionByCreditor GetRecordCollectionByCreditorAccountNo(string creditorAccountNo)
        {
            try
            {
                return RecordsCollectionByCreditorAccountNo.Single(i => i.CreditorAccountNo == creditorAccountNo);
            }
            catch
            {
                return null;
            }
        }

        public int WriteValidationLog(string fileNameWithPath, int fileID)
        {
            bool hasCriticalErrors = false;
            string criticalErrorMsg = string.Empty;
            foreach (OCRValidationRecordCollectionByCreditor recordList in RecordsCollectionByCreditorAccountNo)
            {
                try
                {
                    var record = recordList.ValidationRecordsList.First(rec => rec.HasCriticalError);
                    hasCriticalErrors = true;
                    criticalErrorMsg = record.CriticalErrorMessage;
                    break;
                }
                catch {}
            }
            if (hasCriticalErrors)
            {
                WriteFileStatus(true, criticalErrorMsg, fileID);
            }
            else
            {
                WriteFileStatus(false, criticalErrorMsg, fileID);
            }
            return WriteFileValidatingStates(fileID);
        }

        private void WriteFileStatus(bool hasCriticalErrors, string errrorMessage, int fileID)
        {
            string status = string.Empty;
            status = hasCriticalErrors ? ValidationConstents.VALIDATION_STATUS_INVALID : ValidationConstents.VALIDATION_STATUS_VALIDATED;
            OCRFacade.AddFileStatus(fileID, DateTime.Now, status, errrorMessage, string.Empty, _gymCode);
        }

        private int WriteFileValidatingStates(int fileID)
        {
            int savedRecordCount = 0;
            foreach (OCRValidationRecordCollectionByCreditor record in RecordsCollectionByCreditorAccountNo)
            {
                savedRecordCount += record.WriteValidationLog(fileID);
            }
            return savedRecordCount;
        }
    }
}
