﻿using System.Security.Cryptography;
using System.Text;




namespace US.Exceline.GMS.Modules.Economy.BusinessLogic
{
    class ProcessHandlerForKasselov
    {
        private SHA1Managed sha1;
        private byte[] formatedData;
        private byte[] hashedData;

        public void formatInput(string transactionData) {
            this.formatedData = Encoding.UTF8.GetBytes(transactionData);
            
        }

        public void computeHash() {
            sha1 = new SHA1Managed();
            this.hashedData = sha1.ComputeHash(this.formatedData);
        }

        public byte[] getHashedData()
        {
            return this.hashedData;
        }
      
    }
}
