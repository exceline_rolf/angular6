﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.Exceline.GMS.Modules.Economy.Data;

namespace US.Exceline.GMS.Modules.Economy.BusinessLogic
{
    public class TransactionManager
    {
        public static PaymentProcessResult RegisterTransaction(List<IUSPTransaction> transactionList, DbTransaction transaction)
        {
            return TransactionFacade.RegisterTransaction(transactionList, transaction);
        }
    }
}
