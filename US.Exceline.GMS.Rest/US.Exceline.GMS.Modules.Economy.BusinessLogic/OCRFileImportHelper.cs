﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR;
using US.Exceline.GMS.Modules.Economy.Data;
using US.GMS.Core.ResultNotifications;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums;
using System.Collections;

namespace US.Exceline.GMS.Modules.Economy.BusinessLogic
{
    public static class OCRFileImportHelper
    {
        private const int NUMBER_OF_RECORDS_PER_ONE_SERVICE_ACCESS = 200;
        //private static USImportResult<IUSPCreditor> cred = new USImportResult<IUSPCreditor>();
        //List<INotificationMessage> notificationRecords = new List<INotificationMessage>();

        public static string ReadEntireFile(string fileName)
        {
            string fileContents = string.Empty;
            if (!File.Exists(fileName))
            {
                throw new FileNotFoundException(fileName + " could not be found.");
            }
            try
            {
                //create a new TextReader and open our file
                using (StreamReader sr = new StreamReader(new FileStream(fileName, FileMode.Open)))
                {
                    fileContents = sr.ReadToEnd();
                    sr.Close();
                    sr.Dispose();
                }
                return fileContents;
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to read file " + fileName + " : " + ex.Message);
            }

        }

        public static IUSPOCRRecord ProcessFile(string fileName, string gymCode, bool isSummary, OperationResult<OCRImportSummary> result)
        {
            IUSPOCRRecord ocrRecord = null;
            try
            {
                ocrRecord = GetOCRRecord(fileName, gymCode, isSummary, result);

            }
            catch (Exception ex)
            {
                ocrRecord = null;
                result.CreateMessage(ex.Message, MessageTypes.ERROR);
            }
            return ocrRecord;
        }

        public static OCRValidationRecordCollectionByCreditorCollection ValidateOCRRecord(IUSPOCRRecord ocrRecord, out OCRValidationRecordCollectionByCreditorCollection validationRecordsList, string gymCode)
        {
            validationRecordsList = new OCRValidationRecordCollectionByCreditorCollection(gymCode);

            int transCountForAssignment = 0;
            int numOfRecords = 0;
            decimal amountCountForAssignment = 0.0M;
            int totalTransCount = 0;
            decimal totalAmount = 0.0M;
            int totalNumOfRecords = 0;
            string currentRecordType = string.Empty;
            string inkassoID = string.Empty;


            if (ocrRecord.StartSendingReocrd == null)
            {
                throw new Exception("Start Sending Record not found");
            }
            else
            {
                totalNumOfRecords++;
            }
            if (ocrRecord.EndSendingRecord == null)
            {
                throw new Exception("End Sending Record not found");
            }
            if (ocrRecord.AssignmentRecords == null || (ocrRecord.AssignmentRecords.Count == 0))
            {
                throw new Exception("No Assignment Record found");
            }


            foreach (OCRAssignmentRecord assignmentRecord in ocrRecord.AssignmentRecords)
            {
                transCountForAssignment = 0;
                numOfRecords = 0;
                amountCountForAssignment = 0.0M;

                if (assignmentRecord.StartAssignmentRecord != null)
                {
                    // count start assignment records
                    numOfRecords++;
                    if (string.IsNullOrEmpty(assignmentRecord.StartAssignmentRecord.RecipientAccountNumber.Trim()))
                    {
                        //Throw an Critical Error
                        throw new Exception("An Account Number is empty. Record type : 20 (StartAssignmentRecord)");
                    }
                    else
                    {
                        OperationResult<string> cred = new OperationResult<string>();
                        cred.OperationReturnValue = GetCreditorInkassoIDbyAccountNumber(assignmentRecord.StartAssignmentRecord.RecipientAccountNumber.Trim(), "payment", gymCode);
                        if (cred.ErrorOccured || cred.OperationReturnValue == null)
                        {                     
                            inkassoID = string.Empty;
                        }
                        else
                        {
                            inkassoID = cred.OperationReturnValue;
                        }
                    }
                }
                else
                {
                    throw new Exception("Start Assignment Record not found");
                }
                foreach (OCRTransactionRecord transRecord in assignmentRecord.TransactionRecords)
                {
                    OCRValidationRecord validationLogRecord = new OCRValidationRecord(transRecord.LineIndexOfFirstPart);
                    validationLogRecord.CreditorInkassoID = inkassoID;
                    validationLogRecord.ValidationRecordType = assignmentRecord.RecordTypeOfFirstRecord.ToOCRValidationRecordTypes();

                    transCountForAssignment++;
                    if (transRecord is OCRTransactionValidRecord)
                    {
                        currentRecordType = "OCR Transaction Record";
                        //MRA. Bug(Decimal culture issue) fixed. 2010.07.31
                        //validationLogRecord.Amount = decimal.Parse(((OCRTransactionValidRecord)transRecord).OCRTransactionPost1.Amount)/100;
                        validationLogRecord.Amount = (((OCRTransactionValidRecord)transRecord).OCRTransactionPost1.Amount).ToValidDecimalForCulture() / 100;
                        amountCountForAssignment += validationLogRecord.Amount;
                        numOfRecords += 2;
                        //((OCRTransactionValidRecord)transRecord).OCRTransactionPost1.          
                        validationLogRecord.LineIndex = ((OCRTransactionValidRecord)transRecord).LineIndexOfFirstPart;
                    }
                    else if (transRecord is OCRDirectDeductValidRecord)
                    {
                        // do nothing for the movement.
                        numOfRecords++;
                        currentRecordType = "OCR Direct Deduct Record";
                        validationLogRecord.LineIndex = ((OCRDirectDeductValidRecord)transRecord).LineIndexOfFirstPart;
                    }
                    validationRecordsList.AddRecord(validationLogRecord);
                }
                if (assignmentRecord.EndAssignmentRecord != null)
                {
                    // count start assignment records
                    numOfRecords++;
                }
                else
                {
                    throw new Exception("End Assignment Reocrd not found. Record Type : "
                        + currentRecordType + ". ");
                }
                if (int.Parse(assignmentRecord.EndAssignmentRecord.NumberOfTrans) != transCountForAssignment)
                {
                    throw new Exception("Number of transaction records not match with End Assignment Record transaction count. Record Type : "
                        + currentRecordType + ". ");
                }
                //MRA. Bug(Decimal culture issue) fixed. 2010.07.31
                //if (decimal.Parse(assignmentRecord.EndAssignmentRecord.TotalAmount)/100 != amountCountForAssignment)
                if (assignmentRecord.EndAssignmentRecord.TotalAmount.ToValidDecimalForCulture() / 100 != amountCountForAssignment)
                {
                    throw new Exception("Total amount not match with End Assignment Record total amount. Record Type : "
                        + currentRecordType + ". ");
                }
                if (int.Parse(assignmentRecord.EndAssignmentRecord.NumberOfRecords) != numOfRecords)
                {
                    throw new Exception("Number of records not match with End Assignment Record record count. Record Type : "
                        + currentRecordType + ". ");
                }
                //}
                //else if (assignmentRecord.RecordTypeOfFirstRecord == OCRRecordTypes.StartTransactionAmountItem1)
                //{
                // if there is another record type, it should be handled here.
                //}

                totalTransCount += transCountForAssignment;
                totalAmount += amountCountForAssignment;
                totalNumOfRecords += numOfRecords;
            }
            if (totalTransCount != int.Parse(ocrRecord.EndSendingRecord.NumberOfTrans))
            {
                throw new Exception("Number of transaction records not match with End Sending Record transaction count.");
            }
            else
            {
                totalNumOfRecords++;
            }
            //MRA. Bug(Decimal culture issue) fixed. 2010.07.31
            //if (totalAmount != decimal.Parse(ocrRecord.EndSendingRecord.TotalAmount)/100)
            if (totalAmount != ocrRecord.EndSendingRecord.TotalAmount.ToValidDecimalForCulture() / 100)
            {
                throw new Exception("Total amount not match with End Sending Record total amount.");
            }
            if (totalNumOfRecords != int.Parse(ocrRecord.EndSendingRecord.NumberOfRecords))
            {
                throw new Exception("Total number of records not match with End Sending Record record count.");
            }
            return validationRecordsList;
        }

        private static string GetCreditorInkassoIDbyAccountNumber(string accountNo, string checkType,  string gymCode)
        {
            Creditor creditor = OCRFacade.GetCreditorInkassoIDbyAccountNumber(accountNo, checkType, gymCode);
            if (creditor != null)
                return creditor.InkassoId;
            else
                return string.Empty;
        }

        public static IUSPOCRRecord GetOCRRecord(string filePath, string gymCode, bool isSummary, OperationResult<OCRImportSummary> result)
        {
            IUSPOCRRecord ocrRecord = new OCRRecord();

            bool isFirstAssignmentRecords = true;
            if (File.Exists(filePath))
            {
                int lineIndex = -1;
                //int transactionIndex = -1;
                IUSPOCRAssignmentRecord currentAssignmentRecord = null;
                bool isFirstInnerRecord = true;
                StringReader recordReader = new StringReader(ReadEntireFile(filePath));
                string NextLine;
                string firstLine = string.Empty;
                bool validCreditor = false;
                bool isHelpAccount = false;
                string currentAccNo = string.Empty;
                double errorAmount = 0;
                int transCount = 0;
                string BBSDate = string.Empty;
                OperationResult<string> HelpAccountCreditor = new OperationResult<string>();
                List<string> errorFile = new List<string>();
                while ((NextLine = recordReader.ReadLine()) != null)
                {
                    lineIndex++;

                    if (!checkLine(NextLine, "0", 80, lineIndex))
                    {
                        continue;
                    }

                    string RecordType = NextLine.Substring(6, 2);
                    try
                    {
                        
                        switch (RecordType)
                        {
                            case "10":
                                ocrRecord.StartSendingReocrd = GetStartTransmisionRecord(ref NextLine);
                                firstLine = NextLine;
                                break;
                            case "20":
                                string creditorCheckType = string.Empty;
                                string creditorCheckTypeValue = NextLine.Substring(2, 6);
                                //********************************************************
                                //Modification is done to handle creditor account number.
                                //Payment type will check creditor acccount number from CreditorSettingHistoryTable in case it not in CreditorSettingTable
                                //NewCancel records will not check account number in CreditorSettingHistoryTable
                                //AAB-2012-03-31

                                if (creditorCheckTypeValue == "090020")
                                {
                                    creditorCheckType = "Payment";

                                }
                                else if (creditorCheckTypeValue == "212420")
                                {
                                    creditorCheckType = "NewCancel";
                                }
                                //***********************************************************
                                if (ocrRecord.StartSendingReocrd == null)
                                    throw new Exception("Start Sending Record not found.");
                                if (isFirstAssignmentRecords)
                                {
                                    ocrRecord.AssignmentRecords = new List<IUSPOCRAssignmentRecord>();
                                    isFirstAssignmentRecords = false;
                                }
                                currentAssignmentRecord = new OCRAssignmentRecord();
                                currentAssignmentRecord.StartAssignmentRecord = GetOCRStartAssignmentRecord(ref NextLine);
                                ocrRecord.AssignmentRecords.Add(currentAssignmentRecord);
                                isFirstInnerRecord = true;
                                currentAccNo = currentAssignmentRecord.StartAssignmentRecord.RecipientAccountNumber.Trim();
                                // if accountno is a help account then create a new ocr file
                                HelpAccountCreditor = new OperationResult<string>();
                                HelpAccountCreditor.OperationReturnValue = GetCreditorInkassoIDbyAccountNumber(currentAccNo, "paymentHelpAccount", gymCode);
                                OperationResult<string> cred = new OperationResult<string>();

                                if (string.IsNullOrEmpty(HelpAccountCreditor.OperationReturnValue))
                                {
                                    cred = new OperationResult<string>();
                                    cred.OperationReturnValue = GetCreditorInkassoIDbyAccountNumber(currentAccNo, creditorCheckType, gymCode);
                                }
                                else
                                {
                                    isHelpAccount = true;
                                }
                                if (string.IsNullOrEmpty(cred.OperationReturnValue))
                                {
                                    // if crediot account not exist create new ocr file
                                    validCreditor = false;
                                    errorFile.Add(firstLine);
                                    errorFile.Add(NextLine);
                                    result.CreateMessage("Could not find gym with given Account Number[" + currentAccNo + "]", MessageTypes.WARNING);
                                }
                                else
                                {
                                    validCreditor = true;
                                }
                                break;
                            case "30":

                                string recordType31Line = string.Empty;

                                while (true)
                                {
                                    recordType31Line = recordReader.ReadLine();
                                    if (recordType31Line == null)
                                    {
                                        break;
                                    }
                                    lineIndex++;
                                    if (OCRFileImportHelper.checkLine(recordType31Line, "30", 80, lineIndex))
                                    {
                                        break;
                                    }

                                }

                                if (recordType31Line.Substring(6, 2).ToOCRRecordTypes(6) == OCRRecordTypes.StartTransactionAmountItem2)
                                {
                                    //LogginManager.WriteInfoLog("(Record Type 31) Found Transaction Record 2 nd Part", lineIndex, logFilePath);
                                }
                                else
                                {
                                    //LogginManager.WriteInfoLog("Invalid Record Type : " + recordType31Line.Substring(6, 2).ToOCRRecordTypes(6).ToString() + ". Expected :(Record Type 31) Transaction 2 nd Part Record", lineIndex, logFilePath);
                                    throw new Exception("Invalid Record Type : " + recordType31Line.Substring(6, 2).ToOCRRecordTypes(6).ToString() + ". Expected :(Record Type 31) Transaction 2 nd Part Record");
                                }
                                if (isFirstInnerRecord)
                                {
                                    currentAssignmentRecord.RecordTypeOfFirstRecord = OCRRecordTypes.StartTransactionAmountItem1;
                                    currentAssignmentRecord.TransactionRecords = new List<IUSPOCRTransactionRecord>();
                                    isFirstInnerRecord = false;
                                }
                                IUSPOCRTransactionRecord transRecord = GetOCRTransactionRecord(ref NextLine, ref recordType31Line, (lineIndex - 1), lineIndex);
                                OCRTransactionValidRecord valieRec = (OCRTransactionValidRecord)(transRecord);
                                currentAssignmentRecord.TransactionRecords.Add(transRecord);
                                if (!validCreditor)
                                {
                                    transCount = transCount + 1;
                                    BBSDate = valieRec.OCRTransactionPost1.TransDate;
                                    errorAmount = errorAmount + Convert.ToDouble(valieRec.OCRTransactionPost1.Amount);
                                    errorFile.Add(NextLine);
                                    errorFile.Add(recordType31Line);
                                }
                                //LogginManager.WriteInfoLog("Transaction Record. KID (" + valieRec.OCRTransactionPost1.KID + ")", lineIndex, logFilePath);
                                break;
                            case "88":
                                currentAssignmentRecord.EndAssignmentRecord = GetOCREndAssignmentRecord(NextLine, lineIndex);
                                currentAssignmentRecord = null;
                                if (!validCreditor)
                                {

                                    errorFile.Add(NextLine);
                                    if (!isSummary) // error files are not created when summary is taken
                                        CreateErrorOCR(errorFile, currentAccNo, filePath, transCount, errorAmount, BBSDate, isHelpAccount);

                                    errorFile.Clear();
                                    transCount = 0;
                                    errorAmount = 0;
                                }

                                break;
                            case "89":
                                if (ocrRecord.StartSendingReocrd == null)
                                    throw new Exception("Start Sending Record not found.");
                                ocrRecord.EndSendingRecord = OCRFileImportHelper.GetOCREndTransmisionRecord(NextLine);
                                break;
                            case "70":
                                if (isFirstInnerRecord)
                                {
                                    currentAssignmentRecord.RecordTypeOfFirstRecord = OCRRecordTypes.RecordDirectDeduct;
                                    currentAssignmentRecord.TransactionRecords = new List<IUSPOCRTransactionRecord>();
                                    isFirstInnerRecord = false;
                                }

                                OCRTransactionRecord transRecordDD = GetDirectDeductValidRecord(NextLine, lineIndex);
                                //OCRDirectDeductValidRecord validDD = (OCRDirectDeductValidRecord)transRecordDD;
                                currentAssignmentRecord.TransactionRecords.Add(transRecordDD);

                                if (!validCreditor)
                                {
                                    transCount = transCount + 1;
                                    //BBSDate = validDD.;
                                    errorAmount = 0;
                                    errorFile.Add(NextLine);
                                    //  errorFile.Add(recordType31Line);
                                }
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        result.CreateMessage("Record Type : " + RecordType + " : Line : " + lineIndex.ToString() +" " + ex.Message, MessageTypes.ERROR);
                    }
                }
                recordReader.Close();
                recordReader.Dispose();
            }

            return ocrRecord;

        }

        private static void CreateErrorOCR(List<string> errorFile, string currentAccNo, string filePath, int transCount, double totalAmount, string bbsDate, bool isHelpAccount)
        {
            try
            {
                if (bbsDate == "")
                {
                    bbsDate = DateTime.Now.ToString("ddMMyy");
                }
                else
                {
                    bbsDate = Convert.ToDateTime(bbsDate).ToString("ddMMyy");
                    // bbsDate.conv
                }


                string lastLine = "NY000089" + transCount.ToString().PadLeft(8, '0') + (errorFile.Count + 1).ToString().PadLeft(8, '0') + totalAmount.ToString().PadLeft(17, '0') + bbsDate.ToString() + "000000000000000000000000000000000";
                
                
                string directory = Path.GetDirectoryName(filePath);

                if (!Directory.Exists(directory + "\\ErrorFiles"))
                {
                    Directory.CreateDirectory(directory + "\\ErrorFiles");
                }

                if (!Directory.Exists(directory + "\\ErrorFiles\\" + Path.GetFileName(filePath)))
                {
                    Directory.CreateDirectory(directory + "\\ErrorFiles\\" + Path.GetFileName(filePath));
                }

                directory = directory + "\\ErrorFiles\\" + Path.GetFileName(filePath);

                string newDir = string.Empty;
                if (isHelpAccount)
                {
                    newDir = directory.Replace("Pending", "HelpAccountCreditor");
                }
                else
                {
                    newDir = directory.Replace("Pending", "InvalidCreditor");
                }

                if (!Directory.Exists(newDir))
                {
                    Directory.CreateDirectory(newDir);
                }

                

                string newPath = string.Empty;
                if (totalAmount == 0)
                {
                    newPath = newDir + "\\" + Path.GetFileName(filePath) + "_" + "NewCancel_" + currentAccNo + ".txt";
                }
                else
                {
                    newPath = newDir + "\\" + Path.GetFileName(filePath) + "_" + currentAccNo + ".txt";

                }
                errorFile.Add(lastLine);
                if (!File.Exists(newPath))
                {
                    File.WriteAllLines(newPath, errorFile.ToArray());
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static bool checkLine(string line, string recodeType, int length, int lineIndex)
        {

            string trimLine = line.Trim();
            if (trimLine.Length != length)
            {
                if (recodeType.Equals("30"))
                {
                    //LogginManager.WriteInfoLog("found line with invalid length (error prone): inside recode ", lineIndex, logFilePath);
                }

                else
                {
                    //LogginManager.WriteInfoLog("found line with invalid length :outside recode ", lineIndex, logFilePath);
                }


                return false;
            }

            return true;

        }

        public static List<IUSPPayment> GetPayments(List<OCRTransactionRecord> transRecords, string gymCode)
        {
            List<IUSPPayment> payments = new List<IUSPPayment>();
            List<OCRTransactionValidRecord> validTransRecords = GetValidTransRecords(transRecords);
            if (validTransRecords.Count == 0)
            {
                throw new Exception("No valid transaction records found.");
            }
            foreach (OCRTransactionValidRecord validTransRecord in validTransRecords)
            {
                try
                {
                    payments.Add(GetPaymentDCbyOCRTransactionValidRecord(validTransRecord, gymCode));
                }
                catch
                {
                    //WriteToLog("Invalid record [line : " + validTransRecord.LineIndexOfFirstPart + "]. " + ex.Message);
                }
            }
            return payments;
        }

        private static List<IUSPNewOrChanged> GetNewOrChanges(List<OCRTransactionRecord> transRecords)
        {
            List<IUSPNewOrChanged> newOrChangedOCRs = new List<IUSPNewOrChanged>();
            List<OCRDirectDeductValidRecord> validDirectDeductRecords = GetValidDirectDeductRecords(transRecords);
            if (validDirectDeductRecords.Count == 0)
            {
                throw new Exception("No valid direct deduct records found.");
            }
            foreach (OCRDirectDeductValidRecord validTransRecord in validDirectDeductRecords)
            {
                try
                {
                    newOrChangedOCRs.Add(GetNewOrChangedDCbyOCRTransactionValidRecord(validTransRecord));
                }
                catch
                {
                }
            }
            return newOrChangedOCRs;
        }

        private static List<OCRDirectDeductValidRecord> GetValidDirectDeductRecords(List<OCRTransactionRecord> directDeductRecords)
        {
            List<OCRErrorRecord> errorTransRecords = new List<OCRErrorRecord>();
            List<OCRDirectDeductValidRecord> validTransRecords = new List<OCRDirectDeductValidRecord>();
            OCRErrorRecord errorTransRecord;
            bool foundErrorRecordsFlag = false;
            foreach (OCRTransactionRecord transRecord in directDeductRecords)
            {
                if (transRecord is OCRErrorRecord)
                {

                    if (!foundErrorRecordsFlag)
                    {
                        foundErrorRecordsFlag = true;
                    }
                    errorTransRecord = (OCRErrorRecord)transRecord;
                    errorTransRecords.Add(errorTransRecord);
                }
                else
                {
                    validTransRecords.Add((OCRDirectDeductValidRecord)transRecord);
                }
            }
            if (errorTransRecords.Count == 0)
            { // No error record found
                //Should return valid transaction records
                return validTransRecords;
            }
            else
            {
                // There are some error records
                return new List<OCRDirectDeductValidRecord>();
            }
        }

        private static IUSPPayment GetPaymentDCbyOCRTransactionValidRecord(OCRTransactionValidRecord validTransRecord, string gymCode)
        {
            IUSPPayment newPayment = new USPPayment();
            try
            {
                newPayment.KID = validTransRecord.OCRTransactionPost1.KID;
                decimal amountInDec;
                try
                {
                    //MRA. Bug(Decimal culture issue) fixed. 2010.07.31
                    //amountInDec = ((decimal.Parse(validTransRecord.OCRTransactionPost1.Amount)) / 100);
                    amountInDec = ((validTransRecord.OCRTransactionPost1.Amount.ToValidDecimalForCulture()) / 100);
                }
                catch (Exception)
                {
                    throw new Exception("Amount is not valid.");
                }
                string amountInStr = amountInDec.ToString();
                string currentCul = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
                if (currentCul.Equals("nb-NO"))
                {
                    amountInStr = amountInStr.Replace(",", ".");
                }
                newPayment.Amount = amountInStr;
                //newPaymentDC.ARNo
                newPayment.DueDate = validTransRecord.OCRTransactionPost1.TransDate;
                newPayment.Txt = "OCR Payment";
                newPayment.VouDate = validTransRecord.OCRTransactionPost2.PaymentDate;
                newPayment.ItemType = GetItemTypeByTypes("OP", gymCode);
                //newPaymentDC.ItemType
                //newPaymentDC.Ref=
                //newPaymentDC.Txt=validTransRecord.OCRTransactionPost1.
                //newPaymentDC.VouDate=validTransRecord.OCRTransactionPost2.
                // there are more to assign for newPaymentDC
            }
            catch (Exception ex)
            {
                throw new Exception("Invalid data in transaction record. Line No : "
                            + validTransRecord.LineIndexOfFirstPart + ". " + ex.Message);
            }
            return newPayment;
        }

        public static OCRStartTransmissionRecord GetStartTransmisionRecord(ref string line)
        {
            try
            {
                OCRStartTransmissionRecord record = new OCRStartTransmissionRecord();
                record.FormatCode = line.Substring(0, 2);
                record.ServiceCode = line.Substring(2, 2);
                record.SendingType = line.Substring(4, 2);
                record.RecordType = line.Substring(6, 2).ToOCRRecordTypes(6);

                if (record.RecordType != OCRRecordTypes.StartSending)
                {
                    throw new Exception("Expected record type not found. Expected reocrd type : TransactionAmountItem2 ["
                        + (int)OCRRecordTypes.StartSending + "]. Found record type : " + (int)record.RecordType + "");
                }
                record.DataSender = line.Substring(8, 8);
                record.SendingNumber = line.Substring(16, 7);
                record.DataRecipient = line.Substring(23, 8);
                return record;
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to read Start Transmision Record. " + ex.Message);
            }
        }

        public static int GetItemTypeByTypes(string type, string gymCode)
        {
            foreach (string[] _types in GetItemTypeDefinitions(gymCode).OperationReturnValue.ItemTypes)
            {
                if (_types[1].Trim().ToUpper() == type.Trim().ToUpper())
                {
                    return int.Parse(_types[0]);
                }
            }
            throw new Exception("Invalid Item [Item Type : " + type + "]");
        }

        public static OCRStartAssignmentRecord GetOCRStartAssignmentRecord(ref string line)
        {
            try
            {
                OCRStartAssignmentRecord record = new OCRStartAssignmentRecord();
                record.FormatCode = line.Substring(0, 2);
                record.ServiceCode = line.Substring(2, 2);
                record.TaskType = line.Substring(4, 2);
                record.RecordType = line.Substring(6, 2).ToOCRRecordTypes(6);

                if (record.RecordType != OCRRecordTypes.StartRecordAssingment)
                {
                    throw new Exception("Expected record type not found. Expected reocrd type : TransactionAmountItem2 ["
                        + (int)OCRRecordTypes.StartRecordAssingment + "]. Found record type : " + (int)record.RecordType + "");
                }
                record.AgreementID = line.Substring(8, 9);
                record.TaskNumber = line.Substring(17, 7);
                record.RecipientAccountNumber = line.Substring(24, 11);
                return record;
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to read Start Assignment Record. " + ex.Message);
            }
        }
        /// <summary>
        /// Takes lines(most probebly 2 lines) related to transaction and create transaction reocrd.
        /// If there is error when creating transaction OCRTransactionErrorRecord object will be returned. 
        /// </summary>
        /// <param name="lines"></param>
        /// <returns></returns>
        public static OCRTransactionRecord GetOCRTransactionRecord(ref string firstLines, ref string secondLine,
                                                                    int fristLineIndex, int secondLineIndex)
        {

            try
            {
                if (string.IsNullOrEmpty(secondLine))
                {
                    throw new Exception("Record type 31 is not presented");
                }
                OCRTransactionValidRecord record = new OCRTransactionValidRecord();
                record.LineIndexOfFirstPart = fristLineIndex;
                record.OCRTransactionPost1 = GetOCRTransactionAmountItem1(firstLines);
                record.LineIndexOfSecondPart = secondLineIndex;
                record.OCRTransactionPost2 = GetOCRTransactionAmountItem2(secondLine);
                if (!(record.OCRTransactionPost1.TransNumber.Trim()
                    .Equals(record.OCRTransactionPost2.TransNumber.Trim())))
                {
                    throw new Exception("Transaction Number not match with subsequent line. Line No ["
                                    + fristLineIndex + "]" + "Subsequent Line No [" + secondLineIndex + "]");
                }
                return record;
            }
            catch (Exception ex)
            {
                OCRErrorRecord record = new OCRErrorRecord("Failed to read OCRTransactionRecord data [starting line : "
                                        + secondLineIndex + "]. " + ex.Message);
                return record;
            }
        }

        public static OCRTransactionAmountItem2 GetOCRTransactionAmountItem2(string line)
        {
            OCRTransactionAmountItem2 recordPost2 = new OCRTransactionAmountItem2();
            recordPost2.FormatCode = line.Substring(0, 2);
            recordPost2.ServiceCode = line.Substring(2, 2);
            recordPost2.TransType = line.Substring(4, 2);
            recordPost2.RecordType = line.Substring(6, 2).ToOCRRecordTypes(6);

            if (recordPost2.RecordType != OCRRecordTypes.StartTransactionAmountItem2)
            {
                throw new Exception("Expected record type not found. Expected reocrd type : TransactionAmountItem2 ["
                    + (int)OCRRecordTypes.StartTransactionAmountItem2 + "]. Found record type : " + (int)recordPost2.RecordType + "");
            }
            recordPost2.TransNumber = line.Substring(8, 7);
            recordPost2.FromNo = line.Substring(15, 10);
            recordPost2.AgreementID = line.Substring(25, 9);
            recordPost2.Filler = line.Substring(34, 7);
            recordPost2.PaymentDate = getDateString(line.Substring(41, 6));
            recordPost2.DebetAccount = line.Substring(47, 11);
            return recordPost2;
        }

        public static OCRTransactionAmountItem1 GetOCRTransactionAmountItem1(string line)
        {
            OCRTransactionAmountItem1 recordPost1 = new OCRTransactionAmountItem1();
            recordPost1.FormatCode = line.Substring(0, 2);
            recordPost1.ServiceCode = line.Substring(2, 2);
            recordPost1.TransType = line.Substring(4, 2);
            recordPost1.RecordType = line.Substring(6, 2).ToOCRRecordTypes(6);

            if (recordPost1.RecordType != OCRRecordTypes.StartTransactionAmountItem1)
            {
                throw new Exception("Expected record type not found. Expected reocrd type : OCRTransactionAmountItem1 ["
                    + (int)OCRRecordTypes.StartTransactionAmountItem1 + "]. Found record type : " + (int)recordPost1.RecordType + "");
            }
            recordPost1.TransNumber = line.Substring(8, 7);
            recordPost1.TransDate = getDateString(line.Substring(15, 6));
            recordPost1.CentralID = line.Substring(21, 2);
            recordPost1.DayCode = line.Substring(23, 2);
            recordPost1.PartSettlement = line.Substring(25, 1);
            recordPost1.Counter = line.Substring(26, 5);
            recordPost1.Sign = line.Substring(31, 1);
            recordPost1.Amount = line.Substring(32, 17);
            recordPost1.KID = line.Substring(49, 25);
            recordPost1.FullerOrCardType = line.Substring(74, 2);
            return recordPost1;
        }

        public static string getDateString(string str)
        {
            return (str.Substring(0, 2) + "." + str.Substring(2, 2) + ".20" + str.Substring(4, 2)).ToSQLDateFormat(0, "Date", true); ;

        }

        public static OCREndAssignmentRecord GetOCREndAssignmentRecord(string line, int lineIndex)
        {
            try
            {
                OCREndAssignmentRecord record = new OCREndAssignmentRecord();
                record.FormatCode = line.Substring(0, 2);
                record.ServiceCode = line.Substring(2, 2);
                record.TaskType = line.Substring(4, 2);
                record.RecordType = line.Substring(6, 2).ToOCRRecordTypes(6);

                if (record.RecordType != OCRRecordTypes.EndRecordAssignment)
                {
                    throw new Exception("Expected record type not found. Expected reocrd type : OCRTransactionAmountItem1 ["
                        + (int)OCRRecordTypes.EndRecordAssignment + "]. Found record type : " + (int)record.RecordType + "");
                }

                record.NumberOfTrans = line.Substring(8, 8);
                record.NumberOfRecords = line.Substring(16, 8);
                record.TotalAmount = line.Substring(24, 17);
                record.DateOfSettlement = line.Substring(41, 6);
                record.FirstDate = getDateString(line.Substring(47, 6));
                record.LastDate = getDateString(line.Substring(53, 6));
                return record;
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to read End Assignment Record [line : "
                    + lineIndex + "]" + ex.Message);
            }
        }

        public static OCREndTransmisionRecord GetOCREndTransmisionRecord(string line)
        {
            OCREndTransmisionRecord record = new OCREndTransmisionRecord();
            record.FormatCode = line.Substring(0, 2);
            record.ServiceCode = line.Substring(2, 2);
            record.TaskType = line.Substring(4, 2);
            record.RecordType = line.Substring(6, 2).ToOCRRecordTypes(6);

            if (record.RecordType != OCRRecordTypes.EndSending)
            {
                throw new Exception("Expected record type not found. Expected reocrd type : OCRTransactionAmountItem1 ["
                    + (int)OCRRecordTypes.EndSending + "]. Found record type : " + (int)record.RecordType + "");
            }

            record.NumberOfTrans = line.Substring(8, 8);
            record.NumberOfRecords = line.Substring(16, 8);
            record.TotalAmount = line.Substring(24, 17);
            record.DateOfSettlement = getDateString(line.Substring(41, 6));
            return record;
        }
        public static OCRDirectDeductValidRecord GetDirectDeductValidRecord(string line, int lineIndex)
        {
            try
            {
                OCRDirectDeductValidRecord record = new OCRDirectDeductValidRecord();
                record.LineIndexOfFirstPart = lineIndex;
                record.FormatCode = line.Substring(0, 2);
                record.ServiceCode = line.Substring(2, 2);
                record.TaskType = line.Substring(4, 2);
                record.RecordType = line.Substring(6, 2).ToOCRRecordTypes(6);

                if (record.RecordType != OCRRecordTypes.RecordDirectDeduct)
                {
                    throw new Exception("Expected record type not found. Expected reocrd type : OCRTransactionAmountItem1 ["
                        + (int)OCRRecordTypes.EndSending + "]. Found record type : " + (int)record.RecordType + "");
                }
                record.FBOCounter = line.Substring(8, 7);
                record.RegisterType = line.Substring(15, 1);
                record.KID = line.Substring(16, 25);
                record.Warning = line.Substring(41, 1);
                return record;
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to read Direct Deduct Record [line : "
                    + lineIndex + "]" + ex.Message);
            }
        }


        private static List<OCRTransactionValidRecord> GetValidTransRecords(List<OCRTransactionRecord> transRecords)
        {
            List<OCRErrorRecord> errorTransRecords = new List<OCRErrorRecord>();
            List<OCRTransactionValidRecord> validTransRecords = new List<OCRTransactionValidRecord>();
            OCRErrorRecord errorTransRecord;
            bool foundErrorRecordsFlag = false;
            foreach (OCRTransactionRecord transRecord in transRecords)
            {
                if (transRecord is OCRErrorRecord)
                {
                    // Found an error record.
                    // File should not be send.
                    if (!foundErrorRecordsFlag)
                    {

                        foundErrorRecordsFlag = true;
                    }
                    errorTransRecord = (OCRErrorRecord)transRecord;
                    errorTransRecords.Add(errorTransRecord);
                }
                else
                {
                    validTransRecords.Add((OCRTransactionValidRecord)transRecord);
                }
            }
            if (errorTransRecords.Count == 0)
            { // No error record found
                //Should return valid transaction records
                return validTransRecords;
            }
            else
            {
                // There are some error records
                return new List<OCRTransactionValidRecord>();
            }
        }
        private static IUSPNewOrChanged GetNewOrChangedDCbyOCRTransactionValidRecord(OCRDirectDeductValidRecord validTransRecord)
        {
            IUSPNewOrChanged newPayment = null;
            try
            {
                newPayment.FBOCounter = validTransRecord.FBOCounter;
                newPayment.KID = validTransRecord.KID;
                newPayment.RegisterType = validTransRecord.RegisterType;
                newPayment.Warning = validTransRecord.Warning;
            }
            catch (Exception ex)
            {
                throw new Exception("Invalid data in transaction record. Line No : "
                            + validTransRecord.LineIndexOfFirstPart + ". " + ex.Message);
            }
            return newPayment;
        }

        public static List<IUSPPayment> GetPaymentObjects(OCRAssignmentRecord requiredAssignmentRecord, string gymCode)
        {

            List<OCRTransactionRecord> partOfTransactionRecord = new List<OCRTransactionRecord>();
            List<IUSPPayment> paymentList = GetPayments(partOfTransactionRecord, gymCode);

            return paymentList;
        }


        public static OperationResult<IUSPInitialData> GetItemTypeDefinitions(string gymCode)
        {
            var notificationResult = new OperationResult<IUSPInitialData>();
            try
            {
                SortedList recordTypes = OCRFacade.GetRecordTypes(gymCode);
                if (recordTypes.Count == 0)
                {
                    throw new Exception("No record types defined in the database");
                }

                IUSPInitialData initialData = new USPInitialData();
                initialData.ItemTypes = new string[recordTypes.Count][];

                int index = -1;
                foreach (object key in recordTypes.Keys)
                {
                    index++;
                    initialData.ItemTypes[index] = new string[2];
                    initialData.ItemTypes[index][0] = key.ToString();
                    initialData.ItemTypes[index][1] = recordTypes[key].ToString();
                }
                notificationResult.OperationReturnValue = initialData;
            }
            catch (Exception ex)
            {
                notificationResult.ErrorOccured = true;
                notificationResult.CreateMessage("Failed to retrieve Item Type Definitions : " + ex.Message, MessageTypes.ERROR);
            }
            return notificationResult;
        }
    }
}
