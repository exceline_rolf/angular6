﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.GMS.Core.SystemObjects.Enums;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.Economy.BusinessLogic
{
    public class XMLtoHTMLConverter
    {
        String templateHTMLText = String.Empty;
        String resultHTMLText = String.Empty;

        public void SetTemplateHTMLText(String templateHTMLText)
        {
            this.templateHTMLText = templateHTMLText;
        }

        public String GetTemplateHTMLText()
        {
            return templateHTMLText;
        }

        private void SetResultHTMLText(String resultHTMLText)
        {
            this.resultHTMLText = resultHTMLText;
        }

        public String GetResultHTMLText()
        {
            return resultHTMLText;
        }

        public XMLtoHTMLConverter(String inputXML, Enum type, String gymCode = "", bool isCreditNote = false)
        {

            XmlDocument xmldoc = new XmlDocument();

            xmldoc.LoadXml(inputXML);
            XDocument mydoc = XDocument.Parse(inputXML);


            //Declerations
            InvoiceInfo invoiceInfo = new InvoiceInfo();
            var ReplacementDict = new Dictionary<String, String> { };

            switch (type)
            {
                case PdfTemplatesTypes.INVOICE_SIO:
                    //Begin Retriving values

                    var payerName = from x in mydoc.Descendants("Buyer")
                                    select x;
                    payerName = from y in payerName.Descendants("Name")
                                select y;
                    invoiceInfo.PayerName = payerName.FirstOrDefault().Value.ToString();

                    var payerAdress = from x in mydoc.Descendants("Buyer")
                                      select x;
                    payerAdress = from y in payerAdress.Descendants("AddressInfo")
                                  select y;
                    payerAdress = from z in payerAdress.Descendants("Address")
                                  select z;
                    invoiceInfo.PayerAdress = payerAdress.FirstOrDefault().Value.ToString();

                    var payerZipCode = from x in mydoc.Descendants("Buyer")
                                       select x;
                    payerZipCode = from y in payerZipCode.Descendants("AddressInfo")
                                   select y;
                    payerZipCode = from z in payerZipCode.Descendants("ZipCode")
                                   select z;

                    var payerPlace = from x in mydoc.Descendants("Buyer")
                                     select x;
                    payerPlace = from y in payerPlace.Descendants("AddressInfo")
                                 select y;
                    payerPlace = from z in payerPlace.Descendants("Place")
                                 select z;

                    invoiceInfo.PayerPostNumber = payerZipCode.FirstOrDefault().Value.ToString() + " " + payerPlace.FirstOrDefault().Value.ToString();

                    var invoiceNo = from x in mydoc.Descendants("InvoiceNo")
                                    select x;
                    invoiceInfo.InvoiceNumber = invoiceNo.FirstOrDefault().Value.ToString();

                    var paidByDate = from x in mydoc.Descendants("DueDate")
                                     select x;
                    invoiceInfo.PaidByDate = paidByDate.FirstOrDefault().Value.ToString();

                    var invoiceDate = from x in mydoc.Descendants("InvoiceDate")
                                      select x;
                    invoiceInfo.InvoiceDate = invoiceDate.FirstOrDefault().Value.ToString();

                    var customerId = from x in mydoc.Descendants("BuyerNo")
                                     select x;
                    invoiceInfo.CustomerId = customerId.FirstOrDefault().Value.ToString();

                    var toAccountNumber = from x in mydoc.Descendants("AccountNumber")
                                          select x;
                    invoiceInfo.ReceiverAccountNumber = toAccountNumber.FirstOrDefault().Value.ToString();

                    var kID = from x in mydoc.Descendants("BacsId")
                              select x;
                    invoiceInfo.KID = kID.FirstOrDefault().Value.ToString();

                    int numberOfOrderLines = (from x in mydoc.Descendants("LineNo")
                                              select x).Count();

                    for (var t = 0; t < numberOfOrderLines; t++)
                    {
                        InvoiceOrderLine orderline = new InvoiceOrderLine();

                        var description = from x in mydoc.Descendants("SellerProductDescr")
                                          select x;
                        orderline.ArticleDescription = description.ElementAtOrDefault(t).Value.ToString();

                        var quantity = from x in mydoc.Descendants("Quantity")
                                       select x;

                        orderline.NumberOfUnits = quantity.ElementAtOrDefault(t).Value.ToString();

                        var priceOfUnit = from x in mydoc.Descendants("Price")
                                          select x;
                        orderline.UnitPrice = priceOfUnit.ElementAtOrDefault(t).Value.ToString();

                        invoiceInfo.OrderLines.Add(orderline);
                    }


                    var totalInvoiceAmount = from x in mydoc.Descendants("TotalInclTax")
                                             select x;
                    invoiceInfo.TotalInvoiceAmount = totalInvoiceAmount.FirstOrDefault().Value.ToString();

                    SenderInformation sender = new SenderInformation();

                    var senderName = from x in mydoc.Descendants("Seller")
                                     select x;
                    senderName = from y in senderName.Descendants("Name")
                                 select y;

                    sender.SenderName = senderName.FirstOrDefault().Value.ToString();

                    var senderAdress = from x in mydoc.Descendants("Seller")
                                       select x;
                    senderAdress = from y in senderAdress.Descendants("AddressInfo")
                                   select y;
                    senderAdress = from z in senderAdress.Descendants("Address")
                                   select z;

                    var senderZipCode = from x in mydoc.Descendants("Seller")
                                        select x;
                    senderZipCode = from y in senderZipCode.Descendants("AddressInfo")
                                    select y;
                    senderZipCode = from z in senderZipCode.Descendants("ZipCode")
                                    select z;

                    var senderPlace = from x in mydoc.Descendants("Seller")
                                      select x;
                    senderPlace = from y in senderPlace.Descendants("AddressInfo")
                                  select y;
                    senderPlace = from z in senderPlace.Descendants("Place")
                                  select z;


                    sender.SenderAdress = senderAdress.FirstOrDefault().Value.ToString() + " " + senderZipCode.FirstOrDefault().Value.ToString() + " " + senderPlace.FirstOrDefault().Value.ToString();


                    var senderPhoneNumber = from x in mydoc.Descendants("Seller")
                                            select x;
                    senderPhoneNumber = from y in senderPhoneNumber.Descendants("AddressInfo")
                                        select y;
                    senderPhoneNumber = from z in senderPhoneNumber.Descendants("Phone")
                                        select z;

                    sender.SenderPhoneNumber = senderPhoneNumber.FirstOrDefault().Value.ToString();

                    sender.SenderWebSite = "";

                    var swift = from x in mydoc.Descendants("Swift")
                                select x;
                    sender.SenderSwift = swift.FirstOrDefault().Value.ToString();

                    sender.SenderIBAN = "";

                    var senderOrganiztionNumber = from x in mydoc.Descendants("VatRegNo")
                                                  select x;
                    sender.SenderOrganiztionNumber = senderOrganiztionNumber.FirstOrDefault().Value.ToString();

                    invoiceInfo.SenderInfo = sender;


                    //Begin populating the dictionary
                    ReplacementDict.Add("{PayerName}", invoiceInfo.PayerName);
                    ReplacementDict.Add("{PayerAdress}", invoiceInfo.PayerAdress);
                    ReplacementDict.Add("{PayerPostNumber}", invoiceInfo.PayerPostNumber);
                    ReplacementDict.Add("{InvoiceNumber}", invoiceInfo.InvoiceNumber);
                    ReplacementDict.Add("{PaidByDate}", invoiceInfo.PaidByDate);
                    ReplacementDict.Add("{InvoiceDate}", invoiceInfo.InvoiceDate);
                    ReplacementDict.Add("{CustomerId}", invoiceInfo.CustomerId);
                    ReplacementDict.Add("{ReceiverAccountNumber}", invoiceInfo.ReceiverAccountNumber);
                    ReplacementDict.Add("{KID}", invoiceInfo.KID);


                    //Add iterator here to add all OrderLines
                    for (int i = 0; i < numberOfOrderLines; i++)
                    {
                        ReplacementDict.Add("{ArticleDescription" + i.ToString() + "}", invoiceInfo.OrderLines[i].ArticleDescription);
                        ReplacementDict.Add("{NumberOfUnits" + i.ToString() + "}", invoiceInfo.OrderLines[i].NumberOfUnits.ToString());
                        ReplacementDict.Add("{UnitPrice" + i.ToString() + "}", invoiceInfo.OrderLines[i].UnitPrice.ToString());
                        ReplacementDict.Add("{TotalPrice" + i.ToString() + "}", invoiceInfo.OrderLines[i].TotalPrice);
                    }


                    ReplacementDict.Add("{TotalInvoiceAmount}", invoiceInfo.TotalInvoiceAmount.ToString());
                    ReplacementDict.Add("{SenderName}", invoiceInfo.SenderInfo.SenderName);
                    ReplacementDict.Add("{SenderAdress}", invoiceInfo.SenderInfo.SenderAdress);
                    ReplacementDict.Add("{SenderPhoneNumber}", invoiceInfo.SenderInfo.SenderPhoneNumber);
                    ReplacementDict.Add("{SenderWebSite}", invoiceInfo.SenderInfo.SenderWebSite);
                    ReplacementDict.Add("{SenderSwift}", invoiceInfo.SenderInfo.SenderSwift);
                    ReplacementDict.Add("{SenderIBAN}", invoiceInfo.SenderInfo.SenderIBAN);
                    ReplacementDict.Add("{SenderOrganiztionNumber}", invoiceInfo.SenderInfo.SenderOrganiztionNumber);


                    //Initialize inputxml

                    PdfTemplates template = new PdfTemplates(PdfTemplatesTypes.INVOICE_SIO, numberOfOrderLines);

                    SetTemplateHTMLText(template.getHTMLTemplate());
                    break;

                case PdfTemplatesTypes.INVOICE_EXCELINE:

                    var gymId = Data.EconomyFacade.GetGymCompanyId(gymCode);

                     payerName = from x in mydoc.Descendants("CustomerName")
                                    select x;
                    invoiceInfo.PayerName = payerName.FirstOrDefault().Value.ToString();

                     payerAdress = from x in mydoc.Descendants("CustomerAdress")
                                      select x;
                    invoiceInfo.PayerAdress = payerAdress.FirstOrDefault().Value.ToString();

                     payerZipCode = from x in mydoc.Descendants("CustomerZipCode")
                                       select x;

                     payerPlace = from x in mydoc.Descendants("CustomerZipName")
                                     select x;

                    invoiceInfo.PayerPostNumber = payerZipCode.FirstOrDefault().Value.ToString() + " " + payerPlace.FirstOrDefault().Value.ToString();

                     invoiceNo = from x in mydoc.Descendants("InvoiceNumber")
                                    select x;
                    invoiceInfo.InvoiceNumber = invoiceNo.FirstOrDefault().Value.ToString();

                     var atg = from x in mydoc.Descendants("ATGEnabled")
                              select x;
                    invoiceInfo.IsATG = Convert.ToBoolean(atg.FirstOrDefault().Value.ToString());

                     paidByDate = from x in mydoc.Descendants("DueDate")
                                     select x;
                    invoiceInfo.PaidByDate = paidByDate.FirstOrDefault().Value.ToString();

                     invoiceDate = from x in mydoc.Descendants("InvoiceGeneratedDate")
                                      select x;
                    invoiceInfo.InvoiceDate = invoiceDate.FirstOrDefault().Value.ToString();

                     customerId = from x in mydoc.Descendants("CustomerId")
                                     select x;
                    invoiceInfo.CustomerId = customerId.FirstOrDefault().Value.ToString();

                     toAccountNumber = from x in mydoc.Descendants("GymAccountNumber")
                                          select x;
                    invoiceInfo.ReceiverAccountNumber = toAccountNumber.FirstOrDefault().Value.ToString();

                     kID = from x in mydoc.Descendants("KID")
                              select x;
                    invoiceInfo.KID = kID.FirstOrDefault().Value.ToString();

                    var customerRefrence = from x in mydoc.Descendants("CustomerRefrence")
                          select x;
                    invoiceInfo.CustomerRefrence = customerRefrence.FirstOrDefault().Value.ToString();

                    var employeeNo = from x in mydoc.Descendants("EmployeeNo")
                          select x;
                    invoiceInfo.EmployeeNo = employeeNo.FirstOrDefault().Value.ToString();

                    var paidOnBehalfOfCustId = from x in mydoc.Descendants("PaidOnBehalfOfCustId")
                                     select x;
                    invoiceInfo.PaidOnBehalfOfCustId = paidOnBehalfOfCustId.FirstOrDefault().Value.ToString();

                    var paidOnBehalfOfName = from x in mydoc.Descendants("PaidOnBehalfOfName")
                                       select x;
                    invoiceInfo.PaidOnBehalfOfName = paidOnBehalfOfName.FirstOrDefault().Value.ToString();

                    numberOfOrderLines = (from x in mydoc.Descendants("ArticleDescription")
                                              select x).Count();

                    for (var t = 0; t < numberOfOrderLines; t++)
                    {
                        InvoiceOrderLine orderline = new InvoiceOrderLine();

                        var description = from x in mydoc.Descendants("ArticleDescription")
                                          select x;
                        orderline.ArticleDescription = description.ElementAtOrDefault(t).Value.ToString();

                        var quantity = from x in mydoc.Descendants("NumberOfUnits")
                                       select x;

                        orderline.NumberOfUnits = quantity.ElementAtOrDefault(t).Value.ToString();

                        var priceOfUnit = from x in mydoc.Descendants("UnitPrice")
                                          select x;
                        orderline.UnitPrice = priceOfUnit.ElementAtOrDefault(t).Value.ToString();

                        var InvoicePeriod = from x in mydoc.Descendants("InvoicePeriod")
                                            select x;
                        orderline.PeriodSpan = InvoicePeriod.ElementAtOrDefault(t).Value.ToString();

                        var VATRateOfUnit = from x in mydoc.Descendants("VATRate")
                                            select x;
                        orderline.VATRate = Convert.ToDecimal(VATRateOfUnit.ElementAtOrDefault(t).Value.ToString());

                        var Note = from x in mydoc.Descendants("Note")
                                   select x;
                        orderline.Note = Note.ElementAtOrDefault(t).Value.ToString();

                        invoiceInfo.OrderLines.Add(orderline);
                    }


                     totalInvoiceAmount = from x in mydoc.Descendants("TotalAmount")
                                             select x;
                    invoiceInfo.TotalInvoiceAmount = totalInvoiceAmount.FirstOrDefault().Value.ToString();

                     sender = new SenderInformation();

                     senderName = from x in mydoc.Descendants("GymName")
                                     select x;

                    sender.SenderName = senderName.FirstOrDefault().Value.ToString();

                     senderAdress = from x in mydoc.Descendants("GymAdress")
                                       select x;

                     senderZipCode = from x in mydoc.Descendants("GymZipCode")
                                        select x;

                     senderPlace = from x in mydoc.Descendants("GymZipName")
                                      select x;

                    sender.SenderAdress = senderAdress.FirstOrDefault().Value.ToString() + " " + senderZipCode.FirstOrDefault().Value.ToString() + " " + senderPlace.FirstOrDefault().Value.ToString();


                     senderPhoneNumber = from x in mydoc.Descendants("GymPhoneNumber")
                                            select x;

                    sender.SenderPhoneNumber = senderPhoneNumber.FirstOrDefault().Value.ToString();

                    var senderWebSite = from x in mydoc.Descendants("GymWeb")
                                        select x;

                    sender.SenderWebSite = senderWebSite.FirstOrDefault().Value.ToString();

                    //   var swift = from x in mydoc.Descendants("Swift")
                    //     select x;
                    //     sender.SenderSwift = swift.FirstOrDefault().Value.ToString();

                    //  sender.SenderIBAN = "";

                     senderOrganiztionNumber = from x in mydoc.Descendants("GymOrganizationNumber")
                                                  select x;
                    sender.SenderOrganiztionNumber = senderOrganiztionNumber.FirstOrDefault().Value.ToString();

                    invoiceInfo.SenderInfo = sender;

                    //Begin populating the dictionary
                    ReplacementDict.Add("{PayerName}", invoiceInfo.PayerName);
                    ReplacementDict.Add("{PayerAdress}", invoiceInfo.PayerAdress);
                    ReplacementDict.Add("{PayerPostNumber}", invoiceInfo.PayerPostNumber);
                    ReplacementDict.Add("{InvoiceNumber}", invoiceInfo.InvoiceNumber);
                    ReplacementDict.Add("{PaidByDate}", invoiceInfo.PaidByDate);
                    ReplacementDict.Add("{InvoiceDate}", invoiceInfo.InvoiceDate);
                    ReplacementDict.Add("{CustomerId}", invoiceInfo.CustomerId);
                    ReplacementDict.Add("{ReceiverAccountNumber}", invoiceInfo.ReceiverAccountNumber);
                    ReplacementDict.Add("{KID}", invoiceInfo.KID);
                    ReplacementDict.Add("{ATGEnabled}", Convert.ToString(invoiceInfo.IsATG));
                    ReplacementDict.Add("{CustomerRefrence}", Convert.ToString(invoiceInfo.CustomerRefrence));
                    ReplacementDict.Add("{EmployeeNo}", Convert.ToString(invoiceInfo.EmployeeNo));
                    ReplacementDict.Add("{PaidOnBehalfOfCustId}", invoiceInfo.PaidOnBehalfOfCustId);
                    ReplacementDict.Add("{PaidOnBehalfOfName}", invoiceInfo.PaidOnBehalfOfName);
                    double totalSum = 0;
                    //Add iterator here to add all OrderLines
                    for (int i = 0; i < numberOfOrderLines; i++)
                    {
                        ReplacementDict.Add("{ArticleDescription" + i.ToString() + "}", invoiceInfo.OrderLines[i].ArticleDescription);
                        ReplacementDict.Add("{NumberOfUnits" + i.ToString() + "}", invoiceInfo.OrderLines[i].NumberOfUnits);
                        ReplacementDict.Add("{UnitPrice" + i.ToString() + "}", invoiceInfo.OrderLines[i].UnitPrice);
                        ReplacementDict.Add("{PeriodSpan" + i.ToString() + "}", invoiceInfo.OrderLines[i].PeriodSpan);
                        ReplacementDict.Add("{VATRate" + i.ToString() + "}", Convert.ToString(invoiceInfo.OrderLines[i].VATRate));

                        ReplacementDict.Add("{MVASats" + i.ToString() + "}", Convert.ToString(invoiceInfo.OrderLines[i].VATRate) + "%");
                        ReplacementDict.Add("{MVAAmount" + i.ToString() + "}", invoiceInfo.OrderLines[i].VATAddition);
                        ReplacementDict.Add("{UnitPriceNoMVA" + i.ToString() + "}", invoiceInfo.OrderLines[i].UnitPriceNoVAT);
                        ReplacementDict.Add("{TotalPriceNoMVA" + i.ToString() + "}", invoiceInfo.OrderLines[i].TotalPriceNoVAT);
                        ReplacementDict.Add("{TotalPrice" + i.ToString() + "}", invoiceInfo.OrderLines[i].TotalPrice);
                        if (invoiceInfo.OrderLines[i].Note != String.Empty)
                        {
                            ReplacementDict.Add("{Note" + i.ToString() + "}", @"<tr><td width= ""25%"" style= ""padding-left: 0cm"">" + invoiceInfo.OrderLines[i].Note + "</td></tr>");


                        }
                        else
                        {
                            ReplacementDict.Add("{Note" + i.ToString() + "}", "");
                        }
                        totalSum += Convert.ToDouble(invoiceInfo.OrderLines[i].TotalPrice);
                    }


                    ReplacementDict.Add("{TotalInvoiceAmount}", totalSum.ToString("F2"));
                    ReplacementDict.Add("{SenderName}", invoiceInfo.SenderInfo.SenderName);
                    ReplacementDict.Add("{SenderAdress}", invoiceInfo.SenderInfo.SenderAdress);
                    ReplacementDict.Add("{SenderPhoneNumber}", invoiceInfo.SenderInfo.SenderPhoneNumber);
                    ReplacementDict.Add("{SenderWebSite}", invoiceInfo.SenderInfo.SenderWebSite);
                    ReplacementDict.Add("{SenderOrganiztionNumber}", invoiceInfo.SenderInfo.SenderOrganiztionNumber);


                     template = new PdfTemplates(PdfTemplatesTypes.INVOICE_EXCELINE, numberOfOrderLines, invoiceInfo.IsATG, gymId);

                    SetTemplateHTMLText(template.getHTMLTemplate());
                    break;

                    case PdfTemplatesTypes.MEMBERCONTRACT_EXCELINE:

                    gymId = Data.EconomyFacade.GetGymCompanyId(gymCode);

                    MemberContractPrintInfo memberData = new MemberContractPrintInfo();


                    var trainingPeriodeStart = from x in mydoc.Descendants("ContractInfo")
                                    select x;
                    trainingPeriodeStart = from y in trainingPeriodeStart.Descendants("TrainingPeriodeStart")
                                select y;
                    if (trainingPeriodeStart.FirstOrDefault().Value.ToString() == String.Empty || trainingPeriodeStart.FirstOrDefault().Value.ToString() == null)
                    {
                        memberData.StartLockPeriod = trainingPeriodeStart.FirstOrDefault().Value.ToString();
                    }
                    else
                    {
                        memberData.StartLockPeriod = "Fra " + trainingPeriodeStart.FirstOrDefault().Value.ToString();
                    }
                        

                    var trainingPeriodeStop = from x in mydoc.Descendants("ContractInfo")
                                      select x;
                    trainingPeriodeStop = from y in trainingPeriodeStop.Descendants("TrainingPeriodeStop")
                                  select y;
                    if(trainingPeriodeStop.FirstOrDefault().Value.ToString() == String.Empty || trainingPeriodeStop.FirstOrDefault().Value.ToString() == null)
                    {
                        memberData.StopLockPeriod = trainingPeriodeStop.FirstOrDefault().Value.ToString();
                    }
                    else
                    {
                        memberData.StopLockPeriod = " til " + trainingPeriodeStop.FirstOrDefault().Value.ToString();
                    }

                    var startUpPrice = from x in mydoc.Descendants("ContractInfo")
                                       select x;
                    startUpPrice = from y in startUpPrice.Descendants("StartUpPrice")
                                   select y;
                    memberData.StrtUpItemPrice = Convert.ToDouble(startUpPrice.FirstOrDefault().Value);

                    var servicePrice = from x in mydoc.Descendants("ContractInfo")
                                              select x;
                    servicePrice = from y in servicePrice.Descendants("ServicePrice")
                                          select y;
                    memberData.MonthlyServicePrice = servicePrice.FirstOrDefault().Value.ToString();

                    var addonPrice = from x in mydoc.Descendants("ContractInfo")
                                              select x;
                    addonPrice = from y in addonPrice.Descendants("AddonPrice")
                                          select y;
                    memberData.MonthlyAddonsPrice = Convert.ToDouble(addonPrice.FirstOrDefault().Value);

                    var totalPrice = from x in mydoc.Descendants("ContractInfo")
                                              select x;
                    totalPrice = from y in totalPrice.Descendants("TotalPrice")
                                          select y;
                    memberData.TotPriceLockPeriod = Convert.ToDouble(totalPrice.FirstOrDefault().Value.Replace(".",","));

                    var contractSignDate = from x in mydoc.Descendants("ContractInfo")
                                              select x;
                    contractSignDate = from y in contractSignDate.Descendants("ContractSignDate")
                                          select y;
                    memberData.SignedDate = contractSignDate.FirstOrDefault().Value.ToString();

                    var memberContractNo = from x in mydoc.Descendants("ContractInfo")
                                              select x;
                    memberContractNo = from y in memberContractNo.Descendants("MemberContractNo")
                                          select y;
                    memberData.MemberContractNo = memberContractNo.FirstOrDefault().Value.ToString();

                    var templateid = from x in mydoc.Descendants("ContractInfo")
                                     select x;
                    templateid = from y in templateid.Descendants("Template")
                                 select y;
                    memberData.Template = templateid.FirstOrDefault().Value.ToString();

                    var firstName = from x in mydoc.Descendants("CustomerInfo")
                                     select x;
                    firstName = from y in firstName.Descendants("FirstName")
                                 select y;
                    memberData.FirstName = firstName.FirstOrDefault().Value.ToString();

                    var lastName = from x in mydoc.Descendants("CustomerInfo")
                                     select x;
                    lastName = from y in lastName.Descendants("LastName")
                                 select y;
                    memberData.LastName = lastName.FirstOrDefault().Value.ToString();

                    var adress = from x in mydoc.Descendants("CustomerInfo")
                                     select x;
                    adress = from y in adress.Descendants("Adress")
                                 select y;
                    memberData.Address1 = adress.FirstOrDefault().Value.ToString();

                    var zip = from x in mydoc.Descendants("CustomerInfo")
                                     select x;
                    zip = from y in zip.Descendants("Zip")
                                 select y;
                    memberData.PostalAddress = zip.FirstOrDefault().Value.ToString();

                    var mobileNumber = from x in mydoc.Descendants("CustomerInfo")
                                     select x;
                    mobileNumber = from y in mobileNumber.Descendants("MobileNumber")
                                 select y;
                    memberData.TelMobile = mobileNumber.FirstOrDefault().Value.ToString();

                    var privateNumber = from x in mydoc.Descendants("CustomerInfo")
                                     select x;
                    privateNumber = from y in privateNumber.Descendants("PrivateNumber")
                                 select y;
                    memberData.TelPrivate = privateNumber.FirstOrDefault().Value.ToString();
                    
                    var workNumber = from x in mydoc.Descendants("CustomerInfo")
                                     select x;
                    workNumber = from y in workNumber.Descendants("WorkNumber")
                                 select y;
                    memberData.TelWork = workNumber.FirstOrDefault().Value.ToString();

                    var dateOfBirth = from x in mydoc.Descendants("CustomerInfo")
                                     select x;
                    dateOfBirth = from y in dateOfBirth.Descendants("DateOfBirth")
                                 select y;
                    memberData.Birthdate = dateOfBirth.FirstOrDefault().Value.ToString();

                    var memberNo = from x in mydoc.Descendants("CustomerInfo")
                                     select x;
                    memberNo = from y in memberNo.Descendants("MemberNo")
                                 select y;
                    memberData.MemberNo = memberNo.FirstOrDefault().Value.ToString();



                    var branchId = from x in mydoc.Descendants("GymInfo")
                                     select x;
                    branchId = from y in branchId.Descendants("BranchId")
                                 select y;
                    memberData.BranchID = branchId.FirstOrDefault().Value.ToString();

                    var gymAdress = from x in mydoc.Descendants("GymInfo")
                                   select x;
                    gymAdress = from y in gymAdress.Descendants("GymAdress")
                               select y;
                    memberData.GymAddress1 = gymAdress.FirstOrDefault().Value.ToString();

                    var gymName = from x in mydoc.Descendants("GymInfo")
                                   select x;
                    gymName = from y in gymName.Descendants("GymName")
                               select y;
                    memberData.GymName = gymName.FirstOrDefault().Value.ToString();

                    var gymOrgNr = from x in mydoc.Descendants("GymInfo")
                                   select x;
                    gymOrgNr = from y in gymOrgNr.Descendants("GymOrgNr")
                               select y;
                    memberData.GymOrganizationNo = gymOrgNr.FirstOrDefault().Value.ToString();

                    var gymAccountNo = from x in mydoc.Descendants("GymInfo")
                                   select x;
                    gymAccountNo = from y in gymAccountNo.Descendants("GymAccountNo")
                               select y;
                    memberData.GymAccountNo = gymAccountNo.FirstOrDefault().Value.ToString();

                    var gymZip = from x in mydoc.Descendants("GymInfo")
                                   select x;
                    gymZip = from y in gymZip.Descendants("GymZip")
                               select y;
                    memberData.GymPostalAddress = gymZip.FirstOrDefault().Value.ToString();

                    var gymcode = from x in mydoc.Descendants("GymInfo")
                                   select x;
                    gymcode = from y in gymcode.Descendants("GymCode")
                               select y;
                    memberData.GymCode = gymcode.FirstOrDefault().Value.ToString();


                    var numberOfContractItems = (from x in mydoc.Descendants("ContractItems")
                                          select x).Count();

                    List<MonthlyServicesForContract> listOfContractItems = new List<MonthlyServicesForContract>();

                    for (var t = 0; t < numberOfContractItems; t++)
                    {
                        MonthlyServicesForContract serviceData = new MonthlyServicesForContract();

                        var itemDescription = from x in mydoc.Descendants("ItemDescription")
                                          select x;
                        serviceData.ItemName = itemDescription.ElementAtOrDefault(t).Value.ToString();

                        var itemPrice = from x in mydoc.Descendants("ItemPrice")
                                       select x;

                        serviceData.Price = Convert.ToDouble(itemPrice.ElementAtOrDefault(t).Value);

                        var numberOfItems = from x in mydoc.Descendants("NumberOfItems")
                                          select x;
                        serviceData.Units = Convert.ToInt32(numberOfItems.ElementAtOrDefault(t).Value);

                        var startOrder = from x in mydoc.Descendants("StartOrder")
                                            select x;
                        serviceData.StartOrder = Convert.ToInt32(startOrder.ElementAtOrDefault(t).Value);

                        var endOrder = from x in mydoc.Descendants("EndOrder")
                                            select x;
                        serviceData.EndOrder = Convert.ToInt32(endOrder.ElementAtOrDefault(t).Value);

                        listOfContractItems.Add(serviceData);
                    }


                    //Begin populating the dictionary
                    ReplacementDict.Add("{TrainingPeriodeStart}", memberData.StartLockPeriod);
                    ReplacementDict.Add("{TrainingPeriodeStop}", memberData.StopLockPeriod);
                    ReplacementDict.Add("{StartUpPrice}", memberData.StrtUpItemPrice.ToString("F2"));
                    ReplacementDict.Add("{ServicePrice}", memberData.MonthlyServicePrice);
                    ReplacementDict.Add("{AddonPrice}", memberData.MonthlyAddonsPrice.ToString("F2"));
                    ReplacementDict.Add("{TotalPrice}", memberData.TotPriceLockPeriod.ToString("F2"));
                    ReplacementDict.Add("{ContractSignDate}", memberData.SignedDate);
                    ReplacementDict.Add("{MemberContractNo}", memberData.MemberContractNo);
                    ReplacementDict.Add("{Template}", memberData.Template);
                    ReplacementDict.Add("{FirstName}", memberData.FirstName);
                    ReplacementDict.Add("{LastName}", memberData.LastName);
                    ReplacementDict.Add("{Adress}", memberData.Address1);
                    ReplacementDict.Add("{Zip}", memberData.PostalAddress);
                    ReplacementDict.Add("{MobileNumber}", memberData.TelMobile);
                    ReplacementDict.Add("{PrivateNumber}", memberData.TelPrivate);
                    ReplacementDict.Add("{WorkNumber}", memberData.TelWork);
                    ReplacementDict.Add("{DateOfBirth}", memberData.Birthdate);
                    ReplacementDict.Add("{MemberNo}", memberData.MemberNo);
                    ReplacementDict.Add("{BranchId}", memberData.BranchID);
                    ReplacementDict.Add("{GymAdress}", memberData.GymAddress1);
                    ReplacementDict.Add("{GymName}", memberData.GymName);
                    ReplacementDict.Add("{GymOrgNr}", memberData.GymOrganizationNo);
                    ReplacementDict.Add("{GymAccountNo}", memberData.GymAccountNo);
                    ReplacementDict.Add("{GymZip}", memberData.GymPostalAddress);
                    ReplacementDict.Add("{GymCode}", memberData.GymCode);



                   
                    //Add iterator here to add all OrderLines
                    for (int i = 0; i < numberOfContractItems; i++)
                    {
                        ReplacementDict.Add("{ItemDescription" + i.ToString() + "}", listOfContractItems[i].ItemName);
                        ReplacementDict.Add("{ItemPrice" + i.ToString() + "}", listOfContractItems[i].Price.ToString());
                        ReplacementDict.Add("{NumberOfItems" + i.ToString() + "}", listOfContractItems[i].Units.ToString());
                        ReplacementDict.Add("{StartOrder" + i.ToString() + "}", listOfContractItems[i].StartOrder.ToString());
                        ReplacementDict.Add("{EndOrder" + i.ToString() + "}", listOfContractItems[i].EndOrder.ToString());
                    }

                    template = new PdfTemplates(PdfTemplatesTypes.MEMBERCONTRACT_EXCELINE, numberOfContractItems, false, gymId);
                    String withTemplateContractText = template.getHTMLTemplate();

                    SetTemplateHTMLText(template.getHTMLTemplate());

                    break;

            }

            //Insert values in HTML template
            String finishedHTML = GetTemplateHTMLText();
            if (invoiceInfo.CustomerRefrence != String.Empty)
            {
                var ReplaceString = @"<tr><td width= ""56%"" style= ""padding-left: 0cm""></td><td width= ""15%"">Referanse</td><td style = ""margin-left: 12px"">: {CustomerRefrence}</td></tr>";
                finishedHTML = finishedHTML.Replace("<Refrence>", ReplaceString);
            }

            if (invoiceInfo.EmployeeNo != String.Empty)
            {
                var ReplaceString = @"<tr><td width= ""56%"" style= ""padding-left: 0cm""></td><td width= ""15%"">AnsattNo</td><td style = ""margin-left: 12px"">: {EmployeeNo}</td></tr>";
                finishedHTML = finishedHTML.Replace("<EmployeeNo>", ReplaceString);
            }

            if (invoiceInfo.PaidOnBehalfOfCustId != String.Empty)
            {
                var ReplaceString = @"På vegne av kunde {PaidOnBehalfOfCustId}";
                var ReplaceString2 = @"{PaidOnBehalfOfName}";
                finishedHTML = finishedHTML.Replace("<PaidOnBehalfOfCustId>", ReplaceString);
                finishedHTML = finishedHTML.Replace("<PaidOnBehalfOfName>", ReplaceString2);

            }


            if (isCreditNote)
            {
                finishedHTML = finishedHTML.Replace("F A K T U R A", "K R E D I T N O T A");
            }
            foreach (var key in ReplacementDict.Keys)
            {
                finishedHTML = finishedHTML.Replace(key, ReplacementDict[key]);
            }

            SetResultHTMLText(finishedHTML);
            
        
        }
    }
}

