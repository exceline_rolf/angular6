﻿using System;
using System.Collections.Generic;
using System.Linq;
using US.Common.Logging.API;
using US.Common.Notification.API;
using US.Common.Notification.Core.DomainObjects;
using US.Common.Notification.Core.Enums;
using US.Exceline.GMS.Modules.Admin.API.ManageEmployees;
using US.GMS.API;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.Notification;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.Utils;
using US.Payment.Core.Enums;
using US.Payment.Core.ResultNotifications;

namespace US.Common.Notification.Service
{
    public class USNotificationService : IUSNotificationService
    {
        public int AddNotification(USNotificationTree notification)
        {
            USImportResult<int> result = NotificationAPI.AddNotification(notification);

            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.NotificationMessages)
                {
                    if (message.ErrorLevel == ErrorLevels.Error)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), "TestUser");
                    }
                }
                return -1;

            }
            else
            {
                return result.MethodReturnValue;
            }
        }

        public NotificationTemplateText GetNotificationTextByType(NotifyMethodType method, TextTemplateEnum type, string user, int branchId)
        {
            USImportResult<NotificationTemplateText> result = NotificationAPI.GetNotificationTextByType(method, type, user, branchId);

            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.NotificationMessages)
                {
                    if (message.ErrorLevel == ErrorLevels.Error)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                }
                return new NotificationTemplateText();

            }
            else
            {
                return result.MethodReturnValue;
            }
        }

        public bool UpdateNotification(USNotification notification)
        {
            USImportResult<bool> result = NotificationAPI.UpdateNotification(notification);

            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.NotificationMessages)
                {
                    if (message.ErrorLevel == ErrorLevels.Error)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), "TestUser");
                    }
                }
                return false;

            }
            else
            {
                return result.MethodReturnValue;
            }
        }

        public int SaveNotificationTextTemplate(int entityId, NotificationTemplateText templatetText, int branchId, string loggedUser)
        {

            USImportResult<int> result = NotificationAPI.SaveNotificationTextTemplate(entityId, templatetText, branchId, loggedUser);

            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.NotificationMessages)
                {
                    if (message.ErrorLevel == ErrorLevels.Error)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), loggedUser);
                    }
                }
                return -1;

            }
            else
            {
                return result.MethodReturnValue;
            }
        }

        public List<USNotificationSummaryInfo> GetNotificationByNotifiyMethod(int memberId, string user, DateTime fromDate, DateTime toDate, NotifyMethodType notifyMethod, int branchId)
        {
            USImportResult<List<USNotificationSummaryInfo>> result = NotificationAPI.GetNotificationByNotifiyMethod(memberId, user, branchId, fromDate, toDate, notifyMethod);

            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.NotificationMessages)
                {
                    if (message.ErrorLevel == ErrorLevels.Error)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                }
                return new List<USNotificationSummaryInfo>();

            }
            else
            {
                return result.MethodReturnValue;
            }
        }

        public bool UpdateNotificationStatus(List<int> selectedIds, int status, string user)
        {
            USImportResult<bool> result = NotificationAPI.UpdateNotificationStatus(selectedIds, status, user);

            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.NotificationMessages)
                {
                    if (message.ErrorLevel == ErrorLevels.Error)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                }
                return false;

            }
            else
            {
                return result.MethodReturnValue;
            }
        }

        public TemplateField GetTemplateFieldValue()
        {
            return new TemplateField();
        }

        public TemplateFieldEnum GetEnumValue()
        {
            return new TemplateFieldEnum();
        }

        public NotifyMethodType GetMethodEnumValue()
        {
            return new NotifyMethodType();
        }

        public TextTemplateEnum GetTemplateTextEnumValue()
        {
            return new TextTemplateEnum();
        }

        public List<USNotificationSummaryInfo> GetNotificationSummaryList(int branchId,NotificationStatusEnum status, string user)
        {
            USImportResult<List<USNotificationSummaryInfo>> result = NotificationAPI.GetNotificationSummaryList(branchId,status, user);

            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.NotificationMessages)
                {
                    if (message.ErrorLevel == ErrorLevels.Error)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), "TestUser");
                    }
                }
                return new List<USNotificationSummaryInfo>();

            }
            else
            {
                return result.MethodReturnValue;
            }
        }

        public List<USNotificationSummaryInfo> GetNotificationSearchResult(int branchId,string user, string title, string assignTo, DateTime? receiveDateFrom, DateTime? receiveDateTo, DateTime? dueDateFrom, DateTime? dueDateTo, List<int> severityIdList, List<int> typeIdList, int statusId)
        {
            USImportResult<List<USNotificationSummaryInfo>> result = NotificationAPI.GetNotificationSearchResult( branchId,user, title, assignTo, receiveDateFrom, receiveDateTo, dueDateFrom, dueDateTo, severityIdList, typeIdList, statusId);

            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.NotificationMessages)
                {
                    if (message.ErrorLevel == ErrorLevels.Error)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), "TestUser");
                    }
                }
                return new List<USNotificationSummaryInfo>();

            }
            else
            {
                return result.MethodReturnValue;
            }
        }

        public List<NotificationTemplateText> GetNotificationTemplateText(string methodCode, string typeCode, string user)
        {
            USImportResult<List<NotificationTemplateText>> result = NotificationAPI.GetNotificationTemplateText(methodCode, typeCode, ExceConnectionManager.GetGymCode(user));

            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.NotificationMessages)
                {
                    if (message.ErrorLevel == ErrorLevels.Error)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), "TestUser");
                    }
                }
                return new List<NotificationTemplateText>();

            }
            else
            {
                return result.MethodReturnValue;
            }
        }

        public USNotificationSummaryInfo GetNotificationByIdAction(int notificationId, string user)
        {
            USImportResult<USNotificationSummaryInfo> result = NotificationAPI.GetNotificationByIdAction(notificationId, user);

            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.NotificationMessages)
                {
                    if (message.ErrorLevel == ErrorLevels.Error)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), "TestUser");
                    }
                }
                return new USNotificationSummaryInfo();

            }
            else
            {
                return result.MethodReturnValue;
            }
        }

        public List<CategoryDC> GetCategories(string type, string user, int branchId)
        {
            OperationResult<List<CategoryDC>> result = GMSCategory.GetCategoriesByType(type, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<CategoryDC>(); ;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<CategoryDC> GetTemplateTextKeyByEntity(string type, int entity, string user)
        {
            OperationResult<List<CategoryDC>> result = NotificationAPI.GetTemplateTextKeyByEntity(type, entity, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<CategoryDC>(); ;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }
        
        public List<EntityDC> GetEntityList(string user)
        {
            var result = NotificationAPI.GetEntityList(ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (var message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<EntityDC>(); ;
            }
            return result.OperationReturnValue;
        }

        public List<USNotificationSummaryInfo> GetChangeLogNotificationList(string user, int branchId, DateTime fromDate, DateTime toDate, NotifyMethodType notifyMethod)
        {
            USImportResult<List<USNotificationSummaryInfo>> result = NotificationAPI.GetChangeLogNotificationList(user, branchId, fromDate, toDate, notifyMethod);

            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.NotificationMessages)
                {
                    if (message.ErrorLevel == ErrorLevels.Error)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                    }
                }
                return new List<USNotificationSummaryInfo>();

            }
            else
            {
                return result.MethodReturnValue;
            }
        }


        public List<InstructorDC> GetInstructors(int branchId, string searchText, string user, bool isActive)
        {
            var instructorId = -1;
            var result = GMSInstructor.GetInstructors(branchId, searchText, isActive, instructorId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<InstructorDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        bool IUSNotificationService.AddNotificationWithReportScheduler(string gymCode, string user)
        {
            return AddNotificationWithReportScheduler(gymCode, user);
        }


        public static bool AddNotificationWithReportScheduler(string gymCode, string user)
        {
            OperationResult<bool> result = NotificationAPI.AddNotificationWithReportScheduler(gymCode, user);
            if (result.ErrorOccured)
            {
               
                return false;
            }
            return true;
        }


        public bool UpdateNotificationWithReceiveSMS(string gymCompanyRef, string notificationCode, string message, string sender, string user)
        {
            try
            {
                return NotificationAPI.UpdateNotificationWithReceiveSMS(gymCompanyRef, notificationCode, message, sender, user);
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile(ex.Message, ex, user);
                return false;
            }
        }

        public int SaveCategory(CategoryDC category, string user, int branchId)
        {
            OperationResult<int> result = GMSCategory.SaveCategory(category, user, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<CategoryTypeDC> GetCategoryTypes(string code, string user, int branchId)
        {
            OperationResult<List<CategoryTypeDC>> result = GMSCategory.GetCategoryTypes(code, user, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<CategoryTypeDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<AccessDeniedLog> GetAccessDeniedList(DateTime fromDate, DateTime toDate, string user, int branchId)
        {
            USImportResult<List<AccessDeniedLog>> result = NotificationAPI.GetAccessDeniedList(fromDate, toDate, branchId, ExceConnectionManager.GetGymCode(user));
            if (!result.ErrorOccured)
            {
                return result.MethodReturnValue;
            }
            foreach (var message in result.NotificationMessages.Where(message => message.ErrorLevel == ErrorLevels.Error))
            {
                USLogError.WriteToFile(message.Message, new Exception(), user);
            }
            return new List<AccessDeniedLog>();
        }
    }
}
