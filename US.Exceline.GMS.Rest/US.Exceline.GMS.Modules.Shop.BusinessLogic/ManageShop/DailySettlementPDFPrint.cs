﻿using System;
using System.Text;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.SystemObjects.Enums;
using System.Collections.Generic;
using System.IO;
using TheArtOfDev.HtmlRenderer.PdfSharp;
using System.Linq;
using System.Xml.Linq;
using PdfSharp.Pdf.IO;


namespace US.Exceline.GMS.Modules.Shop.BusinessLogic.ManageShop
{
    public class DailySettlementPDFPrint
    {

        static public String XMLData = String.Empty;
        static public String output;
        static public Byte[] result;


        static public String Getoutput()
        {
            return output;
        }

        static public void Setoutput(String input)
        {
            output = input;
        }

        public Byte[] GetResult()
        {
            return result;
        }

        static public void SetResult(Byte[] input)
        {
            result = input;
        }

        public String FillMeFrontAndBehind(String Input, int amount = 34)
        {
            String result = Input;
            String space = "&nbsp;";
            int inputLength = Input.Length;
            for (int i = inputLength; i <= amount; i++)
            {
                space += "&nbsp;";
            }
            return space + result + space;
        }

        public DailySettlementPDFPrint(String dailySettlementId, String reconiliationId , String SalePointId , String branchId, String mode, String startDate, String endDate, String gymCode, String countedCashDraft)
        {

            /* ------------Initialize variables and get print data -----------------------*/

            String ReconcilliationText = String.Empty;
            DailySettlementPrintData printData =  ShopManager.ExcelineGetDailySettlementDataForPrint( dailySettlementId,  reconiliationId,  SalePointId,  branchId,  mode,  startDate,  endDate,  gymCode,  countedCashDraft);
            HelpDataForDailySettlmentPrintData helpData = ShopManager.ExcelineGetHelpDataForDailySettlmentPrintData(branchId, printData.FromDateTime , printData.ToDateTime, SalePointId, gymCode);
            if(mode == "PRINT") {
                ReconcilliationText = ShopManager.ExcelineGetReconcilliationTextForDailySettlementPrint(Convert.ToInt32(dailySettlementId), gymCode);
            }
            /* --------------------------------------------------------------------------- */

            /* ----------------Initialize Replacement Dictionary ------------------------- */

            var ReplacementDict = new Dictionary<String, String> { };
            ReplacementDict.Add("{GymName}", printData.GymName);
            ReplacementDict.Add("{CreatedUser}", printData.CreatedUser);
            ReplacementDict.Add("{DailySettlementID}", printData.DailySettlementID);
            ReplacementDict.Add("{FromDateTime}", printData.FromDateTime);
            ReplacementDict.Add("{ToDateTime}", printData.ToDateTime);
            ReplacementDict.Add("{ActivityTurnOver}", printData.ActivityTurnOver);
            ReplacementDict.Add("{ShopTurnOver}", printData.ShopTurnOver);
            ReplacementDict.Add("{GiftCardTurnOver}", printData.GiftCardTurnOver);
            ReplacementDict.Add("{OnAccountTurnOver}", printData.OnAccountTurnOver);
            ReplacementDict.Add("{TotalTurnOver}", Convert.ToDouble(printData.TotalTurnOver).ToString("F2"));
            ReplacementDict.Add("{Cash}", printData.Cash);
            ReplacementDict.Add("{BankTerminal}", printData.BankTerminal);
            ReplacementDict.Add("{GiftCard}", printData.GiftCard);
            ReplacementDict.Add("{OnAccount}", printData.OnAccount);
            ReplacementDict.Add("{NextOrder}", printData.NextOrder);
            ReplacementDict.Add("{Invoice}", printData.Invoice);
            ReplacementDict.Add("{SMSEmailInvoice}", printData.SMSEmailInvoice);
            ReplacementDict.Add("{PrePaid}", printData.PrePaid);
            ReplacementDict.Add("{TotalReceived}", printData.TotalReceived);
            ReplacementDict.Add("{CountedCash}",  Convert.ToDouble(printData.CountedCash).ToString("F2"));
            ReplacementDict.Add("{Exchange}", printData.Exchange);
            ReplacementDict.Add("{CashInOut}", printData.CashInOut);
            ReplacementDict.Add("{PaidOut}", printData.PaidOut);
            ReplacementDict.Add("{Deviation}", printData.Deviation);
            ReplacementDict.Add("{ToBank}", printData.ToBank);
            ReplacementDict.Add("{ShopReturn}", printData.ShopReturn);
            ReplacementDict.Add("{CashWithdrawal}", printData.CashWithdrawal);
            ReplacementDict.Add("{PettyCash}", printData.PettyCash);
            ReplacementDict.Add("{TotalPayments}", printData.TotalPayments);
            ReplacementDict.Add("{SalePointId}", SalePointId);
            ReplacementDict.Add("{CashIn}", printData.CashIn);
            ReplacementDict.Add("{CashOut}", printData.CashOut);
            ReplacementDict.Add("{TotalInvoice}", printData.TotalInvoice);
            ReplacementDict.Add("{CashReturn}", printData.CashReturn);
            ReplacementDict.Add("{ReconciliationText}", printData.ReconciliationText);
            ReplacementDict.Add("{IsReconciled}", printData.IsReconciled);
            ReplacementDict.Add("{ID}", printData.ID);
            ReplacementDict.Add("{DocType}", printData.DocType);
            ReplacementDict.Add("{GymCode}", printData.GymCode);
            ReplacementDict.Add("{BranchId}", printData.BranchId);
            ReplacementDict.Add("{NrReceipts}", helpData.ReceiptsForPeriode.NumberOfReceipts.ToString());
            ReplacementDict.Add("{AmountReceipts}", helpData.ReceiptsForPeriode.ReceiptAmount.ToString("F2"));
            ReplacementDict.Add("{NrCopys}", helpData.ReceiptsForPeriode.NumberOfCopys.ToString());
            ReplacementDict.Add("{AmountCopys}", helpData.ReceiptsForPeriode.CopyAmount.ToString("F2"));
            ReplacementDict.Add("{NrReturns}", helpData.ReceiptsForPeriode.NumberOfReturns.ToString());
            ReplacementDict.Add("{AmountReturns}", helpData.ReceiptsForPeriode.ReturnAmount.ToString("F2"));
            ReplacementDict.Add("{TotReturns}",  (Convert.ToDouble(printData.ShopReturn) +  Convert.ToDouble(printData.CashWithdrawal)).ToString("F2"));
            ReplacementDict.Add("{NettoAmount}",  helpData.NettoAmount.ToString("F2"));
            ReplacementDict.Add("{DiscountAmount}", helpData.DiscountedAmount.ToString("F2"));
            ReplacementDict.Add("{NumberOfDiscounts}", helpData.NumberOfDiscounts.ToString());
            ReplacementDict.Add("{NumberOfOpenCashDrawer}", helpData.NumberOfOpenCashDrawer.ToString());
            ReplacementDict.Add("{Mode}", mode =="DRAFT"?"Z-Rapport": "X-Rapport");
            ReplacementDict.Add("{OrgNo}", printData.OrgNo);


            //MVA-Data
            int numberOfMVAEntries = 0;
            if (helpData.PerMVACode.Count > 0) { 
            for (numberOfMVAEntries = 0; numberOfMVAEntries < helpData.PerMVACode.Count; numberOfMVAEntries++) { 
            ReplacementDict.Add("{MVA_ItemsSold_" + numberOfMVAEntries + 1 + "}", helpData.PerMVACode[numberOfMVAEntries].ItemsSold);
            ReplacementDict.Add("{MVA_MVASats_" + numberOfMVAEntries + 1 + "}", helpData.PerMVACode[numberOfMVAEntries].MVASats);
            ReplacementDict.Add("{MVA_TotalAmount_" + numberOfMVAEntries + 1 + "}", helpData.PerMVACode[numberOfMVAEntries].TotalAmount);
            }

            }
            //Account-Data
            int numberOfAccountEntries = 0;
            decimal sum = 0;
            if (helpData.PerAccount.Count > 0) { 
            for (numberOfAccountEntries = 0; numberOfAccountEntries < helpData.PerAccount.Count; numberOfAccountEntries++)
            {
                ReplacementDict.Add("{Account_AccountNo_" + numberOfAccountEntries + 1 + "}", helpData.PerAccount[numberOfAccountEntries].AccountNo);
                ReplacementDict.Add("{Account_AccountName_" + numberOfAccountEntries + 1 + "}", helpData.PerAccount[numberOfAccountEntries].AccountName);
                ReplacementDict.Add("{Account_DebitOrKredit_" + numberOfAccountEntries + 1 + "}", "" + helpData.PerAccount[numberOfAccountEntries].DebitOrKredit);
                ReplacementDict.Add("{Account_ItemsSold_" + numberOfAccountEntries + 1 + "}", helpData.PerAccount[numberOfAccountEntries].ItemsSold);
                ReplacementDict.Add("{Account_TotalAmount_" + numberOfAccountEntries + 1 + "}", helpData.PerAccount[numberOfAccountEntries].TotalAmount);
                ReplacementDict.Add("{Account_Transaction_" + numberOfAccountEntries + 1 + "}", helpData.PerAccount[numberOfAccountEntries].Transaction);
                    sum += Convert.ToDecimal(helpData.PerAccount[numberOfAccountEntries].TotalAmount);
                }
                ReplacementDict.Add("{TotalAccountSum}", sum.ToString());
            }
            else { ReplacementDict.Add("{TotalAccountSum}", 0.ToString("F2")); }
            //PaymentType-Data
            int numberOfPaymentTypeEntries = 0;
            if (helpData.PerPaymentType.Count> 0) { 
            for (numberOfPaymentTypeEntries = 0; numberOfPaymentTypeEntries < helpData.PerPaymentType.Count; numberOfPaymentTypeEntries++)
            {
                ReplacementDict.Add("{PaymentType_PaymentType_" + numberOfPaymentTypeEntries + 1 + "}", helpData.PerPaymentType[numberOfPaymentTypeEntries].PaymentType);
                ReplacementDict.Add("{PaymentType_Transactions_" + numberOfPaymentTypeEntries + 1 + "}", helpData.PerPaymentType[numberOfPaymentTypeEntries].Transactions);
                ReplacementDict.Add("{PaymentType_TotalAmount_" + numberOfPaymentTypeEntries + 1 + "}", helpData.PerPaymentType[numberOfPaymentTypeEntries].TotalAmount);
                ReplacementDict.Add("{PaymentType_DebitOrKredit_" + numberOfPaymentTypeEntries + 1 + "}", "" + helpData.PerPaymentType[numberOfPaymentTypeEntries].DebitOrKredit);
            }
            }
            /* Get template text to fill */


            String template = new PdfTemplates(PdfTemplatesTypes.DAILYSETTLEMENT_EXCELINE,0,false,0).getHTMLTemplate();

            String replacementString = "";
            if (helpData.PerAccount.Count > 0) { 
            for (numberOfAccountEntries = 0; numberOfAccountEntries < helpData.PerAccount.Count; numberOfAccountEntries++)
            {
                replacementString += "<tr>";
                replacementString += @"<td width= ""11%""></td>";
                replacementString += @"<td width= ""30%"">{Account_AccountName_" + numberOfAccountEntries + 1 + "}</td>";
                replacementString += @"<td width= ""15%"">{Account_AccountNo_" + numberOfAccountEntries + 1 + "}</td>";
                replacementString += @"<td width= ""8%"">{Account_Transaction_" + numberOfAccountEntries + 1 + "}</td>";
                replacementString += @"<td width= ""32%"">{Account_TotalAmount_" + numberOfAccountEntries + 1 + "}</td>";
                replacementString += @"<td width= ""4%"">{Account_DebitOrKredit_" + numberOfAccountEntries + 1 + "}</td>";
                replacementString += "</tr>";
            }

            template = template.Replace("<AccountData>", replacementString);
            }
            else
            {
                template = template.Replace("<AccountData>", "");
            }
            replacementString = "";
            if (helpData.PerMVACode.Count > 0) { 
            for (numberOfMVAEntries = 0; numberOfMVAEntries < helpData.PerMVACode.Count; numberOfMVAEntries++)
            {
                replacementString += "<tr>";
                replacementString += @"<td width= ""11%""></td>";
                replacementString += @"<td width= ""30%"" style = ""font-style: italic;"">Salg sats - {MVA_MVASats_" + numberOfMVAEntries + 1 + "}</td>";
                replacementString += @"<td width= ""15%""></td>";
                replacementString += @"<td width= ""8%"" style = ""font-style: italic;"">{MVA_ItemsSold_" + numberOfMVAEntries + 1 + "}</td>";
                replacementString += @"<td width= ""32%"" style = ""font-style: italic;"">{MVA_TotalAmount_" + numberOfMVAEntries + 1 + "}</td>";
                replacementString += @"<td width= ""4%""></td>";
                replacementString += "</tr>";
            }

            template = template.Replace("<MVAData>", replacementString);
            }
            else
            {
                template = template.Replace("<MVAData>", "");
            }
            replacementString = "";
            if (helpData.PerPaymentType.Count > 0) { 
            for (numberOfPaymentTypeEntries = 0; numberOfPaymentTypeEntries < helpData.PerPaymentType.Count; numberOfPaymentTypeEntries++)
            {
                replacementString += "<tr>";
                replacementString += @"<td width= ""11%""></td>";
                replacementString += @"<td width= ""30%"">{PaymentType_PaymentType_" + numberOfPaymentTypeEntries + 1 + "}</td>";
                replacementString += @"<td width= ""15%""></td>";
                replacementString += @"<td width= ""8%"">{PaymentType_Transactions_" + numberOfPaymentTypeEntries + 1 + "}</td>";
                replacementString += @"<td width= ""32%"">{PaymentType_TotalAmount_" + numberOfPaymentTypeEntries + 1 + "}</td>";
                replacementString += @"<td width= ""4%"">{PaymentType_DebitOrKredit_" + numberOfPaymentTypeEntries + 1 + "}</td>";
                replacementString += "</tr>";
            }


            template = template.Replace("<PaymentTypeData>", replacementString);
            }
            else
            {
                template = template.Replace("<PaymentTypeData>", "");
            }
            foreach (var key in ReplacementDict.Keys)
            {
                template = template.Replace(key, ReplacementDict[key]);
            }

            if(mode == "PRINT") {
                ReconcilliationText = ReconcilliationText.Replace("\n", "<br>");
            template += "<br>" + ReconcilliationText;
            
            }
            Setoutput(template);

            using (MemoryStream ms = new MemoryStream())
            {
                var pdf = PdfGenerator.GeneratePdf(Getoutput(), PdfSharp.PageSize.A4);
                pdf.Save(ms);
                SetResult(ms.ToArray());

            }

        }

    }
}
