﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using US.GMS.API.ManageSystemSettings;
using US.GMS.Core.DomainObjects.Admin;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US.GMS.Core.DomainObjects.Notification;
using US.GMS.Core.ResultNotifications;
using US.Common.USSAdmin.API;
using US.Common.Logging.API;
using US.GMS.Core.SystemObjects;
using US.Payment.Core.ResultNotifications;
using US.Exceline.GMS.Modules.Login.API;
using US.GMS.Core.DomainObjects.Login;
using US.GMS.Core.Utils;
using US.Common.Web.UI.Core.SystemObjects;
using SessionManager;
using System.Threading;
using US.GMS.API;
using US.Common.Notification.Core.DomainObjects;
using US.Common.Notification.Core.Enums;
using US.Payment.Core.Enums;
using US.Common.Notification.API;
using System.Configuration;
using US.Communication.API;
using US.Communication.Core.SystemCore.DomainObjects;
using System.Linq;


namespace US.Exceline.GMS.Modules.Login.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession)]
    public class ExcelineLoginService : IExcelineLoginService
    {
        public List<ExceUserBranchDC> GetUserSelectedBranches(string userName, string user)
        {
            OperationResultValue<List<ExceUserBranchDC>> result = ManageExcelineLogin.GetUserSelectedBranches(userName, user);
            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExceUserBranchDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int SaveNotification(NotificationDC notification, string createdUser, int branchId)
        {
            OperationResult<int> result = new OperationResult<int>();
            // result = GMSNotification.SaveNotification(notification, createdUser, branchId, ExceConnectionManager.GetGymCode(createdUser));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), createdUser);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool EditUSPUserBasicInfo(bool isPasswdChange, ExecUserDC uspUser, string loggeduser)
        {
            US.Common.Web.UI.Core.SystemObjects.USPUser user = new US.Common.Web.UI.Core.SystemObjects.USPUser();
            user.Id = uspUser.UserID;
            user.DisplayName = uspUser.DisplayName;
            user.email = uspUser.Email;
            user.passowrd = uspUser.Password;
            user.IsLoginFirstTime = uspUser.IsLoginFirstTime;

            OperationResultValue<bool> result = AdminUserSettings.EditUSPUserBasicInfo(isPasswdChange, user, loggeduser);
            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), loggeduser);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public string ValidateUser(string userName, string password)
        {
            OperationResultValue<string> result = AdminUserSettings.ValidateUser(userName, password);
            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), userName);
                }
                return "0";
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<string> GetVersionNoList(string userName)
        {
            OperationResultValue<List<string>> result = AdminUserSettings.GetVersionNoList(ExceConnectionManager.GetGymCode(userName));
            if(result.ErrorOccured)
            {
                foreach(var message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), userName);
                }
                return new List<string>();
            }
            return result.OperationReturnValue;
        }

        public USPUser ValidateUserWithCard(string cardNumber, string user)
        {
            OperationResultValue<US.Common.Web.UI.Core.SystemObjects.USPUser> result = AdminUserSettings.ValidateUserWithCard(cardNumber, user);
            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.Notifications)
                {
                    //File.AppendAllText("D:\\ExecLineLog.txt", message.Message);
                    USLogError.WriteToFile(message.Message, new Exception(), cardNumber);
                }
                return new USPUser();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        public List<ExecUserDC> GetLoggedUserDetails(string userName, string key)
        {
            OperationResultValue<List<ExecUserDC>> result = ManageExcelineLogin.GetLoggedUserDetails(userName);
            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), userName);
                }
                return new List<ExecUserDC>();
            }
            else
            {
                result.OperationReturnValue[0].MachineName = Session.GetSession(key, ExceConnectionManager.GetGymCode(userName));

                return result.OperationReturnValue;
            }
        }

        public string GetGymSettings(int branchId, string user, GymSettingType gymSettingType)
        {
            OperationResult<string> result = GMSManageGymSetting.GetGymSettings(branchId, ExceConnectionManager.GetGymCode(user), gymSettingType, user);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public void DummyMethodForPassDomainObject(ExceGymSettingDC gymSetting)
        {
        }

        public bool AddEditHardwareProfileId(int userId, int hardwareId, string user)
        {
            OperationResultValue<bool> result = ManageExcelineLogin.AddEditHardwareProfileId(userId, hardwareId, user);
            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public string GetSesstionValue(string user, string sessionKey)
        {
            string sessiovalue = string.Empty;
            try
            {
                sessiovalue = Session.GetSession(sessionKey, ExceConnectionManager.GetGymCode(user));
                if (string.IsNullOrEmpty(sessiovalue))
                {
                    for (int i = 0; i < 5; i++)
                    {
                        sessiovalue = Session.GetSession(sessionKey, ExceConnectionManager.GetGymCode(user));
                        if (!string.IsNullOrEmpty(sessiovalue))
                            break;
                        else
                            Thread.Sleep(1000);
                    }
                }
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile(ex.Message, new Exception(), user);
            }
            return sessiovalue;
        }

        #region USC

        public  int SendNewPassword(string employeeName,string template)
        {
                int resStatus = 0;     
                ExecUserDC excelineUser = new ExecUserDC();
                OperationResultValue<List<ExecUserDC>> userResult =  ManageExcelineLogin.GetLoggedUserDetails(employeeName);
                excelineUser = userResult.OperationReturnValue.FirstOrDefault<ExecUserDC>();
                if (userResult.OperationReturnValue.Count() < 1)
                {
                    resStatus = 1;   //User is not in the system
                    return resStatus;
                }
                else
                {
                        excelineUser = userResult.OperationReturnValue.FirstOrDefault<ExecUserDC>();
                        if(string.IsNullOrEmpty(excelineUser.Email))
                        {
                            resStatus = 2;  //No email is associated with that User Name
                            return resStatus;
                        }
                        else
                        {
                            string temporypw = CreateRandomPassword(8);

                            Dictionary<TemplateFieldEnum, string> paraList = new Dictionary<TemplateFieldEnum, string>();
                            paraList.Add(TemplateFieldEnum.PASSWORD, temporypw);

                            // create common notification
                            USCommonNotificationDC commonNotification = new USCommonNotificationDC();
                            commonNotification.IsTemplateNotification = true;
                            commonNotification.ParameterString = paraList;
                            commonNotification.CreatedUser = excelineUser.UserName;
                            commonNotification.Role = "EMP";
                            commonNotification.NotificationType = NotificationTypeEnum.Messages;
                            commonNotification.Severity = NotificationSeverityEnum.Minor;
                            commonNotification.MemberID = excelineUser.EmployeeId; 
                            commonNotification.Status = NotificationStatusEnum.New;
                            commonNotification.Type = TextTemplateEnum.NEWPASSWORDFOREXCELINE;
                            commonNotification.Method = NotificationMethodType.EMAIL;
                            commonNotification.Title = "NEW PASSWORD FOR EXCELINE";
                            commonNotification.SenderDescription = excelineUser.Email;
                            commonNotification.BranchID = 1;
                            //add EMAIL notification into notification tables
                            USImportResult<int> result = NotificationAPI.AddNotification(commonNotification);

                            if (result.ErrorOccured)
                            {
                                foreach (USNotificationMessage message in result.NotificationMessages)
                                {
                                    if (message.ErrorLevel == ErrorLevels.Error)
                                    {
                                        USLogError.WriteToFile(message.Message, new Exception(), excelineUser.UserName);
                                    }
                                }
                                
                            }
                            else
                            {
                                string application = ConfigurationManager.AppSettings["USC_Notification_Email"].ToString();
                                string outputFormat = "EMAIL";

                                try
                                {
                                    US.Common.Web.UI.Core.SystemObjects.USPUser uspUser = new US.Common.Web.UI.Core.SystemObjects.USPUser();
                                    uspUser.Id = excelineUser.UserID;
                                    uspUser.passowrd = temporypw;
                                    uspUser.email = excelineUser.Email;
                                    uspUser.IsLoginFirstTime = excelineUser.IsLoginFirstTime;
                                    uspUser.DisplayName = excelineUser.DisplayName;                                  

                                    OperationResultValue<bool> stPWRes = AdminUserSettings.EditUSPUserBasicInfo(true, uspUser, excelineUser.UserName);
                                    
                                    if (stPWRes.ErrorOccured)
                                    {
                                        foreach (USNotificationMessage message in stPWRes.Notifications)
                                        {
                                            USLogError.WriteToFile(message.Message, new Exception(), excelineUser.UserName);
                                        }
                                        resStatus = 3; //Error occured while reseting password
                                        return resStatus;
                                    }
                                    if (stPWRes.OperationReturnValue == false)
                                    {
                                        resStatus = 3; //Error occured while reseting password
                                        return resStatus;
                                    }
                                    else
                                    {                                         
                                        // send EMAIL through the USC API
                                        IProcessingStatus emailStatus = USCAPI.ExecuteTemplate(application, outputFormat, ExceConnectionManager.GetGymCode(excelineUser.UserName), result.MethodReturnValue.ToString());
                                        NotificationStatusEnum status = NotificationStatusEnum.Attended;

                                        if(emailStatus != null)
                                            status = (emailStatus.Messages.FirstOrDefault(X => X.Header == "Status").Message.ToString() == "1") ? NotificationStatusEnum.Success : NotificationStatusEnum.Fail;
                                        var statusMessage = emailStatus.Messages.FirstOrDefault(x => x.Header == "StatusMessage").Message;
                                        // update status in notification table
                                        USImportResult<bool> updateResult = NotificationAPI.UpdateNotificationStatus(result.MethodReturnValue, excelineUser.UserName, status, statusMessage);

                                        if(emailStatus == null)
                                        {
                                            resStatus = 4;
                                            return resStatus;
                                        }

                                        if (updateResult.ErrorOccured)
                                        {
                                            foreach (USNotificationMessage message in updateResult.NotificationMessages)
                                            {
                                                if (message.ErrorLevel == ErrorLevels.Error)
                                                {
                                                    USLogError.WriteToFile(message.Message, new Exception(), excelineUser.UserName);
                                                }
                                            }
                                          
                                            resStatus = 4;  // Error occured while sending email.
                                        }                             
                                    }                               
                                }
                                catch (Exception e)
                                {
                                    USLogError.WriteToFile(e.Message, new Exception(), excelineUser.UserName);
                                    resStatus = 4;
                                }
                            }
                       
                        }
                  }
                return resStatus;
        }


        private   string CreateRandomPassword(int passwordLength)
        {
            string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789!@$?_-";
            char[] chars = new char[passwordLength];
            Random rd = new Random();

            for (int i = 0; i < passwordLength; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }

            return new string(chars);
        }
        

        #endregion

        public bool SaveUserCulture(string culture,string userName)
        {
            OperationResultValue<bool> result = AdminUserSettings.SaveUserCulture(culture, userName);
            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), userName);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool UpdateSettingForUserRoutine(string key, string value, string user)
        {
            var result = GMSUser.UpdateSettingForUserRoutine(key, value, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int GetLoginGymByUser(string user)
        {
            try
            {
                return AdminUserSettings.GetLoginGymByUser(user, ExceConnectionManager.GetGymCode(user));
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile("ERROR: GetLoginGymByUser" + ex.Message, ex, user);
                return 1;
            }
        }
    }
}
