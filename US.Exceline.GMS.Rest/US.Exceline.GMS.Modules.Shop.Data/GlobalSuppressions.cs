// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.
//
// To add a suppression to this file, right-click the message in the 
// Code Analysis results, point to "Suppress Message", and click 
// "In Suppression File".
// You do not need to add suppressions to this file manually.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Scope = "member", Target = "US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands.EditVoucherAction.#_branchId")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Scope = "member", Target = "US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands.SavePointOfSaleAction.#_user")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Scope = "member", Target = "US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands.SaveWithdrawalAction.#_withDrawal")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Scope = "member", Target = "US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands.AddNextOrderSale.#_gymCode")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "installmentID", Scope = "member", Target = "US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands.AddSunbedShopSalesAction.#Body(System.Data.Common.DbConnection)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Scope = "member", Target = "US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands.GetGiftVoucherSaleSummaryAction.#_fromDate")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Scope = "member", Target = "US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands.GetGiftVoucherSaleSummaryAction.#_toDate")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields", Scope = "member", Target = "US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands.GetGiftVoucherSaleSummaryAction.#_branchId")]
