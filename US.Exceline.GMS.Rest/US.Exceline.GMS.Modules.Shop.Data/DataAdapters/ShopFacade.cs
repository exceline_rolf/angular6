﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Shop.Data.SystemObjects;
using US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer;
using US.GMS.Core.DomainObjects.Admin;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.ManageShop;
using System.Collections.ObjectModel;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.DomainObjects.ManageMemberships;

namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters
{
    public class ShopFacade
    {
        private static IShopDataAdapter GetDataAdapter()
        {
            return new SQLServerShopDataAdapter();
        }

        #region Manage Sales

        public static SaleResultDC AddShopSales(String cardType, InstallmentDC installment, ShopSalesDC shopSaleDetails, PaymentDetailDC paymentDetails, int memberBranchID, int loggedBranchID, string user, string gymCode, bool isBookingPayment)
        {
            return GetDataAdapter().AddShopSales(cardType, installment, shopSaleDetails, paymentDetails, memberBranchID, loggedBranchID, user, gymCode, isBookingPayment);
        }

        public static SaleResultDC AddSunBedShopSales(SunBedHelperObject sunBedHelperObject, SunBedBusinessHelperObject businessHelperObj)
        {
            return GetDataAdapter().AddSunBedShopSales(sunBedHelperObject, businessHelperObj);
        }

        public static int ValidateArticleStockLevel(InstallmentDC installment ,string gymCode, int branchID)
        {
            return GetDataAdapter().ValidateArticleStockLevel(installment, gymCode, branchID);
        }

        public static List<ShopSalesItemDC> GetDailySales(DateTime salesDate, int branchId, string gymCode)
        {
            return GetDataAdapter().GetDailySales(salesDate, branchId, gymCode);
        }
        public static List<ShopSalesPaymentDC> GetShopBillSummary(DateTime fromDate, DateTime toDate, int branchId, string gymCode)
        {
            return GetDataAdapter().GetShopBillSummary(fromDate, toDate, branchId, gymCode);
        }
        public static ShopBillDetailDC GetShopSaleBillDetail(int saleId, int branchId, string gymCode)
        {
            return GetDataAdapter().GetShopSaleBillDetails(saleId, branchId, gymCode);
        }

        public static Dictionary<string, decimal> GetMemberEconomyBalances(string gymCode, int memberId)
        {
            return GetDataAdapter().GetMemberEconomyBalances(gymCode, memberId);
        }

        public static SalePointDC GetSalesPointByMachineName(int branchID, string machineName, string gymCode)
        {
            return GetDataAdapter().GetSalesPointByMachineName(branchID, machineName, gymCode);
        }

        public static List<WithdrawalDC> GetWithdrawalByMemberId(DateTime startDate, DateTime endDate, WithdrawalTypes type, int memberId, string gymCode, string user)
        {
            return GetDataAdapter().GetWithdrawalByMemberId(startDate, endDate, type, memberId, gymCode, user);
        }

        public static int SaveShopInstallment(InstallmentDC installment, int branchId, string gymCode, String SessionKey = "")
        {
            return GetDataAdapter().SaveShopInstallment(installment, branchId, gymCode, SessionKey);
        }

        public static DailySettlementDC GetDailySettlements(int salePointId, string gymCode)
        {
            return GetDataAdapter().GetDailySettlementsBySalePointId(salePointId, gymCode);

        }
        public static int SaveWithdrawal(int branchId, WithdrawalDC withDrawal, string gymCode, string user)
        {
            return GetDataAdapter().SaveWithdrawal(branchId, withDrawal, gymCode, user);
        }

        public static int SaveOpenCashRegister(int branchId, CashRegisterDC cashRegister, string gymCode, string user)
        {
            return GetDataAdapter().SaveOpenCashRegister(branchId, cashRegister, gymCode, user);
        }

        public static int AddDailySettlements(DailySettlementDC dailySettlement, string gymCode)
        {
            return GetDataAdapter().AddDailySettlement(dailySettlement, gymCode);
        }


        #endregion

        #region Manage Vouchers
        public static List<VoucherDC> GetVouchersList(int branchId, string gymCode)
        {
            return GetDataAdapter().GetVouchersList(branchId, gymCode);
        }

        public static bool AddVoucher(int branchId, VoucherDC voucher, string gymCode)
        {
            return GetDataAdapter().AddVoucher(branchId, voucher, gymCode);
        }

        public static int EditVoucher(int branchId, VoucherDC voucher, string gymCode)
        {
            return GetDataAdapter().EditVoucher(branchId, voucher, gymCode);
        }
        #endregion

        #region Returned Items
        public static List<ReturnItemDC> GetReturnItemList(int branchId, string gymCode)
        {
            return GetDataAdapter().GetReturnList(branchId, gymCode);
        }

        public static int AddReturnedItem(int branchId, ReturnItemDC returnedItem, string gymCode)
        {
            return GetDataAdapter().AddReturnitem(branchId, returnedItem, gymCode);
        }

        public static int EditReturnedItem(int branchId, ReturnItemDC editReturnedItem, string gymCode)
        {
            return GetDataAdapter().EditReturnitem(branchId, editReturnedItem, gymCode);
        }
        #endregion

        public static List<SalePointDC> GetSalesPointList(int branchID, string gymCode)
        {
            return GetDataAdapter().GetSalesPointList(branchID, gymCode);
        }

        public static int SavePointOfSale(int branchId, SalePointDC pointOfSale, string user, string gymCode)
        {
            return GetDataAdapter().SavePointOfSale(branchId, pointOfSale, user, gymCode);
        }

        public static bool AddEditUserHardwareProfile(int branchId, int hardwareProfileId, string user, string gymCode)
        {
            return GetDataAdapter().AddEditUserHardwareProfile(branchId, hardwareProfileId, user, gymCode);
        }

        public static int GetSelectedHardwareProfile(int branchId, string user, string gymCode)
        {
            return GetDataAdapter().GetSelectedHardwareProfile(branchId, user, gymCode);
        }

        public static int GetMemberByEmployeeID(int memberId, int branchId, string gymCode,string user)
        {
            return GetDataAdapter().GetMemberByEmployeeID(memberId,branchId,gymCode,user);
        }
        public static int GetNextGiftVoucherNo(string seqId, string subSeqId, string gymCode)
        {
            return GetDataAdapter().GetNextGiftVoucherNumberAction(seqId, subSeqId, gymCode);
        }

        public static List<ShopSalesPaymentDC> GetGiftVoucherSaleSummary(DateTime fromDate, DateTime toDate,
                                                                         int branchId, string gymCode)
        {
            return GetDataAdapter().GetGiftVoucherSaleSummary(fromDate, toDate, branchId, gymCode);
        }

        public static int ValidateGiftVoucherNumber(string voucherNumber, decimal payment, string gymCode, int branchID)
        {
            return GetDataAdapter().ValidateGiftVoucherNumber(voucherNumber, payment, gymCode, branchID);
        }
        public static int SaveGiftVoucherArticle(ArticleDC article, string gymCode)
        {
            return GetDataAdapter().SaveGiftVoucherArticle(article, gymCode);
        }
        public static Dictionary<string, string> GetGiftVoucherDetail(string gymCode)
        {
            return GetDataAdapter().GetGiftVoucherDetail(gymCode);
        }

        public static bool IsCashdrawerOpen(string machinename, string gymCode, string user)
        {
            return GetDataAdapter().IsCashdrawerOpen(machinename, gymCode, user);
        }

        public static DailySettlementPrintData ExcelineGetDailySettlementDataForPrint(String dailySettlementId, String reconiliationId, String SalePointId, String branchId, String mode, String startDate, String endDate, String gymCode, String countedCashDraft)
        {
            return GetDataAdapter().ExcelineGetDailySettlementDataForPrint( dailySettlementId,  reconiliationId,  SalePointId,  branchId,  mode,  startDate,  endDate,  gymCode,  countedCashDraft);
        }

        public static HelpDataForDailySettlmentPrintData ExcelineGetHelpDataForDailySettlmentPrintData(String branchId, String FromDateTime, String ToDateTime, String SalePointId,String gymCode)
        {
            return GetDataAdapter().ExcelineGetHelpDataForDailySettlmentPrintData(branchId, FromDateTime, ToDateTime, SalePointId, gymCode);
        }

        public static int CheckIfReceiptIsValidForPrint(String invoiceNo, int branchId,  String gymCode)
        {
            return GetDataAdapter().CheckIfReceiptIsValidForPrint( invoiceNo, branchId, gymCode);
        }

        public static List<InvoiceForReturnDC> GetInvoicesForArticleReturn(int memberID, int branchId, string articleNo, int shopOnly, string gymCode, string user)
        {
            return GetDataAdapter().GetInvoicesForArticleReturn(memberID, branchId, articleNo, shopOnly, gymCode, user);
        }

        public static String ExcelineGetReconcilliationTextForDailySettlementPrint(int dailSettlementId, String gymCode)
        {
            return GetDataAdapter().ExcelineGetReconcilliationTextForDailySettlementPrint( dailSettlementId,  gymCode);
        }
    }
}
