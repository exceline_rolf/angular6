﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands
{
    public class SaveShopInstallmentAction : USDBActionBase<int>
    {

        private InstallmentDC _installment;
        private int _branchId = -1;
        private int Id = -1;
        private int _installmentID = -1;
        private int _memberBranchId = -1;
        private String _sessionKey = String.Empty;

        public SaveShopInstallmentAction(InstallmentDC installment, int branchId,int memberBranchId=0, String sessionKey = "")
        {
            _installment = installment;
            _branchId = branchId;
            _memberBranchId = memberBranchId;
            _sessionKey = sessionKey;
        }

        protected override int Body(DbConnection connection)
        {
            bool _isSaved = false;
            DbTransaction transaction = null;
            transaction = connection.BeginTransaction();
            string StoredProcedureName = "USExceGMSShopSaveInstallments";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Transaction = transaction;
                cmd.Connection = transaction.Connection;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _installment.MemberId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@amount", DbType.Decimal, _installment.Amount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SDueDate", DbType.DateTime, _installment.DueDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SessionKey", DbType.String, _sessionKey));

                DbParameter output = new SqlParameter();
                output.DbType = DbType.String;
                output.ParameterName = "@outId";
                output.Size = 50;
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);
                cmd.ExecuteNonQuery();
                Id = Convert.ToInt32(output.Value);
                if (Id == -1)
                {
                    transaction.Rollback();
                }
                _installmentID = Id;
                _isSaved = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (_isSaved)
            {
                if (_installment.AddOnList != null)
                {
                    if (_installment.AddOnList.Count > 0)
                    {

                        foreach (var item in _installment.AddOnList)
                        {
                            _isSaved = false;
                            string _storedProcedureName = "USExceGMSShopSaveInstallmentShare";
                            try
                            {
                                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, _storedProcedureName);
                                cmd.Connection = transaction.Connection;
                                cmd.Transaction = transaction;

                                cmd.Parameters.Add(DataAcessUtils.CreateParam("@installmentId", DbType.Int32, Id));
                                if (item.ArticleId > 0)
                                {
                                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@articleId", DbType.Int32, item.ArticleId));
                                }
                                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InstallmentDate", DbType.DateTime, DateTime.Now));
                                cmd.Parameters.Add(DataAcessUtils.CreateParam("@text", DbType.String, item.ItemName));
                                cmd.Parameters.Add(DataAcessUtils.CreateParam("@articlePrice", DbType.Decimal, item.Price));
                                cmd.Parameters.Add(DataAcessUtils.CreateParam("@isStartupItem", DbType.Boolean, item.IsStartUpItem));
                                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MembercontractId", DbType.Int32, -1));
                                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Quantity", DbType.Int32, item.Quantity));
                                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Discount", DbType.Decimal, item.Discount));
                                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsActivityArticle", DbType.Boolean, item.IsActivityArticle));
                                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId ", DbType.Int32, _branchId));
                                if (item.Description == null)
                                {
                                    item.Description = "";
                                }
                                cmd.Parameters.Add(DataAcessUtils.CreateParam("@description ", DbType.String, item.Description));
                                cmd.Parameters.Add(DataAcessUtils.CreateParam("@priority ", DbType.Int32, item.Priority));

                                DbParameter output = new SqlParameter();
                                output.DbType = DbType.String;
                                output.ParameterName = "@outId";
                                output.Size = 50;
                                output.Direction = ParameterDirection.Output;
                                cmd.Parameters.Add(output);
                                cmd.ExecuteNonQuery();
                                int outID = Convert.ToInt32(output.Value);
                                if (outID == -1)
                                {
                                    transaction.Rollback();
                                }
                            }

                            catch (Exception ex)
                            {
                                throw ex;
                            }

                        }
                        _isSaved = true;
                    }
                }
            }

            StoredProcedureName = "USExceGMSShopSaveInstallmentDetails";
            try
            {
                _isSaved = false;
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Connection = transaction.Connection;
                cmd.Transaction = transaction;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _installment.MemberId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@amount", DbType.Decimal, _installment.Amount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@installmentId", DbType.Int32, _installmentID));

                DbParameter output = new SqlParameter();
                output.DbType = DbType.String;
                output.ParameterName = "@outId";
                output.Size = 50;
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);
                cmd.ExecuteNonQuery();
                int outputID = Convert.ToInt32(output.Value);
                if (outputID == -1)
                {
                    transaction.Rollback();
                }
                _isSaved = true;
            }

            catch (Exception ex)
            {
                _isSaved = false;
                throw ex;
            }
            transaction.Commit();
            if (_isSaved)
            {
                return _installmentID;
            }
            else
            {
                return -1;
            }
        }

        public int RunOnTransaction(DbTransaction transaction, bool isSunbedAddon=false)
        {
            bool _isSaved = false;         
            string StoredProcedureName = "USExceGMSShopSaveInstallments";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Transaction = transaction;
                cmd.Connection = transaction.Connection;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _installment.MemberId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@amount", DbType.Decimal, _installment.Amount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SDueDate", DbType.DateTime, _installment.DueDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberBranchID", DbType.Int32, _memberBranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@isSunbedAddon", DbType.Boolean, isSunbedAddon));//only for sunbed shop sale need to remove if change
                //cmd.Parameters.Add(DataAcessUtils.CreateParam("@SessionKey", DbType.String, _sessionKey));

                DbParameter output = new SqlParameter();
                output.DbType = DbType.String;
                output.ParameterName = "@outId";
                output.Size = 50;
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);
                cmd.ExecuteNonQuery();
                Id = Convert.ToInt32(output.Value);
                if (Id == -1)
                {
                    transaction.Rollback();
                }
                _installmentID = Id;
                _isSaved = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (_isSaved)
            {
                if (_installment.AddOnList != null)
                {
                    if (_installment.AddOnList.Count > 0)
                    {

                        foreach (var item in _installment.AddOnList)
                        {
                            _isSaved = false;
                            string _storedProcedureName = "USExceGMSShopSaveInstallmentShare";
                            try
                            {
                                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, _storedProcedureName);
                                cmd.Connection = transaction.Connection;
                                cmd.Transaction = transaction;

                                cmd.Parameters.Add(DataAcessUtils.CreateParam("@installmentId", DbType.Int32, Id));
                                if (item.ArticleId > 0)
                                {
                                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@articleId", DbType.Int32, item.ArticleId));
                                }
                                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InstallmentDate", DbType.DateTime, DateTime.Now));
                                cmd.Parameters.Add(DataAcessUtils.CreateParam("@text", DbType.String, item.ItemName));
                                cmd.Parameters.Add(DataAcessUtils.CreateParam("@articlePrice", DbType.Decimal, item.Price));
                                cmd.Parameters.Add(DataAcessUtils.CreateParam("@isStartupItem", DbType.Boolean, item.IsStartUpItem));
                                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MembercontractId", DbType.Int32, -1));
                                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Quantity", DbType.Int32, item.Quantity));
                                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Discount", DbType.Decimal, item.Discount));
                                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsActivityArticle", DbType.Boolean, item.IsActivityArticle));
                                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId ", DbType.Int32, _branchId));
                                if (item.Description == null)
                                {
                                    item.Description = "";
                                }
                                cmd.Parameters.Add(DataAcessUtils.CreateParam("@description ", DbType.String, item.Description));
                                cmd.Parameters.Add(DataAcessUtils.CreateParam("@priority ", DbType.Int32, item.Priority));

                                DbParameter output = new SqlParameter();
                                output.DbType = DbType.String;
                                output.ParameterName = "@outId";
                                output.Size = 50;
                                output.Direction = ParameterDirection.Output;
                                cmd.Parameters.Add(output);
                                cmd.ExecuteNonQuery();
                                int outID = Convert.ToInt32(output.Value);
                                if (outID == -1)
                                {
                                    transaction.Rollback();
                                }
                            }

                            catch (Exception ex)
                            {
                                throw ex;
                            }

                        }
                        _isSaved = true;
                    }
                }
            }

            StoredProcedureName = "USExceGMSShopSaveInstallmentDetails";
            try
            {
                _isSaved = false;
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Connection = transaction.Connection;
                cmd.Transaction = transaction;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _installment.MemberId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@amount", DbType.Decimal, _installment.Amount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@installmentId", DbType.Int32, _installmentID));

                DbParameter output = new SqlParameter();
                output.DbType = DbType.String;
                output.ParameterName = "@outId";
                output.Size = 50;
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);
                cmd.ExecuteNonQuery();
                int outputID = Convert.ToInt32(output.Value);
                if (outputID == -1)
                {
                    transaction.Rollback();
                }
                _isSaved = true;
            }

            catch (Exception ex)
            {
                _isSaved = false;
                throw ex;
            }          
            if (_isSaved)
            {
                return _installmentID;
            }
            else
            {
                return -1;
            }
        }
    }
}
