﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageShop;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;

namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands
{
    public class EditReturnedItemAction : USDBActionBase<int>
    {
        private int _branchId = -1;
        private ReturnItemDC _editReturnedItem;

        public EditReturnedItemAction(int branchId, ReturnItemDC editReturnedItem)
        {
            _branchId = branchId;
            _editReturnedItem = editReturnedItem;
        }

        protected override int Body(DbConnection connection)
        {
            int returnedItemId = 0;
            string storedProcedureName = "USExceGMSShopEditReturnItem";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, _editReturnedItem.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ArticleId", DbType.Int32, _editReturnedItem.ArticleId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Quantity", DbType.Int32, _editReturnedItem.Quantity));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ReturnDate", DbType.DateTime, _editReturnedItem.ReturnDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Comment", DbType.String, _editReturnedItem.Comment));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@OutId";
                param.SqlDbType = SqlDbType.Int;
                param.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(param);

                cmd.ExecuteNonQuery();

                returnedItemId = Convert.ToInt32(param.Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return returnedItemId;
        }

    }
}
