﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------f
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageShop;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands
{
    public class GetWithdrawalAction : USDBActionBase<List<WithdrawalDC>>
    {

        private int _memberId = -1;
        private DateTime _startDate = DateTime.Now;
        private DateTime _endDate = DateTime.Now;
        private WithdrawalTypes _type = WithdrawalTypes.ALL;
        private readonly string _user = string.Empty;

        public GetWithdrawalAction(DateTime startDate, DateTime endDate, WithdrawalTypes type, int memberId, string user)
        {
            _memberId = memberId;
            _startDate = startDate;
            _type = type;
            _endDate = endDate;
            _user = user;
        }

        protected override List<WithdrawalDC> Body(DbConnection connection)
        {
            List<WithdrawalDC> withdrawalList = new List<WithdrawalDC>();
            string storedProcedureName = "USExceGMSGetWithdrawalByMember";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                if (_startDate != DateTime.MinValue)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@startDate", DbType.DateTime, _startDate));
                }
                if (_endDate != DateTime.MinValue)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@endDate", DbType.DateTime, _endDate));
                }
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@type", DbType.String, _type.ToString()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@UserName", DbType.String, _user));

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    WithdrawalDC withdrawal = new WithdrawalDC();
                    withdrawal.Id = Convert.ToInt32(reader["Id"].ToString());
                    withdrawal.PaymentAmount = Convert.ToDecimal(reader["Amount"].ToString());
                    withdrawal.Comment = reader["Comment"].ToString();
                    withdrawal.EmployeeId = Convert.ToInt32(reader["EntityId"].ToString());
                    withdrawal.MemberId = Convert.ToInt32(reader["EntityId"].ToString());
                    withdrawal.CreatedDate = Convert.ToDateTime(reader["CreatedDate"].ToString());
                    withdrawal.SalePointId = Convert.ToInt32(reader["SalePointId"].ToString());
                    withdrawal.CreatedUser = reader["CreatedUser"].ToString();
                    withdrawal.WithdrawalType = (WithdrawalTypes)Enum.Parse(typeof(WithdrawalTypes), reader["Type"].ToString(), true);
                    withdrawalList.Add(withdrawal);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return withdrawalList;
        }
    }

}
