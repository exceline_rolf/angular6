﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer
{

    public class IsCashdrawerOpenAction : USDBActionBase<bool>
    {
        private readonly string _machinename;

        public IsCashdrawerOpenAction(string machinename)
        {
            _machinename = machinename;
        }

        protected override bool Body(System.Data.Common.DbConnection dbConnection)
        {
            var result = true;

            try
            {
                string storedProcedure = "USExceGMSIsCashdrawerOpen";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@machinename", DbType.String, _machinename));


                DbParameter outPara = new SqlParameter();
                outPara.DbType = System.Data.DbType.Boolean;
                outPara.ParameterName = "@status";
                outPara.Direction = System.Data.ParameterDirection.Output;
                // outPara.Value = 1;
                cmd.Parameters.Add(outPara);
                cmd.ExecuteNonQuery();

                result = Convert.ToBoolean(outPara.Value);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
   
}