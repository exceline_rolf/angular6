﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.ManageShop;
using US.GMS.Core.SystemObjects;
using US.GMS.Data.DataAdapters.SQLServer.Commands;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands
{
    public class AddSunbedShopSalesAction : USDBActionBase<SaleResultDC>
    {
        private SunBedHelperObject _sunBedHelperObject;
        private SunBedBusinessHelperObject _businessHelperObj;
        private SunBedShopSaleSummary _sunBedShopSaleSummary;

        public AddSunbedShopSalesAction(SunBedHelperObject sunBedHelperObject, SunBedBusinessHelperObject businessHelperObj)
        {
            _sunBedHelperObject = sunBedHelperObject;
            _businessHelperObj = businessHelperObj;
            _sunBedShopSaleSummary = new SunBedShopSaleSummary();

        }
        protected override SaleResultDC Body(System.Data.Common.DbConnection connection)
        {
            DbTransaction transaction = connection.BeginTransaction();
            SaleResultDC _salesResult = null;

            _sunBedShopSaleSummary.MemberBranchId = _sunBedHelperObject.MemberBranchID;
            _sunBedShopSaleSummary.TerminalBranchId = _sunBedHelperObject.LoggedbranchID;
            _sunBedShopSaleSummary.MemberId = _businessHelperObj.MemberId;
            _sunBedShopSaleSummary.PaidAccessMode = _businessHelperObj.PaidAccessMode;
            _sunBedShopSaleSummary.TerminalId = _businessHelperObj.TerminalID;

            try
            {
                if (_sunBedHelperObject.Installments.Count > 1)
                {
                    InstallmentDC onAccInstallment = _sunBedHelperObject.Installments[0];
                    PaymentDetailDC onAccPayment = _sunBedHelperObject.PaymentDetails[0];
                    ShopSalesDC onAccShopSale = _sunBedHelperObject.ShopSaleDetails[0];

                    InstallmentDC nextOrderInstallment = _sunBedHelperObject.Installments[1];
                    PaymentDetailDC nextOrderPayment = _sunBedHelperObject.PaymentDetails[1];
                    ShopSalesDC nextOrderShopSale = _sunBedHelperObject.ShopSaleDetails[1];

                    _sunBedHelperObject.Installments.Clear();
                    _sunBedHelperObject.PaymentDetails.Clear();
                    _sunBedHelperObject.ShopSaleDetails.Clear();

                    _sunBedHelperObject.Installments.Add(onAccInstallment);
                    _sunBedHelperObject.PaymentDetails.Add(onAccPayment);
                    _sunBedHelperObject.ShopSaleDetails.Add(onAccShopSale);

                    //add sunbed shop sale summary data
                    _sunBedShopSaleSummary.Amount = onAccInstallment.Amount + nextOrderInstallment.Amount;


                    PayModeDC NEXTORDERpayMode = onAccPayment.PayModes.FirstOrDefault(X => X.PaymentTypeCode == "NEXTORDER");
                    if (NEXTORDERpayMode == null)
                    {
                        if (onAccInstallment.AddOnList.Where(x => x.Quantity >= 0).ToList().Count > 0)
                        {
                            var saveShopInstallmentAction = new SaveShopInstallmentAction(_sunBedHelperObject.Installments[0], _sunBedHelperObject.LoggedbranchID);
                            _sunBedHelperObject.Installments.FirstOrDefault().Id = saveShopInstallmentAction.RunOnTransaction(transaction);
                            _sunBedShopSaleSummary.InvoiceAmount = _sunBedHelperObject.Installments[0].Amount;
                        }
                    }

                    var paymentAction = new RegisterSunBedShopInstallmentPaymentAction(_sunBedHelperObject);
                    _salesResult = paymentAction.RunOnTransaction(transaction);
                    _sunBedShopSaleSummary.InvoiceNumber = _salesResult.InvoiceNo;
                    if (_salesResult.AritemNo > 0)
                    {
                        var nextOrderAction = new AddSunBedNextOrderSale(_sunBedHelperObject.GatPuchaseShopItem, nextOrderInstallment.MemberId, nextOrderPayment.PayModes.FirstOrDefault().Amount, nextOrderShopSale, _sunBedHelperObject.MemberBranchID, _sunBedHelperObject.LoggedbranchID, _sunBedHelperObject.User);
                        _salesResult = nextOrderAction.RunOnTransaction(transaction);
                        if (_salesResult.NextOrderId > 0)
                        {
                            _sunBedShopSaleSummary.NextOrderId = _salesResult.NextOrderId;
                            _sunBedShopSaleSummary.NextOrderAmount = nextOrderInstallment.Amount;
                        }
                        else if (_salesResult.NextOrderId < 0)
                        {
                            InstallmentDC updatedInstallment = ModifyInstallment(nextOrderInstallment, _businessHelperObj);
                            updatedInstallment.Amount = _businessHelperObj.NextOrderPaymentAmount;
                            var saveShopInstallmentAction = new SaveShopInstallmentAction(updatedInstallment, _sunBedHelperObject.LoggedbranchID, _sunBedHelperObject.MemberBranchID);
                            updatedInstallment.Id = saveShopInstallmentAction.RunOnTransaction(transaction, true);

                            _sunBedShopSaleSummary.ShopOrderId = updatedInstallment.Id;
                            _sunBedShopSaleSummary.ShopOrderAmount = updatedInstallment.Amount;

                            if (updatedInstallment.Id > 0)
                            {
                                SunBedAddShopOrderSaleAction addShopSale = new SunBedAddShopOrderSaleAction(_sunBedHelperObject, updatedInstallment.Id, nextOrderShopSale, updatedInstallment.Amount);
                                int transId = addShopSale.RunOnTransaction(transaction);

                                if (transId > 0)
                                {
                                    UpdateShopAccountItemAndGatPurchaseAction updateGatPurchaseAction = new UpdateShopAccountItemAndGatPurchaseAction(_sunBedHelperObject.GatPuchaseShopItem);
                                    bool isUpdated = updateGatPurchaseAction.RunOnTransaction(transaction);
                                    if (isUpdated)
                                        _salesResult.SaleStatus = SaleErrors.SUCCESS;
                                    else
                                        transaction.Rollback();
                                }
                                else
                                {
                                    transaction.Rollback();
                                }
                            }
                        }
                        AddSunBedShopSaleSummaryAction summaryAction = new AddSunBedShopSaleSummaryAction(_sunBedShopSaleSummary);
                        bool summryResult = summaryAction.RunOnTransaction(transaction);
                        if (summryResult)
                        {
                            transaction.Commit();
                            _salesResult.SaleStatus = SaleErrors.SUCCESS;
                        }
                        else
                        {
                            transaction.Rollback();
                            _salesResult.SaleStatus = SaleErrors.ERROR;
                        }
                    }
                }
                else if (_sunBedHelperObject.Installments.Count == 1)
                {
                    var _paymentDetails = _sunBedHelperObject.PaymentDetails.ToList();
                    var _installments = _sunBedHelperObject.Installments.ToList();
                    var _saleDetails = _sunBedHelperObject.ShopSaleDetails.ToList();

                    var payMode = _paymentDetails.FirstOrDefault().PayModes.FirstOrDefault(X => X.PaymentTypeCode == "NEXTORDER");
                    if (payMode != null)
                    {
                        var nextOrderAction = new AddSunBedNextOrderSale(_sunBedHelperObject.GatPuchaseShopItem, _installments.FirstOrDefault().MemberId, payMode.Amount, _saleDetails.FirstOrDefault(), _sunBedHelperObject.MemberBranchID, _sunBedHelperObject.LoggedbranchID, _sunBedHelperObject.User);
                        _salesResult = nextOrderAction.RunOnTransaction(transaction);
                        if (_salesResult.NextOrderId > 0)
                        {
                            _sunBedShopSaleSummary.NextOrderId = _salesResult.NextOrderId;
                            _sunBedShopSaleSummary.NextOrderAmount = _installments.FirstOrDefault().Amount;
                            _sunBedShopSaleSummary.Amount = _sunBedShopSaleSummary.NextOrderAmount;
                        }
                        else if (_salesResult.NextOrderId < 0)
                        {
                            InstallmentDC updatedInstallment = ModifyInstallment(_sunBedHelperObject.Installments[0], _businessHelperObj);
                            var saveShopInstallmentAction = new SaveShopInstallmentAction(updatedInstallment, _sunBedHelperObject.LoggedbranchID, _sunBedHelperObject.MemberBranchID);
                            updatedInstallment.Id = saveShopInstallmentAction.RunOnTransaction(transaction, true);

                            _sunBedShopSaleSummary.ShopOrderId = updatedInstallment.Id;
                            _sunBedShopSaleSummary.ShopOrderAmount = updatedInstallment.Amount;
                            _sunBedShopSaleSummary.Amount = _sunBedShopSaleSummary.ShopOrderAmount;

                            if (updatedInstallment.Id > 0)
                            {
                                SunBedAddShopOrderSaleAction addShopSale = new SunBedAddShopOrderSaleAction(_sunBedHelperObject, updatedInstallment.Id, _saleDetails.FirstOrDefault(), updatedInstallment.Amount);
                                int transId = addShopSale.RunOnTransaction(transaction);

                                if (transId > 0)
                                {
                                    UpdateShopAccountItemAndGatPurchaseAction updateGatPurchaseAction = new UpdateShopAccountItemAndGatPurchaseAction(_sunBedHelperObject.GatPuchaseShopItem);
                                    bool isUpdated = updateGatPurchaseAction.RunOnTransaction(transaction);
                                    if (isUpdated)
                                        _salesResult.SaleStatus = SaleErrors.SUCCESS;
                                    else
                                        transaction.Rollback();
                                }
                                else
                                {
                                    transaction.Rollback();
                                }
                            }
                        }
                        AddSunBedShopSaleSummaryAction summaryAction = new AddSunBedShopSaleSummaryAction(_sunBedShopSaleSummary);
                        bool summryResult = summaryAction.RunOnTransaction(transaction);
                        if (summryResult)
                        {
                            transaction.Commit();
                            _salesResult.SaleStatus = SaleErrors.SUCCESS;
                        }
                        else
                        {
                            transaction.Rollback();
                            _salesResult.SaleStatus = SaleErrors.ERROR;
                        }
                    }
                    else
                    {
                        PayModeDC NEXTORDERpayMode = _paymentDetails.FirstOrDefault().PayModes.FirstOrDefault(X => X.PaymentTypeCode == "NEXTORDER");
                        if (NEXTORDERpayMode == null)
                        {
                            if (_installments.FirstOrDefault().AddOnList.Where(x => x.Quantity >= 0).ToList().Count > 0)
                            {
                                var saveShopInstallmentAction = new SaveShopInstallmentAction(_sunBedHelperObject.Installments[0], _sunBedHelperObject.LoggedbranchID);
                                _sunBedHelperObject.Installments.FirstOrDefault().Id = saveShopInstallmentAction.RunOnTransaction(transaction);

                                _sunBedShopSaleSummary.Amount = _sunBedHelperObject.Installments[0].Amount;
                                _sunBedShopSaleSummary.InvoiceAmount = _sunBedHelperObject.Installments[0].Amount;

                                if (_sunBedHelperObject.Installments.FirstOrDefault().Id > 0)
                                {
                                    var paymentAction = new RegisterSunBedShopInstallmentPaymentAction(_sunBedHelperObject);
                                    _salesResult = paymentAction.RunOnTransaction(transaction);
                                    if (_salesResult.AritemNo > 0)
                                    {
                                        _sunBedShopSaleSummary.InvoiceNumber = _salesResult.InvoiceNo;

                                        AddSunBedShopSaleSummaryAction summaryAction = new AddSunBedShopSaleSummaryAction(_sunBedShopSaleSummary);
                                        bool summryResult = summaryAction.RunOnTransaction(transaction);
                                        if (summryResult)
                                        {
                                            transaction.Commit();
                                            _salesResult.SaleStatus = SaleErrors.SUCCESS;
                                        }
                                        else
                                        {
                                            transaction.Rollback();
                                            _salesResult.SaleStatus = SaleErrors.ERROR;
                                        }
                                    }
                                    else
                                    {
                                        transaction.Rollback();
                                    }

                                }
                                else
                                {
                                    transaction.Rollback();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw ex;
            }
            return _salesResult;
        }

        private InstallmentDC ModifyInstallment(InstallmentDC installment, SunBedBusinessHelperObject _businessHelperObj)
        {
            installment.InvoiceGeneratedDate = DateTime.Now;
            installment.InstallmentDate = DateTime.Now;
            installment.DueDate = DateTime.Now;


            installment.AddOnList = new List<ContractItemDC>();

            ContractItemDC noArticle = new ContractItemDC();
            noArticle.ArticleId = _businessHelperObj.ArticleNo;
            noArticle.CategoryId = _businessHelperObj.ArticleCategoryId;
            noArticle.ItemName = _businessHelperObj.ArticleName;

            if (_businessHelperObj.NextOrderPaymentAmount > 0)
            {
                installment.Amount = _businessHelperObj.NextOrderPaymentAmount;
                noArticle.Price = _businessHelperObj.NextOrderPaymentAmount;
            }
            else
            {
                installment.Amount = _businessHelperObj.Amount;
                noArticle.Price = _businessHelperObj.Amount;
            }
            noArticle.IsActivityArticle = false;
            installment.AddOnList.Add(noArticle);

            return installment;
        }
    }
}
