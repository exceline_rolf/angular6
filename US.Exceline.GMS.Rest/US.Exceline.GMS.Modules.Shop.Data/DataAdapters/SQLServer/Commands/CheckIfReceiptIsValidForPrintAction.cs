﻿using System;
using System.Collections.Generic;
using US.GMS.Core.DomainObjects.ManageShop;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands
{
    public class CheckIfReceiptIsValidForPrintAction : USDBActionBase<int>
    {
        private int _branchId = 0;
        private String _invoiceNo = String.Empty;
        private int _numberOfReceipts = -1;
        public CheckIfReceiptIsValidForPrintAction(String invoiceNo, int branchid)
        {
            _branchId = branchid;
            _invoiceNo = invoiceNo;
        }

        protected override int Body(DbConnection connection)
        {
            string StoredProcedureName = "CheckIfReceiptIsValidForPrint";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InvoiceNo", DbType.String, _invoiceNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                DbDataReader reader = cmd.ExecuteReader();


                while (reader.Read())
                {

                    _numberOfReceipts = Convert.ToInt32(reader["NumberOfReceipts"]);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return _numberOfReceipts;
        }
    }
}

