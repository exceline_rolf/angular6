﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands
{
    public class AddSunBedShopSaleSummaryAction:USDBActionBase<bool>
    {
        private SunBedShopSaleSummary _sunBedShopSaleSummary;

        public AddSunBedShopSaleSummaryAction(SunBedShopSaleSummary sunBedShopSaleSummary)
        {
            _sunBedShopSaleSummary = sunBedShopSaleSummary;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            bool _isAdded = false;
            const string storedProcedureName = "USExceGMSAddSunbedShopSaleSummary";
            try
            {
                DbCommand command = CreateCommand(CommandType.StoredProcedure, storedProcedureName);                
                command.Parameters.Add(DataAcessUtils.CreateParam("@MemberBranchID", DbType.Int32, _sunBedShopSaleSummary.MemberBranchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@TerminalBranchID", DbType.Int32, _sunBedShopSaleSummary.TerminalBranchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Amount", DbType.Int32, _sunBedShopSaleSummary.Amount));
                command.Parameters.Add(DataAcessUtils.CreateParam("@InvoiceAmount", DbType.Int32, _sunBedShopSaleSummary.InvoiceAmount));
                command.Parameters.Add(DataAcessUtils.CreateParam("@NextOrderAmount", DbType.Int32, _sunBedShopSaleSummary.NextOrderAmount));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ShopOrderAmount", DbType.Int32, _sunBedShopSaleSummary.ShopOrderAmount));
                command.Parameters.Add(DataAcessUtils.CreateParam("@InvoiceNo", DbType.Int32, _sunBedShopSaleSummary.InvoiceNumber));
                command.Parameters.Add(DataAcessUtils.CreateParam("@NextOrderID", DbType.Int32, _sunBedShopSaleSummary.NextOrderId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ShopOrderID", DbType.Int32, _sunBedShopSaleSummary.ShopOrderId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@TerminalID", DbType.Int32, _sunBedShopSaleSummary.TerminalId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@MemberID", DbType.Int32, _sunBedShopSaleSummary.MemberId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@PaidAccessMode", DbType.Int32, _sunBedShopSaleSummary.PaidAccessMode));

                int returnID = Convert.ToInt32(command.ExecuteScalar());
                if (returnID > 0)
                    _isAdded = true;


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _isAdded;
        }

        public bool RunOnTransaction(DbTransaction transaction)
        {
            bool _isAdded = false;
            const string storedProcedureName = "USExceGMSAddSunbedShopSaleSummary";
            try
            {
                DbCommand command = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                command.Connection = transaction.Connection;
                command.Transaction = transaction;
                command.Parameters.Add(DataAcessUtils.CreateParam("@MemberBranchID", DbType.Int32, _sunBedShopSaleSummary.MemberBranchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@TerminalBranchID", DbType.Int32, _sunBedShopSaleSummary.TerminalBranchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Amount", DbType.Int32, _sunBedShopSaleSummary.Amount));
                command.Parameters.Add(DataAcessUtils.CreateParam("@InvoiceAmount", DbType.Int32, _sunBedShopSaleSummary.InvoiceAmount));
                command.Parameters.Add(DataAcessUtils.CreateParam("@NextOrderAmount", DbType.Int32, _sunBedShopSaleSummary.NextOrderAmount));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ShopOrderAmount", DbType.Int32, _sunBedShopSaleSummary.ShopOrderAmount));
                command.Parameters.Add(DataAcessUtils.CreateParam("@InvoiceNo", DbType.Int32, _sunBedShopSaleSummary.InvoiceNumber));
                command.Parameters.Add(DataAcessUtils.CreateParam("@NextOrderID", DbType.Int32, _sunBedShopSaleSummary.NextOrderId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ShopOrderID", DbType.Int32, _sunBedShopSaleSummary.ShopOrderId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@TerminalID", DbType.Int32, _sunBedShopSaleSummary.TerminalId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@MemberID", DbType.Int32, _sunBedShopSaleSummary.MemberId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@PaidAccessMode", DbType.Int32, _sunBedShopSaleSummary.PaidAccessMode));

                int returnID = Convert.ToInt32(command.ExecuteScalar());
                if (returnID > 0)
                    _isAdded = true;

        
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _isAdded;
        }
    }
}
