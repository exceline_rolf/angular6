﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands
{
    public class SaveUserShopHardwareProfileAction : USDBActionBase<bool>
    {
        private int _branchId;
        private int _hardwareProfileId;
        private string _user = string.Empty;

        public SaveUserShopHardwareProfileAction(int branchId, int hardwareProfileId, string user)
        {
            _branchId = branchId;
            _hardwareProfileId = hardwareProfileId;
            _user = user;
            //OverwriteUser(user);
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            bool isSuccess = false;

            try
            {
                string storedProcedure = "USExceGMSSaveShopUserHardwareProfile";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@hardwareProfileId", DbType.Int32, _hardwareProfileId));
                cmd.ExecuteNonQuery();
                isSuccess = true;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isSuccess;
        }
    }
}
