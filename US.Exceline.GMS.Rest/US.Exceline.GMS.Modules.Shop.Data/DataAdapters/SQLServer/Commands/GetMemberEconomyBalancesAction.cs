﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands
{
    public class GetMemberEconomyBalancesAction : USDBActionBase<Dictionary<string, decimal>>
    {

        private int _memberId = -1;


        public GetMemberEconomyBalancesAction(string gymCode, int memberId)
        {
            _memberId = memberId;

        }

        protected override Dictionary<string, decimal> Body(System.Data.Common.DbConnection connection)
        {
            Dictionary<string, decimal> economyBalances = new Dictionary<string, decimal>();
            string storedProcedure = "USExceGMSShopGetMemberEconomyBalances";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    decimal prepaid = Convert.ToDecimal(reader["prepiadBalance"]);
                    decimal onaccount = Convert.ToDecimal(reader["onAccount"]);

                    economyBalances.Add("PREPAID", prepaid);
                    economyBalances.Add("ONACCOUNT", onaccount);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return economyBalances;
        }
    }
}
