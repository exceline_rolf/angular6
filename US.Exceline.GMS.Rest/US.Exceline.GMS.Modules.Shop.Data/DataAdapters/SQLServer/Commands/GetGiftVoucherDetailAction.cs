﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Shop.Data.DataAdapters.SQLServer.Commands
{
    public class GetGiftVoucherDetailAction:USDBActionBase<Dictionary<string,string>>
    {
        protected override Dictionary<string, string> Body(System.Data.Common.DbConnection dbConnection)
        {
            var outputValue = new Dictionary<string, string>();
            const string storedProcedureName = "USExceGetGiftVoucherArticleDetail";
            try
            {
                var command = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    outputValue.Add("ArticleID", reader["ArticleID"].ToString());
                    outputValue.Add("ArticleNo", Convert.ToInt32(reader["ArticleNumber"].ToString()).ToString());
                    outputValue.Add("ArticleName", reader["ArticleName"].ToString());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return outputValue;
        }
    }
}
