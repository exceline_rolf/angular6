﻿// --------------------------------------------------------------------------
// Copyright(c) 2012 UnicornSolutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USPayment
// Project Name      : US.Common.Logging
// Coding Standard   : US Coding Standards
// Author            : AAB
// Created Timestamp : 05/01/2012 
// --------------------------------------------------------------------------
using System;
using System.IO;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System.Diagnostics;

namespace US.Common.Logging.API
{
    public static class USLogError
    {
        private static USLogConfiguration _usLogConfiguration = new USLogConfiguration();

        /// <summary>
        /// In configuration= 0-nothing; 1-Text; 2-DB; 3-Db and Text
        /// </summary>
        /// <param name="message"></param>
        /// <param name="ex"></param>
        /// <param name="errorCode"></param>
        /// <param name="user"></param>
        /// <param name="applicationName"></param>
        /// <param name="category"></param>
        /// <param name="eventDescription1"></param>
        /// <param name="eventDescription2"></param>
        /// <param name="eventDescription3"></param>
        public static void  LogErrors(string message, Exception ex, string errorCode, string user, string applicationName, string category = "", string eventDescription1 = "", string eventDescription2 = "", string eventDescription3 = "")
        {
            _usLogConfiguration = USLogConfigurationReader.GetUSPCongfiuration();

            switch (_usLogConfiguration.EnableErrorLog)
            {
                case 0:
                    {
                        break;
                    }
                case 1:
                    {
                       WriteToFile( message, ex, user);
                        break;
                    }
                case 2:
                    {
                        //WriteToDB(message, user, errorCode, applicationName, category, eventDescription1, eventDescription2, eventDescription3);
                        break;
                    }
                case 3:
                    {
                        //WriteToDB(message, user, errorCode, applicationName, category, eventDescription1, eventDescription2, eventDescription3);
                        WriteToFile(message, ex, user);
                        break;
                    }
                default:
                    break;
            }


        }

        public static void WriteToFile(string message,Exception ex,string user)
        {
            try
            {
                var logWriter = EnterpriseLibraryContainer.Current.GetInstance<LogWriter>();
                var logEntry = new LogEntry();
                logEntry.Message = user + ": "+ message;

                logEntry.Severity = TraceEventType.Error;

                logEntry.Categories = new List<string> { "General"};
                var extendedInfo = new Dictionary<string, object>();
                extendedInfo.Add("StackTrace", ex.StackTrace);
                extendedInfo.Add("User", user);
                if (extendedInfo != null)
                {
                    logEntry.ExtendedProperties = extendedInfo;
                }
                logWriter.Write(logEntry);
            }
            catch (Exception)
            {
                
            }
        
        }

        public static string GetGymCode(string user)
        {
            string gymCode = string.Empty;
            if (!string.IsNullOrEmpty(user))
            {
                string[] userParts = user.Split(new char[] { '/' });
                if (userParts.Length > 0)
                {
                    gymCode = userParts[0];
                }
            }
            return gymCode;
        }

        //public static void WriteToDB(string message, string user,string errorCode, string applicationName, string category = "", string eventDescription1 = "", string eventDescription2 = "", string eventDescription3 = "")
        //{
        //    try
        //    {
        //        _usLogConfiguration = USLogConfigurationReader.GetUSPCongfiuration();
        //        if (_usLogConfiguration.EnableErrorLog == 1)
        //        {

        //            LogDBManager logManager = new LogDBManager();
        //            logManager.WrightErrorToSQLDB(message, user,errorCode, applicationName, category, eventDescription1, eventDescription2, eventDescription3);

        //        }
        //    }
        //    catch
        //    {


        //    }

        //}

        public static void SendEmailNotification(List<string> messages, Exception ex, string user)
        {

            //TO DO
        }
    }
}
