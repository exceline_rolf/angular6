﻿// --------------------------------------------------------------------------
// Copyright(c) 2012 UnicornSolutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USPayment
// Project Name      : US.Common.Logging
// Coding Standard   : US Coding Standards
// Author            : AAB
// Created Timestamp : 05/01/2012 
// --------------------------------------------------------------------------
namespace US.Common.Logging
{
    public class USLogConfigurationItem
    {
        private string _key = string.Empty;
        public string Key
        {
            get { return _key; }
            set { _key = value; }
        }

        private string _value = string.Empty;
        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }


    }
}
