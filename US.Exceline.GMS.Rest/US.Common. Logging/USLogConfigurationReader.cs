﻿// --------------------------------------------------------------------------
// Copyright(c) 2012 UnicornSolutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USPayment
// Project Name      : US.Common.Logging
// Coding Standard   : US Coding Standards
// Author            : AAB
// Created Timestamp : 05/01/2012 
// --------------------------------------------------------------------------
using System;
using System.IO;
using System.Xml.Serialization;
using US.Payment.Core;

namespace US.Common.Logging
{
    public static class USLogConfigurationReader
    {
        private static USLogConfiguration _configObj = null;

        public static USLogConfiguration GetUSPCongfiuration()
        {

            try

            {
                if (_configObj != null)
                    return _configObj;

                string fileName = new FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).Directory.FullName + @"\USLogConfigurations.xml";

                // Following if find the file in read location when the .dll is called from a web application
                if (!File.Exists(fileName))
                {
                    fileName = USPRunTimeVariables.GetConfigurationByName("US_Log_Path"); //System.Configuration.ConfigurationSettings.AppSettings["US_Log_Path"]; 
                }

                using (TextReader textReader = new StreamReader(fileName))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(USLogConfiguration));
                    object deserializedObject = serializer.Deserialize(textReader);
                    serializer = null;
                    _configObj = (USLogConfiguration)deserializedObject;
                    return _configObj;
                }

            }
            catch (Exception ex)
            {
                throw new ApplicationException("Failed to read USlog Configurations from file [USLogConfigurations.xml]. " + ex.Message, ex);
            }
        }

    }
}
