﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.Unity;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.Unity;
using US.Payment.Core;

namespace US.Common.Logging
{
    public class USLogger
    {
        public static void Log(string message, List<string> categories, SeverityFilter severity, IDictionary<string, object> extendedInfo)
        {
            var logWriter = EnterpriseLibraryContainer.Current.GetInstance<LogWriter>();
            var logEntry = new LogEntry();
            logEntry.Message = message;

            switch (severity)
            {
                case SeverityFilter.Critical:
                    logEntry.Severity = TraceEventType.Critical;
                    break;
                case SeverityFilter.Error:
                    logEntry.Severity = TraceEventType.Error;
                    break;
                case SeverityFilter.Warning:
                    logEntry.Severity = TraceEventType.Warning;
                    break;
                case SeverityFilter.Information:
                    logEntry.Severity = TraceEventType.Information;
                    break;
                case SeverityFilter.Verbose:
                    logEntry.Severity = TraceEventType.Verbose;
                    break;
                default:
                    logEntry.Severity = TraceEventType.Verbose;
                    break;
            }

            logEntry.Categories = categories;
            if (extendedInfo != null)
            {
                logEntry.ExtendedProperties = extendedInfo;
            }

            logWriter.Write(logEntry);
        }

        public static void TraceFlow(string message, List<string> categories, IDictionary<string, object> extendedInfo)
        {
            if (USPRunTimeVariables.GetConfigurationByName("FlowTracingEnabled").Equals("true"))// ConfigurationManager.AppSettings["FlowTracingEnabled"].Equals("true"))
            {
                //add flow event category for filtering in the configuration
                categories.Add("FlowEvent");

                var logWriter = EnterpriseLibraryContainer.Current.GetInstance<LogWriter>();
                var logEntry = new LogEntry();
                logEntry.Message = message;
                logEntry.Categories = categories;
                logEntry.Severity = TraceEventType.Verbose;

                if (extendedInfo != null)
                {
                    logEntry.ExtendedProperties = extendedInfo;
                }

                logWriter.Write(logEntry);
            }
            else
            {
                return;
            }
        }

    }
}
