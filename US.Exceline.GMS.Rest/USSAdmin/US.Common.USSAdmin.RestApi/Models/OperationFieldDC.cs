﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Common.USSAdmin.RestApi.Models
{
    public class OperationFieldDC
    {
        public string DisplayName { get; set; }
        public string Status { get; set; }
    }
}