﻿#region File Header
// --------------------------------------------------------------------------
// Copyright(c) <2010> Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USAR
// Project Name      : US.Payment.Data
// Coding Standard   : US Coding Standards
// Author            : NHE
// Created Timestamp : 01/12/2011 HH:MM  PM
// --------------------------------------------------------------------------
#endregion

using System;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;
using US.Payment.Core.Utils;
using US.Common.Web.UI.Core.SystemObjects;

namespace US.Common.USSAdmin.Data.UserSettings.DBCommands
{
    public class AddEditUSPRoleAction : USDBActionBase<int>
    {
        USPRole _role = new USPRole();
        DbTransaction _transaction = null;
        
        public AddEditUSPRoleAction(USPRole role,string loggedUser)
        {
            _role = role;
            //_loggedUser = loggedUser;
            OverwriteUser(loggedUser);
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            int _result = -1;
            
            try
            {
                string spAddEditUSPRole = "USP_AUT_AddEditUSPRole";
                _transaction = connection.BeginTransaction();
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, spAddEditUSPRole);
                cmd.Transaction = _transaction;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@RoleNo", DbType.String, _role.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@RoleName", DbType.String, _role.RoleName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Description", DbType.String, _role.Description));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsActive", DbType.Boolean, _role.IsActive));
                DbParameter paraOut = new SqlParameter();
                paraOut.DbType = DbType.Int32;
                paraOut.ParameterName = "@NewRowId";
                paraOut.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(paraOut);
                cmd.ExecuteNonQuery();
                int upRole = Convert.ToInt32(paraOut.Value);




                foreach (int id in _role.ModuleList)
                {
                    string sp = "USP_AUT_AddEditUSPRoleModFeatOP";
                    DbCommand cmd2 = CreateCommand(System.Data.CommandType.StoredProcedure, sp);
                    cmd2.Transaction = _transaction;
                    cmd2.Parameters.Add(DataAcessUtils.CreateParam("@RoleNo", DbType.String, upRole));
                    cmd2.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, id));
                    cmd2.Parameters.Add(DataAcessUtils.CreateParam("@type", DbType.Int32, 1));
                    cmd2.ExecuteNonQuery();
                }
                foreach (int id in _role.FeatureList)
                {
                    string storedProcedureName = "USP_AUT_AddEditUSPRoleModFeatOP";
                    DbCommand cmd2 = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                    cmd2.Transaction = _transaction;
                    cmd2.Parameters.Add(DataAcessUtils.CreateParam("@RoleNo", DbType.String, upRole));
                    cmd2.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, id));
                    cmd2.Parameters.Add(DataAcessUtils.CreateParam("@type", DbType.Int32, 2));
                    cmd2.ExecuteNonQuery();
                }

                foreach (USPOperation operation in _role.OperationList)
                {
                    string storedProcedureName = "USP_AUT_AddEditUSPRoleModFeatOP";
                    DbCommand cmd2 = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                    string priviledges = "";
                    cmd2.Transaction = _transaction;
                    cmd2.Parameters.Add(DataAcessUtils.CreateParam("@RoleNo", DbType.String, upRole));
                    cmd2.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, operation.ID));
                    cmd2.Parameters.Add(DataAcessUtils.CreateParam("@type", DbType.Int32, 3));
                    cmd2.Parameters.Add(DataAcessUtils.CreateParam("@Privilages", DbType.String, priviledges));
                    if (operation.OperationExInfo.OperationFieldList.Count > 0)
                        cmd2.Parameters.Add(DataAcessUtils.CreateParam("@ExtendedInfo", DbType.Xml, XMLUtils.SerializeDataContractObjectToXML(operation.OperationExInfo)));
                    cmd2.ExecuteNonQuery();
                }
                //Save Sub Operations
               // SaveSubOperations(_role,_transaction);
                _transaction.Commit();
                _result = upRole;


            }
            catch
            {
                _transaction.Rollback();

                throw;
            }
            return _result;
        }

        private void SaveSubOperations(USPRole role, DbTransaction transaction)
        {
            foreach (USPSubOperation subOperation in role.SubOperationList)
            {
                string storedProcedureName = "USP_AUT_AddEditUSPSubOperationForRole";
                DbCommand cmd2 = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd2.Transaction = transaction;
                cmd2.Parameters.Add(DataAcessUtils.CreateParam("@RoleNo", DbType.Int16, role.Id));
                cmd2.Parameters.Add(DataAcessUtils.CreateParam("@SubOperation", DbType.Int32, subOperation.Id));
                cmd2.Parameters.Add(DataAcessUtils.CreateParam("@ExtendedInfo", DbType.Xml, XMLUtils.SerializeDataContractObjectToXML(subOperation.ExtendedInfo)));
                cmd2.ExecuteNonQuery();
            }
        }

    }
}