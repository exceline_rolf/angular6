﻿using System;
using System.Collections.Generic;
using US_DataAccess;
using US.Common.Web.UI.Core.SystemObjects;
using System.Data.Common;
using System.Data;
using US.Payment.Core.Utils;

namespace US.Common.USSAdmin.Data.UserSettings.DBCommands
{
    class USPRolesSearchAction: USDBActionBase<List<USPRoleTemp>>
    {
        private string _searchText;
        private bool _isActive;
        private string _loggedUser = string.Empty;

        public USPRolesSearchAction(string searchText, bool isActive,string loggedUser)
        {
            this._isActive = isActive;
            this._searchText = searchText;
            _loggedUser = loggedUser;
            OverwriteUser(_loggedUser);
        }

        protected override List<USPRoleTemp> Body(System.Data.Common.DbConnection connection)
        {
            List<USPRoleTemp> uspRoleList = new List<USPRoleTemp>();

            try
            {
                string storedProcedure = "USP_AUT_GetUSPRoleSearch";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SearchText", DbType.String, _searchText));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsActive", DbType.Boolean, _isActive));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    USPRoleTemp uspRoleTemp = new USPRoleTemp();
                    if (!string.IsNullOrEmpty(reader["ID"].ToString()))
                    {
                        uspRoleTemp.Id = Convert.ToInt32(reader["ID"]);
                    }
                    if (!string.IsNullOrEmpty(reader["RoleName"].ToString()))
                    {
                         uspRoleTemp.RoleName  = Convert.ToString(reader["RoleName"]);
                    }
                    if (!string.IsNullOrEmpty(reader["Description"].ToString()))
                    {
                        uspRoleTemp.Description = Convert.ToString(reader["Description"]);
                    }
                    if(!string.IsNullOrEmpty(reader["ModuleId"].ToString()))
                    {
                        uspRoleTemp.ModuleId = Convert.ToInt32(reader["ModuleId"]);
                    }
                    if(!string.IsNullOrEmpty(reader["FeatureId"].ToString()))
                    {
                         uspRoleTemp.FeatureId = Convert.ToInt32(reader["FeatureId"]);
                    }
                    if (!string.IsNullOrEmpty(reader["OperationId"].ToString()))
                    {
                         uspRoleTemp.OperationId  = Convert.ToInt32(reader["OperationId"]);
                    }
                    if (!string.IsNullOrEmpty(reader["IsActive"].ToString()))
                    {
                        uspRoleTemp.IsActive = Convert.ToBoolean(reader["IsActive"]);
                    }
                    if (!string.IsNullOrEmpty(reader["FieldList"].ToString()))
                    {
                        string feildListXML = Convert.ToString(reader["FieldList"]);
                        uspRoleTemp.OperationExInfo = (OperationExtendedInfo)XMLUtils.DesrializeXMLToObject(feildListXML, typeof(OperationExtendedInfo));
                    }
                    
                    uspRoleList.Add(uspRoleTemp);
                }
            }
            catch
            {
                throw;
            }
            return uspRoleList;
        }
    }
}


