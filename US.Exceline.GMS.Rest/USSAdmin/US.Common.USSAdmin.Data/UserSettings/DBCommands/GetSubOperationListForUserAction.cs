﻿using System;
using System.Collections.Generic;
using US_DataAccess;
using US.Common.Web.UI.Core.SystemObjects;
using System.Data.Common;
using System.Data;
using US.Payment.Core.Utils;

namespace US.Common.USSAdmin.Data.UserSettings.DBCommands
{
    class GetSubOperationListForUserAction: USDBActionBase<List<USPSubOperation>>
    {
        private string _userName = string.Empty;
        public GetSubOperationListForUserAction(string userName)
        {
            _userName = userName;
            OverwriteUser(_userName);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2200:RethrowToPreserveStackDetails")]
        protected override List<USPSubOperation> Body(System.Data.Common.DbConnection connection)
        {
            List<USPSubOperation> operationList = new List<USPSubOperation>();

            try
            {
                string spName = "USP_AUT_GetSubOperationListForUser";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@UserName", DbType.String, _userName));
                DbDataReader reader;
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    USPSubOperation operation = new USPSubOperation();
                    if (!(string.IsNullOrEmpty(reader["Name"].ToString())))
                    {
                        operation.Name = Convert.ToString(reader["Name"]);
                    }
                    if (!(string.IsNullOrEmpty(reader["DisplayName"].ToString())))
                    {
                        operation.DisplayName = Convert.ToString(reader["DisplayName"]);
                    }
                    if (!(string.IsNullOrEmpty(reader["ID"].ToString())))
                    {
                        operation.Id = Convert.ToInt32(reader["ID"]);
                    }

                    if (reader["ThumbnailImage"] != null)
                    {
                        operation.ThumbnailImage = Convert.ToString(reader["ThumbnailImage"]);
                    }
                    if (reader["COLOR"] != null)
                    {
                        operation.OperationColor = Convert.ToString(reader["COLOR"]);
                    }
                    if (reader["Namespace"] != null)
                    {
                        operation.SubOperationHome = Convert.ToString(reader["Namespace"]);
                    }
                    if (reader["Package"] != null)
                    {
                        operation.SubOperationPackage = Convert.ToString(reader["Package"]);
                    }
                    
                    if (!string.IsNullOrEmpty(reader["FieldList"].ToString()))
                    {
                        string extendedInfo = Convert.ToString(reader["FieldList"]).ToString();
                        extendedInfo = extendedInfo.Replace("US.Payment.Core.DomainObjects.DomainObjects.Authentication", "US.Common.Web.UI.Core.SystemObjects");
                        operation.ExtendedInfo = (OperationExtendedInfo)XMLUtils.DesrializeXMLToObject(extendedInfo, typeof(OperationExtendedInfo));
                    }
                    operationList.Add(operation);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return operationList;
        }
    }
}
