﻿using System;
using System.Collections.Generic;
using US_DataAccess;
using US.Common.Web.UI.Core.SystemObjects;
using System.Data.Common;
using System.Data;

namespace US.Common.USSAdmin.Data.UserSettings.DBCommands
{
    public class GetUserProfileAction : USDBActionBase<List<USPUserProfile>>
    {
        private string _userName = string.Empty;
        public GetUserProfileAction(string userName)
        {
            _userName = userName;

        }
        protected override List<USPUserProfile> Body(System.Data.Common.DbConnection connection)
        {
            List<USPUserProfile> userProfileInfoList = new List<USPUserProfile>();
            try
            {
                string spName = "USP_GetUserProfile";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@userName", DbType.String, _userName));

                DbDataReader dataReader;
                dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    USPUserProfile userProfile = new USPUserProfile();
                    userProfile.ProfileId = Convert.ToInt32(dataReader["ProfileId"]);
                    userProfile.ProfileName = Convert.ToString(dataReader["Name"]);
                    userProfile.UserId = Convert.ToInt32(dataReader["UserId"]);
                    userProfile.UserName = Convert.ToString(dataReader["UserName"]);
                    userProfile.UserHome = Convert.ToString(dataReader["Home"]);

                    userProfile.RoleId = Convert.ToInt32(dataReader["RoleId"]);

                    userProfile.ModuleId = Convert.ToInt32(dataReader["ModuleId"]);
                    userProfile.ModuleName = Convert.ToString(dataReader["ModuleName"]);
                    userProfile.ModuleDisplayName = Convert.ToString(dataReader["ModuleDisplayName"]);
                    userProfile.ModuleNamespace = Convert.ToString(dataReader["ModuleNamespace"]);
                    //userProfile.ModuleThumbnailImage = Convert.ToString(dataReader["RoleId"]);

                    userProfile.ModuleOperationId = Convert.ToInt32(dataReader["ModuleOperationId"]);
                    userProfile.ModuleOperationName = Convert.ToString(dataReader["ModuleOperationName"]);
                    userProfile.ModuleOperationDisplayName = Convert.ToString(dataReader["ModuleOperationDisplayName"]);
                    userProfile.ModuleOperationNamespace = Convert.ToString(dataReader["ModuleOperationNamespace"]);
                    userProfile.ModuleOperationThumbnailImage = Convert.ToString(dataReader["ModuleOperationThumbnailImage"]);

                    userProfile.FeatuerId = Convert.ToInt32(dataReader["FeatureId"]);
                    userProfile.FeatureName = Convert.ToString(dataReader["FeatuerName"]);
                    userProfile.FeatureDisplayName = Convert.ToString(dataReader["FeatureDisplayName"]);
                    userProfile.FeatureNamespace = Convert.ToString(dataReader["FeatureNamespace"]);
                    //userProfile.FeatureThumnailImage = Convert.ToString(dataReader["RoleId"]);

                    userProfile.FeatureOperationId = Convert.ToInt32(dataReader["FeatuerOperationId"]);
                    userProfile.FeatureOperationName = Convert.ToString(dataReader["FeatureOperationName"]);
                    userProfile.FeatureOperationDisplayName = Convert.ToString(dataReader["FeatureOperationDisplayName"]);
                    userProfile.FeatureOperationNamespace = Convert.ToString(dataReader["FeatureOperationNamespace"]);
                    userProfile.FeatuerOperationThumbnailImage = Convert.ToString(dataReader["FeatureOperationThumbnailImage"]);
                    userProfile.OperationMode = Convert.ToString(dataReader["Mode"]);
                    userProfile.AddedToWorkStation = Convert.ToInt32(dataReader["AddToWorkStation"]);
                    userProfile.OperationHome = Convert.ToString(dataReader["OperationHome"]);

                    userProfileInfoList.Add(userProfile);




                }
            }
            catch
            {
                throw;
            }
            return userProfileInfoList;

        }
    }
}
