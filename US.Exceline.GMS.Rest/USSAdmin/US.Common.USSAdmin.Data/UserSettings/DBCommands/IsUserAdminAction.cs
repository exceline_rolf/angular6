﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands
{
    public class IsUserAdminAction : USDBActionBase<string>
    {
        string _user = string.Empty;

        public IsUserAdminAction(string user)
        {
            _user = user;
        }

        protected override string Body(DbConnection connection)
        {
            string role = "";
            string StoredProcedureName = "USExceGMSAdminIsUserAdmin";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@username", DbType.String, _user));
                SqlParameter outputParam = new SqlParameter();
                outputParam.ParameterName = "@outputCode";
                outputParam.Size = 100;
                outputParam.DbType = DbType.String;
                outputParam.Direction = ParameterDirection.Output;

                cmd.Parameters.Add(outputParam);
                cmd.ExecuteScalar();

                role = Convert.ToString(outputParam.Value);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return role;

        }
    }
}
