﻿#region File Header
// --------------------------------------------------------------------------
// Copyright(c) <2010> Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USAR
// Project Name      : US.Payment.Data
// Coding Standard   : US Coding Standards
// Author            : NHE
// Created Timestamp : 01/12/2011 HH:MM  PM
// --------------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using US.Common.Web.UI.Core.SystemObjects;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;
using US.Payment.Core.Utils;
using US.Common.Web.UI.Core.SystemObjects.ScheduleManagement;
using US.GMS.Core.SystemObjects;

namespace US.Common.USSAdmin.Data.UserSettings.DBCommands
{
    public class AddEditUSPUserAction : USDBActionBase<int>
    {
        private UspUsersExtended _uspUser;
        private DataTable _dataTable;
        private string _loggedUser = string.Empty;

        public AddEditUSPUserAction(UspUsersExtended uspUser, List<UserBranchSelected> branchesList, string loggedUser)
        {
            this._uspUser = uspUser;
            //this._branchesList = branchesList;
            _loggedUser = loggedUser;
            if (branchesList != null)
                _dataTable = GetScheduleItemTypeLst(branchesList);
            OverwriteUser(_loggedUser);
        }

        private DataTable GetScheduleItemTypeLst(List<UserBranchSelected> branchesList)
        {
            _dataTable = new DataTable();
            _dataTable.Columns.Add(new DataColumn("Id", typeof(int)));
            _dataTable.Columns.Add(new DataColumn("UserId", typeof(int)));
            _dataTable.Columns.Add(new DataColumn("BranchId", typeof(int)));
            _dataTable.Columns.Add(new DataColumn("Deleted", typeof(bool)));


            foreach (UserBranchSelected branchItem in branchesList)
            {
                DataRow _dataTableRow = _dataTable.NewRow();
                _dataTableRow["Id"] = branchItem.Id;
                _dataTableRow["UserId"] = branchItem.UserId;
                _dataTableRow["BranchId"] = branchItem.BranchId;
                _dataTableRow["Deleted"] = branchItem.IsDeleted;
               
                _dataTable.Rows.Add(_dataTableRow);
            }

            return _dataTable;
        }


        protected override int Body(System.Data.Common.DbConnection connection)
        {
            DbTransaction transaction = null;
            int _result = -1;
            try
            {
                string spName = "USP_AUT_AddEditUSPUser";
                transaction = connection.BeginTransaction();
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                cmd.Transaction = transaction;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ID", DbType.Int32, _uspUser.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Name", DbType.String, _uspUser.UserName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@DisplayName", DbType.String, _uspUser.DisplayName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@RoleId", DbType.String, _uspUser.RoleId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@password", DbType.String, _uspUser.passowrd));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Email", DbType.String, _uspUser.email));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsActive", DbType.Boolean, _uspUser.IsActive));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CardNumber", DbType.String, _uspUser.CardNumber));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@HardwareProfileId",DbType.Int32, _uspUser.HardwareProfileId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@UserBranchList", SqlDbType.Structured, _dataTable));
                if (_uspUser.EmployeeId != -1)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@EmployeeID", DbType.Int32, _uspUser.EmployeeId));
                }


                DbParameter para = new SqlParameter();
                para.DbType = DbType.Int32;
                para.ParameterName = "@OutPutID";
                para.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(para);
                cmd.ExecuteNonQuery();
                int userID = Convert.ToInt32(para.Value);

                if (userID == -199) //sp return -199 for existing user names
                {
                    transaction.Rollback();
                    return -199;
                }

                foreach (int id in _uspUser.UniqueModuleList.Keys)
                {
                    string sp = "USP_AUT_AddEditUSPUserModuleFeatureOperation";
                    DbCommand cmd2 = CreateCommand(System.Data.CommandType.StoredProcedure, sp);
                    cmd2.Transaction = transaction;
                    cmd2.Parameters.Add(DataAcessUtils.CreateParam("@UserId", DbType.Int32, userID));
                    cmd2.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, id));
                    cmd2.Parameters.Add(DataAcessUtils.CreateParam("@IsGranted", DbType.Int32, _uspUser.UniqueModuleList[id]));
                    cmd2.Parameters.Add(DataAcessUtils.CreateParam("@type", DbType.Int32, 1));
                    cmd2.ExecuteNonQuery();
                }
                foreach (int id in _uspUser.UniqueFeatureList.Keys)
                {
                    string storedProcedureName = "USP_AUT_AddEditUSPUserModuleFeatureOperation";
                    DbCommand cmd2 = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                    cmd2.Transaction = transaction;
                    cmd2.Parameters.Add(DataAcessUtils.CreateParam("@UserId", DbType.Int32, userID));
                    cmd2.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, id));
                    cmd2.Parameters.Add(DataAcessUtils.CreateParam("@IsGranted", DbType.Int32, _uspUser.UniqueFeatureList[id]));
                    cmd2.Parameters.Add(DataAcessUtils.CreateParam("@Type", DbType.Int32, 2));
                    cmd2.ExecuteNonQuery();
                }

                foreach (int id in _uspUser.UniqueOperationIdList.Keys)
                {
                    string storedProcedureName = "USP_AUT_AddEditUSPUserModuleFeatureOperation";
                    DbCommand cmd2 = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                    cmd2.Transaction = transaction;
                    cmd2.Parameters.Add(DataAcessUtils.CreateParam("@UserId", DbType.String, userID));
                    cmd2.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32,id));
                    cmd2.Parameters.Add(DataAcessUtils.CreateParam("@IsGranted", DbType.Int32, _uspUser.UniqueOperationIdList[id]));
                    cmd2.Parameters.Add(DataAcessUtils.CreateParam("@Type", DbType.Int32, 3));
                    //if (uspOperations.OperationExInfo.OperationFieldList.Count > 0)
                    //    cmd2.Parameters.Add(DataAcessUtils.CreateParam("@ExtendedInfo", DbType.Xml, XMLUtils.SerializeDataContractObjectToXML(uspOperations.OperationExInfo)));
                    cmd2.ExecuteNonQuery();
                }
                SaveSubOperations(_uspUser, transaction);
                transaction.Commit();
                _result = 1;

            }
            catch
            {
                transaction.Rollback();
                throw;
            }
            return _result;

        }

        private void SaveSubOperations(USPUser user, DbTransaction transaction)
        {
            foreach (USPSubOperation subOperation in user.SubOperationList)
            {
                string storedProcedureName = "USP_AUT_AddEditUSPSubOperationForUser";
                DbCommand cmd2 = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd2.Transaction = transaction;
                cmd2.Parameters.Add(DataAcessUtils.CreateParam("@UserId", DbType.Int16, user.Id));
                cmd2.Parameters.Add(DataAcessUtils.CreateParam("@SubOperation", DbType.Int32, subOperation.Id));
                cmd2.Parameters.Add(DataAcessUtils.CreateParam("@ExtendedInfo", DbType.Xml, XMLUtils.SerializeDataContractObjectToXML(subOperation.ExtendedInfo)));
                cmd2.ExecuteNonQuery();
            }
        }
    }
}
