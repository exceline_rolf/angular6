﻿using System;
using System.Collections.Generic;
using System.Linq;

using US.Payment.Core.ResultNotifications;
using US.Common.USSAdmin.Data.UserSettings;
using US.Payment.Core.Enums;
using US.Common.Web.UI.Core.SystemObjects;
using US.Common.Web.UI.Core.SystemObjects.ScheduleManagement;
using US.GMS.Core.SystemObjects;

namespace US.Common.USSAdmin.BusinessLogic
{
    public class ManageAdminUsers
    {

        #region UserLogin

        public static USImportResult<USPUserProfileInfo> GetUserProfileInfo(string userName)
        {

            USImportResult<USPUserProfileInfo> result = new USImportResult<USPUserProfileInfo>();
            List<USPModule> uspModuleList = new List<USPModule>();
            List<USPFeature> uspFeatureList = new List<USPFeature>();
            List<USPOperation> uspOperationList = new List<USPOperation>();
            List<USPSubOperation> subOperationList = new List<USPSubOperation>();
            List<int> userBranchesList = new List<int>();

            List<UserBranchSelected> branches = new List<UserBranchSelected>();

            USPUserProfileInfo userProfileInfo = new USPUserProfileInfo();
            USPUser user = new USPUser();

            try
            {
                user = UserSettingsManager.GetUserInfo(userName);
                userProfileInfo.UserId = user.Id;
                userProfileInfo.UserName = userName;
                userProfileInfo.UserHome = user.HomePage;
                userProfileInfo.ExternalUserName = user.ExternalUserName;
                userProfileInfo.Email = user.email;
                userProfileInfo.DisplayName = user.DisplayName;
                userProfileInfo.IsLoginFirstTime = user.IsLoginFirstTime;
                userProfileInfo.IsActive = user.IsActive;

                uspModuleList = UserSettingsManager.GetModulesListForUser(userName);
                uspFeatureList = UserSettingsManager.GetFeaturesForUser(userName);
                uspOperationList = UserSettingsManager.GetOperationsForUser(userName);
                subOperationList = UserSettingsManager.GetSubOperationsForUser(userName);
                branches = UserSettingsManager.GetUserSelectedBranches(userName, userName);

                foreach (UserBranchSelected item in branches)
                {
                    userBranchesList.Add(item.BranchId);
                }
                userProfileInfo.Branches = userBranchesList;

                List<USPModule> cuspModuleList = new List<USPModule>();

                foreach (var userModule in uspModuleList)
                {
                    USPModule uspModule = new USPModule();
                    uspModule.ID = userModule.ID;
                    uspModule.Name = userModule.Name;
                    uspModule.ThumbnailImage = userModule.ThumbnailImage;
                    uspModule.DisplayName = userModule.DisplayName;
                    uspModule.ModuleHome = userModule.ModuleHome;
                    uspModule.ModulePackage = userModule.ModulePackage;
                    uspModule.ModuleColor = userModule.ModuleColor;
                    uspModule.ModuleHomeURL = userModule.ModuleHomeURL;
                    List<USPFeature> uspModuleFeatureList = new List<USPFeature>();

                    foreach (var userModuleFeature in uspFeatureList)
                    {
                        if (userModule.ID == userModuleFeature.ModuleId)
                        {
                            USPFeature uspFeature = new USPFeature();
                            uspFeature.ID = userModuleFeature.ID;
                            uspFeature.Name = userModuleFeature.Name;
                            uspFeature.DisplayName = userModuleFeature.DisplayName;
                            uspFeature.ThumbnailImage = userModuleFeature.ThumbnailImage;
                            uspFeature.HomeUIControl = userModuleFeature.HomeUIControl;
                            uspFeature.FeaturColor = userModuleFeature.FeaturColor;
                            uspFeature.FeatureHomeURL = userModuleFeature.FeatureHomeURL;

                            List<USPOperation> uspFeatureOperationList = new List<USPOperation>();
                            foreach (var userFeatureOperation in uspOperationList)
                            {
                                if (userModuleFeature.ID == userFeatureOperation.FeatureId)
                                {
                                    USPOperation uspfeatureOperation = new USPOperation();
                                    uspfeatureOperation.SubOperationList = userFeatureOperation.SubOperationList;
                                    uspfeatureOperation.ID = userFeatureOperation.ID;
                                    uspfeatureOperation.Name = userFeatureOperation.Name;
                                    uspfeatureOperation.DisplayName = userFeatureOperation.DisplayName;
                                    uspfeatureOperation.OperationHomeURL = userFeatureOperation.OperationHomeURL;
                                    uspfeatureOperation.OperationNameSpace = userFeatureOperation.OperationNameSpace;
                                    uspfeatureOperation.ThumbnailImage = userFeatureOperation.ThumbnailImage;
                                    uspfeatureOperation.OperationColor = userFeatureOperation.OperationColor;
                                    uspfeatureOperation.OperationMode = userFeatureOperation.Mode;
                                    uspfeatureOperation.OperationPackage = userFeatureOperation.OperationPackage;
                                    uspfeatureOperation.IsAddedToWorkStation = Convert.ToInt32(userFeatureOperation.IsAddedToWorkStation);
                                    uspfeatureOperation.OperationHome = Convert.ToString(userFeatureOperation.OperationHome);
                                    uspfeatureOperation.OperationHome = userModule.Name + ";" + userModuleFeature.Name + ";" + userFeatureOperation.Name;
                                    uspFeatureOperationList.Add(uspfeatureOperation);
                                }
                            }
                            uspFeature.Operations = uspFeatureOperationList;
                            uspModuleFeatureList.Add(uspFeature);
                        }
                    }

                    uspModule.Features = uspModuleFeatureList;
                    uspModule.Operations = GetModuleOperations(uspOperationList, uspModule.ID);
                    cuspModuleList.Add(uspModule);
                }
                userProfileInfo.ModuleList = cuspModuleList;
                userProfileInfo.UserSubOperationList = subOperationList;
                result.MethodReturnValue = userProfileInfo;

            }
            catch (Exception ex)
            {
                USNotificationMessage errorMsg = new USNotificationMessage(ErrorLevels.Error, "Error in retriving user profile info : " + ex.Message);
                result.ErrorOccured = true;
                result.AddUSNotificationMessage(errorMsg);
            }
            return result;
        }

        private static List<USPOperation> GetModuleOperations(List<USPOperation> uspOperationList, int p)
        {
            List<USPOperation> moduleoperationList = new List<USPOperation>();
            foreach (USPOperation operation in uspOperationList)
            {
                if (operation.ModuleId == p && operation.FeatureId == -1)
                {
                    moduleoperationList.Add(operation);
                }
            }
            return moduleoperationList;
        }

        public static OperationResultValue<List<string>> GetVersionNoList(string gymCode)
        {
            OperationResultValue<List<string>> result = new OperationResultValue<List<string>>();
            try
            {
                result.OperationReturnValue = UserSettingsManager.GetVersionNoList(gymCode);
            }
            catch (Exception)
            {
                result.ErrorOccured = true;
            }
            return result;
        }

        #endregion

        #region UserSettings


        public static USImportResult<List<UspUsersExtended>> GetUSPUsers(string searchText, bool isActive, string loggedUser)
        {
            USImportResult<List<UspUsersExtended>> result = new USImportResult<List<UspUsersExtended>>();
            try
            {
                var tempUsersList = UserSettingsManager.GetUSPUsers(searchText, isActive, loggedUser);
                var uspUserList = from user in tempUsersList
                                  group user by new
                                  {
                                      user.Id,
                                      user.UserName,
                                      user.DisplayName,
                                      user.RoleId,
                                      user.RoleName,
                                      user.ExternalUser,
                                      user.Email,
                                      user.IsActive,
                                      user.CardNumber,
                                      user.HardwareProfileId
                                  }
                                      into grouping
                                      select new { grouping.Key, grouping };

                List<UspUsersExtended> userList = new List<UspUsersExtended>();

                foreach (var r in uspUserList)
                {
                    UspUsersExtended user = new UspUsersExtended();
                    user.Id = r.Key.Id;
                    user.UserName = r.Key.UserName;
                    user.DisplayName = r.Key.DisplayName;
                    user.RoleId = r.Key.RoleId;
                    user.RoleName = r.Key.RoleName;
                    user.ExternalUser = r.Key.ExternalUser;
                    user.email = r.Key.Email;
                    user.IsActive = r.Key.IsActive;
                    user.CardNumber = r.Key.CardNumber;
                    user.HardwareProfileId = r.Key.HardwareProfileId;

                    foreach (var listing in r.grouping)
                    {
                        if (!user.UniqueModuleList.ContainsKey(listing.UniqueModuleId) && listing.UniqueModuleId > 0)
                        {
                            user.UniqueModuleList.Add(listing.UniqueModuleId, listing.ModuleGranted);
                        }
                        if (!user.UniqueFeatureList.ContainsKey(listing.UniqueFeatureId) && listing.UniqueFeatureId > 0)
                        {
                            user.UniqueFeatureList.Add(listing.UniqueFeatureId, listing.FeatureGranted);
                        }
                        if (!Contains(user.UniqueOperationList, listing.UniqueOperationId) && listing.UniqueOperationId > 0)
                        {
                            USPOperation operation = new USPOperation() { ID = listing.UniqueOperationId };
                            List<USPUserTemp> userTempList = tempUsersList.Where(a => a.UniqueOperationId == listing.UniqueOperationId && a.UserName == user.UserName).ToList();
                            if (userTempList.Count > 0)
                            {
                                operation.OperationExInfo = userTempList[0].OperationExInfo;
                            }
                            user.UniqueOperationList.Add(operation, listing.OperationGranted);
                            user.UniqueOperationIdList.Add(operation.ID
                            , listing.OperationGranted);

                        }
                    }
                    userList.Add(user);
                }

                result.MethodReturnValue = userList;
            }
            catch
            {
                throw;
            }

            return result;
        }

        public static OperationResultValue<string> IsUserAdmin(string user, string gymcode)
        {
            OperationResultValue<string> result = new OperationResultValue<string>();
            try
            {
                result.OperationReturnValue = UserSettingsManager.IsUserAdmin(user, gymcode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting user.code" + ex.Message, USMessageTypes.ERROR);
            }
            return result;
        }

        private static bool Contains(Dictionary<USPOperation, int> dictionary, int p)
        {
            List<USPOperation> keyList = new List<USPOperation>(dictionary.Keys);
            return keyList.Where(a => a.ID == p).ToList().Count > 0;
        }

        public static USImportResult<int> AddEditUSPUsers(UspUsersExtended user, List<UserBranchSelected> branchesList, string loggedUser)
        {
            USImportResult<int> result = new USImportResult<int>();
            try
            {
                result.MethodReturnValue = UserSettingsManager.AddEditUSPUsers(user, branchesList, loggedUser);
            }
            catch
            {
                throw;
            }
            return result;
        }



        public static USImportResult<List<USPRole>> GetUSPRoles(string searchText, bool isActive, string loggedUser)
        {
            USImportResult<List<USPRole>> result = new USImportResult<List<USPRole>>();
            List<USPRole> roleList = new List<USPRole>();
            try
            {

                var uspRoleList = UserSettingsManager.GetUSPRoles(searchText, isActive, loggedUser);
                var uspRole = from role in uspRoleList
                              group role by new
                              {
                                  role.Id,
                                  role.RoleName,
                                  role.Description,
                                  role.IsActive,
                              }
                                  into grouping
                                  select new { grouping.Key, grouping };

                foreach (var r in uspRole)
                {
                    USPRole role = new USPRole();
                    role.Id = r.Key.Id;
                    role.RoleName = r.Key.RoleName;
                    role.Description = r.Key.Description;
                    role.IsActive = r.Key.IsActive;
                    //role.SubOperationList = r.Key.SubOperationList;

                    foreach (var listing in r.grouping)
                    {
                        if (!role.ModuleList.Contains(listing.ModuleId) && listing.ModuleId > 0)
                        {
                            role.ModuleList.Add(listing.ModuleId);
                        }
                        if (!role.FeatureList.Contains(listing.FeatureId) && listing.FeatureId > 0)
                        {
                            role.FeatureList.Add(listing.FeatureId);
                        }
                        if (!Contains(role.OperationList, listing.OperationId) && listing.OperationId > 0)
                        {
                            USPOperation operation = new USPOperation() { ID = listing.OperationId };
                            List<USPRoleTemp> roleTempList = uspRoleList.Where(a => a.OperationId == listing.OperationId && a.RoleName == role.RoleName).ToList();
                            if (roleTempList.Count > 0)
                            {
                                operation.OperationExInfo = roleTempList[0].OperationExInfo;
                            }
                            role.OperationList.Add(operation);
                        }

                    }
                    roleList.Add(role);

                }
                result.MethodReturnValue = roleList;

                //result.MethodReturnValue = US.Payment.Data.Admin.UserSettings.UserSettingsManager.GetUSPRoles();
            }
            catch
            {
                throw;
            }
            return result;
        }

        private static bool Contains(List<USPOperation> list, int p)
        {
            return list.Where(a => a.ID == p).ToList().Count > 0;
        }

        public static USImportResult<int> AddEditUSPRole(USPRole uspRole, string loggedUser)
        {
            USImportResult<int> result = new USImportResult<int>();
            try
            {
                result.MethodReturnValue = UserSettingsManager.AddEditUSPRole(uspRole, loggedUser);
            }
            catch
            {
                throw;
            }
            return result;
        }

        public static USImportResult<int> SaveSubOperations(List<USPSubOperation> subOperations, int roleId, string loggedUser)
        {
            USImportResult<int> result = new USImportResult<int>();
            try
            {
                result.MethodReturnValue = UserSettingsManager.SaveSubOperations(subOperations, roleId, loggedUser);
            }
            catch
            {
                throw;
            }
            return result;
        }

        /// <summary>
        /// Get all the Modules,Features, Operations in the System
        /// </summary>
        /// <returns></returns>
        public static USImportResult<List<USPModule>> GetModulesFeaturesOperations(string loggedUser)
        {
            try
            {
                List<USPModule> moduleList = new List<USPModule>();
                List<USPFeature> featureList = new List<USPFeature>();
                List<USPOperation> operationList = new List<USPOperation>();

                USImportResult<List<USPModule>> result = new USImportResult<List<USPModule>>();

                moduleList = UserSettingsManager.GetModules(loggedUser);
                featureList = UserSettingsManager.GetFeatures(loggedUser);
                operationList = UserSettingsManager.GetOperations(loggedUser);

                List<USPOperation> operationListWithFeature = new List<USPOperation>();
                operationListWithFeature = operationList.Where(a => a.FeatureId > 0).ToList();
                List<USPOperation> operationListWithOutFeature = new List<USPOperation>();
                operationListWithOutFeature = operationList.Where(a => a.FeatureId < 0).ToList();

                List<USPModule> completeModuleList = new List<USPModule>();



                foreach (var userModule in moduleList)
                {
                    USPModule uspModule = new USPModule();
                    uspModule.ID = userModule.ID;
                    uspModule.Name = userModule.Name;
                    uspModule.DisplayName = userModule.DisplayName;
                    uspModule.ModuleHome = userModule.ModuleHome;


                    List<USPFeature> uspModuleFeatureList = new List<USPFeature>();
                    foreach (var userModuleFeature in featureList)
                    {
                        if (userModule.ID == userModuleFeature.ModuleId)
                        {
                            USPFeature uspFeature = new USPFeature();
                            uspFeature.ID = userModuleFeature.ID;
                            uspFeature.Name = userModuleFeature.Name;
                            uspFeature.DisplayName = userModuleFeature.DisplayName;
                            uspFeature.HomeUIControl = userModuleFeature.HomeUIControl;


                            List<USPOperation> uspFeatureOperationList = new List<USPOperation>();
                            foreach (var userFeatureOperation in operationListWithFeature)
                            {
                                if (userModuleFeature.ID == userFeatureOperation.FeatureId)
                                {
                                    USPOperation uspfeatureOperation = new USPOperation();
                                    uspfeatureOperation.ID = userFeatureOperation.ID;
                                    uspfeatureOperation.Name = userFeatureOperation.Name;
                                    uspfeatureOperation.DisplayName = userFeatureOperation.DisplayName;
                                    uspfeatureOperation.OperationNameSpace = userFeatureOperation.OperationNameSpace;
                                    uspfeatureOperation.ThumbnailImage = userFeatureOperation.ThumbnailImage;
                                    uspfeatureOperation.OperationMode = userFeatureOperation.Mode;
                                    uspfeatureOperation.OperationExInfo = userFeatureOperation.OperationExInfo;
                                    uspfeatureOperation.IsAddedToWorkStation = Convert.ToInt32(userFeatureOperation.IsAddedToWorkStation);
                                    uspfeatureOperation.OperationHome = Convert.ToString(userFeatureOperation.OperationHome);
                                    uspfeatureOperation.OperationHome = userModule.Name + ";" + userModuleFeature.Name + ";" + userFeatureOperation.Name;
                                    uspFeatureOperationList.Add(uspfeatureOperation);
                                }

                            }
                            uspFeature.Operations = uspFeatureOperationList;

                            uspModuleFeatureList.Add(uspFeature);
                        }
                        uspModule.Features = uspModuleFeatureList;
                    }

                    List<USPOperation> uspFeatureLessOperationList = new List<USPOperation>();
                    foreach (var userModuleOperation in operationListWithOutFeature)
                    {
                        if (userModule.ID == userModuleOperation.ModuleId)
                        {
                            USPOperation uspfeatureOperation = new USPOperation();
                            uspfeatureOperation.ID = userModuleOperation.ID;
                            uspfeatureOperation.Name = userModuleOperation.Name;
                            uspfeatureOperation.DisplayName = userModuleOperation.DisplayName;
                            uspfeatureOperation.OperationNameSpace = userModuleOperation.OperationNameSpace;
                            uspfeatureOperation.ThumbnailImage = userModuleOperation.ThumbnailImage;
                            uspfeatureOperation.OperationMode = userModuleOperation.Mode;
                            uspfeatureOperation.IsAddedToWorkStation = Convert.ToInt32(userModuleOperation.IsAddedToWorkStation);
                            uspfeatureOperation.OperationExInfo = userModuleOperation.OperationExInfo;
                            uspfeatureOperation.OperationHome = Convert.ToString(userModuleOperation.OperationHome);
                            uspfeatureOperation.OperationHome = userModule.Name + ";" + userModuleOperation.Name + ";" + userModuleOperation.Name;
                            uspFeatureLessOperationList.Add(uspfeatureOperation);
                        }
                    }
                    uspModule.Operations = uspFeatureLessOperationList;

                    completeModuleList.Add(uspModule);
                }
                result.MethodReturnValue = completeModuleList;
                return result;

            }
            catch
            {
                throw;
            }
        }


        public static OperationResultValue<List<UserBranchSelected>> GetUserSelectedBranches(string userName, string loggedUser)
        {
            OperationResultValue<List<UserBranchSelected>> result = new OperationResultValue<List<UserBranchSelected>>();
            try
            {
                result.OperationReturnValue = UserSettingsManager.GetUserSelectedBranches(userName, loggedUser);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting Branches" + ex.Message, USMessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResultValue<bool> CheckUserNameAvailability(string userName)
        {
            OperationResultValue<bool> result = new OperationResultValue<bool>();
            try
            {
                result.OperationReturnValue = UserSettingsManager.CheckUserNameAvailability(userName);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in checking user name avialability" + ex.Message, USMessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResultValue<bool> CheckEmailAvailability(string email, string loggedUser)
        {
            OperationResultValue<bool> result = new OperationResultValue<bool>();
            try
            {
                result.OperationReturnValue = UserSettingsManager.CheckEmailAvailability(email, loggedUser);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in checking user name avialability" + ex.Message, USMessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResultValue<bool> EditUSPUserBasicInfo(bool isPasswdChange, USPUser uspUser, string loggedUser)
        {
            OperationResultValue<bool> result = new OperationResultValue<bool>();
            try
            {
                result.OperationReturnValue = UserSettingsManager.EditUSPUserBasicInfo(isPasswdChange, uspUser, loggedUser);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in editing user basic info" + ex.Message, USMessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResultValue<string> ValidateUser(string username, string password)
        {
            OperationResultValue<string> result = new OperationResultValue<string>();
            try
            {
                result.OperationReturnValue = UserSettingsManager.ValidateUser(username, password);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in user validation" + ex.Message, USMessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResultValue<USPUser> ValidateUserWithCard(string cardNumber, string user)
        {
            OperationResultValue<USPUser> result = new OperationResultValue<USPUser>();
            try
            {
                result.OperationReturnValue = UserSettingsManager.ValidateUserWithCard(cardNumber, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in user validation use with card" + ex.Message, USMessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResultValue<List<USPSubOperation>> GetSubOperationsForRole(int roleId, string loggedUser)
        {
            OperationResultValue<List<USPSubOperation>> result = new OperationResultValue<List<USPSubOperation>>();
            try
            {
                result.OperationReturnValue = UserSettingsManager.GetSubOperationsFoleRole(roleId, loggedUser);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting sub operations for Role" + ex.Message, USMessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResultValue<List<USPSubOperation>> GetSubOperationsForUser(string userName)
        {
            OperationResultValue<List<USPSubOperation>> result = new OperationResultValue<List<USPSubOperation>>();
            try
            {
                result.OperationReturnValue = UserSettingsManager.GetSubOperationsForUser(userName);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting sub operations for user" + ex.Message, USMessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResultValue<USPRole> GetDefaultUspRole(string loggedUser)
        {
            OperationResultValue<USPRole> result = new OperationResultValue<USPRole>();
            try
            {
                result.OperationReturnValue = UserSettingsManager.GetDefaultUspRole(loggedUser);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting defalt usp roles" + ex.Message, USMessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResultValue<List<USPEmployee>> SearchUSPEmployees(string searchText, bool isActive, string loggedUser)
        {
            OperationResultValue<List<USPEmployee>> result = new OperationResultValue<List<USPEmployee>>();
            try
            {
                result.OperationReturnValue = UserSettingsManager.SearchUSPEmployees(searchText, isActive, loggedUser);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in search usp employees" + ex.Message, USMessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResultValue<List<UserBranch>> GetBranches(string user)
        {
            OperationResultValue<List<UserBranch>> result = new OperationResultValue<List<UserBranch>>();
            try
            {
                result.OperationReturnValue = UserSettingsManager.GetBranches(user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting Branches" + ex.Message, USMessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResultValue<bool> CheckCardNumberAvilability(string cardNumber, string loggedUser)
        {
            OperationResultValue<bool> result = new OperationResultValue<bool>();
            try
            {
                result.OperationReturnValue = UserSettingsManager.CheckCardNumberAvilability(cardNumber, loggedUser);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in card number avilability checker" + ex.Message, USMessageTypes.ERROR);
            }
            return result;
        }


        public static OperationResultValue<List<ExceCategory>> GetCategories(string type, string loggedUser)
        {
            OperationResultValue<List<ExceCategory>> result = new OperationResultValue<List<ExceCategory>>();
            try
            {
                result.OperationReturnValue = UserSettingsManager.GetCategories(type, loggedUser);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting categories" + ex.Message, USMessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResultValue<List<ExceHardwareProfile>> GetHardwareProfiles(int branchId, string user)
        {
            OperationResultValue<List<ExceHardwareProfile>> result = new OperationResultValue<List<ExceHardwareProfile>>();
            try
            {
                result.OperationReturnValue = UserSettingsManager.GetHardwareProfiles(branchId, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Hardware Profiles" + ex.Message, USMessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResultValue<bool> SaveUserCulture(string culture, string user)
        {
            OperationResultValue<bool> result = new OperationResultValue<bool>();
            try
            {
                result.OperationReturnValue = UserSettingsManager.SaveUserCulture(culture, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in saving culture" + ex.Message, USMessageTypes.ERROR);
            }
            return result;
        }


        #endregion


        public static int GetLoginGymByUser(string user, string gymCode)
        {
            try
            {
                return UserSettingsManager.GetLoginGymByUser(user, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
