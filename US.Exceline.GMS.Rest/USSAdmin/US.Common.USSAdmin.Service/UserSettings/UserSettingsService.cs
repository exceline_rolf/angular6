﻿#region File Header
// --------------------------------------------------------------------------
// Copyright(c) <2010> Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USAR
// Project Name      : US.Payment.Service
// Coding Standard   : US Coding Standards
// Author            : NHE
// Created Timestamp : 01/12/2011 HH:MM  PM
// --------------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using US.Common.USSAdmin.Service.UserSettings;

using US.Common.USSAdmin.Service.DataContracts;
using US.Common.USSAdmin.API;
using US.Common.Logging.API;
using US.Exceline.GMS.Modules.Admin.API.ManageEmployees;
using US.Payment.Core.ResultNotifications;
using US.Common.Web.UI.Core.SystemObjects;
using US.Common.Web.UI.Core.SystemObjects.ScheduleManagement;
using US.Payment.Core.Enums;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.Utils;
using US.GMS.Core.SystemObjects;

namespace US.Common.USSAdmin
{

    public partial class USSService : IUserSettingsService
    {
        #region IUserSettingsService Members

       /// <summary>
       /// Get All the Usesr in the System for admin purposes
       /// </summary>
       /// <returns></returns>
        public List<UspUsersExtended> GetUSPUsers(string searchText, bool isActive, string loggedUser)
        {
            return AdminUserSettings.GetUSPUsers(searchText, isActive, loggedUser).MethodReturnValue; 
        }

        public int AddEditUSPUsers(UspUsersExtended user, List<UserBranchSelected> branchesList, string loggedUser)
        {
            return AdminUserSettings.AddEditUSPUsers(user, branchesList, loggedUser).MethodReturnValue; 
        }

        public List<USPRole> GetUSPRoles(string searchText, bool isActive, string loggedUser)
        {
            return AdminUserSettings.GetUSPRoles(searchText, isActive, loggedUser).MethodReturnValue;
        }

       

        public int AddEditUSPRole(USPRole uspRole, string loggedUser)
        {
            return AdminUserSettings.AddEditUSPRole(uspRole, loggedUser).MethodReturnValue;
            
        }

        public int SaveSubOperations(List<USPSubOperation> subOperations, int roleId, string loggedUser)
        {
            return AdminUserSettings.SaveSubOperations(subOperations, roleId, loggedUser).MethodReturnValue;

        }

       

        /// <summary>
        /// Get All the Methods, Features, Operations in the system
        /// </summary>
        /// <returns></returns>
        public List<USPModuleDC> GetModulesFeaturesOperations(string loggedUser)
        {
            try
            {
                List<USPModule> moduleList = AdminUserSettings.GetModulesFeaturesOperations(loggedUser).MethodReturnValue;
                List<USPModuleDC> uspModuleDCList = new List<USPModuleDC>();


                foreach (var item in moduleList)
                {
                    USPModuleDC mod = new USPModuleDC();
                    mod.DeafaultFeature = GetServiceFeature(item.DeafaultFeature);
                    mod.DisplayName = item.DisplayName;
                    mod.ModuleId = item.ID;
                    mod.Features = GetServiceFeatures(item.Features);
                    mod.ID = item.Name;  //wrong usage
                    mod.ModuleHome = item.ModuleHome;
                    mod.Operations = GetServiceOparations(item.Operations);
                    mod.ThumbnailImage = item.ThumbnailImage;
                    uspModuleDCList.Add(mod);
                }

                return uspModuleDCList;
            }
            catch(Exception ex)
            {
                US.Common.Logging.API.USLogEvent.WriteToFile(ex.Message, loggedUser);
            }
            return null;
        }

        private OperationDC GetServiceOparation(USPOperation item)
        {
            if (item == null)
            {
                return null;
            }
            OperationDC operation = new OperationDC();
            operation.DisplayName = item.DisplayName;
            operation.ID = item.Name;
            operation.OperationId = item.ID;
            operation.OperationHome = item.OperationHome;
            operation.OperationNameSpace = item.OperationNameSpace;
            operation.ThumbnailImage = item.ThumbnailImage;
            operation.Mode = item.Mode;
            operation.RoleId = item.RoleId;
            operation.IsAddedToWorkStation = item.IsAddedToWorkStation;
            operation.FieldList = GetOperationFieldList(item.OperationExInfo);
            return operation;
        }

        private List<OperationFieldDC> GetOperationFieldList(OperationExtendedInfo operationExtendedInfo)
        {
            List<OperationFieldDC> ofdcList = new List<OperationFieldDC>();
            foreach (OperationField of in operationExtendedInfo.OperationFieldList)
            {
                OperationFieldDC fdc = new OperationFieldDC();
                fdc.DisplayName = of.DisplayName;
                fdc.Status = of.FieldStatus.ToString();
                ofdcList.Add(fdc);
            }
            return ofdcList;
        }

        private List<USPFeatureDC> GetServiceFeatures(List<USPFeature> list)
        {
            if (list == null)
            {
                return null;
            }
            List<USPFeatureDC> features = new List<USPFeatureDC>();

            foreach (var item in list)
            {
                features.Add(GetServiceFeature(item));
            }
            return features;
        }

        private USPFeatureDC GetServiceFeature(USPFeature uSPFeature)
        {
            if (uSPFeature == null)
            {
                return null;
            }
            USPFeatureDC feature = new USPFeatureDC();
            if (uSPFeature != null)
            {
                feature.DisplayName = uSPFeature.DisplayName;
                feature.HomeUIControl = uSPFeature.HomeUIControl;
                feature.ID = uSPFeature.Name;
                feature.FeatureId = uSPFeature.ID;
                feature.Operations = GetServiceOparations(uSPFeature.Operations);
                feature.Priority = uSPFeature.Priority;
                feature.ThumbnailImage = uSPFeature.ThumbnailImage;
            }
            return feature;
        }


        private List<OperationDC> GetServiceOparations(List<USPOperation> list)
        {
            if (list == null)
            {
                return null;
            }
            List<OperationDC> oparations = new List<OperationDC>();
            foreach (var item in list)
            {
                oparations.Add(GetServiceOparation(item));
            }

            return oparations;
        }

        public List<UserBranch> GetBranches(string user)
        {
            OperationResultValue<List<UserBranch>> result = AdminUserSettings.GetBranches(user);
            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<UserBranch>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<UserBranchSelected> GetUserSelectedBranches(string userName,string loggedUser)
        {
            OperationResultValue<List<UserBranchSelected>> result = AdminUserSettings.GetUserSelectedBranches(userName,loggedUser);
            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), userName);
                }
                return new List<UserBranchSelected>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool CheckUserNameAvailability(string userName,string loggedUser)
        {
            OperationResultValue<bool> result = AdminUserSettings.CheckUserNameAvailability(userName);
            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), userName);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool CheckEmailAvailability(string email, string loggedUser)
        {
            OperationResultValue<bool> result = AdminUserSettings.CheckEmailAvailability(email, loggedUser);
            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), email);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool EditUSPUserBasicInfo(bool isPasswdChange, USPUserDC uspUser, string loggedUser)
        {
            USPUser user = new USPUser();

            user.Id = uspUser.UserID;
            user.DisplayName = uspUser.DisplayName;
            user.email = uspUser.Email;
            user.ExternalUserName = uspUser.UserExternalName;
            user.passowrd = uspUser.Password;
            user.IsLoginFirstTime = uspUser.IsLoginFirstTime;

            OperationResultValue<bool> result = AdminUserSettings.EditUSPUserBasicInfo(isPasswdChange, user, loggedUser);
            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), loggedUser);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


       
        #endregion



        public List<USPSubOperation> GetSubOperationsForRole(int roleId, string loggedUser)
        {
            OperationResultValue<List<USPSubOperation>> result = AdminUserSettings.GetSubOperationsForRole(roleId, loggedUser);
            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), loggedUser);
                }
                return new List<USPSubOperation>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        public USPRole GetDefaultUspRole(string loggedUser)
        {
            OperationResultValue<USPRole> result = AdminUserSettings.GetDefaultUspRole(loggedUser);
            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), loggedUser);
                }
                return new USPRole();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        public List<USPSubOperation> GetSubOperationsForUser(string userName,string loggeduser)
        {
            OperationResultValue<List<USPSubOperation>> result = AdminUserSettings.GetSubOperationsForUser(userName);
            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), userName);
                }
                return new List<USPSubOperation>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<GymEmployeeDC> SearchUSPEmployees(string searchText, bool isActive, string loggedUser)
        {

            OperationResult<List<GymEmployeeDC>> result = GMSGymEmployee.GetGymEmployees(-2, searchText, isActive, -1, ExceConnectionManager.GetGymCode(loggedUser));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), loggedUser);
                }
                return new List<GymEmployeeDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }

            //OperationResultValue<List<USPEmployee>> result = AdminUserSettings.SearchUSPEmployees(searchText, isActive, loggedUser);
            //if (result.ErrorOccured)
            //{
            //    foreach (USNotificationMessage message in result.Notifications)
            //    {
            //        USLogError.WriteToFile(message.Message, new Exception(), loggedUser);
            //    }
            //    return new List<USPEmployee>();
            //}
            //else
            //{
            //    return result.OperationReturnValue;
            //}
        }

        public USPUserDC GetUSPUserInfo(string userName)
        {
            USPUserDC uspUserDC = new USPUserDC();

            US.Common.Logging.API.USLogEvent.WriteToFile("Get User's Profile Process Info Started", userName);
            //USImportResult<USPUserProfileInfo> uspUserProfileInfo = US.Payment.API.User.GetUserProfileInfo(userName);
            //TODO
            USImportResult<USPUserProfileInfo> uspUserProfileInfo = null;
            US.Common.Logging.API.USLogEvent.WriteToFile("Get User's Profile Info finished", userName);

            if (uspUserProfileInfo.ErrorOccured)
            {
                foreach (USNotificationMessage message in uspUserProfileInfo.NotificationMessages)
                {
                    if (message.ErrorLevel == ErrorLevels.Error)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), userName);
                    }
                }
                return new USPUserDC();
            }
            else
            {
                uspUserDC.UserID = uspUserProfileInfo.MethodReturnValue.UserId;
                uspUserDC.UserName = uspUserProfileInfo.MethodReturnValue.UserName;
                uspUserDC.HomePage = uspUserProfileInfo.MethodReturnValue.UserHome;
                uspUserDC.UserID = uspUserProfileInfo.MethodReturnValue.UserId;
                uspUserDC.Branches = uspUserProfileInfo.MethodReturnValue.Branches;
                uspUserDC.Email = uspUserProfileInfo.MethodReturnValue.Email;
                uspUserDC.DisplayName = uspUserProfileInfo.MethodReturnValue.DisplayName;
                uspUserDC.IsLoginFirstTime = uspUserProfileInfo.MethodReturnValue.IsLoginFirstTime;

                foreach (var item in uspUserProfileInfo.MethodReturnValue.ModuleList)
                {
                    USPModuleDC mod = new USPModuleDC();
                    mod.DeafaultFeature = GetServiceFeature(item.DeafaultFeature);
                    mod.DisplayName = item.DisplayName;
                    mod.ModuleId = item.ID;
                    mod.Features = GetServiceFeatures(item.Features);
                    mod.ID = item.Name;  //wrong usage
                    mod.ModuleHome = item.ModuleHome;
                    mod.Operations = GetServiceOparations(item.Operations);
                    mod.ThumbnailImage = item.ThumbnailImage;
                    mod.ModuleColor = item.ModuleColor;
                    mod.ModulePackage = item.ModulePackage;
                    uspUserDC.ModuleList.Add(mod);
                }
                uspUserDC.SubOperationList = uspUserProfileInfo.MethodReturnValue.UserSubOperationList;
                return uspUserDC;
            }
        }



        public bool CheckCardNumberAvilability(string cardNumber, string loggedUser)
        {
            OperationResultValue<bool> result = AdminUserSettings.CheckCardNumberAvilability(cardNumber, loggedUser);
            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), loggedUser);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }


        public OperationInfo GetOperationInfo(string loggedUser)
        {
            throw new NotImplementedException();
        }


        public string ValidateUser(string useName, string password)
        {
            OperationResultValue<string> result = AdminUserSettings.ValidateUser(useName, password);
            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), string.Empty);
                }
                return "0";
            }
            else
            {
                return result.OperationReturnValue;
            }           
        }

        public List<ExceCategoryDC> GetCategories(string type, string loggedUser)
        {
            OperationResultValue<List<ExceCategory>> result = AdminUserSettings.GetCategories(type, loggedUser);
            List<ExceCategoryDC> exceCategoryDCList = new List<ExceCategoryDC>();
            
            foreach(ExceCategory category in result.OperationReturnValue)
            {
                ExceCategoryDC categoryDC = new ExceCategoryDC();
                categoryDC.Id = category.Id;
                categoryDC.Name = category.Name;
                exceCategoryDCList.Add(categoryDC);
            }

            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), string.Empty);
                }
                return new List<ExceCategoryDC>();
            }
            else
            {
                return exceCategoryDCList;
            }
        }

        public bool test(bool test) 
        {
            return test;
        }

        public List<ExceHardwareProfileDC> GetHardwareProfiles(int branchId, string user)
        {
            OperationResultValue<List<ExceHardwareProfile>> result = AdminUserSettings.GetHardwareProfiles(branchId, user);
            List<ExceHardwareProfileDC> exceCategoryDCList = new List<ExceHardwareProfileDC>();

            foreach (ExceHardwareProfile hardwareProfile in result.OperationReturnValue)
            {
                ExceHardwareProfileDC hardwareProfileDC = new ExceHardwareProfileDC();
                hardwareProfileDC.Id = hardwareProfile.Id;
                hardwareProfileDC.Name = hardwareProfile.Name;
                exceCategoryDCList.Add(hardwareProfileDC);
            }
            if (result.ErrorOccured)
            {
                foreach (USNotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExceHardwareProfileDC>();
            }
            else
            {
                return exceCategoryDCList;
            }
        }

        


    }
}
