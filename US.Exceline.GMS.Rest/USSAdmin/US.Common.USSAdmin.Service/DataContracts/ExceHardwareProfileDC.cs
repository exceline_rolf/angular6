﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.Common.USSAdmin.Service.DataContracts
{
   [DataContract]
    public class ExceHardwareProfileDC
    {
        private int _id;
         [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _name = string.Empty;
         [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private int _branchId = -1;
         [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private string _categoryListString = string.Empty;
         [DataMember]
        public string CategoryListString
        {
            get { return _categoryListString; }
            set { _categoryListString = value; }
        }
    }
}
