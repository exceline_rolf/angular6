﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Data.SystemObjects;
using US.GMS.Data.DataAdapters.SQLServer;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.DomainObjects.Common;

namespace US.GMS.Data.DataAdapters
{
    public class MemberFacade
    {
        private static IDataAdapter GetDataAdapter()
        {
            return new SQLServerDataAdapter();
        }

        public static List<OrdinaryMemberDC> GetMembersForCommonBooking(int branchId, string searchText, string gymCode)
        {
            return GetDataAdapter().GetMembersForCommonBooking(branchId, searchText, gymCode);
        }

        public static List<ActivityDC> GetActivities(int branchId, string gymCode)
        {
            return GetDataAdapter().GetActivities(branchId, gymCode);
        }

        public static List<EntityVisitDC> GetVistsByDate(DateTime selectedDate, int branchId, string systemName, string gymCode, string user)
        {
            return GetDataAdapter().GetVistsByDate( selectedDate,  branchId,  systemName,  gymCode, user);
        }

        public static OrdinaryMemberDC GetMembersById(int branchId, int memberId, string gymCode)
        {
            return GetDataAdapter().GetMembersById(branchId, memberId, gymCode);
        }

        public static List<ActivityDC> GetActivitiesForCommonBooking(int entityId, string entityType, int memberId, string gymCode)
        {
            return GetDataAdapter().GetActivitiesForCommonBooking(entityId, entityType, memberId, gymCode);
        }

        public static bool ManageMemberShopAccount(MemberShopAccountDC memberShopAccount, string mode, string user, int branchId, int salePointId, string gymCode)
        {
            return GetDataAdapter().ManageMemberShopAccount(memberShopAccount, mode, user, branchId, salePointId, gymCode);
        }

        public static MemberShopAccountDC GetMemberShopAccounts(int memberId, int hit, string gymCode, bool itemsNeeded)
        {
            return GetDataAdapter().GetMemberShopAccounts(memberId, hit, gymCode, itemsNeeded);
        }

        public static List<MemberShopAccountItemDC> GetMemberShopAccountItems(int shopAccountId, string gymCode, int branchId)
        {
            return GetDataAdapter().GetMemberShopAccountItems(shopAccountId, gymCode, branchId);
        }

        public static decimal GetSessionTimeOutAfter(int branchId, string gymCode)
        {
            return GetDataAdapter().GetSessionTimeOutAfter(branchId, gymCode);
        }

        public static int CheckEmployeeByEntNo(int entityNO, string gymCode)
        {
            return GetDataAdapter().CheckEmployeeByEntNo(entityNO, gymCode); ;
        }

        public static List<CountryDC> GetCountryDetails(string gymCode)
        {
            return GetDataAdapter().GetCountryDetails(gymCode);
        }

        public static List<LanguageDC> GetLangugeDetails(string gymCode)
        {
            return GetDataAdapter().GetLangugeDetails(gymCode);
        }

        public static int SavePostalArea(string user, string postalCode, string postalArea,Int64 population, Int64 houseHold, string gymCode)
        {
            return GetDataAdapter().SavePostalArea(user, postalCode, postalArea,population, houseHold, gymCode);
        }

        public static int UpdateMemberStatus(string gymCode, int branchId)
        {
            return GetDataAdapter().UpdateMemberStatus(gymCode, branchId);
        }

        public static List<TrainingProgramVisitDetailDC> GetMemberVisitForTrainingProgram(string gymCode, int branchId, string systemName, DateTime lastUpdatedDateTime)
        {
            return GetDataAdapter().GetMemberVisitForTrainingProgram(gymCode, branchId, systemName, lastUpdatedDateTime);
        }

        public static List<TrainingProgramClassVisitDetailDC> GetMemberClassVisitForTrainingProgram(string gymCode, int branchId, string systemName, DateTime lastUpdatedDateTime)
        {
            return GetDataAdapter().GetMemberClassVisitForTrainingProgram(gymCode, branchId, systemName, lastUpdatedDateTime);
        }

        public static bool AddRegionDetails(RegionDC region, string gymCode)
        {
            return GetDataAdapter().AddRegionDetails(region, gymCode);
        }

        public static bool AddCountryDetails(CountryDC country, string gymCode)
        {
            return GetDataAdapter().AddCountryDetails(country, gymCode);
        }

        public static MemberFeeGenerationResultDC GenerateMemberFee(List<int> gyms, int month, string gymCode, string user)
        {
            return GetDataAdapter().GenerateMemberFee(gyms, month, gymCode, user);
        }

        public static List<ExcelineMemberDC> GetMembersForShop(int branchId, string searchText, int status, MemberRole memberRole, int hit, string gymCode)
        {
            return GetDataAdapter().GetMembersForShop(branchId, searchText, status, memberRole, hit, gymCode);
        }
        public static List<ExcelineMemberDC> GetAllMemberAndCompany(int branchId, string searchText, int status, int hit, string gymCode)
        {
            return GetDataAdapter().GetAllMemberAndCompany(branchId, searchText, status, hit, gymCode);
        }

        public static bool ValidateGenerateMemberFeeWithMemberFeeMonth(List<int> gyms, string gymCode, string user)
        {
            return GetDataAdapter().ValidateGenerateMemberFeeWithMemberFeeMonth(gyms, gymCode, user);
        }

        public static List<string> ConfirmationDetailForGenerateMemberFee(List<int> gyms, int month, string gymCode, string user)
        {
            return GetDataAdapter().ConfirmationDetailForGenerateMemberFee(gyms, month, gymCode, user);
        }

        public static List<GymEmployeeDC> GetEmployee(int aciveStatus, string gymCode)
        {
            return GetDataAdapter().GetEmployee(aciveStatus, gymCode);
        }

        public static MemberShopAccountDC GetMemberShopAccounts(int memberId, string entityType, int hit, string gymCode, bool itemsNeeded)
        {
            return GetDataAdapter().GetMemberShopAccounts(memberId, entityType, hit, gymCode, itemsNeeded);
        }
    }
}
