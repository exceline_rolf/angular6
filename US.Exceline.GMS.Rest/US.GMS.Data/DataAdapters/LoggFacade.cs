﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Data.SystemObjects;
using US.GMS.Data.DataAdapters.SQLServer;

namespace US.GMS.Data.DataAdapters
{
    public class LoggFacade
    {

        private static IDataAdapter GetDataAdapter()
        {
            return new SQLServerDataAdapter();
        }

        public static int SaveShopXMLData(String xmlData, String sessionKey , string gymCode)
        {
            return GetDataAdapter().SaveShopXMLData(xmlData, sessionKey ,gymCode);
        }
    }
}
