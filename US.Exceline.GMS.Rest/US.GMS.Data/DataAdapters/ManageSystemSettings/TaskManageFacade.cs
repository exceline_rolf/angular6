﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : JNI
// Created Timestamp : "5/3/2012 5:08:16 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Data.SystemObjects;
using US.GMS.Data.DataAdapters.SQLServer;
using US.GMS.Core.DomainObjects.Common;
using US.Common.Notification.Core.DomainObjects;

namespace US.GMS.Data.DataAdapters.ManageSystemSettings
{
    public class TaskManageFacade
    {
        private static IDataAdapter GetDataAdapter()
        {
            return new SQLServerDataAdapter();
        }

        #region Task Template
        public static bool SaveTaskTemplate(TaskTemplateDC smsTemplate, bool isEdit, string gymCode)
        {
            return GetDataAdapter().SaveTaskTemplate(smsTemplate, isEdit, gymCode);
        }

        public static bool UpdateTaskTemplate(TaskTemplateDC smsTemplate, string gymCode)
        {
            return GetDataAdapter().UpdateTaskTemplate(smsTemplate, gymCode);
        }

        public static bool DeleteTaskTemplate(int smsTemplateId, string gymCode)
        {
            return GetDataAdapter().DeleteTaskTemplate(smsTemplateId, gymCode);
        }

        public static bool DeActivateTaskTemplate(int smsTemplateId, string gymCode)
        {
            return GetDataAdapter().DeActivateTaskTemplate(smsTemplateId, gymCode);
        }

        public static List<TaskTemplateDC> GetTaskTemplates(TaskTemplateType templateType, int branchId, int templateId, string gymCode)
        {
            return GetDataAdapter().GetTaskTemplates(templateType, branchId, templateId, gymCode);
        }

        public static List<ExtendedTaskTemplateDC> GetTaskTemplateExtFields(int extFieldId, string gymCode)
        {
            return GetDataAdapter().GetTaskTemplateExtFields(extFieldId, gymCode);
        }

        public static List<ExtendedFieldDC> GetExtFieldsByCategory(int categoryId, string categoryType, string gymCode)
        {
            return GetDataAdapter().GetExtFieldsByCategory(categoryId,categoryType, gymCode);
        }

        #endregion

        public static int SaveEntityTask(ExcelineTaskDC excelineTask,string gymCode)
        {
            return GetDataAdapter().AddEntityTask(excelineTask, gymCode);
        }

        public static decimal GetSessionTimeOutAfter(int branchId, string gymCode)
        {
            return GetDataAdapter().GetSessionTimeOutAfter(branchId, gymCode);
        }

        #region Exceline Task
        public static int SaveExcelineTask(ExcelineTaskDC excelineTask, string gymCode)
        {
            return GetDataAdapter().SaveExcelineTask(excelineTask, gymCode);
        }

        public static int EditExcelineTask(ExcelineTaskDC excelineTask, string gymCode)
        {
            return GetDataAdapter().EditExcelineTask(excelineTask, gymCode);
        }


        public static List<ExcelineTaskDC> GetExcelineTasks(int branchId, bool isFxied, int templateId, int assignTo, string roleType, int followupMemberId, string gymCode)
        {
            return GetDataAdapter().GetExcelineTasks(branchId, isFxied, templateId, assignTo, roleType, followupMemberId, gymCode);
        }

        public static List<ExcelineTaskDC> SearchExcelineTask(string searchText, TaskTemplateType type, int followUpMemId, int branchId, bool isFxied, string gymCode)
        {
            return GetDataAdapter().SearchExcelineTask(searchText, type, followUpMemId, branchId, isFxied, gymCode);
        }

        public static bool SaveTaskWithOutSchedule(ExcelineTaskDC excelineTask, string gymCode)
        {
            return GetDataAdapter().SaveTaskWithOutSchedule(excelineTask, gymCode);
        }

        #endregion

        #region Task Categories
        public static bool SaveTaskCategory(ExcelineTaskCategoryDC taskCategory, bool isEdit, string gymCode)
        {
            return GetDataAdapter().SaveTaskCategory(taskCategory, isEdit, gymCode);
        }

        public static List<ExcelineTaskCategoryDC> GetTaskCategories(int branchId,string gymCode)
        {
            return GetDataAdapter().GetTaskCategories(branchId, gymCode);
        }

        public static bool DeleteTaskCategory(int taskCategoryId, string gymCode)
        {
            return GetDataAdapter().DeleteTaskCategory(taskCategoryId, gymCode);
        }
        #endregion

        #region Job Categories
        public static bool SaveJobCategory(ExcelineJobCategoryDC jobCategory, bool isEdit, string gymCode)
        {
            return GetDataAdapter().SaveJobCategory(jobCategory, isEdit, gymCode);
        }

        public static List<ExcelineJobCategoryDC> GetJobCategories(int branchId, string gymCode)
        {
            return GetDataAdapter().GetJobCategories(branchId, gymCode);
        }

        public static bool DeleteJobCategory(int jobCategoryId, string gymCode)
        {
            return GetDataAdapter().DeleteJobCategory(jobCategoryId, gymCode);
        }

        public static bool AddEventToNotifications(USCommonNotificationDC notification, string gymCode)
        {
            return GetDataAdapter().AddEventToNotifications(notification, gymCode);
        }
        #endregion

    }
}
