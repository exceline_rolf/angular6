﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NAd
// Created Timestamp : "2013/04/29"
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class AddSponsorInstallmentAction : USDBActionBase<int>
    {
        private InstallmentDC _installment = new InstallmentDC();
        private int _branchId = -1;

        public AddSponsorInstallmentAction(InstallmentDC installment, int branchId)
        {
            _installment = installment;
            _branchId = branchId;
        }

        protected override int Body(DbConnection connection)
        {
            int result = -1;
            string StoredProcedureName = "USExceGMSManageMembershipSaveSponsorInstallment";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberContractID", DbType.Int32, _installment.MemberContractId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@text", DbType.String, _installment.Text));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _installment.MemberId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@createdUser", DbType.String, _installment.CreatedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@NoOfVisits", DbType.Int32, _installment.NoOfVisits));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberInstallmentID", DbType.Int32, _installment.Id));

                DbParameter outputPara = new SqlParameter();
                outputPara.DbType = DbType.Int32;
                outputPara.ParameterName = "@outId";
                outputPara.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outputPara);
                cmd.ExecuteNonQuery();
                result = Convert.ToInt32(outputPara.Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}
