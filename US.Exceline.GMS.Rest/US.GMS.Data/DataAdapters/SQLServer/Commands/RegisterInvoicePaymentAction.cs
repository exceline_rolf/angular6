﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Common.Logging.API;
using US.Exceline.GMS.Modules.Economy.API;
using US.Exceline.GMS.Modules.Economy.Core;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums;
using US.GMS.Core.DomainObjects.ManageShop;
using US.GMS.Core.SystemObjects;
using US_DataAccess;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class RegisterInvoicePaymentAction : USDBActionBase<SaleResultDC>
    {
        private PaymentDetailDC _paymentDetails = null;
        private int _loggedBranchID = -1;
        private string _user = string.Empty;
        private ShopSalesDC _salesDetails = null;
             

        public RegisterInvoicePaymentAction(PaymentDetailDC paymentDetails, string gymCode,  int loggedbranchID, ShopSalesDC salesDetails, string user)
        {
            _paymentDetails = paymentDetails;
            _loggedBranchID = loggedbranchID;
            _user = user;
            _salesDetails = salesDetails;
        }

        protected override SaleResultDC Body(System.Data.Common.DbConnection connection)
        {
            DbTransaction transaction = null;

            IUSPClaim claim = new USPClaim();
            IUSPDebtor debtor = new USPDebtor();
            IUSPCreditor creditor = new USPCreditor();
            SaleResultDC saleResult = new SaleResultDC();
            try
            {
                transaction = connection.BeginTransaction();
                debtor.DebtorcustumerID = Convert.ToString(_paymentDetails.CustId);
                claim.InvoiceNumber = _paymentDetails.Ref;
                creditor.CreditorInkassoID = Convert.ToString(_paymentDetails.Creditorno);
                claim.KID = _paymentDetails.Kid;
                claim.Amount = _paymentDetails.PaidAmount;
                claim.Creditor = creditor;
                claim.Debtor = debtor;
                claim.InvoiceType = InvoiceTypes.DB;
                claim.InvoicedDate = DateTime.Now.ToString("MM-dd-yyyy");
                claim.DueBalance = "0";
                claim.Balance = "0";
                PaymentProcessResult paymentResult = null;
                List<IUSPTransaction> transactions = new List<IUSPTransaction>();
                foreach (PayModeDC payMode in _paymentDetails.PayModes)
                {
                    transactions.Add(GetTransaction(claim, payMode.Amount, payMode.PaymentTypeCode, payMode.PaidDate));
                }
                paymentResult = Transaction.RegsiterTransaction(transactions, transaction);
                if (!paymentResult.ResultStatus)
                {
                     transaction.Rollback();
                     saleResult.AritemNo = -1;
                     saleResult.InvoiceNo = string.Empty;
                     saleResult.SaleStatus = SaleErrors.PAYMENTADDINGERROR;
                     return saleResult;
                }
                else
                {
                   
                    //------------------save shop transations data -------------
                    foreach (PayModeDC payMode in _paymentDetails.PayModes)
                    {
                        ShopTransactionDC _shopTrans = new ShopTransactionDC();
                        _shopTrans.BranchId = _loggedBranchID;
                        _shopTrans.CreatedUser = _paymentDetails.PaidUser;
                        _shopTrans.CreatedDate = DateTime.Now;
                        try
                        {
                            _shopTrans.Mode = (US.GMS.Core.SystemObjects.TransactionTypes)Enum.Parse(typeof(US.GMS.Core.SystemObjects.TransactionTypes), payMode.PaymentTypeCode, true);
                        }
                        catch
                        {
                            _shopTrans.Mode = US.GMS.Core.SystemObjects.TransactionTypes.NONE;
                            _shopTrans.ModeText = payMode.PaymentTypeCode;
                        }
                        _shopTrans.Amount = payMode.Amount;
                        _shopTrans.SalePointId = _paymentDetails.SalepointId;
                        _shopTrans.EntityId = _paymentDetails.PaidMemberId;
                        _shopTrans.EntityRoleType = "MEM";
                        _shopTrans.InvoiceArItemNo = Convert.ToInt32(_paymentDetails.PaymentSourceId);
                        _shopTrans.VoucherNo = payMode.VoucherNo;
                        SaveShopTransactionAction shopTransactionAction = new SaveShopTransactionAction(_shopTrans);
                        shopTransactionAction.RunOnTransaction(transaction);
                    }

                    if (_salesDetails != null)
                    {
                        AddSalesDetailsAction salesDetailAction = new AddSalesDetailsAction(_salesDetails, _loggedBranchID, _user, Convert.ToInt32(_paymentDetails.PaymentSourceId),-1,string.Empty);
                        salesDetailAction.RunOnTransaction(transaction);

                        List<ShopSalesItemDC> giftvouchers = new List<ShopSalesItemDC>();
                        giftvouchers = _salesDetails.ShopSalesItemList.Where(X => string.IsNullOrEmpty(X.VoucherNo) == false).ToList();
                        foreach (ShopSalesItemDC voucher in giftvouchers)
                        {
                            AddGiftVoucherDetailsAction voucherAction = new AddGiftVoucherDetailsAction(voucher, Convert.ToInt32(_paymentDetails.PaymentSourceId), _user, _paymentDetails.PaidMemberId, _loggedBranchID);
                            voucherAction.RunOnTransaction(transaction);
                        }

                    }

                    saleResult.AritemNo = -1;
                    saleResult.InvoiceNo = claim.InvoiceNumber;
                    saleResult.SaleStatus = SaleErrors.SUCCESS;
                    transaction.Commit();
                    return saleResult;
                }
            }
            catch
            {
                transaction.Rollback();
                throw;
            }

        }

        private IUSPTransaction GetTransaction(IUSPClaim claim, decimal paymentAmount, string paymentSourceName, DateTime paidDate)
        {
            IUSPTransaction transaction = new USPTransaction();
            transaction.TransType = GetTransType(paymentSourceName);
            transaction.PID = claim.Creditor.CreditorInkassoID;
            transaction.CID = Convert.ToString(claim.Debtor.DebtorcustumerID.ToString());
            if ((transaction.TransType == "OP" || transaction.TransType == "LOP" || transaction.TransType == "BS" || transaction.TransType == "DC") && paidDate != null && paidDate != DateTime.MinValue)
            {
                transaction.VoucherDate = paidDate;
            }
            else
            {
                transaction.VoucherDate = DateTime.Today;
            }
            transaction.ReceivedDate = DateTime.Today;
            transaction.RegDate = DateTime.Today;
            transaction.Amount = paymentAmount;
            transaction.Source = paymentSourceName;
            transaction.KID = claim.KID;
            transaction.DebtorAccountNo = "";//TODO
            transaction.InvoiceNo = claim.InvoiceNumber;
            transaction.CreditorAccountNo = claim.Creditor.CreditorAccountNo;
            transaction.User = _user;

            return transaction;

        }

        private string GetTransType(string paymentType)
        {
            switch (paymentType)
            {
                case "CASH":
                    return "CSH";

                case "BANKTERMINAL":
                    return "TMN";

                case "OCR":
                    return "LOP";

                case "BANKSTATEMENT":
                    return "BS";

                case "DEBTCOLLECTION":
                    return "DC";

                case "ONACCOUNT":
                    return "OA";

                case "GIFTCARD":
                    return "GC";

                case "BANK":
                    return "BNK";

                case "PREPAIDBALANCE":
                    return "PB";

                case "AGRESSO":
                    return "AGR";

                case "STDTERMINAL":
                    return "STMN";

                case "VIPPS":
                    return "VIPP";

            }
            return string.Empty;
        }
    }
}
