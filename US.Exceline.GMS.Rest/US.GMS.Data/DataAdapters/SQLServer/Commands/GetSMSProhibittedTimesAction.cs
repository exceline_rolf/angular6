﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetSMSProhibittedTimesAction : USDBActionBase<List<ExceSMSProhibitedDayDC>>
    {
        private int _branchId = -1;
        public GetSMSProhibittedTimesAction(int branchid)
        {
            _branchId = branchid;
        }

        protected override List<ExceSMSProhibitedDayDC> Body(System.Data.Common.DbConnection connection)
        {
            List<ExceSMSProhibitedDayDC> prohibittedTimes = new List<ExceSMSProhibitedDayDC>();
            string SpName = "USExceGMSAdminGetSMSProhibittedTime";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, SpName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchID", System.Data.DbType.Int32, _branchId));
                DbDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    ExceSMSProhibitedDayDC prohibittedTime = new ExceSMSProhibitedDayDC();
                    prohibittedTime.Id = Convert.ToInt32(reader["ID"]);
                    prohibittedTime.BranchId = Convert.ToInt32(reader["BranchID"]);
                    prohibittedTime.StartDate = Convert.ToDateTime(reader["StartDate"]);
                    if (reader["EndDate"] != DBNull.Value)
                        prohibittedTime.EndDate = Convert.ToDateTime(reader["EndDate"]);
                    prohibittedTime.Comment = Convert.ToString(reader["Comment"]);
                    prohibittedTimes.Add(prohibittedTime);
                }
                return prohibittedTimes;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
