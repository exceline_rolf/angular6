﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Common;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetLangugeDetailsAction : USDBActionBase<List<LanguageDC>>
    {
       
        protected override List<LanguageDC> Body(DbConnection connection)
        {
            var languageList = new List<LanguageDC>();
            const string storedProcedure = "USP_GetLanguageList";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var language = new LanguageDC
                        {
                            Id = Convert.ToInt32(reader["Id"]),
                            Name = reader["Name"].ToString(),
                            Code = reader["Code"].ToString()
                        };


                    languageList.Add(language);

                }
            }

            catch (Exception ex)
            {
                throw ex;
            }


            return languageList;  
        }
    }
}
