﻿using System;
using System.Collections.Generic;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetCountriesForAccessProfileAction : USDBActionBase<List<string>>
    {
        private readonly int _accessProfileId;


        public GetCountriesForAccessProfileAction(int accessProfileId)
        {
            _accessProfileId = accessProfileId;
        }

        protected override List<string> Body(DbConnection connection)
        {
            List<string> countryList = new List<string>();
            const string spGetItems = "USExceGMSAdminGetCountriesByAccessProfileId";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, spGetItems);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@accessProfileId", DbType.Int32, _accessProfileId));

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    string countryId = Convert.ToString(reader["ID"]);
                    countryList.Add(countryId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return countryList;
        }
    }
}
