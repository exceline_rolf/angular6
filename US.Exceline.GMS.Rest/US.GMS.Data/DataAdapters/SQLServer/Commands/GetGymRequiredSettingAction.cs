﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetGymRequiredSettingAction : USDBActionBase<List<GymRequiredSettingsDC>>
    {
        private int _branchId = -1;
        private string _userName;

        public GetGymRequiredSettingAction(int branchId, string userName)
        {
            _branchId = branchId;
            _userName = userName;
        }

        protected override List<GymRequiredSettingsDC> Body(DbConnection connection)
        {
            List<GymRequiredSettingsDC> gymRequiredSettingsList = new List<GymRequiredSettingsDC>();
           
            const string storedProcedureName = "USExceGMSGetGymRequiredSetting";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@UserName" , DbType.String,_userName));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    GymRequiredSettingsDC gymRequiredSettings = new GymRequiredSettingsDC();
                    gymRequiredSettings.Id = Convert.ToInt32(reader["Id"]);
                    gymRequiredSettings.BranchId = Convert.ToInt32(reader["BranchId"]);
                    gymRequiredSettings.BranchName = Convert.ToString(reader["BranchName"]);
                    gymRequiredSettings.IsExpressGym = Convert.ToBoolean(reader["IsExpress"]);
                    gymRequiredSettings.Region = Convert.ToString(reader["region"]);
                    gymRequiredSettings.MemberRequired = Convert.ToBoolean(reader["MemberRequired"]);
                    gymRequiredSettings.MobileRequired = Convert.ToBoolean(reader["MobileRequired"]);
                    gymRequiredSettings.AddressRequired = Convert.ToBoolean(reader["AddressRequired"]);
                    gymRequiredSettings.ZipCodeRequired = Convert.ToBoolean(reader["ZipCodeRequired"]);
                    gymRequiredSettingsList.Add(gymRequiredSettings);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return gymRequiredSettingsList;
        }
    }
}


