﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : DKA
// Created Timestamp : "10/10/2013 6:06:28 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using US.GMS.Core.DomainObjects.Common;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class SaveJobCategoryAction : USDBActionBase<bool>
    {
        private bool _isSaved = false;
        private ExcelineJobCategoryDC _jobCategory;
        private DataTable _dataTable;

        public SaveJobCategoryAction(ExcelineJobCategoryDC jobCategory, bool isEdit)
        {
            _jobCategory = jobCategory;
            _dataTable = GetExtendedFieldList(_jobCategory.ExtendedFieldsList);
        }

        private DataTable GetExtendedFieldList(List<ExtendedFieldDC> extendedFieldList)
        {
            _dataTable = new DataTable();
            _dataTable.Columns.Add(new DataColumn("Title", Type.GetType("System.String")));
            _dataTable.Columns.Add(new DataColumn("TemplateId", Type.GetType("System.Int32")));
            _dataTable.Columns.Add(new DataColumn("FieldType", Type.GetType("System.String")));
            _dataTable.Columns.Add(new DataColumn("CreatedDateTime", Type.GetType("System.DateTime")));
            _dataTable.Columns.Add(new DataColumn("LastModifiedDateTime", Type.GetType("System.DateTime")));
            _dataTable.Columns.Add(new DataColumn("CreatedUser", Type.GetType("System.String")));
            _dataTable.Columns.Add(new DataColumn("LastModifiedUser", Type.GetType("System.String")));


            foreach (ExtendedFieldDC fieldItem in extendedFieldList)
            {
                DataRow _dataTableRow = _dataTable.NewRow();
                _dataTableRow["Title"] = fieldItem.Title;
                _dataTableRow["TemplateId"] = fieldItem.CategoryId;
                _dataTableRow["FieldType"] = fieldItem.FieldType.ToString();
                _dataTableRow["CreatedDateTime"] = DateTime.Now;
                _dataTableRow["LastModifiedDateTime"] = DateTime.Now;
                _dataTableRow["CreatedUser"] = fieldItem.CreatedUser;
                _dataTableRow["LastModifiedUser"] = fieldItem.LastModifiedUser;

                _dataTable.Rows.Add(_dataTableRow);
            }

            return _dataTable;
        }

      
        protected override bool Body(DbConnection connection)
        {
            string StoredProcedureName = "USExceGMSAdminSaveJobCategory";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Name", DbType.String, _jobCategory.Name));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ExtendedFieldsList", SqlDbType.Structured, _dataTable));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@JobCategoryId", DbType.String, _jobCategory.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsRole", DbType.Boolean, _jobCategory.IsRole));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsEmployee", DbType.Boolean, _jobCategory.IsEmployee));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreatedUser", DbType.String, _jobCategory.CreatedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@LastModifiedUser", DbType.String, _jobCategory.LastModifiedUser));
                

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@outId";
                param.SqlDbType = SqlDbType.Int;
                param.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(param);


                cmd.ExecuteNonQuery();

                int templateId = Convert.ToInt32(param.Value);
                if (templateId > 0)
                    _isSaved = true;
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return _isSaved;
        }
    }
}
