﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : JNI
// Created Timestamp : "6/20/2012 6:06:42 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using System.Data.Common;
using System.Data;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetExtFieldsByCategoryAction : USDBActionBase<List<ExtendedFieldDC>>
    {
        private int _categoryId = -1;
        private string _categoryType = String.Empty;

        public GetExtFieldsByCategoryAction(int categoryId, string categoryType)
        {
            this._categoryId = categoryId;
            this._categoryType = categoryType;
        }
        protected override List<ExtendedFieldDC> Body(DbConnection connection)
        {
            List<ExtendedFieldDC> extFieldList = new List<ExtendedFieldDC>();
            string storedProcedureName = "USExceGMSAdminGetTaskCategoryExtFields";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CategoryId", DbType.Int32, _categoryId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CategoryType", DbType.String, _categoryType));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ExtendedFieldDC extField = new ExtendedFieldDC();
                    extField.Id = Convert.ToInt32(reader["Id"]);
                    extField.Title = reader["Name"].ToString();
                    extField.FieldType = (CommonUITypes)Enum.Parse(typeof(CommonUITypes), reader["FieldType"].ToString(), true);
                    extFieldList.Add(extField);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return extFieldList;
        }
    }
}
