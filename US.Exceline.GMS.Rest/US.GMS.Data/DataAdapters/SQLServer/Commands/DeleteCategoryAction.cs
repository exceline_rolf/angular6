﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "4/24/2012 4:24:03 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class DeleteCategoryAction : USDBActionBase<int>
    {
        private int _categoryId;
        private int _branchId;

        public DeleteCategoryAction(int categoryId, int branchId)
        {
            this._categoryId = categoryId;
            _branchId = branchId;
        }

        protected override int Body(DbConnection connection)
        {
            string storedProcedureName = "USExceGMSDeleteCategory";
            int Id = 0;

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@categoryId", DbType.Int32, _categoryId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));

                object obj = cmd.ExecuteScalar();
                Id = Convert.ToInt32(obj);
                if (Id != -2)
                    return 1;
                else
                    return -2;
            }
            catch (SqlException ex)
            {
                if (ex.Errors.Count > 0) // Assume the interesting stuff is in the first error
                {
                    switch (ex.Errors[0].Number)
                    {
                        case 547: // Foreign Key violation
                            return -1;
                        default:
                            return 0;
                    }
                }
                return 0;
            }
            catch
            {
                return 0;
            }
        }
    }
}
