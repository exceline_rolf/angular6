﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : JNI
// Created Timestamp : "6/20/2012 6:07:02 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using System.Data.Common;
using System.Data;
using US.GMS.Core.SystemObjects;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    class GetSheduleItemsAction : USDBActionBase<List<ScheduleItemDC>>
    {
        private int _scheduleId = -1;
        public GetSheduleItemsAction(int scheduleId)
        {
            this._scheduleId = scheduleId;
        }
        protected override List<ScheduleItemDC> Body(DbConnection connection)
        {
            List<ScheduleItemDC> scheduleItemList = new List<ScheduleItemDC>();
            string storedProcedureName = "USExceGMSGetSheduleItemsBySheduleId";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleId", DbType.Int32, _scheduleId));
               
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ScheduleItemDC scheduleItem = new ScheduleItemDC();
                    scheduleItem.Id = Convert.ToInt32(reader["ID"]);
                    string occurrence = reader["Occurrence"].ToString();
                    if (!string.IsNullOrEmpty(occurrence.Trim()))
                    {
                        scheduleItem.Occurrence = (ScheduleTypes)Enum.Parse(typeof(ScheduleTypes), occurrence, true);
                    }

                    scheduleItem.Day = reader["Day"].ToString();
                    scheduleItem.Week = Convert.ToInt32(reader["Week"]);
                    scheduleItem.Month = reader["Month"].ToString();
                    scheduleItem.Year = Convert.ToInt32(reader["Year"]);
                    scheduleItem.StartTime = Convert.ToDateTime(reader["StartTime"]);
                    scheduleItem.EndTime = Convert.ToDateTime(reader["EndTime"]);
                    scheduleItem.StartDate = Convert.ToDateTime(reader["StartDate"]);
                    scheduleItem.EndDate = Convert.ToDateTime(reader["EndDate"]);
                    scheduleItem.CreatedDate = Convert.ToDateTime(reader["CreatedDateTime"]);
                    scheduleItem.LastModifiedDate = Convert.ToDateTime(reader["LastModifiedDateTime"]);
                    scheduleItem.CreatedUser = reader["CreatedUser"].ToString();
                    scheduleItem.LastModifiedUser = reader["LastModifiedUser"].ToString();
                    scheduleItem.ActiveStatus = Convert.ToBoolean(reader["ActiveStatus"]);
                    scheduleItem.IsFixed = Convert.ToBoolean(reader["IsFixed"]);
                    scheduleItem.LastActiveTimeGeneratedDate = Convert.ToDateTime(reader["LastActiveTimesGeneratedDate"]);

                    scheduleItemList.Add(scheduleItem);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return scheduleItemList;
        }
    }
}
