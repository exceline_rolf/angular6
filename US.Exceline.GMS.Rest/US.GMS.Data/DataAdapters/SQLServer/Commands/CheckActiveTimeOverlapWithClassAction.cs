﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class CheckActiveTimeOverlapWithClassAction : USDBActionBase<string>
    {
        private ScheduleItemDC _scheduleItem;
        private DataTable _insDataTable = null;
        private DataTable _resDataTable = null;
        private DataTable _activeTimeDataTable = null;
        public CheckActiveTimeOverlapWithClassAction(ScheduleItemDC scheduleItem)
        {
            _scheduleItem = scheduleItem;
            if (scheduleItem.InstructorIdList != null && scheduleItem.InstructorIdList.Count > 0)
                _insDataTable = GetEntityList(scheduleItem.InstructorIdList);
            if (scheduleItem.ResourceIdList != null && scheduleItem.ResourceIdList.Count > 0)
                _resDataTable = GetEntityList(scheduleItem.ResourceIdList);
            if (scheduleItem.ActiveTimes != null && scheduleItem.ActiveTimes.Count > 0)
                _activeTimeDataTable = GetActiveTimeList(scheduleItem.ActiveTimes);

        }

        private DataTable GetEntityList(List<int> resList)
        {
            var dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn("ID", typeof(Int32)));
            foreach (var item in resList)
            {
                DataRow dataTableRow = dataTable.NewRow();
                dataTableRow["ID"] = item;
                dataTable.Rows.Add(dataTableRow);
            }
            return dataTable;
        }

        private DataTable GetActiveTimeList(List<EntityActiveTimeDC> activeTimeList)
        {
            var dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn("ID", typeof(Int32)));
            dataTable.Columns.Add(new DataColumn("ScheduleItemId", typeof(Int32)));
            dataTable.Columns.Add(new DataColumn("StartDateTime", typeof(DateTime)));
            dataTable.Columns.Add(new DataColumn("EndDateTime", typeof(DateTime)));
            foreach (var item in activeTimeList)
            {
                DataRow dataTableRow = dataTable.NewRow();
                dataTableRow["ID"] = item.Id;
               // dataTableRow["ScheduleItemId"] = item.sche;
                dataTableRow["StartDateTime"] = item.StartDateTime;
                dataTableRow["EndDateTime"] = item.EndDateTime;
                dataTable.Rows.Add(dataTableRow);
            }
            return dataTable;
        }


        protected override string Body(DbConnection connection)
        {
            string result = string.Empty;
            string storedProcedure = "USExceGMSCheckActiveTimeOverlapWithClass";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@scheduleItemId", DbType.Int32, _scheduleItem.Id));
                cmd.Parameters.Add(_insDataTable != null
                                          ? DataAcessUtils.CreateParam("@insList", SqlDbType.Structured,
                                                                       _insDataTable)
                                          : DataAcessUtils.CreateParam("@insList", SqlDbType.Structured, null));
                cmd.Parameters.Add(_resDataTable != null
                                          ? DataAcessUtils.CreateParam("@resList", SqlDbType.Structured,
                                                                       _resDataTable)
                                          : DataAcessUtils.CreateParam("@resList", SqlDbType.Structured, null));
                cmd.Parameters.Add(_activeTimeDataTable != null
                                          ? DataAcessUtils.CreateParam("@ActiveTimeList", SqlDbType.Structured,
                                                                       _activeTimeDataTable)
                                          : DataAcessUtils.CreateParam("@ActiveTimeList", SqlDbType.Structured, null));

                DbParameter output = new SqlParameter();
                output.DbType = DbType.String;
                output.ParameterName = "@OutVal";
                output.Size = 100;
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);
                cmd.ExecuteNonQuery();
                result = Convert.ToString(output.Value);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}
