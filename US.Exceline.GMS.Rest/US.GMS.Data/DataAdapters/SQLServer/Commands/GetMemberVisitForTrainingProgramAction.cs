﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetMemberVisitForTrainingProgramAction : USDBActionBase<List<TrainingProgramVisitDetailDC>>
    {
        private readonly int _branchId;
        private readonly string _systemName;
        private readonly DateTime _lastUpdatedDateTime;

        public GetMemberVisitForTrainingProgramAction(int branchId, string systemName, DateTime lastUpdatedDateTime)
        {
            _branchId = branchId;
            _systemName = systemName;
            _lastUpdatedDateTime = lastUpdatedDateTime;
        }

        protected override List<TrainingProgramVisitDetailDC> Body(DbConnection connection)
        {
            List<TrainingProgramVisitDetailDC> trainingProgramVisitDetailList = new List<TrainingProgramVisitDetailDC>();

            const string storedProcedureName = "USExceGMSGetMemberVisitDetailForTrainingProgram";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SystemName", DbType.String, _systemName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@LastUpdatedDateTime", DbType.DateTime, _lastUpdatedDateTime));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    TrainingProgramVisitDetailDC trainingProgramVisitDetail = new TrainingProgramVisitDetailDC();
                    EntityVisitDC memberVisit = new EntityVisitDC();
                    MemberIntegrationSettingDC memberIntegrationSetting = new MemberIntegrationSettingDC();

                    memberVisit.InTime = Convert.ToDateTime(reader["InTime"]);
                    memberIntegrationSetting.UserId = Convert.ToString(reader["UserId"]);

                    trainingProgramVisitDetail.MemberVisit = memberVisit;
                    trainingProgramVisitDetail.MemberIntegrationSetting = memberIntegrationSetting;

                    trainingProgramVisitDetail.CurrentUpdatedDateTime = Convert.ToDateTime(reader["CurrentUpdatedDateTime"]);
                    trainingProgramVisitDetailList.Add(trainingProgramVisitDetail);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return trainingProgramVisitDetailList;
        }
    }
}
