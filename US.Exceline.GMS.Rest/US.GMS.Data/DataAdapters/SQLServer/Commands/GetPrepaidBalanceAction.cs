﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetPrepaidBalanceAction : USDBActionBase<decimal>
    {
        private int _memberId = -1;
        private string _entityType = string.Empty;
        public GetPrepaidBalanceAction(int memberId, string entiryType)
        {
            _memberId = memberId;
            _entityType = entiryType;
        }

        protected override decimal Body(System.Data.Common.DbConnection connection)
        {
            string spname = "USExceGMSManageMembershipGetMemberPrepaidBalanace";
            decimal balanace = 0;
            try
            {
                DbCommand commmad = CreateCommand(System.Data.CommandType.StoredProcedure, spname);
                commmad.Parameters.Add(DataAcessUtils.CreateParam("@memberId", System.Data.DbType.Int32, _memberId));
                commmad.Parameters.Add(DataAcessUtils.CreateParam("@entityType", System.Data.DbType.String, _entityType));
                balanace = Convert.ToDecimal(commmad.ExecuteScalar());
            }
            catch (Exception)
            {
                throw;
            }
            return balanace;
        }
    }
}
