﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : JNI
// Created Timestamp : "6/12/2012 12:03:07 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageMemberships;
using System.Data.Common;
using System.Data;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetMemberByIDAction : USDBActionBase<OrdinaryMemberDC>
    {
        private int _branchId = -1;
        private int _memberId = -1;

        public GetMemberByIDAction(int branchId, int memberId)
        {
            this._branchId = branchId;
            this._memberId = memberId;
        }

        protected override OrdinaryMemberDC Body(System.Data.Common.DbConnection connection)
        {
            if (_memberId > 0)
            {
            List<OrdinaryMemberDC> ordinaryMemberList = new List<OrdinaryMemberDC>();

            string storedProcedureName = "USExceGMSManageMembershipGetMemberByID";

            try
            {
                    if (_memberId > 0)
                    {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MemberId", DbType.Int32, _memberId));

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    OrdinaryMemberDC ordinaryMember = new OrdinaryMemberDC();
                    ordinaryMember.EntNo = Convert.ToInt32(reader["EntNo"]);
                    ordinaryMember.Id = Convert.ToInt32(reader["MemberId"]);
                    ordinaryMember.BranchId = Convert.ToInt32(reader["BranchId"]);
                    ordinaryMember.FirstName = reader["FirstName"].ToString();
                    ordinaryMember.LastName = reader["LastName"].ToString();
                    ordinaryMember.CustId = reader["MemberNo"].ToString();                   
                    ordinaryMember.Name = ordinaryMember.FirstName + " " + ordinaryMember.LastName;
                    ordinaryMemberList.Add(ordinaryMember);
                }
            }
                }
            catch (Exception ex)
            {
                throw ex;
            }

            return ordinaryMemberList[0];
        }
            else
            {
                return new OrdinaryMemberDC();
            }
        }
    }
}
