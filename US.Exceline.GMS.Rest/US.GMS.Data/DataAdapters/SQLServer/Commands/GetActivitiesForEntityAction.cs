﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "6/28/2012 12:16:38 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageContracts;
using System.Data.Common;
using System.Data;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetActivitiesForEntityAction : USDBActionBase<List<ActivityDC>>
    {
        private int _entityId;
        private string _entityType;
        private int _branchId;

        public GetActivitiesForEntityAction(int entityId, string entityType, int branchId)
        {
            this._entityId = entityId;
            this._entityType = entityType;
            _branchId = branchId;
        }

        protected override List<ActivityDC> Body(DbConnection connection)
        {
            List<ActivityDC> activityList = new List<ActivityDC>();
            string storedProcedure = "USExceGMSGetActivitiesForEntity ";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityId", DbType.Int32, _entityId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityType", DbType.String, _entityType));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ActivityDC activity = new ActivityDC();
                    activity.Id = Convert.ToInt32(reader["ID"]);
                    activity.Name = reader["Name"].ToString();
                    activity.Code = reader["Code"].ToString();
                    activity.BranchId = Convert.ToInt32(reader["BranchId"]);
                    activity.IsActive = Convert.ToBoolean(reader["IsActive"]);
                    activityList.Add(activity);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return activityList;
        }
    }
}
