﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : DKA
// Created Timestamp : "10/10/2013 6:06:28 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using System.Data.SqlClient;
using US.GMS.Core.DomainObjects.Common;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class SaveTaskCategoryAction : USDBActionBase<bool>
    {
        private bool _isSaved;
        private readonly ExcelineTaskCategoryDC _taskCategory;
        private DataTable _dataTable;

        public SaveTaskCategoryAction(ExcelineTaskCategoryDC taskCategory, bool isEdit)
        {
            _taskCategory = taskCategory;
            _dataTable = GetExtendedFieldList(taskCategory.ExtendedFieldsList);
        }

        private DataTable GetExtendedFieldList(List<ExtendedFieldDC> extendedFieldList)
        {
            _dataTable = new DataTable();
            _dataTable.Columns.Add(new DataColumn("Title", Type.GetType("System.String")));
            _dataTable.Columns.Add(new DataColumn("TemplateId", Type.GetType("System.Int32")));
            _dataTable.Columns.Add(new DataColumn("FieldType", Type.GetType("System.String")));
            _dataTable.Columns.Add(new DataColumn("CreatedDateTime", Type.GetType("System.DateTime")));
            _dataTable.Columns.Add(new DataColumn("LastModifiedDateTime", Type.GetType("System.DateTime")));
            _dataTable.Columns.Add(new DataColumn("CreatedUser", Type.GetType("System.String")));
            _dataTable.Columns.Add(new DataColumn("LastModifiedUser", Type.GetType("System.String")));

            foreach (ExtendedFieldDC fieldItem in extendedFieldList)
            {
                DataRow _dataTableRow = _dataTable.NewRow();
                _dataTableRow["Title"] = fieldItem.Title;
                _dataTableRow["TemplateId"] = fieldItem.CategoryId;
                _dataTableRow["FieldType"] = fieldItem.FieldType.ToString();
                _dataTableRow["CreatedDateTime"] = DateTime.Now;
                _dataTableRow["LastModifiedDateTime"] = DateTime.Now;
                _dataTableRow["CreatedUser"] = fieldItem.CreatedUser;
                _dataTableRow["LastModifiedUser"] = fieldItem.LastModifiedUser;

                _dataTable.Rows.Add(_dataTableRow);
            }
            return _dataTable;
        }

        protected override bool Body(DbConnection connection)
        {
            const string storedProcedureName = "USExceGMSAdminSaveTaskCategory";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Name", DbType.String, _taskCategory.Name));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ExtendedFieldsList", SqlDbType.Structured, _dataTable));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TaskCategoryId", DbType.String, _taskCategory.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsAssignToEmp", DbType.Boolean, _taskCategory.IsAssignToEmp));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsAssignToRole", DbType.Boolean, _taskCategory.IsAssignToRole));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsStartDate", DbType.Boolean, _taskCategory.IsStartDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsEndDate", DbType.Boolean, _taskCategory.IsEndDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsStartTime", DbType.Boolean, _taskCategory.IsStartTime));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsEndTime", DbType.Boolean, _taskCategory.IsEndTime));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsFoundDate", DbType.Boolean, _taskCategory.IsFoundDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsReturnDate", DbType.Boolean, _taskCategory.IsReturnDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsDueDate", DbType.Boolean, _taskCategory.IsDueDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsNoOfDays", DbType.Boolean, _taskCategory.IsNoOfDays));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsPhoneNo", DbType.Boolean, _taskCategory.IsPhoneNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsDueTime", DbType.Boolean, _taskCategory.IsDueTime));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsNextFollowUp", DbType.Boolean, _taskCategory.IsNextFollowUp));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsAutomatedSMS", DbType.Boolean, _taskCategory.IsAutomatedSMS));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsAutomatedEmail", DbType.Boolean, _taskCategory.IsAutomatedEmail));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreatedUser", DbType.String, _taskCategory.CreatedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@LastModifiedUser", DbType.String, _taskCategory.LastModifiedUser));
                SqlParameter param = new SqlParameter();
                param.ParameterName = "@outId";
                param.SqlDbType = SqlDbType.Int;
                param.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(param);
                cmd.ExecuteNonQuery();
                int templateId = Convert.ToInt32(param.Value);
                if (templateId > 0)
                    _isSaved = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _isSaved;
        }
    }
}
