﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "16.08.2012 13:22:37
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class UpdatePaymentInvoiceNoAction : USDBActionBase<int>
    {
        private int _id;
        private string _type;
        private string _invoiceNo;

        public UpdatePaymentInvoiceNoAction(int id,string type,string invoiceNo)
        {
            _id = id;
            _type = type;
            _invoiceNo = invoiceNo;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            string storedProcedureName = "USExceGMSUpdatePaymentInvoceNo";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@id", DbType.Int32, _id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@type", DbType.String, _type));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@invoiceNo", DbType.String, _invoiceNo));
                cmd.ExecuteNonQuery();
                return 1;
            }
            catch
            {
                return 0;
            }
        }
    }
}
