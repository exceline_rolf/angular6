﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class ValidateEmailAction : USDBActionBase<bool>
    {
        private string _email = string.Empty;
        public ValidateEmailAction(string email)
        {
            _email = email;
        }
        protected override bool Body(DbConnection connection)
        {
            bool result = false;
            string spName = "USExceGMSValidateEmail";
            try
            {
                DbCommand command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@Email", DbType.String, _email));
                result = Convert.ToBoolean(command.ExecuteScalar());
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
