﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class ValidateMemberCardAction : USDBActionBase<bool>
    {
        private string _memberCard = string.Empty;
        private int _memberId = -1;
        public ValidateMemberCardAction(string memberCard, int memberId)
        {
            _memberCard = memberCard;
            _memberId = memberId;
        }
        protected override bool Body(DbConnection connection)
        {
            bool result = false;
            string spName = "USExceGMSValidateMemberCard";
            try
            {
                DbCommand command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@memberCard", DbType.String, _memberCard));
                command.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberId));
                result = Convert.ToBoolean(command.ExecuteScalar());
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
