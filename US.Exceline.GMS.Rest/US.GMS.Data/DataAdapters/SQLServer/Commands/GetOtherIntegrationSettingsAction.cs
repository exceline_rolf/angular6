﻿using System;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetOtherIntegrationSettingsAction : USDBActionBase<OtherIntegrationSettingsDC>
    {
        private readonly int _branchId = -1;
        private readonly string _systemName = string.Empty;

        public GetOtherIntegrationSettingsAction(int branchId, string systemName)
        {
            _branchId = branchId;
            _systemName = systemName;
        }

        protected override OtherIntegrationSettingsDC Body(DbConnection connection)
        {
            var otherIntegrationSettings = new OtherIntegrationSettingsDC();
            const string spName = "dbo.USExceGMSGetSystemIntegrationSettings";
            try
            {
                DbCommand command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@SystemName", DbType.String, _systemName));
                DbDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    otherIntegrationSettings.Id = Convert.ToInt32(reader["Id"]);
                    otherIntegrationSettings.ServiceBaseUrl = Convert.ToString(reader["ServiceBaseUrl"]);
                    otherIntegrationSettings.UserName = Convert.ToString(reader["UserName"]);
                    otherIntegrationSettings.Password = Convert.ToString(reader["Password"]);
                    otherIntegrationSettings.ApiKey = Convert.ToString(reader["APIKey"]);
                    otherIntegrationSettings.FacilityUrl = Convert.ToString(reader["FacilityUrl"]);
                    if (reader["Guid"] != DBNull.Value)
                    otherIntegrationSettings.Guid = reader["Guid"].ToString();
                }

                return otherIntegrationSettings;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
