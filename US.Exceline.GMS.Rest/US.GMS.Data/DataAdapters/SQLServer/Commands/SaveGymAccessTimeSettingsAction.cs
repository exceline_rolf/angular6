﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using System.Data;
using System.Data.SqlClient;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{

    public class SaveGymAccessTimeSettingsAction : USDBActionBase<bool>
    {
        #region private variable
        private readonly int _branchId;
        private bool _returnValue;
        private int _outputId;
        private readonly GymAccessTimeSettingsDC _accessTimeSettings = new GymAccessTimeSettingsDC();
        private ExceAccessProfileDC _accessProfile = new ExceAccessProfileDC();
        #endregion

        #region Constructor
        public SaveGymAccessTimeSettingsAction(int branchId, GymAccessTimeSettingsDC accessTimeSettings, string gymCode)
        {
            _branchId = branchId;
            _accessTimeSettings = accessTimeSettings;
        }
        #endregion

        #region Body
        protected override bool Body(DbConnection connection)
        {
            const string storedProcedure = "USExceGMSAdminUpdateGymOpenTime";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                if (cmd != null)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@GymOpenTimes", SqlDbType.Structured, GetTimeTable(_accessTimeSettings.GymOpenTimes)));

                    cmd.ExecuteNonQuery();
                }
                _returnValue = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            try
            {
                DbTransaction transaction = connection.BeginTransaction();
                foreach (ExceAccessProfileDC profile in _accessTimeSettings.AccessProfileList)
                {
                    _accessProfile = profile;
                    _returnValue = SaveAccessProfile(transaction);
                }
                transaction.Commit();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _returnValue;
        }
        #endregion

        #region GetTimeTable
        private DataTable GetTimeTable(List<GymOpenTimeDC> gymOpenTimes)
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn("FromTime", typeof(DateTime)));
            dataTable.Columns.Add(new DataColumn("ToTime", typeof(DateTime)));
            dataTable.Columns.Add(new DataColumn("Monday", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("Tuesday", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("Wednesday", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("Thursday", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("Friday", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("Saturday", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("Sunday", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("BranchId", typeof(int)));

            foreach (GymOpenTimeDC openTime in gymOpenTimes)
            {
                DataRow row = dataTable.NewRow();
                row["FromTime"] = openTime.StartTime;
                row["ToTime"] = openTime.EndTime;
                row["Monday"] = openTime.IsMonday;
                row["Tuesday"] = openTime.IsTuesday;
                row["Wednesday"] = openTime.IsWednesday;
                row["Thursday"] = openTime.IsThursday;
                row["Friday"] = openTime.IsFriday;
                row["Saturday"] = openTime.IsSaturday;
                row["Sunday"] = openTime.IsSunday;
                row["BranchId"] = openTime.BranchId;
                dataTable.Rows.Add(row);
            }
            return dataTable;
        }
        #endregion

        #region SaveAccessProfile
        private bool SaveAccessProfile(DbTransaction transaction)
        {
            const string storedProcedureName = "USExceGMSSaveAccessProfile";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Connection = transaction.Connection;
                cmd.Transaction = transaction;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, _accessProfile.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AccessProfileName", DbType.String, _accessProfile.AccessProfileName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ActiveStatus", DbType.Boolean, true));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Gender", DbType.String, _accessProfile.Gender.ToString()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@GymAccessProfileTime", SqlDbType.Structured, GetAccessProfileTime(_accessProfile.AccessTimeList)));
                SqlParameter param = new SqlParameter();
                param.ParameterName = "@OutputId";
                param.SqlDbType = SqlDbType.Int;
                param.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(param);
                cmd.ExecuteNonQuery();
                if (param.Value != null) _outputId = Convert.ToInt32(param.Value);
                return _outputId > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GetAccessProfileTimeTable
        private DataTable GetAccessProfileTime(List<ExceAccessProfileTimeDC> profileTimeList)
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn("ID", typeof(Int32)));
            dataTable.Columns.Add(new DataColumn("AccessProfileId", typeof(Int32)));
            dataTable.Columns.Add(new DataColumn("FromTime", typeof(DateTime)));
            dataTable.Columns.Add(new DataColumn("ToTime", typeof(DateTime)));
            dataTable.Columns.Add(new DataColumn("Monday", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("Tuesday", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("Wednesday", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("Thursday", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("Friday", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("Saturday", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("Sunday", typeof(bool)));

            foreach (ExceAccessProfileTimeDC profileTime in profileTimeList)
            {
                DataRow row = dataTable.NewRow();
                row["ID"] = profileTime.Id;
                row["AccessProfileId"] = profileTime.AccessProfileId;
                row["FromTime"] = profileTime.FromTime;
                row["ToTime"] = profileTime.ToTime;
                row["Monday"] = profileTime.Monday;
                row["Tuesday"] = profileTime.Tuesday;
                row["Wednesday"] = profileTime.Wednesday;
                row["Thursday"] = profileTime.Thursday;
                row["Friday"] = profileTime.Friday;
                row["Saturday"] = profileTime.Saturday;
                row["Sunday"] = profileTime.Sunday;
                dataTable.Rows.Add(row);
            }
            return dataTable;
        }
        #endregion
    }
}
