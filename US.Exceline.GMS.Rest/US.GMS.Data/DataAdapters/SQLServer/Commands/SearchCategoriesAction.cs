﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "4/24/2012 6:14:54 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Common;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class SearchCategoriesAction : USDBActionBase<List<CategoryDC>>
    {
        private readonly string _searchText;
        private readonly string _searchType;
        private readonly int _branchID = -1;

        public SearchCategoriesAction(string searchText, string searchType, int branchID)
        {
            _searchText = searchText;
            _searchType = searchType;
            _branchID = branchID;
        }

        protected override List<CategoryDC> Body(DbConnection connection)
        {
            List<CategoryDC> categoryList = new List<CategoryDC>();
            string storedProcedureName = "USExceGMSSearchCategories";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@searchText", DbType.String, _searchText));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@searchType", DbType.String, _searchType));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchID", DbType.String, _branchID.ToString()));

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    CategoryDC category = new CategoryDC();
                    category.Id = Convert.ToInt32(reader["ID"]);
                    category.TypeId = Convert.ToInt32(reader["CategoryTypeId"]);
                    if (!string.IsNullOrEmpty(reader["TypeName"].ToString()))
                    {
                        category.TypeName = Convert.ToString(reader["TypeName"]);
                    }
                    category.BranchId = Convert.ToInt32(reader["BranchId"]);
                    category.Code = reader["Code"].ToString();
                    category.Name = reader["Name"].ToString();
                    category.Description = reader["Description"].ToString();
                    //category.CategoryImage = reader["Image"];//TODO:
                    category.CreatedUser = reader["CreatedUser"].ToString();
                    category.LastModifiedUser = reader["LastModifiedUser"].ToString();

                    if (!string.IsNullOrEmpty(reader["CreatedDateTime"].ToString()))
                    {
                        category.CreatedDate = Convert.ToDateTime(reader["CreatedDateTime"]);
                    }
                    if (!string.IsNullOrEmpty(reader["LastModifiedDateTime"].ToString()))
                    {
                        category.CreatedDate = Convert.ToDateTime(reader["LastModifiedDateTime"]);
                    }
                    if (!string.IsNullOrEmpty(reader["ActiveStatus"].ToString()))
                    {
                        category.ActiveStatus = Convert.ToBoolean(reader["ActiveStatus"]);
                    }

                    category.Color = Convert.ToString(reader["Color"]);
                    categoryList.Add(category);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return categoryList;
        }
    }
}
