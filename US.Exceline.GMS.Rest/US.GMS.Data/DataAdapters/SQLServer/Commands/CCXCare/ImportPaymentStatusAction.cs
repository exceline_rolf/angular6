﻿using System;
using System.Data.Common;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands.CCXCare
{
    class ImportPaymentStatusAction : USDBActionBase<int>
    {
        protected override int Body(System.Data.Common.DbConnection connection)
        {
            try
            {
                string spName = "dbo.USExceGMSCCXImportPaymentStatus";
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.ExecuteNonQuery();
                return  1;
            }
            catch (Exception)
            {                
                throw;
            }
        }
    }
}
