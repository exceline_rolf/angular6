﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands.CCXCare
{
    public class UpdateTemplateStatusAction : USDBActionBase<int>
    {
        protected override int Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSCCXUpdateTemplateStatus";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.ExecuteNonQuery();
                return 1;
            }
            catch (Exception )
            {
                return -1;
            }
        }
    }
}
