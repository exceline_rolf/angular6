﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USPayment
// Project Name      : UpdateDWTransferStatusAction 
// Coding Standard   : US Coding Standards
// Author            : AAB
// Created Timestamp : 3/28/2013 1:15:07 PM
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.USDF.Core.DomainObjects;
using US_DataAccess;
namespace US.GMS.Data.DataAdapters.SQLServer.Commands.CCXCare
{
    class UpdateDWTransferStatusAction : USDBActionBase<bool>
    {
         private List<USDFCase> _caseList = new List<USDFCase>();
        private string _batchNo = string.Empty;
        public UpdateDWTransferStatusAction(List<USDFCase> caseList, string batchNo)
        {
            _caseList = caseList;
            _batchNo = batchNo;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string spName = "dbo.USExceGMSUpdateDebtWarningTransferStatus";
            
            try
            {
                foreach(USDFCase usdfCase in _caseList)
                {
                    DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                    command.Parameters.Add(DataAcessUtils.CreateParam("@CaseNo", System.Data.DbType.Int32,Convert.ToInt32(usdfCase.CaseNo)));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@ARItemNoList", SqlDbType.Structured, GetDataTable(usdfCase.Transactions)));
                    command.Parameters.Add(DataAcessUtils.CreateParam("@BatchNo", DbType.String, _batchNo.ToString()));
                    command.ExecuteNonQuery();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private DataTable GetDataTable(List<USDFTransaction> transactions)
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn("ID", typeof(int)));

            foreach (USDFTransaction transaction in transactions)
            {
                DataRow row = dataTable.NewRow();
                row["ID"] = transaction.ArItemNo;
                dataTable.Rows.Add(row);
            }
            return dataTable;
        }
    }
}
