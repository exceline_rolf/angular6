﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using US.USDF.Core.DomainObjects;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands.CCXCare
{
    class GetDebtWarningCasesAction : USDBActionBase<List<USDFCase>>
    {
        private int _chunkSize = 0;
        private int _creditorNo = 0;
        public GetDebtWarningCasesAction(int chunkSize, int creditorNo)
        {
            _chunkSize = chunkSize;
            _creditorNo = creditorNo;
        }

        protected override List<USDFCase> Body(System.Data.Common.DbConnection connection)
        {
            string spName = "dbo.USExceGMSGetDebtWarningCases";
            List<USDFCase> caseList = new List<USDFCase>();
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@ChunkSize", System.Data.DbType.Int32, _chunkSize));
                command.Parameters.Add(DataAcessUtils.CreateParam("@CreditorNo", System.Data.DbType.Int32, _creditorNo));

                DbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    USDFCase usdfCase = new USDFCase();
                    // usdfCase.CaseNo = Convert.ToString(reader["CaseNo"]);
                    usdfCase.CreditorRef = Convert.ToString(reader["CustId"]);
                    usdfCase.CreditorExternalID = Convert.ToString(reader["CreditorInkassoID"]);
                    usdfCase.CaseNo = Convert.ToString(reader["CaseNo"]);
                    usdfCase.ARNo = Convert.ToInt32(reader["ARNo"]);

                    USDFDebtor debtor = new USDFDebtor();

                    if ((reader["Born"]) != DBNull.Value)
                    {
                        debtor.BirthDate = Convert.ToDateTime(reader["Born"]).ToString("yyyy/MM/dd");
                    }
                    debtor.FirstName = Convert.ToString(reader["FirstName"]);
                    debtor.LastName = Convert.ToString(reader["LastName"]);
                    debtor.PersonNo1 = Convert.ToString(reader["PersonNo"]);
                    debtor.TelMobile = Convert.ToString(reader["TelMobile"]);
                    debtor.TelWork = Convert.ToString(reader["TelWork"]);
                    debtor.TelHome = Convert.ToString(reader["TelHome"]);
                    debtor.Email = Convert.ToString(reader["Email"]);
                    debtor.Title = Convert.ToString(reader["RoleId"]);

                    USDFAddress address = new USDFAddress();
                    address.Addr1 = Convert.ToString(reader["Addr1"]);
                    address.Addr2 = Convert.ToString(reader["Addr2"]);
                    address.Addr3 = Convert.ToString(reader["Addr3"]);
                    address.ZipCode = Convert.ToString(reader["ZipCode"]);
                    address.ZipName = Convert.ToString(reader["ZipName"]);
                    address.CountryId = Convert.ToString(reader["CountryId"]);
                    address.AddrSource = Convert.ToString(reader["Addrsource"]);
                    address.IsDefault = Convert.ToInt32(reader["IsDefault"]);

                    debtor.AddressList.Add(address);
                    usdfCase.Customers.Add(debtor);
                    caseList.Add(usdfCase);
                }
                return caseList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
