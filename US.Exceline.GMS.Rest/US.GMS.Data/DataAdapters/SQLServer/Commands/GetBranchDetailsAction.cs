﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data.Common;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetBranchDetailsAction : USDBActionBase<List<BranchDetailsDC>>
    {
        protected override List<BranchDetailsDC> Body(System.Data.Common.DbConnection connection)
        {
            DbDataReader reader = null;
            List<BranchDetailsDC> branchList = new List<BranchDetailsDC>();
            string SpName = "ExceWorkStationGetBranchDetails";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, SpName);
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BranchDetailsDC branch = new BranchDetailsDC();
                    branch.GymCode = Convert.ToString(reader["GymCode"]);
                    branch.BranchId = Convert.ToInt32(reader["BranchId"]);
                    branch.BranchName = Convert.ToString(reader["BranchName"]);
                    branch.CreditorNo = (reader["CreditorNo"] != DBNull.Value) ? Convert.ToInt32(reader["CreditorNo"]) : -1;
                    branch.GymDetailId = Convert.ToInt32(reader["GymDetailId"]);
                    branch.Id = Convert.ToInt32(reader["ID"]);
                    branch.InvoiceProcessEnabled = Convert.ToBoolean(reader["InvoiceEnabled"]);
                    branchList.Add(branch);
                }
                return branchList;
            }
            catch
            {
                throw;
            }
        }
    }
}
