﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetMembersForShopAction : USDBActionBase<List<ExcelineMemberDC>>
    {
        private readonly int _branchId;
        private readonly string _searchText = string.Empty;
        private readonly int _status;
        private readonly MemberRole _memberRole;
        private readonly int _hit;
        public GetMembersForShopAction(int branchId, string searchText, int status,  MemberRole memberRole,int hit)
        {
            _branchId = branchId;
            _searchText = searchText;
            _status = status;
            _memberRole = memberRole;
            _hit = hit;
        }
        protected override List<ExcelineMemberDC> Body(DbConnection connection)
        {
            var ordinaryMemberLst = new List<ExcelineMemberDC>();
            const string storedProcedureName = "USExceGMSGetMembersForShop";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                if (!string.IsNullOrEmpty(_searchText))
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@seachText", DbType.String, _searchText));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@activeStatus", DbType.Int32, _status));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberRole", DbType.String, _memberRole));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Hit", DbType.Int32, _hit));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var ordineryMember = new ExcelineMemberDC();
                    ordineryMember.Id = Convert.ToInt32(reader["MemberId"]);
                    ordineryMember.Name = reader["Name"].ToString();
                    ordineryMember.FirstName = reader["FirstName"].ToString();
                    ordineryMember.LastName = reader["LastName"].ToString();
                    string roleType = reader["RoleId"].ToString();
                    if (!string.IsNullOrEmpty(roleType.Trim()))
                    {
                        try
                        {
                            ordineryMember.Role = (MemberRole)Enum.Parse(typeof(MemberRole), roleType);
                        }
                        catch
                        {
                            ordineryMember.Role = MemberRole.NONE;
                        }
                    }
                    ordineryMember.Address1 = reader["Address1"].ToString();
                    ordineryMember.Address2 = reader["Address2"].ToString();
                    ordineryMember.Mobile = Convert.ToString(reader["Mobile"]).Trim();
                    ordineryMember.StatusName = Convert.ToString(reader["StatusName"]);
                    ordineryMember.MobilePrefix = Convert.ToString(reader["TelMobilePrefix"]).Trim();
                    ordineryMember.Email = Convert.ToString(reader["Email"]);
                    ordineryMember.ZipCode = Convert.ToString(reader["ZipCode"]);
                    ordineryMember.ZipName = Convert.ToString(reader["ZipName"]);
                    ordineryMember.CustId = Convert.ToString(reader["CustId"]);
                    ordineryMember.BranchID = Convert.ToInt32(reader["BranchId"]);
                    ordineryMember.GuardianId = Convert.ToInt32(reader["GuardianId"]);

                    ordinaryMemberLst.Add(ordineryMember);
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ordinaryMemberLst;
        }
    }
}
