﻿
// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.ManageContracts;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetArticlesForVisitsAction : USDBActionBase<List<ArticleDC>>
    {
        private int _branchID = -1;
        public GetArticlesForVisitsAction(int branchID)
        {
            _branchID = branchID;
        }

        protected override List<ArticleDC> Body(System.Data.Common.DbConnection connection)
        {
            List<ArticleDC> _articleList = new List<ArticleDC>();
            string StoredProcedureName = "USExceGMSAdminGetArticlesForVisits";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchID", DbType.Int32, _branchID));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ArticleDC article = new ArticleDC();
                    article.Category = reader["Activity"].ToString();
                    article.Id = Convert.ToInt32(reader["ArticleId"]);
                    article.ActivityId = Convert.ToInt32(reader["ActivityId"]);
                    article.ArticleNo = reader["ArticleNo"].ToString();
                    article.Description = reader["Description"].ToString();
                    article.DefaultPrice = Convert.ToDecimal(reader["UnitPrice"]);
                    article.Price = Convert.ToDecimal(reader["UnitPrice"]);
                    article.Category = Convert.ToString(reader["Category"]);
                    article.BranchId = Convert.ToInt32(reader["BranchId"]);
                    _articleList.Add(article);
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return _articleList;
        }
    }
}
