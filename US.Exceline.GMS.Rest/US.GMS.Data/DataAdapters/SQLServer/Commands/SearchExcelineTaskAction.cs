﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : JNI
// Created Timestamp : "6/20/2012 6:07:33 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.Utils;
using System.Data.Common;
using System.Data;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class SearchExcelineTaskAction : USDBActionBase<List<ExcelineTaskDC>>
    {
        private string _searchText = string.Empty;
        private int _branchId = -1;
        private bool _isFxied = false;
        private TaskTemplateType _type;
        private int _followUpMemId;

        public SearchExcelineTaskAction(string searchText, TaskTemplateType type, int followUpMemId, int branchId, bool isFxied)
        {
            this._branchId = branchId;
            this._searchText = searchText;
            this._isFxied = isFxied;
            this._type = type;
            this._followUpMemId = followUpMemId;
        }
        protected override List<ExcelineTaskDC> Body(System.Data.Common.DbConnection connection)
        {
            List<ExcelineTaskDC> exeTaskList = new List<ExcelineTaskDC>();
            string storedProcedureName = "USExceGMSSearchTasks";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@isFxied", DbType.Boolean, _isFxied));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SearchText", DbType.String, _searchText));
                if (!_type.Equals(TaskTemplateType.NONE))
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@TemplateType", DbType.String, _type.ToString()));
                if (_followUpMemId != -1)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@FollowUpMember", DbType.Int32, _followUpMemId));

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ExcelineTaskDC exeTask = new ExcelineTaskDC();
                    exeTask.Id = Convert.ToInt32(reader["Id"]);
                    exeTask.Title = reader["Name"].ToString();
                    exeTask.Description = reader["Description"].ToString();
                    exeTask.TaskTemplateId = Convert.ToInt32(reader["TaskTemplateId"]);
                    exeTask.Schedule.Id = Convert.ToInt32(reader["ScheduleId"]);
                    exeTask.BranchId = Convert.ToInt32(reader["BranchId"]);
                    exeTask.IsFxied = Convert.ToBoolean(reader["IsFxied"]); ;
                    exeTask.IsPopup = Convert.ToBoolean(reader["IsPopup"]);
                    exeTask.NumOfDays = Convert.ToInt32(reader["NumOfDays"]);
                    exeTask.ActiveStatus = Convert.ToBoolean(reader["ActiveStatus"]);

                    exeTask.StartDate = Convert.ToDateTime(reader["StartDate"]);
                    exeTask.EndDate = Convert.ToDateTime(reader["EndDate"]);
                    exeTask.StartTime = Convert.ToDateTime(reader["StartTime"]);
                    exeTask.EndTime = Convert.ToDateTime(reader["EndTime"]);
                    exeTask.FoundDate = Convert.ToDateTime(reader["FoundDate"]);
                    exeTask.ReturnDate = Convert.ToDateTime(reader["ReturnDate"]);
                    exeTask.DueDate = Convert.ToDateTime(reader["DueDate"]);
                    exeTask.DueTime = Convert.ToDateTime(reader["DueTime"]);
                    exeTask.AssignTo = Convert.ToInt32(reader["EntRoleId"]);
                    exeTask.AssigntoName = reader["AssignToName"].ToString();
                    exeTask.AssignEntityRole = reader["RoleId"].ToString();
                    exeTask.FollowUpMemberId = Convert.ToInt32(reader["MemberId"]);
                    exeTask.BelongsToMemberId = Convert.ToInt32(reader["BelongsToMemberId"]);

                    exeTask.PhoneNo = reader["PhoneNo"].ToString();
                    exeTask.Sms = reader["Sms"].ToString();
                    exeTask.TaskStatus = reader["TaskStatus"].ToString();

                    exeTask.Schedule.StartDate = Convert.ToDateTime(reader["SheduleStartDate"]);
                    exeTask.Schedule.EndDate = Convert.ToDateTime(reader["SheduleEndDate"]);

                    ExtendedFieldInfoDC fieldInfo = new ExtendedFieldInfoDC();


                    if (!String.IsNullOrEmpty(reader["ExtenedInfo"].ToString()))
                    {
                        fieldInfo = (ExtendedFieldInfoDC)XMLUtils.DesrializeXMLToObject(reader["ExtenedInfo"].ToString(), typeof(ExtendedFieldInfoDC));
                        exeTask.ExtendedFieldsList = fieldInfo.ExtendedFieldsList;
                    }
                    exeTask.TemplateType = (TaskTemplateType)Enum.Parse(typeof(TaskTemplateType), reader["TemplateType"].ToString(), true);

                    exeTaskList.Add(exeTask);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return exeTaskList;
        }
    }
}
