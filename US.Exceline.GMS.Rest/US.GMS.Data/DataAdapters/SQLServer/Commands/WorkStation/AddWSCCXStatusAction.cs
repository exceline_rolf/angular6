﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands.WorkStation
{
    public class AddWSCCXStatusAction : USDBActionBase<bool>
    {
        private string _gymCode = string.Empty;
        private int _branchID = -1;
        private bool _ccxEnabled = false;

        public AddWSCCXStatusAction(string gymCode, int branchID, bool CCXEnabled)
        {
            _gymCode = gymCode;
            _branchID = branchID;
            _ccxEnabled = CCXEnabled;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string spName = "ExceWorkStationCCXStatus";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@GymCode", System.Data.DbType.String, _gymCode));
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchID", System.Data.DbType.String, _branchID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@CCXEnabled", System.Data.DbType.String, _ccxEnabled));
                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
