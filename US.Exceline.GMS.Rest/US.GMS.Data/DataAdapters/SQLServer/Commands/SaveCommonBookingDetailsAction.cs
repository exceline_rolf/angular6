﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "5/10/2012 10:38:46 AM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Common;
using System.Data;
using System.Globalization;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using System.Collections.ObjectModel;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class SaveCommonBookingDetailsAction : USDBActionBase<bool>
    {
        private CommonBookingDC _commonBookingDC;
        private int _branchId;
        private string _user;
        private string _scheduleCategoryType;
        private ScheduleDC _scheduleDC;
        private DataTable _scheduleItemDataTable;

        public SaveCommonBookingDetailsAction(CommonBookingDC commonBookingDC,ScheduleDC scheduleDC, int branchId,string user,string scheduleCategoryType)
        {
            this._commonBookingDC = commonBookingDC;
            this._branchId = branchId;
            this._user = user;
            this._scheduleCategoryType = scheduleCategoryType;
            this._scheduleDC = scheduleDC;
            List<ScheduleItemDC> itemList = new List<ScheduleItemDC>();
            if (scheduleDC.SheduleItemList != null && scheduleDC.SheduleItemList.Count > 0)
            {
                GetScheduleItemTypeLst(itemList);
            }
        }

        protected override bool Body(DbConnection connection)
        {
            string storedProcedureName = "USExceGMSSaveCommonBookingDetails";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@scheduleCategoryType", DbType.String, _scheduleCategoryType));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@activeTimeId", DbType.Int32, _commonBookingDC.ActiveTimeId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@bookingEntityId", DbType.Int32, _commonBookingDC.BookingEntityId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@bookingEntityType", DbType.String, _commonBookingDC.BookingEntityType));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@bookingEntityName", DbType.String, _commonBookingDC.BookingEntityName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@bookingForEntityId", DbType.Int32, _commonBookingDC.BookingForEntityId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@bookingForEntityType", DbType.String, _commonBookingDC.BookingForEntityType));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@bookingForEntityName", DbType.String, _commonBookingDC.BookingForEntityName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@bookingActivityId", DbType.String, _commonBookingDC.BookingActivityId));

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@startDateTime", DbType.DateTime, _scheduleDC.StartDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@endDateTime", DbType.DateTime, _scheduleDC.EndDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleItemList", SqlDbType.Structured, _scheduleItemDataTable));

                cmd.ExecuteNonQuery();
                return true;
            }
            catch
            {
                return false;
            }
        }
         
        private DataTable GetScheduleItemTypeLst(List<ScheduleItemDC> scheduleLst)
        {
            _scheduleItemDataTable = new DataTable();
            _scheduleItemDataTable.Columns.Add(new DataColumn("Id", Type.GetType("System.Int32")));
            _scheduleItemDataTable.Columns.Add(new DataColumn("ScheduleId", Type.GetType("System.Int32")));
            _scheduleItemDataTable.Columns.Add(new DataColumn("Occurrence", Type.GetType("System.String")));
            _scheduleItemDataTable.Columns.Add(new DataColumn("Day", Type.GetType("System.String")));
            _scheduleItemDataTable.Columns.Add(new DataColumn("Week", Type.GetType("System.String")));
            _scheduleItemDataTable.Columns.Add(new DataColumn("Month", Type.GetType("System.String")));
            _scheduleItemDataTable.Columns.Add(new DataColumn("Year", Type.GetType("System.String")));
            _scheduleItemDataTable.Columns.Add(new DataColumn("StartTime", Type.GetType("System.DateTime")));
            _scheduleItemDataTable.Columns.Add(new DataColumn("EndTime", Type.GetType("System.DateTime")));
            _scheduleItemDataTable.Columns.Add(new DataColumn("StartDate", Type.GetType("System.DateTime")));
            _scheduleItemDataTable.Columns.Add(new DataColumn("EndDate", Type.GetType("System.DateTime")));
            _scheduleItemDataTable.Columns.Add(new DataColumn("CreatedDateTime", Type.GetType("System.DateTime")));
            _scheduleItemDataTable.Columns.Add(new DataColumn("LastModifiedDateTime", Type.GetType("System.DateTime")));
            _scheduleItemDataTable.Columns.Add(new DataColumn("CreatedUser", Type.GetType("System.String")));
            _scheduleItemDataTable.Columns.Add(new DataColumn("LastModifiedUser", Type.GetType("System.String")));
            _scheduleItemDataTable.Columns.Add(new DataColumn("ActiveStatus", Type.GetType("System.Boolean")));
            _scheduleItemDataTable.Columns.Add(new DataColumn("IsFixed", Type.GetType("System.Boolean")));
            _scheduleItemDataTable.Columns.Add(new DataColumn("LastActiveTimesGeneratedDate", Type.GetType("System.DateTime")));

            foreach (ScheduleItemDC scheduleitem in scheduleLst)
            {
                DataRow _dataTableRow = _scheduleItemDataTable.NewRow();
                _dataTableRow["Id"] = scheduleitem.Id;
                _dataTableRow["ScheduleId"] = scheduleitem.ScheduleId;
                _dataTableRow["Occurrence"] = scheduleitem.Occurrence;
                _dataTableRow["Day"] = scheduleitem.Day;
                if (scheduleitem.Week == -1)
                {
                    _dataTableRow["Week"] = null;
                }
                else
                {
                    _dataTableRow["Week"] = scheduleitem.Week;
                }
                _dataTableRow["Month"] = scheduleitem.Month;
                if (scheduleitem.Year == -1)
                {
                    _dataTableRow["Year"] = null;
                }
                else
                {
                    _dataTableRow["Year"] = scheduleitem.Year;
                };
                _dataTableRow["StartTime"] = scheduleitem.StartTime;
                _dataTableRow["EndTime"] = scheduleitem.EndTime;
                _dataTableRow["StartDate"] = scheduleitem.StartDate;
                _dataTableRow["EndDate"] = scheduleitem.EndDate;
                _dataTableRow["CreatedDateTime"] = DateTime.Now;
                _dataTableRow["LastModifiedDateTime"] = DateTime.Now;
                _dataTableRow["CreatedUser"] = scheduleitem.CreatedUser;
                _dataTableRow["LastModifiedUser"] = scheduleitem.LastModifiedUser;
                _dataTableRow["ActiveStatus"] = scheduleitem.ActiveStatus;
                _dataTableRow["IsFixed"] = scheduleitem.IsFixed;
                _dataTableRow["LastActiveTimesGeneratedDate"] = DBNull.Value;
                _scheduleItemDataTable.Rows.Add(_dataTableRow);
            }

            return _scheduleItemDataTable;
        }
    }
}
