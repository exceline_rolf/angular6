﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetTerminalTypeAction : USDBActionBase<List<TerminalType>>
    {
        public GetTerminalTypeAction()
        {
        }

        protected override List<TerminalType> Body(DbConnection connection)
        {
            var terminalTypesList = new List<TerminalType>();
            const string storedProcedureName = "dbo.USExceGMSAdminGetTerminalTypes";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var terminalType = new TerminalType
                        {
                            TypeId = (reader["TypeID"] != DBNull.Value) ? Convert.ToString(reader["TypeID"]) : "",
                            TypeName = (reader["TypeName"] != DBNull.Value)
                                           ? Convert.ToString(reader["TypeName"])
                                           : "",
                            DefaultPort =
                                (reader["DefaultPort"] != DBNull.Value) ? Convert.ToInt32(reader["DefaultPort"]) : -1
                        };
                    terminalTypesList.Add(terminalType);
                }
            }
            catch (Exception)
            {
                throw;
            }

            return terminalTypesList;
        }
    }
}
