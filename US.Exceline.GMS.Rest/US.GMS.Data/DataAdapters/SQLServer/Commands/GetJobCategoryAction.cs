﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : DKA
// Created Timestamp : "10/10/2013 6:06:49 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.Common;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetJobCategoryAction : USDBActionBase<List<ExcelineJobCategoryDC>>
    {
        //private int _branchId = -1;
        private string _gymCode = string.Empty;

        public GetJobCategoryAction(int branchId, string gymCode)
        {
           // this._branchId = branchId;
            _gymCode = gymCode;
        }

        protected override List<ExcelineJobCategoryDC> Body(DbConnection connection)
        {
            List<ExcelineJobCategoryDC> jobCategoryList = new List<ExcelineJobCategoryDC>();
            string storedProcedureName = "USExceGMSAdminGetJobCategories";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
              //  cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ExcelineJobCategoryDC jobCategory = new ExcelineJobCategoryDC();
                    jobCategory.Id = Convert.ToInt32(reader["ID"]);
                    jobCategory.Name = reader["Name"].ToString();
                    jobCategory.IsRole = Convert.ToBoolean(reader["IsRole"]);
                    jobCategory.IsEmployee = Convert.ToBoolean(reader["IsEmployee"]);

                    GetExtFieldsByCategoryAction action = new GetExtFieldsByCategoryAction(jobCategory.Id, "JOB");
                    jobCategory.ExtendedFieldsList = action.Execute(EnumDatabase.Exceline, _gymCode);
                    jobCategoryList.Add(jobCategory);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return jobCategoryList;
        }
    }
}
