﻿
// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageContracts;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class ImportArticleListAction : USDBActionBase<bool>
    {
        private List<ArticleDC> _articleList = new List<ArticleDC>();
        private string _user = string.Empty;
        private int _branchID = -1;
        private string _gymCode = string.Empty;


        public ImportArticleListAction(List<ArticleDC> articleList, string user, int branchID, string gymCode)
        {
            _articleList = articleList;
            _user = user;
            _branchID = branchID;
            _gymCode = gymCode;
        }

        protected override bool Body(DbConnection connection)
        {
            bool result = false;
            const string storedProcedure = "USExceGMSImportArticleList";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                if (cmd != null)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchID", DbType.Int32, _branchID));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@gymCode", DbType.String, _gymCode));

                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@articleList", SqlDbType.Structured, GetAricleTable(_articleList)));
                    cmd.ExecuteNonQuery();
                }
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        private DataTable GetAricleTable(List<ArticleDC> articleList)
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn("Activity", typeof(string)));
            dataTable.Columns.Add(new DataColumn("ArticleName", typeof(string)));
            dataTable.Columns.Add(new DataColumn("Barcode", typeof(string)));
            dataTable.Columns.Add(new DataColumn("CategoryName", typeof(string)));
            dataTable.Columns.Add(new DataColumn("ContractBooking", typeof(string)));
            dataTable.Columns.Add(new DataColumn("DefaultPrice", typeof(string)));
            dataTable.Columns.Add(new DataColumn("EmployeePrice", typeof(string)));
            dataTable.Columns.Add(new DataColumn("InStock", typeof(string)));
            dataTable.Columns.Add(new DataColumn("NumberOfUnits", typeof(string)));
            dataTable.Columns.Add(new DataColumn("Obsolete ", typeof(string)));
            dataTable.Columns.Add(new DataColumn("PunchCard", typeof(string)));
            dataTable.Columns.Add(new DataColumn("PurchasedPrice", typeof(string)));
            dataTable.Columns.Add(new DataColumn("ReorderLevel", typeof(string)));
            dataTable.Columns.Add(new DataColumn("RevenueAccountNo ", typeof(string)));
            dataTable.Columns.Add(new DataColumn("Type", typeof(string)));
            dataTable.Columns.Add(new DataColumn("ShortcutKey ", typeof(string)));
            dataTable.Columns.Add(new DataColumn("StockItem", typeof(string)));
            dataTable.Columns.Add(new DataColumn("Unit", typeof(string)));
            dataTable.Columns.Add(new DataColumn("VatCode ", typeof(string)));
            dataTable.Columns.Add(new DataColumn("VendorName ", typeof(string)));
            dataTable.Columns.Add(new DataColumn("Voucher", typeof(string)));


            foreach (ArticleDC article in articleList)
            {
                DataRow row = dataTable.NewRow();
                row["Activity"] = article.ActivityName;
                row["ArticleName"] = article.Description;
                row["Barcode"] = article.BarCode;
                row["CategoryName"] = article.Category;
                row["ContractBooking"] = article.IsContractBooking;
                row["DefaultPrice"] = article.DefaultPrice;
                row["EmployeePrice"] = article.EmployeePrice;
                row["InStock"] = article.StockLevel;
                row["NumberOfUnits"] = article.UnitPeriod;
                row["Obsolete"] = article.ObsoleteStatus;
                row["PunchCard"] = article.IsPunchCard;
                row["PurchasedPrice"] = article.PurchasedPrice;
                row["ReorderLevel"] = article.ReOrderLevel;
                row["RevenueAccountNo"] = article.RevenueAccountNo;
                //  row["Type"] = article.IsWednesday;
                row["ShortcutKey"] = article.SortCutKey;
                row["StockItem"] = article.StockStatus;
                row["Unit"] = article.Currency;
                row["VatCode"] = article.VatCode;
                row["VendorName"] = article.VendorName;
                row["Voucher"] = article.IsVoucher;
                dataTable.Rows.Add(row);
            }
            return dataTable;
        }
    }
}
