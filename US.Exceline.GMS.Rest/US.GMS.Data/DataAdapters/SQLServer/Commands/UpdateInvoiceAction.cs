﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateInvoiceAction : USDBActionBase<bool>
    {
        private int _arItemNo = -1;
        private DateTime _dueDate;
        private string _comment = string.Empty;
        public UpdateInvoiceAction(int arItemNo, DateTime DueDate, string comment)
        {
            _arItemNo = arItemNo;
            _dueDate = DueDate;
            _comment = comment;
        }
        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSManageMembershipUpdateInvoice";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@ArItemNo", System.Data.DbType.Int32, _arItemNo));
                command.Parameters.Add(DataAcessUtils.CreateParam("@DueDate", System.Data.DbType.DateTime, _dueDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Comment", System.Data.DbType.String, _comment));
                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception )
            {
                throw;
            }
        }
    }
}
