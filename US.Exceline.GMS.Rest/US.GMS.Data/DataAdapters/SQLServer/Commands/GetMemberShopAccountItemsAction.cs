﻿using System;
using System.Collections.Generic;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageMemberships;
using System.Data.Common;
using System.Data;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetMemberShopAccountItemsAction : USDBActionBase<List<MemberShopAccountItemDC>>
    {
        private readonly int _shopAccountId;
        private readonly int _hit;
        public GetMemberShopAccountItemsAction(int shopAccountId, int hit)
        {
            _shopAccountId = shopAccountId;
            _hit = hit;
        }

        protected override List<MemberShopAccountItemDC> Body(DbConnection connection)
        {
            List<MemberShopAccountItemDC> memberShopAccountItems = new List<MemberShopAccountItemDC>();
            const string storedProcedureName = "USExceGMSManageMembershipGetMemberShopAccountItems";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ShopAccountID", DbType.Int32, _shopAccountId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Hit", DbType.Int32, _hit));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    MemberShopAccountItemDC memberAccountItem = new MemberShopAccountItemDC();
                    memberAccountItem.Id = Convert.ToInt32(reader["ID"]);
                    if (reader["Amount"] != DBNull.Value)
                    {
                        memberAccountItem.PaymentAmount = Convert.ToDecimal(reader["Amount"]);
                    }
                    memberAccountItem.Type = Convert.ToString(reader["Type"]);
                    memberAccountItem.PayMode = Convert.ToString(reader["PaymentType"]);
                    memberAccountItem.InvoiceNo = Convert.ToString(reader["InvoiceNo"]);
                    if (reader["CreatedDateTime"] != DBNull.Value)
                    {
                        memberAccountItem.CreatedDateTime = Convert.ToDateTime(reader["CreatedDateTime"]);
                    }
                    memberShopAccountItems.Add(memberAccountItem);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return memberShopAccountItems;
        }
    }
}
