﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetGymSettingActionByType : USDBActionBase<Dictionary<string, object>>
    {
        private int _branchId = -1;
        private string _type = string.Empty;

        public GetGymSettingActionByType(int branchId,string type)
        {
            this._branchId = branchId;
            this._type = type;
        }

        protected override Dictionary<string, object> Body(DbConnection connection)
        {
             string StoredProcedureName = "USExceGMSGetGymSetting";
             //string GetAccessProfileSP = "USExceGMSGetAccessProfiles";
             Dictionary<string, object> gymSettingValues = new Dictionary<string, object>();

             try
             {
                 DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                 cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                 DbDataReader reader = cmd.ExecuteReader();

                 if (_type == "SMSSettings")
                 {
                     while (reader.Read())
                     {
                         gymSettingValues.Add("Id", reader["Id"]);
                         gymSettingValues.Add("ServiceSMSFreq", reader["ServiceSMSFreq"]);
                         gymSettingValues.Add("ReceiveServiceSMSStart", reader["ServiceSMSStart"]);
                         gymSettingValues.Add("ReceiveServiceSMSEnd", reader["ServiceSMSEnd"]);
                         gymSettingValues.Add("MemberSMSFreq",reader["MemberSMSFreq"]);
                         gymSettingValues.Add("ReceiveMemberSMSStart", reader["MemberSMSStart"]);
                         gymSettingValues.Add("ReceiveMemberSMSEnd", reader["MemberSMSEnd"]);
                         gymSettingValues.Add("RemainderSMSFreq", reader["RemainderSMSFreq"]);
                         gymSettingValues.Add("ReceiveReminderSMSStart", reader["RemaindeSMSStart"]);
                         gymSettingValues.Add("ReceiveReminderSMSEnd", reader["RemaindeSMSEnd"]);
                         gymSettingValues.Add("MaxSMS", reader["MaxSMS"]);
                         gymSettingValues.Add("SMSBookingReminder", reader["SMSBookingReminder"]);

                     }

                 }
                 else if (_type == "RequiredSettings")
                 {
                     while (reader.Read())
                     {
                         gymSettingValues.Add("Id", reader["Id"]);
                         gymSettingValues.Add("MemberRequired", reader["MemberRequired"]);
                         gymSettingValues.Add("MobileRequired", reader["MobileRequired"]);
                         gymSettingValues.Add("AddressRequired", reader["AddressRequired"]);
                         gymSettingValues.Add("ZipCodeRequired", reader["ZipCodeRequired"]);                       

                     }
                 }
                 else if (_type == "OtherSettings")
                 {
                     gymSettingValues.Add("Id", reader["Id"]);
                     gymSettingValues.Add("CountVisitAfter", reader["CountVisitAfter"]);
                     gymSettingValues.Add("PlanIn", reader["PlanIn"]);
                     gymSettingValues.Add("TimeOut", reader["TimeOutAfter"]);
                     gymSettingValues.Add("BarcodeAvailable", reader["BarcodeAvailable"]);
                     gymSettingValues.Add("PrintReceipt", reader["PrintReceipt"]);
                     gymSettingValues.Add("PhoneCode", reader["PhoneCode"]);
                     gymSettingValues.Add("CreditPeriod", reader["CreditPeriod"]);
                     gymSettingValues.Add("InvoiceCancellationPeriod", reader["InvoiceCancellationPeriod"]);
                     gymSettingValues.Add("IsShopAvailable", reader["IsShopAvailable"]);
                     gymSettingValues.Add("CreditDueDate", reader["CreditDueDate"]);
                     gymSettingValues.Add("UseTodayAsDueDate", reader["UseTodayAsDueDate"]);
                 }
                 else if (_type == "MemberSearchSettings")
                 {
                     gymSettingValues.Add("MemberSearchText", reader["MemberSearchText"]);
                 }
             }
             catch(Exception ex)
             {
                 throw ex;
             }

             return gymSettingValues;
        }


    }
}
