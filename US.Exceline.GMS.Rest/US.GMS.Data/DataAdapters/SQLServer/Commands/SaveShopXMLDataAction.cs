﻿using System;
using System.Data;
using System.Data.Common;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class SaveShopXMLDataAction : USDBActionBase<int>
    {
        private String _xmlData;
        private String _sessionKey;

        public SaveShopXMLDataAction(String xmlData, String sessionKey)
        {
            _xmlData = xmlData;
            _sessionKey = sessionKey;
        }

        protected override int Body(DbConnection connection)
        {
            const string storedProcedureName = "AddShopLoggInstance";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@XmlData", DbType.String, _xmlData));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SessionKey", DbType.String, _sessionKey));
                int result = 0;
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    result  = Convert.ToInt32(reader["Result"]);
                }

                return result;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
    }



}

      

   

