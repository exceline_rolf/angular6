﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GenerateMemberFeeAction : USDBActionBase<MemberFeeGenerationResultDC>
    {
        private List<int> _gyms = new List<int>();
        private int _month = -1;
        private string _user = string.Empty;

        public GenerateMemberFeeAction(List<int> gyms, int month, string user)
        {
            _gyms = gyms;
            _month = month;
            _user = user;
        }

        protected override MemberFeeGenerationResultDC Body(System.Data.Common.DbConnection connection)
        {
            string spName = "ExceGMSGenerateMemberFee";
            MemberFeeGenerationResultDC result = new MemberFeeGenerationResultDC();
            try
            {
                DbCommand command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@Month", DbType.Int32, _month));
                command.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _user));

                var parameter = new SqlParameter
                {
                    ParameterName = "@Branches",
                    SqlDbType = SqlDbType.Structured,
                    Value = GetIds(_gyms)
                };

                command.Parameters.Add(parameter);
                command.ExecuteNonQuery();

                result.Status = true;
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private DataTable GetIds(List<int> gyms)
        {
            DataTable branches = new DataTable();
            DataColumn col = new DataColumn("ID", typeof(Int32));
            branches.Columns.Add(col);
            foreach (var id in gyms)
            {
                branches.Rows.Add(id);
            }
            return branches;
        }


    }
}
