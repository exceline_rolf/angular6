﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : JNI
// Created Timestamp : "6/20/2012 6:07:12 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Common;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.Utils;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetExcelineTasksAction : USDBActionBase<List<ExcelineTaskDC>>
    {
        private int _branchId = -1;
        private int _templateId = -1;
        private bool _isFxied = false;
        private int _assignTo;
        private string _roleType;
        private int _followupMemberId;

        public GetExcelineTasksAction(int branchId, bool isFxied, int templateId, int assignTo, string roleType, int followupMemberId)
        {
            _branchId = branchId;
            _templateId = templateId;
            _isFxied = isFxied;
            _assignTo = assignTo;
            _roleType = roleType;
            _followupMemberId = followupMemberId;
        }

        protected override List<ExcelineTaskDC> Body(DbConnection connection)
        {
            List<ExcelineTaskDC> exeTaskList = new List<ExcelineTaskDC>();
            string storedProcedureName = "USExceGMSGetTasks";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@isFxied", DbType.Boolean, _isFxied));
                if (_templateId != -1)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@templateId", DbType.Int32, _templateId));
                if (_assignTo != -1)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@AssignTo", DbType.Int32, _assignTo));
                else
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@AssignTo", DbType.Int32, null));
                if (!string.IsNullOrEmpty(_roleType))
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@RoleType", DbType.String, _roleType));
                if (_followupMemberId != -1)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@FollowUpMemId", DbType.Int32, _followupMemberId));

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ExcelineTaskDC exeTask = new ExcelineTaskDC();
                    exeTask.Id = Convert.ToInt32(reader["Id"]);
                    exeTask.Title = reader["Name"].ToString();
                    exeTask.Description = reader["Description"].ToString();
                    exeTask.TaskTemplateId = Convert.ToInt32(reader["TaskTemplateId"]);
                    exeTask.Schedule.Id = Convert.ToInt32(reader["ScheduleId"]);
                    exeTask.BranchId = Convert.ToInt32(reader["BranchId"]);
                    exeTask.IsFxied = Convert.ToBoolean(reader["IsFxied"]);
                    exeTask.IsPopup = Convert.ToBoolean(reader["IsPopup"]);
                    exeTask.NumOfDays = Convert.ToInt32(reader["NumOfDays"]);
                    exeTask.ActiveStatus = Convert.ToBoolean(reader["ActiveStatus"]);

                    exeTask.StartDate = Convert.ToDateTime(reader["StartDate"]);
                    exeTask.EndDate = Convert.ToDateTime(reader["EndDate"]);
                    exeTask.StartTime = Convert.ToDateTime(reader["StartTime"]);
                    exeTask.EndTime = Convert.ToDateTime(reader["EndTime"]);
                    exeTask.FoundDate = Convert.ToDateTime(reader["FoundDate"]);
                    exeTask.ReturnDate = Convert.ToDateTime(reader["ReturnDate"]);
                    exeTask.DueDate = Convert.ToDateTime(reader["DueDate"]);
                    exeTask.DueTime = Convert.ToDateTime(reader["DueTime"]);
                    exeTask.AssignTo = Convert.ToInt32(reader["EntRoleId"]);
                    exeTask.AssigntoName = reader["AssignToName"].ToString();
                    exeTask.AssignEntityRole = reader["RoleId"].ToString();
                    exeTask.FollowUpMemberId = Convert.ToInt32(reader["MemberId"]);
                    exeTask.BelongsToMemberId = Convert.ToInt32(reader["BelongsToMemberId"]);
                    exeTask.IsShowInCalandar = Convert.ToBoolean(reader["IsShowInCalandar"]);
                    exeTask.PhoneNo = reader["PhoneNo"].ToString();
                    exeTask.Sms = reader["Sms"].ToString();

                    exeTask.Schedule.StartDate = Convert.ToDateTime(reader["SheduleStartDate"]);
                    exeTask.Schedule.EndDate = Convert.ToDateTime(reader["SheduleEndDate"]);

                    if (!String.IsNullOrEmpty(reader["ExtenedInfo"].ToString()))
                    {
                        ExtendedFieldInfoDC fieldInfo = new ExtendedFieldInfoDC();
                        fieldInfo = (ExtendedFieldInfoDC)XMLUtils.DesrializeXMLToObject(reader["ExtenedInfo"].ToString(), typeof(ExtendedFieldInfoDC));
                        exeTask.ExtendedFieldsList = fieldInfo.ExtendedFieldsList;
                    }
                    exeTaskList.Add(exeTask);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return exeTaskList;
        }
    }
}
