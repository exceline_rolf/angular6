﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Common;
using System.Data.Common;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetRegionsAction : USDBActionBase<List<RegionDC>>
    {
        private string _countryId = string.Empty;
        public GetRegionsAction(string countryId)
        {
            _countryId = countryId;
        }
        protected override List<RegionDC> Body(DbConnection connection)
        {
            List<RegionDC> regionList = new List<RegionDC>();
            string storedProcedure = "USExceGMSGetRegions";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                if (!string.IsNullOrEmpty(_countryId))
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CountryId", DbType.String, _countryId));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    RegionDC region = new RegionDC();
                    region.Id = Convert.ToInt32(reader["Id"]);
                    region.Code = reader["Code"].ToString();
                    region.Name = reader["Name"].ToString();
                    region.Description = reader["Description"].ToString();
                    region.CountryId = reader["CountryId"].ToString();
                    regionList.Add(region);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return regionList;  
        }
    }
}
