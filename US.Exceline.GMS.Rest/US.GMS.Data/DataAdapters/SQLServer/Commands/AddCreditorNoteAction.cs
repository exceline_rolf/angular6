﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US.Common.Logging.API;
using US.Exceline.GMS.Modules.Economy.API;
using US.GMS.Core.DomainObjects.Economy;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.SystemObjects;
using US.Payment.Core.BusinessDomainObjects;
using US.Payment.Core.DomainObjects;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class AddCreditorNoteAction : USDBActionBase<int>
    {
        private CreditNoteDC _creditNote;
        private int _branchId = -1;
        private bool _isReturn = false;

        public AddCreditorNoteAction(CreditNoteDC creditNote, int branchId, bool isReturn, string gymCode)
        {
            _creditNote = creditNote;
            _branchId = branchId;
            _isReturn = isReturn;
        }

        protected override int Body(DbConnection connection)
        {
            int result = 1;
            try
            {
                if (_creditNote != null)
                {

                    string StoredProcedureName = "USExceGMSAddCreditInvoice";
                    DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ArItemNo", DbType.Int32, _creditNote.ArItemNo));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _creditNote.User));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchID", DbType.Int32, _branchId));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreditedAmount", DbType.Decimal, _creditNote.Amount));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsReturn", DbType.Boolean, _isReturn));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@comment", DbType.String, _creditNote.Note));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@creditNoteDetails", SqlDbType.Structured, GetDetailsTable()));

                    DbParameter output = new SqlParameter();
                    output.DbType = DbType.Int32;
                    output.ParameterName = "@outId";
                    output.Size = 50;
                    output.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(output);
                    cmd.ExecuteNonQuery();
                    result = Convert.ToInt32(output.Value);
                    return result;
 
                }
                return -1;
            }
            catch
            {
                throw;
            }
        }

        private DataTable GetDetailsTable()
        {
            try
            {
                DataTable dataTable = new DataTable();
                dataTable.Columns.Add(new DataColumn("CreditorNumber", typeof(string)));
                dataTable.Columns.Add(new DataColumn("Date", typeof(DateTime)));
                dataTable.Columns.Add(new DataColumn("EditUser", typeof(string)));
                dataTable.Columns.Add(new DataColumn("Note", typeof(string)));
                dataTable.Columns.Add(new DataColumn("ARItemNo", typeof(int)));
                dataTable.Columns.Add(new DataColumn("ArticleText", typeof(string)));
                dataTable.Columns.Add(new DataColumn("OrderLineId", typeof(int)));
                dataTable.Columns.Add(new DataColumn("CreditedQuantity", typeof(int)));
                dataTable.Columns.Add(new DataColumn("CreditedAmount", typeof(decimal)));
                dataTable.Columns.Add(new DataColumn("ArticleID", typeof(int)));

                foreach (CreditNoteDetailsDC creditNoteDetail in _creditNote.Details)
                {
                    DataRow dataRow = dataTable.NewRow();
                    dataRow["CreditorNumber"] = _creditNote.CreditorNumber;
                    dataRow["Date"] = DateTime.Today;
                    dataRow["EditUser"] = _creditNote.User;
                    dataRow["Note"] = _creditNote.Note; ;
                    dataRow["ARItemNo"] = -1;
                    dataRow["ArticleText"] = creditNoteDetail.ArticleText;
                    dataRow["OrderLineId"] = creditNoteDetail.OrderlineId;
                    dataRow["CreditedQuantity"] = creditNoteDetail.CreditedCount;
                    dataRow["CreditedAmount"] = creditNoteDetail.Amount;
                    dataRow["ArticleID"] = creditNoteDetail.ArticleId;
                    dataTable.Rows.Add(dataRow);
                }

                return dataTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
