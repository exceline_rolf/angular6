﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US.GMS.Core.DomainObjects.Admin;
using US.GMS.Core.DomainObjects.Admin.ManageClassSettings;
using US.GMS.Core.DomainObjects.Common;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class AddEditClassTypeAction : USDBActionBase<bool>
    {
        private ExceClassTypeDC _classType;
        public AddEditClassTypeAction(ExceClassTypeDC classType)
        {
            _classType = classType;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string storedProcedure = "USExceGMSAdminAddClassType";
            bool isSuccess = false;
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", System.Data.DbType.Int32, _classType.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Name", System.Data.DbType.String, _classType.Name));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ClassGroupId", System.Data.DbType.Int32, _classType.ClassGroupId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Colour", System.Data.DbType.String , _classType.Colour));
                if (_classType.ClassLevelId != 0)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ClassLevelId", System.Data.DbType.Int32, _classType.ClassLevelId));
                }
                if (_classType.TimeCategoryId != 0)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@TimeCategoryId", System.Data.DbType.Int32, _classType.TimeCategoryId));
                }
                if (_classType.HoursToBePaid > 0)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@HoursToBePaid", System.Data.DbType.Decimal, _classType.HoursToBePaid));

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsLocal", System.Data.DbType.Boolean, _classType.IsLocal));
                if (!string.IsNullOrEmpty(_classType.Comment))
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@Comment", System.Data.DbType.String, _classType.Comment));
                }

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TimeDuration", System.Data.DbType.Int32, _classType.TimeDuration));

                //if (_classType.HoursToBePaid > 0)
                //    cmd.Parameters.Add(DataAcessUtils.CreateParam("@HoursToBePaid", System.Data.DbType.Decimal, _classType.HoursToBePaid));

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Obsolete", System.Data.DbType.Boolean, _classType.ObsoleteLevel));

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Categories", SqlDbType.Structured, GetClassTypeCategories(_classType.ClassCategoryList, _classType.Id)));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Branches", SqlDbType.Structured, GetClassTypeBranches(_classType.ExcelineBranchList, _classType.Id)));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Keywords", SqlDbType.Structured, GetClassTypeKeywords(_classType.ClassKeywordList, _classType.Id)));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ClassKeywords", DbType.String, _classType.ClassKeywordString));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MaxNumberOfBookings", DbType.Int32, _classType.MaxNoOfBookings));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EnglishComment", DbType.String, _classType.EnglishComment));

                SqlParameter output = new SqlParameter("@OutPutClassTypeId", SqlDbType.Int);
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);
                cmd.ExecuteNonQuery();
                isSuccess = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return isSuccess;
        }

        private DataTable GetClassTypeCategories(List<CategoryDC> classTypeCategories, int classTypeId)
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn("ClassTypeId", typeof(int)));
            dataTable.Columns.Add(new DataColumn("ItemId", typeof(int)));


            if (classTypeCategories != null)
            {
                foreach (CategoryDC cat in classTypeCategories)
                {
                    DataRow dataTableRow = dataTable.NewRow();
                    dataTableRow["ClassTypeId"] = classTypeId;
                    dataTableRow["ItemId"] = cat.Id;
                    dataTable.Rows.Add(dataTableRow);
                }
            }
            return dataTable;
        }

        private DataTable GetClassTypeBranches(List<ExcelineBranchDC> classTypeBranches, int classTypeId)
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn("ClassTypeId", typeof(int)));
            dataTable.Columns.Add(new DataColumn("ItemId", typeof(int)));
            if (classTypeBranches != null)
            {
                foreach (ExcelineBranchDC branch in classTypeBranches)
                {
                    DataRow dataTableRow = dataTable.NewRow();
                    dataTableRow["ClassTypeId"] = classTypeId;
                    dataTableRow["ItemId"] = branch.Id;
                    dataTable.Rows.Add(dataTableRow);
                }
            }
            return dataTable;
        }

        private DataTable GetClassTypeKeywords(List<CategoryDC> classTypekeywords, int classTypeId)
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn("ClassTypeId", typeof(int)));
            dataTable.Columns.Add(new DataColumn("ItemId", typeof(int)));
            if (classTypekeywords != null)
            {
                foreach (CategoryDC key in classTypekeywords)
                {
                    DataRow dataTableRow = dataTable.NewRow();
                    dataTableRow["ClassTypeId"] = classTypeId;
                    dataTableRow["ItemId"] = key.Id;
                    dataTable.Rows.Add(dataTableRow);
                }
            }
            return dataTable;
        }
    }
}
