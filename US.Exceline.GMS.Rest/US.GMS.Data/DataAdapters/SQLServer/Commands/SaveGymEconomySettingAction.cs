﻿using System;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US_DataAccess;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class SaveGymEconomySettingAction : USDBActionBase<bool>
    {
        private GymEconomySettingsDC _gymEconomySetting = new GymEconomySettingsDC();
        private bool _returnValue;

        public SaveGymEconomySettingAction(int branchId, GymEconomySettingsDC gymEconomySetting)
        {
            _gymEconomySetting = gymEconomySetting;
        }

        protected override bool Body(DbConnection connection)
        {
            const string storedProcedure = "USExceGMSSaveGymEconomySetting";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", System.Data.DbType.Int32, _gymEconomySetting.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", System.Data.DbType.Int32, _gymEconomySetting.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MoveExceededPaymentToInvoice", System.Data.DbType.Boolean, _gymEconomySetting.MoveExceededPayToInvoice));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@RestPlusMonth", System.Data.DbType.Int32, _gymEconomySetting.RestPlusMonth));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InvoiceAccountNo", System.Data.DbType.String, _gymEconomySetting.InvoiceAccountNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@PaymentAccountNo", System.Data.DbType.String, _gymEconomySetting.PaymentAccountNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@DebtWarningNoDate", System.Data.DbType.Int32, _gymEconomySetting.DebtCollectionPeriod));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InvoiceCancellationPeriod", System.Data.DbType.Int32, _gymEconomySetting.InvoiceCancellationPeriod));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ReminderPeriod", System.Data.DbType.Int32, _gymEconomySetting.RemainderPeriod));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ReminderFee", System.Data.DbType.Decimal, _gymEconomySetting.RemainderFee));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SMSReminderFee", System.Data.DbType.Decimal, _gymEconomySetting.SmsRemainderFee));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InvoiceCharge", System.Data.DbType.Decimal, _gymEconomySetting.InvoiceCharge));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SuspendedAfter", System.Data.DbType.Int32, _gymEconomySetting.SuspendedDays));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@NoOrdersResigning", System.Data.DbType.Int32, _gymEconomySetting.NoOfOrdersWhenResign));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EnrollmentFeeFirstOrderIfSponsor", System.Data.DbType.Decimal, _gymEconomySetting.EnrollmentFeeFirstOrderIfSponsor));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreditUnpaidInvoiceFee", System.Data.DbType.Int32, _gymEconomySetting.CreditUnpaidInvoiceFee));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreditUnpaidRemainderFee", System.Data.DbType.Int32, _gymEconomySetting.CreditUnpaidRemainderFee));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreditPaidDeviationSettableAmount", System.Data.DbType.Decimal, _gymEconomySetting.CreditPaidDeviationSettableAmount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ShopOnNextOrder", System.Data.DbType.Boolean, _gymEconomySetting.ShopOnNextOrder));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@OnAccount", System.Data.DbType.Boolean, _gymEconomySetting.OnAccount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@NegativeOnAccount", System.Data.DbType.Decimal, _gymEconomySetting.OnNegativeAccount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsDefaultCustomerAvailable", System.Data.DbType.Boolean, _gymEconomySetting.IsDefaultCustomerAvailable));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsPayButtonsAvailable", System.Data.DbType.Boolean, _gymEconomySetting.IsPayButtonAvailable));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsPettyCashAllowedOnUser", System.Data.DbType.Int32, _gymEconomySetting.IsPettyCashAllowedOnUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@NetsCustomerId", System.Data.DbType.String, _gymEconomySetting.NetsCustomerId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@NetsAggigmentNo", System.Data.DbType.String, _gymEconomySetting.NetsAssignmentNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@NetsCancelationPerid", System.Data.DbType.Int32, _gymEconomySetting.NetsCancellationPeriod));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsBankTerminalIntegrated", System.Data.DbType.Boolean, _gymEconomySetting.IsBankTerminalIntegrated));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsPrintInvoiceInShop", System.Data.DbType.Boolean, _gymEconomySetting.IsPrintInvoiceInShop));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsPrintReceiptInShop", System.Data.DbType.Boolean, _gymEconomySetting.IsPrintReceiptInShop));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsShopOnNextOrder", System.Data.DbType.Boolean, _gymEconomySetting.IsShopOnNextOrder));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreditSaleLimit", System.Data.DbType.Decimal, _gymEconomySetting.CreditSaleLimit));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SmsInvoice", System.Data.DbType.Boolean, _gymEconomySetting.IsSmsInvoiceShop));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsDailySettlementForAll", System.Data.DbType.Boolean, _gymEconomySetting.IsDailySettlementForAll));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@PaidAccessMode", System.Data.DbType.Byte, _gymEconomySetting.PaidAccessMode));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SMSReminderPeriod", System.Data.DbType.Int32, _gymEconomySetting.SmsReminderPeriod));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsMemberFee", System.Data.DbType.Boolean, _gymEconomySetting.IsMemberFee));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsMemberFeeMonth", System.Data.DbType.Boolean, _gymEconomySetting.IsMemberFeeMonth));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsMemberFeeAutoGenerate", System.Data.DbType.Boolean, _gymEconomySetting.IsMemberFeeAutoGenerate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsMemberFeeATG", System.Data.DbType.Boolean, _gymEconomySetting.IsMemberFeeATG));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsCashSalesActived", System.Data.DbType.Boolean, _gymEconomySetting.IsCashSalesActivated));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@NotEditableVoucher", System.Data.DbType.Boolean, _gymEconomySetting.NotEditableVoucher));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ShopReturnOnSelectedBranchOnly", System.Data.DbType.Boolean, _gymEconomySetting.ShopReturnOnSelectedBranchOnly));
                if (_gymEconomySetting.MemberFeeGenerateDay <= 0)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@MemberFeeGenerateDay", System.Data.DbType.Int32, 1));
                }
                else
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@MemberFeeGenerateDay", System.Data.DbType.Int32, _gymEconomySetting.MemberFeeGenerateDay));
                }

                if (_gymEconomySetting.MemberFeeBranchID > 0)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@MemberFeeBranchID", System.Data.DbType.Int32, _gymEconomySetting.MemberFeeBranchID));
                }
                if (_gymEconomySetting.MoveToPrepaidBalanace)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@MoveExceedPaymentsTo", System.Data.DbType.Int32, 1));
                else
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@MoveExceedPaymentsTo", System.Data.DbType.Int32, 2));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SMSInvoiceFee", System.Data.DbType.Decimal, _gymEconomySetting.SmsInvoiceCharge));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EditRemainingPunches", System.Data.DbType.Decimal, _gymEconomySetting.EditRemainingPunches));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CCXEnabled", System.Data.DbType.Boolean, _gymEconomySetting.CCXEnabled));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@PrintIpccx", System.Data.DbType.Boolean, _gymEconomySetting.PrintIpccx));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@PrintPpccx", System.Data.DbType.Boolean, _gymEconomySetting.PrintPpccx));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@PrintSpccx", System.Data.DbType.Boolean, _gymEconomySetting.PrintSpccx));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SendSmSforPriceChange", System.Data.DbType.Boolean, _gymEconomySetting.SendSmSforPriceChange));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@PostPay", System.Data.DbType.Boolean, _gymEconomySetting.PostPay));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BasisX", System.Data.DbType.Boolean, _gymEconomySetting.BasisX));
                cmd.ExecuteNonQuery();
                _returnValue = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _returnValue;
        }
    }
}
