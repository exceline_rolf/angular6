﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : JNI
// Created Timestamp : "6/20/2012 6:07:55 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using System.Collections.ObjectModel;
using US.GMS.Core.Utils;
using System.Data.SqlClient;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class SaveExcelineTaskAction : USDBActionBase<int>
    {
        private ExcelineTaskDC _excelineTask;
        private DataTable _dataTable;

        public SaveExcelineTaskAction(ExcelineTaskDC excelineTask)
        {
            _excelineTask = excelineTask;
            if (excelineTask.Schedule != null)
                _dataTable = GetScheduleItemTypeLst(_excelineTask.Schedule.SheduleItemList);
        }

        private DataTable GetScheduleItemTypeLst(ObservableCollection<ScheduleItemDC> scheduleLst)
        {
            _dataTable = new DataTable();
            _dataTable.Columns.Add(new DataColumn("Id", Type.GetType("System.Int32")));
            _dataTable.Columns.Add(new DataColumn("ScheduleId", Type.GetType("System.Int32")));
            _dataTable.Columns.Add(new DataColumn("Occurrence", Type.GetType("System.String")));
            _dataTable.Columns.Add(new DataColumn("Day", Type.GetType("System.String")));
            _dataTable.Columns.Add(new DataColumn("Week", Type.GetType("System.String")));
            _dataTable.Columns.Add(new DataColumn("Month", Type.GetType("System.String")));
            _dataTable.Columns.Add(new DataColumn("Year", Type.GetType("System.String")));
            _dataTable.Columns.Add(new DataColumn("StartTime", Type.GetType("System.DateTime")));
            _dataTable.Columns.Add(new DataColumn("EndTime", Type.GetType("System.DateTime")));
            _dataTable.Columns.Add(new DataColumn("StartDate", Type.GetType("System.DateTime")));
            _dataTable.Columns.Add(new DataColumn("EndDate", Type.GetType("System.DateTime")));
            _dataTable.Columns.Add(new DataColumn("CreatedDateTime", Type.GetType("System.DateTime")));
            _dataTable.Columns.Add(new DataColumn("LastModifiedDateTime", Type.GetType("System.DateTime")));
            _dataTable.Columns.Add(new DataColumn("CreatedUser", Type.GetType("System.String")));
            _dataTable.Columns.Add(new DataColumn("LastModifiedUser", Type.GetType("System.String")));
            _dataTable.Columns.Add(new DataColumn("ActiveStatus", Type.GetType("System.Boolean")));
            _dataTable.Columns.Add(new DataColumn("IsFixed", Type.GetType("System.Boolean")));
            _dataTable.Columns.Add(new DataColumn("LastActiveTimesGeneratedDate", Type.GetType("System.DateTime")));

            foreach (ScheduleItemDC scheduleitem in scheduleLst)
            {
                DataRow _dataTableRow = _dataTable.NewRow();
                _dataTableRow["Id"] = scheduleitem.Id;
                _dataTableRow["ScheduleId"] = scheduleitem.ScheduleId;
                _dataTableRow["Occurrence"] = scheduleitem.Occurrence;
                _dataTableRow["Day"] = scheduleitem.Day;
                if (scheduleitem.Week == -1)
                {
                    _dataTableRow["Week"] = null;
                }
                else
                {
                    _dataTableRow["Week"] = scheduleitem.Week;
                }
                _dataTableRow["Month"] = scheduleitem.Month;
                if (scheduleitem.Year == -1)
                {
                    _dataTableRow["Year"] = null;
                }
                else
                {
                    _dataTableRow["Year"] = scheduleitem.Year;
                };
                _dataTableRow["StartTime"] = scheduleitem.StartTime;
                _dataTableRow["EndTime"] = scheduleitem.EndTime;
                _dataTableRow["StartDate"] = scheduleitem.StartDate;
                _dataTableRow["EndDate"] = scheduleitem.EndDate;
                _dataTableRow["CreatedDateTime"] = DateTime.Now;
                _dataTableRow["LastModifiedDateTime"] = DateTime.Now;
                _dataTableRow["CreatedUser"] = scheduleitem.CreatedUser;
                _dataTableRow["LastModifiedUser"] = scheduleitem.LastModifiedUser;
                _dataTableRow["ActiveStatus"] = scheduleitem.ActiveStatus;
                _dataTableRow["IsFixed"] = scheduleitem.IsFixed;
                _dataTableRow["LastActiveTimesGeneratedDate"] = DBNull.Value;
                _dataTable.Rows.Add(_dataTableRow);
            }

            return _dataTable;
        }

        protected override int Body(DbConnection connection)
        {
            int _result = -1;
            string StoredProcedureName = "USExceGMSAddTask";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TaskId", DbType.Int32, _excelineTask.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Name", DbType.String, _excelineTask.Title));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Description", DbType.String, _excelineTask.Description));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _excelineTask.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TemplateId", DbType.Int32, _excelineTask.TaskTemplateId));
                if (_excelineTask.AssignTo != -1)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@Assignto", DbType.Int32, _excelineTask.AssignTo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AssigntoEntityRole", DbType.String, _excelineTask.AssignEntityRole));
                if(_excelineTask.FollowUpMemberId != null)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@MemberId", DbType.Int32, _excelineTask.FollowUpMemberId));
                if (_excelineTask.BelongsToMemberId != null)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@BelongsToMemberId", DbType.Int32, _excelineTask.BelongsToMemberId));
                
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreatedUser", DbType.String, _excelineTask.CreatedUser));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@LastModifiedUser", DbType.String, _excelineTask.LastModifiedUser));

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@NumOfDays", DbType.Int32, _excelineTask.NumOfDays));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsPopup", DbType.Boolean, _excelineTask.IsPopup));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsFxied", DbType.Boolean, _excelineTask.IsFxied));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@PhoneNo", DbType.String, _excelineTask.PhoneNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Sms", DbType.String, _excelineTask.Sms));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ExtenedInfo", DbType.Xml, XMLUtils.SerializeDataContractObjectToXML(
                    new ExtendedFieldInfoDC() { ExtendedFieldsList = _excelineTask.ExtendedFieldsList })));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ActiveStatus", DbType.Boolean, _excelineTask.ActiveStatus));

                if (_excelineTask.Schedule != null)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@scheduleId", DbType.Int32, _excelineTask.Schedule.Id));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsSchedule", DbType.Boolean, true));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@Startdate", DbType.DateTime, _excelineTask.Schedule.StartDate));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@Enddate", DbType.DateTime, _excelineTask.Schedule.EndDate));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@EntityId", DbType.Int32, _excelineTask.Schedule.EntityId));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleItemList", SqlDbType.Structured, _dataTable));
                    if (_excelineTask.StartTime == DateTime.MinValue)
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Starttime", DbType.DateTime, DBNull.Value));
                    else
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Starttime", DbType.DateTime, _excelineTask.StartTime));
                    if (_excelineTask.EndTime == DateTime.MinValue)
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Endtime", DbType.DateTime, DBNull.Value));
                    else
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Endtime", DbType.DateTime, _excelineTask.EndTime));
                   
                }
                else
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsSchedule", DbType.Boolean, false));
                    if (_excelineTask.StartDate == DateTime.MinValue)
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Startdate", DbType.DateTime, DBNull.Value));
                    else
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Startdate", DbType.DateTime, _excelineTask.StartDate));
                    if (_excelineTask.EndDate == DateTime.MinValue)
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Enddate", DbType.DateTime, DBNull.Value));
                    else
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Enddate", DbType.DateTime, _excelineTask.EndDate));
                    if (_excelineTask.StartTime == DateTime.MinValue)
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Starttime", DbType.DateTime, DBNull.Value));
                    else
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Starttime", DbType.DateTime, _excelineTask.StartTime));
                    if (_excelineTask.EndTime == DateTime.MinValue)
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Endtime", DbType.DateTime, DBNull.Value));
                    else
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Endtime", DbType.DateTime, _excelineTask.EndTime));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@EntityId", DbType.Int32, null));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleItemList", SqlDbType.Structured, null));
                }

                if (_excelineTask.FoundDate == DateTime.MinValue)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@FoundDate", DbType.DateTime, DBNull.Value));
                else
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@FoundDate", DbType.DateTime, _excelineTask.FoundDate));
                if (_excelineTask.ReturnDate == DateTime.MinValue)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ReturnDate", DbType.DateTime, DBNull.Value));
                else
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@ReturnDate", DbType.DateTime, _excelineTask.ReturnDate));
                
                if (_excelineTask.DueDate == DateTime.MinValue)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@Duedate", DbType.DateTime, DBNull.Value));
                else
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@DueDate", DbType.DateTime, _excelineTask.DueDate));
                if (_excelineTask.DueTime == DateTime.MinValue)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@Duetime", DbType.DateTime, DBNull.Value));
                else
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@Duetime", DbType.DateTime, _excelineTask.DueTime));


                SqlParameter param = new SqlParameter();
                param.ParameterName = "@outId";
                param.SqlDbType = SqlDbType.Int;
                param.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(param);


                cmd.ExecuteNonQuery();

                int TaskID = Convert.ToInt32(param.Value);
                if (TaskID > 0)
                    _result = TaskID;

            }

            catch (Exception ex)
            {
                throw ex;
            }

            return _result;
        }
    }
}
