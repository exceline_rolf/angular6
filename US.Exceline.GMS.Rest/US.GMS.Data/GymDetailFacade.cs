﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin;
using US.GMS.Core.SystemObjects;
using US.GMS.Data.DataAdapters.SQLServer;
using US.GMS.Data.SystemObjects;

namespace US.GMS.Data
{
    public class GymDetailFacade
    {
        private static IDataAdapter GetDataAdapter()
        {
            return new SQLServerDataAdapter();
        }

        public static string GetGymConnection(string gymCode)
        {
            return GetDataAdapter().GetGymConnection(gymCode);
        }

        public static int GetGymCompanyId(string gymCode)
        {
            return GetDataAdapter().GetGymCompanyId(gymCode);
        }

        public static List<BranchDetailsDC> GetBranchDetails()
        {
            return GetDataAdapter().GetBranchDetails();
        }

        public static List<string> GetGymCodes()
        {
            return GetDataAdapter().GetGymCodes();
        }

        public static List<BranchDetailsDC> GetBranches(string gymCode)
        {
            return GetDataAdapter().GetBranches(gymCode);
        }

        public static List<ExceUserBranchDC> GetGymsforAccountNumber(string accountNumber, string gymCode)
        {
            return GetDataAdapter().GetGymsforAccountNumber(accountNumber, gymCode);
        }

        public static List<ExcelineRoleDc> GetUserRole(string gymCode)
        {
            return GetDataAdapter().GetUserRole(gymCode);
        }
    }
}
