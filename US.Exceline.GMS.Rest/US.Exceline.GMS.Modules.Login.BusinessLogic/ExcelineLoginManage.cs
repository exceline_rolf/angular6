﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Payment.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.Login;
using US.GMS.Core.DomainObjects.Admin;
using US.Exceline.GMS.Modules.Login.Data.DataAdapters.ManageLogin;

namespace US.Exceline.GMS.Modules.Login.BusinessLogic
{
    public class ExcelineLoginManage
    {
        public static OperationResultValue<List<ExceUserBranchDC>> GetUserSelectedBranches(string userName,string user)
        {
            OperationResultValue<List<ExceUserBranchDC>> result = new OperationResultValue<List<ExceUserBranchDC>>();
            try
            {
                result.OperationReturnValue = ManageLoginFacade.GetUserSelectedBranches(userName, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting Branches" + ex.Message, USMessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResultValue<bool> SetNewUserPassword(ExcePWSetRecDC userData)
        {
            OperationResultValue<bool> result = new OperationResultValue<bool>();
            try
            {
                result.OperationReturnValue = ManageLoginFacade.SetNewUserPassword(userData);
            }
            catch (Exception Ex)
            {
                result.CreateMessage("Error in setting new password. " + Ex.Message, USMessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResultValue<ExcePWRecDC> AnonymousUserPasswordReset(string gymCode, string potentialUserName)
        {
            OperationResultValue<ExcePWRecDC> result = new OperationResultValue<ExcePWRecDC>();
            try
            {
                result.OperationReturnValue = ManageLoginFacade.AnonymousUserPasswordReset(gymCode, potentialUserName);
            }
            catch (Exception Ex)
            {
                result.CreateMessage("Error in running anonymous user password reset. " + Ex.Message, USMessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResultValue<List<ExecUserDC>> GetLoggedUserDetails(string userName)
        {
            OperationResultValue<List<ExecUserDC>> result = new OperationResultValue<List<ExecUserDC>>();
            try
            {
                result.OperationReturnValue = ManageLoginFacade.GetLoggedUserDetails(userName);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting logged user details" + ex.Message, USMessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResultValue<bool> AddEditUserHardwareProfile(int userId, int hardwareProfileId, string user) 
        {
            OperationResultValue<bool> result = new OperationResultValue<bool>();
            try
            {
                result.OperationReturnValue = ManageLoginFacade.AddEditUserHardwareProfile(userId,hardwareProfileId, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in adding user hardware profile id" + ex.Message, USMessageTypes.ERROR);
            }
            return result;
        }


       

    }
}
