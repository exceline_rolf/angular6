﻿using System;
using System.Web;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace US.Exceline.GMS.Modules.Login.BusinessLogic
{
    public class EmailSMTPClient
    {
        public void SendPasswordResetMail(string ToEmail, string UserName, string hash)
        {
            try
            {
                MailMessage message = new MailMessage("passwordrecoveryanders@automail.exceline.no", ToEmail)
                {
                    Subject = "A reset password action was taken",
                    IsBodyHtml = true
                };
                // Email body. Using Stringbuilder as the body is mutated multiple times.
                string OnlyUserName = UserName.Substring(UserName.IndexOf('/') + 1);

                StringBuilder mailBody = new StringBuilder();
                mailBody.Append("Greetings " + OnlyUserName + ", <br/><br/>");
                mailBody.Append("We have detected that someone wishes to reset your password");
                mailBody.Append("<br/>");
                mailBody.Append("If this was not you, please contact support <br/>");
                // Input link to navigate user to reset password page
                string link = "http://localhost:3000/#/?pwrec=1&username=" + UserName + "&base=" + hash;
                mailBody.Append("<a href="+link+"> Please click here to reset your password </a>");

                message.Body = mailBody.ToString();

                // Create and authenticate the mail client
                SmtpClient client = new SmtpClient("smtp.mailgun.org", 587)
                {
                    EnableSsl = true,
                    Credentials = new NetworkCredential("passwordrecoveryanders@automail.exceline.no", "WOmo1s6CdLPSL94r9Tmn3p0JblqVJq")
                };
                client.Send(message);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }

        }
    }
}
