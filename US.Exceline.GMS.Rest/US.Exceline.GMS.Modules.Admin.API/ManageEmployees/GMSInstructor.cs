﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : JNI
// Created Timestamp : "3/29/2012 8:24:31 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System.Collections.Generic;
using US.Exceline.GMS.Modules.Admin.BusinessLogic.ManageEmployees;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.ResultNotifications;

namespace US.Exceline.GMS.Modules.Admin.API.ManageEmployees
{
    public class GMSInstructor
    {
        public static OperationResult<List<InstructorDC>> GetInstructors(int branchId, string searchText, bool isActive, int instructorId,string gymCode)
        {
            return InstructorManager.GetInstructors(branchId, searchText, isActive, instructorId, gymCode);
        }
    }
}
