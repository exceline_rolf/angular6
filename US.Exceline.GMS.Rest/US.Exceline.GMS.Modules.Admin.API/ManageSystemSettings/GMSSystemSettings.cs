﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "5/29/2012 16:15:15
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using US.GMS.Core.DomainObjects.AccessControl;
using US.GMS.Core.DomainObjects.AccessControl.ARX;
using US.GMS.Core.ResultNotifications;
using US.Exceline.GMS.Modules.Admin.BusinessLogic.ManageSystemSettings;
using US.GMS.Core.DomainObjects.Admin;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.Payments;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.Admin.API.ManageSystemSettings
{
    public class GMSSystemSettings
    {
        public static OperationResult<int> SaveBranchDetails(ExcelineBranchDC branch, string user, string gymCode)
        {
            return SystemSettingsManager.SaveBranchDetails(branch, user,  gymCode);
        }

        public static OperationResult<bool> ValidateCreditorNo(int branchId, string gymCode, string creditorNo)
        {
            return SystemSettingsManager.ValidateCreditorNo(branchId, gymCode, creditorNo);
        }
        public static OperationResult<List<ExcelineCreditorGroupDC>> GetBranchGroups(string user, string gymCode)
        {
            return SystemSettingsManager.GetBranchGroups(user,  gymCode);
        }

        public static OperationResult<List<ExcelineBranchDC>> GetBranches(string user, string gymCode, int branchId)
        {
            return SystemSettingsManager.GetBranches(user, gymCode, branchId);
        }

        public static OperationResult<int> UpdateBranchDetails(ExcelineBranchDC branch, string user, string gymCode)
        {
            return SystemSettingsManager.UpdateBranchDetails(branch, user,  gymCode);
        }

        public static OperationResult<int> AddActivitySettings(ActivitySettingDC activitySetting, string gymCode, string user)
        {
            return SystemSettingsManager.AddActivitySettings(activitySetting,  gymCode, user);
        }

        public static OperationResult<List<ArticleDC>> GetinventoryList(int branchId,string gymCode)
        {
            return SystemSettingsManager.GetInventoryList(branchId, gymCode);
        }

        #region Gym Company Settings
        public static OperationResult<string> GetGymCompanySettings(string gymCode, GymCompanySettingType gymCompanySettingType)
        {
            return SystemSettingsManager.GetGymCompanySettings(gymCode, gymCompanySettingType);
        }

        //-------------------------------------------------------------------------------------------------------
        public static OperationResult<SystemSettingBasicInfoDC> GetGymCompanyInfoSettings(string gymCode)
        {
            return SystemSettingsManager.GetGymCompanyInfoSettings(gymCode);
        }

        public static OperationResult<List<VatCodeDC>> GetGymCompanyVATSettings(string gymCode)
        {
            return SystemSettingsManager.GetGymCompanyVATSettings(gymCode);
        }

        public static OperationResult<SystemSettingEconomySettingDC> GetGymCompanyEconomySettings(string gymCode)
        {
            return SystemSettingsManager.GetGymCompanyEconomySettings(gymCode);
        }

        public static OperationResult<SystemSettingOtherSettingDC> GetGymCompanyOtherSettings(string gymCode)
        {
            return SystemSettingsManager.GetGymCompanyOtherSettings(gymCode);
        }
        public static OperationResult<List<ContractCondition>> GetGymCompanyContractConditions(string gymCode)
        {
            return SystemSettingsManager.GetGymCompanyContractConditions(gymCode);
        }

        public static OperationResult<int> AddUpdateGymCompanySettings(string settingsDetails, string user, string gymCode, GymCompanySettingType gymCompanySettingType)
        {
            return SystemSettingsManager.AddUpdateGymCompanySettings(settingsDetails, user, gymCode, gymCompanySettingType);
        }

        public static OperationResult<int> SaveGymCompanyBasicSettings(SystemSettingBasicInfoDC gymCompanyBasicSetting, string user, string gymCode)
        {
            return SystemSettingsManager.SaveGymCompanyBasicSettings(gymCompanyBasicSetting, user, gymCode);
        }

        public static OperationResult<int> SaveGymCompanyOtherSettings(SystemSettingOtherSettingDC gymCompanyOtherSetting, string user, string gymCode)
        {
            return SystemSettingsManager.SaveGymCompanyOtherSettings(gymCompanyOtherSetting, user, gymCode);
        }

        public static OperationResult<int> SaveGymCompanyContractSettings(ContractCondition gymCompanyContractSetting, string user, string gymCode)
        {
            return SystemSettingsManager.SaveGymCompanyContractSettings(gymCompanyContractSetting, user, gymCode);
        }

        public static OperationResult<int> SaveGymCompanyVATSettings(VatCodeDC vatCodeDetail, string user, string gymCode)
        {
            return SystemSettingsManager.SaveGymCompanyVATSettings(vatCodeDetail, user, gymCode);
        }

        public static OperationResult<int> SaveGymCompanyEconomySettings(SystemSettingEconomySettingDC systemEconomySetting, string user, string gymCode)
        {
            return SystemSettingsManager.SaveGymCompanyEconomySettings(systemEconomySetting, user, gymCode);
        }

        public static OperationResult<List<string>> GetClassKeyword(string gymCode)
        {
            return SystemSettingsManager.GetClassKeyword(gymCode);
        }
        #endregion

        public static OperationResult<List<AccountDC>> GetRevenueAccounts(string gymCode)
        {
            return SystemSettingsManager.GetRevenueAccounts(gymCode);
        }

        public static OperationResult<int> AddUpdateRevenueAccounts(AccountDC revenueAccountDetail, string user, string gymCode)
        {
            return SystemSettingsManager.AddUpdateRevenueAccounts(revenueAccountDetail,user,gymCode);
        }

        public static OperationResult<int> DeleteRevenueAccounts(AccountDC revenueAccountDetail, string user, string gymCode)
        {
            return SystemSettingsManager.DeleteRevenueAccounts(revenueAccountDetail, user, gymCode);
        }

        public static OperationResult<decimal> SaveStock(ArticleDC article, int branchId, string gymCode, string user)
        {
            return SystemSettingsManager.SaveStock(article, branchId, gymCode, user);
        }

        public static OperationResult<int> DeleteArticle(int articleId, int branchId, string gymCode, string user, bool isAdminUser)
        {
            return SystemSettingsManager.DeleteArticle(articleId, branchId, gymCode, user, isAdminUser);
        }

        public static OperationResult<List<ActivityTimeDC>> GetActivityTimes(int branchId, bool isUnavailableTimes, string gymCode)
        {
            return SystemSettingsManager.GetActivityTimes(branchId, isUnavailableTimes, gymCode);
        }

        public static OperationResult<bool> AddActivityTimes(List<ActivityTimeDC> activityTimes, string gymCode, bool isUnavailableTimes)
        {
            return SystemSettingsManager.AddActivityTimes(activityTimes, gymCode, isUnavailableTimes);
        }

        public static OperationResult<int> SaveInventory(InventoryItemDC inventoryItem, string gymCode,string user)
        {
            return SystemSettingsManager.SaveInventory(inventoryItem, gymCode, user);
        }

        public static OperationResult<List<InventoryItemDC>> GetInventory(int branchId, string gymCode)
        {
            return SystemSettingsManager.GetInventory(branchId, gymCode);
        }


        public static OperationResult<int> DeleteInventory(int inventoryId, string gymCode)
        {
            return SystemSettingsManager.DeleteInventory(inventoryId, gymCode);
        }

        public static OperationResult<List<ArticleDC>> GetInventoryDetail(int branchId, int inventoryId, string gymCode)
        {
            return SystemSettingsManager.GetInventoryDetail(branchId,inventoryId, gymCode);
        }


        public static OperationResult<decimal> SaveInventoryDetail(List<ArticleDC> articleList, int inventoryId, string gymCode)
        {
            return SystemSettingsManager.SaveInventoryDetail(articleList, inventoryId, gymCode);
        }

        public static OperationResult<Dictionary<decimal, decimal>> GetNetValueForArticle(int inventoryId, int articleId, int counted, int branchId, string gymCode)
        {
            return SystemSettingsManager.GetNetValueForArticle(inventoryId, articleId,counted,branchId, gymCode);
        }
        public static OperationResult<CategoryDC> GetCategoryByCode(string categoryCode, string gymCode)
        {
            return SystemSettingsManager.GetCategoryByCode(categoryCode, gymCode);
        }

        #region followUp

        public static OperationResult<bool> AddFollowUpTemplate(FollowUpTemplateDC followUpTemplate, string gymCode, int branchId)
        {
            return SystemSettingsManager.AddFollowUpTemplate(followUpTemplate, gymCode, branchId);
        }

        public static OperationResult<List<FollowUpTemplateDC>> GetFollowupTemplates(string gymCode, int branchId)
        {
            return SystemSettingsManager.GetFollowUpTemplates(gymCode, branchId);
        }

        public static OperationResult<bool> DeleteFollowUpTemplate(int followUptemplateId, string gymCode)
        {
            return SystemSettingsManager.DeleteFollowUpTemplate(followUptemplateId, gymCode);
        }

        public static OperationResult<List<FollowUpTemplateTaskDC>> GetFollowUpTaskByTemplateId(int followUpTemplateId, string gymCode)
        { 
            return SystemSettingsManager.GetFollowUpTaskByTemplateId(followUpTemplateId,  gymCode);
        }


        #endregion

        public static List<ExcACCAccessControl> GetExcAccessControlList(int branchId, string gymCode)
        {
            try
            {
                return SystemSettingsManager.GetExcAccessControlList(branchId, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ExceACCMember GetLastAccMember(string gymCode, int branchId, int accTerminalId)
        {
            try
            {
                return SystemSettingsManager.GetLastAccMember(gymCode, branchId, accTerminalId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static OperationResult<List<ExceArxFormatDetail>> GetArxSettingDetail(int branchId, string gymCode)
        {
            try
            {
                return SystemSettingsManager.GetArxSettingDetail(branchId, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static OperationResult<List<ExceArxFormatType>> GetArxFormatType(string gymCode)
        {
            try
            {
                return SystemSettingsManager.GetArxFormatType(gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static OperationResult<int> SaveArxSettingDetail(ExceArxFormatDetail arxFormatDetail, int tempCodeDeleteDate, int branchId, string user, string gymCode, string accessContolTypes)
        {
            try
            {
                return SystemSettingsManager.SaveArxSettingDetail(arxFormatDetail, tempCodeDeleteDate, branchId, user, gymCode, accessContolTypes);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<ContractTemplateDC> ValidateContractConditionWithTemplate(int contractConditionId, string gymCode)
        {
            try
            {
                return SystemSettingsManager.ValidateContractConditionWithTemplate(contractConditionId, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int ValidateContractConditionWithContracts(int contractConditionId, string gymCode)
        {
            try
            {
                return SystemSettingsManager.ValidateContractConditionWithContracts(contractConditionId, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
