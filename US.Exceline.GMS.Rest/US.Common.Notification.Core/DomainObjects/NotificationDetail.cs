﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Common.Notification.Core.DomainObjects
{
    public class NotificationDetail
    {
        public NotificationGridRow Notification{ get; set; }
        public List<ChannelGridRow> ChannelList{ get; set; }
        public List<ActionGridRow> ActionList{ get; set; }
    }
}
