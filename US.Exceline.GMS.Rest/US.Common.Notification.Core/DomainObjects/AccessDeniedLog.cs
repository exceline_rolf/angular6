﻿using System;

namespace US.Common.Notification.Core.DomainObjects
{
    public class AccessDeniedLog
    {
        public int Id { get; set; }
        public int MemberNo { get; set; }
        public int TerminalId { get; set; }
        public string TerminalName { get; set; }
        public int BranchId { get; set; }
        public string CardNo { get; set; }
        public string Message { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
