﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace US.Common.Notification.Core.DomainObjects
{
    public class USNotificationChannel
    {
        public int ChannelId { get; set; }
        public DateTime ChannelDueDate { get; set; }
        public int Occurance { get; set; }
        public string ChannelMessage { get; set; }
        public int NotificationID { get; set; }
        public DateTime ChannelNextDueDate { get; set; }
        public DateTime EffectiveDate { get; set; }
        public string ChannelAssign { get; set; }
        public bool ChannelStatus { get; set; }
        public string TemplateCategory { get; set; }
        public string TemplateName { get; set; }
        public int ChannelType { get; set; }
        public string MessageType { get; set; }
        public string OccuranceType { get; set; }
    }
}
