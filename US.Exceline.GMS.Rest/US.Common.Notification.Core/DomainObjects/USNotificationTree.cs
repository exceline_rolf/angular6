﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace US.Common.Notification.Core.DomainObjects
{
    public class USNotificationTree
    {
        public USNotification Notification{ get; set; }
        public int BranchId { get; set; }
        public int MemberId { get; set; }
        public USNotificationAction Action { get; set; }
        public List<USNotificationChannel> NotificationMethods{ get; set; }
    }
}
