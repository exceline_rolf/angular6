﻿
namespace US.Common.Notification.Core.DomainObjects
{
    public class NotificationSMS
    {
        public int NotificationID { get; set; }
        public string Message { get; set; }
        public string Sender { get; set; }
        public string Receiver { get; set; }
        public string StatusCode { get; set; }
        public string StatusMessage { get; set; }
        public string batchId { get; set; }
    }
}
