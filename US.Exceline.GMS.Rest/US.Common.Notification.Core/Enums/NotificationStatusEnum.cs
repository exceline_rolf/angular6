﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Common.Notification.Core.Enums
{
    public enum NotificationStatusEnum
    {
        New=1,
        Attended=2,
        Ignored=3,
        Closed=4,
        Fail=5,
        Success=6
    }
}
