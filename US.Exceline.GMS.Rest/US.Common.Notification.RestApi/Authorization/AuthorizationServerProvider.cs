﻿using bfDemoService;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using US.Common.USSAdmin.API;
using US.Common.USSAdmin.Data.UserSettings.DBCommands;
using US.GMS.Core.Utils;
using US_DataAccess;

namespace US.Common.Notification.RestApi.Authorization
{
    public class AuthorizationServerProvider : OAuthAuthorizationServerProvider
    {

        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
              identity.AddClaim(new Claim(ClaimTypes.Name, context.UserName));

               CheckForAdminAccess action = new CheckForAdminAccess(context.UserName);
               int role = action.Execute(EnumDatabase.Exceline, ExceConnectionManager.GetGymCode(context.UserName));

               if (role == 1 || role == 2)
               {
                   identity.AddClaim(new Claim(ClaimTypes.Role, "Admin"));
               }
            


            string decrypted = EncryptDecrypt.DecryptStringAES(context.Password);
            var result = AdminUserSettings.ValidateUser(context.UserName, decrypted);

            if (result.OperationReturnValue.Length > 1 || result.OperationReturnValue.Equals("1"))
            {
                context.Validated(identity);
            }
            else
            {
                context.SetError("invalid_grant", "Provided username and password is incorrect");
            }
        }
    }
}