﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "5/29/2012 16:15:31
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using US.Exceline.GMS.Modules.Admin.Data.DataAdapters.ManageSystemSettings;
using US.GMS.Core.DomainObjects.Admin;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.Payments;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.Utils;
using US.GMS.Core.DomainObjects.Admin.ManageAnonymizing;
using US.Exceline.GMS.Modules.Admin.Data.DataAdapters.ManageAnonymizing;

namespace US.Exceline.GMS.Modules.Admin.BusinessLogic.ManageAnonymizing
{
    public class AnonymizingManager
    {
        public static OperationResult<int> DeleteAnonymizingData(ExceAnonymizingDC anonymizing,string user, int branchId, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = AnonymizingFacade.DeleteAnonymizingData(anonymizing,user, branchId,gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Deleting Anonymizing data" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ExceAnonymizingDC>> GetAnonymizingData(string gymCode)
        {
            OperationResult<List<ExceAnonymizingDC>> result = new OperationResult<List<ExceAnonymizingDC>>();
            try
            {
                result.OperationReturnValue = AnonymizingFacade.GetAnonymizingData( gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting Anonymizing data" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

    }
}
