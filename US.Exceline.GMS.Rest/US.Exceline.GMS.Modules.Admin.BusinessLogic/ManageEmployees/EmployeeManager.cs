﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.SystemObjects;
using US.Exceline.GMS.Modules.Admin.Data.DataAdapters.ManageEmployees;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using System.IO;

namespace US.Exceline.GMS.Modules.Admin.BusinessLogic.ManageEmployees
{
   public class EmployeeManager
    {
       public static OperationResult<string> UpdateEmployees(EmployeeDC employeeDc, string imageFolderPath,string user, string gymCode)
        {
            OperationResult<string> result = new OperationResult<string>();
            try
            {
                string imagePath = byteArrayToImage(employeeDc.ProfilePicture, employeeDc.FirstName + employeeDc.Mobile, imageFolderPath);
                employeeDc.ImagePath = imagePath;
                result.OperationReturnValue = EmployeeFacade.UpdateEmployees(employeeDc, user, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Trainer Updating" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

       public static string byteArrayToImage(byte[] byteArrayIn, string memberName, string imageFolderPath)
       {
           if (byteArrayIn != null)
           {
               string imagePath = string.Empty;
               try
               {
                   if (!Directory.Exists(imageFolderPath))
                   {
                       Directory.CreateDirectory(imageFolderPath);
                   }
                   if (File.Exists(imageFolderPath + memberName + ".file"))
                   {
                       File.Delete(imageFolderPath + memberName + ".file");
                   }
                   ByteArrayToFile(imageFolderPath + memberName + ".file", byteArrayIn);
                   imagePath = imageFolderPath + memberName + ".file";
               }
               catch 
               {
                   return string.Empty;
               }
               return imagePath;
           }
           return string.Empty;
       }

       public static bool ByteArrayToFile(string _FileName, byte[] _ByteArray)
       {
           try
           {
               System.IO.FileStream _FileStream = new System.IO.FileStream(_FileName, System.IO.FileMode.Create, System.IO.FileAccess.Write);
               _FileStream.Write(_ByteArray, 0, _ByteArray.Length);
               _FileStream.Close();
               return true;
           }
           catch
           {
           }
           return false;
       }

       public static OperationResult<string> UpdateMemeberActiveness(int trainerId, string rolId, int branchId, string user, bool isActive, string gymCode)
       {
           OperationResult<string> result = new OperationResult<string>();
           try
           {
               result.OperationReturnValue = EmployeeFacade.UpdateMemeberActiveness(trainerId, rolId, branchId, user, isActive,  gymCode);
           }
           catch (Exception ex)
           {
               result.CreateMessage("Error in Trainer Deleting " + ex.Message, MessageTypes.ERROR);
           }
           return result;
       }


       public static OperationResult<List<ExcelineTaskDC>> GetEntityTasks(int entityId, int branchId, string entityRoleType, string gymCode)
       {
           OperationResult<List<ExcelineTaskDC>> result = new OperationResult<List<ExcelineTaskDC>>();
           try
           {
               result.OperationReturnValue = EmployeeFacade.GetEntityTasks(entityId,branchId, entityRoleType,  gymCode);
           }
           catch(Exception ex)
           {
               result.CreateMessage("Error in Getting Entity Taks " + ex.Message, MessageTypes.ERROR);
           }
           return result;
       }

       public static OperationResult<bool> SaveEntityTimeEntry(TimeEntryDC timeEntry, string gymCode)
       {
           OperationResult<bool> result = new OperationResult<bool>();
           try
           {
               result.OperationReturnValue = EmployeeFacade.SaveEntityTimeEntry(timeEntry,  gymCode);
           }
           catch(Exception ex)
           {
               result.CreateMessage("Error in Saving Entity Time " +ex.Message, MessageTypes.ERROR);
           }
           return result;
       }

       public static OperationResult<List<TimeEntryDC>> GetEntityTimeEntries(DateTime startDate, DateTime endDate, int entityId, string entityRoleType, string gymCode)
       {
           OperationResult<List<TimeEntryDC>> result = new OperationResult<List<TimeEntryDC>>();
           try
           {
               result.OperationReturnValue = EmployeeFacade.GetEntityTimeEntries(startDate, endDate, entityId,entityRoleType,  gymCode);
           }
           catch (Exception ex)
           {
               result.CreateMessage("Error in Getting entity Time entries " + ex.Message, MessageTypes.ERROR);
           }
           return result;
       
       }

       public static OperationResult<bool> DeleteEntityTimeEntry(int entityTimeEntryId, int taskId, decimal timeSpent, string gymCode)
       {
           OperationResult<bool> result = new OperationResult<bool>();
           try
           {
               result.OperationReturnValue = EmployeeFacade.DeleteEntityTimeEntry(entityTimeEntryId,taskId, timeSpent,  gymCode) ;
           }
           catch (Exception ex)
           {
               result.CreateMessage("Error in Deleting Employee Time Entry " + ex.Message, MessageTypes.ERROR);
           }
           return result;
       }

       public static OperationResult<List<CategoryDC>> GetTaskStatusCategories(int branchId, string gymCode)
       {
           OperationResult<List<CategoryDC>> result = new OperationResult<List<CategoryDC>>();
            try
            {
                result.OperationReturnValue = EmployeeFacade.GetTaskStatusCategories(branchId,gymCode);
            }
            catch(Exception ex)
            {
                result.CreateMessage("Error in Getting Tast Status Categories " + ex.Message, MessageTypes.ERROR);
            }
           return result;       
       }

       public static OperationResult<bool> SaveEmployeesRoles(EmployeeDC employeeDc, string gymCode)
       {
           OperationResult<bool> result = new OperationResult<bool>();
           try
           {
               result.OperationReturnValue = EmployeeFacade.SaveEmployeesRoles(employeeDc,  gymCode);
           }
           catch (Exception ex)
           {
               result.CreateMessage("Error in Saving Roles " + ex.Message, MessageTypes.ERROR);
           }
           return result;
       }

       public static OperationResult<List<RoleActivityDC>> GetActivitiesForRoles(int memberId, string gymCode)
       {
           OperationResult<List<RoleActivityDC>> result = new OperationResult<List<RoleActivityDC>>();
           try
           {
               result.OperationReturnValue = EmployeeFacade.GetActivitiesForRoles(memberId,  gymCode);
           }
           catch (Exception ex)
           {
               result.CreateMessage("Error in Getting ActivitiesForRoles " + ex.Message, MessageTypes.ERROR);
           }
           return result;
       }

       public static OperationResult<bool> DeleteGymEmployee(int memberId, int assignEmpId, string user, string gymCode)
       {
           OperationResult<bool> result = new OperationResult<bool>();
           try
           {
               result.OperationReturnValue = EmployeeFacade.DeleteGymEmployee(memberId,assignEmpId, user, gymCode);
           }
           catch (Exception ex)
           {
               result.CreateMessage("Error in Delete Gym Employee" + ex.Message, MessageTypes.ERROR);
           }
           return result;
       }

       public static OperationResult<List<EmployeeClass>> GetEmployeeClasses(int empId, string gymCode)
       {
           OperationResult<List<EmployeeClass>> result = new OperationResult<List<EmployeeClass>>();
           try
           {
               result.OperationReturnValue = EmployeeFacade.GetEmployeeClasses(empId, gymCode);
           }
           catch (Exception ex)
           {
               result.CreateMessage("Error in Get Employee Classes.." + ex.Message, MessageTypes.ERROR);
           }
           return result;
       }

       public static OperationResult<int> UpdateApproveStatus(bool IsApproved, int EntityActiveTimeID, string user)
       {
           OperationResult<int> result = new OperationResult<int>();
           try
           {
               result.OperationReturnValue = EmployeeFacade.UpdateApproveStatus(IsApproved, EntityActiveTimeID, user);
           }
           catch (Exception ex)
           {
               result.CreateMessage("Error in Update Approve Status.." + ex.Message, MessageTypes.ERROR);
           }
           return result;
       }

       public static OperationResult<List<ExcelineRoleDc>> GetGymEmployeeRolesById(int employeeId, int branchId, string user)
       {
           OperationResult<List<ExcelineRoleDc>> result = new OperationResult<List<ExcelineRoleDc>>();
           try
           {
               result.OperationReturnValue = EmployeeFacade.GetGymEmployeeRolesById(employeeId, branchId, user);
           }
           catch (Exception ex)
           {
               result.CreateMessage("Error in Getting empoyee role.." + ex.Message, MessageTypes.ERROR);
           }
           return result;
       }
    }
}
