﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS.Modules.Admin.BusinessLogic
// Coding Standard   : US Coding Standards
// Author            : NHE
// Created Timestamp : "4/21/2012 5:37:44 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.Exceline.GMS.Modules.Admin.Data.DataAdapters.ManageEmployees;

namespace US.Exceline.GMS.Modules.Admin.BusinessLogic.ManageEmployees
{
    public class TimeEntryManager
    {
        public static OperationResult<List<TimeEntryDC>> GetTimeEntriesForAdminView(DateTime fromTime, DateTime toTime, string gymCode)
        {
            OperationResult<List<TimeEntryDC>> result = new OperationResult<List<TimeEntryDC>>();
            try
            {
                result.OperationReturnValue = TimeEntryFacade.GetTimeEntriesForAdminView(fromTime, toTime,  gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting Time Entries for Admin View " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

    }
}
