﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : JNI
// Created Timestamp : "3/31/2012 6:31:54 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.Exceline.GMS.Modules.Admin.Data.DataAdapters.ManageContract;

namespace US.Exceline.GMS.Modules.Admin.BusinessLogic.ManageContract
{
    public class ContractManager
    {
        public static OperationResult<int> SaveContract(PackageDC resource, string user, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ContractFacade.SaveContract(resource,  user,  gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Contract Saving" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> UpdateContractPriority(Dictionary<int, int> packages, string user, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ContractFacade.UpdateContractPriority(packages, user, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Contract Saving" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        public static OperationResult<bool> UpdateContract(PackageDC resource, string user)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ContractFacade.UpdateContract(resource, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Contract Saving" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
        
        public static OperationResult<bool> DeActivateContract(int resourceId, int branchId, string user)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ContractFacade.DeActivateContract(resourceId, branchId, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Contract Saving" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<PackageDC>> GetContracts(int branchId, string searchText, int searchType, string searchCriteria, string gymCode)
        {
            OperationResult<List<PackageDC>> result = new OperationResult<List<PackageDC>>();
            try
            {
                result.OperationReturnValue = ContractFacade.GetContracts( branchId,  searchText,  searchType, searchCriteria,  gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Contract Getting" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<string> GetNextContractId(int branchId, string gymCode)
        {
            OperationResult<string> result = new OperationResult<string>();
            try
            {
                result.OperationReturnValue = ContractFacade.GetNextContractId(branchId,  gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Contract Saving" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> DeleteContract(int contractId, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ContractFacade.DeleteContract(contractId,gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Deleting Contract" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<PriceItemTypeDC>> GetPriceItemTypes(int branchId, string gymCode)
        {
            OperationResult<List<PriceItemTypeDC>> result = new OperationResult<List<PriceItemTypeDC>>();
            try
            {
                result.OperationReturnValue = ContractFacade.GetPriceItemTypes(branchId,  gymCode);
            }
            catch(Exception ex)
            {
                result.CreateMessage("Error in Getting Price Item Types " + ex.Message, MessageTypes.ERROR);
            }
             
            return result;
        }

        #region Contract Items
        public static OperationResult<List<ContractItemDC>> GetContractItems(int branchId,  string gymCode)
        {
            OperationResult<List<ContractItemDC>> result = new OperationResult<List<ContractItemDC>>();
            try
            {
                result.OperationReturnValue = ContractFacade.GetContractItems(branchId,  gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Getting contract Items " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> AddContractItem(ContractItemDC contractItem, string user, int branchId, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ContractFacade.AddContractItem(contractItem,user,branchId,  gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in adding contract Items " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }


        public static OperationResult<bool> RemoveContractItem(int contractItemID, string user, int branchId, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ContractFacade.RemoveContractItem(contractItemID, user, branchId,  gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in removing contract Item " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        } 
        #endregion
    }
}
