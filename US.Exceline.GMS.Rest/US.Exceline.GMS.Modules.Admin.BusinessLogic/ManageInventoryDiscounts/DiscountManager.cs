﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// -------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.ManageShop;
using US.Exceline.GMS.Modules.Admin.Data.DataAdapters.ManageInventoryDiscounts;
using US.GMS.Core.DomainObjects.Admin;

namespace US.Exceline.GMS.Modules.Admin.BusinessLogic.ManageInventoryDiscounts
{
    public class DiscountManager
    {

        public static OperationResult<bool> AddDiscount(ShopDiscountDC newDiscount, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = DiscountFacade.AddDiscount(newDiscount, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Adding discount" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ShopDiscountDC>> GetDiscountsList(int branchId, bool isActive, string gymCode)
        {
            OperationResult<List<ShopDiscountDC>> result = new OperationResult<List<ShopDiscountDC>>();
            try
            {
                result.OperationReturnValue = DiscountFacade.GetDiscountsList(branchId, isActive, gymCode);
            }
            catch (Exception ex)
            {

                result.CreateMessage("Error in Getting Discounts List" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> UpdateDiscount(ShopDiscountDC updateDiscount, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = DiscountFacade.UpdateDiscount(updateDiscount, gymCode);
            }
            catch (Exception ex)
            {

                result.CreateMessage("Error in updating Discount" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }



        public static OperationResult<List<InventoryItemDC>> GetInventoryList(int branchId)
        {
            OperationResult<List<InventoryItemDC>> result = new OperationResult<List<InventoryItemDC>>();
            try
            {
                result.OperationReturnValue = DiscountFacade.GetInventoryList(branchId);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting inventory items" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ShopSalesItemDC>> GetDailySales(DateTime salesDate, int branchId, string gymCode)
        {
            throw new NotImplementedException();
        }

        public static OperationResult<bool> DeleteDiscount(int discountId, string type, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = DiscountFacade.DeleteDiscount(discountId,type,gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Deletering discount" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }



    }



}
