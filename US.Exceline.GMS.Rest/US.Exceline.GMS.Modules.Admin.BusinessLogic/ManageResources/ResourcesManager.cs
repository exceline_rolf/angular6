﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : JNI
// Created Timestamp : "3/29/2012 8:25:11 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Globalization;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US.Exceline.GMS.Modules.Admin.Data.DataAdapters.ManageResources;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.ResultNotifications;
using System.IO;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.Admin.BusinessLogic.ManageResources
{
    public class ResourcesManager
    {

    public static OperationResult<int> UpdateContractVisits(int selectedContractId, int punchedContractId, int bookingId, string gymCode, string user)
    {
        OperationResult<int> result = new OperationResult<int>();
        try
        {
            result.OperationReturnValue = ResourcesFacade.UpdateContractVisits(selectedContractId, punchedContractId, bookingId, gymCode, user);
        }
        catch (Exception ex)
        {
            result.CreateMessage("Error in getting punchcard contracts " + ex.Message, MessageTypes.ERROR);
        }
        return result;
    }

        public static OperationResult<List<ResourceBookingElemnt>> GetResourceBooking(int scheduleId, string _startDateTime, string _endDateTime, string gymCode, string user)
    {
        OperationResult<List<ResourceBookingElemnt>> result = new OperationResult<List<ResourceBookingElemnt>>();
        try
        {
            result.OperationReturnValue = ResourcesFacade.GetResourceBooking(scheduleId, _startDateTime, _endDateTime, gymCode, user);
        }
        catch (Exception ex)
        {
            result.CreateMessage("Error in getting punchcard contracts " + ex.Message, MessageTypes.ERROR);
        }
        return result;
    }

    public static OperationResult<List<ContractInfo>> GetPunchcardContractsOnMember(int memberId, string activity, string gymCode, string user)
        {
            OperationResult<List<ContractInfo>> result = new OperationResult<List<ContractInfo>>();
            try
            {
                result.OperationReturnValue = ResourcesFacade.GetPunchcardContractsOnMember(memberId, activity, gymCode, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting punchcard contracts " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> IsArticlePunchcard(int articleId, string gymCode, string user)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ResourcesFacade.IsArticlePunchcard(articleId, gymCode, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting article punchardvalue " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> SaveResources(ResourceDC resource, string user, string imageFolderPath, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                string imagepath = byteArrayToImage(resource.ProfilePicture, resource.Name, imageFolderPath);
                resource.ImagePath = imagepath;
                result.OperationReturnValue = ResourcesFacade.SaveResources(resource, user, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Resources Saving" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static string byteArrayToImage(byte[] byteArrayIn, string memberName, string imageFolderPath)
        {
            if (byteArrayIn != null)
            {
                string imagePath = string.Empty;
                try
                {
                    if (!Directory.Exists(imageFolderPath))
                    {
                        Directory.CreateDirectory(imageFolderPath);
                    }
                    if (File.Exists(imageFolderPath + memberName + ".file"))
                    {
                        File.Delete(imageFolderPath + memberName + ".file");
                    }
                    ByteArrayToFile(imageFolderPath + memberName + ".file", byteArrayIn);
                    imagePath = imageFolderPath + memberName + ".file";
                }
                catch
                {
                    return string.Empty;
                }
                return imagePath;
            }
            return string.Empty;
        }

        public static OperationResult<int> ChangeResourceOnBooking(int scheduleItemId, int resourceId, string comment, string gymCode, string user, int branchId)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ResourcesFacade.ChangeResourceOnBooking(scheduleItemId, resourceId, comment, gymCode, user, branchId);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in changing resource on booking" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static bool ByteArrayToFile(string _FileName, byte[] _ByteArray)
        {
            try
            {
                System.IO.FileStream _FileStream = new System.IO.FileStream(_FileName, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream.Write(_ByteArray, 0, _ByteArray.Length);
                _FileStream.Close();
                return true;
            }
            catch
            {
            }
            return false;
        }

        public static OperationResult<bool> UpdateResource(ResourceDC resource, string user, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ResourcesFacade.UpdateResource(resource, user, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Resources Updating " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> DeleteResource(int branchId, string user, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ResourcesFacade.DeleteResource(branchId, user, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Resources Deleting" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> DeActivateResource(int resourceId, int branchId, string user, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ResourcesFacade.DeActivateResource(resourceId, branchId, user, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Resources Deactivating" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> SwitchResource(int resourceTd1, int resourceId2, DateTime startDate, DateTime endDate,string comment, int branchId, string user, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ResourcesFacade.SwitchResource(resourceTd1, resourceId2, startDate, endDate,comment, branchId, user, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Switch Resource" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> SwitchResourceUndo(int resId, DateTime startDate, DateTime endDate, string user, int branchId, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ResourcesFacade.SwitchResourceUndo(resId, startDate, endDate,user,branchId,gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Undo Switch Resource" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> ValidateScheduleWithResourceId(int resourceId, DateTime startDate, DateTime endDate, int branchId, string user, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ResourcesFacade.ValidateScheduleWithResourceId(resourceId, startDate, endDate, branchId, user, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Validate Schedule With Resource Id" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> ValidateScheduleWithSwitchResource(int resourceId1, int resourceId2, DateTime startDate, DateTime endDate, int branchId, string user, string gymCode)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ResourcesFacade.ValidateScheduleWithSwitchResource(resourceId1, resourceId2, startDate, endDate, branchId, user, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Validate Schedule With Resource Id" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ResourceDC>> GetResources(int branchId, string searchText, int categoryId, int activityId, int equipmentid, bool isActive, string gymCode)
        {
            OperationResult<List<ResourceDC>> result = new OperationResult<List<ResourceDC>>();
            try
            {
                result.OperationReturnValue = ResourcesFacade.GetResources(branchId, searchText, categoryId, activityId, equipmentid, isActive, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Resources Getting" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ScheduleItemDC>> GetBookingsOnDeletedScheduleItem(int resourceId, string gymCode)
        {
            var result = new OperationResult<List<ScheduleItemDC>>();
            try
            {
                result.OperationReturnValue = ResourcesFacade.GetBookingsOnDeletedScheduleItem(resourceId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in  scheuldeItems by id" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<ResourceDC>  GetResourceDetailsById(int resourceId, string gymCode)
        {
            var result = new OperationResult<ResourceDC>();
            try
            {
                result.OperationReturnValue = ResourcesFacade.GetResourceDetailsById(resourceId,gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in  Getting Resources detail by id" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ResourceDC>> GetResourceCalender(int branchId, string gymCode, int hit)
        {
            var result = new OperationResult<List<ResourceDC>>();
            try
            {
                result.OperationReturnValue = ResourcesFacade.GetResourceCalender(branchId, gymCode,hit);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Resources Getting Calender" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<ResourceActiveTimeDC>> GetResourceActiveTimes(int branchId, DateTime startTime, DateTime endTime, int entNO, string gymCode)
        {
            OperationResult<List<ResourceActiveTimeDC>> result = new OperationResult<List<ResourceActiveTimeDC>>();
            try
            {
                result.OperationReturnValue = ResourcesFacade.GetResourceActiveTimes(branchId, startTime, endTime, entNO, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Resources Getting" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> SaveScheduleItem(ScheduleItemDC scheduleItem,int resId, int branchId, string gymCode)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                //if (scheduleItem.StartDate.Date < DateTime.Now.AddMonths(1).Date)
                //{
                //    DateTime startDate = scheduleItem.StartDate;
                //    DateTime endDate = DateTime.Now.Date.AddMonths(1);
                //    if (startDate.Date <= DateTime.Now.Date)
                //    {
                //        startDate = DateTime.Now.Date.AddDays(1);
                //    }
                //    if (endDate > scheduleItem.EndDate.Date)
                //    {
                //        endDate = scheduleItem.EndDate.Date;
                //    }
                //    scheduleItem.ActiveTimes = GetWeekActiveTimes(scheduleItem, startDate, endDate);
                //}
                result.OperationReturnValue = ResourcesFacade.SaveScheduleItem(scheduleItem,resId,branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.ErrorOccured = true;
                result.CreateMessage("Error in Saving schedule item" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        //private static List<EntityActiveTimeDC> GetWeekActiveTimes(ScheduleItemDC scheduleItem, DateTime startDate, DateTime endDate)
        //{
        //    switch (scheduleItem.Day)
        //    {
        //        case "Sunday":
        //            return GenerateActiveTimes(scheduleItem, DayOfWeek.Sunday, startDate, endDate);

        //        case "Monday":
        //            return GenerateActiveTimes(scheduleItem, DayOfWeek.Monday, startDate, endDate);

        //        case "Tuesday":
        //            return GenerateActiveTimes(scheduleItem, DayOfWeek.Tuesday, startDate, endDate);

        //        case "Wednesday":
        //            return GenerateActiveTimes(scheduleItem, DayOfWeek.Wednesday, startDate, endDate);

        //        case "Thursday":
        //            return GenerateActiveTimes(scheduleItem, DayOfWeek.Thursday, startDate, endDate);

        //        case "Friday":
        //            return GenerateActiveTimes(scheduleItem, DayOfWeek.Friday, startDate, endDate);

        //        case "Saturday":
        //            return GenerateActiveTimes(scheduleItem, DayOfWeek.Saturday, startDate, endDate);

        //        case "søndag":
        //            return GenerateActiveTimes(scheduleItem, DayOfWeek.Sunday, startDate, endDate);

        //        case "mandag":
        //            return GenerateActiveTimes(scheduleItem, DayOfWeek.Monday, startDate, endDate);

        //        case "tirsdag":
        //            return GenerateActiveTimes(scheduleItem, DayOfWeek.Tuesday, startDate, endDate);

        //        case "onsdag":
        //            return GenerateActiveTimes(scheduleItem, DayOfWeek.Wednesday, startDate, endDate);

        //        case "torsdag":
        //            return GenerateActiveTimes(scheduleItem, DayOfWeek.Thursday, startDate, endDate);

        //        case "fredag":
        //            return GenerateActiveTimes(scheduleItem, DayOfWeek.Friday, startDate, endDate);

        //        case "lørdag":
        //            return GenerateActiveTimes(scheduleItem, DayOfWeek.Saturday, startDate, endDate);
        //    }
        //    return new List<EntityActiveTimeDC>();
        //}
        //private static List<EntityActiveTimeDC> GenerateActiveTimes(ScheduleItemDC scheduleItem, DayOfWeek dayofWeek, DateTime startDate, DateTime endDate)
        //{
        //    List<EntityActiveTimeDC> activeTimes = new List<EntityActiveTimeDC>();
        //    TimeSpan duration = endDate - startDate;
        //    DateTime activestartDate = startDate;
        //    if (scheduleItem.WeekType == 2)
        //    {
        //        for (int i = 0; i < duration.Days + 1; i++)
        //        {
        //            if (activestartDate.DayOfWeek == dayofWeek)
        //            {
        //                EntityActiveTimeDC activeTime = new EntityActiveTimeDC();
        //                activeTime.SheduleItemId = scheduleItem.Id;
        //                activeTime.StartDateTime = Convert.ToDateTime(activestartDate.ToShortDateString() + " " + scheduleItem.StartTime.ToShortTimeString());
        //                activeTime.EndDateTime = Convert.ToDateTime(activestartDate.ToShortDateString() + " " + scheduleItem.EndTime.ToShortTimeString());
        //                activeTimes.Add(activeTime);
        //                if (scheduleItem.WeekType != 2)
        //                {
        //                    activestartDate = activestartDate.AddDays(7);
        //                    i += 7;
        //                }
        //            }
        //            activestartDate = activestartDate.AddDays(1);
        //        }
        //    }
        //    else
        //    {
        //        int durationDays = duration.Days;
        //        DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
        //        Calendar cal = dfi.Calendar;
        //        int weekNumber = cal.GetWeekOfYear(activestartDate, dfi.CalendarWeekRule, dfi.FirstDayOfWeek);
        //        int oddEvenStatus = weekNumber % 2;
        //        if (oddEvenStatus != scheduleItem.WeekType)
        //        {
        //            activestartDate = activestartDate.AddDays(7);
        //            durationDays = duration.Days - 7;
        //        }
        //        for (int i = 0; i < durationDays + 1; i++)
        //        {
        //            if (activestartDate.DayOfWeek == dayofWeek)
        //            {
        //                EntityActiveTimeDC activeTime = new EntityActiveTimeDC();
        //                activeTime.SheduleItemId = scheduleItem.Id;
        //                activeTime.StartDateTime = Convert.ToDateTime(activestartDate.ToShortDateString() + " " + scheduleItem.StartTime.ToShortTimeString());
        //                activeTime.EndDateTime = Convert.ToDateTime(activestartDate.ToShortDateString() + " " + scheduleItem.EndTime.ToShortTimeString());
        //                activeTimes.Add(activeTime);
        //                if (scheduleItem.WeekType != 2)
        //                {
        //                    activestartDate = activestartDate.AddDays(7);
        //                    i += 7;
        //                }
        //            }
        //            activestartDate = activestartDate.AddDays(1);
        //        }

        //    }
        //    return activeTimes;
        //}

        public static OperationResult<List<AvailableResourcesDC>> GetAvailableResources(int branchId,int resId, string gymCode)
        {
            OperationResult<List<AvailableResourcesDC>> result = new OperationResult<List<AvailableResourcesDC>>();
            try
            {
                result.OperationReturnValue = ResourcesFacade.GetAvailableResources(branchId, resId,gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Available Resources Getting" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<AvailableResourcesDC>> GetResourceAvailableTimeById(int branchId, int resId, string gymCode)
        {
            OperationResult<List<AvailableResourcesDC>> result = new OperationResult<List<AvailableResourcesDC>>();
            try
            {
                result.OperationReturnValue = ResourcesFacade.GetResourceAvailableTimeById(branchId, resId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in get resource available time" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<ScheduleDC> GetResourceScheduleItems(int resId, string roleTpye, string gymCode)
         {
             OperationResult<ScheduleDC> result = new OperationResult<ScheduleDC>();
             try
             {
                 result.OperationReturnValue = ResourcesFacade.GetResourceScheduleItems(resId, roleTpye, gymCode);
             }
             catch (Exception ex)
             {
                 result.CreateMessage("Error in ScheduleItem Getting" + ex.Message, MessageTypes.ERROR);
             }
             return result;
         }

        public static OperationResult<List<ScheduleDC>> GetResourceCalenderScheduleItems(List<int> resIdList, string roleTpye, string gymCode)
        {
            OperationResult<List<ScheduleDC>> result = new OperationResult<List<ScheduleDC>>();
            try
            {
                result.OperationReturnValue = ResourcesFacade.GetResourceCalenderScheduleItems(resIdList, roleTpye, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in ScheduleItem Getting calender" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<List<string>> ValidateResourcesWithSchedule(string roleType, List<AvailableResourcesDC> resourceList, int branchId, string gymCode)
        {
            OperationResult<List<string>> result = new OperationResult<List<string>>();
            try
            {
                result.OperationReturnValue = ResourcesFacade.ValidateResourcesWithSchedule(roleType, resourceList, branchId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Validate Resources With Schedule" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> AddOrUpdateAvailableResources(List<AvailableResourcesDC> resourceList, int branchId, string user)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ResourcesFacade.AddOrUpdateAvailableResources(resourceList, branchId, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Add Or Update Available Resources" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

         public static OperationResult<List<EntityActiveTimeDC>> GetMemberBookingDetails(int branchId,int scheduleItemId, string gymCode)
         {
             OperationResult<List<EntityActiveTimeDC>> result = new OperationResult<List<EntityActiveTimeDC>>();
             try
             {
                 result.OperationReturnValue = ResourcesFacade.GetMemberBookingDetails(branchId, scheduleItemId, gymCode);
             }
             catch (Exception ex)
             {
                 result.CreateMessage("Error in Getting Member booking" + ex.Message, MessageTypes.ERROR);
             }
             return result;
         }

         public static OperationResult<bool> ValidateScheduleWithPaidBooking(int scheduleItemId,int resourceId, string gymCode)
         {
             OperationResult<bool> result = new OperationResult<bool>();
             try
             {
                 result.OperationReturnValue = ResourcesFacade.ValidateScheduleWithPaidBooking(scheduleItemId, resourceId,gymCode);
             }
             catch (Exception ex)
             {
                 result.CreateMessage("Error in validating paid booking" + ex.Message, MessageTypes.ERROR);
             }
             return result;
         }

         public static OperationResult<int> DeleteResourcesScheduleItem(int scheduleItemId, string gymCode)
         {
             OperationResult<int> result = new OperationResult<int>();
             try
             {
                 result.OperationReturnValue = ResourcesFacade.DeleteResourcesScheduleItem(scheduleItemId, gymCode);
             }
             catch (Exception ex)
             {
                 result.CreateMessage("Error in deleting ScheduleItem" + ex.Message, MessageTypes.ERROR);
             }
             return result;
         }


         public static OperationResult<int> CheckDuplicateResource(int sourceId, int destinationId, DateTime startDate, DateTime endDate, string gymCode)
         {
             OperationResult<int> result = new OperationResult<int>();
             try
             {
                 result.OperationReturnValue = ResourcesFacade.CheckDuplicateResource(sourceId, destinationId,startDate,endDate, gymCode);
             }
             catch (Exception ex)
             {
                 result.CreateMessage("Error in Check Duplicate Resource ScheduleItem" + ex.Message, MessageTypes.ERROR);
             }
             return result;
         }

         public static OperationResult<string> CheckActiveTimeOverlap(EntityActiveTimeDC activeTimeItem, string gymCode)
         {
             OperationResult<string> result = new OperationResult<string>();
             try
             {
                 result.OperationReturnValue = ResourcesFacade.CheckActiveTimeOverlap(activeTimeItem, gymCode);
             }
             catch (Exception ex)
             {
                 result.CreateMessage("Error in Check Active Time Overlap" + ex.Message, MessageTypes.ERROR);
             }
             return result;
         }

        public static OperationResult<int> SaveResourceActiveTimePunchcard(EntityActiveTimeDC activeTimeItem, int contractId, string gymCode, int branchId, string user)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ResourcesFacade.SaveResourceActiveTimePunchcard(activeTimeItem, contractId, gymCode, branchId, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Saving activeTime" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

         public static OperationResult<int> SaveResourceActiveTimeRepeatablePunchcard(List<EntityActiveTimeDC> activeTimeItemList, int contractId, string gymCode, string user)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ResourcesFacade.SaveResourceActiveTimeRepeatablePunchcard(activeTimeItemList, contractId, gymCode, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Saving activeTime punchcard repeatable" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> SaveResourceActiveTimeRepeatable(List<EntityActiveTimeDC> activeTimeItemList, string gymCode, string user)
        {
            OperationResult<int> result = new OperationResult<int>();
            try
            {
                result.OperationReturnValue = ResourcesFacade.SaveResourceActiveTimeRepeatable(activeTimeItemList, gymCode, user);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Saving activeTime repeatable" + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<int> SaveResourceActiveTime(EntityActiveTimeDC activeTimeItem, string gymCode, int branchId, string user)
         {
             OperationResult<int> result = new OperationResult<int>();
             try
             {
                 result.OperationReturnValue = ResourcesFacade.SaveResourceActiveTime(activeTimeItem, gymCode, branchId, user);
             }
             catch (Exception ex)
             {
                 result.CreateMessage("Error in Saving activeTime" + ex.Message, MessageTypes.ERROR);
             }
             return result;
         }

         public static OperationResult<int> DeleteResourceActiveTime(int activeTimeId,string articleName,DateTime visitDateTime, string roleType, bool isArrived, bool isPaid,List<int> memberList, string gymCode)
         {
             OperationResult<int> result = new OperationResult<int>();
             try
             {
                 result.OperationReturnValue = ResourcesFacade.DeleteResourceActiveTime(activeTimeId, articleName, visitDateTime, roleType, isArrived, isPaid, memberList,gymCode);
             }
             catch (Exception ex)
             {
                 result.CreateMessage("Error in updating activeTime" + ex.Message, MessageTypes.ERROR);
             }
             return result;
         }


         public static OperationResult<List<ArticleDC>> GetArticleForResouceBooking(int activityId,int branchId, string gymCode)
         {
             OperationResult<List<ArticleDC>> result = new OperationResult<List<ArticleDC>>();
             try
             {
                 result.OperationReturnValue = ResourcesFacade.GetArticleForResouceBooking(activityId,branchId, gymCode);
             }
             catch (Exception ex)
             {
                 result.CreateMessage("Error in geting article" + ex.Message, MessageTypes.ERROR);
             }
             return result;
         }

         public static OperationResult<List<ArticleDC>> GetArticleForResouce(int activityId, int branchId, string gymCode)
         {
             OperationResult<List<ArticleDC>> result = new OperationResult<List<ArticleDC>>();
             try
             {
                 result.OperationReturnValue = ResourcesFacade.GetArticleForResouce(activityId, branchId, gymCode);
             }
             catch (Exception ex)
             {
                 result.CreateMessage("Error in geting article for resource" + ex.Message, MessageTypes.ERROR);
             }
             return result;
         }

         public static OperationResult<Dictionary<string, List<int>>> CheckUnusedCancelBooking(int activityId,int branchId, string accountNo, List<int> memberIdList, DateTime bookingDate, string gymCode)
         {
             OperationResult<Dictionary<string, List<int>>> result = new OperationResult<Dictionary<string, List<int>>>();
             try
             {
                 result.OperationReturnValue = ResourcesFacade.CheckUnusedCancelBooking(activityId, branchId,accountNo, memberIdList, bookingDate, gymCode);
             }
             catch (Exception ex)
             {
                 result.CreateMessage("Error in checking boking" + ex.Message, MessageTypes.ERROR);
             }
             return result;
         }

         public static OperationResult<int> UnavailableResourceBooking(int scheduleItemId, int activeTimeId, DateTime startDate, DateTime endDate, string gymCode, string user)
         {
             OperationResult<int> result = new OperationResult<int>();
             try
             {
                 result.OperationReturnValue = ResourcesFacade.UnavailableResourceBooking(scheduleItemId, activeTimeId, startDate, endDate, gymCode, user);
             }
             catch (Exception ex)
             {
                 result.CreateMessage("Error in unavailable Resource Booking" + ex.Message, MessageTypes.ERROR);
             }
             return result;
         }

         public static OperationResult<Dictionary<string, List<int>>> ValidatePunchcardContractMember(int activityId,int branchId, string accountNo, List<int> memberIdList, DateTime startDate, string gymCode)
         {
             var result = new OperationResult<Dictionary<string, List<int>>>();
             try
             {
                 result.OperationReturnValue = ResourcesFacade.ValidatePunchcardContractMember(activityId, branchId,accountNo, memberIdList, startDate, gymCode);
             }
             catch (Exception ex)
             {
                 result.CreateMessage("Error in Punchcard Contract Member validation" + ex.Message, MessageTypes.ERROR);
             }
             return result;
         }

         public static OperationResult<int>  SavePaymentBookingMember(int activetimeId,int articleId, int memberId, bool paid, int aritemNo, decimal amount,string paymentType, string gymCode)
         {
             OperationResult<int> result = new OperationResult<int>();
             try
             {
                 result.OperationReturnValue = ResourcesFacade.SavePaymentBookingMember(activetimeId,articleId, memberId, paid, aritemNo,amount,paymentType, gymCode);
             }
             catch (Exception ex)
             {
                 result.CreateMessage("Error in Saving payment" + ex.Message, MessageTypes.ERROR);
             }
             return result;
         }

         public static OperationResult<List<int>> GetEmpolyeeForResources(List<int> resIdList, string gymCode)
         {
             OperationResult<List<int>> result = new OperationResult<List<int>>();
             try
             {
                 result.OperationReturnValue = ResourcesFacade.GetEmpolyeeForResources(resIdList, gymCode);
             }
             catch (Exception ex)
             {
                 result.CreateMessage("Error getting Employee for resouse List" + ex.Message, MessageTypes.ERROR);
             }
             return result;
         }

         public static OperationResult<List<int>> ValidateResWithSwitchDataRange(List<int> resourceId, DateTime startDate, DateTime endDate, string gymCode)
        {
            OperationResult<List<int>> result = new OperationResult<List<int>>();
            try
            {
                result.OperationReturnValue = ResourcesFacade.ValidateResWithSwitchDataRange(resourceId,startDate,endDate, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Validate the Resource With Switch DataRange" + ex.Message, MessageTypes.ERROR);
            }
            return result; 
        }

        public static string GetResourceBookingViewMode(string user, string gymCode)
        {
            try
            {
                return ResourcesFacade.GetResourceBookingViewMode(user, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static OperationResult<bool> ValidateArticleByActivityId(int activityId, int articleId, string gymCode)
        {
            var result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ResourcesFacade.ValidateArticleByActivityId(activityId, articleId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in Validate the Article "+ ex.Message, MessageTypes.ERROR);
            }
            return result; 
        }
    }
}
