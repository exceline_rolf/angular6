﻿using SessionManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Common.Logging.API;
using US.GMS.Core.Utils;

namespace SessionService
{
    public class SessionService : ISessionService
    {
        public int AddSessionKeyValue(string sessionKey, string sessionValue, string user, int branchId = -1, int salePointId = -1 )
        {
            try
            {
                return Session.AddSessionValue(sessionKey, sessionValue, ExceConnectionManager.GetGymCode(user), branchId,salePointId);
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile(ex.Message, ex, user);
            }
            return -1;
        }

        public string GetSessionKeyValue(string sessionKey, string user)
        {
            try
            {
                return Session.GetSession(sessionKey, ExceConnectionManager.GetGymCode(user));
            }
            catch (Exception ex)
            {
                USLogError.WriteToFile(ex.Message, ex, user);
            }
            return string.Empty;
        }
    }
}
