﻿using System;
using System.Collections.Generic;
using US.Exceline.GMS.Modules.Economy.Core.DomainObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums;

namespace US.Exceline.GMS.Modules.Economy.Data
{
    public class EconomyFacade
    {
        private static ISQLServerEconomyDataAdapter GetDataAdapter()
        {
            return new SQLServerEconomyDataAdapter();
        }

        public static List<USPApportionmentTransaction> GetInitialApportionmentData(string KID, string creditorNo, decimal amount, int ARNo, int caseNo, int paymentID, string paymentType, string gymCode)
        {
            return GetDataAdapter().GetInitialApportionmentData(KID, creditorNo, amount, ARNo, caseNo, paymentID, paymentType, gymCode);
        }

        public static ReverseConditions ReverseApportionments(int paymentId, int arItem, string actionStatus, string user, string gymCode)
        {
            return GetDataAdapter().ReverseApportionments(paymentId, arItem, actionStatus, user, gymCode);
        }

        public static int AddNotApportionmentTransactions(int ARNo, string KID, int errorPayment, string creditoNo, decimal mainAmount, int itemTypeID, List<USPApportionmentTransaction> transactionList, string user, string gymCode)
        {
            return GetDataAdapter().AddNotApportionmentTransactions(ARNo, KID, errorPayment, creditoNo, mainAmount, itemTypeID, transactionList, "", DateTime.Now, user, gymCode);
        }

        public static bool SendReminders(string GymCode, int branchID)
        {
            return GetDataAdapter().SendReminders(GymCode, branchID);
        }

        public static List<EconomyFileLogData> GetEconomyFileLogData(DateTime fromDate, DateTime toDate, string source,
                                                                     string filterBy, string gymCode)
        {
            return GetDataAdapter().GetEconomyFileLogData(fromDate, toDate, source, filterBy, gymCode);
        }

        public static List<EconomyFileLogData> GetEconomyFileLogDetailData(EconomyLogFileHelper economyLogFileHelper)
        {
            return GetDataAdapter().GetEconomyFileLogDetail(economyLogFileHelper);
        }

        public static int GetCcxEnabledStatus(int branchId, string gymCode)
        {
            return GetDataAdapter().GetCcxEnabledStatus(branchId, gymCode);
        }

        public static List<KeyData> GetKeyFromDB(String gymCode)
        {
            return GetDataAdapter().GetKeyFromDB(gymCode);
        }

        public static int AddPreviousSignatureForNextTransaction(int SalePointID, int branchID, string signature, String gymCode)
        {
            return GetDataAdapter().AddPreviousSignatureForNextTransaction( SalePointID,  branchID,  signature, gymCode);
        }

        public static int FinalizeTransaction(int SalePointID, int branchID, string signature, String transDate, String transTime, int nr, decimal transAmntIn, decimal transAmntEx, String gymCode)
        { 
           return GetDataAdapter().FinalizeTransaction(SalePointID, branchID, signature, transDate, transTime, nr, transAmntIn, transAmntEx, gymCode);
        }
        public static String GetLastTransaction(int salePointID, int branchID, String gymCode)
        {
            return GetDataAdapter().GetLastTransaction( salePointID,  branchID, gymCode);
        }

        public static List<DataForLedger> ViewSkatteEtatenLedger(DateTime FromDate, DateTime ToDate, int branchID, String gymCode)
        {
            return GetDataAdapter().ViewSkatteEtatenLedger( FromDate,  ToDate,  branchID, gymCode);
        }

        public static List<KeyData> GetSalePointsByBranch(int branchId, String gymCode)
        {
            return GetDataAdapter().GetSalePointsByBranch(branchId, gymCode);
        }

        public static InvoiceInfo GetInformationForInvoicePrint(String invoiceRef, int branchId, String gymCode)
        {
            return GetDataAdapter().GetInformationForInvoicePrint(invoiceRef,branchId, gymCode);
        }

        public static int InsertInvoicePrintRecord(int branchId, String invoiceRef, String xmlData, String hashValue, String gymCode)
        {
            return GetDataAdapter().InsertInvoicePrintRecord(branchId, invoiceRef, xmlData, hashValue, gymCode);
        }

        public static InvoiceXMLData GetInvoicePrintRecord(String invoiceRef, int branchId, String gymCode)
        {
            return GetDataAdapter().GetInvoicePrintRecord(invoiceRef, branchId, gymCode);
        }

        public static double GetVatRateFromInvoiceNo(int invoiceNo, String gymCode)
        {
            return GetDataAdapter().GetVatRateFromInvoiceNo(invoiceNo, gymCode);
        }

        public static int GetGymCompanyId(String gymCode)
        {
            return GetDataAdapter().GetGymCompanyId(gymCode);
        }

        public static void InsertInvoicePrintDiscrepancyInstance(DateTime timeStamp, String invoiceNo, String originalXml, String newXml , String gymCode)
        {
            GetDataAdapter().InsertInvoicePrintDiscrepancyInstance(timeStamp,  invoiceNo,  originalXml,  newXml, gymCode);
        }


        public static List<ExcelineSponsorMembersForPrint> ExcelineGetSponsoredMembersInvoicePrint(String sponsorARItemNo, String gymCode)
        {
            return GetDataAdapter().ExcelineGetSponsoredMembersInvoicePrint(sponsorARItemNo, gymCode);
        }


        public static ExcelineSponsorInfoForPrint ExcelineSponsorInfoForPrint(String sponsorARItemNo, String gymCode)
        {
            return GetDataAdapter().ExcelineGetSponsorInfoForInvoicePrint(sponsorARItemNo, gymCode);
        }
    }
}
