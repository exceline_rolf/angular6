﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class ReverseApportionmentsAction : USDBActionBase<int>
    {
        private int _paymentID = -1;
        private string _user = string.Empty;
        private int _isReversePayment = -1;

        public ReverseApportionmentsAction(int paymentID, int IsReversePayment, string user)
        {
            _paymentID = paymentID;
            _isReversePayment = IsReversePayment;
            _user = user;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            return 0;
        }

        public int RunOnTransaction(DbTransaction transaction)
        {
            int result = -1;
            try
            {
                DbCommand command = GenericDBFactory.Factory.CreateCommand();
                command.CommandText = "USP_ReverseApportionments";
                command.CommandType = CommandType.StoredProcedure;
                command.Transaction = transaction;
                command.Connection = transaction.Connection;

                command.Parameters.Add(DataAcessUtils.CreateParam("@PaymentId", System.Data.DbType.Int32, _paymentID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@User", System.Data.DbType.String, _user));
                DbParameter outPara = new SqlParameter();
                outPara.ParameterName = "@ReverseAppResult";
                outPara.Direction = System.Data.ParameterDirection.Output;
                outPara.DbType = System.Data.DbType.Int32;
                command.Parameters.Add(outPara);
                command.ExecuteNonQuery();

                int appResult = Convert.ToInt32(outPara.Value);
                if (appResult == 1 || appResult == 4)
                {
                    ReverseApportionmentPaymentAction reverseAction = new ReverseApportionmentPaymentAction(_paymentID, _isReversePayment);
                    result = reverseAction.RunOnTransaction(transaction);
                }
                else
                {
                    return appResult;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }
    }
}
