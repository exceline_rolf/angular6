﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands.OCR
{
    public class ReadCreditorByAccountNoAction : USDBActionBase<Creditor>
    {
        private string _AccountNo = string.Empty;
        private string _checkType = string.Empty;

        public ReadCreditorByAccountNoAction(string AccountNo, string checkType)
        {
            _AccountNo = AccountNo.Trim();
            _checkType = checkType.Trim();
        }

        protected override Creditor Body(System.Data.Common.DbConnection connection)
        {
            Creditor creditor = null;
            DbDataReader reader;
            string sp_name = "USP_GetCreditorByAccountNo";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, sp_name);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@accno", DbType.String, _AccountNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@checkType", DbType.String, _checkType));


                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    creditor = new Creditor();
                    creditor.EntID = Convert.ToString(reader["EntNo"]); //reader.GetInt32(0).ToString();
                    creditor.Name = Convert.ToString(reader["Name"]);//reader.GetString(1);                    
                    creditor.PersonNo = Convert.ToString(reader["PersonNo"]);//reader.GetString(3);
                    creditor.AccountNo = Convert.ToString(reader["AccountNo"]);//reader.GetString(4);
                    creditor.Notify = Convert.ToString(reader["NotifyBank"]);//reader[5].ToString();
                    creditor.InkassoId = Convert.ToString(reader["CreditorInkassoID"]);
                }
                else
                {
                    return null;
                }


            }
            catch (Exception)
            {
                throw;
            }

            return creditor;
        }
    }
}
