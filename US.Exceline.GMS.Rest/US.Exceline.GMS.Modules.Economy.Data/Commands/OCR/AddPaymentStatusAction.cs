﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands.OCR
{
    public class AddPaymentStatusAction : USDBActionBase<bool>
    {
        private List<ImportStatus> _paymentStatus;

        public AddPaymentStatusAction(List<ImportStatus> payments)
        {
            _paymentStatus = payments;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSAddPaymentStatus";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                DataTable paymentTable = GetStatusTable(_paymentStatus);
                command.Parameters.Add(DataAcessUtils.CreateParam("@PaymentStatus", SqlDbType.Structured, paymentTable));
                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private DataTable GetStatusTable(List<ImportStatus> payments)
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn("ID", typeof(int)));
            dataTable.Columns.Add(new DataColumn("RegDate", typeof(DateTime)));
            dataTable.Columns.Add(new DataColumn("RecordType", typeof(string)));
            dataTable.Columns.Add(new DataColumn("PID", typeof(string)));
            dataTable.Columns.Add(new DataColumn("BNumber", typeof(string)));
            dataTable.Columns.Add(new DataColumn("CID", typeof(string)));
            dataTable.Columns.Add(new DataColumn("TNumber", typeof(string)));
            dataTable.Columns.Add(new DataColumn("Amount", typeof(decimal)));
            dataTable.Columns.Add(new DataColumn("KID", typeof(string)));
            dataTable.Columns.Add(new DataColumn("RemidDate", typeof(string)));
            dataTable.Columns.Add(new DataColumn("RefernceID", typeof(int)));
            dataTable.Columns.Add(new DataColumn("BatchID", typeof(string)));
            dataTable.Columns.Add(new DataColumn("DataSource", typeof(string)));
            dataTable.Columns.Add(new DataColumn("FileID", typeof(int)));
            

            foreach (ImportStatus importStatus in payments)
            {
                DataRow dataTableRow = dataTable.NewRow();
                dataTableRow["ID"] = -1;
                dataTableRow["RegDate"] = DateTime.Now;
                dataTableRow["RecordType"] = importStatus.RecordType;
                dataTableRow["PID"] = importStatus.Pid;
                dataTableRow["BNumber"] = importStatus.BNumber;
                dataTableRow["CID"] = importStatus.Cid;
                dataTableRow["TNumber"] = importStatus.TNumber;
                dataTableRow["Amount"] = importStatus.Amount;
                dataTableRow["KID"] = importStatus.KID;
                dataTableRow["RemidDate"] = importStatus.RemitDate;
                dataTableRow["RefernceID"] = importStatus.Referanceid;
                dataTableRow["BatchID"] = importStatus.BatchId;
                dataTableRow["DataSource"] = importStatus.DataSource;
                dataTableRow["FileID"] = importStatus.FileId;
                dataTable.Rows.Add(dataTableRow);
            }
            return dataTable;
        }
    }
}
