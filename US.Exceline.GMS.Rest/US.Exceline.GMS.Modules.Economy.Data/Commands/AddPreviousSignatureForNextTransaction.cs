﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class AddPreviousSignatureForNextTransactionAction : USDBActionBase<int>
    {
        private int _SalePointID = -1;
        private int _branchID = -1;
        private string _signature = string.Empty;

        public AddPreviousSignatureForNextTransactionAction(int SalePointID, int branchID, string signature)
        {
            _SalePointID = SalePointID;
            _branchID = branchID;
            _signature = signature;
            
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            string spName = "AddPreviousSignatureForNextTransaction";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@SalePointID", System.Data.DbType.Int16, _SalePointID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@branchID", System.Data.DbType.Int16, _branchID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@signature", System.Data.DbType.String, _signature));
                command.ExecuteNonQuery();

            }
            catch (Exception) { }
            return 1;
        }
    }
}
