﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class FindDestinationAction
    {
        private IUSPTransaction _transactionInfo;
        public FindDestinationAction(IUSPTransaction trabsactionInfo)
        {
            _transactionInfo = trabsactionInfo;
        }
        public PaymentProcessStepResult RunOnTranaction(DbTransaction transaction, TransactionProfile profile)
        {
            PaymentProcessStepResult result = new PaymentProcessStepResult();
            result.ResultStatus = true;
            DbDataReader dataReader = null;
            string user = "System";
            if (_transactionInfo.User != null && _transactionInfo.User != string.Empty)
                user = _transactionInfo.User;

            result.StepName = "Find Invoice";
            try
            {
                DbConnection connection = transaction.Connection;
                DbCommand command = connection.CreateCommand();
                command.Transaction = transaction;
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = profile.FindDestinationSp;
                command.Parameters.Add(DataAcessUtils.CreateParam("@TranactionDate", DbType.DateTime, _transactionInfo.TransactionDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@CaseNo", DbType.Int32, _transactionInfo.CaseNo));
                command.Parameters.Add(DataAcessUtils.CreateParam("@SubCaseNo", DbType.Int32, _transactionInfo.SubCaseNo));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ARNo", DbType.Int32, _transactionInfo.ARNo));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ARItemNo", DbType.Int32, _transactionInfo.ARItemNo));
                command.Parameters.Add(DataAcessUtils.CreateParam("@TransType", DbType.String, _transactionInfo.TransType));
                command.Parameters.Add(DataAcessUtils.CreateParam("@TransText", DbType.String, _transactionInfo.TransText));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Delayed", DbType.Int32, _transactionInfo.Delayed));
                command.Parameters.Add(DataAcessUtils.CreateParam("@VoucherDate", DbType.DateTime, _transactionInfo.VoucherDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@DueDate", DbType.DateTime, _transactionInfo.DueDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ReceivedDate", DbType.DateTime, _transactionInfo.ReceivedDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@RegDate", DbType.DateTime, _transactionInfo.RegDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Amount", DbType.Decimal, _transactionInfo.Amount));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Source", DbType.String, _transactionInfo.Source));
                command.Parameters.Add(DataAcessUtils.CreateParam("@FileName", DbType.String, _transactionInfo.FileName));
                command.Parameters.Add(DataAcessUtils.CreateParam("@DebtorAccountNo", DbType.String, _transactionInfo.DebtorAccountNo));
                command.Parameters.Add(DataAcessUtils.CreateParam("@KID", DbType.String, _transactionInfo.KID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@CID", DbType.String, _transactionInfo.CID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@InvoiceNo", DbType.String, _transactionInfo.InvoiceNo));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Reference1", DbType.String, _transactionInfo.Reference1));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ApportionStatus", DbType.Int32, _transactionInfo.ApportionStatus));
                command.Parameters.Add(DataAcessUtils.CreateParam("@VoucherDetailId", DbType.Int32, _transactionInfo.VoucherDetailId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@PID", DbType.String, _transactionInfo.PID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@CreditorAccountNo", DbType.String, _transactionInfo.CreditorAccountNo));
                command.Parameters.Add(DataAcessUtils.CreateParam("@NotifiedDate", DbType.DateTime, _transactionInfo.NotifiedDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Reference2", DbType.String, _transactionInfo.Reference2));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ContractKid", DbType.String, _transactionInfo.ContractKid));
                //command.Parameters.Add(DataAcessUtils.CreateParam("@CancelDate", DbType.DateTime, _transactionInfo.CancelDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ExternalTransactionNo", DbType.String, _transactionInfo.ExternalTransactionNo));
                command.Parameters.Add(DataAcessUtils.CreateParam("@VATLiability", DbType.Int32, _transactionInfo.VATLiability));
                command.Parameters.Add(DataAcessUtils.CreateParam("@IsPrinted", DbType.Int32, _transactionInfo.IsPrinted));
                command.Parameters.Add(DataAcessUtils.CreateParam("@PrintFilePath", DbType.String, _transactionInfo.PrintFilePath));
                command.Parameters.Add(DataAcessUtils.CreateParam("@TranactionProfileID", DbType.Int32, _transactionInfo.TransactionProfile.Id));
                command.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, user));
                command.Parameters.Add(DataAcessUtils.CreateParam("@PaymentId", DbType.Int32, _transactionInfo.PaymentId));
                dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    if (!string.IsNullOrEmpty(dataReader["TranactionDate"].ToString()))
                    {
                        _transactionInfo.TransactionDate = Convert.ToDateTime(dataReader["TranactionDate"].ToString());
                    }
                    if (!string.IsNullOrEmpty(dataReader["CaseNo"].ToString()))
                    {
                        _transactionInfo.CaseNo = Convert.ToInt32(dataReader["CaseNo"].ToString());
                    }
                    if (!string.IsNullOrEmpty(dataReader["SubCaseNo"].ToString()))
                    {
                        _transactionInfo.SubCaseNo = Convert.ToInt32(dataReader["SubCaseNo"].ToString());
                    }
                    if (!string.IsNullOrEmpty(dataReader["ARNo"].ToString()))
                    {
                        _transactionInfo.ARNo = Convert.ToInt32(dataReader["ARNo"].ToString());
                    }
                    if (!string.IsNullOrEmpty(dataReader["ARItemNo"].ToString()))
                    {
                        _transactionInfo.ARItemNo = Convert.ToInt32(dataReader["ARItemNo"].ToString());
                    }
                    if (!string.IsNullOrEmpty(dataReader["CID"].ToString()))
                    {
                        _transactionInfo.CID = Convert.ToString(dataReader["CID"].ToString());
                    }
                    if (!string.IsNullOrEmpty(dataReader["PID"].ToString()))
                    {
                        _transactionInfo.PID = Convert.ToString(dataReader["PID"].ToString());
                    }
                    if (!string.IsNullOrEmpty(dataReader["Delayed"].ToString()))
                    {
                        _transactionInfo.Delayed = Convert.ToInt32(dataReader["Delayed"].ToString());
                    }
                    if (!string.IsNullOrEmpty(dataReader["VoucherDate"].ToString()))
                    {
                        _transactionInfo.VoucherDate = Convert.ToDateTime(dataReader["VoucherDate"].ToString());
                    }
                    if (!string.IsNullOrEmpty(dataReader["DueDate"].ToString()))
                    {
                        _transactionInfo.DueDate = Convert.ToDateTime(dataReader["DueDate"].ToString());
                    }
                    if (!string.IsNullOrEmpty(dataReader["ReceivedDate"].ToString()))
                    {
                        _transactionInfo.ReceivedDate = Convert.ToDateTime(dataReader["ReceivedDate"].ToString());
                    }
                    if (!string.IsNullOrEmpty(dataReader["RegDate"].ToString()))
                    {
                        _transactionInfo.RegDate = Convert.ToDateTime(dataReader["RegDate"].ToString());
                    }
                    if (!string.IsNullOrEmpty(dataReader["Amount"].ToString()))
                    {
                        _transactionInfo.Amount = Convert.ToDecimal(dataReader["Amount"].ToString());
                    }
                    if (!string.IsNullOrEmpty(dataReader["Source"].ToString()))
                    {
                        _transactionInfo.Source = Convert.ToString(dataReader["Source"].ToString());
                    }
                    if (!string.IsNullOrEmpty(dataReader["FileName"].ToString()))
                    {
                        _transactionInfo.FileName = Convert.ToString(dataReader["FileName"].ToString());
                    }
                    if (!string.IsNullOrEmpty(dataReader["DebtorAccountNo"].ToString()))
                    {
                        _transactionInfo.DebtorAccountNo = Convert.ToString(dataReader["DebtorAccountNo"].ToString());
                    }
                    if (!string.IsNullOrEmpty(dataReader["KID"].ToString()))
                    {
                        _transactionInfo.KID = Convert.ToString(dataReader["KID"].ToString());
                    }
                    if (!string.IsNullOrEmpty(dataReader["InvoiceNo"].ToString()))
                    {
                        _transactionInfo.InvoiceNo = Convert.ToString(dataReader["InvoiceNo"].ToString());
                    }
                    if (!string.IsNullOrEmpty(dataReader["Reference1"].ToString()))
                    {
                        _transactionInfo.Reference1 = Convert.ToString(dataReader["Reference1"].ToString());
                    }
                    if (!string.IsNullOrEmpty(dataReader["ApportionStatus"].ToString()))
                    {
                        _transactionInfo.ApportionStatus = Convert.ToInt32(dataReader["ApportionStatus"].ToString());
                    }
                    if (!string.IsNullOrEmpty(dataReader["VoucherDetailId"].ToString()))
                    {
                        _transactionInfo.VoucherDetailId = Convert.ToInt32(dataReader["VoucherDetailId"].ToString());
                    }
                    if (!string.IsNullOrEmpty(dataReader["CreditorAccountNo"].ToString()))
                    {
                        _transactionInfo.CreditorAccountNo = Convert.ToString(dataReader["CreditorAccountNo"].ToString());
                    }
                    if (!string.IsNullOrEmpty(dataReader["NotifiedDate"].ToString()))
                    {
                        _transactionInfo.NotifiedDate = Convert.ToDateTime(dataReader["NotifiedDate"].ToString());
                    }
                    if (!string.IsNullOrEmpty(dataReader["Reference2"].ToString()))
                    {
                        _transactionInfo.Reference2 = Convert.ToString(dataReader["Reference2"].ToString());
                    }
                    if (!string.IsNullOrEmpty(dataReader["ContractKid"].ToString()))
                    {
                        _transactionInfo.ContractKid = Convert.ToString(dataReader["ContractKid"].ToString());
                    }
                    if (!string.IsNullOrEmpty(dataReader["CancelDate"].ToString()))
                    {
                        _transactionInfo.CancelDate = Convert.ToDateTime(dataReader["CancelDate"].ToString());
                    }
                    if (!string.IsNullOrEmpty(dataReader["ExternalTransactionNo"].ToString()))
                    {
                        _transactionInfo.ExternalTransactionNo = Convert.ToString(dataReader["ExternalTransactionNo"].ToString());
                    }
                    if (!string.IsNullOrEmpty(dataReader["VATLiability"].ToString()))
                    {
                        _transactionInfo.VATLiability = Convert.ToInt32(dataReader["VATLiability"].ToString());
                    }
                    if (!string.IsNullOrEmpty(dataReader["IsPrinted"].ToString()))
                    {
                        _transactionInfo.IsPrinted = Convert.ToInt32(dataReader["IsPrinted"].ToString());
                    }
                    if (!string.IsNullOrEmpty(dataReader["PrintFilePath"].ToString()))
                    {
                        _transactionInfo.PrintFilePath = Convert.ToString(dataReader["PrintFilePath"].ToString());
                    }
                    if (!string.IsNullOrEmpty(dataReader["PaymentId"].ToString()))
                    {
                        _transactionInfo.PaymentId = Convert.ToInt32(dataReader["PaymentId"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                result.ResultStatus = false;
                result.MessageList.Add("Finding Invoice Failed Error - " + ex.Message);
            }
            finally
            {
                if (dataReader != null && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }
            return result;
        }
    }
}
