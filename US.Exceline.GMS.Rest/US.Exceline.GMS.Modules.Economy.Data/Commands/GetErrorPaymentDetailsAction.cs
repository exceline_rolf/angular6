﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.Exceline.GMS.Modules.Economy.Core.DomainObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class GetErrorPaymentDetailsAction : USDBActionBase<List<USPErrorPayment>>
    {

        private readonly string _errorPaymentType = string.Empty;
        private readonly DateTime _fromDate = DateTime.MinValue;
        private readonly DateTime _toDate = DateTime.MinValue;
        private readonly int _branchId = -1;
        private readonly string _batchId;
        public GetErrorPaymentDetailsAction(string errorPaymentType, DateTime fromDate, DateTime toDate, int branchId,string batchId=null)
        {
            _errorPaymentType = errorPaymentType;
            _fromDate = fromDate;
            _toDate = toDate;
            _branchId = branchId;
            _batchId = batchId;
        }
        protected override List<USPErrorPayment> Body(DbConnection dbConnection)
        {
            var errorPaymentList = new List<USPErrorPayment>();
            DbDataReader dataReader = null;
            try
            {
                const string storedProcedureName = "dbo.USP_GetPayments"; //"dbo.USP_GetErrorPaymentsByFileName";
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Type", DbType.String, _errorPaymentType));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@FromDate", DbType.Date, _fromDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndDate", DbType.Date, _toDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BatchID", SqlDbType.VarChar, _batchId));

                
                dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    var errorPayment = new USPErrorPayment();
                    errorPayment.ID = Convert.ToInt32(dataReader["PaymentId"]);
                    errorPayment.ArNo = Convert.ToInt32(dataReader["ARNo"]);
                    errorPayment.ExceedPaymentAritemNo = Convert.ToInt32(dataReader["ARItemNo"]);
                    errorPayment.ErrorMessage = Convert.ToString(dataReader["PaymentId"]) + "/" + Convert.ToString(dataReader["ARItemNo"]) + "/" + Convert.ToString(dataReader["ImportStatus"]);
                    errorPayment.AccountNo = Convert.ToString(dataReader["CreditorAccountNo"]);
                    errorPayment.Amount = Convert.ToDecimal(dataReader["Amount"]);
                    errorPayment.CustId = Convert.ToString(dataReader["Custid"]);
                    errorPayment.InkassoId = Convert.ToString(dataReader["CreditorInkassoID"]);
                    errorPayment.KID = Convert.ToString(dataReader["KID"]);
                    errorPayment.ErrorPaymentType = (Convert.ToString(dataReader["ImportStatus"]));
                    if (!string.IsNullOrEmpty(Convert.ToString(dataReader["RegDate"])))
                    {
                        errorPayment.RegDate = Convert.ToDateTime(dataReader["RegDate"]);
                    }

                    errorPayment.ErrorPaymentOperationName = "Match";
                    if (errorPayment.ErrorPaymentType == "OK")
                    {
                        errorPayment.IsEnabledViewButton = false;
                    }
                    else
                    {
                        errorPayment.IsEnabledViewButton = true;
                    }

                    if (errorPayment.ErrorPaymentType == "NotApportioned" || errorPayment.ErrorPaymentType == "Exceeded")
                    {
                        errorPayment.IsEnabledIgnoreButton = false;
                    }
                    else
                    {
                        errorPayment.IsEnabledIgnoreButton = true;
                    }

                    if (errorPayment.ErrorPaymentType == "Exceeded")
                    {
                        errorPayment.IsEnabledPaymentButton = false;
                    }
                    else
                    {
                        errorPayment.IsEnabledPaymentButton = true;
                    }

                    if (errorPayment.ErrorPaymentType == "Exceeded")
                    {
                        //errorPayment.IsEnabledExMatchButton = true;
                    }
                    else
                    {
                        errorPayment.IsEnabledExMatchButton = false;
                    }

                    errorPayment.Ref = Convert.ToString(dataReader["Ref"]);
                    if (dataReader["VoucherDate"] != DBNull.Value)
                        errorPayment.VoucherDate = Convert.ToDateTime(dataReader["VoucherDate"]);

                    errorPayment.SubCaseNo = Convert.ToInt32(dataReader["SubCaseNo"]);
                    errorPayment.CaseNo = Convert.ToInt32(dataReader["CaseNo"]);
                    errorPayment.VoucherNo = Convert.ToInt32(dataReader["VoucherNo"]);
                    errorPayment.ItemType = Convert.ToInt32(dataReader["ItemTypeId"]);
                    if (errorPayment.IsIgnored)
                    {
                        errorPayment.IgnoredButtonName = "Undo";
                        errorPayment.IsOpButtonEnabled = false;
                    }
                    else if (!errorPayment.IsIgnored)
                    {
                        errorPayment.IgnoredButtonName = "Ignore";
                        errorPayment.IsOpButtonEnabled = true;
                    }

                    if (DBNull.Value != dataReader["Filename"])
                        errorPayment.FileName = dataReader["Filename"].ToString();

                    errorPayment.ExceedPaymentApportionId = Convert.ToInt32(dataReader["ApportionId"]);

                    errorPayment.Status = Convert.ToString(dataReader["Status"]);
                    errorPayment.RecordType = Convert.ToString(dataReader["RecordType"]);
                    if (!string.IsNullOrEmpty(errorPayment.Status))
                    {
                        errorPayment.EnableButtons = false;
                    }
                    errorPaymentList.Add(errorPayment);
                }

            }
            catch (Exception)
            {
                if (dataReader != null)
                {
                    dataReader.Close();
                    dataReader.Dispose();
                }
                throw;
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();
                    dataReader.Dispose();
                }
            }
            return errorPaymentList;

        }
    }
}
