﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class FinalizeTransactionAction : USDBActionBase<int>
    {
        private int _SalePointID = -1;
        private int _branchID = -1;
        private String _signature = String.Empty;
        private String _transDate = string.Empty;
        private String _transTime = string.Empty;
        private int _nr = -1;
        private decimal _transAmntIn = -1;
        private decimal _transAmntEx = -1;


        public FinalizeTransactionAction(int SalePointID, int branchID, String signature, String transDate, String transTime, int nr, decimal transAmntIn, decimal transAmntEx)
        {
            _SalePointID = SalePointID;
            _branchID = branchID;
            _signature = signature;
            _transDate = transDate;
            _transTime = transTime;
            _nr = nr;
            _transAmntIn = transAmntIn;
            _transAmntEx = transAmntEx;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            string spName = "FinalizeTransaction";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@salePointID", System.Data.DbType.Int16, _SalePointID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@branchId", System.Data.DbType.Int16, _branchID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@signature", System.Data.DbType.String, _signature));
                command.Parameters.Add(DataAcessUtils.CreateParam("@transDate", System.Data.DbType.String, _transDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@transTime", System.Data.DbType.String, _transTime));
                command.Parameters.Add(DataAcessUtils.CreateParam("@nr", System.Data.DbType.Int32, _nr));
                command.Parameters.Add(DataAcessUtils.CreateParam("@transAmntIn", System.Data.DbType.Double, _transAmntIn));
                command.Parameters.Add(DataAcessUtils.CreateParam("@transAmntEx", System.Data.DbType.Double, _transAmntEx));
                command.ExecuteNonQuery();

            }
            catch (Exception) { }
            return 1;
        }
    }
}
