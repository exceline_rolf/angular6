﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class GetInitialApportionmentDataAction : USDBActionBase<List<USPApportionmentTransaction>>
    {
        private string _kid = string.Empty;
        private string _creditorNo = string.Empty;
        private decimal _amount = 0;
        private int _arNo = -1;
        private int _caseNo = -1;
        private int _paymentID = -1;
        private string _paymentType = string.Empty;
        private readonly string _gymCode;

        public GetInitialApportionmentDataAction(string KID, string creditorNo, decimal amount, int ARno, int caseNo, int paymentID, string paymentType, string gymCode)
        {
            _kid = KID;
            _creditorNo = creditorNo;
            _amount = amount;
            _arNo = ARno;
            _caseNo = caseNo;
            _paymentID = paymentID;
            _paymentType = paymentType;
            _gymCode = gymCode;
        }

        protected override List<USPApportionmentTransaction> Body(DbConnection connection)
        {
            List<USPApportionmentTransaction> transList = new List<USPApportionmentTransaction>();
            try
            {
                int lastRowID = 1;
                if (_caseNo > 0)
                {
                    GetInitialApportionmentTransAction action = new GetInitialApportionmentTransAction(_creditorNo, _amount, _caseNo, _arNo, lastRowID, _paymentID, _paymentType);
                    List<USPApportionmentTransaction> tempList = action.Execute(EnumDatabase.Exceline, _gymCode);
                    // decimal tempTotal = (from trn in tempList select trn.CreditorSum).Sum() + (from trn in tempList select trn.BureauSum).Sum();
                    //_amount = _amount - tempTotal;
                    //_amount = tempTotal - _amount;
                    transList.AddRange(tempList);

                    /*
                    if (_amount > 0)
                    {
                        USPApportionmentTransaction trans = new USPApportionmentTransaction();
                        if (tempList.Count > 0)
                            trans.ArNo = tempList[0].ArNo;

                        trans.ArItemNo = -1;
                        trans.PaymentID = -1;
                        trans.Amount = _amount;
                        trans.ItemTypeId = 15;
                        trans.CreditorSum = 0;
                        trans.BureauSum = 0;
                        trans.DebitorSum = _amount;
                        trans.RegDate = DateTime.Now;
                        trans.Balance = 0;
                        trans.Text = "Exceeded Payment";
                        trans.TotalBalance = 0;
                        transList.Add(trans);
                    }*/

                }
               /* else
                {
                    if (_paymentType == "Exceeded")
                    {
                        GetInitialApportionmentTransAction action = new GetInitialApportionmentTransAction(_creditorNo, _amount, 0, _arNo, lastRowID, _paymentID, _paymentType);
                        List<USPApportionmentTransaction> tempList = action.Execute(EnumDatabase.Exceline, _gymCode);
                        decimal tempTotal = (from trn in tempList select trn.CreditorSum).Sum() + (from trn in tempList select trn.BureauSum).Sum();
                        _amount = _amount - tempTotal;
                        transList.AddRange(tempList);
                    }
                    else
                    {
                        GetApportionmentCaseNoAction getappCaseNo = new GetApportionmentCaseNoAction(_kid, _creditorNo, _arNo);
                        List<int> caseNumbers = getappCaseNo.Execute(EnumDatabase.Exceline, _gymCode);

                        foreach (int caseNo in caseNumbers)
                        {
                            if (transList.Count > 0)
                                lastRowID = transList.Last().RowNo + 1;

                            GetInitialApportionmentTransAction action = new GetInitialApportionmentTransAction(_creditorNo, _amount, caseNo, _arNo, lastRowID, _paymentID, _paymentType);
                            List<USPApportionmentTransaction> tempList = action.Execute(EnumDatabase.Exceline, _gymCode);
                            decimal tempTotal = (from trn in tempList select trn.CreditorSum).Sum() + (from trn in tempList select trn.BureauSum).Sum();
                            _amount = _amount - tempTotal;
                            transList.AddRange(tempList);
                        }

                        if (_amount > 0)
                        {
                            USPApportionmentTransaction trans = new USPApportionmentTransaction();
                            trans.ArNo = _arNo;
                            trans.ArItemNo = -1;
                            trans.PaymentID = -1;
                            trans.Amount = _amount;
                            trans.ItemTypeId = 25;
                            trans.CreditorSum = 0;
                            trans.BureauSum = 0;
                            trans.DebitorSum = _amount;
                            trans.RegDate = DateTime.Now;
                            trans.Balance = 0;
                            trans.Text = "Return to Debtor";
                            if (transList.Count > 0)
                                trans.RowNo = transList.LastOrDefault().RowNo + 1;
                            else
                                trans.RowNo = 1;
                            trans.TotalBalance = 0;
                            transList.Add(trans);
                        }
                    }

                }*/
            }
            catch (Exception)
            {
                throw; ;
            }
            return transList;
        }
    }
}
