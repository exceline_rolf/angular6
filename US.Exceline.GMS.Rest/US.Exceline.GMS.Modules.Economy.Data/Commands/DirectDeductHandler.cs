﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.GMS.Core.ResultNotifications;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    internal class DirectDeductHandler : ClaimHandler
    {
        public DirectDeductHandler(string userName)
        {
            //ClaimPersistingManager = new ClaimPersititngManager(US_DataAccess.EnumDatabase.USP, userName);
        }

        ///// <summary>
        ///// Add direct deduct record to the database
        ///// </summary>
        ///// <param name="claim">Claim to be added</param>
        ///// <param name="ARItemNo">ARItem No of saved ARItem record</param>
        //public void AddDirectDeduct(IUSPClaim claim, int ARItemNo)
        //{
        //    int DirectdiductID = -1;

        //    //Mapping DirectDeduct Data Contract to DirectDeduct object
        //    DirectDeduct directDeduct = new DirectDeduct();
        //    directDeduct.AccountNo = claim.AccountNumber;//dd.AccountNumber;
        //    directDeduct.Balance = claim.Balance;
        //    directDeduct.BranchNo = claim.BranchNumber;
        //    directDeduct.CollectingStatus = (CollectingStatus)(int)claim.CollectingStatus;
        //    directDeduct.CollectingText = claim.InkassoTest;
        //    directDeduct.ContractExpire = claim.ContractExpire;
        //    directDeduct.ContractKID = claim.ContractKID;
        //    directDeduct.ContractNo = claim.ContractNumber;
        //    directDeduct.DueBalance = claim.DueBalance;
        //    directDeduct.InstallmentKID = claim.KID;
        //    directDeduct.InstallmentNo = claim.InstallmentNumber;
        //    directDeduct.LastContract = claim.LastContract;
        //    directDeduct.LastReminder = claim.LastReminder;
        //    directDeduct.StatusMendatory = (DirectDeductStatus)(int)claim.Status;
        //    directDeduct.TrancemissionNo = claim.TransmissionNumberBBS;

        //    directDeduct.nameInContract = claim.NameInContract;
        //    directDeduct.lastVisit = claim.LastVisit;
        //    directDeduct.printDate = claim.PrintDate;
        //    directDeduct.originalDue = claim.OriginalDueDate;
        //    directDeduct.reminderfee = claim.ReminderFee.ToString();

        //    // Empty string should be filled
        //    DirectdiductID = DirectDeductManager.SaveDirectDeduct(directDeduct, ARItemNo);
        //    //entNo = DebitorManager.AddDebitor(adebitor, addr, DebitorManager.IsDebtorAvailable(inkassoID, custID));

        //    if (DirectdiductID < 0)
        //    {
        //        throw new USPOperationFailedException("Failed to add Direct Deduct with InkassoID["
        //            + claim.Creditor.InkassoID + "] , Customer ID[" + claim.Debtor.custumerID + "] and Invoice Number[" + claim.InvoiceNumber + "]", null);
        //    }
        //}

        /// <summary>
        /// Direct Deduct sepecific logic to add Direct Deduct claim
        /// </summary>
        /// <param name="claim">Claim to be added</param>
        /// <param name="arItemNo">ARItem No of saved ARItem record</param>
        /// <returns>USImportResult object containing error,warn,infos</returns>
        internal override OperationResult SaveClaim(DbTransaction transaction, IUSPClaim claim, int arItemNo)
        {

            //Nothing to do because direct deduct table is removed

            var result = new OperationResult();

            #region



            //// Assign tags of claim to returning result
            //result.Tag1 = claim.Tag1;
            //result.Tag2 = claim.Tag2;

            //try
            //{
            //    AddDirectDeduct(claim,ARItemNo);
            //}
            //catch (USPOperationFailedException ex)
            //{
            //    // Add error messages to sending result
            //    USNotificationMessage message = new USNotificationMessage(ErrorLevels.Error, ex.Message);
            //    result.ErrorOccured = true;
            //    result.AddUSNotificationMessage(message);
            //}
            //catch (Exception ex)
            //{
            //    // Add error messages to sending result
            //    USNotificationMessage message = new USNotificationMessage(ErrorLevels.Error, "Failed to update debt warning date for record with InkassoID["
            //        + claim.Creditor.InkassoID + "] , Customer ID[" + claim.Debtor.custumerID + "] and Invoice Number[" + claim.InvoiceNumber + "]. Because " + ex.Message);
            //    result.ErrorOccured = true;
            //    result.AddUSNotificationMessage(message);
            //}

            #endregion

            return result;
        }
    }
}
