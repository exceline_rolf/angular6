﻿using System;
using System.Data.Common;
using System.Data;
using System.Text;
using US_DataAccess;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;


namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class GetInvoicePrintRecordAction : USDBActionBase<InvoiceXMLData>
    {
        int _branchId = 0;
        String _invoiceNo = String.Empty;

        public GetInvoicePrintRecordAction(int branchId, String invoiceNo)
        {
            _branchId = branchId;
            _invoiceNo = invoiceNo;

        }

        protected override InvoiceXMLData Body(System.Data.Common.DbConnection connection)
        {
            string spName = "exceline.GetInvoicePrintRecord";
            DbDataReader reader = null;
            InvoiceXMLData Data = new InvoiceXMLData();

            try
            {

                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
               
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InvoiceNo", DbType.String, _invoiceNo));
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {

                    if (reader.HasRows == true)
                    {
                        {
                            Data.hashValue = Convert.ToString(reader["HashValue"]);
                            Data.InvoiceXML = Convert.ToString(reader["XMLData"]);
                        
                        }
                    };
              
                }
               

            }
            catch (Exception ex)
            {


                throw ex;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }
            return Data;
        }
    }
}
