﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class RegisterTransactionAction : USDBActionBase<PaymentProcessResult>
    {
        private readonly List<IUSPTransaction> _transactionInfoList;
        private DbTransaction _transaction = null;

        public RegisterTransactionAction(List<IUSPTransaction> transactionList)
        {
            _transactionInfoList = transactionList;
        }

        protected override PaymentProcessResult Body(DbConnection connection)
        {
            return new PaymentProcessResult();
        }

        public PaymentProcessResult RunOnTransaction(DbTransaction transaction)
        {
            PaymentProcessResult result = new PaymentProcessResult();
            result.ResultStatus = true;
            string paymentVoucherNo = string.Empty;
            try
            {
                foreach (var transactionInfo in _transactionInfoList)
                {
                    GetTransactionProfileAction getInvoiceProfileAction = new GetTransactionProfileAction(transactionInfo);
                    PaymentProcessStepResult profileResult = getInvoiceProfileAction.RunOnTranaction(transaction);
                    result.ProcessStepResultList.Add(profileResult);

                    if (!profileResult.ResultStatus)
                    {
                        transaction.Rollback();
                        result.ResultStatus = false;
                        return result;
                    }

                    FindDestinationAction searchInvoiceAction = new FindDestinationAction(transactionInfo);
                    PaymentProcessStepResult searchInvoiceResult = searchInvoiceAction.RunOnTranaction(transaction, transactionInfo.TransactionProfile);
                    result.ProcessStepResultList.Add(searchInvoiceResult);

                    if (!searchInvoiceResult.ResultStatus)
                    {
                        transaction.Rollback();
                        result.ResultStatus = false;
                        return result;
                    }

                    SaveTransactionAction savePaymentAction = new SaveTransactionAction(transactionInfo);
                    PaymentProcessStepResult savePayumentResult = savePaymentAction.RunOnTransaction(transaction, transactionInfo.TransactionProfile);
                    result.ProcessStepResultList.Add(savePayumentResult);

                    if (!savePayumentResult.ResultStatus)
                    {
                        result.ResultStatus = false;
                        return result;
                    }

                    DistributePaymentAction distributeAction = new DistributePaymentAction(transactionInfo);
                    PaymentProcessStepResult distributeResult = distributeAction.RunOnTransaction(transaction, transactionInfo.TransactionProfile);

                    result.ProcessStepResultList.Add(distributeResult);

                    if (!distributeResult.ResultStatus)
                    {
                        result.ResultStatus = false;
                        return result;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }
    }
}
