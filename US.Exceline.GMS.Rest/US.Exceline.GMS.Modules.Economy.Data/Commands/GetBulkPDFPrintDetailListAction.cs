﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.Exceline.GMS.Modules.Economy.Core.DomainObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class GetBulkPDFPrintDetailListAction : USDBActionBase<List<BulkPDFPrint>>
    {
        private readonly int _branchId = -1;
        private readonly string _type = string.Empty;
        private readonly DateTime _fromDate = DateTime.Now;
        private readonly DateTime _toDate = DateTime.Now;

        public GetBulkPDFPrintDetailListAction(string type, DateTime fromDate, DateTime toDate, int branchId)
        {
            _type = type;
            _fromDate = fromDate;
            _toDate = toDate;
            _branchId = branchId;
        }

        protected override List<BulkPDFPrint> Body(DbConnection connection)
        {
            var pdfList = new List<BulkPDFPrint>();
            DbDataReader dataReader = null;
            try
            {
                const string storedProcedureName = "USExceGetBulkPDFPrintDetail";
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@type", DbType.String, _type));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@fromDate", DbType.DateTime, _fromDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@toDate", DbType.DateTime, _toDate));

                dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    var item = new BulkPDFPrint();
                    item.Id = Convert.ToInt32(dataReader["Id"]);
                    item.DocType = Convert.ToString(dataReader["DocType"]);
                    item.PDFPath = Convert.ToString(dataReader["PDFPath"]);
                    if (!string.IsNullOrEmpty(Convert.ToString(dataReader["NoOfDoc"])))
                        item.NoOfDoc = Convert.ToInt32(dataReader["NoOfDoc"]);
                    if (!string.IsNullOrEmpty(Convert.ToString(dataReader["FailCount"])))
                        item.FailCount = Convert.ToInt32(dataReader["FailCount"]);
                    item.CreatedUser = Convert.ToString(dataReader["CreatedUser"]);
                    if (!string.IsNullOrEmpty(Convert.ToString(dataReader["CreatedDateTime"])))
                        item.CreatedDateTime = Convert.ToDateTime(dataReader["CreatedDateTime"]);
                    if (!string.IsNullOrEmpty(Convert.ToString(dataReader["BranchId"])))
                        item.BranchId = Convert.ToInt32(dataReader["BranchId"]);
                    pdfList.Add(item);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();
                    dataReader.Dispose();
                }
            }
            return pdfList;
        }
    }
}
