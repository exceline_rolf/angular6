﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.DomainObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class GeneralInvoiceSearchAction : USDBActionBase<List<USPARItem>>
    {
        private string _searchValue = string.Empty;
        private string _secondValue = string.Empty;
        private int _invoiceType = 0;
        private int _suggest = 0;
        private string _fieldType = string.Empty;
        private SearchModes _searchMode = SearchModes.NONE;
        private object _contsValue = null;
        private int _paymentID = -1;

        public GeneralInvoiceSearchAction(string searchValue, string secondValue
            , int suggest, string fieldType, int invoiceType, SearchModes searchMode, object constValue, int paymentID)
        {
            _searchValue = searchValue;
            _secondValue = secondValue;
            _suggest = suggest;
            _invoiceType = invoiceType;
            _fieldType = fieldType;
            _searchMode = searchMode;
            _contsValue = constValue;
            _paymentID = paymentID;
        }

        protected override List<USPARItem> Body(System.Data.Common.DbConnection connection)
        {
            List<USPARItem> arItemList = new List<USPARItem>();
            DbDataReader reader = null;
            try
            {
                DbCommand command = connection.CreateCommand();
                command.CommandTimeout = int.MaxValue;
                command.CommandText = "USP_Search_Invoice_Combine_Fulltext";
                command.CommandType = System.Data.CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@searchvalue", _searchValue));
                command.Parameters.Add(new SqlParameter("@secvalue", _secondValue));
                command.Parameters.Add(new SqlParameter("@suggest", _suggest));
                command.Parameters.Add(new SqlParameter("@invoiceType", _invoiceType));
                command.Parameters.Add(new SqlParameter("@fieldType", _fieldType));
                command.Parameters.Add(new SqlParameter("@paymentID", _paymentID));

                if (_searchMode == SearchModes.AR)
                {
                    command.Parameters.Add(new SqlParameter("@ArNo", Convert.ToInt32(_contsValue)));
                }
                if (_searchMode == SearchModes.CREDITOR)
                {
                    command.Parameters.Add(new SqlParameter("@creditorNo", Convert.ToInt32(_contsValue)));
                }
                if (_searchMode == SearchModes.CREDITORGROUP)
                {
                    command.Parameters.Add(new SqlParameter("@creditorGroupId", Convert.ToInt32(_contsValue)));
                }

                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    USPARItem arItem = new USPARItem();
                    arItem.InkassoID = reader["pid"].ToString();
                    arItem.cusID = reader["cusid"].ToString();
                    arItem.DebtorName = reader["DebtorName"].ToString();
                    arItem.KID = reader["KID"].ToString();
                    arItem.InvoiceNumber = reader["ref"].ToString();
                    arItem.Amount = (reader["amount"] != DBNull.Value) ? Convert.ToDecimal(reader["amount"]) : 0;
                    arItem.ARItemNo = reader["ArItemNo"].ToString();
                    if (reader["RegDate"] != DBNull.Value)
                        arItem.RegDate = Convert.ToDateTime(reader["RegDate"]);
                    if (reader["VoucherDate"] != DBNull.Value)
                        arItem.InvoicedDate = Convert.ToDateTime(reader["VoucherDate"]);
                    if (reader["DueDate"] != DBNull.Value)
                        arItem.DueDate = Convert.ToDateTime(reader["DueDate"]);
                    arItem.ItemTypeId = Convert.ToInt32(reader["ItemTypeId"]);

                    arItem.CreditorName = reader["CreditorName"].ToString();
                    arItem.Creditor = arItem.InkassoID + " - " + arItem.CreditorName;
                    arItem.Debtor = arItem.cusID + " - " + arItem.DebtorName;
                    arItem.Hit = reader["hit"].ToString().Trim(',');
                    arItem.SubCaseNo = Convert.ToInt32(reader["SubCaseNo"]);
                    arItem.CaseNo = Convert.ToInt32(reader["CaseNo"]);
                    arItem.Balance = (reader["balance"] != DBNull.Value) ? Convert.ToDecimal(reader["balance"]) : 0;
                    if (_searchMode == SearchModes.AR)
                    {
                        arItem.IsCheckBoxEnabled = false;
                    }
                    else
                    {
                        arItem.IsCheckBoxEnabled = true;
                    }
                    arItemList.Add(arItem);
                }
                return arItemList;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
            //return arItemList;
        }
    }
}
