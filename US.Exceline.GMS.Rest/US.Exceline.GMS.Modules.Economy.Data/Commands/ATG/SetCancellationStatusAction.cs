﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands.ATG
{
    public class SetCancellationStatusAction : USDBActionBase<bool>
    {
        private string _SendingNo = string.Empty;

        public SetCancellationStatusAction(string sendingNo)
        {
            _SendingNo = sendingNo;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USP_SetCancellationStatus";

            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@SendingNo", System.Data.DbType.String, _SendingNo));
                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
