﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands.ATG
{
    public class ReadDDAction : USDBActionBase<DirectDeductInfo>
    {
        public string creditornum;
        public string firstDue;
        public string lastDue;
        public string file;
        public string totalAm;
        private int status;
        private DateTime _startDate;
        private DateTime _endDate;
        private string _SendingNo = string.Empty;

        public ReadDDAction(DateTime startDate, DateTime endDate, string creditor, int st, string sendingNo)
        {
            this.creditornum = creditor;
            this.status = st;
            _startDate = startDate;
            _endDate = endDate;
            _SendingNo = sendingNo;
        }

        protected override DirectDeductInfo Body(System.Data.Common.DbConnection connection)
        {
            DirectDeductInfo infoObj = new DirectDeductInfo();
            List<DirectDeduct> ddList = new List<DirectDeduct>();
            try
            {
                string storedProcedureName = "USP_GetDirectDeductsForCreditor";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@credId", DbType.Int32, int.Parse(creditornum)));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@creStatus", DbType.Int32, status));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@dueDateStarting", DbType.DateTime, _startDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@dueDateEnding", DbType.DateTime, _endDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SendingNo", DbType.String, _SendingNo));
                double total = 0;

                DbDataReader ddReader = cmd.ExecuteReader();
                while (ddReader.Read())
                {
                    try
                    {

                        DateTime dt;
                        DirectDeduct dd = new DirectDeduct();
                        dt = Convert.ToDateTime(ddReader["DueDate"]);
                        dd.dueDate = String.Format("{0:ddMMyy}", dt);
                        string am = (Convert.ToDecimal(ddReader["Amount"]) * 100).ToString();
                        dd.amount = am.Substring(0, (am.Length - 3));

                        dd.InstallmentKID = (ddReader["InstallmentKID"] == DBNull.Value) ? string.Empty : Convert.ToString(ddReader["InstallmentKID"]);

                        dd.debtorName = Convert.ToString(ddReader["DebtorName"]);

                        total = total + double.Parse(dd.amount);

                        infoObj.seq = Convert.ToString(ddReader["Trno"]); 

                        if ( ddReader["MessageText"] == DBNull.Value)
                        {
                            dd.message = "";
                        }
                        else
                        {
                            dd.message = Convert.ToString(ddReader["MessageText"]);
                        }
                        dd.AritemNo = Convert.ToString(ddReader["ARItemNo"]);
                        dd.ContractNo = Convert.ToString(ddReader["ContractNo"]);                 
                        ddList.Add(dd);
                        
                    }
                    
                    catch (Exception ){}                    
                }

                infoObj.ddObj = ddList;
                string tot = total.ToString();
                infoObj.totalAmount = tot;
            }
            catch (Exception )
            {
                throw ;
            }
            return infoObj;
        }
    }
}
