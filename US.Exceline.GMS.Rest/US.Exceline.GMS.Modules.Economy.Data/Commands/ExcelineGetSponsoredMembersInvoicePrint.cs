﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using US_DataAccess;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class ExcelineGetSponsoredMembersInvoicePrint : USDBActionBase<List<ExcelineSponsorMembersForPrint>>
    {
        private String _sponsorARItemNo = String.Empty;
        private String _gymCode = String.Empty;

        public ExcelineGetSponsoredMembersInvoicePrint(String sponsorARItemNo, String gymCode)
        {
            _sponsorARItemNo = sponsorARItemNo;
            _gymCode = gymCode;
        }

        protected override List<ExcelineSponsorMembersForPrint> Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USC_ADP_LDRPTSP_SponsorInvoice_OrderLine";
            DbDataReader reader = null;

            List<ExcelineSponsorMembersForPrint> memberList = new List<ExcelineSponsorMembersForPrint>();

            try
            {

                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, spName);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@itemID", System.Data.DbType.String, _sponsorARItemNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@gymCode", System.Data.DbType.String, _gymCode));
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ExcelineSponsorMembersForPrint sponsoredMember = new ExcelineSponsorMembersForPrint();

                    sponsoredMember.OrderLineNo = Convert.ToInt32(reader["OrderLineNo"]);
                    sponsoredMember.ARItemNo = Convert.ToInt32(reader["ARItemNo"]);
                    sponsoredMember.CreditorInkassoID = Convert.ToString(reader["CreditorInkassoID"]);
                    sponsoredMember.CustId = Convert.ToString(reader["CusId"]);
                    sponsoredMember.LastName = Convert.ToString(reader["LastName"]);
                    sponsoredMember.FirstName = Convert.ToString(reader["FirstName"]);
                    sponsoredMember.NameOnContract = Convert.ToString(reader["NameOnContract"]);
                    sponsoredMember.Address = Convert.ToString(reader["Address"]);
                    sponsoredMember.ZipCode = Convert.ToString(reader["ZipCode"]);
                    sponsoredMember.PostPlace = Convert.ToString(reader["PostPlace"]);
                    sponsoredMember.DiscountAmount = Convert.ToInt32(reader["DiscountAmount"]);
                    sponsoredMember.EmployeeNo = Convert.ToString(reader["EmployeeNo"]);
                    sponsoredMember.SponsorRef = Convert.ToString(reader["SponsorRef"]);
                    sponsoredMember.Visits = Convert.ToInt32(reader["Visits"]);
                    sponsoredMember.AmountOrderline = Convert.ToInt32(reader["AmountOrderline"]);
                   // sponsoredMember.LastVisit = Convert.ToDateTime(reader["LastVisit"]);
                    sponsoredMember.Amount = Convert.ToInt32(reader["Amount"]);
                    sponsoredMember.TrainingPeriodStart = Convert.ToDateTime(reader["TrainingPeriodStart"]);
                    sponsoredMember.TrainingPeriodEnd = Convert.ToDateTime(reader["TrainingPeriodEnd"]);
                   
                    memberList.Add(sponsoredMember);
                }

            }
            catch (Exception)
            {


                throw;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }
            return memberList;
        }
    }
}
