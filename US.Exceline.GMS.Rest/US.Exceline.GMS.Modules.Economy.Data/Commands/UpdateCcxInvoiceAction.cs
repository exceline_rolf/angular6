﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Ccx;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class UpdateCcxInvoiceAction : USDBActionBase<int>
    {
        private DataTable _dataTable;
        public UpdateCcxInvoiceAction(List<Invoice> invoices)
        {
            _dataTable = GetInvoiceLst(invoices);
        }

        private DataTable GetInvoiceLst(List<Invoice> invList)
        {
            _dataTable = new DataTable();
            _dataTable.Columns.Add(new DataColumn("ReferenceId", typeof(Int32)));
            _dataTable.Columns.Add(new DataColumn("CustId", typeof(String)));
            _dataTable.Columns.Add(new DataColumn("BatchId", typeof(String)));
            _dataTable.Columns.Add(new DataColumn("CreditorInkassoId", typeof(Int32)));
            _dataTable.Columns.Add(new DataColumn("Kid", typeof(String)));
            _dataTable.Columns.Add(new DataColumn("NewDueDate", typeof(DateTime)));

            if (invList != null && invList.Any())
                foreach (var item in invList)
                {
                    DataRow dataTableRow = _dataTable.NewRow();
                    dataTableRow["ReferenceId"] = item.ReferenceId;
                    dataTableRow["CustId"] = item.CustId;
                    dataTableRow["BatchId"] = item.BatchNumber;
                    dataTableRow["CreditorInkassoId"] = item.Cid;
                    dataTableRow["Kid"] = item.InvoiceKid;
                    dataTableRow["NewDueDate"] = item.NewDueDate;
                    _dataTable.Rows.Add(dataTableRow);
                }

          

            return _dataTable;
        }

        protected override int Body(DbConnection connection)
        {
            const string storedProcedureName = "ExceWorkStationSaveCcxInvoiceDetail";
            try
            {
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add( DataAcessUtils.CreateParam("@InvoiceDetail", SqlDbType.Structured, _dataTable));

                //cmd.Parameters.Add(DataAcessUtils.CreateParam("@docType", DbType.String, _docType));
                //cmd.Parameters.Add(DataAcessUtils.CreateParam("@dataIdList", DbType.String, _dataIdList));
                //cmd.Parameters.Add(DataAcessUtils.CreateParam("@pdfFilePath", DbType.String, _pdfFilePath));
                //cmd.Parameters.Add(DataAcessUtils.CreateParam("@noOfDocument", DbType.Int32, _noOfDoc));
                //cmd.Parameters.Add(DataAcessUtils.CreateParam("@failCount", DbType.Int32, _failCount));
                //cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                //cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));

                cmd.ExecuteNonQuery();
                
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return 1;
        }
    }
}
