﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.GMS.Core.ResultNotifications;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    internal class IPorSPRecordHandle : ClaimHandler
    {
        /// <summary>
        /// Invoice for Printing or  sepecific logic to add Direct Deduct claim
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public IPorSPRecordHandle(string userName)
        {
            //ClaimPersistingManager = new ClaimPersititngManager(EnumDatabase.USP, userName);
        }

        internal override OperationResult SaveClaim(DbTransaction transaction, IUSPClaim claim, int arItemNo)
        {
            // Create object that send as result of method invocation
            var result = new OperationResult { Tag1 = claim.Tag1, Tag2 = claim.Tag2, TagSavedID = arItemNo };
            // Assign tags of claim to returning result

            if (claim.OrderLines == null)
            {
                throw new ArgumentException("Order line list is NULL");
            }
            if (claim.OrderLines.Count < 1)
            {
                throw new ArgumentException("Order line record count is zero");
            }
            foreach (IUSPOrderLine orderLineRecord in claim.OrderLines)
            {
                try
                {
                    AddOrderLineRecord(transaction, claim, orderLineRecord, arItemNo);
                }
                catch (Exception ex)
                {
                    throw new Exception("Failed to add Order Line Record with Tag [" + orderLineRecord.Tag1 +
                                     "] InkassoID["
                                     + claim.Creditor.CreditorInkassoID + "] , Customer ID[" +
                                     claim.Debtor.DebtorcustumerID
                                     + "] and Invoice Number[" + claim.InvoiceNumber + "]. Because " + ex.Message + ex.Message);
                }
            }
            return result;
        }

        /// <summary>
        /// Add Order Line records to the database
        /// </summary>
        /// <param name="mainClaim">Claim that contain the list of Order Line records</param>
        /// <param name="orderLineRecord">Order Line record to be added</param>
        /// <param name="arItemNo">ARItem No of saved ARItem record</param>
        private void AddOrderLineRecord(DbTransaction transaction, IUSPClaim mainClaim, IUSPOrderLine orderLineRecord, int arItemNo)
        {
            var orderLine = new OrderLine
            {
                ArItemNo = arItemNo,
                InkassoId = mainClaim.Creditor.CreditorInkassoID,
                CutId = orderLineRecord.CustId,
                LastName = orderLineRecord.LastName,
                FirstName = orderLineRecord.FirstName,
                NameOnContract = orderLineRecord.NameOncontract,
                Address = orderLineRecord.Address,
                ZipCode = orderLineRecord.ZipCode,
                PostPlace = orderLineRecord.PostPlace,
                DiscountAmount = orderLineRecord.DiscountAmount,
                EmployeeNo = orderLineRecord.EmployeeNo,
                SponsorRef = orderLineRecord.SponsorRef,
                Visit = orderLineRecord.Visit,
                LastVisit = orderLineRecord.LastVisit,
                Amount = orderLineRecord.Amount,
                AmountThisOrderLines = orderLineRecord.AmountThisOrderLines,
                 SourceInstallmentID = orderLineRecord.SourceInstallmentID
            };

            int savedId = -1;

            savedId = SaveOrderLine(transaction, orderLine);
            if (savedId < 0)
            {
                throw new Exception(
                    "Failed to add Order Line Record with Tag [" + orderLineRecord.Tag1
                    + "] InkassoID[" + mainClaim.Creditor.CreditorInkassoID + "] , Customer ID[" +
                    mainClaim.Debtor.DebtorcustumerID
                    + "] and Invoice Number[" + mainClaim.InvoiceNumber + "]", null);
            }
        }

        private int SaveOrderLine(DbTransaction transaction,  OrderLine orderLine)
        {
            AddOrderLine action = new AddOrderLine(orderLine);
            return action.RunOnTransaction(transaction);
        }


    }
}
