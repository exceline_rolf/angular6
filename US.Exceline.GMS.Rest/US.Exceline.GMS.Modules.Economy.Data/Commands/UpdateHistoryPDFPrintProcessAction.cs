﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class UpdateHistoryPDFPrintProcessAction : USDBActionBase<int>
    {
        private readonly int _id = -1;
        private readonly int _invoiceCount = -1;
        private readonly int _failCount = -1;
        private readonly int _branchId = -1;
        private readonly string _user = string.Empty;
        private readonly string _filePath = string.Empty;

        public UpdateHistoryPDFPrintProcessAction(int id, int invoiceCount, int failCount, int branchId, string user, string filePath)
        {
            _id = id;
            _invoiceCount = invoiceCount;
            _failCount = failCount;
            _branchId = branchId;
            _user = user;
            _filePath = filePath;
        }

        protected override int Body(DbConnection connection)
        {
            var historyId = -1;
            const string storedProcedureName = "USExceGMSUpdateHistoryPDFPrintProcess";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@id", DbType.Int32, _id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@filePath", DbType.String, _filePath));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@invoiceCount", DbType.Int32, _invoiceCount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@failCount", DbType.Int32, _failCount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));

                DbParameter para = new SqlParameter();
                para.DbType = DbType.Int32;
                para.ParameterName = "@outId";
                para.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(para);
                
                cmd.ExecuteNonQuery();
                historyId = Convert.ToInt32(para.Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return historyId;
        }
    }
}
