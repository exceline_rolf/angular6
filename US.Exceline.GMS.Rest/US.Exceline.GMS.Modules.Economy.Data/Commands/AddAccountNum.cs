﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class AddAccountNum : USDBActionBase<int>
    {
        private string _description = string.Empty;
        private int _account = -1;

        public AddAccountNum(string description, int account)
        {
            _description = description;
            _account = account;
        }

        protected override int Body(DbConnection connection)
        {
            string spName = "UpdateKasseFileSettings";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@description", System.Data.DbType.String, _description));
                command.Parameters.Add(DataAcessUtils.CreateParam("@account", System.Data.DbType.Int32, _account));
                command.ExecuteNonQuery();

            }
            catch (Exception) { }
            return 1;
        }
    }
}
