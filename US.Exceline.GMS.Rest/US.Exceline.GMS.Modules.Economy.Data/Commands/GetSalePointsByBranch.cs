﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class GetSalePointsByBranchAction : USDBActionBase<List<KeyData>>
    {

        

        public GetSalePointsByBranchAction(int branchId)
        {
           
           

        }


        protected override List<KeyData> Body(DbConnection connection)
        {
            string spName = "GetKeyDataForBranch";
            DbDataReader reader = null;
            List<KeyData> economyFileLogData = new List<KeyData>();



            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    KeyData fileLogData = new KeyData
                    {
                        KeyVersion = Convert.ToInt32(reader["KeyVersion"]),
                        PrivateKey = Convert.ToString(reader["PrivateKey"]),
                        PublicKey = Convert.ToString(reader["PublicKey"])

                    };
                    economyFileLogData.Add(fileLogData);
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }


            finally
            {

                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }

            return economyFileLogData;
        }
    }
}
