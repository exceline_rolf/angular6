﻿using System;
using System.Data.Common;
using System.Data;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class InsertInvoicePrintDiscrepancyInstanceAction : USDBActionBase<bool>
    {
        DateTime _timeStamp = DateTime.Now;
        String _invoiceNo = String.Empty;
        String _originalXml = String.Empty;
        String _newXml = String.Empty;

        public InsertInvoicePrintDiscrepancyInstanceAction(DateTime timeStamp, String invoiceNo, String originalXml, String newXml)
        {
            _timeStamp = timeStamp;
            _invoiceNo = invoiceNo;
            _originalXml = originalXml;
            _newXml = newXml;

        }

        protected override bool Body(DbConnection connection)
        {
            string spName = "InsertInvoicePrintDiscrepancyInstance";
            
            try
            {

                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, spName);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TimeStamp", DbType.DateTime, _timeStamp));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InvoiceRef", DbType.String, _invoiceNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@OriginalXml", DbType.String, _originalXml));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@NewXml", DbType.String, _newXml));
                cmd.ExecuteNonQuery();

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            
        }
    }
}
