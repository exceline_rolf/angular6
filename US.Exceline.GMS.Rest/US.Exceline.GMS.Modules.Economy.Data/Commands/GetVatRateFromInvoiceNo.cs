﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class GetVatRateFromInvoiceNoAction : USDBActionBase<double>
    {
        private String _invoiceNo;
      

        public GetVatRateFromInvoiceNoAction(int invoiceNo)
        {
            _invoiceNo = Convert.ToString(invoiceNo);

        }


        protected override double Body(System.Data.Common.DbConnection connection)
        {
            string spName = "GetVatRateFromInvoiceNo";
            DbDataReader reader = null;
            double fileLogData = 0.00;



            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@InvoiceNo", System.Data.DbType.String, _invoiceNo));
                
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    
                    {

                        fileLogData = Convert.ToDouble(reader["VAT"]);
                      

                    };
                    
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }


            finally
            {

                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }

            return fileLogData;
        }
    }
}
