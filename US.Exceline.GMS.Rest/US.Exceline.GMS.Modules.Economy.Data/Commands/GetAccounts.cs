﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Diagnostics;
using US_DataAccess;
using US.Exceline.GMS.Modules.Economy.Core.DomainObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class GetAccounts : USDBActionBase<List<AccountNamesAndNumbers>>
    {

    
        public GetAccounts()
        {
            
        }

        protected override List<AccountNamesAndNumbers> Body(System.Data.Common.DbConnection connection)
        {
            string spName = "GetAccountsFromKasseFil";
            DbDataReader reader = null;
            List<AccountNamesAndNumbers> economyFileLogData = new List<AccountNamesAndNumbers>();
         

            

            try
            {
              
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure,spName);
                reader = cmd.ExecuteReader();
               
                while (reader.Read())
                {
                    AccountNamesAndNumbers fileLogData = new AccountNamesAndNumbers
                    {
                        
                        Name = Convert.ToString(reader["Name"]),
                        Account = Convert.ToString(reader["Account"]),
                        Description = Convert.ToString(reader["Description"])
                    };

                    economyFileLogData.Add(fileLogData);
                    
                }
                
            }
            catch (Exception) {
               
                
                throw;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }
            return economyFileLogData;
        }
    }  
}
