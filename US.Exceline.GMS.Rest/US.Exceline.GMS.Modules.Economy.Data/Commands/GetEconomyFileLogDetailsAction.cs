﻿using System;
using System.Collections.Generic;
using US.Exceline.GMS.Modules.Economy.Core.DomainObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class GetEconomyFileLogDetailsAction : USDBActionBase<List<EconomyFileLogData>>
    {
        private readonly DateTime _fromDate;
        private readonly DateTime _toDate;
        private readonly string _uniqueId;
        private readonly string _source;
        private readonly string _filterBy;
        private readonly string _paymentType;

        public GetEconomyFileLogDetailsAction(EconomyLogFileHelper economyLogFileHelper)
        {
            _fromDate = economyLogFileHelper.FromDate;
            _toDate = economyLogFileHelper.ToDate;
            _uniqueId = economyLogFileHelper.UniqueId;
            _source = economyLogFileHelper.Source;
            _filterBy = economyLogFileHelper.FilterBy;
            _paymentType = economyLogFileHelper.PaymentType;
        }
        protected override List<EconomyFileLogData> Body(System.Data.Common.DbConnection dbConnection)
        {
            var economyFileLogData = new List<EconomyFileLogData>();
            const string storedProcedureName = "USExceGMSGetEconomyFileLogDetail";

            var cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
            cmd.Parameters.Add(DataAcessUtils.CreateParam("@FromDate", System.Data.DbType.DateTime, _fromDate));
            cmd.Parameters.Add(DataAcessUtils.CreateParam("@ToDate", System.Data.DbType.DateTime, _toDate));
            cmd.Parameters.Add(DataAcessUtils.CreateParam("@Source", System.Data.DbType.String, _source));
            cmd.Parameters.Add(DataAcessUtils.CreateParam("@FilterBy", System.Data.DbType.String, _filterBy));
            cmd.Parameters.Add(DataAcessUtils.CreateParam("@UniqueId", System.Data.DbType.String, _uniqueId));
            cmd.Parameters.Add(DataAcessUtils.CreateParam("@PaymentType", System.Data.DbType.String, _paymentType));

            try
            {
                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var fileLogData = new EconomyFileLogData
                    {
                        Date = Convert.ToDateTime(reader["Date"].ToString()),
                        DateOfFirstOpening = Convert.ToDateTime(reader["Date"].ToString()),
                        TotalDeduction = Convert.ToDecimal(reader["TotalDeduction"]),
                        BatchID = reader["BatchID"].ToString(),
                        Type = reader["ItemType"].ToString(),
                        Status = reader["Status"].ToString(),
                        KID = reader["KID"].ToString()
                    };
                    if (_filterBy == "Export")
                        fileLogData.DateOfFirstOpening = null;
                    economyFileLogData.Add(fileLogData);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return economyFileLogData;

        }
    }
}
