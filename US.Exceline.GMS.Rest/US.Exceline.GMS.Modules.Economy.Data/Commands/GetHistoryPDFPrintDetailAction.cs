﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.Exceline.GMS.Modules.Economy.Core.DomainObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class GetHistoryPDFPrintDetailAction : USDBActionBase<List<HistoryPDFPrint>>
    {
        private DateTime _fromDate;
        private DateTime _toDate;

        public GetHistoryPDFPrintDetailAction(DateTime fromDate, DateTime toDate)
        {
            _fromDate = fromDate;
            _toDate = toDate;
        }
        protected override List<HistoryPDFPrint> Body(DbConnection connection)
        {
            var historyPDFPrint = new List<HistoryPDFPrint>();
            DbDataReader dataReader = null;
            try
            {
                const string storedProcedureName = "USExceGetHistoryPDFPrintDetail";
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@FromDate", DbType.DateTime, _fromDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ToDate", DbType.DateTime, _toDate));
                
                dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    var item = new HistoryPDFPrint();
                    item.Id = Convert.ToInt32(dataReader["Id"]);
                    if (!string.IsNullOrEmpty(Convert.ToString(dataReader["StartDateTime"])))
                        item.StartDateTime = Convert.ToDateTime(dataReader["StartDateTime"]);
                    if (!string.IsNullOrEmpty(Convert.ToString(dataReader["EndDateTime"])))
                        item.EndDateTime = Convert.ToDateTime(dataReader["EndDateTime"]);
                    item.CreatedUser = Convert.ToString(dataReader["CreatedUser"]);
                    item.NoOfInvoice = Convert.ToInt32(dataReader["NoOfInvoice"]);
                    if (!string.IsNullOrEmpty(Convert.ToString(dataReader["FailCount"])))
                        item.FailCount = Convert.ToInt32(dataReader["FailCount"]);
                    item.FilePath = Convert.ToString(dataReader["PDFPath"]);
                    item.IsPDFPrinted = !string.IsNullOrEmpty(Convert.ToString(dataReader["PDFPath"]));

                    historyPDFPrint.Add(item);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();
                    dataReader.Dispose();
                }
            }
            return historyPDFPrint;
        }
    }
}
