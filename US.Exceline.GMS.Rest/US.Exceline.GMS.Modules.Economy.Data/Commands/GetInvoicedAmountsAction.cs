﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class GetInvoicedAmountsAction : USDBActionBase<EstimatedInvoiceDetails>
    {
        private int _branchID = -1;
        private DateTime? _startDueDate;
        private DateTime? _endDueDate;

        public GetInvoicedAmountsAction(int branchID, DateTime? startDueDate, DateTime? endDueDate)
        {
            _branchID = branchID;
            _startDueDate = startDueDate;
            _endDueDate = endDueDate;
        }

        protected override EstimatedInvoiceDetails Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSGetInvoicedAmounts";
            DbDataReader reader = null;
            EstimatedInvoiceDetails invoiceDetails = null;
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", System.Data.DbType.Int32, _branchID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@StartDueDate", System.Data.DbType.DateTime, _startDueDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@EndDueDate", System.Data.DbType.DateTime, _endDueDate));

                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    invoiceDetails = new EstimatedInvoiceDetails();
                    invoiceDetails.GymId = _branchID;
                    invoiceDetails.InAmount = Convert.ToDecimal(reader["INAmount"]);
                    invoiceDetails.IpAmount = Convert.ToDecimal(reader["IPAmount"]);
                    invoiceDetails.PpAmount = Convert.ToDecimal(reader["PPAmount"]);
                    invoiceDetails.SpAmount = Convert.ToDecimal(reader["SPAmount"]);
                    invoiceDetails.DDAmount = Convert.ToDecimal(reader["DDAmount"]);
                    invoiceDetails.TotalAmount = Convert.ToDecimal(reader["TotalAmount"]);
                    invoiceDetails.ItemsCount = Convert.ToInt32(reader["InvoiceCount"]);
                    invoiceDetails.SpDDAmount = Convert.ToDecimal(reader["SPDDAmount"]);
                }
                return invoiceDetails;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (reader != null)
                    reader.Dispose();
            }
        }
    }
}
