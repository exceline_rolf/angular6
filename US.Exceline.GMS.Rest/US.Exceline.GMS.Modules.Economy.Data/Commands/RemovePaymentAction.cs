﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class RemovePaymentAction : USDBActionBase<bool>
    {
        private string _user = string.Empty;
        private int _paymentID = -1;
        private int _arItemno = -1;
        private string _type = string.Empty;

        public RemovePaymentAction(int paymentID, int arItemNo,  string user, string type)
        {
            _user = user;
            _paymentID = paymentID;
            _arItemno = arItemNo;
            _type = type;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            string spName = "ExceGMSRemoveErrorPayment";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@PaymentID", System.Data.DbType.Int32, _paymentID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@AritemNo", System.Data.DbType.Int32, _arItemno));
                command.Parameters.Add(DataAcessUtils.CreateParam("@User", System.Data.DbType.String, _user));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Type", System.Data.DbType.String, _type));
                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
