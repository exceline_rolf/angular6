﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    class UpdateCreditorInvoiceImportStatusAction : USDBActionBase<int>
    {
        private int _creditorInvoiceNo = -1;
        private int _arItemNo = -1;

        public UpdateCreditorInvoiceImportStatusAction(int creditorInvoiceNo, int arItemNo)
        {
            _creditorInvoiceNo = creditorInvoiceNo;
            _arItemNo = arItemNo;

        }
        protected override int Body(System.Data.Common.DbConnection connection)
        {
            try
            {
                DbCommand command = connection.CreateCommand();
                command.CommandText = "dbo.USP_CRE_UpdateCreditorInvoiceImportStatus";
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@creditorInvoiceId", _creditorInvoiceNo));
                command.Parameters.Add(new SqlParameter("@arItemNo", _arItemNo));
                command.ExecuteNonQuery();
                return 1;

            }
            catch (Exception)
            {
                throw;
            }
        }

        public int RunOnTransaction(DbTransaction transaction)
        {
            try
            {
                string spName = "dbo.USP_CRE_UpdateCreditorInvoiceImportStatus";
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Transaction = transaction;
                command.Connection = transaction.Connection;

                command.Parameters.Add(new SqlParameter("@creditorInvoiceId", _creditorInvoiceNo));
                command.Parameters.Add(new SqlParameter("@arItemNo", _arItemNo));
                command.ExecuteNonQuery();
                return 1;

            }
            catch (Exception ex)
            {
                throw new Exception("Error in updating Invoice Import Status", ex);
            }
        }
    }
}
