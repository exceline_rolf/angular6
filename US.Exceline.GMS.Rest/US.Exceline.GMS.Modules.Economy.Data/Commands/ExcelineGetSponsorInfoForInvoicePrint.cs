﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using US_DataAccess;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.Economy.Data.Commands
{
    public class ExcelineGetSponsorInfoForInvoicePrint : USDBActionBase<ExcelineSponsorInfoForPrint>
    {
        private String _sponsorARItemNo = String.Empty;
        private String _gymCode = String.Empty;

         public ExcelineGetSponsorInfoForInvoicePrint(String sponsorARItemNo, String gymCode )
         {
            _sponsorARItemNo = sponsorARItemNo;
            _gymCode = gymCode;
         }

        protected override ExcelineSponsorInfoForPrint Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USC_ADP_LDSP_SponsorInvoice";
            DbDataReader reader = null;

            ExcelineSponsorInfoForPrint sponsorData = new ExcelineSponsorInfoForPrint();

            try
            {

                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
         
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@itemID", System.Data.DbType.String, _sponsorARItemNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@gymCode", System.Data.DbType.String, _gymCode));
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {


                    sponsorData.InvoiceNo = Convert.ToString(reader["InvoiceNo"]);
                    sponsorData.ViewVisit = Convert.ToString(reader["ViewVisit"]);
                    sponsorData.GymName = Convert.ToString(reader["GymName"]);
                    sponsorData.GymAddress1 = Convert.ToString(reader["GymAddress1"]);
                    sponsorData.GymAddress2 = Convert.ToString(reader["GymAddress2"]);
                    sponsorData.GymPostalAddress = Convert.ToString(reader["GymPostalAddress"]);
                    sponsorData.GymOrganizationNo = Convert.ToString(reader["GymOrganizationNo"]);
                    sponsorData.GymAccountNo = Convert.ToString(reader["GymAccountNo"]);
                    sponsorData.FirstName = Convert.ToString(reader["FirstName"]);
                    sponsorData.LastName = Convert.ToString(reader["LastName"]);
                    sponsorData.Address = Convert.ToString(reader["Address"]);
                    sponsorData.SponsorAddress2 = Convert.ToString(reader["SponsorAddress2"]);
                    sponsorData.PostalAddress = Convert.ToString(reader["PostalAddress"]);
                    sponsorData.SponsorCountry = Convert.ToString(reader["SponsorCountry"]);
                    sponsorData.ArItemNo = Convert.ToInt32(reader["ArItemNo"]);
                    sponsorData.CustID = Convert.ToInt32(reader["CustID"]);
                    sponsorData.InvoiceGeneratedDate = Convert.ToString(reader["InvoiceGeneratedDate"]);
                    sponsorData.DueDate = Convert.ToString(reader["DueDate"]);
                    sponsorData.Reference = Convert.ToString(reader["Reference"]);
                    sponsorData.InvoiceAmount = Convert.ToDouble(reader["InvoiceAmount"]);
                    sponsorData.DiscountTotal = Convert.ToDouble(reader["DiscountTotal"]);
                    sponsorData.ContactPerson = Convert.ToString(reader["ContactPerson"]);
                    sponsorData.DicountCount = Convert.ToInt32(reader["DicountCount"]);
                    sponsorData.SponsorCount = Convert.ToInt32(reader["SponsorCount"]);
                    sponsorData.OriginalCustomer = Convert.ToString(reader["OriginalCustomer"]);
                    sponsorData.OrderText = Convert.ToString(reader["OrderText"]);
                    sponsorData.IsDisplayVisitNo = Convert.ToInt32(reader["IsDisplayVisitNo"]);
                    sponsorData.IsDisplayRemain = Convert.ToInt32(reader["IsDisplayRemain"]);
                    sponsorData.ID = Convert.ToString(reader["ID"]);
                    sponsorData.GymCompanyId = Convert.ToInt32(reader["GymCompanyId"]);
                    sponsorData.BranchID = Convert.ToString(reader["BranchID"]);
                    sponsorData.DocType = Convert.ToString(reader["DocType"]);
                    sponsorData.InvoiceID = Convert.ToInt32(reader["InvoiceID"]);

                }

            }
            catch (Exception)
            {


            throw;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }
            return sponsorData;
        }
    }
}




