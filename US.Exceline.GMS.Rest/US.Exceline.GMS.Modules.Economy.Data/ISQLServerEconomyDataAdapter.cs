﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.DomainObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Ccx;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR;
using US.GMS.Core.ResultNotifications;

namespace US.Exceline.GMS.Modules.Economy.Data
{
    interface ISQLServerEconomyDataAdapter
    {
        List<USPErrorPayment> GetErrorPaymentDetails(string errorPaymentType, DateTime fromDate, DateTime toDate, string GymCode, int branchid,string batchId=null);
        List<USPARItem> AdvanceInvoiceSearch(InvoiceSearchCriteria criteria, SearchModes searchMode, object constValue, string gymCode);
        List<USPARItem> GeneralInvoiceSearch(string searchValue, string secondValue, int suggest, int invoiceType, string fieldType, SearchModes searchMode, object constValue, string gymCode, int paymentID);
        List<USPApportionmentTransaction> GetInitialApportionmentData(string KID, string creditorNo, decimal amount, int ARNo, int caseNo, int paymentID, string paymentType, string gymCode);
        ReverseConditions ReverseApportionments(int paymentId, int arItem, string actionStatus, string user, string dbCode);
        int AddNotApportionmentTransactions(int ArNo, string KID, int errorpaymentID, string creditoNo, decimal mainAmount, int itemTypeID, List<USPApportionmentTransaction> transactionList, string errorType, DateTime voucherDate, string user, string dbCode);
        bool SendReminders(string GymCode, int branchID);
        List<Creditor> GetDDCreditors(int DDSt, int branchID, DateTime startDate, DateTime endDate, string gymcode);
        ApplicationSetting GetApplicationSetting(string gymCode);
        string GetATGTransactionID(string gymCode);
        DirectDeductInfo GetDirectDeduct(DateTime startDate, DateTime endDate, string creditorEntityID, int status, string gymCode, string sendingNo);
        bool UpdateStatus(List<DDHistory> historyEnt, string gymCode);
        int AddFileValidationSatatesRecord(string creditorNo, int fileID, int recordCounte, decimal sum, string recordTypeString, string message1, string message2, string applicaton, string gymCode);
        int AddFileStatus(int fileId, DateTime time, string status, string msg1, string msg2, string gymCode);
        Creditor GetCreditorInkassoIDbyAccountNumber(string accountNumber, string checkType, string gymCode);
        SortedList GetRecordTypes(string gymCode);
        int GetFileIDbyFileName(string fileNameWithoutPath, string gymCode);
        Creditor GetCreditorEntityRoleIDbyAccountNumber(string creditorAccountNumber, string checkType, string gymCode);
        void UpdateWarningType(string kid, string credAccountNo, int status, string gymCode);
        string UpdateDirectDeductCancellationStatus(string cancellationKID, string credAccountNo, string status, string gymCode);
        void UpdateOCRAllRecords(string fileName, string kid, string status, string creditorAccountNo, string gymCode);
        int SaveFileLog(string fileName, string folderPath, string user, string fileType, string gymCode);
        bool AddPaymentStatus(List<ImportStatus> payments, string gymCode);
        bool ImportOCRPayments(int fileID, string gymCode);
        bool AddCancellationStatus(List<Core.SystemObjects.OCR.CancellationStatus> statusList, string gymCode);
        bool ImportCancellations(string gymCode);
        List<InvoicingOrder> GetOrdersforInvoice(int branchID, DateTime? startDueDate, DateTime? endDueDate, string orderType, string gymCode);
        OperationResult<string> GenerateInvoiceswithOrders(List<InvoicingOrder> orderIds, string invoiceKey, string orderType, string user, string gymCode, int branchID);
        EstimatedInvoiceDetails GetEstimatedInvoiceDetails(int branchID, DateTime? startDueDate, DateTime? endDueDate, string gymCode);
        EstimatedInvoiceDetails GetInvoicedAmounts(int branchID, DateTime? startDueDate, DateTime? endDueDate, string gymCode);
        void AddProcessLog(List<string> messages, string user, int fileId, string gymCode);
        void AddImportHistory(Core.SystemObjects.OCR.OCRImportSummary oCRImportSummary, string user, string gymCode);
        List<OCRImportSummary> GetOCRImportHistory(DateTime? startDate, DateTime? endDate, string gymCode);
        int AddCreditorOrderLine(CreditorOrderLine OrderLine, string user, DbTransaction transaction);
        OperationResult<List<IUSPClaim>> SelectedOrderLineInvoiceGenerator(List<int> creditorOrdeLineIdList, string invoiceKey, string orderType, string user, DbTransaction transaction, int installmentID, int creditorNo, string gymCode);
        PaymentProcessResult RegisterTransaction(List<IUSPTransaction> transactionList, DbTransaction transaction);
        List<EconomyFileLogData> GetEconomyFileLogData(DateTime fromDate, DateTime toDate, string source, string filterBy, string gymCode);

        List<EconomyFileLogData> GetEconomyFileLogDetail(EconomyLogFileHelper economyLogFileHelper);
        int GetCcxEnabledStatus(int branchId, string gymCode);
        List<InvoiceDC> GetInvoiceList(string category, DateTime fromDate, DateTime toDate, int branchId, string gymCode);
        bool UpdatePDFFileParth(int id, string fileParth, int branchId, string gymCode, string user);

        int UpdateHistoryPDFPrintProcess(int id, int invoiceCount, int FailCount, int branchId, string user, string gymCode, string filePath);
        List<HistoryPDFPrint> GetHistoryPDFPrintDetail(DateTime fromDate, DateTime toDate, string gymCode);
        bool RemovePayment(int paymentID, int arItemNo, string user, string gymCode, string type);
        int MovePayment(int paymentID, int aritemno, string type, string user, string gymCode);
        void SetCancellationStatus(string sendingNo, string gymCode);

        List<PaymentImportLogDC> GetPaymentImportProcessLog(DateTime fromDate, DateTime toDate, string gymCode);

        List<ClassDetail> GetClassSalary(List<int> branchIdList, List<int> employeeIdList, List<int> categoryIdList,
                                         DateTime fromDate, DateTime toDate, string gymCode);

        List<ClassFileDetail> GetClassFileDetails(string gymCode);

        int SaveClassFileDetail(string fileName, string filePath, string user, string gymCode);
        int UpdateBulkPDFPrintDetails(int id, string docType, string dataIdList, string pdfFilePath, int noOfDoc, int failCount, int branchId, string user, string gymCode);
        List<BulkPDFPrint> GetBulkPDFPrintDetailList(string type, DateTime fromDate, DateTime toDate, int branchId, string gymCode);
        int UpdateCcxInvoice(List<Invoice> invoice);
        List<Invoice> GetCcxInvoiceDetail(int batchId);
        List<KeyData> GetKeyFromDB(String gymCode);
        int AddPreviousSignatureForNextTransaction(int SalePointID, int branchID, string signature,String gymCode);
        int FinalizeTransaction(int SalePointID, int branchID, string signature, String transDate, String transTime, int nr, decimal transAmntIn, decimal transAmntEx, String gymCode);
        String GetLastTransaction(int salePointID, int branchID, String gymCode);
        List<DataForLedger> ViewSkatteEtatenLedger(DateTime FromDate, DateTime ToDate, int branchID, String gymCode);
        List<KeyData> GetSalePointsByBranch(int branchId, String gymCode);
        InvoiceInfo GetInformationForInvoicePrint(String invoiceRef, int branchId, String gymCode);
        int InsertInvoicePrintRecord(int branchId, String invoiceRef, String xmlData, String hashValue, String gymCode);
        InvoiceXMLData GetInvoicePrintRecord(String invoiceRef, int branchId, String gymCode);
        double GetVatRateFromInvoiceNo(int invoiceNo, String gymCode);
        int GetGymCompanyId(String gymCode);
        void InsertInvoicePrintDiscrepancyInstance(DateTime timeStamp, String invoiceNo, String originalXml, String newXml, String gymCode);
        List<ExcelineSponsorMembersForPrint> ExcelineGetSponsoredMembersInvoicePrint(String sponsorARItemNo, String gymCode);
        ExcelineSponsorInfoForPrint ExcelineGetSponsorInfoForInvoicePrint(String sponsorARItemNo, String gymCode);
      
    }
}
