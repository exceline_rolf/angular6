﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.DomainObjects;

namespace US.Exceline.GMS.Modules.Economy.Data
{
    public class ClassFacade
    {
        private static ISQLServerEconomyDataAdapter GetDataAdapter()
        {
            return new SQLServerEconomyDataAdapter();
        }


        public static List<ClassDetail> GetClassSalary(List<int> branchIdList, List<int> employeeIdList, List<int> categoryIdList, DateTime fromDate, DateTime toDate, string gymCode)
        {
            try
            {
                return GetDataAdapter().GetClassSalary(branchIdList, employeeIdList, categoryIdList, fromDate, toDate, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<ClassFileDetail> GetClassFileDetails(string gymCode)
        {
            try
            {
                return GetDataAdapter().GetClassFileDetails(gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int SaveClassFileDetail(string fileName, string filePath, string user, string gymCode)
        {
            try
            {
                return GetDataAdapter().SaveClassFileDetail(fileName, filePath, user, gymCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
