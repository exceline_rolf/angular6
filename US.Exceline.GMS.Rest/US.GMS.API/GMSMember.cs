﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.ResultNotifications;
using US.GMS.BusinessLogic;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.DomainObjects.Common;

namespace US.GMS.API
{
    public class GMSMember
    {
        public static OperationResult<List<OrdinaryMemberDC>> GetMembersForCommonBooking(int branchId, string searchText, string user, string gymCode)
        {
            return MemberManager.GetMembersForCommonBooking(branchId, searchText, user, gymCode);
        }

        public static OperationResult<OrdinaryMemberDC> GetMembersById(int branchId, int memberId, string gymCode)
        {
            return MemberManager.GetMembersById(branchId, memberId, gymCode);
        }

        public static OperationResult<List<ActivityDC>> GetActivitiesForCommonBooking(int entityId, string entityType, int memberId, string gymCode)
        {
            return MemberManager.GetActivitiesForCommonBooking(entityId, entityType, memberId, gymCode);
        }

        public static OperationResult<bool> ManageMemberShopAccount(MemberShopAccountDC memberShopAccount, string mode, string user, int salePointId, int branchId, string gymCode)
        {
            return MemberManager.ManageMemberShopAccount(memberShopAccount, mode, user, branchId, salePointId, gymCode);
        }

        public static OperationResult<MemberShopAccountDC> GetMemberShopAccounts(int memberId, int hit, bool itemsNeeded, string user, string gymCode)
        {
            return MemberManager.GetMemberShopAccounts(memberId, hit, user, itemsNeeded, gymCode);
        }

        public static OperationResult<List<MemberShopAccountItemDC>> GetMemberShopAccountItems(int shopAccountId, string user, string gymCode, int branchId)
        {
            return MemberManager.GetMemberShopAccountItems(shopAccountId, user, gymCode, branchId);
        }

        public static OperationResult<int> CheckEmployeeByEntNo(int entityNO, string gymCode)
        {
            return MemberManager.CheckEmployeeByEntNo(entityNO, gymCode);
        }

        public static OperationResult<List<CountryDC>> GetCountryDetails(string gymCode)
        {
            return MemberManager.GetCountryDetails(gymCode);
        }

        public static OperationResult<List<LanguageDC>> GetLangugeDetails(string gymCode)
        {
            return MemberManager.GetLangugeDetails(gymCode);
        }

        public static OperationResult<List<EntityVisitDC>> GetVistsByDate(DateTime selectedDate, int branchId, string systemName, string gymCode, string user)
        {
            return MemberManager.GetVistsByDate( selectedDate,  branchId,  systemName,  gymCode, user);
        }

        public static OperationResult<int> SavePostalArea(string user, string postalCode, string postalArea, Int64 population, Int64 houseHold, string gymCode)
        {
            return MemberManager.SavePostalArea(user, postalCode, postalArea, population, houseHold,gymCode);
        }

        public static OperationResult<int> UpdateMemberStatus(string gymCode, int branchId)
        {
            return MemberManager.UpdateMemberStatus(gymCode, branchId);
        }

        public static OperationResult<List<TrainingProgramVisitDetailDC>> GetMemberVisitForTrainingProgram(string gymCode, int branchId, string systemName, DateTime lastUpdatedDateTime)
        {
            return MemberManager.GetMemberVisitForTrainingProgram(gymCode, branchId, systemName, lastUpdatedDateTime);
        }

        public static OperationResult<List<TrainingProgramClassVisitDetailDC>> GetMemberClassVisitForTrainingProgram(string gymCode, int branchId, string systemName, DateTime lastUpdatedDateTime)
        {
            return MemberManager.GetMemberClassVisitForTrainingProgram(gymCode, branchId, systemName, lastUpdatedDateTime);
        }

        public static OperationResult<bool> AddCountryDetails(CountryDC country, string gymCode)
        {
            return MemberManager.AddCountryDetails(country,gymCode);
        }

        public static OperationResult<bool> AddRegionDetails(RegionDC region, string gymCode)
        {
            return MemberManager.AddRegionDetails(region,gymCode);
        }

        public static OperationResult<MemberFeeGenerationResultDC> GenerateMemberFee(List<int> gyms, int month, string gymCode, string user)
        {
            return MemberManager.GenerateMemberFee(gyms, month, gymCode, user);
        }

        public static OperationResult<List<ExcelineMemberDC>> GetMembersForShop(int branchId, string searchText, int status, MemberRole memberRole, int hit, string gymCode)
        {
            return MemberManager.GetMembersForShop(branchId, searchText, status, memberRole, hit, gymCode);
        }

        public static OperationResult<string> GetAllMemberAndCompany(int branchId, string searchText, int status, int hit, string gymCode)
        {
            return MemberManager.GetAllMemberAndCompany(branchId, searchText, status, hit, gymCode);
        }

        public static OperationResult<bool> ValidateGenerateMemberFeeWithMemberFeeMonth(List<int> gyms, string gymCode, string user)
        {
            return MemberManager.ValidateGenerateMemberFeeWithMemberFeeMonth(gyms, gymCode, user);
        }

        public static OperationResult<List<string>> ConfirmationDetailForGenerateMemberFee(List<int> gyms, int month, string gymCode, string user)
        {
            return MemberManager.ConfirmationDetailForGenerateMemberFee(gyms, month, gymCode, user);
        }

        public static OperationResult<List<GymEmployeeDC>> GetEmployee(int aciveStatus, string gymCode)
        {
            return MemberManager.GetEmployee(aciveStatus, gymCode);
        }

        public static OperationResult<MemberShopAccountDC> GetMemberShopAccounts(int memberId, string entityType, int hit, bool itemsNeeded, string user, string gymCode)
        {
            return MemberManager.GetMemberShopAccounts(memberId, entityType, hit, user, itemsNeeded, gymCode);
        }
    }
}
