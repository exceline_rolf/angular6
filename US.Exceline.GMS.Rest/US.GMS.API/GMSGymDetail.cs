﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.BusinessLogic;
using US.GMS.Core.DomainObjects.Admin;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.SystemObjects;

namespace US.GMS.API
{
    public class GMSGymDetail
    {
        public static OperationResult<string> GetGymConnection(string gymCode)
        {
            return GymDetailHandler.GetGymConnection(gymCode);
        }

        public static OperationResult<List<BranchDetailsDC>> GetBranchDetails()
        {
             return GymDetailHandler.GetBranchDetails();
        }

        public static OperationResult<int> GetGymCompanyId(string gymCode)
        {
            return GymDetailHandler.GetGymCompanyId(gymCode);
        }


        public static OperationResult<List<BranchDetailsDC>> GetBranches(string gymCode)
        {
            return GymDetailHandler.GetBranches(gymCode);
        }

        public static OperationResult<List<string>> GetGymCodes()
        {
            return GymDetailHandler.GetGymCodes();
        }

        public static OperationResult<List<ExceUserBranchDC>> GetGymsforAccountNumber(string accountNumber, string gymCode)
        {
            return GymDetailHandler.GetGymsforAccountNumber(accountNumber,gymCode);
        }

        public static OperationResult<List<ExcelineRoleDc>> GetUserRole(string gymCode)
        {
            return GymDetailHandler.GetUserRole(gymCode);
        }
    }
}
