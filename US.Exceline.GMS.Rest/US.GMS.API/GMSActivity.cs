﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "6/28/2012 12:33:43 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.SystemObjects;
using US.GMS.BusinessLogic;

namespace US.GMS.API
{
    public class GMSActivity
    {
        public static OperationResult<List<ActivityDC>> GetActivitiesForEntity(int entityId, string entityType, string gymCode, int branchId)
        {
            return ActivityManager.GetActivitiesForEntity(entityId, entityType, gymCode, branchId);
        }
    }
}
