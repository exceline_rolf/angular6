﻿using System;
using System.Data;
using System.Data.Common;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class InfoGymUpdateExceMemberAction : USDBActionBase<bool>
    {
        private string _infoGymId = string.Empty;
        private int _memberId = -1;
        private string _user = string.Empty;

        public InfoGymUpdateExceMemberAction(string infoGymId, int memberId, string user)
        {
            _infoGymId = infoGymId;
            _memberId = memberId;
            _user = user;
        }

        protected override bool Body(DbConnection connection)
        {
            const string sp = "USExceGMSManageMembershipInfoGymUpdateExceMember";
            try
            {
                var cmd = CreateCommand(CommandType.StoredProcedure, sp);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@infoGymId", DbType.String, _infoGymId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
