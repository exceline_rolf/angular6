﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageContracts;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class SetContractSequenceIDAction :USDBActionBase<bool>
    {
        private readonly List<PackageDC> _contractList;
        private readonly int _branchID;
        private DataTable _dataTable;

        public SetContractSequenceIDAction(List<PackageDC> contractList, int branchID)
        {
            _contractList = contractList;
            _branchID = branchID;
            SequenceIDList();
        }

        protected override bool Body(DbConnection connection)
        {
            string StoredProcedureName = "USExceGMSAdminSetContractSequenceID";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchID", DbType.Int32, _branchID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@sequenceIDList", SqlDbType.Structured, _dataTable));
                cmd.ExecuteNonQuery();
                return true;
            }
            catch(Exception)
            {
                return false;
            }
        }

        private void SequenceIDList()
        {
            _dataTable = new DataTable();
            _dataTable.Columns.Add(new DataColumn("Id", Type.GetType("System.Int32")));
            _dataTable.Columns.Add(new DataColumn("Item", Type.GetType("System.String")));
            foreach(var item in _contractList)
            {
                DataRow dataTableRow = _dataTable.NewRow();
                dataTableRow["Id"] = item.PackageId;
                dataTableRow["Item"] = item.SortingNo;
                _dataTable.Rows.Add(dataTableRow);
            }
        }
    }
}
