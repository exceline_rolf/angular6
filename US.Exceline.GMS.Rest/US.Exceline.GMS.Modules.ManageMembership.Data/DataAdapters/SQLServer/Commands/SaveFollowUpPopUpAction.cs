﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    class SaveFollowUpPopUpAction : USDBActionBase<bool>
    {
        private int _followUpId;

        public SaveFollowUpPopUpAction(int FollowUpId)
        {
            this._followUpId = FollowUpId;

        }

        protected override bool Body(DbConnection connection)
        {
            //bool result = false;
            string StoredProcedureName = "USExceGMSManageMembershipSaveFollowUpPopUp";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@FollowUpId", DbType.Int32, _followUpId));
                cmd.ExecuteNonQuery();
                
                //result = true
                    return true;
            }

            catch (Exception ex)
            {
                throw ex;
                return false;
            }
            //return result;
        }
    }
}
