﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageContracts;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetContractsAction : USDBActionBase<List<PackageDC>>
    {
        private int _branchId;
        private int _contractType;

        public GetContractsAction(int branchId, int contractType)
        { 
            _branchId = branchId;
            _contractType = contractType;
        }

        protected override List<PackageDC> Body(System.Data.Common.DbConnection connection)
        {
            List<PackageDC> packageList = new List<PackageDC>();
            string StoredProcedureName = "USExceGMSManageMembershipGetContracts";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@contractType", DbType.Int32, _contractType));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    PackageDC package = new PackageDC();
                    package.ContractTypeValue = new US.GMS.Core.DomainObjects.Common.CategoryDC();
                    package.PackageId = Convert.ToInt32(reader["Id"]);
                    package.PackageName = reader["Name"].ToString();
                    package.BranchId = Convert.ToInt32(reader["BranchId"]);

                    package.ContractTypeValue.Id = Convert.ToInt32(reader["PackageTypeId"].ToString());
                    package.ContractTypeValue.Code = Convert.ToString(reader["PackageType"].ToString());
                    package.ContractTypeValue.Name = Convert.ToString(reader["PackageTypeName"].ToString());

                    package.StartDate = Convert.ToDateTime(reader["StartDate"]);
                    package.EndDate = Convert.ToDateTime(reader["EndDate"]);
                    package.PackagePrice = Convert.ToDecimal(reader["PackagePrice"]);
                    package.NumOfInstallments = Convert.ToInt32(reader["NumberOfInstallments"]);
                    package.RatePerInstallment = Convert.ToDecimal(reader["RatePerInstallment"]);
                    package.EnrollmentFee = Convert.ToDecimal(reader["EnrollmentFee"]);
                    package.NumOfVisits = Convert.ToInt32(reader["NumberOfVisits"]);
                    package.ActiveStatus = Convert.ToBoolean(reader["ActiveStatus"]);
                    package.ActivityId = Convert.ToInt32(reader["ActivityId"]);
                    package.ActivityName = Convert.ToString(reader["ActivityName"]);
                    package.BranchName = reader["BranchName"].ToString();
                    package.RestPlusMonth = Convert.ToBoolean(reader["RestPlusMonth"]);
                    package.LockInPeriod = Convert.ToInt32(reader["LockInPeriod"]);
                    package.PriceGuaranty = Convert.ToInt32(reader["PriceGuaranty"]);
                    package.AutoRenew = Convert.ToBoolean(reader["AutoRenew"]);
                    package.IsInvoiceDetail = Convert.ToBoolean(reader["InvoiceDetail"]);
                    package.ArticleNo = Convert.ToInt32(reader["ArticleId"]);
                    package.ArticleText = Convert.ToString(reader["ArticleText"]);
                    package.AccessProfileId = Convert.ToInt32(reader["AccessProfileId"]);
                    package.IsATG = Convert.ToBoolean(reader["IsATG"]);
                    if (reader["FixDateOfContract"] != DBNull.Value)
                        package.FixDateOfContract = Convert.ToDateTime(reader["FixDateOfContract"]);
                    if (reader["SecondDueDate"] != DBNull.Value)
                        package.SecondDueDate = Convert.ToDateTime(reader["SecondDueDate"]);
                    package.NextTemplateId = Convert.ToInt32(reader["NextTemplateNo"]);
                    package.OrderPrice = Convert.ToDecimal(reader["OrderPrice"]);
                    package.StartUpItemPrice = Convert.ToDecimal(reader["StartupItemPrice"]);
                    package.EveryMonthItemsPrice = Convert.ToDecimal(reader["EveryMonthItemPrice"]);
                    package.NextContractTemplateName = Convert.ToString(reader["NextTemplateName"]);
                    package.NoOfMonths = Convert.ToInt32(reader["NoOfMonths"]);
                    package.NoOfDays = Convert.ToInt32(reader["NoOfDays"]);
                    package.TemplateNumber = Convert.ToString(reader["TemplateNo"]);
                    package.MaxAge = Convert.ToInt32(reader["MaxAge"]);
                    if (reader["FirstDueDate"] != DBNull.Value)
                        package.FirstDueDate = Convert.ToDateTime(reader["FirstDueDate"]);
                    package.PackageCategory.Id = Convert.ToInt32(reader["PackateCatId"]);
                    package.PackageCategory.Name = Convert.ToString(reader["PackageCatName"]);
                    package.PackageCategory.Code = Convert.ToString(reader["PackageTypeCode"]);
                    if(reader["InStock"] != DBNull.Value)
                       package.InStock = Convert.ToInt32(reader["InStock"]);
                    package.CreditDueDays = Convert.ToInt32(reader["CreditDueDays"]);
                    if (reader["SecondDueDate"] != DBNull.Value)
                        package.SecondDueDate = Convert.ToDateTime(reader["SecondDueDate"]);
                    package.CreditDueDateSetting = Convert.ToInt32(reader["DuePaymentsDate"]);
                    package.UseTodayAsDueDate = Convert.ToBoolean(reader["useTodaysAsDueDate"]);
                    if (reader["StartDateOfContract"] != DBNull.Value)
                        package.FixStartDateOfContract = Convert.ToDateTime(reader["StartDateOfContract"]);
                    package.NextTemplateNo = Convert.ToString(reader["NextTemplateNo"]);
                    package.SortingNo = Convert.ToInt32(reader["Priority"]);
                    if (string.IsNullOrEmpty(package.NextTemplateNo) || package.NextTemplateNo.Trim().Equals("0"))
                        package.NextTemplateNo = string.Empty;
                    if (reader["AmountForATG"] != DBNull.Value)
                    package.AmountForATG = Convert.ToDecimal(reader["AmountForATG"]);
                    package.ServicePrice = Convert.ToDecimal(reader["ServiceAmount"]);
                    package.IsBookingActivity = Convert.ToBoolean(reader["IsBookingActivity"]);
                    package.IsBasicActivity = Convert.ToBoolean(reader["IsBasicActivity"]);
                    package.CreditPeriod = Convert.ToInt32(reader["CreditPeriod"]);
                    if (Convert.ToInt32(reader["MemberFeeArticleID"]) > 0)
                    {
                        ContractItemDC memberFeeArtilce = new ContractItemDC();
                        memberFeeArtilce.ArticleId = Convert.ToInt32(reader["MemberFeeArticleID"]);
                        memberFeeArtilce.Description = Convert.ToString(reader["MemberFeeArticleName"]);
                        memberFeeArtilce.Price = Convert.ToDecimal(reader["MemberFeePrice"]);
                        memberFeeArtilce.IsAvailableForGym = Convert.ToBoolean(reader["IsMemberFeeAvailable"]);
                        memberFeeArtilce.ItemName = memberFeeArtilce.Description;
                        package.MemberFeeArticle = memberFeeArtilce;
                        memberFeeArtilce.IsMemberFee = true;

                    }
                    else
                    {
                        ContractItemDC memberFeeArtilce = new ContractItemDC();
                        memberFeeArtilce.ArticleId = -1;
                        memberFeeArtilce.IsMemberFee = true;
                        memberFeeArtilce.IsAvailableForGym = false;
                        memberFeeArtilce.Price = 0;
                        memberFeeArtilce.Description = string.Empty;
                        memberFeeArtilce.ItemName = memberFeeArtilce.Description;
                        package.MemberFeeArticle = memberFeeArtilce;
                    }
                    package.PostPay = Convert.ToBoolean(reader["PostPay"]);
                    package.ContractConditionId = Convert.ToInt32(reader["ContractConditionID"]);
                    package.ContractCondition = Convert.ToString(reader["ContractCondition"]);
                    packageList.Add(package);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            
            return packageList;
        }
    }
}
