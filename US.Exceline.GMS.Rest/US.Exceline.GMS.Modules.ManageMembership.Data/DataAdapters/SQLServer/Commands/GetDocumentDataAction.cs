﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    class GetDocumentDataAction : USDBActionBase<List<DocumentDC>>
    {
        private string _searchText=string.Empty;
        private int _branchId=-1;
        private int _documentType=-1;
        private string _custId = string.Empty;
        private bool _isActive = false;

        public GetDocumentDataAction(bool isActive, string custId, string searchText, int documentType, int branchId)
        {
            _searchText = searchText;
            _documentType = documentType;
            _branchId = branchId;
            _custId = custId;
            _isActive = isActive;
        }

        protected override List<DocumentDC> Body(DbConnection connection)
        {
            var documentList = new List<DocumentDC>();
            const string spName = "USExceGMSManageMembershipGetDocumentData";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@searchText", System.Data.DbType.String, _searchText));
                command.Parameters.Add(DataAcessUtils.CreateParam("@documentType", System.Data.DbType.Int32, _documentType));
                command.Parameters.Add(DataAcessUtils.CreateParam("@branchId", System.Data.DbType.Int32, _branchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@UserId", System.Data.DbType.String, _custId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@IsActive", System.Data.DbType.String, _isActive));

                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    var doc = new DocumentDC
                        {
                            FileId          = Convert.ToInt32(reader["FileId"]),
                            FileName        = Convert.ToString(reader["FileName"]),
                            FilePath        = Convert.ToString(reader["FilePath"]),
                            UploadedUser    = Convert.ToString(reader["UploadedUser"]),
                            UploadedDate    = Convert.ToDateTime(reader["UploadedDate"]),
                            BranchId        = Convert.ToInt32(reader["BranchId"]),
                            CusId           = Convert.ToString(reader["UserId"]),
                            DocumentType    = Convert.ToInt32(reader["DocumentType"]),
                            DocumentCategory = Convert.ToString(reader["DocumentCategory"]),
                            ContractNo      = Convert.ToInt32(reader["ContractNo"])
                        };

                    documentList.Add(doc);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return documentList;
        }
    }
}
