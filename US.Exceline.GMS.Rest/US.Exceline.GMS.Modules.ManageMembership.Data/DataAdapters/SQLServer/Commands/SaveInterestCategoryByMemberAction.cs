﻿using System;
using System.Collections.Generic;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Common;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class SaveInterestCategoryByMemberAction : USDBActionBase<bool>
    {
        private readonly int _memberId = -1;
        private DataTable _dataTable;

        public SaveInterestCategoryByMemberAction(int memberId, int branchId, List<CategoryDC> interestCategoryList)
        {
            _memberId = memberId;
            if (interestCategoryList != null)
                FillDataToTable(interestCategoryList, memberId);
        }

        private DataTable FillDataToTable(List<CategoryDC> categoryList, int memberId)
        {
            _dataTable = new DataTable();
            _dataTable.Columns.Add(new DataColumn("MemberId", Type.GetType("System.Int32")));
            _dataTable.Columns.Add(new DataColumn("CategoryId", Type.GetType("System.Int32")));

            foreach (CategoryDC category in categoryList)
            {
                DataRow dataTableRow = _dataTable.NewRow();

                dataTableRow["MemberId"] = memberId;
                dataTableRow["CategoryId"] = category.Id;

                _dataTable.Rows.Add(dataTableRow);
            }
            return _dataTable;
        }

        protected override bool Body(DbConnection connection)
        {
            bool result;
            const string storedProcedureName = "USExceGMSManageMembershipSaveInterestCategoryByMember";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MemberId", DbType.Int32, _memberId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InterestedList", SqlDbType.Structured, _dataTable));
                cmd.ExecuteNonQuery();
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}
