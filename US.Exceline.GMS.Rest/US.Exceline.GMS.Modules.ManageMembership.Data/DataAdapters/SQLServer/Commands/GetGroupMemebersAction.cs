﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 6/11/2012 12:52:23 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageMemberships;
using System.Data.Common;
using System.Data;
using System.IO;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetGroupMemebersAction : USDBActionBase<List<OrdinaryMemberDC>>
    {
        private int _branchId = 0;
        private string _searchText = string.Empty;
        private bool _isActive = true;
        private int _groupId = -1;

        public GetGroupMemebersAction(int branchId, string searchText, bool IsActive, int groupId)
        {
            _branchId = branchId;
            _groupId = groupId;
            _searchText = searchText;
            _isActive = IsActive;
        }

        protected override List<OrdinaryMemberDC> Body(DbConnection connection)
        {
            List<OrdinaryMemberDC> ordinaryMemberLst = new List<OrdinaryMemberDC>();
            string StoredProcedureName = "USExceGMSManageMembershipGetGroupMembersByGroupId";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@searchText", DbType.String, _searchText));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@activeStatus", DbType.Boolean, _isActive));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@groupID", DbType.Int32, _groupId));
                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    OrdinaryMemberDC ordineryMember = new OrdinaryMemberDC();
                    ordineryMember.EntNo = Convert.ToInt32(reader["EntNo"]);
                    ordineryMember.Id = Convert.ToInt32(reader["MemberId"]);
                    ordineryMember.GroupId = Convert.ToInt32(reader["GroupId"]);
                    ordineryMember.ActiveStatus = Convert.ToBoolean(reader["ActiveStatus"]);
                    ordineryMember.BranchId = Convert.ToInt32(reader["BranchId"]);
                    ordineryMember.FirstName = reader["FirstName"].ToString();
                    ordineryMember.LastName = reader["LastName"].ToString();
                    ordineryMember.Name = ordineryMember.FirstName + " " + ordineryMember.LastName;
                    if (!string.IsNullOrEmpty(ordineryMember.ImagePath))
                    {
                        ordineryMember.ProfilePicture = GetMemberProfilePicture(ordineryMember.ImagePath);
                    }
                    ordinaryMemberLst.Add(ordineryMember);
                }
            }

            catch (Exception ex)
            {
                throw new NotImplementedException("", ex);
            }
            return ordinaryMemberLst;
        }

        private byte[] GetMemberProfilePicture(string p)
        {
            if (File.Exists(p))
            {
                byte[] byteArray = System.IO.File.ReadAllBytes(p);
                return byteArray;
            }
            return new byte[0];
        }

    }
}
