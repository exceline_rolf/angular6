﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageContracts;
using System.Data.Common;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetMemberContractItemsAction : USDBActionBase<List<ContractItemDC>>
    {
        private int _memberContractId = -1;
        private int _articleId = -1;
        public GetMemberContractItemsAction(int memberContractId, int articleId)
        {
            _memberContractId = memberContractId;
            _articleId = articleId;
        }
        protected override List<ContractItemDC> Body(System.Data.Common.DbConnection connection)
        {
            List<ContractItemDC> contractItems = new List<ContractItemDC>();
            string spName = "USExceGMSManageMembershipGetMemberContractItems";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@memberContractId", System.Data.DbType.Int32, _memberContractId));

                DbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ContractItemDC cItem = new ContractItemDC();
                    cItem.Id = Convert.ToInt32(reader["ID"]);
                    cItem.ItemName = Convert.ToString(reader["ItemName"]);
                    cItem.ArticleId = Convert.ToInt32(reader["ArticleId"]);
                    cItem.Price = Convert.ToDecimal(reader["Price"]);
                    cItem.CategoryId = Convert.ToInt32(reader["CategoryId"]);
                    cItem.IsSelected = true;
                    cItem.IsStartUpItem = Convert.ToBoolean(reader["IsStartupItem"]);
                    cItem.Quantity = Convert.ToInt32(reader["Units"]);
                    cItem.NumberOfOrders = Convert.ToInt32(reader["NoOfOrders"]);
                    cItem.UnitPrice = Convert.ToDecimal(reader["UnitPrice"]);
                    cItem.StartOrder = Convert.ToInt32(reader["StartOrder"]);
                    cItem.EndOrder = Convert.ToInt32(reader["EndOrder"]);
                    cItem.IsBookingArticle = Convert.ToBoolean(reader["IsBookingArticle"]);
                    cItem.ActivityID = Convert.ToInt32(reader["ActivityID"]);
                    cItem.IsMemberFee = Convert.ToBoolean(reader["IsMemberFee"]);

                    cItem.VoucherNo = Convert.ToString(reader["VoucherNo"]);

                    if (reader["VoucherExpireDate"] != DBNull.Value)
                        cItem.ExpiryDate = Convert.ToDateTime(reader["VoucherExpireDate"]);
                    cItem.CategoryCode = Convert.ToString(reader["CategoryCode"]);
                    if (_articleId == cItem.ArticleId)
                        cItem.IsActivityArticle = true;

                    contractItems.Add(cItem);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return contractItems;
        }
    }
}
