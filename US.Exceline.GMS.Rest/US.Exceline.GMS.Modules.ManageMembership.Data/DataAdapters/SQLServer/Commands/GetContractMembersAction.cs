﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetContractMembersAction : USDBActionBase<List<ExcelineMemberDC>>
    {
        private int _memberContractId = -1;
        public GetContractMembersAction(int memberContractId)
        {
            _memberContractId = memberContractId;
        }

        protected override List<ExcelineMemberDC> Body(System.Data.Common.DbConnection connection)
        {
            List<ExcelineMemberDC> memberList = new List<ExcelineMemberDC>();
            string spName = "USExceGMSManageMembershipGetContractMembers";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@memberContractId", System.Data.DbType.Int32, _memberContractId));
                DbDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    ExcelineMemberDC member = new ExcelineMemberDC();
                    member.Id = Convert.ToInt32(reader["MemberId"]);
                    member.Name = Convert.ToString(reader["Name"]);
                    member.Address1 = Convert.ToString(reader["Address1"]);
                    member.Gender = (US.GMS.Core.SystemObjects.Gender)Enum.Parse(typeof(US.GMS.Core.SystemObjects.Gender), reader["Gender"].ToString());
                    member.CustId = Convert.ToString(reader["CustId"]);
                    member.IntroduceDate = Convert.ToDateTime(reader["IntroduceDate"]);
                    member.Email = Convert.ToString(reader["Email"]);
                    member.Mobile = Convert.ToString(reader["Mobile"]);
                    if (reader["GroupStartDate"] != DBNull.Value)
                    member.GmStartDate = Convert.ToDateTime(reader["GroupStartDate"]);
                    if (reader["GroupEndDate"] != DBNull.Value)
                    member.GmEndDate = Convert.ToDateTime(reader["GroupEndDate"]); 
                    memberList.Add(member);
                }
                return memberList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
