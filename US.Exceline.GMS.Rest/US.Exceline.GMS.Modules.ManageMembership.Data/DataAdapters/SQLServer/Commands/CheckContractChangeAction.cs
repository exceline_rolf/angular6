﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class CheckContractChangeAction : USDBActionBase<int>
    {
        private int _memberID = -1;
        private int _memberContractId = -1;
        private int _oldABranchId = -1;
        private int _newBranchID = -1;

        public CheckContractChangeAction(int memberID, int memberContractID, int oldBranchID, int newBranchID)
        {
            _memberID = memberID;
            _memberContractId = memberContractID;
            _oldABranchId = oldBranchID;
            _newBranchID = newBranchID;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
           string spName = "USExceGMSManageMembershipCheckContractChange";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@MemberID", System.Data.DbType.Int32, _memberID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@MemberContractID", System.Data.DbType.Int32, _memberContractId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@OldBranchID", System.Data.DbType.Int32, _oldABranchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@NewBranchID", System.Data.DbType.Int32, _newBranchID));
               
                DbParameter outPara = new SqlParameter();
                outPara.DbType = System.Data.DbType.Int32;
                outPara.ParameterName = "@StatusCode";
                outPara.Direction = System.Data.ParameterDirection.Output;
                outPara.Value = 1;
                command.Parameters.Add(outPara);

                return Convert.ToInt32(outPara.Value);

            }catch(Exception)
            {
                throw;
            }

        }
    }
}
