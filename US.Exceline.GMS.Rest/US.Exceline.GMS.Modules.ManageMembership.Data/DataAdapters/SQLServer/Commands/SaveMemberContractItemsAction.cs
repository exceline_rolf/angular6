﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : AME
// Created Timestamp : "4/30/2012 14:38:32
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageContracts;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class SaveMemberContractItemsAction : USDBActionBase<int>
    {
        private int _memberContractId = -1;
        private bool _isStartUpItem = false;
        private int _branchId = -1;
        private ContractItemDC _contractItem;
        private string _user = string.Empty;

        public SaveMemberContractItemsAction(ContractItemDC item, int memberContractId, int branchId, bool isStrtUpItem, string user)
        {
            _memberContractId = memberContractId;
            _isStartUpItem = isStrtUpItem;
            _contractItem = item;
            _branchId = branchId;
            _user = user;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSManageMembershipSaveMemberContractItems";
            try
            {
                DbCommand command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@memberContractID", DbType.Int32, _memberContractId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@articleId", DbType.Int32, _contractItem.ArticleId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@IsStartupItem", DbType.Boolean, _isStartUpItem));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Price", DbType.Decimal, _contractItem.Price));
                command.Parameters.Add(DataAcessUtils.CreateParam("@units", DbType.Decimal, _contractItem.Quantity));
                command.Parameters.Add(DataAcessUtils.CreateParam("@noofOrders", DbType.Decimal, _contractItem.NumberOfOrders));
                command.Parameters.Add(DataAcessUtils.CreateParam("@StartOrder", DbType.Int32, _contractItem.StartOrder));
                if (_contractItem.EndOrder > 0)
                    command.Parameters.Add(DataAcessUtils.CreateParam("@EndOrder", DbType.Int32, _contractItem.EndOrder));
                command.Parameters.Add(DataAcessUtils.CreateParam("@branchID", DbType.Int32, _branchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@isbookingArticle", DbType.Boolean, _contractItem.IsBookingArticle));
                command.Parameters.Add(DataAcessUtils.CreateParam("@IsMemberFee", DbType.Boolean, _contractItem.IsMemberFee));
                command.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _user));
                command.Parameters.Add(DataAcessUtils.CreateParam("@VoucherNo ", DbType.String, _contractItem.VoucherNo));
                command.Parameters.Add(DataAcessUtils.CreateParam("@VoucherExpireDate ", DbType.DateTime, _contractItem.ExpiryDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@UnitPrice", DbType.Decimal, _contractItem.UnitPrice));
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return 1;
        }

        public int SaveContractItems(DbTransaction transaction)
        {
            string spName = "USExceGMSManageMembershipSaveMemberContractItems";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Connection = transaction.Connection;
                command.Transaction = transaction;
                command.Parameters.Add(DataAcessUtils.CreateParam("@memberContractID", DbType.Int32, _memberContractId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@articleId", DbType.Int32, _contractItem.ArticleId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@IsStartupItem", DbType.Boolean, _isStartUpItem));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Price", DbType.Decimal, _contractItem.Price));
                command.Parameters.Add(DataAcessUtils.CreateParam("@units", DbType.Decimal, _contractItem.Quantity));
                command.Parameters.Add(DataAcessUtils.CreateParam("@noofOrders", DbType.Decimal, _contractItem.NumberOfOrders));
                command.Parameters.Add(DataAcessUtils.CreateParam("@StartOrder", DbType.Int32, _contractItem.StartOrder));
                if (_contractItem.EndOrder > 0)
                    command.Parameters.Add(DataAcessUtils.CreateParam("@EndOrder", DbType.Int32, _contractItem.EndOrder));
                command.Parameters.Add(DataAcessUtils.CreateParam("@branchID", DbType.Int32, _branchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@isbookingArticle", DbType.Boolean, _contractItem.IsBookingArticle));
                command.Parameters.Add(DataAcessUtils.CreateParam("@IsMemberFee", DbType.Boolean, _contractItem.IsMemberFee));
                command.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _user));
                command.Parameters.Add(DataAcessUtils.CreateParam("@VoucherNo ", DbType.String, _contractItem.VoucherNo));
                command.Parameters.Add(DataAcessUtils.CreateParam("@VoucherExpireDate ", DbType.DateTime, _contractItem.ExpiryDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@UnitPrice", DbType.Decimal, _contractItem.UnitPrice));
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return 1;
        }
    }
}
