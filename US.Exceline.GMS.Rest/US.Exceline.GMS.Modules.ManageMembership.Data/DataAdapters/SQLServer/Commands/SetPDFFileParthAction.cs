﻿using System;
using System.Data;
using System.Data.Common;
using System.Linq;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    class SetPDFFileParthAction : USDBActionBase<bool>
    {
        private int _id             = -1;
        private string _fileParth   = string.Empty;
        private string _flag        = string.Empty;
        private int _branchId = -1;
        private string _user = string.Empty;

        public SetPDFFileParthAction(int id, string fileParth, string flag, int branchId, string user)
        {
            _id = id;
            _fileParth = fileParth;
            _flag = flag;
            _branchId = branchId;
            _user = user;
        }

        protected override bool Body(DbConnection connection)
        {
            string StoredProcedureName = "USC_SetPDFFileParth";
            try
            {
                if (!string.IsNullOrEmpty(_fileParth))
                {
                    string[] tempList = _fileParth.Split('\\');
                    string fileName = tempList.Last();

                    DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);

                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@id", DbType.Int32, _id));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@fileParth", DbType.String, _fileParth));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@flag", DbType.String, _flag));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@fileName", DbType.String, fileName));
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }
    }
}
