﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageMemberships;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetContractFreezeItemsAction : USDBActionBase<List<ContractFreezeItemDC>>
    {
        private int _memberContractId;

        public GetContractFreezeItemsAction(int memberContractId)
        {
            this._memberContractId = memberContractId;
        }

        protected override List<ContractFreezeItemDC> Body(DbConnection connection)
        {
            List<ContractFreezeItemDC> freezeItemList = new List<ContractFreezeItemDC>();
            string StoredProcedureName = "USExceGMSManageMembershipGetFreezeContractDetails";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberContractId", DbType.Int32, _memberContractId));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ContractFreezeItemDC freezeItem = new ContractFreezeItemDC
                    {
                        Id = Convert.ToInt32(reader["Id"]),
                        MemberContractid = Convert.ToInt32(reader["MemberContractId"]),
                        MemberId = Convert.ToInt32(reader["MemberId"]),
                        CategoryId = Convert.ToInt32(reader["CategoryId"]),
                        Category = reader["CategoryName"].ToString(),
                        FromDate = Convert.ToDateTime(reader["FromDate"]),
                        ToDate = Convert.ToDateTime(reader["ToDate"]),
                        Note = reader["Note"].ToString(),
                        FreezeDays = Convert.ToInt32(reader["FreezeDays"]),
                        ShiftCount = Convert.ToInt32(reader["ShiftCount"]),
                        ShiftStartInstallmentId = Convert.ToInt32(reader["ShiftStartInstallmentId"]),
                        ActiveStatus = Convert.ToBoolean(reader["ActiveStatus"]),
                        FreezeMonths = Convert.ToInt32(reader["FreezeMonths"]),
                        IsExtended = Convert.ToBoolean(reader["ExtendedStatus"]),
                        ContractEndDate = Convert.ToDateTime(reader["ContractEndDate"])
                    };
                    freezeItemList.Add(freezeItem);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return freezeItemList;
        }
    }
}
