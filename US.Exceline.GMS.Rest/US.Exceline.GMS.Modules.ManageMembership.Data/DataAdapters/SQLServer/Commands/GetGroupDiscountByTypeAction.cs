﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 5/7/2012 4:19:11 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageContracts;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetGroupDiscountByTypeAction : USDBActionBase<List<DiscountDC>>
    {
        private int _branchId = 0;
        private int _sponsorId = 0;
        private int _discountTypeId = -1;


        public GetGroupDiscountByTypeAction(int branchId, string user, int discountTypeId, int sponsorId)
        {
            _branchId = branchId;
            _sponsorId = sponsorId;
            _discountTypeId = discountTypeId;
        }

        protected override List<DiscountDC> Body(DbConnection connection)
        {
            List<DiscountDC> _discountCategoryList = new List<DiscountDC>();
            string StoredProcedureName = "USExceGMSManageMembershipGetGroupDiscountByType";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchID", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@sponsorId", DbType.Int32, _sponsorId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@typeId", DbType.Int32, _discountTypeId));


                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    DiscountDC _discountcategory = new DiscountDC();
                    _discountcategory.Name = Convert.ToString(reader["Name"]);
                    _discountcategory.SponsorId = _sponsorId;
                    _discountcategory.GroupDiscountName = Convert.ToString(reader["Name"]);
                    _discountcategory.Id = Convert.ToInt32(reader["ID"]);
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["MinMembersCount"]).Trim()))
                    {
                        _discountcategory.MinMemberNo = Convert.ToInt32(reader["MinMembersCount"]);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["Discount"]).Trim()))
                    {
                        _discountcategory.DiscountAmount = Convert.ToDecimal(reader["Discount"]);
                    }

                    /* Adding: IsActive */
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["IsActive"]).Trim()))
                    {
                        _discountcategory.IsActive = Convert.ToBoolean(reader["IsActive"]);
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(reader["MinPayment"]).Trim()))
                    {
                        _discountcategory.MinPayment = Convert.ToDecimal(reader["MinPayment"]);
                    }

                    if (Convert.ToString(reader["LowMode"]) == "FIXEDAMOUNT")
                    {
                        _discountcategory.LowAmount = Convert.ToDecimal(reader["LowDiscount"]);
                        _discountcategory.IsLowAmoutEnable = true;
                    }
                    else //if (Convert.ToString(reader["LowMode"]) == "PERCENTATGE")
                    {
                        _discountcategory.LowPercentage = Convert.ToDecimal(reader["LowDiscount"]);
                        _discountcategory.IsLowPercentageEnable = true;
                    }
                    if (Convert.ToString(reader["HighMode"]) == "PERCENTATGE")
                    {
                        _discountcategory.HighPercentage = Convert.ToDecimal(reader["HighDiscout"]);
                        _discountcategory.IsHighPercentageEnable = true;
                    }
                    else //if (Convert.ToString(reader["HighMode"]) == "FIXEDAMOUNT")
                    {
                        _discountcategory.HighAmount = Convert.ToDecimal(reader["HighDiscout"]);
                        _discountcategory.IsHighAmoutEnable = true;
                    }
                    _discountcategory.PercentageModeId = Convert.ToInt32(reader["PercentageModeId"]);
                    _discountcategory.AmountModeId = Convert.ToInt32(reader["AmountModeId"]);
                    if (_discountcategory.Id > 0)
                    {
                        _discountCategoryList.Add(_discountcategory);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _discountCategoryList;
        }
    }
}
