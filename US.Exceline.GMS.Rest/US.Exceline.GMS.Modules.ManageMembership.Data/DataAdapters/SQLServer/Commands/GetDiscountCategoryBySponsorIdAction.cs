﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetDiscountCategoryBySponsorIdAction : USDBActionBase<string>
    {
        private int _branchId = -1;
        private int _sponsorId = -1;
        public GetDiscountCategoryBySponsorIdAction(int branchId, int sponsorId)
        {
            _branchId = branchId;
            _sponsorId = sponsorId;
        }

        protected override string Body(DbConnection connection)
        {
            string disCountCategory = string.Empty;
            const string storedProcedureName = "USExceGMSManageMembershipGetDiscountCategoryBySponsorId";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@sponsorId", DbType.Int32, _sponsorId));

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    disCountCategory = Convert.ToString(reader["CategoryName"]);

                }

                return disCountCategory;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
