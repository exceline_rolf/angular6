﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class AddBlancInstallmentAction : USDBActionBase<int>
    {
        private string _user = string.Empty;
        private int _memberId;
        private int _branchId;

        public AddBlancInstallmentAction(int MemberId, int BranchId, string GymCode, string user)
        {
            _memberId = MemberId;
            _branchId = BranchId;
            _user = user;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            string spName = "USExceGMSManageMembershipAddBlancInstallment";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MemberId", SqlDbType.Int, _memberId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", SqlDbType.Int, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", SqlDbType.VarChar, _user));
                cmd.ExecuteNonQuery();
                return 1;
            }
            catch
            {
                throw;
            }
        }
    }
}
