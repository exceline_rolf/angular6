﻿using System;
using System.Data;
using System.Data.Common;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class SaveSponsorAction : USDBActionBase<bool>
    {
        private readonly int _memberId;
        private readonly int _sponsorId;

        public SaveSponsorAction(int memberId, int sponsorId)
        {
            _memberId = memberId;
            _sponsorId = sponsorId;
        }

        protected override bool Body(DbConnection connection)
        {
            bool result;
            const string storedProcedureName = "USExceGMSManageMembershipSaveSponsor";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberId));
                if (_sponsorId != -1)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@sponsorId", DbType.Int32, _sponsorId));
                cmd.ExecuteNonQuery();
                result = true;
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }


        public bool SaveSponsor(DbTransaction transaction)
        {
            bool result;
            const string storedProcedureName = "USExceGMSManageMembershipSaveSponsor";

            try
            {
                DbCommand command = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                command.Connection = transaction.Connection;
                command.Transaction = transaction;

                command.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberId));
                if (_sponsorId != -1)
                    command.Parameters.Add(DataAcessUtils.CreateParam("@sponsorId", DbType.Int32, _sponsorId));
                command.ExecuteNonQuery();
                result = true;
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}
