﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetCreditNotePaymentSummaryAction : USDBActionBase<List<InvoicePaymentSummaryDC>>
    {
        private readonly int _arItemNo;
        public GetCreditNotePaymentSummaryAction(int arItemNo)
        {
            _arItemNo = arItemNo;
        }

        protected override List<InvoicePaymentSummaryDC> Body(DbConnection connection)
        {
            const string spName = "USExceGMSManageMembershipGetCreditNotePaymentSummary";
            DbDataReader reader = null;
            var payments = new List<InvoicePaymentSummaryDC>();
            try
            {
                DbCommand command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@ARItemNo", DbType.Int32, _arItemNo));
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    var pSummary = new InvoicePaymentSummaryDC();

                    pSummary.Id = Convert.ToInt32(reader["ARItemNo"]);

                    if (reader["Amount"] != DBNull.Value)
                        pSummary.PaymentAmount = Convert.ToDecimal(reader["Amount"]);

                    if (reader["PaymentDate"] != DBNull.Value)
                        pSummary.PaymentDate = Convert.ToDateTime(reader["PaymentDate"]);

                    if (reader["PaymentMode"] != DBNull.Value)
                        pSummary.Type = Convert.ToString(reader["PaymentMode"]);
                        
                    payments.Add(pSummary);
                }

             
                reader.Close();
                return payments;
            }
            catch
            {
                if (reader != null)
                    reader.Close();
                throw;
            }
        }
    }
}
