﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.ManageContracts;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetEmployeeCategoryBySponsorIdAction : USDBActionBase<List<EmployeeCategoryDC>>
    {
        private int _branchId = -1;
        private int _sponsorId = -1;

       public GetEmployeeCategoryBySponsorIdAction(int branchId, int sponsorId)
       {
           _branchId = branchId;
           _sponsorId = sponsorId;
       }

       protected override List<EmployeeCategoryDC> Body(DbConnection connection)
       {
           var employeeCategoryList = new List<EmployeeCategoryDC>();
           const string storedProcedureName = "USExceGMSManageMembershipGetEmployeeCategoryBySponsorId";

           try
           {
               DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);

               cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
               cmd.Parameters.Add(DataAcessUtils.CreateParam("@sponsorId", DbType.Int32, _sponsorId));

               DbDataReader reader = cmd.ExecuteReader();
               while (reader.Read())
               {

                   var employeeCategory = new EmployeeCategoryDC
                                              {
                                                  Name = Convert.ToString(reader["CategoryName"]),
                                                  Id   = Convert.ToInt32(reader["CategoryId"])
                                              };

                  

                   employeeCategoryList.Add(employeeCategory);

               }

               return employeeCategoryList;
           }
           catch (Exception ex)
           {
               throw ex;
           }


       }
    }
}
