﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using US.GMS.Core.DomainObjects.AccessControl;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetmemberDetailsForCardAction : USDBActionBase<List<ExceACCMember>>
    {
        private readonly string _cardNo = string.Empty;
        private readonly string _accessType = string.Empty;
        public GetmemberDetailsForCardAction(string cardNo, string accesType)
        {
            _cardNo = cardNo;
            _accessType = accesType;
        }
        protected override List<ExceACCMember> Body(DbConnection connection)
        {
            List<ExceACCMember> memberList = new List<ExceACCMember>();
            string spName = "USExceGMSGetMemberDetailsForCard";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@CardNo", System.Data.DbType.String, _cardNo));
                command.Parameters.Add(DataAcessUtils.CreateParam("@AccessType", System.Data.DbType.String, _accessType));
                DbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ExceACCMember accessMember = new ExceACCMember();
                    accessMember.Id = Convert.ToInt32(reader["ID"]);
                    accessMember.MemberCardNo = Convert.ToString(reader["MemberCardNo"]);
                    accessMember.Name = Convert.ToString(reader["Name"]);
                    accessMember.CustId = Convert.ToString(reader["CustId"]);
                    accessMember.HomeGym = Convert.ToString(reader["HomeGym"]);
                    accessMember.Gender = Convert.ToString(reader["Gender"]);
                    if (reader["Born"] != DBNull.Value)
                        accessMember.Born = Convert.ToDateTime(reader["Born"]).ToString("yyyy.MM.dd");
                    if (reader["LastVisitDate"] != DBNull.Value)
                        accessMember.LastVisitDate = Convert.ToDateTime(reader["LastVisitDate"]).ToString("yyyy.MM.dd");
                    accessMember.CreditBalance = Convert.ToDecimal(reader["CreditBalance"]);
                    accessMember.GuestCardNo = Convert.ToString(reader["GuestCardNo"]);
                    accessMember.MemberContractNo = Convert.ToString(reader["MemberContractNo"]);
                    accessMember.MemberContractId = Convert.ToInt32(reader["memberContractId"]);
                    if (reader["ContractStartDate"] != DBNull.Value)
                        accessMember.ContractStartDate = Convert.ToDateTime(reader["ContractStartDate"]).ToString("yyyy.MM.dd");
                    if (reader["ContractEndDate"] != DBNull.Value)
                        accessMember.ContractEndDate = Convert.ToDateTime(reader["ContractEndDate"]).ToString("yyyy.MM.dd");

                    accessMember.TemplateNo = Convert.ToString(reader["TemplateNo"]);
                    accessMember.TemplateName = Convert.ToString(reader["TemplateName"]);
                    accessMember.ContractType = Convert.ToString(reader["ContractType"]);
                    accessMember.AvailableVisits = Convert.ToInt32(reader["AvailableVisits"]);
                    accessMember.AccessProfileId = Convert.ToInt32(reader["AccessProfileID"]);
                    accessMember.ShopAccBalance = Convert.ToDecimal(reader["AccountBalance"]);
                    if (reader["AntiDopingSignedDate"] != DBNull.Value)
                        accessMember.AntiDopingDate = Convert.ToDateTime(reader["AntiDopingSignedDate"]).ToString("yyyy.MM.dd");
                    accessMember.IsFreezed = Convert.ToString(reader["IsFreezed"]);
                    accessMember.IsContractATG = Convert.ToString(reader["IsContractATG"]);
                    accessMember.MemberATGStatus = Convert.ToString(reader["MemberATGStatus"]);
                    accessMember.ActivityId = Convert.ToInt32(reader["ActivityId"]);
                    accessMember.MemberMessages = Convert.ToString(reader["MemberMessages"]);
                    memberList.Add(accessMember);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return memberList;
        }
    }
}
