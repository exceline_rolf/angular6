﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Data;
using US_DataAccess;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class RegisterChangeOfInvoiceDueDateAction : USDBActionBase<bool>
    {
        private AlterationInDueDateData _changeData = new AlterationInDueDateData();
        private string _user = string.Empty;

        public RegisterChangeOfInvoiceDueDateAction(AlterationInDueDateData changeData, String user)
        {
            _changeData = changeData;
            _user = user;
        }

        protected override bool Body(DbConnection connection)
        {
            bool result = false;
            string StoredProcedureName = "US_AddNotification";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Title", DbType.String, _changeData.Title));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Description", DbType.String, _changeData.Description));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreatedUser", DbType.String, _user));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Role", DbType.String, _changeData.RoleId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TypeId", DbType.Int32, _changeData.TypeId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _changeData.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SeverityId", DbType.Int32, _changeData.SeverityId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _changeData.MemberId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@LastAttendDate", DbType.DateTime, _changeData.LastAttendDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Status", DbType.Int32, _changeData.StatusId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@RecordType", DbType.String, _changeData.RecordType));
                cmd.ExecuteNonQuery();
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}









