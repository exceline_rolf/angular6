﻿using System;
using System.Data;
using System.Data.Common;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class DeleteSMSNotificationsAction : USDBActionBase<bool>
    {
        private int _memberVisitId;
        private int _memberContractId;

        public DeleteSMSNotificationsAction(int smsId, int memberId)
        {
            _memberVisitId = smsId;
            _memberContractId = memberId;
        }

        protected override bool Body(DbConnection connection)
        {
            bool result;
            const string storedProcedureName = "USExceGMSManageMembershipDeleteMemberVisit";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@entityVisitId", DbType.Int32, _memberVisitId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberContractId", DbType.Int32, _memberContractId));
                //cmd.Parameters.Add(DataAcessUtils.CreateParam("@code", DbType.Int32, _memberContractId)
                cmd.ExecuteNonQuery();
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}
