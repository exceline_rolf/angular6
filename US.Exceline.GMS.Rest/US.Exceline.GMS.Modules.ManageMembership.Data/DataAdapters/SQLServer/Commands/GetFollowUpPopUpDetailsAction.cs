﻿
using System;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageMemberships;
using System.Data.Common;
using System.Data;
using US.GMS.Core.SystemObjects;
using System.IO;
using US.GMS.Data.DataAdapters.SQLServer.Commands;
using US.GMS.Core.DomainObjects.Common;
using System.Collections.Generic;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
   public class GetFollowUpPopUpDetailsAction : USDBActionBase<List<FollowUpDetailDC>>
    {
       private int _memberId;


       public GetFollowUpPopUpDetailsAction(int memberId)
       {
           _memberId = memberId;

       }

       protected override List<FollowUpDetailDC> Body(DbConnection connection)
        {
            List<FollowUpDetailDC> followUpPopUpDetailsList = new List<FollowUpDetailDC>();
            const string storedProcedureName = "USExceGMSGetFollowUpPopUpDetailsByMemberId";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberId));
                
               
                DbDataReader reader = cmd.ExecuteReader();

            
                while (reader.Read())
                {
                    FollowUpDetailDC exeTask = new FollowUpDetailDC();
                    exeTask.FollowUpId = Convert.ToInt32(reader["FollowUpId"]);
                    exeTask.Id = Convert.ToInt32(reader["Id"]);

                    exeTask.Description = reader["Description"].ToString();

                    exeTask.Comment = reader["Comment"].ToString();

                    exeTask.Name = reader["Name"].ToString();
   
                    exeTask.IsPopUp = Convert.ToBoolean(reader["IsPopUp"]);

                    followUpPopUpDetailsList.Add(exeTask);

                }
                reader.Close();

                return followUpPopUpDetailsList;

            }

            catch (Exception ex)
            {
                throw new NotImplementedException("", ex);
            }
            return new List<FollowUpDetailDC>();
        }


       
    }
}
