﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using US.GMS.Core.DomainObjects.ManageMemberships;
using System.Data;
using System.Data.SqlClient;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class UnfreezeContractAction : USDBActionBase<int>
    {
        private ContractFreezeItemDC _freezeItem;
        private string _user;

        public UnfreezeContractAction(ContractFreezeItemDC freezeItem, List<ContractFreezeInstallmentDC> unfreezeInstallments, string user)
        {
            this._freezeItem = freezeItem;
            this._user = user;
        }

        protected override int Body(DbConnection connection)
        {
            string StoredProcedureName = "USExceGMSManageMembershipUnfreezeContractInstallments";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberContractId", DbType.Int32, _freezeItem.MemberContractid));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@freezeItemId", DbType.Int32, _freezeItem.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@fromDate", DbType.DateTime,_freezeItem.FromDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@toDate", DbType.DateTime, _freezeItem.ToDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@user", DbType.String, _user));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@note", DbType.String, _freezeItem.Note));

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@freezeDays", DbType.Int32, _freezeItem.FreezeDays));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@unfreezeDays", DbType.Int32, _freezeItem.UnfreezeDays));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@freezeCount", DbType.Int32, _freezeItem.ShiftCount));

                DataTable freezeInstallmentsTable = new DataTable();
                DataColumn col = null;
                col = new DataColumn("FreezeInstallmentId", typeof(Int32));
                freezeInstallmentsTable.Columns.Add(col);
                col = new DataColumn("InstallmentId", typeof(Int32));
                freezeInstallmentsTable.Columns.Add(col);
                col = new DataColumn("NewAmount", typeof(decimal));
                freezeInstallmentsTable.Columns.Add(col);
                col = new DataColumn("AmountDifference", typeof(decimal));
                freezeInstallmentsTable.Columns.Add(col);

                SqlParameter parameter = new SqlParameter();
                parameter.ParameterName = "@unfreezeInstallments";
                parameter.SqlDbType = System.Data.SqlDbType.Structured;
                parameter.Value = freezeInstallmentsTable;
                cmd.Parameters.Add(parameter);

                object obj = cmd.ExecuteScalar();

                int freezeItemId = Convert.ToInt32(obj);
                return freezeItemId;
            } 
            catch
            {
                return -1;
            }
        }
    }
}
