﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetOtherMembersForBookingAction : USDBActionBase<List<ExcelineMemberDC>>
    {
        private int _scheduleItemId;

        public GetOtherMembersForBookingAction(int scheduleItemId)
        { 
            _scheduleItemId = scheduleItemId;
        
        }

        protected override List<ExcelineMemberDC> Body(System.Data.Common.DbConnection connection)
        {
            string storedProcedure = "USGMSManageMembershipGetOtherMembersForBooking";
            List<ExcelineMemberDC> memberList = new List<ExcelineMemberDC>();
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ScheduleItemId", System.Data.DbType.Int32, _scheduleItemId));
                 DbDataReader reader = cmd.ExecuteReader();
                 while (reader.Read())
                 {
                     ExcelineMemberDC member = new ExcelineMemberDC();
                     member.Id = Convert.ToInt32(reader["ID"]);
                     member.Name = Convert.ToString(reader["Name"]);
                     memberList.Add(member);
                 }            
            }
            catch 
            {
                throw;
            }
            return memberList;
        }


    }
}
