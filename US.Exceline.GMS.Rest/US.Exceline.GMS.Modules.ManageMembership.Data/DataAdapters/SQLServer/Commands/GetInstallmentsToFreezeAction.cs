﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;
using System.Data.Common;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetInstallmentsToFreezeAction:USDBActionBase<List<InstallmentDC>>
    {
        private int _memberContractId = -1;
        public GetInstallmentsToFreezeAction(int memberContractId)
        {
            _memberContractId = memberContractId;
        }

        protected override List<InstallmentDC> Body(DbConnection connection)
        {
            List<InstallmentDC> installmentsList = new List<InstallmentDC>();
            string spName = "USExceGMSManageMembershipGetInstallmentsToFreeze";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@memberContractId", System.Data.DbType.Int32, _memberContractId));

                DbDataReader reader = command.ExecuteReader();
                int index = 0;
                while (reader.Read())
                {
                    InstallmentDC installment = new InstallmentDC();
                    installment.Id = index;
                    installment.TableId = Convert.ToInt32(reader["ID"]);
                    installment.MemberId = Convert.ToInt32(reader["MemberId"]);
                    installment.MemberContractId = Convert.ToInt32(reader["MemberContractId"]);
                    installment.Text = Convert.ToString(reader["Text"]);
                    installment.InstallmentDate = Convert.ToDateTime(reader["InstallmentDate"]);
                    installment.DueDate = Convert.ToDateTime(reader["DueDate"]);
                    if (reader["PaidDate"] != DBNull.Value)
                        installment.PaidDate = Convert.ToDateTime(reader["PaidDate"]);                   
                    installment.Amount = Convert.ToDecimal(reader["Amount"]);
                    if (reader["BillingDate"] != DBNull.Value)
                        installment.BillingDate = Convert.ToDateTime(reader["BillingDate"]);
                    if (reader["DebtCollectionDate"] != DBNull.Value)
                        installment.DebtCollectionDate = Convert.ToDateTime(reader["DebtCollectionDate"]);
                    if (reader["PrintedDate"] != DBNull.Value)
                        installment.PrintedDate = Convert.ToDateTime(reader["PrintedDate"]);
                    if (reader["Submitted"] != DBNull.Value)
                        installment.Submitted = Convert.ToString(reader["Submitted"]);
                    if(reader["BillingFee"] != DBNull.Value)
                        installment.BillingFee=Convert.ToDecimal(reader["BillingFee"]);
                    if(reader["ReminderFee"] != DBNull.Value)
                        installment.ReminderFee=Convert.ToDecimal(reader["ReminderFee"]);
                    if(reader["AddOnPrice"] != DBNull.Value)
                        installment.AdonPrice=Convert.ToDecimal(reader["AddOnPrice"]);
                    if(reader["Discount"] != DBNull.Value)
                        installment.Discount=Convert.ToDecimal(reader["Discount"]);
                    if(reader["SponsoredAmount"] != DBNull.Value)
                        installment.SponsoredAmount=Convert.ToDecimal(reader["SponsoredAmount"]);
                    if(reader["NumberOfReminders"] != DBNull.Value)
                        installment.NumberOfReminders=Convert.ToInt32(reader["NumberOfReminders"]);
                    if(reader["PriceListItem"] != DBNull.Value)
                        installment.PriceListItem=reader["PriceListItem"].ToString();
                    if(reader["Comment"] != DBNull.Value)
                        installment.Comment=reader["Comment"].ToString();
                    if(reader["OriginalMatuarity"] != DBNull.Value)
                        installment.OriginalDueDate=Convert.ToDateTime(reader["OriginalMatuarity"]);
                    if(reader["LastReminderDate"] != DBNull.Value)
                        installment.LastReminderDate=Convert.ToDateTime(reader["LastReminderDate"]);
                    installment.KID = Convert.ToString(reader["KID"]);
                    if (reader["TrainingPeriodStart"] != DBNull.Value)
                        installment.TrainingPeriodStart = Convert.ToDateTime(reader["TrainingPeriodStart"]);
                    if (reader["TrainingPeriodEnd"] != DBNull.Value)
                        installment.TrainingPeriodEnd = Convert.ToDateTime(reader["TrainingPeriodEnd"]);
                    installment.CreditPeriod = Convert.ToInt32(reader["CreditPeriod"]);
                    installment.MemberCreditPeriod = Convert.ToInt32(reader["MemberCreditPeriod"]);

                    installmentsList.Add(installment);
                    index++;
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return installmentsList;
        }
    }
}
