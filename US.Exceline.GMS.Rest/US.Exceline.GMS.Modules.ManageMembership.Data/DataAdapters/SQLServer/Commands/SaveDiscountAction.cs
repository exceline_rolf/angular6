﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 5/8/2012 5:39:04 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US.GMS.Core.DomainObjects.Admin;
using US.GMS.Core.DomainObjects.ManageContracts;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class SaveDiscountAction : USDBActionBase<int>
    {
        private readonly List<DiscountDC> _discountList = new List<DiscountDC>();
        private readonly string _user = string.Empty;
        readonly int _branchId = -1;
        private readonly int _contractSettingId = -1;
        private DiscountDC _discount = new DiscountDC();
        private bool _isSponsorShip = false;

        public SaveDiscountAction(List<DiscountDC> discountList, string user, int branchId, int contractSettingId)
        {
            _discountList = discountList;
            _branchId = branchId;
            _user = user;
            _contractSettingId = contractSettingId;
        }


        public SaveDiscountAction(string user, int branchId, int contractSettingId, DiscountDC discount, bool isSponsorShip)
        {
            _branchId = branchId;
            _isSponsorShip = isSponsorShip;
            _user = user;
            _contractSettingId = contractSettingId;
            _discount = discount;
        }
        public SaveDiscountAction(string user, int branchId, int contractSettingId, DiscountDC discount)
        {
            _branchId = branchId;
            _user = user;
            _contractSettingId = contractSettingId;
            _discount = discount;
        }

        protected override int Body(DbConnection connection)
        {
            int result = -1;
            DbTransaction transaction = null;
            try
            {
                transaction = connection.BeginTransaction();
                foreach (DiscountDC discountItem in _discountList)
                {
                    discountItem.BranchId = _branchId;
                    discountItem.CreatedUser = _user;
                    _discount = discountItem;
                    result = SaveDiscountCategory(transaction);
                }
                transaction.Commit();
            }
            catch (Exception ex)
            {
                if (transaction != null) transaction.Rollback();
                throw ex;
            }

            return result;
        }

        public int SaveDiscountCategory(DbTransaction transaction)
        {
            int result = -1;
            try
            {
                if (_discount.DiscountType != DiscountType.SHOP)
                {
                    const string storedProcedureName = "USExceGMSManageMembershipSaveDiscount";
                    DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                    cmd.Connection = transaction.Connection;
                    cmd.Transaction = transaction;

                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, _discount.Id));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@CategoryCode", DbType.String, _discount.DiscountType.ToString()));

                    if (_discount.DiscountType == DiscountType.GROUP)
                    {
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@MinMembersNo", DbType.Int32, _discount.MinMemberNo));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Name", DbType.String, _discount.GroupDiscountName));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@MinPayment", DbType.Decimal, _discount.MinPayment));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Discount", DbType.Decimal, _discount.DiscountAmount));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@TypeId", DbType.Int32, _discount.TypeId));

                        if (_discount.HighAmount > 0)
                        {
                            cmd.Parameters.Add(DataAcessUtils.CreateParam("@HighDiscount", DbType.Decimal, _discount.HighAmount));
                            cmd.Parameters.Add(DataAcessUtils.CreateParam("@HighMode", DbType.String, "FIXEDAMOUNT"));
                        }
                        else if (_discount.HighPercentage > 0)
                        {
                            cmd.Parameters.Add(DataAcessUtils.CreateParam("@HighDiscount", DbType.Decimal, _discount.HighPercentage));
                            cmd.Parameters.Add(DataAcessUtils.CreateParam("@HighMode", DbType.String, "PERCENTATGE"));
                        }
                        if (_discount.LowAmount > 0)
                        {
                            cmd.Parameters.Add(DataAcessUtils.CreateParam("@LowDiscount", DbType.Decimal, _discount.LowAmount));
                            cmd.Parameters.Add(DataAcessUtils.CreateParam("@LowMode", DbType.String, "FIXEDAMOUNT"));
                        }
                        else if (_discount.LowPercentage > 0)
                        {
                            cmd.Parameters.Add(DataAcessUtils.CreateParam("@LowDiscount", DbType.Decimal, _discount.LowPercentage));
                            cmd.Parameters.Add(DataAcessUtils.CreateParam("@LowMode", DbType.String, "PERCENTATGE"));
                        }
                        //if (!_isSponsorShip)
                        //{
                        //cmd.Parameters.Add(DataAcessUtils.CreateParam("@ModeId", DbType.Int32, _discount.ModeId));
                        //}
                        if (_discount.SponsorId > 0)
                        {
                            cmd.Parameters.Add(DataAcessUtils.CreateParam("@SponsorId", DbType.Int32, _discount.SponsorId));
                        }
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@MinVisits", DbType.Int32, _discount.MinVisitsNo));
                    }

                    else if (_discount.DiscountType == DiscountType.ACTIVITY)
                    {
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Discount", DbType.Decimal, _discount.DiscountPercentage));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Name", DbType.String, _discount.ActivityDiscountName));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@TrailTime", DbType.Date, _discount.TrailTime));
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@ActivityId", DbType.Int32, _discount.ActivityId));
                    }

                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _discount.BranchId));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsActive", DbType.String, _discount.IsActive));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreatedUser", DbType.String, _discount.CreatedUser));
                    if (_contractSettingId > 0)
                    {
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@ContractSettingId", DbType.Int32, _contractSettingId));
                    }
                    DbParameter outputPara = new SqlParameter();
                    outputPara.DbType = DbType.Int32;
                    outputPara.ParameterName = "@OutID";
                    outputPara.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(outputPara);
                    cmd.ExecuteNonQuery();

                    result = Convert.ToInt32(outputPara.Value);
                }
                else
                {
                    int outputId = -1;
                    const string storedProcedureName = "USExceGMSShopAddDiscount";
                    const string storedProcedureName2 = "USExceGMSAdminAddArticleDiscount";
                    DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                    cmd.Connection = transaction.Connection;
                    cmd.Transaction = transaction;
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, _discount.Id));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@Name", DbType.String, _discount.Name));

                    if (_discount.Description == null)
                    {
                        _discount.Description = "";
                    }
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@Description", DbType.String, _discount.Description));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@Discount", DbType.Decimal, _discount.DiscountAmount));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@StartDate", DbType.DateTime, _discount.StartDate));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndDate", DbType.DateTime, _discount.EndDate));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _discount.BranchId));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsActive", DbType.Boolean, _discount.IsActive));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreatedUser", DbType.String, _discount.CreatedUser));
                    //-----------------------------------------------------------
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@DiscountTypeId", DbType.Int32, _discount.TypeId));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@NoOfFreeItems", DbType.Int32, _discount.NumberOfFreeItems));
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@MinNoOfArticles", DbType.Int32, _discount.MinNumberOfArticles));
                    if (_discount.VendorId > 0)
                    {
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@VendorId", DbType.Int32, _discount.VendorId));
                    }
                    if (_discount.ArticleCategoryId > 0)
                    {
                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@ArticleCategoryId", DbType.Int32, _discount.ArticleCategoryId));
                    }

                    //---------------------------------------------------------
                    DbParameter outputPara = new SqlParameter();
                    outputPara.DbType = DbType.Int32;
                    outputPara.ParameterName = "@OutID";
                    outputPara.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(outputPara);
                    cmd.ExecuteNonQuery();
                    result = Convert.ToInt32(outputPara.Value);
                    outputId = Convert.ToInt32(outputPara.Value);

                    if (outputId != -1)
                    {
                        DbCommand cmdItems = CreateCommand(CommandType.StoredProcedure, storedProcedureName2);
                        foreach (int itemId in _discount.ItemList)
                        {
                            cmdItems.Connection = transaction.Connection;
                            cmdItems.Transaction = transaction;
                            cmdItems.Parameters.Clear();
                            cmdItems.Parameters.Add(DataAcessUtils.CreateParam("@ExceShopDiscountId", DbType.Int32, outputId));
                            cmdItems.Parameters.Add(DataAcessUtils.CreateParam("@ItemId", DbType.Int32, itemId));
                            cmdItems.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result = -1;
                throw ex;
            }
            return result;
        }
    }
}
