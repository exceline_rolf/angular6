﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.SystemObjects;
using US_DataAccess;


namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetMemberListAction : USDBActionBase<List<MemberForMemberlist>>
    {
        private string lastname;
        private string firstname;
        private string custid;
        private string age;
        private string mobile;
        private string email;
        private string membercardNo;
        private string contractno;
        private string branchname;
        private string invoiceno;
        private bool isNumber;
        private int filterBranchId;
        private int filterStatusId;
        private string filterRoleId;

        public GetMemberListAction(FilterMemberList parameters)
        {
            lastname = parameters.LastName;
            firstname = parameters.FirstName;
            custid = parameters.CustId;
            age = parameters.Age;
            mobile = parameters.Mobile;
            email = parameters.Email;
            membercardNo = parameters.MembercardNo;
            contractno = parameters.ContractNo;
            branchname = parameters.BranchName;
            invoiceno = parameters.InvoiceNo;
            filterBranchId = parameters.FilterBranchId;
            filterStatusId = parameters.FilterStatusId;
            filterRoleId = parameters.FilterRoleId;
            
            int n;
            isNumber = int.TryParse(age, out n);
    }

        protected override List<MemberForMemberlist> Body(DbConnection connection)
        {
            var memberList = new List<MemberForMemberlist>();
            const string storedProcedureName = "USExceGMSManageMembershipGetMemberlist";

            try
            {
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@lastname", DbType.String, lastname));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@firstname", DbType.String, firstname));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@custid", DbType.String, custid));
                if (!String.IsNullOrEmpty(age) || isNumber)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@age", DbType.Int32, age));
                }
                else
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@age", DbType.Int32, -1));
                }
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@mobile", DbType.String, mobile));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@email", DbType.String, email));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@membercardNo", DbType.String, membercardNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@contractno", DbType.String, contractno));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchname", DbType.String, branchname));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@invoiceno", DbType.String, invoiceno));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, filterBranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@StatusId", DbType.Int32, filterStatusId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@RoleId", DbType.String, filterRoleId));


                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var ordinaryMember = new MemberForMemberlist();
                    ordinaryMember.ID = Convert.ToInt32(reader["ID"]);
                    ordinaryMember.FirstName = reader["FirstName"].ToString();
                    ordinaryMember.LastName = reader["LastName"].ToString();
                    ordinaryMember.Age = Convert.ToInt32(reader["Age"]);
                    ordinaryMember.RoleId = reader["RoleId"].ToString();
                    ordinaryMember.Email = reader["Email"].ToString();
                    ordinaryMember.StatusName = reader["StatusName"].ToString();
                    ordinaryMember.BranchName = reader["BranchName"].ToString();
                    ordinaryMember.BranchID = Convert.ToInt32(reader["BranchId"]);
                    ordinaryMember.ActiveStatus = Convert.ToInt32(reader["ActiveStatus"]);
                    ordinaryMember.CustId = reader["CustomerNo"].ToString();
                    ordinaryMember.MainContractNo = reader["MainContractNo"].ToString();
                    ordinaryMember.OrderAmount = Convert.ToInt32(reader["OrderAmount"]);
                    ordinaryMember.Mobile = reader["Mobile"].ToString();
                    ordinaryMember.ContractId = reader["ContractId"].ToString();
                    ordinaryMember.MembercardNo = reader["MembercardNo"].ToString();
                    ordinaryMember.Name = reader["FirstName"].ToString() + " " + reader["LastName"].ToString();
                    ordinaryMember.InvoiceNo = reader["InvoiceNo"].ToString();

                    memberList.Add(ordinaryMember);
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return memberList;
        }

    }
}
