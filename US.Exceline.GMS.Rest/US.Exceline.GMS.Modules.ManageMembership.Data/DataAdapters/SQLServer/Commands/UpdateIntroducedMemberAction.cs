﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : GMA
// Created Timestamp : 12/10/2013 8:26:15 AM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class UpdateIntroducedMemberAction : USDBActionBase<bool>
    {
        private readonly int _memberId = -1;
        private readonly DateTime? _creditedDate;
        private readonly string _creditedText;
        private readonly bool _isDelete;

        public UpdateIntroducedMemberAction(int memberId, DateTime? creditedDate, string creditedText, bool isDelete)
        {
            _memberId = memberId;
            _creditedDate = creditedDate;
            _creditedText = creditedText;
            _isDelete = isDelete;
        }

        protected override bool Body(DbConnection connection)
        {
            bool result;
            const string storedProcedureName = "USExceGMSManageMembershipUpdateIntroduceMemberByMemberId";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@creditedDate", DbType.DateTime, _creditedDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@creditedText", DbType.String, _creditedText));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@isDelete", DbType.Boolean, _isDelete));

                cmd.ExecuteNonQuery();
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}

