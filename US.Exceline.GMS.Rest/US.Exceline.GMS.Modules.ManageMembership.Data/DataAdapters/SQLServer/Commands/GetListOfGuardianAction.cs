﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetListOfGuardianAction : USDBActionBase<List<ExcelineMemberDC>>
    {
        private int _guardianId = -1;
        public GetListOfGuardianAction(int guardianId)
        {
            _guardianId = guardianId;
        }
        protected override List<ExcelineMemberDC> Body(DbConnection connection)
        {
            List<ExcelineMemberDC> guardianList = new List<ExcelineMemberDC>();
            const string spName = "USExceGMSManageMembershipGetListOfGuardian";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@guardianId", System.Data.DbType.Int32, _guardianId));
                DbDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    ExcelineMemberDC member = new ExcelineMemberDC
                                                  {
                                                      Id = Convert.ToInt32(reader["GuardianId"]),
                                                      Name = Convert.ToString(reader["GuardianName"]),
                                                      CustId = Convert.ToString(reader["CustId"])
                                                  };
                    guardianList.Add(member);
                }
                return guardianList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
