﻿
// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 5/10/2012 8:26:15 AM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class DeleteCreditNoteAction : USDBActionBase<bool>
    {
        private int _creditNoteId = -1;

        public DeleteCreditNoteAction(int creditNoteId)
        {
            _creditNoteId = creditNoteId;
        }

        protected override bool Body(System.Data.Common.DbConnection connection)
        {
            bool result = false;
            string StoredProcedureName = "USExceGMSManageMembershipDeleteCreditNote";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@creditNoteId", DbType.Int32, _creditNoteId));
                cmd.ExecuteNonQuery();
                result = true;
            }
            catch
            {
                throw;
            }
            return result;
        }
    }
}
