﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class SaveContractMembersAction : USDBActionBase<int>
    {
        private int _groupId = -1;
        private int _memberId = -1;
        private int _memberContractId = -1;
        private string _user = string.Empty;

        public SaveContractMembersAction(int groupId, int memberId, int memberContractId, string user)
        {
            _groupId = groupId;
            _memberId = memberId;
            _memberContractId = memberContractId;
            _user = user;
        }

        public int RunOnTransaction(DbTransaction transaction)
        {
            string spName = "USExceGMSManageMembershipAddContractGroupMembers";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure,spName);
                command.Transaction = transaction;
                command.Connection = transaction.Connection;
                command.Parameters.Add(DataAcessUtils.CreateParam("@GroupId", System.Data.DbType.Int32, _groupId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@MemberId", System.Data.DbType.Int32, _memberId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ContractId", System.Data.DbType.Int32, _memberContractId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@User", System.Data.DbType.String, _user));
                command.ExecuteNonQuery();
                return 1;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        protected override int Body(DbConnection connection)
        {
           return 1;
        }
    }
}
