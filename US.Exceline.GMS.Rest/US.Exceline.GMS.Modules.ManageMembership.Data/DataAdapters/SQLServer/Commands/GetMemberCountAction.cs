﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US_DataAccess;


namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetMemberCountAction : USDBActionBase<List<int>>
    {
        private int _branchId;

        public GetMemberCountAction(int branchId)
        {
            _branchId = branchId;
        }

        protected override List<int> Body(DbConnection connection)
        {
            var memberCounts = new List<int>();
            const string storedProcedureName = "USExceGMSManageMembershipGetMemberCounts";

            try
            {
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _branchId));
                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var count = Convert.ToInt32(reader["num"]);
                    memberCounts.Add(count);
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return memberCounts;
        }

    }
}
