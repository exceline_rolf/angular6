﻿/*
 * Created by Anders Feyling, anders@exceline.no
 * Date: 17.01.2019
 * Disc: Created with the purpose of getting a specific notification from the event log, e.g. when a sponsor was added
 * 
 * */

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.Common.Notification.Core.DomainObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class ManageMembershipLogAddNotificationAction : USDBActionBase<bool>
    {
        private USNotificationTree _notification;
        private string _user;

        public ManageMembershipLogAddNotificationAction(USNotificationTree notification, string user)
        {
            _notification = notification;
            _user = user;
        }
        protected override bool Body(DbConnection connection)
        {
            bool result = false;
            try
            {
                string storedProcedure = "dbo.US_AddNotification";
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Title", DbType.String, _notification.Notification.Title));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Description", DbType.String, _notification.Notification.Description));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreatedUser", DbType.String, _user));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@TypeId", DbType.Int32, _notification.Notification.TypeId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SeverityId", DbType.Int32, _notification.Notification.SeverityId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _notification.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Role", DbType.String, "MEM"));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@LastAttendDate", DbType.Date, _notification.Notification.CreatedDate));

                if (_notification.MemberId > 0)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _notification.MemberId));
                }
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Status", DbType.Int32, _notification.Notification.StatusId));

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}
