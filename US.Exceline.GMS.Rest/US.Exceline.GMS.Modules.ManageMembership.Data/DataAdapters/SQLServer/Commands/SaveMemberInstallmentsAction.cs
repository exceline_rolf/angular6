﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageMemberships;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.ManageMembership;
using System.Data.SqlClient;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class SaveMemberInstallmentsAction : USDBActionBase<bool>
    {
        private List<InstallmentDC> _installmentList;
        private DataTable _dataTable;
        private bool _initialGenInstmnts;
        DateTime _startDate;
        DateTime _endDate;
        private int _memberContractNo = -1;
        private int _memberContractId;
        private int _branchId = -1;
        private int _renewdTemplateId = -1;
        private int _currentTemplateId = -1;

        public SaveMemberInstallmentsAction(List<InstallmentDC> installmentList, bool initialGenInstmnts, DateTime startDate, DateTime endDate, int memberContractId, int membercontractNo, int branchId, int renewdTemplateId)
        {
            _installmentList = installmentList;
            _memberContractId = memberContractId;
            _initialGenInstmnts = initialGenInstmnts;
            _startDate = startDate;
            _renewdTemplateId = renewdTemplateId;
            _endDate = endDate;
            _memberContractNo = membercontractNo;
            _branchId = branchId;
            _dataTable = GetInstallmentList(_installmentList);
        }

        private DataTable GetInstallmentList(List<InstallmentDC> installmentList)
        {
            try
            {
                DataTable dataTable = new DataTable();
                dataTable.Columns.Add(new DataColumn("MemberId", typeof(int)));
                dataTable.Columns.Add(new DataColumn("MemberContractId", typeof(int)));
                dataTable.Columns.Add(new DataColumn("Text", typeof(string)));
                dataTable.Columns.Add(new DataColumn("InstallmentDate", typeof(DateTime)));
                dataTable.Columns.Add(new DataColumn("DueDate", typeof(DateTime)));
                dataTable.Columns.Add(new DataColumn("PaidDate", typeof(DateTime)));
                dataTable.Columns.Add(new DataColumn("Amount", typeof(decimal)));
                dataTable.Columns.Add(new DataColumn("InvoiceDate", typeof(DateTime)));
                dataTable.Columns.Add(new DataColumn("DebtCollectionDate", typeof(DateTime)));
                dataTable.Columns.Add(new DataColumn("PrintedDate", typeof(DateTime)));
                dataTable.Columns.Add(new DataColumn("Submitted", typeof(string)));
                dataTable.Columns.Add(new DataColumn("AddonPrice", typeof(decimal)));
                dataTable.Columns.Add(new DataColumn("InstallmentNo", typeof(int)));
                dataTable.Columns.Add(new DataColumn("KID", typeof(string)));
                dataTable.Columns.Add(new DataColumn("InstallmentType", typeof(string)));
                dataTable.Columns.Add(new DataColumn("TransferDate", typeof(DateTime)));
                dataTable.Columns.Add(new DataColumn("Balance", typeof(decimal)));
                dataTable.Columns.Add(new DataColumn("TSDate", typeof(DateTime)));
                dataTable.Columns.Add(new DataColumn("TEDate", typeof(DateTime)));
                dataTable.Columns.Add(new DataColumn("IsATG", typeof(bool)));
                dataTable.Columns.Add(new DataColumn("Debit", typeof(bool)));
                dataTable.Columns.Add(new DataColumn("DeleteRequest", typeof(bool)));
                dataTable.Columns.Add(new DataColumn("Treat", typeof(bool)));
                dataTable.Columns.Add(new DataColumn("IsOtherPay", typeof(bool)));
                dataTable.Columns.Add(new DataColumn("OrderNo", typeof(string)));
                dataTable.Columns.Add(new DataColumn("ActivityPrice", typeof(decimal)));
                dataTable.Columns.Add(new DataColumn("AdonText", typeof(string)));
                dataTable.Columns.Add(new DataColumn("IsInvoiceFee", typeof(bool)));
                dataTable.Columns.Add(new DataColumn("ContractNo", typeof(int)));
                dataTable.Columns.Add(new DataColumn("ServiceAmount", typeof(decimal)));
                dataTable.Columns.Add(new DataColumn("ItemAmount", typeof(decimal)));
                dataTable.Columns.Add(new DataColumn("SponsoredAmount", typeof(decimal)));
                dataTable.Columns.Add(new DataColumn("TemplateId", typeof(int)));
                dataTable.Columns.Add(new DataColumn("CreatedUser", typeof(string)));
                dataTable.Columns.Add(new DataColumn("PayerId", typeof(int)));
                //dataTable.Columns.Add(new DataColumn("EstimatedInvoicedDate", typeof(DateTime)));

                foreach (InstallmentDC installment in _installmentList)
                {
                    DataRow dataTableRow = dataTable.NewRow();
                    dataTableRow["MemberId"] = installment.MemberId;
                    dataTableRow["SponsoredAmount"] = installment.SponsoredAmount;

                    dataTableRow["MemberContractId"] = installment.MemberContractId;
                    dataTableRow["Text"] = installment.Text;
                    dataTableRow["InstallmentDate"] = installment.InstallmentDate;
                    dataTableRow["DueDate"] = installment.DueDate;
                    if (installment.PaidDate == new DateTime(1, 1, 1))
                    {
                        dataTableRow["PaidDate"] = DBNull.Value; // installment.PaidDate;
                    }
                    else if (installment.PaidDate == null)
                    {
                        dataTableRow["PaidDate"] = DBNull.Value;
                    }
                    else
                    {
                        dataTableRow["PaidDate"] = installment.PaidDate;
                    }
                    dataTableRow["InvoiceDate"] = installment.BillingDate;

                    if (installment.DebtCollectionDate == new DateTime(1, 1, 1))
                    {
                        dataTableRow["DebtCollectionDate"] = DBNull.Value;
                    }
                    else if (installment.DebtCollectionDate == null)
                    {
                        dataTableRow["DebtCollectionDate"] = DBNull.Value;
                    }
                    else
                    {
                        dataTableRow["DebtCollectionDate"] = installment.DebtCollectionDate;
                    }

                    dataTableRow["PrintedDate"] = DBNull.Value;
                    if (installment.TransferDate == new DateTime(1, 1, 1))
                    {
                        dataTableRow["TransferDate"] = DBNull.Value;
                    }
                    else
                    {
                        dataTableRow["TransferDate"] = installment.TransferDate;
                    }
                    dataTableRow["Amount"] = installment.Amount;
                    dataTableRow["Submitted"] = installment.Submitted;
                    dataTableRow["AddonPrice"] = installment.AdonPrice;
                    dataTableRow["InstallmentNo"] = installment.InstallmentNo;
                    dataTableRow["KID"] = string.Empty;
                    dataTableRow["InstallmentType"] = "M";
                    dataTableRow["Balance"] = installment.Balance;
                    if (installment.TrainingPeriodStart == null)
                    {
                        dataTableRow["TSDate"] = DBNull.Value;
                    }
                    else
                    {
                        dataTableRow["TSDate"] = installment.TrainingPeriodStart;
                    }

                    if (installment.TrainingPeriodEnd == null)
                    {
                        dataTableRow["TEDate"] = DBNull.Value;
                    }
                    else
                    {
                        dataTableRow["TEDate"] = installment.TrainingPeriodEnd;
                    }
                    dataTableRow["IsATG"] = installment.IsATG;
                    dataTableRow["Debit"] = installment.IsDebit;
                    dataTableRow["DeleteRequest"] = installment.IsDeleteRequest;
                    dataTableRow["Treat"] = installment.IsTreat;

                    if (installment.PayerId != -1)
                    {
                        if (installment.MemberId != installment.PayerId)
                            dataTableRow["IsOtherPay"] = true;
                        else
                            dataTableRow["IsOtherPay"] = false;
                    }
                    else
                    {
                        dataTableRow["IsOtherPay"] = true;
                    }

                    dataTableRow["OrderNo"] = installment.OrderNo;
                    dataTableRow["ActivityPrice"] = installment.ActivityPrice; ;
                    dataTableRow["AdonText"] = installment.AdonText;
                    dataTableRow["IsInvoiceFee"] = installment.IsInvoiceFeeAdded;
                    dataTableRow["ContractNo"] = _memberContractNo;
                    dataTableRow["ServiceAmount"] = installment.ServiceAmount;
                    dataTableRow["ItemAmount"] = installment.ItemAmount;
                    dataTableRow["TemplateId"] = installment.TemplateId;
                    dataTableRow["CreatedUser"] = installment.CreatedUser;
                    dataTableRow["PayerId"] = installment.PayerId;
                    //dataTableRow["EstimatedInvoicedDate"] = installment.EstimatedInvoiceDate; 
                    dataTable.Rows.Add(dataTableRow);
                }
                return dataTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool RunOnTransaction(DbTransaction transaction)
        {
            bool _isSaved = false;
            string StoredProcedureName = "USExceGMSManageMembershipSaveMemberInstallments";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Transaction = transaction;
                cmd.Connection = transaction.Connection;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@installmentList", SqlDbType.Structured, _dataTable));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@initialGenInstmnts", DbType.Boolean, _initialGenInstmnts));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@startDate ", DbType.DateTime, _startDate));
                if (_endDate == DateTime.MinValue)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@endDate ", DbType.DateTime, null));
                }
                else
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@endDate ", DbType.DateTime, _endDate));
                }

                if (_renewdTemplateId > 0)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@renewedTemplateId ", DbType.Int32, _renewdTemplateId));
                }
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberContractId ", DbType.Int32, _memberContractId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId ", DbType.Int32, _branchId));

                cmd.ExecuteNonQuery();
                _isSaved = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (_isSaved)
            {
                foreach (InstallmentDC installment in _installmentList)
                {
                    if (installment.AddOnList != null)
                    {
                        if (installment.AddOnList.Count > 0)
                        {
                            foreach (var item in installment.AddOnList)
                            {
                                if (item != null)
                                {
                                    string _storedProcedureName = "USExceGMSManageMembershipAddInstallmentShare";
                                    try
                                    {
                                        DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, _storedProcedureName);
                                        cmd.Connection = transaction.Connection;
                                        cmd.Transaction = transaction;
                                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@articleId", DbType.Int32, item.ArticleId));
                                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@installmentNo", DbType.Int32, installment.InstallmentNo));
                                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@InstallmentDate", DbType.DateTime, installment.InstallmentDate));
                                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@text", DbType.String, item.ItemName));
                                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@articlePrice", DbType.Decimal, item.Price));
                                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@isStartupItem", DbType.Boolean, item.IsStartUpItem));
                                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@MembercontractId", DbType.Int32, _memberContractId));
                                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Quantity", DbType.Int32, item.Quantity));
                                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Discount", DbType.Decimal, item.Discount));
                                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsActivityArticle", DbType.Boolean, item.IsActivityArticle));
                                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId ", DbType.Int32, _branchId));
                                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@description ", DbType.String, item.Description));
                                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@priority ", DbType.Int32, item.Priority));
                                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@isBookingArticle", DbType.Boolean, item.IsBookingArticle));
                                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@VoucherNo ", DbType.String, item.VoucherNo));
                                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@VoucherExpireDate ", DbType.DateTime, item.ExpiryDate));
                                        cmd.ExecuteNonQuery();
                                        _isSaved = true;
                                    }

                                    catch (Exception ex)
                                    {
                                        throw ex;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return _isSaved;
        }

        protected override bool Body(DbConnection connection)
        {
            bool _isSaved = false;
            DbTransaction transaction = null;
            transaction = connection.BeginTransaction();
            string StoredProcedureName = "USExceGMSManageMembershipSaveMemberInstallments";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Transaction = transaction;
                cmd.Connection = transaction.Connection;
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@installmentList", SqlDbType.Structured, _dataTable));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@initialGenInstmnts", DbType.Boolean, _initialGenInstmnts));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@startDate ", DbType.DateTime, _startDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@endDate ", DbType.DateTime, _endDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberContractId ", DbType.Int32, _memberContractId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId ", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@currentContractId", DbType.Int32, _currentTemplateId));

                cmd.ExecuteNonQuery();
                _isSaved = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (_isSaved)
            {
                foreach (InstallmentDC installment in _installmentList)
                {
                    if (installment.AddOnList != null)
                    {
                        if (installment.AddOnList.Count > 0)
                        {
                            foreach (var item in installment.AddOnList)
                            {
                                if (item != null)
                                {
                                    string _storedProcedureName = "USExceGMSManageMembershipAddInstallmentShare";
                                    try
                                    {
                                        DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, _storedProcedureName);
                                        cmd.Connection = transaction.Connection;
                                        cmd.Transaction = transaction;
                                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@articleId", DbType.Int32, item.ArticleId));
                                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@InstallmentDate", DbType.DateTime, installment.InstallmentDate));
                                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@text", DbType.String, item.ItemName));
                                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@articlePrice", DbType.Decimal, item.Price));
                                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@isStartupItem", DbType.Boolean, item.IsStartUpItem));
                                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@MembercontractId", DbType.Int32, _memberContractId));
                                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Quantity", DbType.Int32, item.Quantity));
                                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@Discount", DbType.Decimal, item.Discount));
                                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsActivityArticle", DbType.Boolean, item.IsActivityArticle));
                                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId ", DbType.Int32, _branchId));
                                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@description ", DbType.String, item.Description));
                                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@priority ", DbType.Int32, item.Priority));
                                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@VoucherNo ", DbType.String, item.VoucherNo));
                                        cmd.Parameters.Add(DataAcessUtils.CreateParam("@VoucherExpireDate ", DbType.DateTime, item.ExpiryDate));

                                        cmd.ExecuteNonQuery();
                                        _isSaved = true;
                                    }

                                    catch (Exception ex)
                                    {
                                        throw ex;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return _isSaved;
        }
    }
}

