﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetSponsorInstallmentDetailsAction : USDBActionBase<List<SponsorshipItemDC>>
    {
        private int _installmentID = -1;
        public GetSponsorInstallmentDetailsAction(int installmentID)
        {
            _installmentID = installmentID;
        }


        protected override List<SponsorshipItemDC> Body(System.Data.Common.DbConnection connection)
        {
            string _storedProcedureName = "USExceGMSManageMembershipGetSponsorInstallmentDetails";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, _storedProcedureName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@InstallmentId", System.Data.DbType.Int32, _installmentID));
                DbDataReader reader = command.ExecuteReader();
                List<SponsorshipItemDC> spInstallmentItemList = new List<SponsorshipItemDC>();
                while (reader.Read())
                {
                    SponsorshipItemDC spItem = new SponsorshipItemDC();
                    spItem.CusId = Convert.ToString(reader["CusId"]);
                    spItem.MemberContractNo = Convert.ToString(reader["MemberContractNo"]);
                    spItem.InstallmentText = Convert.ToString(reader["IText"]);
                    spItem.Amount= Convert.ToDecimal(reader["Amount"]);

                    spInstallmentItemList.Add(spItem);
                }

                reader.Close();
                return spInstallmentItemList;
            }
            catch
            {
                throw;
            }
        }
    }
}
