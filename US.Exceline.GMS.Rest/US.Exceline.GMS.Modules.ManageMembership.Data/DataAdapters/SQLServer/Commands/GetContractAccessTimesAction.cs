﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetContractAccessTimesAction : USDBActionBase<List<ContractAccessTimeDC>>
    {
        private int _accessProfileId = -1;
        private int _branchId = -1;
        public GetContractAccessTimesAction(int accessProfileId, int branchId )
        {
            _accessProfileId = accessProfileId;
            _branchId = branchId;
        }

        protected override List<ContractAccessTimeDC> Body(System.Data.Common.DbConnection connection)
        {
            List<ContractAccessTimeDC> accesTimes = new List<ContractAccessTimeDC>();
            string spName = "USExceGMSManageMembershipGetContractTime";
            DbDataReader reader = null;
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@AccessProfileId", System.Data.DbType.Int32, _accessProfileId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", System.Data.DbType.Int32, _branchId));
                
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ContractAccessTimeDC time = new ContractAccessTimeDC();
                    time.Day = Convert.ToString(reader["AccessDay"]);
                    if (reader["FromTime"] != DBNull.Value)
                        time.InTime = Convert.ToDateTime(reader["FromTime"]);
                    if (reader["ToTime"] != DBNull.Value)
                         time.OutTime = Convert.ToDateTime(reader["ToTime"]);
                     accesTimes.Add(time);
                }
                return accesTimes;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
