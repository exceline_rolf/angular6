﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class ValidateSponsoringGenerationAction : USDBActionBase<DateTime?>
    {
        protected override DateTime? Body(DbConnection connection)
        {
            const string storedProcedureName = "USExceGMSMemberValidateSponsoringGeneration";

            try
            {
                DateTime? result = null;
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);

                DbParameter output = new SqlParameter();
                output.DbType = DbType.DateTime;
                output.ParameterName = "@outId";
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);

                cmd.ExecuteNonQuery();
                if (output.Value != null)
                {
                    result =  Convert.ToDateTime(output.Value);
                }
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
}
}
