﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : GMA
// Created Timestamp : "10/21/2013 6:07:12 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US.GMS.Core.DomainObjects.Common;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.Utils;

namespace US.GMS.Data.DataAdapters.SQLServer.Commands
{
    public class GetFollowUpDetailsAction : USDBActionBase<List<FollowUpDetailDC>>
    {
        private readonly int _followUpId;

        public GetFollowUpDetailsAction(int followUpId)
        {
            _followUpId = followUpId;
        }

        protected override List<FollowUpDetailDC> Body(DbConnection connection)
        {
            List<FollowUpDetailDC> exeTaskList = new List<FollowUpDetailDC>();
            const string storedProcedureName = "USExceGMSGetFollowUpDetails";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@followUpId", DbType.Int32, _followUpId));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    FollowUpDetailDC exeTask = new FollowUpDetailDC();
                    exeTask.Id = Convert.ToInt32(reader["Id"]);
                    exeTask.Name = reader["Name"].ToString();
                    exeTask.TaskCategoryId = Convert.ToInt32(reader["TaskCategoryId"]);
                    exeTask.TaskCategoryName = Convert.ToString(reader["TaskCategoryName"]);
                    exeTask.StartDate = Convert.ToDateTime(reader["StartDate"]);
                    exeTask.EndDate = Convert.ToDateTime(reader["EndDate"]);
                    if (reader["StartTime"] != DBNull.Value)
                    {
                        exeTask.StartTime = Convert.ToDateTime(reader["StartTime"]);
                    }
                    if (reader["EndTime"] != DBNull.Value)
                    {
                        exeTask.EndTime = Convert.ToDateTime(reader["EndTime"]);
                    }
                    exeTask.FoundDate = Convert.ToDateTime(reader["FoundDate"]);
                    exeTask.ReturnDate = Convert.ToDateTime(reader["ReturnDate"]);
                    exeTask.DueDate = Convert.ToDateTime(reader["DueDate"]);
                    exeTask.DueTime = Convert.ToDateTime(reader["DueTime"]);
                    exeTask.NumOfDays = Convert.ToInt32(reader["NumOfDays"]);
                    exeTask.PhoneNo = reader["PhoneNo"].ToString();
                    exeTask.AutomatedSMS = reader["Sms"].ToString();
                    if (!String.IsNullOrEmpty(reader["ExtenedInfo"].ToString()))
                    {
                        ExtendedFieldInfoDC fieldInfo = new ExtendedFieldInfoDC();
                        fieldInfo = (ExtendedFieldInfoDC)XMLUtils.DesrializeXMLToObject(reader["ExtenedInfo"].ToString(), typeof(ExtendedFieldInfoDC));
                        exeTask.ExtendedFieldsList = fieldInfo.ExtendedFieldsList;
                    }
                    exeTask.CreatedDateTime = Convert.ToDateTime(reader["CreatedDateTime"]);
                    exeTask.LastModifiedDateTime = Convert.ToDateTime(reader["LastModifiedDateTime"]);
                    exeTask.CreatedUser = Convert.ToString(reader["CreatedUser"]);
                    exeTask.LastModifiedUser = Convert.ToString(reader["LastModifiedUser"]);
                    exeTask.ActiveStatus = Convert.ToBoolean(reader["ActiveStatus"]);
                    exeTask.IsShowInCalandar = Convert.ToBoolean(reader["IsShowInCalandar"]);
                    exeTask.FollowUpId = Convert.ToInt32(reader["FollowUpId"]);
                    exeTask.AssignedEmpId = Convert.ToInt32(reader["AssignedEmpId"]);
                    exeTask.AssignedEmpName = Convert.ToString(reader["AssignedEmpName"]);
                    exeTask.EntRoleId = Convert.ToInt32(reader["EntRoleId"]);
                    exeTask.RoleType = Convert.ToString(reader["RoleType"]);
                    if (reader["CompletedDate"] != DBNull.Value)
                    {
                        exeTask.CompletedDate = Convert.ToDateTime(reader["CompletedDate"]);
                    }
                    exeTask.CompletedEmpId = Convert.ToInt32(reader["CompletedEmpId"]);
                    exeTask.CompletedEmpName = Convert.ToString(reader["CompletedEmpName"]);
                    if (reader["PlannedDate"] != DBNull.Value)
                    {
                        exeTask.PlannedDate = Convert.ToDateTime(reader["PlannedDate"]);
                    }
                    exeTask.IsFixed = Convert.ToBoolean(reader["IsFixed"]);
                    exeTask.AutomatedEmail = reader["Email"].ToString();
                    exeTask.AutomatedEmail = reader["Description"].ToString();
                    exeTask.IsNextFollowUpValue = Convert.ToBoolean(reader["IsNextFollowUp"]);
                    exeTask.Comment = reader["Comment"].ToString();
                    exeTask.CompletedStatus = Convert.ToBoolean(reader["CompletedStatus"]);
                    exeTaskList.Add(exeTask);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return exeTaskList;
        }
    }
}
