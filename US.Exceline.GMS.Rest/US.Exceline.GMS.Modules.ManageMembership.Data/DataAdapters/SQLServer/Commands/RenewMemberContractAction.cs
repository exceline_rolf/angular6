﻿using System;
using System.Collections.Generic;
using US_DataAccess;
using System.Data.Common;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.ManageContracts;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class RenewMemberContractAction : USDBActionBase<bool>
    {
        private List<InstallmentDC> _installmentList;
        private int _branchId = -1;
        private bool _isAutoRenew = false;
        private MemberContractDC _renewedContract;
        private string _gymCode = string.Empty;
        private string _user = string.Empty;

        public RenewMemberContractAction(MemberContractDC renewedContract, List<InstallmentDC> installmentList, int branchId, string gymCode, bool isAutoRenew, string user)
        {
            _installmentList = installmentList;
            _branchId = branchId;
            _renewedContract = renewedContract;
            _gymCode = gymCode;
            _isAutoRenew = isAutoRenew;
            _user = user;
        }

        protected override bool Body(DbConnection connection)
        {
            DbTransaction transaction = null;
            transaction = connection.BeginTransaction();
            try
            {
                if (!_isAutoRenew)
                {
                    _renewedContract.InstalllmentList = new List<InstallmentDC>();
                    _renewedContract.ContractBookingList = new List<ContractBookingDC>();
                    UpdateContractAction contractAction = new UpdateContractAction(_renewedContract, true, _user);
                    contractAction.Execute(EnumDatabase.Exceline, _gymCode);
                }
                else
                {
                    UpdateMemberContractItems itemUpdateAction = new UpdateMemberContractItems(_renewedContract.Id, new List<ContractItemDC>(), _renewedContract.EveryMonthItemList,_user);
                    itemUpdateAction.RunOnTransaction(transaction);
                }

                SaveMemberInstallmentsAction action = new SaveMemberInstallmentsAction(_installmentList, false, _renewedContract.ContractStartDate, _renewedContract.ContractEndDate, _renewedContract.Id, Convert.ToInt32(_renewedContract.MemberContractNo), _branchId, _renewedContract.RenewedTemplateID);
                action.RunOnTransaction(transaction);
                transaction.Commit();
                return true;

            }
            catch
            {
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw;
            }
        }
    }
}
