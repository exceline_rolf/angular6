﻿using System;
using System.Collections.Generic;
using US.GMS.Core.DomainObjects.Common;
using US_DataAccess;
using System.Data.Common;
using System.Data;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetMonthlyServicesAction : USDBActionBase<List<MonthlyServicesForContract>>
    {
        private int _contractId = -1;
        private String _gymcode = String.Empty;

        public GetMonthlyServicesAction(int contractId, String gymcode)
        {
            _contractId = contractId;
            _gymcode = gymcode;
        }

        protected override List<MonthlyServicesForContract> Body(DbConnection connection)
        {

            List<MonthlyServicesForContract> listOfServices = new List<MonthlyServicesForContract>();
            const string storedProcedureName = "USC_ADP_LDRPTSP_MemberContract_MonthlyServices";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@itemID", DbType.String, _contractId.ToString()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@gymCode", DbType.String, _gymcode));

                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    MonthlyServicesForContract services = new MonthlyServicesForContract();

                    services.ItemName = Convert.ToString(reader["ItemName"]);
                    services.Price = Convert.ToDouble(reader["Price"]);
                    services.Units = Convert.ToInt32(reader["Units"]);
                    services.StartOrder = Convert.ToInt32(reader["StartOrder"]);
                    services.EndOrder = Convert.ToInt32(reader["EndOrder"]);

                    listOfServices.Add(services);
                }
            }

            catch (Exception ex)
            {
                throw new NotImplementedException("", ex);
            }
            return listOfServices;
        }
    }
}
