﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.SystemObjects;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetMemberSearchCategoryAction : USDBActionBase<string>
    {
        private int _branchId = -1;
        public GetMemberSearchCategoryAction(int branchId)
        {
            _branchId = branchId;
        }
        protected override string Body(DbConnection connection)
        {
            const string storedProcedureName = "USExceGMSManageMembershipGetSearchCategory";
            string searchText = string.Empty;
               try
                {
                    DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchID", DbType.Int32, _branchId));

                    DbDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        searchText = reader["MemberSearchText"].ToString();
                    }
                }
               catch (Exception ex)
               {
                   throw ex;
               }

            return searchText;
        }
    }
}
