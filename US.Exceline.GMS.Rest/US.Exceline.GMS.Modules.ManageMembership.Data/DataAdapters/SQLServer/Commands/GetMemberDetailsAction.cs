﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.SystemObjects;
using US_DataAccess;


namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetMemberDetailsAction : USDBActionBase<List<ExcelineMemberDC>>
    {
        private readonly int _branchId;
        private readonly string _searchText = string.Empty;
        private readonly int _status = -1;
        private readonly MemberSearchType _memberSearchType = MemberSearchType.SEARCH;
        private readonly MemberRole _membeRole = MemberRole.MEM;
        private readonly int _hit = 1;
        private readonly bool _isHeaderClick;
        private readonly bool _isAscending;
        private readonly string _sortName = string.Empty;
        private readonly string _user = string.Empty;


        public GetMemberDetailsAction(int branchId, string searchText, int status, MemberSearchType searchType, MemberRole memberRole,string user, int hit, bool isHeaderClick, bool isAscending, string sortName)
        {
            _branchId = branchId;
            _searchText = ProcessKeyWord(searchText);
            _status = status;
            _memberSearchType = searchType;
            _membeRole = memberRole;
            _hit = hit;
            _isHeaderClick = isHeaderClick;
            _isAscending = isAscending;
            _sortName = sortName;
            _user = user;
        }

        protected override List<ExcelineMemberDC> Body(DbConnection connection)
        {
            var ordinaryMemberLst = new List<ExcelineMemberDC>();
            const string storedProcedureName = "USExceGMSManageMembershipGetMemberDetails";

            try
            {
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchId", DbType.Int32, _branchId));
                if (!string.IsNullOrEmpty(_searchText))
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@seachText", DbType.String, _searchText));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@activeStatus", DbType.Int32, _status));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberSearchType", DbType.String, _memberSearchType));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberRole", DbType.String, _membeRole));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@isHeaderClick", DbType.Boolean, _isHeaderClick));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@isAscending", DbType.Boolean, _isAscending));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@UserName", DbType.String, _user));
                if (!string.IsNullOrEmpty(_sortName))
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@sortName", DbType.String, _sortName));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Hit", DbType.Int32, _hit));
                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var ordineryMember = new ExcelineMemberDC();
                    ordineryMember.Id = Convert.ToInt32(reader["MemberId"]);
                    ordineryMember.FirstName = reader["FirstName"].ToString();
                    ordineryMember.LastName = reader["LastName"].ToString();
                    string roleType = reader["RoleId"].ToString();
                    if (!string.IsNullOrEmpty(roleType.Trim()))
                    {
                        try
                        {
                            ordineryMember.Role = (MemberRole)Enum.Parse(typeof(MemberRole), roleType);
                        }
                        catch
                        {
                            ordineryMember.Role = MemberRole.NONE;
                        }
                    }

                    ordineryMember.Name = ordineryMember.Role == MemberRole.MEM ? String.Format("{0} {1}", ordineryMember.FirstName, ordineryMember.LastName) : String.Format("{0} {1}", ordineryMember.LastName, ordineryMember.FirstName);
                    ordineryMember.Address1 = reader["Address1"].ToString();
                    ordineryMember.Address2 = reader["Address2"].ToString();
                    ordineryMember.Mobile = Convert.ToString(reader["Mobile"]).Trim();
                    ordineryMember.StatusName = Convert.ToString(reader["StatusName"]);
                    ordineryMember.MobilePrefix = Convert.ToString(reader["TelMobilePrefix"]).Trim();
                    ordineryMember.Email = Convert.ToString(reader["Email"]);
                    if (string.IsNullOrEmpty(ordineryMember.Email)) ordineryMember.IsHasntEmail = true;
                    else ordineryMember.IsHasEmail = true;
                    if (reader["Age"] != DBNull.Value)
                        ordineryMember.Age = Convert.ToInt32(reader["Age"]);
                    if (reader["OrderAmount"] != DBNull.Value)
                        ordineryMember.OrderAmount = Convert.ToDecimal(reader["OrderAmount"]);
                    if (reader["ContractNo"] != DBNull.Value)
                        ordineryMember.ContractNumber = Convert.ToString(reader["ContractNo"]);
                    var gender = reader["Gender"].ToString();
                    if (!string.IsNullOrEmpty(gender.Trim()))
                    {
                        try
                        {
                            ordineryMember.Gender = (Gender)Enum.Parse(typeof(Gender), gender);
                        }
                        catch { }
                    }
                    ordineryMember.ZipCode = Convert.ToString(reader["ZipCode"]);
                    ordineryMember.ZipName = Convert.ToString(reader["ZipName"]);
                    ordineryMember.CustId = Convert.ToString(reader["CustId"]);
                    try
                    {
                        ordineryMember.IntCustId = Convert.ToInt32(reader["CustId"]);
                    }
                    catch
                    {
                    }

                    if (reader["BirthDate"] != DBNull.Value)
                        ordineryMember.BirthDate = Convert.ToDateTime(reader["BirthDate"]);
                    if (reader["IntroduceDate"] != DBNull.Value)
                        ordineryMember.IntroduceDate = Convert.ToDateTime(reader["IntroduceDate"]);
                    if (reader["DataCount"] != DBNull.Value)
                        ordineryMember.Count = Convert.ToInt32(reader["DataCount"]);
                    if(reader["BranchName"] != DBNull.Value)
                        ordineryMember.BranchName = Convert.ToString(reader["BranchName"]);
                    if(reader["BranchId"] != DBNull.Value)
                        ordineryMember.BranchID = Convert.ToInt32(reader["BranchId"]);
                    ordinaryMemberLst.Add(ordineryMember);
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return ordinaryMemberLst;
        }

        #region ProcessKeyWord

        private string ProcessKeyWord(string searchWord)
        {
            if (!string.IsNullOrEmpty(searchWord))
            {
                string[] keywordparts = searchWord.Trim().Split(new[] { ':' }, 2);
                if (keywordparts.Length > 1)
                {
                    string[] wordParts = keywordparts[1].Split(' ');
                    if (wordParts.Length > 1)
                    {
                        string newword = wordParts[0];

                        for (int index = 1; index < wordParts.Length; index++)
                        {
                            newword = string.Format("{0} AND {1}", newword, wordParts[index]);
                        }
                        return string.Format("{0}:{1}", keywordparts[0], newword);
                    }

                    else
                        return string.Format("{0}:{1}", keywordparts[0], wordParts[0]);
                }
                else if (keywordparts.Length == 1)
                {
                    string[] wordParts = keywordparts[0].Split(' ');
                    if (wordParts.Length > 1)
                    {
                        string newword = wordParts[0];
                        for (int index = 1; index < wordParts.Length; index++)
                        {
                            newword = string.Format("{0} AND {1}", newword, wordParts[index]);
                        }

                        return newword;
                    }

                    else
                        return wordParts[0];
                }
                else
                {
                    return string.Empty;
                }

            }
            else
            {
                return string.Empty;
            }
        }

        #endregion

    }
}
