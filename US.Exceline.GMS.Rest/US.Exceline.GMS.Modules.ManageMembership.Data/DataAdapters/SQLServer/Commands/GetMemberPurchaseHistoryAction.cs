﻿
// --------------------------------------------------------------------------
// Copyright(c) 2011 UnicornSolutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS.Modules.ManageMembership.Data
// Coding Standard   : US Coding Standards
// Author            : 
// Created Timestamp : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using US.GMS.Core.DomainObjects.ManageShop;
using US_DataAccess;
using System.Data.Common;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetMemberPurchaseHistoryAction : USDBActionBase<ShopSalesDC>
    {
        private readonly int _memberId = -1;
        private readonly DateTime _fromDate = DateTime.Now;
        private readonly DateTime _toDate = DateTime.Now;
        private readonly int _branchId = -1;
        private readonly string _user = string.Empty;

        public GetMemberPurchaseHistoryAction(int memberId, DateTime fromDate, DateTime toDate, int branchId, string user)
        {
            _memberId = memberId;
            _fromDate = fromDate;
            _toDate = toDate;
            _branchId = branchId;
            _user = user;
        }

        protected override ShopSalesDC Body(DbConnection connection)
        {
            const string storedProcedureName = "USExceGMSManageMembershipGetMemberPurchaseHistory";
            ShopSalesDC shopSalesDC = new ShopSalesDC();
            shopSalesDC.ShopSalesItemList = new List<ShopSalesItemDC>();

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MemberId", System.Data.DbType.Int32, _memberId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@PurchaseFromDate", System.Data.DbType.DateTime, _fromDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@PurchaseToDate", System.Data.DbType.DateTime, _toDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@UserName", System.Data.DbType.String, _user));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ShopSalesItemDC salesItemDC = new ShopSalesItemDC();
                    salesItemDC.ShopSalesId = Convert.ToInt32(reader["SalesID"]);
                    salesItemDC.ShopSalesItemId = Convert.ToInt32(reader["ID"]);
                    if (reader["SaleDate"] != DBNull.Value)
                    {
                        salesItemDC.SaleDate = Convert.ToDateTime(reader["SaleDate"]);
                    }
                    if (reader["ArticleNo"] != DBNull.Value)
                    {
                        salesItemDC.ArticleId = Convert.ToInt32(reader["ArticleNo"]);
                    }
                    if (reader["Quantity"] != DBNull.Value)
                    {
                        salesItemDC.Quantity = Convert.ToInt32(reader["Quantity"]);
                    }
                    else
                    {
                        salesItemDC.Quantity = 1;
                    }
                    if (reader["UnitPrice"] != DBNull.Value)
                    {
                        salesItemDC.UnitPrice = Convert.ToDecimal(reader["UnitPrice"]);
                    }
                    else
                    {
                        salesItemDC.UnitPrice = 0.00M;
                    }

                    if (reader["Discount"] != DBNull.Value)
                    {
                        salesItemDC.Discount = Convert.ToDecimal(reader["Discount"]);
                    }
                    else
                    {
                        salesItemDC.Discount = 0.00M;
                    }
                    if (reader["Total"] != DBNull.Value)
                    {
                        salesItemDC.TotalAmount = Convert.ToDecimal(reader["Total"]);
                    }
                    else
                    {
                        salesItemDC.TotalAmount = 0.00M;
                    }

                    if (reader["EmployeeId"] != DBNull.Value)
                    {
                        salesItemDC.EmployeeId = Convert.ToInt32(reader["EmployeeId"]);
                    }
                    salesItemDC.ItemType = Convert.ToString(reader["ItemType"]);
                    salesItemDC.EmployeeName = Convert.ToString(reader["Employee"]);
                    salesItemDC.ItemName = reader["Description"].ToString();
                    salesItemDC.BranchName = reader["BranchName"].ToString();
                    salesItemDC.ItemCategory = reader["CategoryCode"].ToString();
                    salesItemDC.VoucherNo = reader["VoucherNumber"].ToString();
                    if (reader["ExpiryDate"] != DBNull.Value)
                        salesItemDC.ExpiryDate = Convert.ToDateTime(reader["ExpiryDate"]);

                    shopSalesDC.ShopSalesItemList.Add(salesItemDC);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return shopSalesDC;
        }
    }
}
