﻿using System;
using System.Collections.Generic;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Payments;
using System.Data.Common;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetMemberPaymentsHistoryAction : USDBActionBase<List<ExcePaymentInfoDC>>
    {
        private readonly int _memberId = -1;
        private readonly string _paymentType = String.Empty;
        private readonly int _hit = -1;
        private readonly string _user = string.Empty;

        public GetMemberPaymentsHistoryAction(int memberId, string paymentType, int hit, string user)
        {
            _memberId = memberId;
            _paymentType = paymentType;
            _hit = hit;
            _user = user;
        }

        protected override List<ExcePaymentInfoDC> Body(DbConnection connection)
        {
            var paymentsList = new List<ExcePaymentInfoDC>();
            const string spName = "USExceGMSManageMembershipGetPaymentHistory";

            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@memberId", System.Data.DbType.Int32, _memberId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@PaymentType", System.Data.DbType.String, _paymentType));
                command.Parameters.Add(DataAcessUtils.CreateParam("@Hit", System.Data.DbType.String, _hit));
                command.Parameters.Add(DataAcessUtils.CreateParam("@UserName", System.Data.DbType.String, _user));
                DbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ExcePaymentInfoDC payment = new ExcePaymentInfoDC();
                    payment.PaymentArItemNo = Convert.ToInt32(reader["PaymentID"]);
                    if (reader["VoucherDate"] != null)
                        payment.PaymentDateTime = Convert.ToDateTime(reader["VoucherDate"]);
                    if (reader["PaymentRegDate"] != null)
                        payment.PaymentRegDateTime = Convert.ToDateTime(reader["PaymentRegDate"]);
                    payment.PaymentTypeId = Convert.ToInt32(reader["ItemTypeId"]);
                    payment.PaymentType = reader["ItemType"].ToString().Trim();
                    payment.InvoiceNo = reader["Ref"].ToString().Trim();
                    payment.ContractNo = reader["ContractNo"].ToString().Trim();
                    payment.ContractTemplateNo = reader["TemplateNo"].ToString().Trim();
                    payment.PaymentAmount = Convert.ToDecimal(reader["Amount"]);
                    payment.RegisteredEmpName = reader["Createduser"].ToString().Trim();
                    payment.OrderNo = Convert.ToString(reader["OrderNo"]);
                    payment.GymName = Convert.ToString(reader["GymName"]);
                    paymentsList.Add(payment);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return paymentsList;
        }
    }
}
