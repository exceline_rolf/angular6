﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 5/14/2012 5:39:19 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageMemberships;
using System.Data.Common;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetContractsForSponsershipAction : USDBActionBase<List<MemberContractDC>>
    {
        private int _activityId = -1;
        private int _branchId = -1;
        private DateTime _startDate = DateTime.Now;
        private DateTime _endDate = DateTime.Now;

        public GetContractsForSponsershipAction(int activityId, int branchId, DateTime endDate, DateTime startDate)
        {
            _activityId = activityId;
            _branchId = branchId;
            _startDate = startDate;
            _endDate = endDate;
        }

        protected override List<MemberContractDC> Body(System.Data.Common.DbConnection connection)
        {
            List<MemberContractDC> memberContractList = new List<MemberContractDC>();
            string storedProcedureName = "USExceGMSManageMembershipGetContractsForSponsership";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, storedProcedureName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@ContractEndDate", System.Data.DbType.DateTime, _endDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ContractStartDate", System.Data.DbType.DateTime, _startDate));
                command.Parameters.Add(DataAcessUtils.CreateParam("@ActivityId", System.Data.DbType.Int32, _activityId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", System.Data.DbType.Int32, _branchId));
                DbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    MemberContractDC memberContract = new MemberContractDC();
                    memberContract.Id = Convert.ToInt32(reader["ID"]);
                    memberContract.MemberId = Convert.ToInt32(reader["MemberId"]);
                    memberContract.ContractId = Convert.ToInt32(reader["ContractId"]);
                    memberContract.EnrollmentFee = Convert.ToDecimal(reader["EnrollmentFee"]);
                    memberContract.AmountPerInstallment = Convert.ToDecimal(reader["AmountPerInstallment"]);
                    memberContract.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
                    memberContract.ActivityId = Convert.ToInt32(reader["ActivityId"]);
                    memberContract.ExtraActivityName = Convert.ToString(reader["ActivityName"]);
                    memberContract.ContractName = Convert.ToString(reader["ContractName"]);
                    memberContract.MemberName = Convert.ToString(reader["MemberName"]);
                    memberContractList.Add(memberContract);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return memberContractList;
        }
    }
}
