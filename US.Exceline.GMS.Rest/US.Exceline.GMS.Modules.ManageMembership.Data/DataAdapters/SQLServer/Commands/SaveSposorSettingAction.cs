﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 5/8/2012 5:38:41 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US.GMS.Core.DomainObjects.Admin;
using US.GMS.Core.DomainObjects.ManageContracts;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class SaveSponsorSettingAction : USDBActionBase<bool>
    {
        private readonly SponsorSettingDC _sponsorSetting = new SponsorSettingDC();
        private readonly int _sponsorId = -1;
        private readonly string _user = string.Empty;
        private string _gymCode = string.Empty;
        private readonly int _branchId = -1;

        public SaveSponsorSettingAction(SponsorSettingDC sponsorSetting, string user, string gymCode)
        {
            _sponsorSetting = sponsorSetting;
            _gymCode = gymCode;
            _sponsorId = _sponsorSetting.SponsorId;
            _branchId = _sponsorSetting.BranchId;
            _user = user;
        }

        protected override bool Body(DbConnection connection)
        {
            bool result;
            const string storedProcedureName = "USExceGMSManageMembershipSaveSponsorSetting";
            DbTransaction transaction = null;
            try
            {
                transaction = connection.BeginTransaction();
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Transaction = transaction;

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@settingId", DbType.Int32, _sponsorSetting.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@startDate", DbType.DateTime, _sponsorSetting.StartDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsDisplayVisitNo", DbType.Int32, _sponsorSetting.IsDisplayVisitNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsDisplayRemain", DbType.Int32, _sponsorSetting.IsDisplayRemain));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MinAmount", DbType.Decimal, _sponsorSetting.MinAmountAfterDiscount));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MinVisits", DbType.Int32, _sponsorSetting.MinVisits));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@createdUser", DbType.String, _user));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", DbType.Int32, _sponsorSetting.BranchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SponsorId", DbType.Int32, _sponsorSetting.SponsorId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@DiscountTypeId", DbType.Int32, _sponsorSetting.DiscountTypeId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@InvoiceText", DbType.String, _sponsorSetting.InvoiceText));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EnrollmentFeePayer", DbType.String, _sponsorSetting.EnrollmentFeePayer.ToString()));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreditDueDate", DbType.Int32, _sponsorSetting.SponsorDueDay));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@SponsorDueDay", DbType.Int32, _sponsorSetting.SponsorDueDay));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsSponsor", DbType.Boolean, _sponsorSetting.IsSponsor));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsEmailInvoice", DbType.Boolean, _sponsorSetting.IsEmailInvoice));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@IsSponsorWithoutVist", DbType.Int32, _sponsorSetting.IsSponsorWithoutVist));
                if (_sponsorSetting.InvoicingPeriod > 0)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@InvoicingPeriod", DbType.Int32, _sponsorSetting.InvoicingPeriod));
                }
                else
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@InvoicingPeriod", DbType.Int32, 1));
                }
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@EndDate", DbType.DateTime, _sponsorSetting.EndDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@PinCode", DbType.String, _sponsorSetting.PinCode));


                DbParameter outputPara = new SqlParameter();
                outputPara.DbType = DbType.Int32;
                outputPara.ParameterName = "@OutSettingID";
                outputPara.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outputPara);
                cmd.ExecuteNonQuery();

                if (_sponsorSetting.EmployeeCategoryList != null)
                {
                    UpdateEmployeeCategory(_sponsorSetting.EmployeeCategoryList, _user, transaction);
                }

                if (_sponsorSetting.DiscountCategoryList != null)
                {
                    UpdateDiscountCategory(_sponsorSetting.DiscountCategoryList, _sponsorSetting.Id, _sponsorSetting.DiscountTypeId, transaction);
                }

                if (_sponsorSetting.SponsoredMembers != null)
                {
                    UpdateSponsor(_sponsorId, _sponsorSetting.SponsoredMembers, transaction);
                }
                transaction.Commit();

                if (_sponsorSetting.SponsoredMembers != null)
                {
                    foreach (SponsoredMemberDC sponsoredMember in _sponsorSetting.SponsoredMembers)
                    {
                        sponsoredMember.SponsorId = _sponsorId;
                        SaveSponsoredMemberAction saveSponsoredMember = new SaveSponsoredMemberAction(sponsoredMember, _user);
                        saveSponsoredMember.Execute(EnumDatabase.Exceline, _gymCode);
                        // saveSponsoredMember.SaveSponsoredMember(transaction);
                    }
                }

              
                result = true;
            }
            catch (Exception ex)
            {
                if (transaction != null) transaction.Rollback();
                throw ex;
            }
            return result;
        }

        private void UpdateDiscountCategory(List<DiscountDC> discountList, int contractSettingId, int discountTypeId, DbTransaction transaction)
        {
            foreach (DiscountDC discount in discountList)
            {
                discount.BranchId = _branchId;
                discount.TypeId = discountTypeId;
                discount.SponsorId = _sponsorId;
                discount.DiscountType = DiscountType.GROUP;
                discount.CreatedUser = _user;
                discount.IsActive = true;
                SaveDiscountAction saveDiscountAction = new SaveDiscountAction(_user, _branchId, contractSettingId, discount, true);
                saveDiscountAction.SaveDiscountCategory(transaction);
            }
        }

        private void UpdateEmployeeCategory(List<EmployeeCategoryDC> employeeCategoryList, string user, DbTransaction transaction)
        {
            foreach (EmployeeCategoryDC employeeCategory in employeeCategoryList)
            {
                employeeCategory.SponsorId = _sponsorId;
                employeeCategory.BranchId = _branchId;
                SaveSponsorEmployeeCategoryAction updateEmployeeCategoryAction = new SaveSponsorEmployeeCategoryAction(employeeCategory, user);
                updateEmployeeCategoryAction.SaveEmployeeCategory(transaction);
            }
        }

        private void UpdateSponsor(int sponorId, List<SponsoredMemberDC> sponsoredMemberList, DbTransaction transaction)
        {
            foreach (SponsoredMemberDC member in sponsoredMemberList)
            {
                if (member.StartDate.Date <= DateTime.Now.Date & member.EndDate.Date >= DateTime.Now.Date)
                {
                    SaveSponsorAction saveSponsor = new SaveSponsorAction(member.MemberId, sponorId);
                    saveSponsor.SaveSponsor(transaction);
                }
            }
        }
    }
}
