﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 5/7/2012 3:28:56 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using US.GMS.Core.DomainObjects.ManageContracts;
using US_DataAccess;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetSponsorSettingAction : USDBActionBase<SponsorSettingDC>
    {
        private readonly int _branchId;
        private readonly int _sponsorId;

        public GetSponsorSettingAction(int branchId, string user, int sponsorId)
        {
            _branchId = branchId;
            _sponsorId = sponsorId;
        }

        protected override SponsorSettingDC Body(DbConnection connection)
        {
            SponsorSettingDC sponsorSetting = new SponsorSettingDC();
            const string storedProcedureName = "USExceGMSManageMembershipGetSponsorSettings";
            sponsorSetting.SponsorId = _sponsorId;
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@sponsorId", DbType.Int32, _sponsorId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchID", DbType.Int32, _branchId));
                DbDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {

                    sponsorSetting.Id = Convert.ToInt32(reader["Id"]);
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["MinAmount"]).Trim()))
                    {
                        sponsorSetting.MinAmountAfterDiscount = Convert.ToDecimal(reader["MinAmount"]);
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(reader["IsDisplayVisitNo"]).Trim()))
                    {
                        sponsorSetting.IsDisplayVisitNo = ((Convert.ToInt32(reader["IsDisplayVisitNo"])) == 1) ? true : false;
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(reader["IsDisplayRemain"]).Trim()))
                    {
                        sponsorSetting.IsDisplayRemain = ((Convert.ToInt32(reader["IsDisplayRemain"])) == 1) ? true : false;
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(reader["IsSponsorWithoutVist"]).Trim()))
                    {
                        sponsorSetting.IsSponsorWithoutVist = ((Convert.ToInt32(reader["IsSponsorWithoutVist"])) == 1) ? true : false;
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["CreditDueDate"]).Trim()))
                    {
                        sponsorSetting.SponsorDueDay = Convert.ToInt32(reader["CreditDueDate"]);
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(reader["SponsorDueDay"]).Trim()))
                    {
                        sponsorSetting.SponsorDueDay = Convert.ToInt32(reader["SponsorDueDay"]);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["InvoicingPeriod"]).Trim()))
                    {
                        sponsorSetting.InvoicingPeriod = Convert.ToInt32(reader["InvoicingPeriod"]);
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(reader["StartDate"]).Trim()))
                    {
                        sponsorSetting.StartDate = Convert.ToDateTime(reader["StartDate"]);
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(reader["EndDate"]).Trim()))
                    {
                        sponsorSetting.EndDate = Convert.ToDateTime(reader["EndDate"]);
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(reader["InvoiceText"]).Trim()))
                    {
                        sponsorSetting.InvoiceText = Convert.ToString(reader["InvoiceText"]);
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(reader["LastSponsoredMembersCount"]).Trim()))
                    {
                        sponsorSetting.LastSponsoredMembersCount = Convert.ToInt32(reader["LastSponsoredMembersCount"]);
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(reader["PinCode"]).Trim()))
                    {
                        sponsorSetting.PinCode = Convert.ToString(reader["PinCode"]);
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(reader["IsEmailInvoice"]).Trim()))
                    {
                        sponsorSetting.IsEmailInvoice = Convert.ToBoolean(reader["IsEmailInvoice"]);
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(reader["BranchId"]).Trim()))
                    {
                        sponsorSetting.BranchId = Convert.ToInt32(reader["BranchId"]);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["SponsorId"]).Trim()))
                    {
                        sponsorSetting.SponsorId = Convert.ToInt32(reader["SponsorId"]);
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(reader["PecentageModeId"]).Trim()))
                    {
                        sponsorSetting.PercentageModeId = Convert.ToInt32(reader["PecentageModeId"]);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["AmountModeId"]).Trim()))
                    {
                        sponsorSetting.AmountModeId = Convert.ToInt32(reader["AmountModeId"]);
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(reader["MinVisits"]).Trim()))
                    {
                        sponsorSetting.MinVisits = Convert.ToInt32(reader["MinVisits"]);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["DiscountTypeId"]).Trim()))
                    {
                        sponsorSetting.DiscountTypeId = Convert.ToInt32(reader["DiscountTypeId"]);
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(reader["CurrentMemberCount"]).Trim()))
                    {
                        sponsorSetting.CurrentMembersCount = Convert.ToInt32(reader["CurrentMemberCount"]);
                    }
                    sponsorSetting.DiscountTypeName = Convert.ToString(reader["TypeName"]);

                    if (!string.IsNullOrEmpty(Convert.ToString(reader["EnrollmentFeePayer"]).Trim()))
                    {
                        sponsorSetting.EnrollmentFeePayer = (EnrollmentFeePayerType)Enum.Parse(typeof(EnrollmentFeePayerType), reader["EnrollmentFeePayer"].ToString());
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(reader["IsSponsor"]).Trim()))
                    {
                        sponsorSetting.IsSponsor = Convert.ToBoolean(reader["IsSponsor"]);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return sponsorSetting;
        }
    }
}
