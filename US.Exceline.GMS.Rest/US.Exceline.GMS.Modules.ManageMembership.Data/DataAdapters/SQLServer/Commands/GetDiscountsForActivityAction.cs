﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "6/18/2012 10:39:55
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.ManageContracts;
using System.Data.Common;
using System.Data;
using US.GMS.Core.DomainObjects.Admin;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class GetDiscountsForActivityAction : USDBActionBase<List<DiscountDC>>
    {
        private int _branchId;
        private int _activityId;

        public GetDiscountsForActivityAction(int branchId, int activityId)
        {
            this._branchId = branchId;
            this._activityId = activityId;
        }

        protected override List<DiscountDC> Body(DbConnection connection)
        {
            List<DiscountDC> _discountCategoryList = new List<DiscountDC>();
            string StoredProcedureName = "USExceGMSManageMembershipGetDiscountsForActivity";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchID", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@activityId", DbType.Int32, _activityId));


                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    DiscountDC _discountcategory = new DiscountDC();
                    _discountcategory.DiscountType = DiscountType.ACTIVITY;
                    //[Name]
                    _discountcategory.Name = Convert.ToString(reader["Name"]);
                    //[ID]
                    _discountcategory.Id = Convert.ToInt32(reader["ID"]);
                    //[MinMembersCount]
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["MinMembersCount"]).Trim()))
                    {
                        _discountcategory.MinMemberNo = Convert.ToInt32(reader["MinMembersCount"]);
                    }
                    //[Discount]
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["Discount"]).Trim()))
                    {
                        _discountcategory.DiscountPercentage = Convert.ToDecimal(reader["Discount"]);
                    }
                    //[ActivityId]
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["ActivityId"]).Trim()))
                    {
                        _discountcategory.ActivityId = Convert.ToInt32(reader["ActivityId"]);
                    }
                    //[GroupId]
                    //if (!string.IsNullOrEmpty(Convert.ToString(reader["GroupId"]).Trim()))
                    //{
                    //    _discountcategory.GroupId = Convert.ToInt32(reader["GroupId"]);
                    //}
                    // [TrailTime]
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["TrailTime"]).Trim()))
                    {
                        _discountcategory.TrailTime = Convert.ToDateTime(reader["TrailTime"]);
                    }
                    //[IsActive]
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["IsActive"]).Trim()))
                    {
                        _discountcategory.IsActive = Convert.ToBoolean(reader["IsActive"]);
                    }
                    //_discountcategory.GroupHeader = "Group ID:" + _discountcategory.GroupId.ToString();
                    _discountCategoryList.Add(_discountcategory);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _discountCategoryList;
        }
    }
}
