﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class SaveEconomyDetailsAction : USDBActionBase<bool>
    {
        private OrdinaryMemberDC _member = new OrdinaryMemberDC();
        private string _user = string.Empty;

        public SaveEconomyDetailsAction(OrdinaryMemberDC member, string user)
        {
            _member = member;
            _user = user;
        }

        protected override bool Body(DbConnection connection)
        {
            bool result = false;
            const string storedProcedureName = "USExceGMSManageMembershipSaveEconomyDetails";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _member.Id));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@accountNo", DbType.String, _member.AccountNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@invoiceChage", DbType.Boolean, _member.InvoiceChage));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@smsInvoice", DbType.Boolean, _member.SmsInvoice));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@ref", DbType.String, _member.Ref));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@employeeNo", DbType.String, _member.EmployeeNo));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@User", DbType.String, _user));
                if (_member.GuardianId > 0)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@guardianId", DbType.Int32, _member.GuardianId));
                }
               
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@sponserId", DbType.Int32, _member.SponserId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@startDate", DbType.DateTime, _member.SponsorStartDate));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@endDate", DbType.DateTime, _member.SponsorEndDate));

                
                if (_member.EmployeeCategoryForSponser != null)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@sponserCategoryId", DbType.Int32, _member.EmployeeCategoryForSponser.Id));
                else
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@sponserCategoryId", DbType.Int32,-1));

                if (_member.CreditPeriod > 0)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@CreditPeriod", DbType.Int32, _member.CreditPeriod));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@sponseringId", DbType.Int32, _member.SponsoringId));

                if(_member.DueDate > 0)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@DueDate", DbType.Int32, _member.DueDate));
                    

                cmd.ExecuteNonQuery();
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;

            }
            return result;
        }
    }
}
