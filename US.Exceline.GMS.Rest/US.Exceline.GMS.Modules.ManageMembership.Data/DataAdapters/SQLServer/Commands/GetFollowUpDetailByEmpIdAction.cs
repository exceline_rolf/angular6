﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Common;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer
{
    class GetFollowUpDetailByEmpIdAction : USDBActionBase<List<FollowUpDetailDC>>
    {
        private readonly int _empId = -1;

        public GetFollowUpDetailByEmpIdAction(int empId)
        {
            _empId = empId;
        }

        protected override List<FollowUpDetailDC> Body(DbConnection connection)
        {
            List<FollowUpDetailDC> followUpDetailList = new List<FollowUpDetailDC>();
            const string storedProcedureName = "USExceGMSGetFollowUpDetailByEmpId";
            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@AssignedEmpId", DbType.Int32, _empId));

                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    FollowUpDetailDC followUpDetail = new FollowUpDetailDC();

                    followUpDetail.Id = Convert.ToInt32(reader["Id"]);
                    followUpDetail.Day = Convert.ToDateTime(reader["Day"]).DayOfWeek.ToString();
                    followUpDetail.PlannedDate = Convert.ToDateTime(reader["PlannedDate"]);
                    followUpDetail.StartTime = Convert.ToDateTime(reader["StartTime"]);
                    followUpDetail.EndTime = Convert.ToDateTime(reader["EndTime"]);
                    followUpDetail.TaskCategoryId = Convert.ToInt32(reader["TaskCategoryId"]);
                    followUpDetail.TaskCategoryName = Convert.ToString(reader["TaskCategoryName"]);
                    followUpDetail.Name = Convert.ToString(reader["Name"]);
                    followUpDetail.Member = Convert.ToString(reader["Member"]);
                    followUpDetail.Status = Convert.ToBoolean(reader["Status"]) ? "Done" : "To Be Done";
                    followUpDetail.Description = Convert.ToString(reader["Description"]);
                    followUpDetail.AutomatedSMS = Convert.ToString(reader["AutomatedSMS"]);
                    followUpDetail.AutomatedEmail = Convert.ToString(reader["AutomatedEmail"]);
                    followUpDetail.CompletedDate = Convert.ToDateTime(reader["CompletedDate"]);
                    followUpDetail.CompletedEmpName = Convert.ToString(reader["CompletedEmpName"]);
                    followUpDetail.Comment = Convert.ToString(reader["Comment"]);
                    followUpDetail.BranchId = Convert.ToInt32(reader["BranchId"]);
                    followUpDetail.BranchName = reader["BranchName"].ToString();
                    followUpDetailList.Add(followUpDetail);
                }
            }

            catch (Exception ex)
            {
                throw new NotImplementedException("", ex);
            }
            return followUpDetailList;
        }
    }
}
