﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.ManageMembership.Data.DataAdapters.SQLServer.Commands
{
    public class ValidateMemberWithStatusAction : USDBActionBase<int>
    {
        private readonly int _statusId = -1;
        private readonly int _memberID = -1;
        private string _user = string.Empty;

        public ValidateMemberWithStatusAction(int memberId, int statusId, string user)
        {
            _statusId = statusId;
            _memberID = memberId;
            _user = user;
        }

        protected override int Body(DbConnection connection)
        {
            const string storedProcedureName = "USExceGMSMemberValidateChangeStatus";
            try
            {
                var cmd = CreateCommand(CommandType.StoredProcedure, storedProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@StatusId", DbType.Int32, _statusId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@MemberId", DbType.Int32, _memberID));

                DbParameter output = new SqlParameter();
                output.DbType = DbType.Int32;
                output.ParameterName = "@validateResult";
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);

                cmd.ExecuteNonQuery();

                if (output.Value != null)
                    return Convert.ToInt32(output.Value);
                return 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
