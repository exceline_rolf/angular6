﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using US.Common.Logging;
using US.Exceline.GMS.Modules.Economy.Core.DomainObjects;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.Utils;
using US.Exceline.GMS.Modules.Economy.BusinessLogic;
using System.Web;
using System.Runtime.Serialization;

namespace US.Exceline.GMS.Modules.Economy.RestApi.Controllers
{
    [RoutePrefix("invoice")]
    public class InvoiceController : ApiController
    {
        [HttpGet]
        [Route("SearchInvoices")]
        [Authorize]
        public HttpResponseMessage SearchInvoices(US.Exceline.GMS.Modules.Economy.Core.SystemObjects.InvoiceSearchCriteria searchCriteria, US.Exceline.GMS.Modules.Economy.Core.SystemObjects.SearchModes searchMode, object constValue)
        {
            string user = Request.Headers.GetValues("UserName").First();
            OperationResult<List<USPARItem>> result = US.Exceline.GMS.Modules.Economy.API.Invoice.AdvanaceInvoiceSearchXML(searchCriteria, searchMode, constValue, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                List<string> errorMsg = new List<string>();
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogger.Log(message.Message, new List<string> { "Payment", "Service" }, SeverityFilter.Error, null);
                    errorMsg.Add(message.Message);
                }
                return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<USPARItem>(), ApiResponseStatus.ERROR, errorMsg));
            }
            return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
        }

        [HttpGet]
        [Route("GeneralInvoiceSearch")]
        [Authorize]
        public HttpResponseMessage SearchGeneralInvoiceData(string searchValue, int invoiceType, string fieldType, US.Exceline.GMS.Modules.Economy.Core.SystemObjects.SearchModes searchMode, string constValue, int paymentID)
        {
            string user = Request.Headers.GetValues("UserName").First();
            OperationResult<List<USPARItem>> result = US.Exceline.GMS.Modules.Economy.API.Invoice.SearchGeneralInvoiceDataXML(searchValue, invoiceType, fieldType, searchMode, constValue, ExceConnectionManager.GetGymCode(user), paymentID);
            if (result.ErrorOccured)
            {
                List<string> errorMsg = new List<string>();
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogger.Log(message.Message, new List<string> { "Payment", "Service" }, SeverityFilter.Error, null);
                    errorMsg.Add(message.Message);
                }
                return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<USPARItem>(), ApiResponseStatus.ERROR, errorMsg));
            }
            return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
        }

        [HttpGet]
        [Route("GetConfigSettings")]
        [Authorize]
        public HttpResponseMessage GetConfigSettings(string type)
        {
            switch (type)
            {
                case "RPT":
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(ConfigurationManager.AppSettings["ReportViewerURL"].ToString(), ApiResponseStatus.OK));
                case "EXL":
                    return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(ConfigurationManager.AppSettings["ExorLiveURL"].ToString(), ApiResponseStatus.OK));
            }
            return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(string.Empty, ApiResponseStatus.ERROR, new List<string>() { "not found" }));
        }

        [HttpGet]
        [Route("GetInvoice")]
        [Authorize]
        public HttpResponseMessage GetInvoice(String orderno, bool isBris, int branchId = 0, int sponsorAritemNo = 0, Boolean anonymizeNames = false)
        {
            string user = Request.Headers.GetValues("UserName").First();
            byte[] pdfBytes = null;
            if (isBris == true)
            {
                SiOInvoiceGenerator Gen = new SiOInvoiceGenerator();
                Gen.Main(orderno);
                pdfBytes = Gen.GetResult();
                var response = new HttpResponseMessage(HttpStatusCode.OK);
            //    response.Content = new ByteArrayContent(pdfBytes);
            //    response.Content.Headers.Add("x-filename", "myPDF.pdf");
          //      response.Content.Headers.Add("Content-Type", "application/pdf");
            }
            else if (sponsorAritemNo > 0)
            {
                ExcelineSponsorInvoiceGenerator spons = new ExcelineSponsorInvoiceGenerator(sponsorAritemNo, branchId, ExceConnectionManager.GetGymCode(user), anonymizeNames);
                 pdfBytes = spons.GetResult();
                 var response = new HttpResponseMessage(HttpStatusCode.OK);
            }
            else
            {
                ExcelineInvoiceGenerator Gen = new ExcelineInvoiceGenerator(orderno, branchId, ExceConnectionManager.GetGymCode(user));           
                pdfBytes = Gen.GetResult();
                var response = new HttpResponseMessage(HttpStatusCode.OK);
            }

              return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(pdfBytes, ApiResponseStatus.OK));
          
        }
    }
}
