﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Exceline.GMS.Modules.Economy.RestApi.Models
{
    public class SessionDetails
    {
        public string sessionKey { get; set; }
        public string value { get; set; }
    }
}