﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using US.Common.Web.UI.Core.SystemObjects;
using US.Exceline.GMS.Modules.Operations.Data.DataAdapters.SQLServer;
using US.Exceline.GMS.Modules.Operations.Data.SystemObjects;
using US.GMS.Core.DomainObjects.AccessControl;
using US.GMS.Core.DomainObjects.AccessControl.ARX;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.ResultNotifications;

namespace US.Exceline.GMS.Modules.Operations.Data.DataAdapters.ManageOperations
{
    public class ManageOperationsFacade
    {

        private static IOperationsDataAdapter GetDataAdapter()
        {
            return new SQLServerOperationsDataAdapter();
        }

        public static List<ExcACCCommand> GetCommands(int branchId, string gymCode)
        {
           return GetDataAdapter().GetCommands(branchId,gymCode);
        }

        public static ExceACCSettings GetSettings(int branchId, string gymCode)
        {
            return GetDataAdapter().GetSettings(branchId, gymCode);
        }

        public static List<ExceACCAccessProfileTime> GetAccessTimes(int branchId, string gymCode, string dateFromat)
        {
            return GetDataAdapter().GetAccessTimes(branchId, gymCode, dateFromat);
        }

       
        public static List<GymACCOpenTime> GetOpenTimes(int branchId, string gymCode)
        {
            return GetDataAdapter().GetOpenTimes(branchId, gymCode);
        }

        public static List<ExceACCGymClosedTime> GetClosedTimes(int branchId, string gymCode)
        {
            return GetDataAdapter().GetClosedTimes(branchId, gymCode);
        }

        public static List<ExceACCMember> GetMembers(int branchId, string gymCode)
        {
            return GetDataAdapter().GetMembers(branchId, gymCode);
        }

        public static string GetImage(int branchId, string gymCode, string customerNo)
        {
            return GetDataAdapter().GetImage(branchId, gymCode,customerNo);
        }

        public static List<person> GetARXMembers(int branchId, string gymCode, bool isAllMember)
        {
            return GetDataAdapter().GetARXMembers(branchId, gymCode, isAllMember);
        }

        public static int GetSystemId(string gymCode)
        {
            return GetDataAdapter().GetSystemId(gymCode);
        }

        public static int AddVisitList(string gymCode, List<EntityVisitDC> visitList)
        {
            return GetDataAdapter().AddVisitList(gymCode, visitList);
        }

        public static int RegisterControl(ExcACCAccessControl accessControl, string gymCode)
        {
            return GetDataAdapter().RegisterControl(accessControl, gymCode);
        }

        public static int PingServer(int terminalId, string gymCode)
        {
            return GetDataAdapter().PingServer(terminalId, gymCode);
        }

        public static int AddEventLogs(ExcAccessEvent accessEvent, string gymCode)
        {
            return GetDataAdapter().AddEventLogs(accessEvent, gymCode);
        }

        public static ExceACCTerminal GetTerminalDetails(int terminalID, string gymCode)
        {
            return GetDataAdapter().GetTerminalDetails(terminalID, gymCode);
        }

        public static int GetAvailableVisitByContractId(int contractId, string gymCode)
        {
            return GetDataAdapter().GetAvailableVisitByContractId(contractId, gymCode);
        }
    }
}
