﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using US.Common.Notification.API;
using US.Common.Web.UI.Core.SystemObjects;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.Notification;
using US.GMS.Core.ResultNotifications;
using US.Payment.Core.ResultNotifications;
using US_DataAccess;
using US.Common.Notification.Core.DomainObjects;
using US.Common.Logging.API;
using US.GMS.Core.Utils;
using US.GMS.Core.DomainObjects.AccessControl;
using US.GMS.Data.DataAdapters.SQLServer.Commands;
using US.GMS.Core.DomainObjects.Common;
using US.Exceline.GMS.Modules.Admin.Data.DataAdapters.SQLServer.Commands;

namespace US.Exceline.GMS.Modules.Operations.Data.DataAdapters.SQLServer.Commands
{
    public class GetMemberLoginDataPerDayAction : USDBActionBase<List<ExceACCMember>>
    {
        private DateTime _selectedDate;
        private int _branchId;
        public GetMemberLoginDataPerDayAction(DateTime selectedDate, string gymCode, int branchId)
        {
            this._selectedDate = selectedDate;
            this._branchId = branchId;
        }
        protected override List<ExceACCMember> Body(System.Data.Common.DbConnection connection)
        {
            List<ExceACCMember> memberList = new List<ExceACCMember>();
            string StoredProcedureName = "USExceGMSGetAuthorizedMemberPerDay";
            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchID", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@selectedDate", DbType.DateTime, _selectedDate));

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ExceACCMember accessMember = new ExceACCMember();
                    accessMember.Id = Convert.ToInt32(reader["ID"]);
                    accessMember.MemberCardNo = Convert.ToString(reader["MemberCardNo"]);
                    accessMember.Name = Convert.ToString(reader["Name"]);
                    accessMember.CustId = Convert.ToString(reader["CustId"]);
                    accessMember.HomeGym = Convert.ToString(reader["HomeGym"]);
                    accessMember.Gender = Convert.ToString(reader["Gender"]);
                    if (reader["Born"] != DBNull.Value)
                        accessMember.Born = Convert.ToDateTime(reader["Born"]).ToString("yyyy.MM.dd"); 
                    if(reader["LastVisitDate"] != DBNull.Value)
                        accessMember.LastVisitDate = Convert.ToDateTime(reader["LastVisitDate"]).ToString("yyyy.MM.dd"); 
                    accessMember.CreditBalance = Convert.ToDecimal(reader["CreditBalance"]);
                    accessMember.GuestCardNo = Convert.ToString(reader["GuestCardNo"]);
                    accessMember.MemberContractNo = Convert.ToString(reader["MemberContractNo"]);
                    accessMember.MemberContractId = Convert.ToInt32(reader["memberContractId"]);
                    if (reader["ContractStartDate"] != DBNull.Value)
                        accessMember.ContractStartDate = Convert.ToDateTime(reader["ContractStartDate"]).ToString("yyyy.MM.dd");
                    if (reader["ContractEndDate"] != DBNull.Value)
                        accessMember.ContractEndDate = Convert.ToDateTime(reader["ContractEndDate"]).ToString("yyyy.MM.dd");

                    accessMember.TemplateNo = Convert.ToString(reader["TemplateNo"]);
                    accessMember.TemplateName = Convert.ToString(reader["TemplateName"]);
                    accessMember.ContractType = Convert.ToString(reader["ContractType"]);
                    accessMember.AvailableVisits = Convert.ToInt32(reader["AvailableVisits"]);
                    accessMember.AccessProfileId = Convert.ToInt32(reader["AccessProfileID"]);
                    accessMember.ShopAccBalance = Convert.ToDecimal(reader["AccountBalance"]);
                    if (reader["AntiDopingSignedDate"] != DBNull.Value)
                        accessMember.AntiDopingDate = Convert.ToDateTime(reader["AntiDopingSignedDate"]).ToString("yyyy.MM.dd");
                    accessMember.IsFreezed = Convert.ToString(reader["IsFreezed"]);
                    accessMember.IsContractATG = Convert.ToString(reader["IsContractATG"]);
                    accessMember.MemberATGStatus = Convert.ToString(reader["MemberATGStatus"]);
                    accessMember.ActivityId = Convert.ToInt32(reader["ActivityId"]);
                    accessMember.MemberMessages = Convert.ToString(reader["MemberMessages"]);
                    if (reader["FreezeToDate"] != DBNull.Value)
                         accessMember.FreezeToDate = Convert.ToDateTime(reader["FreezeToDate"]).ToString("yyyy.MM.dd");
                    accessMember.ActivityName = reader["ActivityName"].ToString();
                    accessMember.Mobile = reader["Mobile"].ToString().Trim();
                    accessMember.UnPaidInvoiceCount = Convert.ToInt32(reader["UnPaidInvoiceCount"]);
                    accessMember.MinutesBetweenSwipes = Convert.ToInt32(reader["AccMinutesBetweenAccess"]);
                    accessMember.MinimumUnpaidInvoices = Convert.ToInt32(reader["MinimumUnpaidInvoices"]);
                    accessMember.UnPaidDueBalance = Convert.ToDecimal(reader["UnPaidDueBalance"]);
                    accessMember.MinimumOnAccountBalance = Convert.ToDecimal(reader["MinimumOnAccountBalance"]);
                    accessMember.IsSendSms = Convert.ToBoolean(reader["IsSendSms"]);
                    accessMember.MinimumPunches = Convert.ToInt32(reader["MinimumPunches"]);


                    memberList.Add(accessMember);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return memberList;
        }
    }
}
