﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Operations.Data.DataAdapters.SQLServer.Commands
{
    public class PingServerAction : USDBActionBase<int>
    {
        private int _terminalId = -1;
        public PingServerAction(int terminalId)
        {
            _terminalId = terminalId;
        }

        protected override int Body(System.Data.Common.DbConnection connection)
        {
            string spName = "ExceGMSACCPingServer";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@TerminalId", System.Data.DbType.Int32, _terminalId));
                command.ExecuteNonQuery();
                return 1;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
