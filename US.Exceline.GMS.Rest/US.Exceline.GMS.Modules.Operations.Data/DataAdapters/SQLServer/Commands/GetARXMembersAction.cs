﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.AccessControl.ARX;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Operations.Data.DataAdapters.SQLServer.Commands
{
    public class GetARXMembersAction : USDBActionBase<List<person>>
    {
        private int _branchId = -1;
        private bool _isAllMember;
        public GetARXMembersAction(int branchID, bool isAllMember)
        {
            _branchId = branchID;
            _isAllMember = isAllMember;
        }

        protected override List<person> Body(DbConnection connection)
        {
            string spName = "ExceGMSACCGetARXMembers";
            var persons = new List<person>();
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@BranchId", System.Data.DbType.Int32, _branchId));
                command.Parameters.Add(DataAcessUtils.CreateParam("@IsAllMember", System.Data.DbType.Boolean, _isAllMember));
                DbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    person per = new person();
                    //int memberId = Convert.ToInt32(reader["MemberId"]);
                    per.id = Convert.ToString(reader["CustId"]);
                    per.first_name = Convert.ToString(reader["FirstName"]);
                    per.last_name = Convert.ToString(reader["LastName"]);
                    if (reader["PinCode"] != DBNull.Value)
                    per.PinCode = Convert.ToString(reader["PinCode"]);

                    per.AccessProfileID = Convert.ToInt32(reader["AccessProfileId"]);
                    per.CardNo = Convert.ToString(reader["CardNo"]);
                    per.BranchName = Convert.ToString(reader["BranchName"]);
                    per.FormatName = Convert.ToString(reader["FormatName"]);
                    per.Deleted = Convert.ToBoolean(reader["Deleted"]);
                    per.BranchID = Convert.ToInt32(reader["BranchID"]);
                    per.EndDate = Convert.ToDateTime(reader["EndDate"]).ToString("yyyy-MM-dd");
                    per.AccessProfileName = Convert.ToString(reader["AccessProfileName"]);

                    //var action = new GetArxCardNumberAction(memberId);
                    //per.CardList = action.Execute(EnumDatabase.Exceline, _gymCode); ;

                    persons.Add(per);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return persons;
        }
    }
}
