﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US_DataAccess;

namespace US.Exceline.GMS.Modules.Operations.Data.DataAdapters.SQLServer.Commands
{
    public class AddVisitAction : USDBActionBase<int>
    {
        private List<EntityVisitDC> _visitLst;
        public AddVisitAction(List<EntityVisitDC> visitList)
        {
            _visitLst = visitList;
        }
        protected override int Body(DbConnection connection)
        {
            int result = -1;
            const string spName = "USExceGMSAddVisit";
            try
            {
                DbCommand command = CreateCommand(CommandType.StoredProcedure, spName);
                command.Parameters.Add(_visitLst != null
                                           ? DataAcessUtils.CreateParam("@MemberVisit", SqlDbType.Structured,
                                                                        GetDataTable(_visitLst))
                                           : DataAcessUtils.CreateParam("@MemberVisit", SqlDbType.Structured,
                                                                        null));
                command.ExecuteNonQuery();
                result = 1;
            }
            catch 
            {
            }
            return result;
        }

        private DataTable GetDataTable(IEnumerable<EntityVisitDC> visitList)
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn("ID", typeof(int)));
            dataTable.Columns.Add(new DataColumn("CustomerNo", typeof(string)));
            dataTable.Columns.Add(new DataColumn("BranchId", typeof(int)));
            dataTable.Columns.Add(new DataColumn("InTime", typeof(DateTime)));
            dataTable.Columns.Add(new DataColumn("VisitDate", typeof(DateTime)));
            dataTable.Columns.Add(new DataColumn("CountVisit", typeof(bool)));
            dataTable.Columns.Add(new DataColumn("ContractID", typeof(int)));
            dataTable.Columns.Add(new DataColumn("ArticleId", typeof(int)));
            dataTable.Columns.Add(new DataColumn("VisitType", typeof(string)));
            dataTable.Columns.Add(new DataColumn("Description", typeof(string)));
            dataTable.Columns.Add(new DataColumn("ActivityId", typeof(int)));
            dataTable.Columns.Add(new DataColumn("ControllerId", typeof(int)));

            int count = 1;
            foreach (EntityVisitDC item in visitList)
            {
                if (item.VisitDate > DateTime.MinValue || item.InTime > DateTime.MinValue)
                {
                    if (item.BranchId > 0)
                    {
                        DataRow dataTableRow = dataTable.NewRow();
                        dataTableRow["ID"] = count;
                        dataTableRow["CustomerNo"] = item.CutomerNo;
                        dataTableRow["BranchId"] = item.BranchId;
                        if (item.InTime > DateTime.MinValue)
                            dataTableRow["InTime"] = item.InTime;
                        else
                            dataTableRow["InTime"] = item.VisitDate;

                        if (item.VisitDate > DateTime.MinValue)
                            dataTableRow["VisitDate"] = item.VisitDate;
                        else
                            dataTableRow["VisitDate"] = item.InTime;

                        dataTableRow["CountVisit"] = item.CountVist;
                        dataTableRow["ContractID"] = item.MemberContractId;
                        dataTableRow["ArticleId"] = item.ArticleID;
                        dataTableRow["VisitType"] = item.VisitType;
                        dataTableRow["Description"] = item.ItemName;
                        dataTableRow["ActivityId"] = item.ActivityId;
                        dataTableRow["ControllerId"] = item.ControllerId;
                        dataTable.Rows.Add(dataTableRow);
                        count++;
                    }
                }
            }
            return dataTable;
        }

    }
}
