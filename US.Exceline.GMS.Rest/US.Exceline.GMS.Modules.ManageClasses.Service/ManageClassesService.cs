﻿using System;
using System.Collections.Generic;
using US.Exceline.GMS.Modules.ManageClasses.API;
using US.GMS.Core.DomainObjects.ManageClasses;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.Exceline.GMS.Modules.Admin.API.ManageEmployees;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.Exceline.GMS.Modules.ManageMembership.API.ManageMembership;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US.Exceline.GMS.Modules.Admin.API.ManageResources;
using US.Common.Logging.API;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.API;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.Utils;

namespace US.Exceline.GMS.Modules.ManageClasses.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class ManageClassesService : IManageClassesService
    {

        #region Class

        public int SaveClass(ExcelineClassDC excelineClass, string user)
        {
            OperationResult<int> result = new OperationResult<int>();
            result = GMSClass.SaveClass(excelineClass, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ExcelineClassDC> GetClasses(string className, int branchId, string user)
        {
            OperationResult<List<ExcelineClassDC>> result = new OperationResult<List<ExcelineClassDC>>();
            result = GMSClass.GetClasses(className, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExcelineClassDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool UpdateClass(ExcelineClassDC excelineClass, string user)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            result = GMSClass.UpdateClass(excelineClass, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool DeleteClass(int classId, string user)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            result = GMSClass.DeleteClass(classId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<TrainerDC> GetTrainersForClass(int branchId, string searchText, string user)
        {
            int trainerId = -1;
            OperationResult<List<TrainerDC>> trainerLst = GMSTrainer.GetTrainers(branchId, searchText, true, trainerId, ExceConnectionManager.GetGymCode(user));
            if (trainerLst.ErrorOccured)
            {
                foreach (NotificationMessage message in trainerLst.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<TrainerDC>();
            }
            else
            {
                return trainerLst.OperationReturnValue;
            }
        }

        public List<InstructorDC> GetInstructorsForClass(int branchId, string searchText, string user)
        {
            int instructorId = -1;
            OperationResult<List<InstructorDC>> instructorLst = GMSInstructor.GetInstructors(branchId, searchText, true, instructorId, ExceConnectionManager.GetGymCode(user));
            if (instructorLst.ErrorOccured)
            {
                foreach (NotificationMessage message in instructorLst.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<InstructorDC>();
            }
            else
            {
                return instructorLst.OperationReturnValue;
            }
        }

        public string GetMembersForClass(int branchId, string searchText, string user,int hit)
        {
            int status = 1;  //active member status=1;
            OperationResult<string> result = GMSManageMembership.GetMembers(branchId, searchText, status, MemberSearchType.CLASS, MemberRole.MEM, ExceConnectionManager.GetGymCode(user), hit);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return US.GMS.Core.Utils.XMLUtils.SerializeDataContractObjectToXML(new List<ExcelineMemberDC>());
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ResourceDC> GetResourcesForClass(int branchId, string searchText, string user)
        {
            OperationResult<List<ResourceDC>> resourceLst = GMSResources.GetResources(branchId, searchText, -1, true, ExceConnectionManager.GetGymCode(user));
            if (resourceLst.ErrorOccured)
            {
                foreach (NotificationMessage message in resourceLst.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ResourceDC>();
            }
            else
            {
                return resourceLst.OperationReturnValue;
            }
        }

        public int GetNextClassId(string user)
        {
            OperationResult<int> result = new OperationResult<int>();
            result = GMSClass.GetNextClassId(ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ExcelineMemberDC> GetMembersForClassUpdate(int classId, string user)
        {
            OperationResult<List<ExcelineMemberDC>> members = GMSClass.GetMembersForClass(classId, ExceConnectionManager.GetGymCode(user));
            if (members.ErrorOccured)
            {
                foreach (NotificationMessage message in members.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExcelineMemberDC>();
            }
            else
            {
                return members.OperationReturnValue;
            }
        }

        public List<TrainerDC> GetTrainersForClassUpdate(int classId, string user)
        {
            OperationResult<List<TrainerDC>> result = new OperationResult<List<TrainerDC>>();
            result = GMSClass.GetTrainersForClass(classId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<TrainerDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<InstructorDC> GetInstructorsForClassUpdate(int classId, string user)
        {
            OperationResult<List<InstructorDC>> result = new OperationResult<List<InstructorDC>>();
            result = GMSClass.GetInstructorsForClass(classId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<InstructorDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ResourceDC> GetResourcesForClassUpdate(int classId, string user)
        {
            OperationResult<List<ResourceDC>> result = new OperationResult<List<ResourceDC>>();
            result = GMSClass.GetResourceForClass(classId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ResourceDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<OrdinaryMemberDC> GetMembersForClassBooking(int classId, int branchId, string searchText, string user)
        {
            OperationResult<List<OrdinaryMemberDC>> result = new OperationResult<List<OrdinaryMemberDC>>();
            result = GMSClass.GetMembersForClassBooking(classId, branchId, searchText, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<OrdinaryMemberDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ClassBookingActiveTimeDC> GetClassActiveTimesForBooking(int branchId, DateTime startTime, DateTime endTime, int entNO, string user)
        {
            OperationResult<List<ClassBookingActiveTimeDC>> result = GMSClass.GetClassActiveTimesForBooking(branchId, startTime, endTime, entNO, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ClassBookingActiveTimeDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<MemberBookingDetailsDC> GetMemberBookingDetails(int classId, int branchId, string user)
        {
            OperationResult<List<MemberBookingDetailsDC>> result = GMSClass.GetMemberBookingDetails(classId, branchId, user, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<MemberBookingDetailsDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int SaveMemberBookingDetails(int classId, OrdinaryMemberDC ordinaryMemberDC, int memberId, List<ClassBookingDC> bookingList, decimal totalBookingAmount, decimal totalAvailableAmount, string user, int branchId, string scheduleCategoryType)
        {
            OperationResult<int> result = GMSClass.SaveMemberBookingDetails(classId, ordinaryMemberDC, memberId, bookingList, totalBookingAmount, totalAvailableAmount, user, branchId, scheduleCategoryType, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return 0;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<CategoryDC> GetCategories(string type, string user, int branchId)
        {
            OperationResult<List<CategoryDC>> result = GMSCategory.GetCategoriesByType(type, user, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<CategoryDC>(); ;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public ScheduleDC GetClassSchedule(int classId, int branchId, string user)
        {
            OperationResult<ScheduleDC> result = new OperationResult<ScheduleDC>();
            result = GMSClass.GetClassSchedule(classId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new ScheduleDC();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<CategoryTypeDC> GetCategoryTypes(string name, string user, int branchId)
        {
            OperationResult<List<CategoryTypeDC>> result = GMSCategory.GetCategoryTypes(name, user, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<CategoryTypeDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int SaveCategory(CategoryDC category, string user, int branchId)
        {
            OperationResult<int> result = GMSCategory.SaveCategory(category, user, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ExcelineClassActiveTimeDC> GetClassHistory(int classId, List<ScheduleItemDC> scheduleItemList, string user)
        {
            OperationResult<List<ExcelineClassActiveTimeDC>> result = new OperationResult<List<ExcelineClassActiveTimeDC>>();
            result = GMSClass.GetClassHistory(classId, scheduleItemList, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ExcelineClassActiveTimeDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool ApplyClassScheduleForNextYear(int classId, DateTime startdate, DateTime enddate, string name, int branchId, string createdUser, List<ScheduleItemDC> scheduleItemList)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            result = GMSClass.ApplyClassScheduleForNextYear(classId, startdate, enddate, name, branchId, createdUser, scheduleItemList, ExceConnectionManager.GetGymCode(createdUser));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), createdUser);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public bool GetVerificationOfAppliedClassScheduleAction(int branchId, string user, int classScheduleId)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            result = GMSClass.GetVerificationOfAppliedClassScheduleAction(classScheduleId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<ArticleDC> GetArticlesForClass(int branchId, string user)
        {
            OperationResult<List<ArticleDC>> result = new OperationResult<List<ArticleDC>>();
            result = GMSClass.GetArticlesForClass(ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<ArticleDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public int GetExcelineClassIdByName(int branchId, string user, string className)
        {
           return GMSClass.GetExcelineClassIdByName(className, ExceConnectionManager.GetGymCode(user)).OperationReturnValue;
        }
        #endregion

        #region Schedule
        public bool UpdateActiveTime(EntityActiveTimeDC activetime, string user, bool isDeleted)
        {
            OperationResult<bool> result = GMSSchedule.UpdateActiveTime(activetime, isDeleted, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return false;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        public List<EntityActiveTimeDC> GetEntityActiveTimes(int branchid, DateTime startDate, DateTime endDate, List<int> entityList, string entityRoleType, string user)
        {
            OperationResult<List<EntityActiveTimeDC>> result = GMSSchedule.GetEntityActiveTimes(branchid, startDate, endDate, entityList, entityRoleType, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<EntityActiveTimeDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }
        #endregion

        #region Article
        public ArticleDC GetArticle(int articleId, int branchId, string user)
        {
            OperationResult<ArticleDC> result = GMSArticle.GetArticleById(articleId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return null;
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        #endregion

        #region Member
        public List<MemberContractDC> GetMemberContracts(int memberID, string user, int branchId)
        {
            OperationResult<List<MemberContractDC>> result = GMSManageMembership.GetMemberContracts(memberID, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<MemberContractDC>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }

        #endregion

        #region Booking
        public int SaveClassBookingPayment(int memberId, int classId, BookingArticleDC bookingArticle, List<ClassBookingDC> PaidBookings, List<BookingPaymentDC> bookingPayments, decimal totalAmount, decimal paidAmount, decimal defaultPrice, string user, int branchId)
        {
            OperationResult<int> result = GMSClass.SaveClassBookingPayment(memberId, classId, bookingArticle, PaidBookings, bookingPayments, totalAmount, paidAmount, defaultPrice, user, branchId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return -1;
            }
            else
            {
                return result.OperationReturnValue;
            }
    }

        #endregion

        #region Utility
        public List<CalendarHoliday> GetHolidays(DateTime calendarDate, string user, int brnachId)
        {
            OperationResult<List<CalendarHoliday>> result = GMSUtility.GetHolidays(calendarDate, brnachId, ExceConnectionManager.GetGymCode(user));
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    USLogError.WriteToFile(message.Message, new Exception(), user);
                }
                return new List<CalendarHoliday>();
            }
            else
            {
                return result.OperationReturnValue;
            }
        }
        #endregion
    }
}
