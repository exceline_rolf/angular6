﻿using System;
using System.Collections.Generic;
using US.Exceline.GMS.AccessControl.Data.DataAdapters;
using US.Exceline.GMS.Modules.ManageMembership.API.ManageMembership;
using US.GMS.API.ManageSystemSettings;
using US.GMS.Core.DomainObjects.AccessControl;
using US.GMS.Core.ResultNotifications;
using System.Linq;
using US.GMS.Core.DomainObjects.ManageContracts;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.AccessControl.BusinessLogic
{
    public class ExcelineAccessControlManager
    {
        public static OperationResult<ExceAccessControlAuthenticationDC> CheckAuthentication(int branchId, string cardNo, string gymCode, ExceAccessControlTerminalDetailDC terminalDetail, int terminalId, string user)
        {
            OperationResult<int> result = new OperationResult<int>();
            OperationResult<ExceAccessControlAuthenticationDC> checkAuthenticationResult = new OperationResult<ExceAccessControlAuthenticationDC>();
            OperationResult<Dictionary<string, object>> results = null;
            bool isHasShopAccount = true;
            try
            {
                checkAuthenticationResult.OperationReturnValue = ExcelineAccessControlFacade.CheckAuthentication(branchId, cardNo, gymCode, terminalId);
                if (checkAuthenticationResult.OperationReturnValue.IsValidMember == null)
                {
                    isHasShopAccount = false;
                    checkAuthenticationResult.OperationReturnValue.IsValidMember = true;
                }

                bool isGatNoCorrect = checkAuthenticationResult.OperationReturnValue.IsValidMember != null && (bool)checkAuthenticationResult.OperationReturnValue.IsValidMember;

                if (checkAuthenticationResult.OperationReturnValue.IsValidMember != null && (bool)checkAuthenticationResult.OperationReturnValue.IsValidMember)
                {
                    List<string> gymSettingColumnList = new List<string>();
                    gymSettingColumnList.Add("EcoPaidAccessMode");
                    gymSettingColumnList.Add("EcoNegativeOnAccount");
                    results = GMSManageGymSetting.GetSelectedGymSettings(gymSettingColumnList, true, checkAuthenticationResult.OperationReturnValue.BranchId, gymCode);
                    if (terminalDetail != null)
                    {
                        if (terminalDetail.IsPaid)
                        {
                            if (!results.ErrorOccured)
                            {
                                switch (Convert.ToInt32(results.OperationReturnValue["EcoPaidAccessMode"]))
                                {
                                    case 1://On Account
                                        if (CanPurchase(results, terminalDetail, checkAuthenticationResult))
                                        {
                                            //on acount
                                            ExecuteOnAccount(checkAuthenticationResult.OperationReturnValue.BranchId, cardNo, gymCode, terminalDetail, checkAuthenticationResult);
                                        }
                                        else
                                        {
                                            checkAuthenticationResult.OperationReturnValue.IsValidMember = false;
                                        }
                                        break;
                                    case 2://On Account Next order
                                        if (CanPurchase(results, terminalDetail, checkAuthenticationResult))
                                        {
                                            //on account
                                            ExecuteOnAccount(checkAuthenticationResult.OperationReturnValue.BranchId, cardNo, gymCode, terminalDetail, checkAuthenticationResult);
                                        }
                                        else
                                        {
                                            //Next order
                                            ExecuteNextOrder(checkAuthenticationResult.OperationReturnValue.BranchId, cardNo, gymCode, terminalDetail, terminalId, checkAuthenticationResult.OperationReturnValue.MemberId, ref result);
                                            //Don't have a next order
                                            if (result.OperationReturnValue == -2)
                                            {
                                                checkAuthenticationResult.OperationReturnValue.IsValidMember = false;
                                            }
                                        }
                                        break;
                                    case 3://Next order
                                        ExecuteNextOrder(checkAuthenticationResult.OperationReturnValue.BranchId, cardNo, gymCode, terminalDetail, terminalId, checkAuthenticationResult.OperationReturnValue.MemberId, ref result);
                                        //Don't have a next order
                                        if (result.OperationReturnValue == -2)
                                        {
                                            checkAuthenticationResult.OperationReturnValue.IsValidMember = false;
                                        }
                                        break;
                                    case 0://none
                                        checkAuthenticationResult.OperationReturnValue.IsValidMember = false;
                                        break;
                                }
                            }
                        }
                        else if (terminalDetail.TypeId == "4" && !(bool)checkAuthenticationResult.OperationReturnValue.CheckFingerPrint)
                        {
                            OperationResult<bool> validateMemberWithCardResult = GMSManageMembership.ValidateMemberWithCard(cardNo, "GAT", gymCode, branchId, user);

                            if (!validateMemberWithCardResult.OperationReturnValue)
                            {
                                checkAuthenticationResult.OperationReturnValue.IsValidMember = validateMemberWithCardResult.OperationReturnValue;

                                foreach (NotificationMessage message in validateMemberWithCardResult.Notifications)
                                {
                                    AccessControlLog(message, cardNo, terminalId, terminalDetail.TypeId, gymCode);
                                }
                            }
                        }
                    }
                }

                if (isGatNoCorrect && isHasShopAccount)
                {
                    if (results != null)
                    {
                        if ((decimal)results.OperationReturnValue["EcoNegativeOnAccount"] == (decimal)0.00 && checkAuthenticationResult.OperationReturnValue.Balance < 0)
                        {
                            checkAuthenticationResult.OperationReturnValue.Balance = (decimal)0.00;
                        }
                        else
                        {
                            checkAuthenticationResult.OperationReturnValue.Balance += (decimal)results.OperationReturnValue["EcoNegativeOnAccount"];
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                checkAuthenticationResult.CreateMessage("Error check authentication in access control, " + ex.Message, MessageTypes.ERROR);
            }
            return checkAuthenticationResult;
        }

        public static OperationResult<ExceAccessControlAuthenticationDC> CheckAuthenticationForGatPurchase(string cardNo, int branchId, int terminalId, string gymCode, string durtaion, string price)
        {
            OperationResult<ExceAccessControlAuthenticationDC> checkAuthenticationResult = new OperationResult<ExceAccessControlAuthenticationDC>();

            try
            {
                checkAuthenticationResult.OperationReturnValue = ExcelineAccessControlFacade.CheckGatPurchaseAuthentication(cardNo, branchId, terminalId, gymCode);


                List<string> gymSettingColumnList = new List<string>();
                OperationResult<Dictionary<string, object>> results = null;
                gymSettingColumnList.Add("EcoPaidAccessMode");
                gymSettingColumnList.Add("EcoNegativeOnAccount");
                results = GMSManageGymSetting.GetSelectedGymSettings(gymSettingColumnList, true, branchId, gymCode);

                OperationResult<List<ExceAccessControlTerminalDetailDC>> terminalResult = new OperationResult<List<ExceAccessControlTerminalDetailDC>>();
                terminalResult.OperationReturnValue = ExcelineAccessControlFacade.GetTerminalDetails(Convert.ToInt32(branchId), gymCode.Trim());
                var terminal = terminalResult.OperationReturnValue.Where(a => a.Id == Convert.ToInt32(terminalId)).SingleOrDefault();
                terminal.ArticlePrice = Convert.ToDecimal(price);



                if (Convert.ToInt32(results.OperationReturnValue["EcoPaidAccessMode"]) == 1)
                {
                    if (checkAuthenticationResult.OperationReturnValue.BranchId == Convert.ToInt32(branchId))
                    {
                        checkAuthenticationResult.OperationReturnValue.CanPurchase = CanPurchase(results, terminal, checkAuthenticationResult);
                    }
                    else
                    {
                        checkAuthenticationResult.OperationReturnValue.CanPurchase = false;
                    }
                }
                else if (Convert.ToInt32(results.OperationReturnValue["EcoPaidAccessMode"]) == 2)
                {
                    GatPurchasePaymentStatus purchaseStatus = CanPurchaseOnAccountNextOrder(results, terminal, checkAuthenticationResult);
                    if (purchaseStatus == GatPurchasePaymentStatus.ONACCOUNTONLY)
                    {
                        checkAuthenticationResult.OperationReturnValue.CanPurchase = true;
                    }
                    else if (purchaseStatus == GatPurchasePaymentStatus.ONACCOUNTANDNEXTORDER)
                    {
                        checkAuthenticationResult.OperationReturnValue.CanPurchase = true;
                    }
                    else if (purchaseStatus == GatPurchasePaymentStatus.NEXTORDERONLY)
                    {
                        checkAuthenticationResult.OperationReturnValue.CanPurchase = true;
                    }
                    else
                    {
                        checkAuthenticationResult.OperationReturnValue.CanPurchase = false;
                    }
                }
                else if (Convert.ToInt32(results.OperationReturnValue["EcoPaidAccessMode"]) == 3)
                {
                    checkAuthenticationResult.OperationReturnValue.CanPurchase = true;
                }
                else if (Convert.ToInt32(results.OperationReturnValue["EcoPaidAccessMode"]) == 0)
                {
                    checkAuthenticationResult.OperationReturnValue.CanPurchase = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return checkAuthenticationResult;
        }

        public static OperationResult<bool> RegisterMemberVisit(string gymCode, ExceAccessControlRegisterMemberVisitDC memberVisit, string user)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                //if (terminalDetail.TypeId == "4")
                //{
                OperationResult<bool> validateMemberWithCardResult = GMSManageMembership.ValidateMemberWithCard(memberVisit.CardNo, "GAT", gymCode, memberVisit.BranchId, user);

                if (!validateMemberWithCardResult.OperationReturnValue)
                {
                    result.OperationReturnValue = validateMemberWithCardResult.OperationReturnValue;

                    foreach (NotificationMessage message in validateMemberWithCardResult.Notifications)
                    {
                        AccessControlLog(message, memberVisit.CardNo, memberVisit.TerminalId, "4", gymCode);
                    }
                }
                //}
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in registering member visit - access control, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }


        private static void ExecuteNextOrder(int branchId, string cardNo, string gymCode, ExceAccessControlTerminalDetailDC terminalDetail, int terminalId, int memberId, ref  OperationResult<int> result)
        {
            result = GMSManageMembership.AddMachineSale(Convert.ToInt32(terminalDetail.ArticleNo), memberId, terminalDetail.ArticleName, terminalDetail.ArticlePrice, terminalDetail.Name, gymCode, branchId, terminalDetail.SalePointId);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    AccessControlLog(message, cardNo, terminalId, terminalDetail.TypeId, gymCode);
                }
            }
            else if (result.OperationReturnValue == -2)
            {
                NotificationMessage message = new NotificationMessage("Member is not valid for paid access.", MessageTypes.ERROR);
                AccessControlLog(message, cardNo, terminalId, terminalDetail.TypeId, gymCode);
            }
            else
            {
                NotificationMessage message = new NotificationMessage("Paid visit success.", MessageTypes.INFO);
                AccessControlLog(message, cardNo, terminalId, terminalDetail.TypeId, gymCode);
            }
        }

        private static bool CanPurchase(OperationResult<Dictionary<string, object>> results, ExceAccessControlTerminalDetailDC terminalDetail, OperationResult<ExceAccessControlAuthenticationDC> checkAuthenticationResult)
        {
            decimal negativeValue = -(decimal)results.OperationReturnValue["EcoNegativeOnAccount"];
            decimal itemPrice = terminalDetail.ArticlePrice;
            decimal balance = checkAuthenticationResult.OperationReturnValue.Balance;
            decimal afterPurchase = balance - itemPrice;

            if (afterPurchase >= negativeValue)
            {
                return true;
            }
            return false;
        }

        private static GatPurchasePaymentStatus CanPurchaseOnAccountNextOrder(OperationResult<Dictionary<string, object>> results, ExceAccessControlTerminalDetailDC terminalDetail, OperationResult<ExceAccessControlAuthenticationDC> checkAuthenticationResult)
        {
            GatPurchasePaymentStatus returnVal = GatPurchasePaymentStatus.NEXTORDERONLY;

            decimal negativeValue = (decimal)results.OperationReturnValue["EcoNegativeOnAccount"];
            decimal itemPrice = terminalDetail.ArticlePrice;
            decimal balance = checkAuthenticationResult.OperationReturnValue.Balance;

            if (balance >= itemPrice)
            {
                returnVal = GatPurchasePaymentStatus.ONACCOUNTONLY;
            }
            else if (negativeValue == 0 && balance > 0 && itemPrice > balance)
            {
                returnVal = GatPurchasePaymentStatus.ONACCOUNTANDNEXTORDER;
            }
            else if (negativeValue == 0 && balance == 0)
            {
                returnVal = GatPurchasePaymentStatus.NEXTORDERONLY;
            }
            else if (negativeValue > 0 && balance > 0 && itemPrice > balance)
            {
                if ((negativeValue + balance) >= itemPrice)
                {
                    returnVal = GatPurchasePaymentStatus.ONACCOUNTONLY;
                }
                else if ((negativeValue + balance) < itemPrice)
                {
                    returnVal = GatPurchasePaymentStatus.ONACCOUNTANDNEXTORDER;
                }
            }
            else if (negativeValue > 0 && balance < 0)
            {
                if ((negativeValue + balance) >= itemPrice)
                {
                    returnVal = GatPurchasePaymentStatus.ONACCOUNTONLY;
                }
                else if ((itemPrice > (negativeValue + balance)) && ((negativeValue + balance) > 0))
                {
                    returnVal = GatPurchasePaymentStatus.ONACCOUNTANDNEXTORDER;
                }
                else if ((negativeValue + balance) == 0)
                {
                    returnVal = GatPurchasePaymentStatus.NEXTORDERONLY;
                }
            }
            else if (negativeValue > 0 && balance == 0)
            {
                if (negativeValue >= itemPrice)
                    returnVal = GatPurchasePaymentStatus.ONACCOUNTONLY;
                else if(negativeValue < itemPrice)
                    returnVal = GatPurchasePaymentStatus.ONACCOUNTANDNEXTORDER;
            }
            return returnVal;
        }

        private static void ExecuteOnAccount(int branchId, string cardNo, string gymCode, ExceAccessControlTerminalDetailDC terminalDetail, OperationResult<ExceAccessControlAuthenticationDC> checkAuthenticationResult)
        {
            ExceAccessControlRegisterPurchaseDC purchaseDetail = new ExceAccessControlRegisterPurchaseDC();
            purchaseDetail.Balance = checkAuthenticationResult.OperationReturnValue.Balance - terminalDetail.ArticlePrice;
            purchaseDetail.BranchId = branchId;
            purchaseDetail.CardNo = cardNo;
            purchaseDetail.ItemCode = terminalDetail.ArticleNo;
            purchaseDetail.ItemPrice = terminalDetail.ArticlePrice;
            purchaseDetail.TerminalId = terminalDetail.Id;
            purchaseDetail.IsVending = false;

            OperationResult<bool> result = RegisterPurchase(gymCode, purchaseDetail, true);
            if (result.ErrorOccured)
            {
                foreach (NotificationMessage message in result.Notifications)
                {
                    AccessControlLog(message, cardNo, terminalDetail.Id, terminalDetail.TypeId, gymCode);
                }
            }
            else
            {
                checkAuthenticationResult.OperationReturnValue.Balance = purchaseDetail.Balance;
            }
        }

        private static void AccessControlLog(NotificationMessage message, string cardNo, int terminalId, string terminalType, string gymCode)
        {
            ExceAccessControlEventDC accessControlEventDetail = new ExceAccessControlEventDC();
            accessControlEventDetail.CardNo = cardNo;
            accessControlEventDetail.TerminalId = terminalId;
            accessControlEventDetail.TerminalType = terminalType;
            accessControlEventDetail.AccessControlType = "GAT";
            accessControlEventDetail.Message = message.Message;
            ExcelineAccessControlFacade.RegisterAccessControlEvent(gymCode, accessControlEventDetail);
        }

        public static OperationResult<List<ExceAccessControlTerminalDetailDC>> GetTerminalDetails(int branchId, string gymCode)
        {
            OperationResult<List<ExceAccessControlTerminalDetailDC>> result = new OperationResult<List<ExceAccessControlTerminalDetailDC>>();
            try
            {
                result.OperationReturnValue = ExcelineAccessControlFacade.GetTerminalDetails(branchId, gymCode);

            }
            catch (Exception ex)
            {
                result.CreateMessage("Error get terminal details in access control, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> UpdateTerminalStatus(string gymCode, ExceAccessControlUpdateTerminalStatusDC terminalStatusDetail)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ExcelineAccessControlFacade.UpdateTerminalStatus(gymCode, terminalStatusDetail);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in updating terminal status - access control, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> RegisterSunBedPurchase(string gymCode, ExceAccessControlRegisterPurchaseDC purchaseDetail, bool isPaid = false)
        {
            OperationResult<bool> result = new OperationResult<bool>();

            try
            {
                OperationResult<Dictionary<string, object>> results = null;
                List<string> gymSettingColumnList = new List<string>();
                gymSettingColumnList.Add("EcoPaidAccessMode");
                gymSettingColumnList.Add("EcoNegativeOnAccount");
                results = GMSManageGymSetting.GetSelectedGymSettings(gymSettingColumnList, true, purchaseDetail.BranchId, gymCode);
                OperationResult<ExceAccessControlAuthenticationDC> checkAuthenticationResult = new OperationResult<ExceAccessControlAuthenticationDC>();

                checkAuthenticationResult.OperationReturnValue = ExcelineAccessControlFacade.GetMemberDetailForSunBedPurchase(purchaseDetail.CardNo, gymCode);
                ArticleDC articleResult = checkAuthenticationResult.OperationReturnValue.TimeMachineArticle;
                OperationResult<List<ExceAccessControlTerminalDetailDC>> terminalResult = new OperationResult<List<ExceAccessControlTerminalDetailDC>>();
                terminalResult.OperationReturnValue = ExcelineAccessControlFacade.GetTerminalDetails(Convert.ToInt32(purchaseDetail.BranchId), gymCode.Trim());
                ExceAccessControlTerminalDetailDC terminalDetail = terminalResult.OperationReturnValue.Where(a => a.Id == purchaseDetail.TerminalId).SingleOrDefault();

                terminalDetail.ArticlePrice = purchaseDetail.ItemPrice;
                terminalDetail.ArticleName = articleResult.Description;
                terminalDetail.ArticleNo = articleResult.Id.ToString();
                terminalDetail.ArticleCategoryId = articleResult.CategoryId;

                OperationResult<SaleResultDC> callbackResult = new OperationResult<SaleResultDC>();
                SunBedBusinessHelperObject sunBedBusinessObject = new SunBedBusinessHelperObject();

                GatpurchaseShopItem gatPurchaseShopItem = new GatpurchaseShopItem()
                {
                    MemberId = checkAuthenticationResult.OperationReturnValue.MemberId,
                    ItemPrice = purchaseDetail.ItemPrice,
                    ItemCode = terminalDetail.ArticleName,
                    BranchId = purchaseDetail.BranchId,
                    GymCode = gymCode,
                    CardNumber = purchaseDetail.CardNo
                };

                sunBedBusinessObject.GatPurchaseShopItem = gatPurchaseShopItem;
                sunBedBusinessObject.AccountBalance = checkAuthenticationResult.OperationReturnValue.Balance;
                sunBedBusinessObject.Amount = purchaseDetail.ItemPrice;
                sunBedBusinessObject.ArticleCategoryId = terminalDetail.ArticleCategoryId;
                sunBedBusinessObject.ArticleName = terminalDetail.ArticleName;
                sunBedBusinessObject.ArticleNo = Convert.ToInt32(terminalDetail.ArticleNo);
                sunBedBusinessObject.BranchId = purchaseDetail.BranchId;
                sunBedBusinessObject.MemberBranchId = checkAuthenticationResult.OperationReturnValue.BranchId;
                sunBedBusinessObject.GymCode = gymCode;
                sunBedBusinessObject.MemberId = checkAuthenticationResult.OperationReturnValue.MemberId;
                sunBedBusinessObject.PaidAccessMode = Convert.ToInt32(results.OperationReturnValue["EcoPaidAccessMode"]);
                sunBedBusinessObject.OnAccountNegativeBalance = (decimal)results.OperationReturnValue["EcoNegativeOnAccount"];
                sunBedBusinessObject.SalepointId = terminalDetail.SalePointId;
                sunBedBusinessObject.User = "Time Machine";
                sunBedBusinessObject.TerminalID = terminalDetail.Id;

                int terminalBranchId = sunBedBusinessObject.BranchId;
                int memberBranchId = sunBedBusinessObject.MemberBranchId;

                if (!isPaid)
                {
                    switch (sunBedBusinessObject.PaidAccessMode)
                    {
                        case 1://On Account
                            if (CanPurchase(results, terminalDetail, checkAuthenticationResult))
                            {
                                if (terminalBranchId == memberBranchId)
                                {
                                    sunBedBusinessObject.GatPurchaseShopItem.IsNextOrder = false;
                                    callbackResult = GMSManageMembership.AddSunBedShopSale(sunBedBusinessObject);
                                    result.OperationReturnValue = callbackResult.OperationReturnValue.SaleStatus == SaleErrors.SUCCESS;
                                }
                                else
                                {
                                    result.OperationReturnValue = false;
                                }
                            }
                            else
                            {
                                result.OperationReturnValue = false;
                            }
                            break;
                        case 2://On Account Next order
                            GatPurchasePaymentStatus purchaseStatus = CanPurchaseOnAccountNextOrder(results, terminalDetail, checkAuthenticationResult);
                            switch (purchaseStatus)
                            {
                                case GatPurchasePaymentStatus.ONACCOUNTONLY:
                                    if (terminalBranchId == memberBranchId)
                                    {
                                        sunBedBusinessObject.PaymentOrderStatus = GatPurchasePaymentStatus.ONACCOUNTONLY;
                                        callbackResult = GMSManageMembership.AddSunBedShopSale(sunBedBusinessObject);
                                        result.OperationReturnValue = callbackResult.OperationReturnValue.SaleStatus == SaleErrors.SUCCESS;
                                    }
                                    else
                                    {
                                        sunBedBusinessObject.PaymentOrderStatus = GatPurchasePaymentStatus.NEXTORDERONLY;
                                        callbackResult = GMSManageMembership.AddSunBedShopSale(sunBedBusinessObject);
                                        result.OperationReturnValue = callbackResult.OperationReturnValue.SaleStatus == SaleErrors.SUCCESS;
                                    }
                                    break;
                                case GatPurchasePaymentStatus.ONACCOUNTANDNEXTORDER:
                                    if (terminalBranchId == memberBranchId)
                                    {
                                        sunBedBusinessObject.PaymentOrderStatus = GatPurchasePaymentStatus.ONACCOUNTANDNEXTORDER;
                                        sunBedBusinessObject.NextOrderPaymentAmount = (sunBedBusinessObject.Amount - (sunBedBusinessObject.OnAccountNegativeBalance + sunBedBusinessObject.AccountBalance));
                                        sunBedBusinessObject.OnAccountPaymentAmount = (sunBedBusinessObject.Amount - sunBedBusinessObject.NextOrderPaymentAmount);
                                        callbackResult = GMSManageMembership.AddSunBedShopSale(sunBedBusinessObject);
                                        result.OperationReturnValue = callbackResult.OperationReturnValue.SaleStatus == SaleErrors.SUCCESS;
                                    }
                                    else
                                    {
                                        sunBedBusinessObject.PaymentOrderStatus = GatPurchasePaymentStatus.NEXTORDERONLY;
                                        callbackResult = GMSManageMembership.AddSunBedShopSale(sunBedBusinessObject);
                                        result.OperationReturnValue = callbackResult.OperationReturnValue.SaleStatus == SaleErrors.SUCCESS;
                                    }
                                    break;
                                case GatPurchasePaymentStatus.NEXTORDERONLY:
                                    sunBedBusinessObject.PaymentOrderStatus = GatPurchasePaymentStatus.NEXTORDERONLY;
                                    callbackResult = GMSManageMembership.AddSunBedShopSale(sunBedBusinessObject);
                                    result.OperationReturnValue = callbackResult.OperationReturnValue.SaleStatus == SaleErrors.SUCCESS;
                                    break;
                            }
                            break;
                        case 3://Next order                                                      
                            callbackResult = GMSManageMembership.AddSunBedShopSale(sunBedBusinessObject);
                            result.OperationReturnValue = callbackResult.OperationReturnValue.SaleStatus == SaleErrors.SUCCESS;
                            break;
                        case 0://none
                            result.OperationReturnValue = false;
                            break;
                    }

                }
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in registering purchase - access control, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> RegisterPurchase(string gymCode, ExceAccessControlRegisterPurchaseDC purchaseDetail, bool isPaid = false)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                if (!isPaid)
                {
                    purchaseDetail.IsVending = true;
                    List<string> gymSettingColumnList = new List<string>();
                    gymSettingColumnList.Add("EcoNegativeOnAccount");
                    OperationResult<Dictionary<string, object>> results = GMSManageGymSetting.GetSelectedGymSettings(gymSettingColumnList, true, purchaseDetail.BranchId, gymCode);

                    if (!results.ErrorOccured)
                    {
                        decimal negativeBalance = (decimal)results.OperationReturnValue["EcoNegativeOnAccount"];
                        purchaseDetail.Balance -= negativeBalance;
                        if (purchaseDetail.Balance < -negativeBalance)
                        {
                            return result;
                        }
                    }
                }
                result.OperationReturnValue = ExcelineAccessControlFacade.RegisterPurchase(gymCode, purchaseDetail);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in registering purchase - access control, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<bool> RegisterVendingEvent(string gymCode, ExceAccessControlVendingEventDC vendingEventDetail)
        {
            OperationResult<bool> result = new OperationResult<bool>();
            try
            {
                result.OperationReturnValue = ExcelineAccessControlFacade.RegisterVendingEvent(gymCode, vendingEventDetail);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in registering purchase - access control, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<string> GetGymCodeByCompanyId(string companyId)
        {
            OperationResult<string> result = new OperationResult<string>();
            try
            {
                result.OperationReturnValue = ExcelineAccessControlFacade.GetGymCodeByCompanyId(companyId);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting gym code - access control, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }

        public static OperationResult<ExceAccessControlTerminalDetailDC> GetTypeIdByTerminalId(int terminalId, string gymCode)
        {
            OperationResult<ExceAccessControlTerminalDetailDC> result = new OperationResult<ExceAccessControlTerminalDetailDC>();
            try
            {
                result.OperationReturnValue = ExcelineAccessControlFacade.GetTypeIdByTerminalId(terminalId, gymCode);
            }
            catch (Exception ex)
            {
                result.CreateMessage("Error in getting type id - access control, " + ex.Message, MessageTypes.ERROR);
            }
            return result;
        }
    }
}

