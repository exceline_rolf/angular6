﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.Common.Service.DataContracts
{
    [DataContract]
    public class OperationFieldDC
    {
        [DataMember]
        public string DisplayName { get; set; }
        [DataMember]
        public string Status { get; set; }
    }
}
