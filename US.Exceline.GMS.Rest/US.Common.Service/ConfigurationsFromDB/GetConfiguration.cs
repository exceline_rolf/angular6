﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace US.Common.Service.ConfigurationsFromDB
{
    public class GetConfiguration
    {
        public string DBConnectionString;

        public string Key(int MID, string Key)
        {
            try
            {

                var connectObject = new SqlConnection(DBConnectionString);
                connectObject.Open();

                DbCommand comm = connectObject.CreateCommand();
                comm.CommandText = "USP_GetModuleConfigurations";
                comm.CommandType = CommandType.StoredProcedure;
                comm.CommandTimeout = int.MaxValue;
                comm.Parameters.Add(new SqlParameter("@MID", MID));
                comm.Parameters.Add(new SqlParameter("@Key", Key));

                DbDataReader reader = comm.ExecuteReader();

                var retVal = string.Empty;

                while (reader.Read())
                {
                    retVal = reader["Value"].ToString();
                }
                return retVal;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
