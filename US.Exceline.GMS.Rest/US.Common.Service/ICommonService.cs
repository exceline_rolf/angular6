﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using US.Common.Service.DataContracts;
using US.Common.Web.UI.Core.SystemObjects;

namespace US.Common.Service
{
    [ServiceContract]
    public interface IPolicyRetriever
    {
        [OperationContract, WebGet(UriTemplate = "/clientaccesspolicy.xml")]
        Stream ProvidePolicyFile();
    }
   // [ServiceContract(Name = "MainService", Namespace = "US.Common.Service")]
    [ServiceContract]
    interface ICommonService
    {
        [OperationContract]
        bool EditUserBasicInfo(bool isPasswdChange, USPUserDC uspUser, string loggedUser);

        [OperationContract]
        USPUserDC GetUSPUserInfo(string userName);

        [OperationContract]
        LoginInfoDC GetLoginInfo();

          [OperationContract]
        USPUserDC getUser();

          [OperationContract]
          bool SaveUserCulture(string culture, string userName);
    }
}
