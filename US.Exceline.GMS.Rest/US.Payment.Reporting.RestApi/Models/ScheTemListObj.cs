﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using US.Payment.Reporting.Core.DomainObjects;

namespace US.Payment.Reporting.RestApi.Models
{
    public class ScheTemListObj
    {
        public KeyValueObject category { get; set; }

        public string entity { get; set; }
    }
}