﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.SystemObjects;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.ScheduleManagement
{
    [DataContract]
    public class ScheduleDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private DateTime _startDate = DateTime.Now;
        [DataMember]
        public DateTime StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }

        private DateTime? _endDate = DateTime.Now;
        [DataMember]
        public DateTime? EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }

        private int _entityId = -1;
        [DataMember]
        public int EntityId
        {
            get { return _entityId; }
            set { _entityId = value; }
        }

        private int _entityTypeId = -1;
        [DataMember]
        public int EntityTypeId
        {
            get { return _entityTypeId; }
            set { _entityTypeId = value; }
        }

        private ScheduleTypes _occurrence = ScheduleTypes.NONE;
        [DataMember]
        public ScheduleTypes Occurrence
        {
            get { return _occurrence; }
            set { _occurrence = value; }
        }       

        private ObservableCollection<ScheduleItemDC> _sheduleItemList = new ObservableCollection<ScheduleItemDC>();
        [DataMember]
        public ObservableCollection<ScheduleItemDC> SheduleItemList
        {
            get { return _sheduleItemList; }
            set { _sheduleItemList = value; }
        }

        private DateTime _createdDate;
        [DataMember]
        public DateTime CreatedDate
        {
            get
            {
                return _createdDate;
            }
            set
            {
                _createdDate = value;
            }
        }

        private DateTime _lastModifiedDate;
        [DataMember]
        public DateTime LastModifiedDate
        {
            get
            {
                return _lastModifiedDate;
            }
            set
            {
                _lastModifiedDate = value;
            }
        }

        private string _createdUser = string.Empty;
        [DataMember]
        public string CreatedUser
        {
            get
            {
                return _createdUser;
            }
            set
            {
                _createdUser = value;
            }
        }

        private string _entityRoleType = string.Empty;
        [DataMember]
        public string EntityRoleType
        {
            get
            {
                return _entityRoleType;
            }
            set
            {
                _entityRoleType = value;
            }
        }


        private string _lastModifiedUser;
        [DataMember]
        public string LastModifiedUser
        {
            get
            {
                return _lastModifiedUser;
            }
            set
            {
                _lastModifiedUser = value;
            }
        }

        private bool _activeStatus;
        [DataMember]
        public bool ActiveStatus
        {
            get
            {
                return _activeStatus;
            }
            set
            {
                _activeStatus = value;
            }
        }

        //private string _startDatePart;
        //[DataMember]
        //public string StartDatePart
        //{
        //    get { return _startDatePart; }
        //    set { _startDatePart = value; }
        //}

        //private string _endDatePart;
        //[DataMember]
        //public string EndDatePart
        //{
        //    get { return _endDatePart; }
        //    set { _endDatePart = value; }
        //}

    }
}
