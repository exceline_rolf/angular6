﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.ScheduleManagement
{
    [DataContract]
    public class TaskExtendedFieldDC
    {
        private string _key = string.Empty;
        [DataMember]
        public string Key
        {
            get { return _key; }
            set { _key = value; }
        }

        private Object _value = null;
        [DataMember]
        public Object Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }
}
