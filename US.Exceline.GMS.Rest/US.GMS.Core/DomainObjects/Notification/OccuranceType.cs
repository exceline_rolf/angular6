﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.GMS.Core.DomainObjects.Notification
{
    public enum OccuranceType
    {
        DAILY,
        WEEKLY,
        MONTHLY,
        NONE,
        ONCE,
        OTHER
    }
}
