﻿using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.IBooking
{
    [DataContract]
    public class ExceIBookingStartReason
    {
        private int _startReasonId = -1;
        private string _startReasonName = string.Empty;

        [DataMember(Order = 1)]
        public int StartReasonId
        {
            get { return _startReasonId; }
            set { _startReasonId = value; }
        }

        [DataMember(Order = 2)]
        public string StartReasonName
        {
            get { return _startReasonName; }
            set { _startReasonName = value; }
        }
    }
}
