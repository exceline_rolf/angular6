﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.IBooking
{
    [DataContract]
    public class ExceIBookingResourceBooking
    {
        private int _systemId = -1;
        [DataMember(Order = 1, Name = "SystemId")]
        public int SystemId
        {
            get { return _systemId; }
            set { _systemId = value; }
        }

        private int _branchId = -1;
        [DataMember(Order = 2, Name = "BranchId")]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private int _id = -1;
        [DataMember(Order = 3,Name = "Id")]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private List<int> _customerIdList;
        [DataMember(Order = 4, Name = "CustomerIds")]
        public List<int> CustomerIdList
        {
            get { return _customerIdList; }
            set { _customerIdList = value; }
        }

        private int _resourceId = -1;
        [DataMember(Order = 5, Name = "ResourceId")]
        public int ResourceId
        {
            get { return _resourceId; }
            set { _resourceId = value; }
        }

        private string _fromDateTime;
        [DataMember(Order = 6, Name = "FromDateTime")]
        public string FromDateTime
        {
            get { return _fromDateTime; }
            set { _fromDateTime = value; }
        }

        private string _toDateTime;
        [DataMember(Order = 7, Name = "ToDateTime")]
        public string ToDateTime
        {
            get { return _toDateTime; }
            set { _toDateTime = value; }
        }

        private int _bookingCategoryId;
         [DataMember(Order = 8, Name = "BookingCategoryId")]
        public int BookingCategoryId
        {
            get { return _bookingCategoryId; }
            set { _bookingCategoryId = value; }
        }


        private Dictionary<int,int> _scheduleItemIdList;
        [DataMember(Order = 9, Name = "ScheduleItemIdList")]
        public Dictionary<int,int> ScheduleItemIdList
        {
            get { return _scheduleItemIdList; }
            set { _scheduleItemIdList = value; }
        }


        private List<int> _extraResourceIdList;
         [DataMember(Order = 10, Name = "ExtraResourceIds")]
        public List<int> ExtraResourceIdList
        {
            get { return _extraResourceIdList; }
            set { _extraResourceIdList = value; }
        }

        private int _scheduleItemId = -1;
        [DataMember(Order = 11, Name = "ScheduleItemId")]
        public int ScheduleItemId
        {
            get { return _scheduleItemId; }
            set { _scheduleItemId = value; }
        }

        private bool _arrivalStatus;
       // [DataMember(Order = 12, Name = "ArrivalStatus")]
        public bool ArrivalStatus
        {
            get { return _arrivalStatus; }
            set { _arrivalStatus = value; }
        }

        private bool _paid;
        [DataMember(Order = 12, Name = "Paid")]
        public bool Paid
        {
            get { return _paid; }
            set { _paid = value; }
        }

        private bool _arrived;
         [DataMember(Order = 13, Name = "Arrived")]
        public bool Arrived
        {
            get { return _arrived; }
            set { _arrived = value; }
        }

         private string _note = string.Empty;
         [DataMember(Order = 14, Name = "Note")]
         public string Note
         {
             get { return _note; }
             set { _note = value; }
         }
    }
}
