﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.IBooking
{
    [DataContract]
    public class ExceIBookingNewMember
    {
        private string _systemId = string.Empty;
        private string _gymId = string.Empty;
        private string _exportDateTime = string.Empty;
        private string _registeredDateTime = string.Empty;
        private string _firstName = string.Empty;
        private string _lastName = string.Empty;
        private string _address1 = string.Empty;
        private string _address2 = string.Empty;
        private string _address3 = string.Empty;
        private string _postCode = string.Empty;
        private string _postPlace = string.Empty;
        private string _mobile = string.Empty;
        private string _workTeleNo = string.Empty;
        private string _privateTeleNo = string.Empty;
        private string _email = string.Empty;
        private string _gender = string.Empty;
        private string _birthDate = string.Empty;
        private string _accountNumber = string.Empty;
        private string _offeringId = string.Empty;
        private string _startreasonId = string.Empty;
        private string _promoCode = string.Empty;
        private int _belongsTo ;
        private string _cardNumber = string.Empty;
        private string _paidAmount;
        private List<int> _interests;
        private string _countryCode = string.Empty;
        private string _guardianFirstName = string.Empty;
        private string _guardianLastName = string.Empty;
        private string _guardianAddress = string.Empty;
        private string _guardianPostalCode = string.Empty;
        private string _guardianPostalPlace = string.Empty;
        private string _guardianBirthdate = string.Empty;
        private string _guardianGender = string.Empty;
        private string _guardianMobile = string.Empty;
        private string _guardianEmail = string.Empty;
        private string _PinCode = string.Empty;
        private int _customerNo = -1;
        private int _brisID = -1;
        private int agressoID = -1;
        private bool _sendEmail = false;
        private bool _sendSMS = false;
        private bool _customerClub = false;

        [DataMember(Order = 1)]
        public string SystemId
        {
            get { return _systemId; }
            set { _systemId = value; }
        }

        [DataMember(Order = 2)]
        public string GymId
        {
            get { return _gymId; }
            set { _gymId = value; }
        }

        [DataMember(Order = 3)]
        public string ExportDateTime
        {
            get { return _exportDateTime; }
            set { _exportDateTime = value; }
        }

        [DataMember(Order = 4)]
        public string RegisteredDateTime
        {
            get { return _registeredDateTime; }
            set { _registeredDateTime = value; }
        }

        [DataMember(Order = 5)]
        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        [DataMember(Order = 6)]
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        [DataMember(Order = 7)]
        public string Address
        {
            get { return _address1; }
            set { _address1 = value; }
        }

        [DataMember(Order = 8)]
        public string Address2
        {
            get { return _address2; }
            set { _address2 = value; }
        }

        [DataMember(Order = 9)]
        public string Address3
        {
            get { return _address3; }
            set { _address3 = value; }
        }

        [DataMember(Order = 10)]
        public string PostCode
        {
            get { return _postCode; }
            set { _postCode = value; }
        }

        [DataMember(Order = 11)]
        public string PostPlace
        {
            get { return _postPlace; }
            set { _postPlace = value; }
        }

        [DataMember(Order = 12)]
        public string Mobile
        {
            get { return _mobile; }
            set { _mobile = value; }
        }

        [DataMember(Order = 13)]
        public string WorkTeleNo
        {
            get { return _workTeleNo; }
            set { _workTeleNo = value; }
        }

        [DataMember(Order = 14)]
        public string PrivateTeleNo
        {
            get { return _privateTeleNo; }
            set { _privateTeleNo = value; }
        }

        [DataMember(Order = 15)]
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        [DataMember(Order = 16)]
        public string Gender
        {
            get { return _gender; }
            set { _gender = value; }
        }

        [DataMember(Order = 17)]
        public string BirthDate
        {
            get { return _birthDate; }
            set { _birthDate = value; }
        }

        [DataMember(Order = 18)]
        public string AccountNumber
        {
            get { return _accountNumber; }
            set { _accountNumber = value; }
        }

        [DataMember(Order = 19)]
        public string OfferingId
        {
            get { return _offeringId; }
            set { _offeringId = value; }
        }

        [DataMember(Order = 20)]
        public string StartreasonId
        {
            get { return _startreasonId; }
            set { _startreasonId = value; }
        }

        [DataMember(Order = 21)]
        public string PromoCode
        {
            get { return _promoCode; }
            set { _promoCode = value; }
        }

        [DataMember(Order = 22)]
        public int BelongsTo
        {
            get { return _belongsTo; }
            set { _belongsTo = value; }
        }

        [DataMember(Order = 23)]
        public string CardNumber
        {
            get { return _cardNumber; }
            set { _cardNumber = value; }
        }

        [DataMember(Order = 24)]
        public string PaidAmount
        {
            get { return _paidAmount; }
            set { _paidAmount = value; }
        }

        [DataMember(Order = 25)]
        public List<int> Interests
        {
            get { return _interests; }
            set { _interests = value; }
        }

        [DataMember(Order = 26)]
        public string CountryCode
        {
            get { return _countryCode; }
            set { _countryCode = value; }
        }

        [DataMember(Order = 27)]
        public string GuardianFirstName
        {
            get { return _guardianFirstName; }
            set { _guardianFirstName = value; }
        }

        [DataMember(Order = 28)]
        public string GuardianLastName
        {
            get { return _guardianLastName; }
            set { _guardianLastName = value; }
        }

        [DataMember(Order = 29)]
        public string GuardianAddress
        {
            get { return _guardianAddress; }
            set { _guardianAddress = value; }
        }

        [DataMember(Order = 30)]
        public string GuardianPostalCode
        {
            get { return _guardianPostalCode; }
            set { _guardianPostalCode = value; }
        }

        [DataMember(Order = 31)]
        public string GuardianPostalPlace
        {
            get { return _guardianPostalPlace; }
            set { _guardianPostalPlace = value; }
        }

         [DataMember(Order = 32)]
        public string GuardianBirthdate
        {
            get { return _guardianBirthdate; }
            set { _guardianBirthdate = value; }
        }

         [DataMember(Order = 33)]
        public string GuardianGender
        {
            get { return _guardianGender; }
            set { _guardianGender = value; }
        }

         [DataMember(Order = 34)]
        public string GuardianMobile
        {
            get { return _guardianMobile; }
            set { _guardianMobile = value; }
        }

         [DataMember(Order = 35)]
        public string GuardianEmail
        {
            get { return _guardianEmail; }
            set { _guardianEmail = value; }
        }

         [DataMember(Order = 36)]
         public string PinCode
         {
             get { return _PinCode; }
             set { _PinCode = value; }
         }

         [DataMember(Order = 37)]
         public int CustomerNo
         {
             get { return _customerNo; }
             set { _customerNo = value; }
         }

         [DataMember(Order = 38)]
         public int BrisID
         {
             get { return _brisID; }
             set { _brisID = value; }
         }

         [DataMember(Order = 39)]
         public int AgressoID
         {
             get { return agressoID; }
             set { agressoID = value; }
         }

        [DataMember(Order = 40)]
        public bool SendEmail
        {
            get { return _sendEmail; }
            set { _sendEmail = value; }
        }

        [DataMember(Order = 41)]
        public bool SendSMS
        {
            get { return _sendSMS; }
            set { _sendSMS = value; }
        }

        [DataMember(Order = 42)]
        public bool CustomerClub
        {
            get { return _customerClub; }
            set { _customerClub = value; }
        }
    }
}
