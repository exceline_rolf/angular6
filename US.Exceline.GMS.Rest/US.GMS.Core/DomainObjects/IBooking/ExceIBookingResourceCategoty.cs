﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.DomainObjects.IBooking
{
    [DataContract]
    public class ExceIBookingResourceCategoty
    {
        private int _categoryId = -1;

         [DataMember(Order = 1,Name = "Id")]
        public int CategoryId
        {
            get { return _categoryId; }
            set { _categoryId = value; }
        }

        private string _categoryName = string.Empty;
        [DataMember(Order = 2, Name = "Name")]
        public string CategoryName
        {
            get { return _categoryName; }
            set { _categoryName = value; }
        }

    
        private string _categoryColor = string.Empty;
          [DataMember(Order = 3, Name = "Color")]
        public string CategoryColor
        {
            get { return _categoryColor; }
            set { _categoryColor = value; }
        }



    }
}
