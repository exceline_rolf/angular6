﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.DomainObjects.IBooking
{
    [DataContract]
    public class ExceIBookingMemberGymDetail
    {
        private string _gymId = string.Empty;
        private string _customerNo = string.Empty;


        [DataMember(Order = 1)]
        public string CustomerNo
        {
            get { return _customerNo; }
            set { _customerNo = value; }
        }

        [DataMember(Order = 2)]
        public string GymID
        {
            get { return _gymId; }
            set { _gymId = value; }
        }
    }
}
