﻿using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.IBooking
{
    [DataContract]
    public class ExceIBookingUpdateClass
    {
        private string _systemId = string.Empty;
        private string _gymId = string.Empty;
        private string _classId = string.Empty;
        private string _visitCount = string.Empty;
        private string _visitPercentage = string.Empty;
        private string _newInstructorId = string.Empty;
        private string _originalInstructorId = string.Empty;

        [DataMember(Order = 1)]
        public string SystemId
        {
            get { return _systemId; }
            set { _systemId = value; }
        }

        [DataMember(Order = 2)]
        public string GymId
        {
            get { return _gymId; }
            set { _gymId = value; }
        }

        [DataMember(Order = 3)]
        public string ClassId
        {
            get { return _classId; }
            set { _classId = value; }
        }

        [DataMember(Order = 4)]
        public string VisitCount
        {
            get { return _visitCount; }
            set { _visitCount = value; }
        }

        [DataMember(Order = 5)]
        public string VisitPercentage
        {
            get { return _visitPercentage; }
            set { _visitPercentage = value; }
        }

        [DataMember(Order = 6)]
        public string NewInstructorId
        {
            get { return _newInstructorId; }
            set { _newInstructorId = value; }
        }

        [DataMember(Order = 7)]
        public string OriginalInstructorId
        {
            get { return _originalInstructorId; }
            set { _originalInstructorId = value; }
        }
    }
}
