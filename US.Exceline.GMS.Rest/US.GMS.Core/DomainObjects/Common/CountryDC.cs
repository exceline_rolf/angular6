﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Common
{
      [DataContract]
   public class CountryDC
    {
        private string _Id = string.Empty;
        [DataMember]
        public string Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        private string _name = string.Empty;
            [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _countryCode = string.Empty;
        [DataMember]
        public string CountryCode
        {
            get { return _countryCode; }
            set { _countryCode = value; }
        }  
    }
}
