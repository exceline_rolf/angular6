﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Common
{
    [DataContract]
    public class ActivitySettingDC
    {
        private int _id;
        [DataMember]
        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _activityName;
        [DataMember]
        public string ActivityName
        {
            get { return _activityName; }
            set { _activityName = value; }
        }

        private string _activityCode = string.Empty;
        [DataMember]
        public string ActivityCode
        {
            get { return _activityCode; }
            set { _activityCode = value; }
        }

        private int _activityId;
        [DataMember]
        public int ActivityId
        {
            get { return _activityId; }
            set { _activityId = value; }
        }

        private bool _isContractTimeLimited;
        [DataMember]
        public bool IsContractTimeLimited
        {
            get { return _isContractTimeLimited; }
            set { _isContractTimeLimited = value; }
        }

        private bool _isPunchCardLimited;
        [DataMember]
        public bool IsPunchCardLimited
        {
            get { return _isPunchCardLimited; }
            set { _isPunchCardLimited = value; }
        }

        private bool _isSMSRemindered;
        [DataMember]
        public bool IsSMSRemindered
        {
            get { return _isSMSRemindered; }
            set { _isSMSRemindered = value; }
        }

        private int _daysInAdvanceForBooking;
        [DataMember]
        public int DaysInAdvanceForBooking
        {
            get { return _daysInAdvanceForBooking; }
            set { _daysInAdvanceForBooking = value; }
        }

        private bool _isTrial;
        [DataMember]
        public bool IsTrial
        {
            get { return _isTrial; }
            set { _isTrial = value; }
        }

        private bool _isContractBooking = false;
        [DataMember]
        public bool IsContractBooking
        {
            get { return _isContractBooking; }
            set { _isContractBooking = value; }
        }

        private bool _isBookViaIbooking = false;
        [DataMember]
        public bool IsBookViaIbooking
        {
            get { return _isBookViaIbooking; }
            set { _isBookViaIbooking = value; }
        }

        private int _branchId = -1;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private bool _isBookingActivity = false;
        [DataMember]
        public bool IsBookingActivity
        {
            get { return _isBookingActivity; }
            set { _isBookingActivity = value; }
        }

        private bool _isBasicActivity = false;
        [DataMember]
        public bool IsBasicActivity
        {
            get { return _isBasicActivity; }
            set { _isBasicActivity = value; }
        }

    }
}