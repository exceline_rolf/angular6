﻿using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Common
{
    [DataContract]
    public class FollowUpTemplateTaskDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _followUpTemplateId = -1;
        [DataMember]
        public int FollowUpTemplateId
        {
            get { return _followUpTemplateId; }
            set { _followUpTemplateId = value; }
        }

        private int _taskCategoryId = -1;
        [DataMember]
        public int TaskCategoryId
        {
            get { return _taskCategoryId; }
            set { _taskCategoryId = value; }
        }

        private ExcelineTaskCategoryDC _excelineTaskCategory;
        [DataMember]
        public ExcelineTaskCategoryDC ExcelineTaskCategory
        {
            get { return _excelineTaskCategory; }
            set { _excelineTaskCategory = value; }
        }

        private int _numberOfDays;
        [DataMember]
        public int NumberOfDays
        {
            get { return _numberOfDays; }
            set { _numberOfDays = value; }
        }

        private bool _isNextFollowUpValue;
        [DataMember]
        public bool IsNextFollowUpValue
        {
            get { return _isNextFollowUpValue; }
            set { _isNextFollowUpValue = value; }
        }

        private string _text;
        [DataMember]
        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }
    }
}
