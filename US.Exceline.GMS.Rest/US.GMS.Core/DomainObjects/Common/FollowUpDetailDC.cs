﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.ScheduleManagement;

namespace US.GMS.Core.DomainObjects.Common
{
    [DataContract]
    public class FollowUpDetailDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _name = string.Empty;
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _day = string.Empty;
        [DataMember]
        public string Day
        {
            get { return _day; }
            set { _day = value; }
        }

        private string _status = string.Empty;
        [DataMember]
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        private string _member = string.Empty;
        [DataMember]
        public string Member
        {
            get { return _member; }
            set { _member = value; }
        }

        private bool _completedStatus;
        [DataMember]
        public bool CompletedStatus
        {
            get { return _completedStatus; }
            set { _completedStatus = value; }
        }

        private string _comment = string.Empty;
        [DataMember]
        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        private int? _taskCategoryId = -1;
        [DataMember]
        public int? TaskCategoryId
        {
            get { return _taskCategoryId; }
            set { _taskCategoryId = value; }
        }

        private string _taskCategoryName = string.Empty;
        [DataMember]
        public string TaskCategoryName
        {
            get { return _taskCategoryName; }
            set { _taskCategoryName = value; }
        }
      
        private DateTime? _startDate = DateTime.Now;
        [DataMember]
        public DateTime? StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }

        private DateTime? _endDate = DateTime.Now;
        [DataMember]
        public DateTime? EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }

        private DateTime? _startTime;
        [DataMember]
        public DateTime? StartTime
        {
            get { return _startTime; }
            set { _startTime = value; }
        }

        private DateTime? _endTime;
        [DataMember]
        public DateTime? EndTime
        {
            get { return _endTime; }
            set { _endTime = value; }
        }

        private DateTime? _foundDate = DateTime.Now;
        [DataMember]
        public DateTime? FoundDate
        {
            get { return _foundDate; }
            set { _foundDate = value; }
        }

        private DateTime? _returnDate = DateTime.Now;
        [DataMember]
        public DateTime? ReturnDate
        {
            get { return _returnDate; }
            set { _returnDate = value; }
        }

        private DateTime? _dueDate = DateTime.Now;
        [DataMember]
        public DateTime? DueDate
        {
            get { return _dueDate; }
            set { _dueDate = value; }
        }

        private DateTime? _dueTime = DateTime.Now;
        [DataMember]
        public DateTime? DueTime
        {
            get { return _dueTime; }
            set { _dueTime = value; }
        }

        private int _numOfDays = 0;
        [DataMember]
        public int NumOfDays
        {
            get { return _numOfDays; }
            set { _numOfDays = value; }
        }

        private string _phoneNo = string.Empty;
        [DataMember]
        public string PhoneNo
        {
            get { return _phoneNo; }
            set { _phoneNo = value; }
        }

        private string _automatedSMS = string.Empty;
        [DataMember]
        public string AutomatedSMS
        {
            get { return _automatedSMS; }
            set { _automatedSMS = value; }
        }

        private List<TaskExtendedFieldDC> _extendedFieldsList = new List<TaskExtendedFieldDC>();
        [DataMember]
        public List<TaskExtendedFieldDC> ExtendedFieldsList
        {
            get { return _extendedFieldsList; }
            set { _extendedFieldsList = value; }
        }

        private DateTime? _createdDateTime;
        [DataMember]
        public DateTime? CreatedDateTime
        {
            get { return _createdDateTime; }
            set { _createdDateTime = value; }
        }

        private DateTime? _lastModifiedDateTime;
        [DataMember]
        public DateTime? LastModifiedDateTime
        {
            get { return _lastModifiedDateTime; }
            set { _lastModifiedDateTime = value; }
        }

        private string _createdUser = string.Empty;
        [DataMember]
        public string CreatedUser
        {
            get { return _createdUser; }
            set { _createdUser = value; }
        }

        private string _lastModifiedUser = string.Empty;
        [DataMember]
        public string LastModifiedUser
        {
            get { return _lastModifiedUser; }
            set { _lastModifiedUser = value; }
        }

        private bool? _activeStatus = false;
        [DataMember]
        public bool? ActiveStatus
        {
            get { return _activeStatus; }
            set { _activeStatus = value; }
        }

        private bool? _isShowInCalandar = false;
        [DataMember]
        public bool? IsShowInCalandar
        {
            get { return _isShowInCalandar; }
            set { _isShowInCalandar = value; }
        }

        private int? _followUpId = -1;
        [DataMember]
        public int? FollowUpId
        {
            get { return _followUpId; }
            set { _followUpId = value; }
        }

        private int? _assignedEmpId = -1;
        [DataMember]
        public int? AssignedEmpId
        {
            get { return _assignedEmpId; }
            set { _assignedEmpId = value; }
        }

        private string _assignedEmpName = string.Empty;
        [DataMember]
        public string AssignedEmpName
        {
            get { return _assignedEmpName; }
            set { _assignedEmpName = value; }
        }

        private int? _entRoleId = -1;
        [DataMember]
        public int? EntRoleId
        {
            get { return _entRoleId; }
            set { _entRoleId = value; }
        }

        private string _roleType = string.Empty;
        [DataMember]
        public string RoleType
        {
            get { return _roleType; }
            set { _roleType = value; }
        }

        private DateTime? _completedDate;
        [DataMember]
        public DateTime? CompletedDate
        {
            get { return _completedDate; }
            set { _completedDate = value; }
        }

        private int? _completedEmpId = -1;
        [DataMember]
        public int? CompletedEmpId
        {
            get { return _completedEmpId; }
            set { _completedEmpId = value; }
        }

        private string _completedEmpName = string.Empty;
        [DataMember]
        public string CompletedEmpName
        {
            get { return _completedEmpName; }
            set { _completedEmpName = value; }
        }

        private DateTime? _plannedDate;
        [DataMember]
        public DateTime? PlannedDate
        {
            get { return _plannedDate; }
            set { _plannedDate = value; }
        }

        private bool _isFixed;
        [DataMember]
        public bool IsFixed
        {
            get { return _isFixed; }
            set { _isFixed = value; }
        }

        private string _automatedEmail = string.Empty;
        [DataMember]
        public string AutomatedEmail
        {
            get { return _automatedEmail; }
            set { _automatedEmail = value; }
        }

        private string _description;
        [DataMember]
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private bool _isNextFollowUpValue = false;
        [DataMember]
        public bool IsNextFollowUpValue
        {
            get { return _isNextFollowUpValue; }
            set { _isNextFollowUpValue = value; }
        }

        private bool _isMoved;
        [DataMember]
        public bool IsMoved
        {
            get { return _isMoved; }
            set { _isMoved = value; }
        }

        private int? _taskTemplateId = -1;
        [DataMember]
        public int? TaskTemplateId
        {
            get { return _taskTemplateId; }
            set { _taskTemplateId = value; }
        }

        private bool _isPopUp = false;
        [DataMember]
        public bool IsPopUp
        {
            get { return _isPopUp; }
            set { _isPopUp = value; }
        }

        private bool _isStartTime;
         [DataMember]
        public bool IsStartTime
        {
            get { return _isStartTime; }
            set { _isStartTime = value; }
        }

         private bool _isEndTime;
        [DataMember]
        public bool IsEndTime
        {
            get { return _isEndTime; }
            set { _isEndTime = value; }
        }

        private int _branchId;
         [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

         private string _branchName = string.Empty;
         [DataMember]
        public string BranchName
        {
            get { return _branchName; }
            set { _branchName = value; }
        }

        



    }
}
