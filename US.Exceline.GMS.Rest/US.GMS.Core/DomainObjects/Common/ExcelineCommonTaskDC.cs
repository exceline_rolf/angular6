﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.ScheduleManagement;

namespace US.GMS.Core.DomainObjects.Common
{
    [DataContract]
    public class ExcelineCommonTaskDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _branchId = -1;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private bool _isFxied = false;
        [DataMember]
        public bool IsFxied
        {
            get { return _isFxied; }
            set { _isFxied = value; }
        }

        private string _text;
        [DataMember]
        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        private string _category;
        [DataMember]
        public string Category
        {
            get { return _category; }
            set { _category = value; }
        }

        private int _assignTo = -1;
        [DataMember]
        public int AssignTo
        {
            get { return _assignTo; }
            set { _assignTo = value; }
        }

        private string _assigntoName = string.Empty;
        [DataMember]
        public string AssigntoName
        {
            get { return _assigntoName; }
            set { _assigntoName = value; }
        }

        private string _assignEntityRole = string.Empty;
        [DataMember]
        public string AssignEntityRole
        {
            get { return _assignEntityRole; }
            set { _assignEntityRole = value; }
        }

        private int _followUpMemberId = -1;
        [DataMember]
        public int FollowUpMemberId
        {
            get { return _followUpMemberId; }
            set { _followUpMemberId = value; }
        }

        private int _belongsToMemberId = -1;
        [DataMember]
        public int BelongsToMemberId
        {
            get { return _belongsToMemberId; }
            set { _belongsToMemberId = value; }
        }

        private string _followupMemName = string.Empty;
        [DataMember]
        public string FollowupMemName
        {
            get { return _followupMemName; }
            set { _followupMemName = value; }
        }

        private string _followupMemRole = string.Empty;
        [DataMember]
        public string FollowupMemRole
        {
            get { return _followupMemRole; }
            set { _followupMemRole = value; }
        }

        private string _belongsToMemName = string.Empty;
        [DataMember]
        public string BelongsToMemName
        {
            get { return _belongsToMemName; }
            set { _belongsToMemName = value; }
        }

        private DateTime _startDate = DateTime.Now;
        [DataMember]
        public DateTime StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }
        private DateTime _endDate = DateTime.Now;
        [DataMember]
        public DateTime EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }
        private DateTime _dueDate = DateTime.Now;
        [DataMember]
        public DateTime DueDate
        {
            get { return _dueDate; }
            set { _dueDate = value; }
        }

        private DateTime _dueTime = DateTime.Now;
        [DataMember]
        public DateTime DueTime
        {
            get { return _dueTime; }
            set { _dueTime = value; }
        }
        private DateTime _startTime;
        [DataMember]
        public DateTime StartTime
        {
            get { return _startTime; }
            set { _startTime = value; }
        }

        private DateTime _endTime;
        [DataMember]
        public DateTime EndTime
        {
            get { return _endTime; }
            set { _endTime = value; }
        }

        private decimal  _estimatedTime;
        [DataMember]
        public decimal EstimatedTime
        {
            get { return _estimatedTime; }
            set { _estimatedTime = value; }
        }

        private int _taskTemplateId = -1;
        [DataMember]
        public int TaskTemplateId
        {
            get { return _taskTemplateId; }
            set { _taskTemplateId = value; }
        }

        private ScheduleDC _schedule = new ScheduleDC();
        [DataMember]
        public ScheduleDC Schedule
        {
            get { return _schedule; }
            set { _schedule = value; }
        }

        private string _description;
        [DataMember]
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private int _numOfDays = 0;
        [DataMember]
        public int NumOfDays
        {
            get { return _numOfDays; }
            set { _numOfDays = value; }
        }

        private bool _isPopup = false;
        [DataMember]
        public bool IsPopup
        {
            get { return _isPopup; }
            set { _isPopup = value; }
        }

        private DateTime _foundDate = DateTime.Now;
        [DataMember]
        public DateTime FoundDate
        {
            get { return _foundDate; }
            set { _foundDate = value; }
        }

        private DateTime _returnDate = DateTime.Now;
        [DataMember]
        public DateTime ReturnDate
        {
            get { return _returnDate; }
            set { _returnDate = value; }
        }

        private DateTime _planDate = DateTime.Now;
        [DataMember]
        public DateTime PlanDate
        {
            get { return _planDate; }
            set { _planDate = value; }
        }

        private string _phoneNo = string.Empty;
        [DataMember]
        public string PhoneNo
        {
            get { return _phoneNo; }
            set { _phoneNo = value; }
        }

        private string _sms = string.Empty;
        [DataMember]
        public string Sms
        {
            get { return _sms; }
            set { _sms = value; }
        }

        private string _entityRoleType;
        [DataMember]
        public string EntityRoleType
        {
            get { return _entityRoleType; }
            set { _entityRoleType = value; }
        }

        private List<TaskExtendedFieldDC> _extendedFieldsList = new List<TaskExtendedFieldDC>();
        [DataMember]
        public List<TaskExtendedFieldDC> ExtendedFieldsList
        {
            get { return _extendedFieldsList; }
            set { _extendedFieldsList = value; }
        }

        private bool _activeStatus = false;
        [DataMember]
        public bool ActiveStatus
        {
            get { return _activeStatus; }
            set { _activeStatus = value; }
        }

        private TaskTemplateType _templateType;
        [DataMember]
        public TaskTemplateType TemplateType
        {
            get { return _templateType; }
            set { _templateType = value; }
        }

        private bool _isShowInCalandar = false;
        [DataMember]
        public bool IsShowInCalandar
        {
            get { return _isShowInCalandar; }
            set { _isShowInCalandar = value; }
        }

        private DateTime _createdDate;
        [DataMember]
        public DateTime CreatedDate
        {
            get
            {
                return _createdDate;
            }
            set
            {
                _createdDate = value;
            }
        }

        private DateTime _lastModifiedDate;
        [DataMember]
        public DateTime LastModifiedDate
        {
            get
            {
                return _lastModifiedDate;
            }
            set
            {
                _lastModifiedDate = value;
            }
        }

        private string _createdUser = string.Empty;
        [DataMember]
        public string CreatedUser
        {
            get
            {
                return _createdUser;
            }
            set
            {
                _createdUser = value;
            }
        }

        private string _lastModifiedUser = string.Empty;
        [DataMember]
        public string LastModifiedUser
        {
            get
            {
                return _lastModifiedUser;
            }
            set
            {
                _lastModifiedUser = value;
            }
        }

        private decimal _totalTimeSpent;
        [DataMember]
        public decimal TotalTimeSpent
        {
            get { return _totalTimeSpent; }
            set { _totalTimeSpent = value; }
        }

        private string _taskStatus;
        [DataMember]
        public string TaskStatus
        {
            get { return _taskStatus; }
            set { _taskStatus = value; }
        }

        private string _notification;
        [DataMember]
        public string Notification
        {
            get { return _notification; }
            set { _notification = value; }
        }

        private bool _isEmployee = false;
        [DataMember]
        public bool IsEmployee
        {
            get { return _isEmployee; }
            set { _isEmployee = value; }
        }

        private bool _isFollowUp = false;
        [DataMember]
        public bool IsFollowUp
        {
            get { return _isFollowUp; }
            set { _isFollowUp = value; }
        }

        private int _entityId;
        [DataMember]
        public int EntityId 
        {
            get { return _entityId; }
            set { _entityId = value; }
        }

    }
}
