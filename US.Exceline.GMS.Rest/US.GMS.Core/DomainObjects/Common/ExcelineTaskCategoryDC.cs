﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.ScheduleManagement;

namespace US.GMS.Core.DomainObjects.Common
{
    [DataContract]
    public class ExcelineTaskCategoryDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _name = string.Empty;
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private bool _isAssignToEmp = false;
        [DataMember]
        public bool IsAssignToEmp
        {
            get { return _isAssignToEmp; }
            set { _isAssignToEmp = value; }
        }

        private bool _isAssignToRole = false;
        [DataMember]
        public bool IsAssignToRole
        {
            get { return _isAssignToRole; }
            set { _isAssignToRole = value; }
        }

        private bool _isStartDate = false;
        [DataMember]
        public bool IsStartDate
        {
            get { return _isStartDate; }
            set { _isStartDate = value; }
        }
        private bool _isEndDate = false;
        [DataMember]
        public bool IsEndDate
        {
            get { return _isEndDate; }
            set { _isEndDate = value; }
        }

        private bool _isDueTime = false;
        [DataMember]
        public bool IsDueTime
        {
            get { return _isDueTime; }
            set { _isDueTime = value; }
        }

        private bool _isStartTime = false;
        [DataMember]
        public bool IsStartTime
        {
            get { return _isStartTime; }
            set { _isStartTime = value; }
        }

        private bool _isEndTime = false;
        [DataMember]
        public bool IsEndTime
        {
            get { return _isEndTime; }
            set { _isEndTime = value; }
        }

        private bool _isFoundDate = false;
        [DataMember]
        public bool IsFoundDate
        {
            get { return _isFoundDate; }
            set { _isFoundDate = value; }
        }

        private bool _isReturnDate = false;
        [DataMember]
        public bool IsReturnDate
        {
            get { return _isReturnDate; }
            set { _isReturnDate = value; }
        }

        private bool _isDueDate = false;
        [DataMember]
        public bool IsDueDate
        {
            get { return _isDueDate; }
            set { _isDueDate = value; }
        }

        private bool _isNoOfDays = false;
        [DataMember]
        public bool IsNoOfDays
        {
            get { return _isNoOfDays; }
            set { _isNoOfDays = value; }
        }

        private bool _isPhoneNo = false;
        [DataMember]
        public bool IsPhoneNo
        {
            get { return _isPhoneNo; }
            set { _isPhoneNo = value; }
        }

        private List<ExtendedFieldDC> _extendedFieldsList = new List<ExtendedFieldDC>();
        [DataMember]
        public List<ExtendedFieldDC> ExtendedFieldsList
        {
            get { return _extendedFieldsList; }
            set { _extendedFieldsList = value; }
        }

        private DateTime _createdDate;
        [DataMember]
        public DateTime CreatedDate
        {
            get
            {
                return _createdDate;
            }
            set
            {
                _createdDate = value;
            }
        }

        private DateTime _lastModifiedDate;
        [DataMember]
        public DateTime LastModifiedDate
        {
            get
            {
                return _lastModifiedDate;
            }
            set
            {
                _lastModifiedDate = value;
            }
        }

        private string _createdUser = string.Empty;
        [DataMember]
        public string CreatedUser
        {
            get
            {
                return _createdUser;
            }
            set
            {
                _createdUser = value;
            }
        }

        private string _lastModifiedUser = string.Empty;
        [DataMember]
        public string LastModifiedUser
        {
            get
            {
                return _lastModifiedUser;
            }
            set
            {
                _lastModifiedUser = value;
            }
        }

        private bool _activeStatus;
        [DataMember]
        public bool ActiveStatus
        {
            get { return _activeStatus; }
            set { _activeStatus = value; }
        }

        private bool _isDefault = false;
        [DataMember]
        public bool IsDefault
        {
            get { return _isDefault; }
            set { _isDefault = value; }
        }

        private bool _isNextFollowUp = false;
        [DataMember]
        public bool IsNextFollowUp
        {
            get { return _isNextFollowUp; }
            set { _isNextFollowUp = value; }
        }

        private bool _isAutomatedSMS = false;
        [DataMember]
        public bool IsAutomatedSMS
        {
            get { return _isAutomatedSMS; }
            set { _isAutomatedSMS = value; }
        }

        private bool _isAutomatedEmail = false;
        [DataMember]
        public bool IsAutomatedEmail
        {
            get { return _isAutomatedEmail; }
            set { _isAutomatedEmail = value; }
        }

    }
}
