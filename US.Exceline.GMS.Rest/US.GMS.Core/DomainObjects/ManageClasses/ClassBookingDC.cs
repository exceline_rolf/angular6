﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.ManageClasses
{
    [DataContract]
    public class ClassBookingDC
    {
        private int _activeTimeId;
        [DataMember]
        public int ActiveTimeId
        {
            get { return _activeTimeId; }
            set { _activeTimeId = value; }
        }

        private int _participants;
        [DataMember]
        public int Participants
        {
            get { return _participants; }
            set { _participants = value; }
        }

        private bool _isBooked;
        [DataMember]
        public bool IsBooked
        {
            get { return _isBooked; }
            set { _isBooked = value; }
        }
        

    }
}
