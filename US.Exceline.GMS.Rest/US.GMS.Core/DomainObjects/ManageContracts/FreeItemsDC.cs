﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.ManageContracts
{
    [DataContract]
    public class FreeItemsDC
    {
        private int _freeItemId = -1;
        [DataMember]
        public int FreeItemId
        {
            get { return _freeItemId; }
            set { _freeItemId = value; }
        }

        private string _freeItemName = string.Empty;
        [DataMember]
        public string FreeItemName
        {
            get { return _freeItemName; }
            set { _freeItemName = value; }
        }
    }
}
