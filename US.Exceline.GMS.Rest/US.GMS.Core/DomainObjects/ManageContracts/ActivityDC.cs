﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS.Core
// Coding Standard   : US Coding Standards
// Author            : NHE
// Created Timestamp : "4/25/2012 4:38:44 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.Common;

namespace US.GMS.Core.DomainObjects.ManageContracts
{
    [DataContract]
    public class ActivityDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _name = string.Empty;
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _code = String.Empty;
        [DataMember]
        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }


        private int _branchId = -1;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private bool _isActive = false;
        [DataMember]
        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }

        private ArticleDC _article = new ArticleDC();
        [DataMember]
        public ArticleDC Article
        {
            get { return _article; }
            set { _article = value; }
        }

        private bool _isAssign;
        [DataMember]
        public bool IsAssign
        {
            get { return _isAssign; }
            set { _isAssign = value; }
        }

        private ActivitySettingDC _activitySetting = new ActivitySettingDC();
        [DataMember]
        public ActivitySettingDC ActivitySetting
        {
            get { return _activitySetting; }
            set { _activitySetting = value; }
        }
    }
}
