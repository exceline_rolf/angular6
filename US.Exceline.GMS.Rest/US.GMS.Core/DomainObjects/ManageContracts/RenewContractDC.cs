﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.ManageContracts
{
    [DataContract]
    public class RenewContractDC
    {

//-----------------------------------------Contract Data--------------------------------------------
        private int _memberContractID = -1;
        [DataMember]
        public int MemberContractID
        {
            get { return _memberContractID; }
            set { _memberContractID = value; }
        }

        private int _memberContractNo = -1;
        [DataMember]
        public int MemberContractNo
        {
            get { return _memberContractNo; }
            set { _memberContractNo = value; }
        }

        private int _oldTemplateId = -1;
        [DataMember]
        public int OldTemplateId
        {
            get { return _oldTemplateId; }
            set { _oldTemplateId = value; }
        }

        private bool _isATG = false;
        [DataMember]
        public bool IsATG
        {
            get { return _isATG; }
            set { _isATG = value; }
        }

        private int _memberId = -1;
        [DataMember]
        public int MemberId
        {
            get { return _memberId; }
            set { _memberId = value; }
        }


        private int _noOfMonths = -1;
        [DataMember]
        public int NoOfMonths
        {
            get { return _noOfMonths; }
            set { _noOfMonths = value; }
        }

        private string _memberName = string.Empty;
        [DataMember]
        public string MemberName
        {
            get { return _memberName; }
            set { _memberName = value; }
        }


        private int _memberAge = -1;
        [DataMember]
        public int MemberAge
        {
            get { return _memberAge; }
            set { _memberAge = value; }
        }

        private DateTime _contractEndDate = DateTime.Now;
        [DataMember]
        public DateTime ContractEndDate
        {
            get { return _contractEndDate; }
            set { _contractEndDate = value; }
        }
//------------------------------Last Installment Data-------------------------------
        private DateTime? _dueDate;
        [DataMember]
        public DateTime? DueDate
        {
            get { return _dueDate; }
            set { _dueDate = value; }
        }

        private DateTime _originalDueDate = DateTime.Now;
        [DataMember]
        public DateTime OriginalDueDate
        {
            get { return _originalDueDate; }
            set { _originalDueDate = value; }
        }

        private int _installmentNo = -1;
        [DataMember]
        public int InstallmentNo
        {
            get { return _installmentNo; }
            set { _installmentNo = value; }
        }

        private DateTime? _trainingPeriodStart;
        [DataMember]
        public DateTime? TrainingPeriodStart
        {
            get { return _trainingPeriodStart; }
            set { _trainingPeriodStart = value; }
        }

        private DateTime? _trainingPeriodEnd;
        [DataMember]
        public DateTime? TrainingPeriodEnd
        {
            get { return _trainingPeriodEnd; }
            set { _trainingPeriodEnd = value; }
        }

        private DateTime? _installmentDate;
        [DataMember]
        public DateTime? InstallmentDate
        {
            get { return _installmentDate; }
            set { _installmentDate = value; }
        }

        private int _memberID = -1;
        [DataMember]
        public int MemberID
        {
            get { return _memberID; }
            set { _memberID = value; }
        }

        private int _payerId = -1;
        [DataMember]
        public int PayerId
        {
            get { return _payerId; }
            set { _payerId = value; }
        }

        private string _payerName = string.Empty;
        [DataMember]
        public string PayerName
        {
            get { return _payerName; }
            set { _payerName = value; }
        }
//------------------Template Data-------------------------
        private int _numOfInstallments = -1;
        [DataMember]
        public int NumOfInstallments
        {
            get { return _numOfInstallments; }
            set { _numOfInstallments = value; }
        }

        private decimal _orderPrice = -1;
        [DataMember]
        public decimal OrderPrice
        {
            get { return _orderPrice; }
            set { _orderPrice = value; }
        }

        private string _templateName = string.Empty;
        [DataMember]
        public string TemplateName
        {
            get { return _templateName; }
            set { _templateName = value; }
        }

        private int _articleId = -1;
        [DataMember]
        public int ArticleId
        {
            get { return _articleId; }
            set { _articleId = value; }
        }

        private int _templateId = -1;
        [DataMember]
        public int TemplateId
        {
            get { return _templateId; }
            set { _templateId = value; }
        }


        private List<ContractItemDC> _itemList = new List<ContractItemDC>();
        [DataMember]
        public List<ContractItemDC> ItemList
        {
            get { return _itemList; }
            set { _itemList = value; }
        }

        private List<ContractItemDC> _monthlyItemList = new List<ContractItemDC>();
        [DataMember]
        public List<ContractItemDC> MonthlyItemList
        {
            get { return _monthlyItemList; }
            set { _monthlyItemList = value; }
        }

        private string _activityArticelName = string.Empty;
        [DataMember]
        public string ActivityArticelName
        {
            get { return _activityArticelName; }
            set { _activityArticelName = value; }
        }

        private DateTime? _fixEndDate;
        [DataMember]
        public DateTime? FixEndDate
        {
            get { return _fixEndDate; }
            set { _fixEndDate = value; }
        }

        private int _fixedDueDay = -1;
        [DataMember]
        public int FixedDueDay
        {
            get { return _fixedDueDay; }
            set { _fixedDueDay = value; }
        }

        private int _activityId = -1;
        [DataMember]
        public int ActivityId
        {
            get { return _activityId; }
            set { _activityId = value; }
        }

        private bool _isInvoiceFeeAdded = false;
        [DataMember]
        public bool IsInvoiceFeeAdded
        {
            get { return _isInvoiceFeeAdded; }
            set { _isInvoiceFeeAdded = value; }
        }

        private decimal _lastOrderAmount = 0;
        [DataMember]
        public decimal LastOrderAmount
        {
            get { return _lastOrderAmount; }
            set { _lastOrderAmount = value; }
        }

        private string _gymName = string.Empty;
        [DataMember]
        public string GymName
        {
            get { return _gymName; }
            set { _gymName = value; }
        }

        private int _branchID = -1;
        [DataMember]
        public int BranchID
        {
            get { return _branchID; }
            set { _branchID = value; }
        }
    }
}
