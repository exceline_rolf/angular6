﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using US.GMS.Core.SystemObjects;

namespace US.GMS.Core.DomainObjects.Economy
{
    [DataContract]
    public  class CreditNoteDC
    {
        private int _id;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _creditorNumber = string.Empty;
        [DataMember]
        public string CreditorNumber
        {
            get { return _creditorNumber; }
            set { _creditorNumber = value; }
        }

        private string _customerNo = string.Empty;
        [DataMember]
        public string CustomerNo
        {
            get { return _customerNo; }
            set { _customerNo = value; }
        }

        private DateTime _createdDate;
        [DataMember]
        public DateTime CreatedDate
        {
            get { return _createdDate ; }
            set { _createdDate = value; }
        }

        private string _user;
        [DataMember]
        public string User
        {
            get { return _user; }
            set { _user = value; }
        }

        private decimal _amount = 0;
        [DataMember]
        public decimal Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }


        private decimal _balance = 0;
        [DataMember]
        public decimal Balance
        {
            get { return _balance; }
            set { _balance = value; }
        }

        private string _note;
        [DataMember]
        public string Note
        {
            get { return _note; }
            set { _note = value; }
        }

        private string _kid = string.Empty;
         [DataMember]
        public string Kid
        {
            get { return _kid; }
            set { _kid = value; }
        }

         private string _invoiceNo = string.Empty;
         [DataMember]
         public string InvoiceNo
         {
             get { return _invoiceNo; }
             set { _invoiceNo = value; }
         }

         private int _caseNo = -1;
         [DataMember]
         public int CaseNo
         {
             get { return _caseNo; }
             set { _caseNo = value; }
         }


        private List<CreditNoteDetailsDC> _details = new List<CreditNoteDetailsDC>();
        [DataMember]
        public List<CreditNoteDetailsDC> Details
        {
            get { return _details; }
            set { _details = value; }
        }

        private List<InvoicePaymentSummaryDC> _paymentlist = new List<InvoicePaymentSummaryDC>();
        [DataMember]
        public List<InvoicePaymentSummaryDC> Paymentlist
        {
            get { return _paymentlist; }
            set { _paymentlist = value; }
        }

        private int _arItemNo;
        [DataMember]
        public int ArItemNo
        {
            get { return _arItemNo; }
            set { _arItemNo = value; }
        }

        private int _memberId;
        [DataMember]
        public int MemberId
        {
            get { return _memberId; }
            set { _memberId = value; }
        }

        private int _salePointId;
        [DataMember]
        public int SalePointId
        {
            get { return _salePointId; }
            set { _salePointId = value; }
        }

        private int _shopAccoutId;
        [DataMember]
        public int ShopAccoutId
        {
            get { return _shopAccoutId; }
            set { _shopAccoutId = value; }
        }

        private int _gymId = -1;
        [DataMember]
        public int GymId
        {
            get { return _gymId; }
            set { _gymId = value; }
        }

        private int _branchId;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }
    }
}
