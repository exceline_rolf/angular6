﻿// --------------------------------------------------------------------------
// Copyright(c) <2011> Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USPayment
// Project Name      : US.Payment.Web.LightBox.InvoiceSearch.Views
// Coding Standard   : US Coding Standards
// Author            : AME
// Created Timestamp : 09/08/2011 HH:MM  PM
// --------------------------------------------------------------------------

namespace US.GMS.Core.DomainObjects.Economy
{
    public enum InvoicesSelectionModes
    {
        NONE,
        SINGLE,
        MULTIPLE
    }
}
