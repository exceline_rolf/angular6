﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Login
{

    [DataContract]
    public class ExcePWSetRecDC
    {
        private String _gymCode= String.Empty;
        [DataMember]
        public String GymCode
        {
            get { return _gymCode; }
            set { _gymCode = value; }
        }

        private String _userName = String.Empty;
        [DataMember]
        public String UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }

        private String _base= String.Empty;
        [DataMember]
        public String Base
        {
            get { return _base; }
            set { _base = value; }
        }

        private String _password = String.Empty;
        [DataMember]
        public String Password
        {
            get { return _password; }
            set { _password = value; }
        }
    }
}
