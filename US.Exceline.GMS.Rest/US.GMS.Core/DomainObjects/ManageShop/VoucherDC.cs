﻿using System;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.ManageShop
{
    [DataContract]
    public class VoucherDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _branchId = -1;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private int _voucherNo;
        [DataMember]
        public int VoucherNo
        {
            get { return _voucherNo; }
            set { _voucherNo = value; }
        }

        private int _customerNo;
        [DataMember]
        public int CustomerNo
        {
            get { return _customerNo; }
            set { _customerNo = value; }
        }

        private string _customerName;
        [DataMember]
        public string CustomerName
        {
            get { return _customerName; }
            set { _customerName = value; }
        }

        private decimal _voucherPrice;
        [DataMember]
        public decimal VoucherPrice
        {
            get { return _voucherPrice; }
            set { _voucherPrice = value; }
        }

        private decimal _payableAmount;
        [DataMember]
        public decimal PayableAmount
        {
            get { return _payableAmount; }
            set { _payableAmount = value; }
        }

        private DateTime _expiryDate = DateTime.Now;
        [DataMember]
        public DateTime ExpiryDate
        {
            get { return _expiryDate; }
            set { _expiryDate = value; }
        }

        private DateTime _issuedDate = DateTime.Now;
        [DataMember]
        public DateTime IssuedDate
        {
            get { return _issuedDate; }
            set { _issuedDate = value; }
        }

        private decimal _remainingAmount;
        [DataMember]
        public decimal RemainingAmount
        {
            get { return _remainingAmount; }
            set { _remainingAmount = value; }
        }


        #region Date
        private DateTime _date;
        [DataMember]
        public DateTime Date
        {
            get { return _date; }
            set { _date = value; }
        }
        #endregion

        #region Settlement Number
        private int _settlementNumber = -1;
        [DataMember]
        public int SettlementNumber
        {
            get { return _settlementNumber; }
            set { _settlementNumber = value; }
        }
        #endregion

        #region Amount
        private decimal _amount = 0.00M;
        [DataMember]
        public decimal Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }
        #endregion

        #region Pay Mood Cash
        private decimal _payMoodCash = 0.00M;
        [DataMember]
        public decimal PayMoodCash
        {
            get { return _payMoodCash; }
            set { _payMoodCash = value; }
        }
        #endregion

        #region Pay Mood Bank Terminal
        private decimal _payMoodBankTerminal = 0.00M;
        [DataMember]
        public decimal PayMoodBankTerminal
        {
            get { return _payMoodBankTerminal; }
            set { _payMoodBankTerminal = value; }
        }
        #endregion
    }
}
