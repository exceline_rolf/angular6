﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 14/08/2014
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.ManageShop
{
    [DataContract]
    public class ShopSalesPaymentDC
    {
        private int _shopSalesId;
        [DataMember]
        public int ShopSalesId
        {
            get { return _shopSalesId; }
            set { _shopSalesId = value; }
        }

        private int _payModeId = -1;
        [DataMember]
        public int PayModeId
        {
            get { return _payModeId; }
            set { _payModeId = value; }
        }

        private string _payMode = string.Empty;
        [DataMember]
        public string PayMode
        {
            get { return _payMode; }
            set { _payMode = value; }
        }

        private string _paymentCode = string.Empty;
        [DataMember]
        public string PaymentCode
        {
            get { return _paymentCode; }
            set { _paymentCode = value; }
        }


        private decimal _amount = 0.00M;
        [DataMember]
        public decimal Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

        private DateTime _saleDate;
        [DataMember]
        public DateTime SaleDate
        {
            get { return _saleDate; }
            set { _saleDate = value; }
        }

        private string _saleTime = string.Empty;
        [DataMember]
        public string SaleTime
        {
            get { return _saleTime; }
            set { _saleTime = value; }
        }



        private string _createdUser = string.Empty;
        [DataMember]
        public string CreatedUser
        {
            get { return _createdUser; }
            set { _createdUser = value; }
        }

        private string _ref = string.Empty;
        [DataMember]
        public string Ref
        {
            get { return _ref; }
            set { _ref = value; }
        }

        private string _memberName = string.Empty;
        [DataMember]
        public string MemberName
        {
            get { return _memberName; }
            set { _memberName = value; }
        }

        private string _memberCustId = string.Empty;
        [DataMember]
        public string MemberCustId
        {
            get { return _memberCustId; }
            set { _memberCustId = value; }
        }

        private string _settlementCode = string.Empty;
        [DataMember]
        public string SettlementCode
        {
            get { return _settlementCode; }
            set { _settlementCode = value; }
        }

        private string _giftVoucherNumber;
        [DataMember]
        public string GiftVoucherNumber
        {
            get { return _giftVoucherNumber; }
            set { _giftVoucherNumber = value; }
        }

        private decimal _giftVoucherAmount;
        [DataMember]
        public decimal GiftVoucherAmount
        {
            get { return _giftVoucherAmount; }
            set { _giftVoucherAmount = value; }
        }

        private string _paymentType;
        [DataMember]
        public string PaymentType
        {
            get { return _paymentType; }
            set { _paymentType = value; }
        }

        private decimal _paymentAmount;
        [DataMember]
        public decimal PaymentAmount
        {
            get { return _paymentAmount; }
            set { _paymentAmount = value; }
        }

        private decimal _balance = 0;
        [DataMember]
        public decimal Balance
        {
            get { return _balance; }
            set { _balance = value; }
        }

        private bool _isvalidForPrint = false;
        [DataMember]
        public bool IsValidForPrint
        {
            get { return _isvalidForPrint; }
            set { _isvalidForPrint = value; }
        }

    }
}
