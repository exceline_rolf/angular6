﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : SSI
// Created Timestamp : "5/29/2012 10:45:22
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Admin
{
    [DataContract]
	public class ExcelineCreditorDC
	{

        private int _branchID = -1;
        [DataMember]
        public int BranchID
        {
            get { return _branchID; }
            set { _branchID = value; }
        }

        private int _creditorEntNo;
        [DataMember]
        public int CreditorEntNo
        {
            get { return _creditorEntNo; }
            set { _creditorEntNo = value; }
        }

        private string _firstName;
        [DataMember]
        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        private string _lastName;
        [DataMember]
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        private DateTime _bornDate;
        [DataMember]
        public DateTime BornDate
        {
            get { return _bornDate; }
            set { _bornDate = value; }
        }

        private string _addr1;
        [DataMember]
        public string Addr1
        {
            get { return _addr1; }
            set { _addr1 = value; }
        }

        private string _addr2;
        [DataMember]
        public string Addr2
        {
            get { return _addr2; }
            set { _addr2 = value; }
        }

        private string _addr3;
        [DataMember]
        public string Addr3
        {
            get { return _addr3; }
            set { _addr3 = value; }
        }

        private string _zipCode;
        [DataMember]
        public string ZipCode
        {
            get { return _zipCode; }
            set { _zipCode = value; }
        }

        private string _zipName;
        [DataMember]
        public string ZipName
        {
            get { return _zipName; }
            set { _zipName = value; }
        }

        private int _creditorInkassoId;
        [DataMember]
        public int CreditorInkassoId
        {
            get { return _creditorInkassoId; }
            set { _creditorInkassoId = value; }
        }

        private string _countryId;
        [DataMember]
        public string CountryId
        {
            get { return _countryId; }
            set { _countryId = value; }
        }

        private string _region = String.Empty;
        [DataMember]
        public string Region
        {
            get { return _region; }
            set { _region = value; }
        }

        private string _telMobile;
        [DataMember]
        public string TelMobile
        {
            get { return _telMobile; }
            set { _telMobile = value; }
        }

        private string _telWork;
        [DataMember]
        public string TelWork
        {
            get { return _telWork; }
            set { _telWork = value; }
        }

        private string _email;
        [DataMember]
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        private string _fax;
        [DataMember]
        public string Fax
        {
            get { return _fax; }
            set { _fax = value; }
        }

        private string _accountNo;
        [DataMember]
        public string AccountNo
        {
            get { return _accountNo; }
            set { _accountNo = value; }
        }

        private string _kidSwapAccountNo;
        [DataMember]
        public string KidSwapAccountNo
        {
            get { return _kidSwapAccountNo; }
            set { _kidSwapAccountNo = value; }
        }

        private string _collectionAccountNo = string.Empty;
        [DataMember]
        public string CollectionAccountNo
        {
            get { return _collectionAccountNo; }
            set { _collectionAccountNo = value; }
        }


        private string _companyId;
        [DataMember]
        public string CompanyId
        {
            get { return _companyId; }
            set { _companyId = value; }
        }

        private int _creditorGroupId;
        [DataMember]
        public int CreditorGroupId
        {
            get { return _creditorGroupId; }
            set { _creditorGroupId = value; }
        }

        private int _area = 0;
        [DataMember]
        public int Area
        {
            get { return _area; }
            set { _area = value; }
        }

        private string _bureauAccountNO = string.Empty;
         [DataMember]
        public string BureauAccountNO
        {
            get { return _bureauAccountNO; }
            set { _bureauAccountNO = value; }
        }

         private string _reorderSMSReceiver = string.Empty;
         [DataMember]
         public string ReorderSmsReceiver
         {
             get { return _reorderSMSReceiver; }
             set { _reorderSMSReceiver = value; }
         }
	}
}
