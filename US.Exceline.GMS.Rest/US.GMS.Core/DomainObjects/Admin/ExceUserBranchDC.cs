﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Admin
{
    [DataContract]
    public class ExceUserBranchDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _userId = -1;
        [DataMember]
        public int UserId
        {
            get { return _userId; }
            set { _userId = value; }
        }

        private int _branchId = -1;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private string _branchName = string.Empty;
        [DataMember]
        public string BranchName
        {
            get { return _branchName; }
            set { _branchName = value; }
        }

        private bool _isDeleted = false;
        [DataMember]
        public bool IsDeleted
        {
            get { return _isDeleted; }
            set { _isDeleted = value; }
        }

        private bool _isExpressGym = false;
        [DataMember]
        public bool IsExpressGym
        {
            get { return _isExpressGym; }
            set { _isExpressGym = value; }
        }

        private string _accountNo = string.Empty;
         [DataMember]
        public string AccountNo
        {
            get { return _accountNo; }
            set { _accountNo = value; }
        }


        




    }
}
