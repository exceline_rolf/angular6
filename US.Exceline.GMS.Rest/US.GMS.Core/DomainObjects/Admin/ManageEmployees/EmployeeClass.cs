﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.DomainObjects.Admin.ManageEmployees
{
    [DataContract]
    public class EmployeeClass
    {
        private int _classId = -1;
        [DataMember]
        public int ClassId
        {
            get { return _classId; }
            set { _classId = value; }
        }
        private string _day = string.Empty;
        [DataMember]
        public string Day
        {
            get { return _day; }
            set { _day = value; }
        }
        private DateTime _startDate;
        [DataMember]
        public DateTime StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }
        private DateTime _endDate;
        [DataMember]
        public DateTime EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }

        private TimeSpan _startTime;
        [DataMember]
        public TimeSpan StartTime
        {
            get { return _startTime; }
            set { _startTime = value; }
        }
        private TimeSpan _endTime;
        [DataMember]
        public TimeSpan EndTime
        {
            get { return _endTime; }
            set { _endTime = value; }
        }
        private string _classType = string.Empty;
        [DataMember]
        public string ClassType
        {
            get { return _classType; }
            set { _classType = value; }
        }
        private bool _isApproved = true;
        [DataMember]
        public bool IsApproved
        {
            get { return _isApproved; }
            set { _isApproved = value; }
        }
        [DataMember]
        public string ContentOfApproved
        {
            set { }
            get
            {
                if (_isApproved)
                {
                    return "Approved";
                }
                else
                {
                    return "Unapproved";
                }
            }
        }
        private int _entityActiveTimeID = -1;
        [DataMember]
        public int EntityActiveTimeID
        {
            get { return _entityActiveTimeID; }
            set { _entityActiveTimeID = value; }
        }

        private int _branchId;
         [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private string _branchName = string.Empty;
         [DataMember]
        public string BranchName
        {
            get { return _branchName; }
            set { _branchName = value; }
        }

        

    }
}
