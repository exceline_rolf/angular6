﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.SystemObjects;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Admin.ManageEmployees
{
    [DataContract]
    public class TrainerActiveTimeDC : ActiveTimeDC
    {
        private int _id = -1;
        [DataMember]
        public override int Id
        {
            get { return _id; }
            set
            {
                _id = value;
            }
        }

        private int _sheduleItemId = -1;
        [DataMember]
        public override int SheduleItemId
        {
            get
            {
                return _sheduleItemId;
            }
            set
            {
                _sheduleItemId = value;
            }
        }

        private DateTime _startDateTime;
        [DataMember]
        public override DateTime StartDateTime
        {
            get { return _startDateTime; }
            set
            {
                _startDateTime = value;
            }
        }

        private DateTime _endDateTime;
        [DataMember]
        public override DateTime EndDateTime
        {
            get { return _endDateTime; }
            set
            {
                _endDateTime = value;
            }
        }

        private int _entityId = -1;
        [DataMember]
        public override int EntityId
        {
            get { return _entityId; }
            set
            {
                _entityId = value;
            }
        }

        private string _entityRoleType = string.Empty;
        [DataMember]
        public override string EntityRoleType
        {
            get { return _entityRoleType; }
            set
            {
                _entityRoleType = value;
            }
        }

        private int _entNo = -1;
        [DataMember]
        public override int EntNo
        {
            get { return _entNo; }
            set
            {
                _entNo = value;
            }
        }

        private string _name = string.Empty;
        [DataMember]
        public override string Name
        {
            get { return _name; }
            set
            {
                _name = value;
            }
        }

        public override int NumberOfMembers
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override string InstructorName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }
}
