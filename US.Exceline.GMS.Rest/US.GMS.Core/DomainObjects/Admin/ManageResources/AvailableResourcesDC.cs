﻿using System;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Admin.ManageResources
{
    [DataContract]
    public class AvailableResourcesDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
        get { return _id; }
        set { _id = value; }
        }

        private int _resourID = -1;
        [DataMember]
        public int ResourID
        {
        get { return _resourID; }
        set { _resourID = value; }
        }

        private string _resourName = string.Empty;
        [DataMember]
        public string ResourName
        {
        get { return _resourName; }
        set { _resourName = value; }
        }

        private string _day = string.Empty;
        [DataMember]
        public string Day
        {
        get { return _day; }
        set { _day = value; }
        }

        private DateTime _fromTime = DateTime.MinValue;
        [DataMember]
        public DateTime FromTime
        {
        get { return _fromTime; }
        set { _fromTime = value; }
        }

        private DateTime _toTime = DateTime.MinValue;
        [DataMember]
        public DateTime ToTime
        {
        get { return _toTime; }
        set { _toTime = value; }
        }

        private int _activityId = -1;
        [DataMember]
        public int ActivityId
        {
        get { return _activityId; }
        set { _activityId = value; }
        }

        private string _activityName = string.Empty;
        [DataMember]
        public string ActivityName
        {
        get { return _activityName; }
        set { _activityName = value; }
        }

        private bool _hasIntroduceDate = true;
        [DataMember]
        public bool HasIntroduceDate
        {
            get { return _hasIntroduceDate; }
            set { _hasIntroduceDate = value; }
        }
    }
}
