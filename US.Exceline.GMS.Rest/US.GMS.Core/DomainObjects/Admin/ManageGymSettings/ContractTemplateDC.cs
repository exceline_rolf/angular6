﻿using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Admin.ManageGymSettings
{
    [DataContract]
    public class ContractTemplateDC
    {
        private int _templateID = -1;
        [DataMember]
        public int TemplateID
        {
            get { return _templateID; }
            set { _templateID = value; }
        }

        private int _templateNo = -1;
        [DataMember]
        public int TemplateNo
        {
            get { return _templateNo; }
            set { _templateNo = value; }
        }

        private string _templateName = string.Empty;
        [DataMember]
        public string TemplateName
        {
            get { return _templateName; }
            set { _templateName = value; }
        }
    }
}
