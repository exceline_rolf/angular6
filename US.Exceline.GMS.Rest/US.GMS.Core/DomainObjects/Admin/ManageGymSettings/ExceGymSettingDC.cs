﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.Admin.ManageGymSettings
{
    [DataContract]
    public class ExceGymSettingDC
    {
        private int _id;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _branchId;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private bool _isDailySettlementForAll;

        [DataMember]
        public bool IsDailySettlementForAll
        {
            get { return _isDailySettlementForAll; }
            set { _isDailySettlementForAll = value; }
        }

        private bool _isShopLoginWithCard;
        [DataMember]
        public bool IsShopLoginWithCard
        {
            get { return _isShopLoginWithCard; }
            set { _isShopLoginWithCard = value; }
        }

        private decimal _onAccountNegativeVale;
        [DataMember]
        public decimal OnAccountNegativeVale
        {
            get { return _onAccountNegativeVale; }
            set { _onAccountNegativeVale = value; }
        }

        private bool _isSMSInvoiceShop;
        [DataMember]
        public bool IsSMSInvoiceShop
        {
            get { return _isSMSInvoiceShop; }
            set { _isSMSInvoiceShop = value; }
        }

        private bool _followUpNewPerson;
        [DataMember]
        public bool FollowUpNewPerson
        {
            get { return _followUpNewPerson; }
            set { _followUpNewPerson = value; }
        }

        #region Region by GMA
        private bool _isShopAvailable = false;
        [DataMember]
        public bool IsShopAvailable
        {
            get { return _isShopAvailable; }
            set { _isShopAvailable = value; }
        }


        private bool _isLogoutFromShopAfterSale = false;
        [DataMember]
        public bool IsLogoutFromShopAfterSale
        {
            get { return _isLogoutFromShopAfterSale; }
            set { _isLogoutFromShopAfterSale = value; }
        }


        private int _invoiceCollectionPeriod = -1;
        [DataMember]
        public int InvoiceCollectionPeriod
        {
            get { return _invoiceCollectionPeriod; }
            set { _invoiceCollectionPeriod = value; }
        }

        private int _smsFreqDaysBeforeCashContEnd;
        [DataMember]
        public int SmsFreqDaysBeforeCashContEnd
        {
            get { return _smsFreqDaysBeforeCashContEnd; }
            set { _smsFreqDaysBeforeCashContEnd = value; }
        }

        private int _debtWarningdate;
        [DataMember]
        public int DebtWarningdate
        {
            get { return _debtWarningdate; }
            set { _debtWarningdate = value; }
        }

        private int _reminderDate;
        [DataMember]
        public int ReminderDate
        {
            get { return _reminderDate; }
            set { _reminderDate = value; }
        }

        private DateTime _gymStartTime;
        [DataMember]
        public DateTime GymStartTime
        {
            get { return _gymStartTime; }
            set { _gymStartTime = value; }
        }

        private DateTime _gymEndTime;
        [DataMember]
        public DateTime GymEndTime
        {
            get { return _gymEndTime; }
            set { _gymEndTime = value; }
        }


        #endregion

        #region Access Times settings
        private GymAccessTimeSettingsDC _accessTimeSettings;
        [DataMember]
        public GymAccessTimeSettingsDC AccessTimeSettings
        {
            get { return _accessTimeSettings; }
            set { _accessTimeSettings = value; }
        }
        #endregion

        #region Required Settings
        private GymRequiredSettingsDC _requiredSettings;
        [DataMember]
        public GymRequiredSettingsDC RequiredSettings
        {
            get { return _requiredSettings; }
            set { _requiredSettings = value; }
        }
        #endregion

        #region Hardware Profile Settings
        private ExceHardwareProfileDC _hardwareProfile = new ExceHardwareProfileDC();
        [DataMember]
        public ExceHardwareProfileDC HardwareProfile
        {
            get { return _hardwareProfile; }
            set { _hardwareProfile = value; }
        }
        #endregion

        #region Member Search Settings
        private GymMemberSearchSettingsDC _memberSearchSettings;
        [DataMember]
        public GymMemberSearchSettingsDC MemberSearchSettings
        {
            get { return _memberSearchSettings; }
            set { _memberSearchSettings = value; }
        }
        #endregion

        #region Other Settings
        private GymOtherSettingsDC _otherSettings;
        [DataMember]
        public GymOtherSettingsDC OtherSettings
        {
            get { return _otherSettings; }
            set { _otherSettings = value; }
        }
        #endregion

        #region SMS Settings
        private GymSmsSettingsDC _smsSettings;
        [DataMember]
        public GymSmsSettingsDC SmsSettings
        {
            get { return _smsSettings; }
            set { _smsSettings = value; }
        }
        #endregion

        #region Economy Settings
        private GymEconomySettingsDC _economySettings;
        [DataMember]
        public GymEconomySettingsDC EconomySettings
        {
            get { return _economySettings; }
            set { _economySettings = value; }
        }
        #endregion

        //Should be Delete
        private int _creditDueDate = 0;
        [DataMember]
        public int CreditDueDate
        {
            get { return _creditDueDate; }
            set { _creditDueDate = value; }
        }

        //Should be Delete
        private decimal _creditNoteMinInvoiceFee;
        [DataMember]
        public decimal CreditNoteMinInvoiceFee
        {
            get { return _creditNoteMinInvoiceFee; }
            set { _creditNoteMinInvoiceFee = value; }
        }

        //Should be Delete
        private bool _isMemCardAsGuestCard = false;
        [DataMember]
        public bool IsMemCardAsGuestCard
        {
            get { return _isMemCardAsGuestCard; }
            set { _isMemCardAsGuestCard = value; }
        }

        private bool _isSMSInvoice;
        [DataMember]
        public bool IsSMSInvoice
        {
            get { return _isSMSInvoice; }
            set { _isSMSInvoice = value; }
        }

        //Should be Delete
        private bool _useTodayAsDueDate = false;
        [DataMember]
        public bool UseTodayAsDueDate
        {
            get { return _useTodayAsDueDate; }
            set { _useTodayAsDueDate = value; }
        }
        //Should be Delete
        private int _creditPeriod = -1;
        [DataMember]
        public int CreditPeriod
        {
            get { return _creditPeriod; }
            set { _creditPeriod = value; }
        }

        //Should be Delete
        private bool _isXtraUsed = false;
        [DataMember]
        public bool IsXtraUsed
        {
            get { return _isXtraUsed; }
            set { _isXtraUsed = value; }
        }

        //Should be Delete
        private decimal _creditSaleLimit;
        [DataMember]
        public decimal CreditSaleLimit
        {
            get { return _creditSaleLimit; }
            set { _creditSaleLimit = value; }
        }

        //Should be Delete
        private bool _isMinTotalPayable = false;
        [DataMember]
        public bool IsMinTotalPayable
        {
            get { return _isMinTotalPayable; }
            set { _isMinTotalPayable = value; }
        }

        //Should be Delete
        private bool _isIgnoreInvoiceFee = false;
        [DataMember]
        public bool IsIgnoreInvoiceFee
        {
            get { return _isIgnoreInvoiceFee; }
            set { _isIgnoreInvoiceFee = value; }
        }

        //Should be Delete
        private bool _isShopLoginNeeded = false;
        [DataMember]
        public bool IsShopLoginNeeded
        {
            get { return _isShopLoginNeeded; }
            set { _isShopLoginNeeded = value; }
        }

        //Should be Delete
        #region Economy Settings

        private string _invoiceAccountNo = String.Empty;
        [DataMember]
        public string InvoiceAccountNo
        {
            get { return _invoiceAccountNo; }
            set { _invoiceAccountNo = value; }
        }

        private string _paymentAccountNo = String.Empty;
        [DataMember]
        public string PaymentAccountNo
        {
            get { return _paymentAccountNo; }
            set { _paymentAccountNo = value; }
        }

        private int _suspendedDays = 0;

        [DataMember]
        public int SuspendedDays
        {
            get { return _suspendedDays; }
            set { _suspendedDays = value; }
        }

        private int _resignPeriod = 0;
        [DataMember]
        public int ResignPeriod
        {
            get { return _resignPeriod; }
            set { _resignPeriod = value; }
        }

        private bool _restPlusMonth = false;

        [DataMember]
        public bool RestPlusMonth
        {
            get { return _restPlusMonth; }
            set { _restPlusMonth = value; }
        }

        private decimal _janEnrollment = 0;
        [DataMember]
        public decimal JanEnrollment
        {
            get { return _janEnrollment; }
            set { _janEnrollment = value; }
        }

        private decimal _febEnrollment = 0;

        [DataMember]
        public decimal FebEnrollment
        {
            get { return _febEnrollment; }
            set { _febEnrollment = value; }
        }

        private decimal _marEnrollment = 0;
        [DataMember]
        public decimal MarEnrollment
        {
            get { return _marEnrollment; }
            set { _marEnrollment = value; }
        }

        private decimal _aprEnrollment = 0;
        [DataMember]
        public decimal AprEnrollment
        {
            get { return _aprEnrollment; }
            set { _aprEnrollment = value; }
        }

        private decimal _mayEnrollment = 0;

        [DataMember]
        public decimal MayEnrollment
        {
            get { return _mayEnrollment; }
            set { _mayEnrollment = value; }
        }

        private decimal _junEnrollment = 0;

        [DataMember]
        public decimal JunEnrollment
        {
            get { return _junEnrollment; }
            set { _junEnrollment = value; }
        }

        private decimal _julEnrollment = 0;

        [DataMember]
        public decimal JulEnrollment
        {
            get { return _julEnrollment; }
            set { _julEnrollment = value; }
        }

        private decimal _augEnrollment = 0;

        [DataMember]
        public decimal AugEnrollment
        {
            get { return _augEnrollment; }
            set { _augEnrollment = value; }
        }

        private decimal _sepEnrollment = 0;

        [DataMember]
        public decimal SepEnrollment
        {
            get { return _sepEnrollment; }
            set { _sepEnrollment = value; }
        }

        private decimal _octEnrollment = 0;

        [DataMember]
        public decimal OctEnrollment
        {
            get { return _octEnrollment; }
            set { _octEnrollment = value; }
        }

        private decimal _novEnrollment = 0;

        [DataMember]
        public decimal NovEnrollment
        {
            get { return _novEnrollment; }
            set { _novEnrollment = value; }
        }

        private decimal _decEnrollment = 0;
        [DataMember]
        public decimal DecEnrollment
        {
            get { return _decEnrollment; }
            set { _decEnrollment = value; }
        }

        private ExceMonthlyEnrollmentDC _temp = new ExceMonthlyEnrollmentDC();
        [DataMember]
        public ExceMonthlyEnrollmentDC Temp
        {
            get { return _temp; }
            set { _temp = value; }
        }

        private int _duePaymentAfter = 0;
        [DataMember]
        public int DuePaymentAfter
        {
            get { return _duePaymentAfter; }
            set { _duePaymentAfter = value; }
        }

        private decimal _invoiceCharge = 0;
        [DataMember]
        public decimal InvoiceCharge
        {
            get { return _invoiceCharge; }
            set { _invoiceCharge = value; }
        }

        private int _invoiceCancellationPeriod = 0;
        [DataMember]
        public int InvoiceCancellationPeriod
        {
            get { return _invoiceCancellationPeriod; }
            set { _invoiceCancellationPeriod = value; }
        }

        private bool _moveExceededPayToInvoice = false;
        [DataMember]
        public bool MoveExceededPayToInvoice
        {
            get { return _moveExceededPayToInvoice; }
            set { _moveExceededPayToInvoice = value; }
        }

        #endregion

        //Should be Delete
        #region SMS Settings

        #region Service SMS

        private int _serviceSMSFreq = -1;

        [DataMember]
        public int ServiceSMSFreq
        {
            get { return _serviceSMSFreq; }
            set { _serviceSMSFreq = value; }
        }

        private DateTime _receiveServiceSMSStart;

        [DataMember]
        public DateTime ReceiveServiceSMSStart
        {
            get { return _receiveServiceSMSStart; }
            set { _receiveServiceSMSStart = value; }
        }

        private DateTime _receiveServiceSMSEnd;

        [DataMember]
        public DateTime ReceiveServiceSMSEnd
        {
            get { return _receiveServiceSMSEnd; }
            set { _receiveServiceSMSEnd = value; }
        }

        #endregion

        #region Member SMS

        private int _memberSMSFreq = -1;

        [DataMember]
        public int MemberSMSFreq
        {
            get { return _memberSMSFreq; }
            set { _memberSMSFreq = value; }
        }

        private DateTime _receiveMemberSMSStart;

        [DataMember]
        public DateTime ReceiveMemberSMSStart
        {
            get { return _receiveMemberSMSStart; }
            set { _receiveMemberSMSStart = value; }
        }

        private DateTime _receiveMemberSMSEnd;

        [DataMember]
        public DateTime ReceiveMemberSMSEnd
        {
            get { return _receiveMemberSMSEnd; }
            set { _receiveMemberSMSEnd = value; }
        }

        #endregion

        #region Riminder SMS

        private int _remainderSMSFreq = -1;

        [DataMember]
        public int RemainderSMSFreq
        {
            get { return _remainderSMSFreq; }
            set { _remainderSMSFreq = value; }
        }

        private DateTime _receiveReminderSMSStart;

        [DataMember]
        public DateTime ReceiveReminderSMSStart
        {
            get { return _receiveReminderSMSStart; }
            set { _receiveReminderSMSStart = value; }
        }

        private DateTime _receiveReminderSMSEnd;

        [DataMember]
        public DateTime ReceiveReminderSMSEnd
        {
            get { return _receiveReminderSMSEnd; }
            set { _receiveReminderSMSEnd = value; }
        }

        #endregion

        private int _maxSMS = 0;

        [DataMember]
        public int MaxSMS
        {
            get { return _maxSMS; }
            set { _maxSMS = value; }
        }

        private int _sMSBookingReminder = 0;
        [DataMember]
        public int SMSBookingReminder
        {
            get { return _sMSBookingReminder; }
            set { _sMSBookingReminder = value; }
        }

        private int _sMSFreqDaysBeforeFreeze;
        [DataMember]
        public int SMSFreqDaysBeforeFreeze
        {
            get { return _sMSFreqDaysBeforeFreeze; }
            set { _sMSFreqDaysBeforeFreeze = value; }
        }

        #endregion

        //Should be Delete
        #region Other Settings

        private string _requiredPerGym = String.Empty;

        [DataMember]
        public string RequiredPerGym
        {
            get { return _requiredPerGym; }
            set { _requiredPerGym = value; }
        }

        private int _countVisitAfter;

        [DataMember]
        public int CountVisitAfter
        {
            get { return _countVisitAfter; }
            set { _countVisitAfter = value; }
        }

        private int _planIn = 0;

        [DataMember]
        public int PlanIn
        {
            get { return _planIn; }
            set { _planIn = value; }
        }

        private int _timeOut = 0;
        [DataMember]
        public int TimeOut
        {
            get { return _timeOut; }
            set { _timeOut = value; }
        }

        private bool _barcodeAvailable = false;
        [DataMember]
        public bool BarcodeAvailable
        {
            get { return _barcodeAvailable; }
            set { _barcodeAvailable = value; }
        }

        private bool _isPrintReceiptInShop = false;
        [DataMember]
        public bool IsPrintReceiptInShop
        {
            get { return _isPrintReceiptInShop; }
            set { _isPrintReceiptInShop = value; }
        }

        private bool _isPrintInvoiceInShop = false;
        [DataMember]
        public bool IsPrintInvoiceInShop
        {
            get { return _isPrintInvoiceInShop; }
            set { _isPrintInvoiceInShop = value; }
        }

        private string _phoneCode = string.Empty;
        [DataMember]
        public string PhoneCode
        {
            get { return _phoneCode; }
            set { _phoneCode = value; }
        }

        private bool _isAntiDopingRequired = false;
        [DataMember]
        public bool IsAntiDopingRequired
        {
            get { return _isAntiDopingRequired; }
            set { _isAntiDopingRequired = value; }
        }

        #endregion

        //Should be Delete
        private string _memberSearchText = string.Empty;
        [DataMember]
        public string MemberSearchText
        {
            get { return _memberSearchText; }
            set { _memberSearchText = value; }
        }

        //Should be Delete
        private List<ExceAccessProfileDC> _accessProfileList = new List<ExceAccessProfileDC>();
        [DataMember]
        public List<ExceAccessProfileDC> AccessProfileList
        {
            get { return _accessProfileList; }
            set { _accessProfileList = value; }
        }
        //Should be Delete
        private List<GymOpenTimeDC> _gymOpenTimes = new List<GymOpenTimeDC>();
        [DataMember]
        public List<GymOpenTimeDC> GymOpenTimes
        {
            get { return _gymOpenTimes; }
            set { _gymOpenTimes = value; }
        }

        //Should be Delete
        private List<GymIntegrationSettingsDC> _gymIntegration = new List<GymIntegrationSettingsDC>();
        [DataMember]
        public List<GymIntegrationSettingsDC> GymIntegration
        {
            get { return _gymIntegration; }
            set { _gymIntegration = value; }
        }

        private OtherIntegrationSettingsDC _otherIntegrationSettings = new OtherIntegrationSettingsDC();
        [DataMember]
        public OtherIntegrationSettingsDC OtherIntegrationSettings
        {
            get { return _otherIntegrationSettings; }
            set { _otherIntegrationSettings = value; }
        }

        //Should be Delete
        #region Required Fields

        private bool _memberRequired = false;

        [DataMember]
        public bool MemberRequired
        {
            get { return _memberRequired; }
            set { _memberRequired = value; }
        }

        private bool _mobileRequired = false;

        [DataMember]
        public bool MobileRequired
        {
            get { return _mobileRequired; }
            set { _mobileRequired = value; }
        }

        private bool _addressRequired = false;

        [DataMember]
        public bool AddressRequired
        {
            get { return _addressRequired; }
            set { _addressRequired = value; }
        }

        private bool _zipCodeRequired = false;

        [DataMember]
        public bool ZipCodeRequired
        {
            get { return _zipCodeRequired; }
            set { _zipCodeRequired = value; }
        }

        private decimal _negativeOnAccount;
        [DataMember]
        public decimal NegativeOnAccount
        {
            get { return _negativeOnAccount; }
            set { _negativeOnAccount = value; }
        }


        private bool _isShopOnNextOrder = false;
        [DataMember]
        public bool IsShopOnNextOrder
        {
            get { return _isShopOnNextOrder; }
            set { _isShopOnNextOrder = value; }
        }


        private bool _isDefaultCustomerAvailable = false;
        [DataMember]
        public bool IsDefaultCustomerAvailable
        {
            get { return _isDefaultCustomerAvailable; }
            set { _isDefaultCustomerAvailable = value; }
        }


        private bool _isPayButtonsAvailable = false;
        [DataMember]
        public bool IsPayButtonsAvailable
        {
            get { return _isPayButtonsAvailable; }
            set { _isPayButtonsAvailable = value; }
        }

        private bool _isOnAccount = false;
        [DataMember]
        public bool IsOnAccount
        {
            get { return _isOnAccount; }
            set { _isOnAccount = value; }
        }


        private bool _isPettyCashSameAsUserAllowed = false;
        [DataMember]
        public bool IsPettyCashSameAsUserAllowed
        {
            get { return _isPettyCashSameAsUserAllowed; }
            set { _isPettyCashSameAsUserAllowed = value; }
        }
        #endregion

        private bool _isBankTerminalIntegrated = false;
        [DataMember]
        public bool IsBankTerminalIntegrated
        {
            get { return _isBankTerminalIntegrated; }
            set { _isBankTerminalIntegrated = value; }
        }

        //----------------------- Member Fee-----------------------------------------------------
        private bool _isMemberFee;
        [DataMember]
        public bool IsMemberFee
        {
            get { return _isMemberFee; }
            set { _isMemberFee = value; }
        }

        private bool _isValidateShopGymId;
        [DataMember]
        public bool IsValidateShopGymId
        {
            get { return _isValidateShopGymId; }
            set { _isValidateShopGymId = value; }
        }

        private bool _isCommunicatorAvailable = false;
        [DataMember]
        public bool IsCommunicatorAvailable
        {
            get { return _isCommunicatorAvailable; }
            set { _isCommunicatorAvailable = value; }
        }
    }
}
