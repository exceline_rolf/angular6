﻿using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.ManageMemberships
{
    [DataContract]
    public class MemberIntegrationSettingDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _branchId = -1;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private int _memberId = -1;
        [DataMember]
        public int MemberId
        {
            get { return _memberId; }
            set { _memberId = value; }
        }

        private string _token = string.Empty;
        [DataMember]
        public string Token
        {
            get { return _token; }
            set { _token = value; }
        }

        private string _userId = string.Empty;
        [DataMember]
        public string UserId
        {
            get { return _userId; }
            set { _userId = value; }
        }

        private string _facilityUserId = string.Empty;
        [DataMember]
        public string FacilityUserId
        {
            get { return _facilityUserId; }
            set { _facilityUserId = value; }
        }

        private string _systemName = string.Empty;
        [DataMember]
        public string SystemName
        {
            get { return _systemName; }
            set { _systemName = value; }
        }

        private string _user = string.Empty;
        [DataMember]
        public string User
        {
            get { return _user; }
            set { _user = value; }
        }
    }
}
