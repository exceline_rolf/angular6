﻿using System;
using System.Runtime.Serialization;
using US.GMS.Core.SystemObjects;

namespace US.GMS.Core.DomainObjects.ManageMemberships
{
    [DataContract]
    public class ExcelineMemberDC
    {
        private int _id = 0;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _name = string.Empty;
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _roleId = string.Empty;
        [DataMember]
        public string RoleId
        {
            get { return _roleId; }
            set { _roleId = value; }
        }

        private string _firstName = string.Empty;
        [DataMember]
        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        private string _lastName = string.Empty;
        [DataMember]
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        private string _address1 = string.Empty;
        [DataMember]
        public string Address1
        {
            get { return _address1; }
            set { _address1 = value; }
        }

        private string _address2 = string.Empty;
        [DataMember]
        public string Address2
        {
            get { return _address2; }
            set { _address2 = value; }
        }

        private string _mobile = string.Empty;
        [DataMember]
        public string Mobile
        {
            get { return _mobile; }
            set { _mobile = value; }
        }

        private MemberRole _role = MemberRole.MEM;
        [DataMember]
        public MemberRole Role
        {
            get { return _role; }
            set { _role = value; }
        }

        private string _categoryName = string.Empty;
        [DataMember]
        public string CategoryName
        {
            get { return _categoryName; }
            set { _categoryName = value; }
        }

        private Gender _gender = Gender.NONE;
        [DataMember]
        public Gender Gender
        {
            get { return _gender; }
            set { _gender = value; }
        }

        private bool _isSelected = false;
        [DataMember]
        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; }
        }

        private string _custId = string.Empty;
        [DataMember]
        public string CustId
        {
            get { return _custId; }
            set { _custId = value; }
        }

        private string _mainContractNo = string.Empty;
        [DataMember]
        public string MainContractNo
        {
            get { return _mainContractNo;  }
            set { _mainContractNo = value; }
        }

        private int _intCustId;
        [DataMember]
        public int IntCustId
        {
            get { return _intCustId; }
            set { _intCustId = value; }
        }

        private string _statusName = string.Empty;
        [DataMember]
        public string StatusName
        {
            get { return _statusName; }
            set { _statusName = value; }
        }

        private DateTime? _introduceDate;
        [DataMember]
        public DateTime? IntroduceDate
        {
            get { return _introduceDate; }
            set { _introduceDate = value; }
        }

        private DateTime? _creditedDate;
        [DataMember]
        public DateTime? CreditedDate
        {
            get { return _creditedDate; }
            set { _creditedDate = value; }
        }

        private string _creditedText = string.Empty;
        [DataMember]
        public string CreditedText
        {
            get { return _creditedText; }
            set { _creditedText = value; }
        }

        private string _email = string.Empty;
        [DataMember]
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        private bool _hasIntroduceDate = true;
        [DataMember]
        public bool HasIntroduceDate
        {
            get { return _hasIntroduceDate; }
            set { _hasIntroduceDate = value; }
        }

        private DateTime? _birthDate;
        [DataMember]
        public DateTime? BirthDate
        {
            get { return _birthDate; }
            set { _birthDate = value; }
        }

        private string _branchName = string.Empty;
        [DataMember]
        public string BranchName
        {
            get { return _branchName; }
            set { _branchName = value; }
        }

        private int _branchID = -1;
        [DataMember]
        public int BranchID
        {
            get { return _branchID; }
            set { _branchID = value; }
        }

        private string _mobilePrefix = string.Empty;
        [DataMember]
        public string MobilePrefix
        {
            get { return _mobilePrefix; }
            set { _mobilePrefix = value; }
        }

        private string _postCode = string.Empty;
        [DataMember]
        public string PostCode
        {
            get { return _postCode; }
            set { _postCode = value; }
        }

        private string _zipCode = string.Empty;
        [DataMember]
        public string ZipCode
        {
            get { return _zipCode; }
            set { _zipCode = value; }
        }

        private string _zipName = string.Empty;
        [DataMember]
        public string ZipName
        {
            get { return _zipName; }
            set { _zipName = value; }
        }

        private string _homeGym = string.Empty;
        [DataMember]
        public string HomeGym
        {
            get { return _homeGym; }
            set { _homeGym = value; }
        }


        private bool _isCheckFamilymember = false;
        [DataMember]
        public bool IsCheckFamilymember
        {
            get { return _isCheckFamilymember; }
            set { _isCheckFamilymember = value; }
        }

        private int _age = 0;
        [DataMember]
        public int Age
        {
            get { return _age; }
            set { _age = value; }
        }

        private decimal _orderAmount = 0;
        [DataMember]
        public decimal OrderAmount
        {
            get { return _orderAmount; }
            set { _orderAmount = value; }
        }

        private int _activeStatus = 0;
        [DataMember]
        public int ActiveStatus
        {
            get { return _activeStatus; }
            set { _activeStatus = value; }
        }

        private string _contractNumber = string.Empty;
        [DataMember]
        public string ContractNumber
        {
            get { return _contractNumber; }
            set { _contractNumber = value; }
        }

        private bool _isHasEmail = false;
        [DataMember]
        public bool IsHasEmail
        {
            get { return _isHasEmail; }
            set { _isHasEmail = value; }
        }

        private bool _isHasntEmail = false;
        [DataMember]
        public bool IsHasntEmail
        {
            get { return _isHasntEmail; }
            set { _isHasntEmail = value; }
        }

        private DateTime _createdDate = new DateTime();
        [DataMember]
        public DateTime CreatedDate
        {
            get { return _createdDate; }
            set { _createdDate = value; }
        }

        private DateTime _contractSignDate = new DateTime();
        [DataMember]
        public DateTime ContractSignDate
        {
            get { return _contractSignDate; }
            set { _contractSignDate = value; }
        }

        private int _memberStatus = -1;
        [DataMember]
        public int MemberStatus
        {
            get { return _memberStatus; }
            set { _memberStatus = value; }
        }

        private DateTime? _gmStartDate;
        [DataMember]
        public DateTime? GmStartDate
        {
            get { return _gmStartDate; }
            set { _gmStartDate = value; }
        }

        private DateTime? _gmEndDate;
        [DataMember]
        public DateTime? GmEndDate
        {
            get { return _gmEndDate; }
            set { _gmEndDate = value; }
        }

        private bool _isFollowUp = false;
        [DataMember]
        public bool IsFollowUp
        {
            get { return _isFollowUp; }
            set { _isFollowUp = value; }
        }

        private int _count = -1;
        [DataMember]
        public int Count
        {
            get { return _count; }
            set { _count = value; }
        }

        private string _companyName = string.Empty;
        [DataMember]
        public string CompanyName
        {
            get { return _companyName; }
            set { _companyName = value; }
        }

        private bool _isDeleteButVisible;
        [DataMember]
        public bool IsDeleteButVisible
        {
            get { return _isDeleteButVisible; }
            set { _isDeleteButVisible = value; }
        }

        private int _guardianId;
        [DataMember]
        public int GuardianId
        {
            get { return _guardianId; }
            set { _guardianId = value; }
        }

       

    }
}
