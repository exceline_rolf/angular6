﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.ManageContracts;

namespace US.GMS.Core.DomainObjects.ManageMemberships
{
    [DataContract]
    public class InstallmentDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _noOfVisits = -1;
        [DataMember]
        public int NoOfVisits
        {
            get { return _noOfVisits; }
            set { _noOfVisits = value; }
        }

        private int _minVisits = -1;
        [DataMember]
        public int MinVisits
        {
            get { return _minVisits; }
            set { _minVisits = value; }
        }


        private string _installmentid = string.Empty;
        [DataMember]
        public string Installmentid
        {
            get { return _installmentid; }
            set { _installmentid = value; }
        }

        private int _sponsorArticleID = -1;
        [DataMember]
        public int SponsorArticleID
        {
            get { return _sponsorArticleID; }
            set { _sponsorArticleID = value; }
        }

        private int _discountArticleID = -1;
        [DataMember]
        public int DiscountArticleID
        {
            get { return _discountArticleID; }
            set { _discountArticleID = value; }
        }

        private string _adonId;
        [DataMember]
        public string AdonId
        {
            get { return _adonId; }
            set { _adonId = value; }
        }

        private int _installmentNo = 0;
        [DataMember]
        public int InstallmentNo
        {
            get { return _installmentNo; }
            set { _installmentNo = value; }
        }

        private int _installmentId = -1;
        [DataMember]
        public int InstallmentId
        {
            get { return _installmentId; }
            set { _installmentId = value; }
        }

        private int _memberId = -1;
        [DataMember]
        public int MemberId
        {
            get { return _memberId; }
            set { _memberId = value; }
        }

        private int _contractId = -1;
        [DataMember]
        public int ContractId
        {
            get { return _contractId; }
            set { _contractId = value; }
        }

        private int _memberContractId = -1;
        [DataMember]
        public int MemberContractId
        {
            get { return _memberContractId; }
            set { _memberContractId = value; }
        }

        private string _text = string.Empty;
        [DataMember]
        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        private DateTime _installmentDate;
        [DataMember]
        public DateTime InstallmentDate
        {
            get { return _installmentDate; }
            set { _installmentDate = value; }
        }

        private DateTime _dueDate;
        [DataMember]
        public DateTime DueDate
        {
            get { return _dueDate; }
            set { _dueDate = value; }
        }

        private DateTime? _paidDate;
        [DataMember]
        public DateTime? PaidDate
        {
            get { return _paidDate; }
            set { _paidDate = value; }
        }

        private int _tableId;
        [DataMember]
        public int TableId
        {
            get { return _tableId; }
            set { _tableId = value; }
        }

        private decimal _amount = 0;
        [DataMember]
        public decimal Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

        private DateTime? _billingDate;
        [DataMember]
        public DateTime? BillingDate
        {
            get { return _billingDate; }
            set { _billingDate = value; }
        }

        private DateTime? _debtCollectionDate;
        [DataMember]
        public DateTime? DebtCollectionDate
        {
            get { return _debtCollectionDate; }
            set { _debtCollectionDate = value; }
        }

        private DateTime? _printedDate;
        [DataMember]
        public DateTime? PrintedDate
        {
            get { return _printedDate; }
            set { _printedDate = value; }
        }

        private string _submitted;
        [DataMember]
        public string Submitted
        {
            get { return _submitted; }
            set { _submitted = value; }
        }

        private string _registeredBy;
        [DataMember]
        public string RegisteredBy
        {
            get { return _registeredBy; }
            set { _registeredBy = value; }
        }

        private string _memberName;
        [DataMember]
        public string MemberName
        {
            get { return _memberName; }
            set { _memberName = value; }
        }

        private string _activity;
        [DataMember]
        public string Activity
        {
            get { return _activity; }
            set { _activity = value; }
        }

        private decimal _billingFee;
        [DataMember]
        public decimal BillingFee
        {
            get { return _billingFee; }
            set { _billingFee = value; }
        }

        private decimal _reminderFee;
        [DataMember]
        public decimal ReminderFee
        {
            get { return _reminderFee; }
            set { _reminderFee = value; }
        }

        private decimal _adonPrice;
        [DataMember]
        public decimal AdonPrice
        {
            get { return _adonPrice; }
            set { _adonPrice = value; }
        }

        private string _adonName;
        [DataMember]
        public string AdonName
        {
            get { return _adonName; }
            set { _adonName = value; }
        }

        private decimal _discount;
        [DataMember]
        public decimal Discount
        {
            get { return _discount; }
            set { _discount = value; }
        }

        private decimal _sponsoredAmount;
        [DataMember]
        public decimal SponsoredAmount
        {
            get { return _sponsoredAmount; }
            set { _sponsoredAmount = value; }
        }

        private DateTime? _originalDueDate;
        [DataMember]
        public DateTime? OriginalDueDate
        {
            get { return _originalDueDate; }
            set { _originalDueDate = value; }
        }

        private DateTime? _sentToDebtCollection;
        [DataMember]
        public DateTime? SentToDebtCollection
        {
            get { return _sentToDebtCollection; }
            set { _sentToDebtCollection = value; }
        }

        private DateTime? _lastReminderDate;
        [DataMember]
        public DateTime? LastReminderDate
        {
            get { return _lastReminderDate; }
            set { _lastReminderDate = value; }
        }

        private int _numberOfMembers = 0;
        [DataMember]
        public int NumberOfMembers
        {
            get { return _numberOfMembers; }
            set { _numberOfMembers = value; }
        }

        private string _priceListItem = string.Empty;
        [DataMember]
        public string PriceListItem
        {
            get { return _priceListItem; }
            set { _priceListItem = value; }
        }

        private string _kid = string.Empty;
        [DataMember]
        public string KID
        {
            get { return _kid; }
            set { _kid = value; }
        }

        private string _comment = string.Empty;
        [DataMember]
        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        private int _numberOfReminders = -1;
        [DataMember]
        public int NumberOfReminders
        {
            get { return _numberOfReminders; }
            set { _numberOfReminders = value; }
        }

        private string _group = string.Empty;
        [DataMember]
        public string Group
        {
            get { return _group; }
            set { _group = value; }
        }

        private DateTime _transferDate;
        [DataMember]
        public DateTime TransferDate
        {
            get { return _transferDate; }
            set { _transferDate = value; }
        }

        private decimal _balance = 0;
        [DataMember]
        public decimal Balance
        {
            get { return _balance; }
            set { _balance = value; }
        }

        private string _invoiceNo = string.Empty;
        [DataMember]
        public string InvoiceNo
        {
            get { return _invoiceNo; }
            set { _invoiceNo = value; }
        }

        private DateTime? _invoiceGeneratedDate;
        [DataMember]
        public DateTime? InvoiceGeneratedDate
        {
            get { return _invoiceGeneratedDate; }
            set { _invoiceGeneratedDate = value; }
        }

        private int _generatedInvoices = 0;
        [DataMember]
        public int GeneratedInvoices
        {
            get { return _generatedInvoices; }
            set { _generatedInvoices = value; }
        }

        private List<ContractItemDC> _addonList = new List<ContractItemDC>();
        [DataMember]
        public List<ContractItemDC> AddOnList
        {
            get { return _addonList; }
            set { _addonList = value; }
        }

        private DateTime? trainingPeriodStart;
        [DataMember]
        public DateTime? TrainingPeriodStart
        {
            get { return trainingPeriodStart; }
            set { trainingPeriodStart = value; }
        }

        private DateTime? trainingPeriodEnd;
        [DataMember]
        public DateTime? TrainingPeriodEnd
        {
            get { return trainingPeriodEnd; }
            set { trainingPeriodEnd = value; }
        }

        private bool _isATG = false;
        [DataMember]
        public bool IsATG
        {
            get { return _isATG; }
            set { _isATG = value; }
        }

        private bool _isOtherPay = false;
        [DataMember]
        public bool IsOtherPay
        {
            get { return _isOtherPay; }
            set { _isOtherPay = value; }
        }


        private bool _isDebit = false;
        [DataMember]
        public bool IsDebit
        {
            get { return _isDebit; }
            set { _isDebit = value; }
        }

        private bool _isDeleteRequest = false;
        [DataMember]
        public bool IsDeleteRequest
        {
            get { return _isDeleteRequest; }
            set { _isDeleteRequest = value; }
        }

        private bool _isTreat = false;
        [DataMember]
        public bool IsTreat
        {
            get { return _isTreat; }
            set { _isTreat = value; }
        }

        private string _memberContractNo = string.Empty;
        [DataMember]
        public string MemberContractNo
        {
            get { return _memberContractNo; }
            set { _memberContractNo = value; }
        }

        private bool _isLocked = false;
        [DataMember]
        public bool IsLocked
        {
            get { return _isLocked; }
            set { _isLocked = value; }
        }

        private bool _isPaid = false;
        [DataMember]
        public bool IsPaid
        {
            get { return _isPaid; }
            set { _isPaid = value; }
        }

        private bool _isInvoiced = false;
        [DataMember]
        public bool IsInvoiced
        {
            get { return _isInvoiced; }
            set { _isInvoiced = value; }
        }

        private string _contractName = string.Empty;
        [DataMember]
        public string ContractName
        {
            get { return _contractName; }
            set { _contractName = value; }
        }

        private string _activityName = string.Empty;
        [DataMember]
        public string ActivityName
        {
            get { return _activityName; }
            set { _activityName = value; }
        }

        private string _createdUser = string.Empty;
        [DataMember]
        public string CreatedUser
        {
            get { return _createdUser; }
            set { _createdUser = value; }
        }

        private string _status = string.Empty;
        [DataMember]
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        private string _orderNo = string.Empty;
        [DataMember]
        public string OrderNo
        {
            get { return _orderNo; }
            set { _orderNo = value; }
        }

        private int _payerId = -1;
        [DataMember]
        public int PayerId
        {
            get { return _payerId; }
            set { _payerId = value; }
        }

        private string _payerName = string.Empty;
        [DataMember]
        public string PayerName
        {
            get { return _payerName; }
            set { _payerName = value; }
        }

        private string _originalCustomer = string.Empty;
        [DataMember]
        public string OriginalCustomer
        {
            get { return _originalCustomer; }
            set { _originalCustomer = value; }
        }

        private string _adonText = string.Empty;
        [DataMember]
        public string AdonText
        {
            get { return _adonText; }
            set { _adonText = value; }
        }

        private bool _isSpOrder = false;
        [DataMember]
        public bool IsSpOrder
        {
            get { return _isSpOrder; }
            set { _isSpOrder = value; }
        }

        private decimal _activityPrice = 0;
        [DataMember]
        public decimal ActivityPrice
        {
            get { return _activityPrice; }
            set { _activityPrice = value; }
        }

        private decimal _estimatedOrderAmount = 0;
        [DataMember]
        public decimal EstimatedOrderAmount
        {
            get { return _estimatedOrderAmount; }
            set { _estimatedOrderAmount = value; }
        }

        private bool _isInvoiceFeeAdded = false;
        [DataMember]
        public bool IsInvoiceFeeAdded
        {
            get { return _isInvoiceFeeAdded; }
            set { _isInvoiceFeeAdded = value; }
        }

        private DateTime? _estimatedInvoiceDate;
        [DataMember]
        public DateTime? EstimatedInvoiceDate
        {
            get { return _estimatedInvoiceDate; }
            set { _estimatedInvoiceDate = value; }
        }

        private string _branchName = string.Empty;
        [DataMember]
        public string BranchName
        {
            get { return _branchName; }
            set { _branchName = value; }
        }

        private decimal _serviceAmount = 0;
        [DataMember]
        public decimal ServiceAmount
        {
            get { return _serviceAmount; }
            set { _serviceAmount = value; }
        }

        private decimal _itemAmount = 0;
        [DataMember]
        public decimal ItemAmount
        {
            get { return _itemAmount; }
            set { _itemAmount = value; }
        }

        private string _installmentType = string.Empty;
        [DataMember]
        public string InstallmentType
        {
            get { return _installmentType; }
            set { _installmentType = value; }
        }

        private List<SponsorshipItemDC> _sponsorItemList = new List<SponsorshipItemDC>();
        [DataMember]
        public List<SponsorshipItemDC> SponsorItemList
        {
            get { return _sponsorItemList; }
            set { _sponsorItemList = value; }
        }

        private int _templateId = -1;
        [DataMember]
        public int TemplateId
        {
            get { return _templateId; }
            set { _templateId = value; }
        }

        private bool _isSelected = false;
        [DataMember]
        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; }
        }

        private bool _isSponsored = false;
        [DataMember]
        public bool IsSponsored
        {
            get { return _isSponsored; }
            set { _isSponsored = value; }
        }

        private bool _orderOperationsEnabled = true;
        [DataMember]
        public bool OrderOperationsEnabled
        {
            get { return _orderOperationsEnabled; }
            set { _orderOperationsEnabled = value; }
        }

        private bool _enableInvoice = true;
        [DataMember]
        public bool EnableInvoice
        {
            get { return _enableInvoice; }
            set { _enableInvoice = value; }
        }

        private string _sponsorName = string.Empty;
        [DataMember]
        public string SponsorName
        {
            get { return _sponsorName; }
            set { _sponsorName = value; }
        }

        private bool _isCheckOutEnable = true;
        [DataMember]
        public bool IsCheckOutEnable
        {
            get { return _isCheckOutEnable; }
            set { _isCheckOutEnable = value; }
        }

        private int _gymID = -1;
        [DataMember]
        public int GymID
        {
            get { return _gymID; }
            set { _gymID = value; }
        }

        private bool _btnDeleteOrderVisible = true;
        [DataMember]
        public bool BtnDeleteOrderVisible
        {
            get { return _btnDeleteOrderVisible; }
            set { _btnDeleteOrderVisible = value; }
        }

        private bool _btnDeleteOrderEnable = true;
        [DataMember]
        public bool BtnDeleteOrderEnable
        {
            get { return _btnDeleteOrderEnable; }
            set { _btnDeleteOrderEnable = value; }
        }

        private int _creditPeriod = 0;
        [DataMember]
        public int CreditPeriod
        {
            get { return _creditPeriod; }
            set { _creditPeriod = value; }
        }

        private int _memberCreditPeriod = 0;
        [DataMember]
        public int MemberCreditPeriod
        {
            get { return _memberCreditPeriod; }
            set { _memberCreditPeriod = value; }
        }

        private int _sponsorCreditPeriod = 0;
        [DataMember]
        public int SponsorCreditPeriod
        {
            get { return _sponsorCreditPeriod; }
            set { _sponsorCreditPeriod = value; }
        }

        //------UI  SUb Operations --------------------
        private bool _isOrderCheckOutvisible = true;
        [DataMember]
        public bool IsOrderCheckOutvisible
        {
            get { return _isOrderCheckOutvisible; }
            set { _isOrderCheckOutvisible = value; }
        }

        private bool _isOrderSMSEmailvisible = true;
        [DataMember]
        public bool IsOrderSMSEmailvisible
        {
            get { return _isOrderSMSEmailvisible; }
            set { _isOrderSMSEmailvisible = value; }
        }

        private bool _isOrderInvoicevisible = true;
        [DataMember]
        public bool IsOrderInvoicevisible
        {
            get { return _isOrderInvoicevisible; }
            set { _isOrderInvoicevisible = value; }
        }

        private bool _HasAddonFromShop = false;
        [DataMember]
        public bool HasAddonFromShop
        {
            get { return _HasAddonFromShop; }
            set { _HasAddonFromShop = value; }
        }
    }
}
