﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.SystemObjects;
using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.ManageMemberships
{
    [DataContract]
    public class VIPMemberDC : MemberDC
    {
        private int _id = 0;
        [DataMember]
        public override int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _firstName = string.Empty;
        [DataMember]
        public override string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        private string _lastName = string.Empty;
        [DataMember]
        public override string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        private Gender _gender = Gender.NONE;
        [DataMember]
        public override Gender Gender
        {
            get { return _gender; }
            set { _gender = value; }
        }

        private DateTime _birthDate;
        [DataMember]
        public override DateTime BirthDate
        {
            get { return _birthDate; }
            set { _birthDate = value; }
        }

        private int _entNo = 0;
        [DataMember]
        public override int EntNo
        {
            get { return _entNo; }
            set { _entNo = value; }
        }

        private int _age = 0;
        [DataMember]
        public override int Age
        {
            get { return _age; }
            set { _age = value; }
        }       

        private string _postCode = string.Empty;
        [DataMember]
        public override string PostCode
        {
            get { return _postCode; }
            set { _postCode = value; }
        }

        private string _postPlace = string.Empty;
        [DataMember]
        public override string PostPlace
        {
            get { return _postPlace; }
            set { _postPlace = value; }
        }

        private string _email = string.Empty;
        [DataMember]
        public override string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        private string _mobile = string.Empty;
        [DataMember]
        public override string Mobile
        {
            get { return _mobile; }
            set { _mobile = value; }
        }

        private string _workTeleNo = string.Empty;
        [DataMember]
        public override string WorkTeleNo
        {
            get { return _workTeleNo; }
            set { _workTeleNo = value; }
        }

        private string _privateTeleNo = string.Empty;
        [DataMember]
        public override string PrivateTeleNo
        {
            get { return _privateTeleNo; }
            set { _privateTeleNo = value; }
        }

        private string _department = string.Empty;
        [DataMember]
        public override string Department
        {
            get { return _department; }
            set { _department = value; }
        }

        private string _instructor = string.Empty;
        [DataMember]
        public override string Instructor
        {
            get { return _instructor; }
            set { _instructor = value; }
        }

        private string _accountNo = string.Empty;
        [DataMember]
        public override string AccountNo
        {
            get { return _accountNo; }
            set { _accountNo = value; }
        }

        private DateTime _contractStartDate;
        [DataMember]
        public override DateTime ContractStartDate
        {
            get { return _contractStartDate; }
            set { _contractStartDate = value; }
        }

        private DateTime _contractEndDate;
        [DataMember]
        public override DateTime ContractEndDate
        {
            get { return _contractEndDate; }
            set { _contractEndDate = value; }
        }

        private string _companyName = string.Empty;
        [DataMember]
        public override string CompanyName
        {
            get { return _companyName; }
            set { _companyName = value; }
        }

        private string _employeeNo = string.Empty;
        [DataMember]
        public override string EmployeeNo
        {
            get { return _employeeNo; }
            set { _employeeNo = value; }
        }

        //Sponsor and Sponsorship

        private byte[] _profilePicture = null;
        [DataMember]
        public override byte[] ProfilePicture
        {
            get { return _profilePicture; }
            set { _profilePicture = value; }
        }

        private int _branchId = 0;
        [DataMember]
        public override int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }
        private string _address1;
        [DataMember]
        public override string Address1
        {
            get { return _address1; }
            set { _address1 = value; }
        }

        private string _address2;
        [DataMember]
        public override string Address2
        {
            get { return _address2; }
            set { _address2 = value; }
        }
        private string _address3;
        [DataMember]
        public override string Address3
        {
            get { return _address3; }
            set { _address3 = value; }
        }

        private string _createdUser;
        [DataMember]
        public override string CreatedUser
        {
            get { return _createdUser; }
            set { _createdUser = value; }
        }

        private string _modifiedUser;
        [DataMember]
        public override string ModifiedUser
        {
            get { return _modifiedUser; }
            set { _modifiedUser = value; }
        }

        private string _personNo;
        [DataMember]
        public override string PersonNo
        {
            get { return _personNo; }
            set { _personNo = value; }
        }

        private bool _activeStatus;
        [DataMember]
        public override bool ActiveStatus
        {
            get { return _activeStatus; }
            set { _activeStatus = value; }
        }
        private string _name;
        [DataMember]
        public override string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        private bool _isSelected;
        [DataMember]
        public override bool IsSelected
        {
            get
            {
                return _isSelected;
            }
            set
            {
                _isSelected = value;
            }
        }

        public override string ImagePath
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override string Image
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override string Description
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }
}
