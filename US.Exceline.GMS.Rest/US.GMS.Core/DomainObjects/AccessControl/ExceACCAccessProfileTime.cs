﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.DomainObjects.AccessControl
{
    [DataContract]
    public class ExceACCAccessProfileTime
    {
        private string _id = string.Empty;
        [DataMember]
        public string Id
        {
            get { return _id; }
            set { _id = value; }
        }


        private string _accessProfileId = string.Empty;
        [DataMember]
        public string AccessProfileId
        {
            get { return _accessProfileId; }
            set { _accessProfileId = value; }
        }

        private string _assignedDays = string.Empty;
        [DataMember]
        public string AssignedDays
        {
            get { return _assignedDays; }
            set { _assignedDays = value; }
        }

        private string _accessProfileName = string.Empty;
        [DataMember]
        public string AccessProfileName
        {
            get { return _accessProfileName; }
            set { _accessProfileName = value; }
        }

        private string _fromTime;
        [DataMember]
        public string FromTime
        {
            get { return _fromTime; }
            set { _fromTime = value; }
        }

        private string _toTime;
        [DataMember]
        public string ToTime
        {
            get { return _toTime; }
            set { _toTime = value; }
        }

        private string _monday = string.Empty;
        [DataMember]
        public string Monday
        {
            get { return _monday; }
            set { _monday = value; }
        }

        private string _tuesday = string.Empty;
        [DataMember]
        public string Tuesday
        {
            get { return _tuesday; }
            set { _tuesday = value; }
        }

        private string _wednesday = string.Empty;
        [DataMember]
        public string Wednesday
        {
            get { return _wednesday; }
            set { _wednesday = value; }
        }

        private string _thursday = string.Empty;
        [DataMember]
        public string Thursday
        {
            get { return _thursday; }
            set { _thursday = value; }
        }

        private string _friday = string.Empty;
        [DataMember]
        public string Friday
        {
            get { return _friday; }
            set { _friday = value; }
        }

        private string _saturday = string.Empty;
        [DataMember]
        public string Saturday
        {
            get { return _saturday; }
            set { _saturday = value; }
        }

        private string _sunday = string.Empty;
        [DataMember]
        public string Sunday
        {
            get { return _sunday; }
            set { _sunday = value; }
        }

        private string _allowExpressGym = string.Empty;
        [DataMember]
        public string AllowExpressGym
        {
            get { return _allowExpressGym; }
            set { _allowExpressGym = value; }
        }

        private string _allowExtraActivity = string.Empty;
        [DataMember]
        public string AllowExtraActivity
        {
            get { return _allowExtraActivity; }
            set { _allowExtraActivity = value; }
        }

        private List<int> _branchIdList;
        [DataMember]
        public List<int> BranchIdList
        {
            get { return _branchIdList; }
            set { _branchIdList = value; }
        }
     
        private string _gender = string.Empty;
        [DataMember]
        public string Gender
        {
            get { return _gender; }
            set { _gender = value; }
        }



    }
}
