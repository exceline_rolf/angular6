﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.GMS.Core.DomainObjects.AccessControl
{
    public class ExceACCTerminal
    {
        private int _terminalID = -1;
        public int TerminalID
        {
            get { return _terminalID; }
            set { _terminalID = value; }
        }

        private int _activityId = -1;
        public int ActivityId
        {
            get { return _activityId; }
            set { _activityId = value; }
        }

        private List<int> _accessProfileIDList = new List<int>();
        public List<int> AccessProfileIDList
        {
            get { return _accessProfileIDList; }
            set { _accessProfileIDList = value; }
        }
    }
}
