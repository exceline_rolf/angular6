﻿using System.Runtime.Serialization;

namespace US.GMS.Core.DomainObjects.AccessControl
{
    public class ExceAccessControlRegisterPurchaseDC
    {
        private string _cardNo;
        private decimal _balance;
        private int _branchId;
        private string _itemCode;
        private decimal _itemPrice;
        private int _terminalId;
        private bool _isVending;

        [DataMember(Order = 1)]
        public string CardNo
        {
            get { return _cardNo; }
            set { _cardNo = value; }
        }

        [DataMember(Order = 2)]
        public decimal Balance
        {
            get { return _balance; }
            set { _balance = value; }
        }

        [DataMember(Order = 3)]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        [DataMember(Order = 4)]
        public string ItemCode
        {
            get { return _itemCode; }
            set { _itemCode = value; }
        }

        [DataMember(Order = 5)]
        public decimal ItemPrice
        {
            get { return _itemPrice; }
            set { _itemPrice = value; }
        }

        [DataMember(Order = 6)]
        public int TerminalId
        {
            get { return _terminalId; }
            set { _terminalId = value; }
        }

        public bool IsVending
        {
            get { return _isVending; }
            set { _isVending = value; }
        }
    }
}
