﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.DomainObjects.AccessControl
{
    [DataContract]
    public class ExcACCCommand
    {
        private string _entityName = string.Empty;
        [DataMember]
        public string EntityName
        {
            get { return _entityName; }
            set { _entityName = value; }
        }

        private string _command = string.Empty;
        [DataMember]
        public string Cmd
        {
            get { return _command; }
            set { _command = value; }
        }

        private string _lastUpdateTime;
        [DataMember]
        public string LastUpdateTime
        {
          get { return _lastUpdateTime; }
          set { _lastUpdateTime = value; }
        }
    }
}
