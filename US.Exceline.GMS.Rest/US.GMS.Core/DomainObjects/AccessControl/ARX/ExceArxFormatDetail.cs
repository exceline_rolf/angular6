﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.DomainObjects.AccessControl.ARX
{
    [DataContract]
   public class ExceArxFormatDetail
    {
       
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        
        [DataMember]
        public int FormatTypeId
        {
            get { return _formatTypeId; }
            set { _formatTypeId = value; }
        }

        [DataMember]
        public int? MinLength
        {
            get { return _minLength; }
            set { _minLength = value; }
        }

        [DataMember]
        public int? MaxLength
        {
            get { return _maxLength; }
            set { _maxLength = value; }
        }

        [DataMember]
        public int TempCodeDeleteDate
        {
            get { return _tempCodeDeleteDate; }
            set { _tempCodeDeleteDate = value; }
        }

        private int _tempCodeDeleteDate;

        private int _id;

        private int _formatTypeId;

        private int? _minLength;

        private int? _maxLength;

    }
}
