﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.SystemObjects;

namespace US.GMS.Core.Utils
{
    public class ApiUtils
    {
        public static ServiceResponse CreateApiResponse(object data, ApiResponseStatus status, List<string> messages = null)
        {
            return new ServiceResponse() { Data = data, Status = status.ToString(), Messages = messages };
        }
    }
}
