﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.GMS.Core.Utils
{
    public class ExceConnectionManager
    {
        public static string GetGymCode(string user)
        {
            string gymCode = string.Empty;
            if (!string.IsNullOrEmpty(user))
            {
                string[] userParts = user.Split(new char[] { '/' });
                if (userParts.Length > 0)
                {
                    gymCode = userParts[0];
                }
            }
            return gymCode;
        }
    }
}
