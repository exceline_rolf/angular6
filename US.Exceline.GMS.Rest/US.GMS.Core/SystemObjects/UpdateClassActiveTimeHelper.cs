﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.SystemObjects
{
    [DataContract]
    public class UpdateClassActiveTimeHelperDC
    {
        
        private List<int> _addedINSIDList;
        [DataMember]
        public List<int> AddedINSIDList
        {
            get { return _addedINSIDList; }
            set { _addedINSIDList = value; }
        }
        private List<int> _deletedINSIDList;
        [DataMember]
        public List<int> DeletedINSIDList
        {
            get { return _deletedINSIDList; }
            set { _deletedINSIDList = value; }
        }
        private List<int> _addedRESIDList;
        [DataMember]
        public List<int> AddedRESIDList
        {
            get { return _addedRESIDList; }
            set { _addedRESIDList = value; }
        }
        private List<int> _deletedRESIDList;
        [DataMember]
        public List<int> DeletedRESIDList
        {
            get { return _deletedRESIDList; }
            set { _deletedRESIDList = value; }
        }

        private List<int> _allINSIDList;
        [DataMember]
        public List<int> AllINSIDList
        {
            get { return _allINSIDList; }
            set { _allINSIDList = value; }
        }
        private List<int> _allRESIDList;
        [DataMember]
        public List<int> AllRESIDList
        {
            get { return _allRESIDList; }
            set { _allRESIDList = value; }
        }
        private DateTime _startDate;
        [DataMember]
        public DateTime StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }
        private DateTime _startDateTime;
        [DataMember]
        public DateTime StartDateTime
        {
            get { return _startDateTime; }
            set { _startDateTime = value; }
        }
        private DateTime _endDateTime;
        [DataMember]
        public DateTime EndDateTime
        {
            get { return _endDateTime; }
            set { _endDateTime = value; }
        }
        private int _activeTimeId;
        [DataMember]
        public int ActiveTimeId
        {
            get { return _activeTimeId; }
            set { _activeTimeId = value; }
        }

        private int _maxNoOfBookings;
        [DataMember]
        public int MaxNumberOfBookings
        {
            get { return _maxNoOfBookings; }
            set { _maxNoOfBookings = value; }
        }

        private int _noOfParticipants;
        [DataMember]
        public int NoOfParticipants
        {
            get { return _noOfParticipants; }
            set { _noOfParticipants = value; }
        }

    }
}
