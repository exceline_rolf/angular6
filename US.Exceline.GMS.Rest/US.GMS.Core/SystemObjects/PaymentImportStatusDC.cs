﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.SystemObjects
{
    [DataContract]
    public class PaymentImportStatusDC
    {
        private int _totalImported = 0;
        [DataMember]
        public int TotalImported
        {
            get { return _totalImported; }
            set { _totalImported = value; }
        }

        private int _errorCount = 0;
        [DataMember]
        public int ErrorCount
        {
            get { return _errorCount; }
            set { _errorCount = value; }
        }

        private int _successCount = 0;
        [DataMember]
        public int SuccessCount
        {
            get { return _successCount; }
            set { _successCount = value; }
        }

        private string _gymCompanyName = string.Empty;
        [DataMember]
        public string GymCompanyName
        {
            get { return _gymCompanyName; }
            set { _gymCompanyName = value; }
        }

        private List<ErrorPaymentDC> _errorPaymentsList = new List<ErrorPaymentDC>();
        [DataMember]
        public List<ErrorPaymentDC> ErrorPaymentsList
        {
            get { return _errorPaymentsList; }
            set { _errorPaymentsList = value; }
        }
    }
}
