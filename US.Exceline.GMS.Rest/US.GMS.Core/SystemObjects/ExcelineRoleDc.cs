﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.SystemObjects
{
    [DataContract]
    public class ExcelineRoleDc
    {
        private int _id;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _roleName = string.Empty;
        [DataMember]
        public string RoleName
        {
            get { return _roleName; }
            set { _roleName = value; }
        }

        private int _roleId;
         [DataMember]
        public int RoleId
        {
            get { return _roleId; }
            set { _roleId = value; }
        }

        private bool _isActive;
         [DataMember]
        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }

       private List<int> _branchList = new List<int>();
          [DataMember]
        public List<int> BranchList
        {
            get { return _branchList; }
            set { _branchList = value; }
        }

          private int _branchId;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        


    }
}
