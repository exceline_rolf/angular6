﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Common.Web.UI.Core.SystemObjects;

namespace US.GMS.Core.SystemObjects
{
    public class UspUsersExtended : USPUser
    {
        private Dictionary<int, int> _uniqueOperationIdList = new Dictionary<int, int>();
        public Dictionary<int, int> UniqueOperationIdList
        {
            get { return _uniqueOperationIdList; }
            set { _uniqueOperationIdList = value;}
        }
    }
}
