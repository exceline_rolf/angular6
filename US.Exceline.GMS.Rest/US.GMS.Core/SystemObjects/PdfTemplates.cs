﻿using System;
using System.Drawing;
using System.IO;

namespace US.GMS.Core.SystemObjects.Enums
{
    public class PdfTemplates
    {
        private String HTML = string.Empty;

        public String getHTMLTemplate()
        {
            if (HTML != string.Empty)
            {
                return HTML;
            }
            else
                return "ERROR";
        }

        public String FillMe(String Input, int amount = 34)
        {
            String result = Input;
            String space = "&nbsp;";
            int inputLength = Input.Length;
            for (int i = inputLength; i <= amount; i++)
            {
                space += "&nbsp;";
            }
            return space + result;
        }

        public PdfTemplates(Enum template, int numberOfOrderLines = 0, bool isAtg = false, int gymId = 0)
        {

            Image gymImage = File.Exists(@"D:\Exceline\Instances\Pilot2\Pilot2Web\images\GymLogo\" + gymId + ".png") == false ? Image.FromFile(@"D:\Exceline\Instances\Pilot2\Pilot2Web\images\GymLogo\" + 0 + ".png") : Image.FromFile(@"D:\Exceline\Instances\Pilot2\Pilot2Web\images\GymLogo\" + gymId + ".png");
            gymId = File.Exists(@"D:\Exceline\Instances\Pilot2\Pilot2Web\images\GymLogo\" + gymId + ".png") == false ? 0 : gymId;

            String Width = (gymImage.Width).ToString();

            String Height = gymImage.Height.ToString();

            switch (template)
            {
                case PdfTemplatesTypes.INVOICE_EXCELINE:
                    
                    HTML = @"<DOCTYPE html><html><body>";

                    HTML += $"<img height= \"{Height}\" width= \"{Width}\" " + $@"src=D:\Exceline\Instances\Pilot2\Pilot2Web\images\GymLogo\{gymId}.png><br><br><br>";

                    HTML += @"<table style = ""border-spacing: 0 ;border-collapse: collapse; "" > ";

                    HTML += "<tr>";
                    HTML += @"<td width= ""56%"" style= ""padding-left: 0cm"">{PayerName}</td>";
                    HTML += @"<td width= ""15%"">Fakturanummer</td>";
                    HTML += @"<td style = ""margin-left: 12px"">: {InvoiceNumber}</td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width= ""56%"" style= ""padding-left: 0cm"">{PayerAdress}</td>";
                    HTML += @"<td width= ""15%"">Forfallsdato</td>";
                    HTML += @"<td style = ""margin-left: 12px"">: {PaidByDate}</td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width= ""56%"" style= ""padding-left: 0cm"">{PayerPostNumber}</td>";
                    HTML += @"<td width= ""15%"">Fakturadato</td>";
                    HTML += @"<td style = ""margin-left: 12px"">: {InvoiceDate}</td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width= ""56%"" style= ""padding-left: 0cm""></td>";
                    HTML += @"<td width= ""15%"">Kundenummer</td>";
                    HTML += @"<td style = ""margin-left: 12px"">: {CustomerId}</td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width= ""56%"" style= ""padding-left: 0cm""><PaidOnBehalfOfCustId></td>";
                    HTML += @"<td width= ""15%"">TIL KONTO</td>";
                    HTML += @"<td style = ""margin-left: 12px"">: {ReceiverAccountNumber}</td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width= ""56%"" style= ""padding-left: 0cm""><PaidOnBehalfOfName></td>";
                    HTML += @"<td width= ""15%"">KID</td>";
                    HTML += @"<td style = ""margin-left: 12px"">: {KID}</td>";
                    HTML += "</tr>";

                    HTML += "<Refrence>";
                    HTML += "<EmployeeNo>";

                    HTML += "</table>";

                    HTML += "<br><span> </span><br>";
                    HTML += "<br><br><span> </span><br><br>";

                    HTML += @"<span style = ""margin-left: 190px""> </span> 
                            <b style= ""font-size: 20px"">F A K T U R A </b><br>";

                    HTML += @"<table style = ""border-spacing: 5"">";
                    HTML += "<tr>";
                    HTML += @"<th width= ""25%"" style = ""border-bottom: 1px solid black;  padding: 0; margin:0;"" align = ""left"">BESKRIVELSE</th>";
                    HTML += @"<th width= ""10%"" style = ""border-bottom: 1px solid black;  padding: 0; margin:0;"">ANTALL</th>";
                    HTML += @"<th width= ""15%"" style = ""border-bottom: 1px solid black;  padding: 0; margin:0;"">PRIS</th>";
                    HTML += @"<th width= ""10%"" style = ""border-bottom: 1px solid black;  padding: 0; margin:0;"">MVA</th>";
                    HTML += @"<th width= ""15%"" style = ""border-bottom: 1px solid black;  padding: 0; margin:0;"">TILLEGG</th>";
                    HTML += @"<th width= ""15%"" style = ""border-bottom: 1px solid black;  padding: 0; margin:0;"">PRIS</th>";
                    HTML += @"<th width= ""10%"" style = ""border-bottom: 1px solid black;  padding: 0; margin:0;"">TOTAL</th>";
                    HTML += "</tr>";
                    for (int i = 0; i < numberOfOrderLines; i++)
                    {
                        HTML += "<tr>";
                        HTML += @"<td width= ""25%"" style= ""padding-left: 0cm"">{ArticleDescription" + i.ToString() + "} {PeriodSpan" + i.ToString() + "}</td>";
                        HTML += @"<td width= ""10%"" style = ""text-align: center"">{NumberOfUnits" + i.ToString() + "}</td>";
                        HTML += @"<td width= ""15%"" style = ""text-align: center"">{UnitPriceNoMVA" + i.ToString() + "}</td>";
                        HTML += @"<td width= ""10%"" style = ""text-align: center"">{MVASats" + i.ToString() + "}</td>";
                        HTML += @"<td width= ""15%"" style = ""text-align: center"">{MVAAmount" + i.ToString() + "}</td>";
                        HTML += @"<td width= ""15%"" style = ""text-align: center"">{UnitPrice" + i.ToString() + "}</td>";
                        HTML += @"<td width= ""10%"" style = ""text-align: center"">{TotalPrice" + i.ToString() + "}</td>";
                        HTML += "</tr>";
                        HTML += "{Note" + i.ToString() + "}";
                    }
                    HTML += "</table>";

                    HTML += "<br><span> </span><br>";
                    HTML += "<br><br><span> </span><br><br>";
                    HTML += "<br><br><span> </span><br><br>";
                    HTML += "<br><br><span> </span><br><br>";
                    HTML += "<br><br><span> </span><br><br>";

                    if (isAtg == true)
                    {
                        HTML += @"<br><br><br><br><span width= ""100"" align = ""left"" >Beløpet trekkes automatisk på konto den {PaidByDate}</span><br>";
                    }
                    else
                    {
                        HTML += @"<br><br><br><br><br>";
                    }

                    HTML += @"<span style= ""margin-left: 410px""></span> 
                            <span ><b style = ""border-top: 1px solid black"">SUM" + FillMe("{TotalInvoiceAmount}") + "<b></span>";

                    HTML += @"<hr style= ""border-top: 1px solid black"">";

                    HTML += @"<div style = ""line-height: 4px; text-align: center;"" ><p   style = ""font-size: 10px; >{SenderName} | {SenderAdress} | Telefon: {SenderPhoneNumber} | {SenderWebSite} <br>
                           Orgnr: {SenderOrganiztionNumber}</p></div>";

                    HTML += "</body></html> ";
                    break;


                case PdfTemplatesTypes.MEMBERCONTRACT_EXCELINE:


                    HTML = @"<DOCTYPE html><html><body>";

                    HTML += $"<img height= \"{Height}\" width= \"{Width}\" " + $@"src=D:\Exceline\Instances\Pilot2\Pilot2Web\images\GymLogo\{gymId}.png>";

                   
                    HTML += @"<span style=""margin left: 250px; margin top: 10px;"" align=""middle"">" + FillMe("Org.nr: {GymOrgNr}", 160 - (Convert.ToInt32(Width)/3)) + "</span> <br><br><br>";

                    HTML += @"<table style =""border-spacing: 0 ;border-collapse: collapse;"">";
                    HTML += "<tr>";
                    HTML += @"<td width=""21%"" style=""padding-left: 0cm; font-weight: bold;"">MEDLEMSKONTRAKT</td>";
                    HTML += @"<td width=""52%"" style=""padding-left: 0cm; font-weight: bold;"">: {GymName}</td>";
                    HTML += @"<td width=""11%"" style =""padding-right: 0cm; text-align: left;"">Tlf.Mobil</td>";
                    HTML += @"<td width=""17%"" style=""padding-left: 0cm; text-align: left;""> : {MobileNumber}</td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width=""21%"" style=""padding-left: 0cm; font-weight: bold;"">KONTRAKTNUMMER</td>";
                    HTML += @"<td width=""52%"" style=""padding-left: 0cm; font-weight: bold;"">: {MemberContractNo}</td>";
                    HTML += @"<td width=""11%"" style=""padding-right: 0cm; text-align: left;"">Tlf.Privat</td>";
                    HTML += @"<td width=""17%"" style=""padding-left: 0cm; text-align: left;""> : {PrivateNumber}</td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width=""21%"" style=""padding-left: 0cm; font-weight: bold;"">KUNDENUMMER</td>";
                    HTML += @"<td width=""52%"" style=""padding-left: 0cm; font-weight: bold;"">: {MemberNo}</td>";
                    HTML += @"<td width=""11%"" style=""padding-right: 0cm; text-align: left;"">Tlf.Arbeid</td>";
                    HTML += @"<td width=""17%"" style=""padding-left: 0cm; text-align: left;""> : {WorkNumber}</td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width=""21%"" style=""padding-left: 0cm; font-weight: bold;""></td>";
                    HTML += @"<td width=""52%"" style=""padding-left: 0cm; font-weight: bold;""></td>";
                    HTML += @"<td td width=""11%"" style=""padding-right: 0cm; text-align: left;"">Fødselsdato</td>";
                    HTML += @"<td td width=""17%"" style=""padding-left: 0cm; text-align: left;""> : {DateOfBirth}</td>";
                    HTML += "</tr>";
                    HTML += "</table><br><br>";

                    HTML += @"<table align=""left"" style =""border-spacing: 0 ;border-collapse: collapse;"">";
                    HTML += "<tr>";
                    HTML += @"<td width=""8%"" style=""padding-left: 0cm;"">Navn</td>";
                    HTML += @"<td width=""50%"" style=""padding-left: 0cm;"">: {FirstName} {LastName}</td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width=""8%"" style=""padding-left: 0cm;"">Adresse</td>";
                    HTML += @"<td width=""50%"" style=""padding-left: 0cm;"">: {Adress}</td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width=""8%"" style=""padding-left: 0cm;"">Postadr.</td>";
                    HTML += @"<td width=""50%"" style=""padding-left: 0cm;"">: {Zip} </td>";
                    HTML += "</tr></table><br>";

                    HTML += @"<table align=""left"" style =""border-spacing: 0 ;border-collapse: collapse;"">";
                    HTML += "<tr>";
                    HTML += @"<td width=""12%"" style=""padding-left: 0cm;"">Medlemskap</td>";
                    HTML += @"<td width=""60%"" style=""padding-left: 0cm;"">: {Template}</td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width=""12%"" style=""padding-left: 0cm;"">Bindingstid</td>";
                    HTML += @"<td width=""60%"" style=""padding-left: 0cm;"">: {TrainingPeriodeStart}{TrainingPeriodeStop}</td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width=""12%"" style=""padding-left: 0cm;"">Inngått</td>";
                    HTML += @"<td width=""60%"" style=""padding-left: 0cm;"">: {ContractSignDate}</td>";
                    HTML += "</tr></table><br>";

                    HTML += @"<table align=""left"" style =""border-spacing: 0 ;border-collapse: collapse;"">";
                    HTML += "<tr>";
                    HTML += @"<td width=""18%"" style=""padding-left: 0cm;"">Oppstartskostnader</td>";
                    HTML += @"<td width=""3%"" style=""padding-left: 0cm;"">: kr</td>";
                    HTML += @"<td width=""20%"" style=""padding-left: 0cm;""> {StartUpPrice}</td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width=""18%"" style=""padding-left: 0cm;"">Månedspris</td>";
                    HTML += @"<td width=""4%"" style=""padding-left: 0cm;"">: kr</td>";
                    HTML += @"<td width=""20%"" style=""padding-left: 0cm;""> {ServicePrice}</td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width=""18%"" style=""padding-left: 0cm;"">Tilleggstjenester</td>";
                    HTML += @"<td width=""3%"" style=""padding-left: 0cm;"">: kr</td>";
                    HTML += @"<td width=""20%"" style=""padding-left: 0cm;""> {AddonPrice}</td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width=""18%"" style=""padding-left: 0cm; font-weight: bold;"">Totalpris</td>";
                    HTML += @"<td width=""3%"" style=""padding-left: 0cm; font-weight: bold;"">: kr</td>";
                    HTML += @"<td width=""20%"" style=""padding-left: 0cm; font-weight: bold;""> {TotalPrice}</td>";
                    HTML += "</tr></table><br><br>";

                    if (numberOfOrderLines > 0) {

                        HTML += @"<table align=""center"" style =""border-spacing: 0 ; border:1px solid black;"">";
                        HTML += "<tr>";
                        HTML += @"<td width=""30%"" style=""padding-left: 0cm;border:1px solid black;text-align: center;"">Tjeneste</td>";
                        HTML += @"<td width=""10%"" style=""padding-left: 0cm;border:1px solid black;text-align: center;"">Pris</td>";
                        HTML += @"<td width=""10%"" style=""padding-left: 0cm;border:1px solid black;text-align: center;"">Antall</td>";
                        HTML += @"<td colspan = ""2""; width=""10%"" style=""padding-left: 0cm;border:1px solid black;text-align: center;"">Måneder</td>";
                        HTML += "</tr>";

                        for (int i = 0; i < numberOfOrderLines; i++)
                        {
                            HTML += "<tr>";
                            HTML += @"<td width= ""30%"" style= ""padding-left: 0cm;text-align: center;border:1px solid black;"">{ItemDescription" + i.ToString() + "}</td>";
                            HTML += @"<td width= ""10%"" style = ""text-align: center;border:1px solid black;"">{ItemPrice" + i.ToString() + "}</td>";
                            HTML += @"<td width= ""10%"" style = ""text-align: center;border:1px solid black;"">{NumberOfItems" + i.ToString() + "}</td>";
                            HTML += @"<td width= ""10%"" style = ""text-align: center;border:1px solid black;"">{StartOrder" + i.ToString() + "}</td>";
                            HTML += @"<td width= ""10%"" style = ""text-align: center;border:1px solid black;"">{EndOrder" + i.ToString() + "}</td>";
                            HTML += "</tr>";
                        }
                        HTML += "</table>";
                    }

                    HTML += "</body></html> ";
                    break;


                case PdfTemplatesTypes.INVOICE_SIO:

                    HTML = @" < DOCTYPE html><html><body>";

                    HTML += @"<img height=""50"" width= ""235"" src=""D:\tmp\SIO_LOGO.jpg""><br><br><br>";

                    HTML += @"<table style = ""border-spacing: 0 ;border-collapse: collapse; "" > ";

                    HTML += "<tr>";
                    HTML += @"<td width= ""56%"" style= ""padding-left: 0cm"">{PayerName}</td>";
                    HTML += @"<td width= ""15%"">Fakturanummer</td>";
                    HTML += @"<td style = ""margin-left: 12px"">: {InvoiceNumber}</td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width= ""56%"" style= ""padding-left: 0cm"">{PayerAdress}</td>";
                    HTML += @"<td width= ""15%"">Forfallsdato</td>";
                    HTML += @"<td style = ""margin-left: 12px"">: {PaidByDate}</td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width= ""56%"" style= ""padding-left: 0cm"">{PayerPostNumber}</td>";
                    HTML += @"<td width= ""15%"">Fakturadato</td>";
                    HTML += @"<td style = ""margin-left: 12px"">: {InvoiceDate}</td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width= ""56%"" style= ""padding-left: 0cm""></td>";
                    HTML += @"<td width= ""15%"">Kundenummer</td>";
                    HTML += @"<td style = ""margin-left: 12px"">: {CustomerId}</td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width= ""56%"" style= ""padding-left: 0cm""></td>";
                    HTML += @"<td width= ""15%"">TIL KONTO</td>";
                    HTML += @"<td style = ""margin-left: 12px"">: {ReceiverAccountNumber}</td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width= ""56%"" style= ""padding-left: 0cm""></td>";
                    HTML += @"<td width= ""15%"">KID</td>";
                    HTML += @"<td style = ""margin-left: 12px"">: {KID}</td>";
                    HTML += "</tr>";

                    HTML += "</table>";

                    HTML += "<br><span> </span><br>";
                    HTML += "<br><br><span> </span><br><br>";

                    HTML += @"<span style = ""margin-left: 190px""> </span> 
                            <b style= ""font-size: 20px"">F A K T U R A </b><br>";

                    HTML += @"<table style = ""border-spacing: 5"">";
                    HTML += "<tr>";
                    HTML += @"<th width= ""60%"" style = ""border-bottom: 1px solid black;  padding: 0; margin:0;"" align = ""left"">BESKRIVELSE</th>";
                    HTML += @"<th width= ""13%"" style = ""border-bottom: 1px solid black;  padding: 0; margin:0;"">ANTALL</th>";
                    HTML += @"<th width= ""13%"" style = ""border-bottom: 1px solid black;  padding: 0; margin:0;"">PRIS</th>";
                    HTML += @"<th width= ""13%"" style = ""border-bottom: 1px solid black;  padding: 0; margin:0;"">BELØP</th>";
                    HTML += "</tr>";
                    for (int i = 0; i < numberOfOrderLines; i++)
                    {
                        HTML += "<tr>";
                        HTML += @"<td width= ""60%"" style= ""padding-left: 0cm"">{ArticleDescription" + i.ToString() + "}</td>";
                        HTML += @"<td width= ""13%"" style = ""text-align: center"">{NumberOfUnits" + i.ToString() + "}</td>";
                        HTML += @"<td width= ""13%"" style = ""text-align: center"">{UnitPrice" + i.ToString() + "}</td>";
                        HTML += @"<td width= ""13%"" style = ""text-align: center"">{TotalPrice" + i.ToString() + "}</td>";
                        HTML += "</tr>";
                    }
                    HTML += "</table>";

                    HTML += "<br><span> </span><br>";
                    HTML += "<br><br><span> </span><br><br>";
                    HTML += "<br><br><span> </span><br><br>";
                    HTML += "<br><br><span> </span><br><br>";
                    HTML += "<br><br><span> </span><br><br>";

                    HTML += @"<br><br><br><br><span width= ""100"" align = ""left"" >Beløpet trekkes automatisk på konto den {PaidByDate}</span><br>";


                    HTML += @"<span style= ""margin-left: 410px""> </span> 
                            <span ><b style = ""border-top: 1px solid black"">SUM" + FillMe("{TotalInvoiceAmount}") + "<b></span>";

                    HTML += @"<hr style= ""border-top: 1px solid black"">";

                    HTML += @"<div style = ""line-height: 4px; text-align: center;"" ><p   style = ""font-size: 10px; >{SenderName} | {SenderAdress} | Telefon: {SenderPhoneNumber} | {SenderWebSite} <br>
                          SWIFT:{SenderSwift} | IBAN: {SenderIBAN} | Orgnr: {SenderOrganiztionNumber}</p></div>";

                    HTML += "</body></html> ";
                    break;

                case PdfTemplatesTypes.DAILYSETTLEMENT_EXCELINE:
                    HTML = @"<DOCTYPE html><html><body>";
               
                    HTML += $"<img height= \"{Height}\" width= \"{Width}\" " + $@"src=D:\Exceline\Instances\Pilot2\Pilot2Web\images\GymLogo\{gymId}.png>";

                    HTML += @"<span style=""margin left: 20px; margin top: 15px; font-weight: bold;"" align=""middle"">" + FillMe("{Mode}", 160 - (Convert.ToInt32(Width) / 3)) + " </span>";
                

                    HTML += @"<br><br><br><div style= ""text-align: center; font-weight: bold;"">Dagsoppgjør nr {DailySettlementID}</div>";
                    HTML += @"<div style= ""text-align: center; font-weight: bold;"">{GymName} - {OrgNo}</div><br><br><br>";
                    
                    HTML += @"<table style =""border-spacing: 0 ;border-collapse: collapse;"">";

                    HTML += "<tr>";
                    HTML += @"<td width= ""11%""></td>";
                    HTML += @"<td width= ""30%"">Fom:{FromDateTime}</td>";
                    HTML += @"<td width= ""15%""></td>";
                    HTML += @"<td width= ""8%""></td>";
                    HTML += @"<td width= ""32%"">Tom:{ToDateTime}</td>";
                    HTML += @"<td width= ""4%""></td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width= ""11%""></td>";
                    HTML += @"<td width= ""30%"">Kassepunkt ID:</td>";
                    HTML += @"<td width= ""15%"">{SalePointId}</td>";
                    HTML += @"<td width= ""8%""></td>";
                    HTML += @"<td width= ""32%""></td>";
                    HTML += @"<td width= ""4%""></td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width= ""11%""></td>";
                    HTML += @"<td width= ""30%"">Ansatt:</td>";
                    HTML += @"<td width= ""15%"">{CreatedUser}</td>";
                    HTML += @"<td width= ""8%""></td>";
                    HTML += @"<td width= ""32%""></td>";
                    HTML += @"<td width= ""4%""></td>";
                    HTML += "</tr>";

                    HTML += "</table><br><br><br>";

                    HTML += @"<table style =""border-spacing: 0 ;border-collapse: collapse;"">";

                    HTML += "<tr>";
                    HTML += @"<td width= ""11%"" style=""font-weight: bold;"" >Bruttosalg:</td>";
                    HTML += @"<td width= ""30%""></td>";
                    HTML += @"<td width= ""15%""></td>";
                    HTML += @"<td width= ""8%"">Antall</td>";
                    HTML += @"<td width= ""32%"">Sum</td>";
                    HTML += @"<td width= ""4%"">D/K</td>";
                    HTML += "</tr>";

                    HTML += "</table>";

                    HTML += @"<br><table style =""border-spacing: 0 ;border-collapse: collapse;"">";
                    HTML += "<AccountData>";

                    HTML += "<tr>";
                    HTML += @"<td width= ""11%""></td>";
                    HTML += @"<td width= ""30%"" style=""font-weight: bold;"">Sum bruttosalg:</td>";
                    HTML += @"<td width= ""15%""></td>";
                    HTML += @"<td width= ""8%""></td>";
                    HTML += @"<td width= ""32%"" style=""font-weight: bold;"">{TotalAccountSum}</td>";
                    HTML += @"<td width= ""4%"">K</td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width= ""11%""></td>";
                    HTML += @"<td width= ""30%"">Sum nettosalg:</td>";
                    HTML += @"<td width= ""15%""></td>";
                    HTML += @"<td width= ""8%""></td>";
                    HTML += @"<td width= ""32%"">{NettoAmount}</td>";
                    HTML += @"<td width= ""4%""></td>";
                    HTML += "</tr>";

                    HTML += "</table><br>";

                    HTML += @"<br><table style =""border-spacing: 0 ;border-collapse: collapse;"">";

                    HTML += "<tr>";
                    HTML += @"<td width= ""11%""></td>";
                    HTML += @"<td width= ""30%"" style = ""font-style: italic;"">Salgsfordeling pr. mva-sats:</td>";
                    HTML += @"<td width= ""15%""></td>";
                    HTML += @"<td width= ""8%""></td>";
                    HTML += @"<td width= ""32%""></td>";
                    HTML += @"<td width= ""4%""></td>";
                    HTML += "</tr>";

                    HTML += "<MVAData>";

                    HTML += "</table><br><br>";

                    HTML += @"<table style =""border-spacing: 0 ;border-collapse: collapse;"">";

                    HTML += "<tr>";
                    HTML += @"<td width= ""11%"" style=""font-weight: bold;"" >Betalingsform:</td>";
                    HTML += @"<td width= ""30%""></td>";
                    HTML += @"<td width= ""15%""></td>";
                    HTML += @"<td width= ""8%""></td>";
                    HTML += @"<td width= ""32%""></td>";
                    HTML += @"<td width= ""4%""></td>";
                    HTML += "</tr>";

                    HTML += "</table><br>";

                    HTML += @"<table style =""border-spacing: 0 ;border-collapse: collapse;"">";

                    HTML += "<PaymentTypeData>";

                    HTML += "</table><br><br>";

                    HTML += @"<table style =""border-spacing: 0 ;border-collapse: collapse;"">";

                    HTML += "<tr>";
                    HTML += @"<td width= ""11%"" style=""font-weight: bold;"">Kontantoppgjør:</td>";
                    HTML += @"<td width= ""30%""></td>";
                    HTML += @"<td width= ""15%""></td>";
                    HTML += @"<td width= ""8%""></td>";
                    HTML += @"<td width= ""32%""></td>";
                    HTML += @"<td width= ""4%""></td>";
                    HTML += "</tr>";

                    HTML += "</table><br>";

                    HTML += @"<table style =""border-spacing: 0 ;border-collapse: collapse;"">";

                    HTML += "<tr>";
                    HTML += @"<td width= ""11%""></td>";
                    HTML += @"<td width= ""30%"">Veksel:</td>";
                    HTML += @"<td width= ""15%""></td>";
                    HTML += @"<td width= ""8%""></td>";
                    HTML += @"<td width= ""32%"">{Exchange}</td>";
                    HTML += @"<td width= ""4%"">K</td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width= ""11%""></td>";
                    HTML += @"<td width= ""30%"">Innbetalt:</td>";
                    HTML += @"<td width= ""15%""></td>";
                    HTML += @"<td width= ""8%""></td>";
                    HTML += @"<td width= ""32%"">{CashIn}</td>";
                    HTML += @"<td width= ""4%"">D</td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width= ""11%""></td>";
                    HTML += @"<td width= ""30%"">Utbetalt:</td>";
                    HTML += @"<td width= ""15%""></td>";
                    HTML += @"<td width= ""8%""></td>";
                    HTML += @"<td width= ""32%"">{PaidOut}</td>";
                    HTML += @"<td width= ""4%"">K</td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width= ""11%""></td>";
                    HTML += @"<td width= ""30%"">Opptalt:</td>";
                    HTML += @"<td width= ""15%""></td>";
                    HTML += @"<td width= ""8%""></td>";
                    HTML += @"<td width= ""32%"">{CountedCash}</td>";
                    HTML += @"<td width= ""4%"">D</td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width= ""11%""></td>";
                    HTML += @"<td width= ""30%"">Avvik:</td>";
                    HTML += @"<td width= ""15%""></td>";
                    HTML += @"<td width= ""8%""></td>";
                    HTML += @"<td width= ""32%"">{Deviation}</td>";
                    HTML += @"<td width= ""4%"">D</td>";
                    HTML += "</tr>";

                    HTML += "</table><br><br>";

                    HTML += @"<table style =""border-spacing: 0 ;border-collapse: collapse;"">";

                    HTML += "<tr>";
                    HTML += @"<td width= ""11%"" style=""font-weight: bold;"">Gavekort:</td>";
                    HTML += @"<td width= ""30%""></td>";
                    HTML += @"<td width= ""15%""></td>";
                    HTML += @"<td width= ""8%""></td>";
                    HTML += @"<td width= ""32%""></td>";
                    HTML += @"<td width= ""4%""></td>";
                    HTML += "</tr>";

                    HTML += "</table><br>";

                    HTML += @"<table style =""border-spacing: 0 ;border-collapse: collapse;"">";

                    HTML += "<tr>";
                    HTML += @"<td width= ""11%""></td>";
                    HTML += @"<td width= ""30%"">Salg gavekort:</td>";
                    HTML += @"<td width= ""15%""></td>";
                    HTML += @"<td width= ""8%""></td>";
                    HTML += @"<td width= ""32%"">{GiftCard}</td>";
                    HTML += @"<td width= ""4%"">K</td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width= ""11%""></td>";
                    HTML += @"<td width= ""30%"">Benyttet gavekort:</td>";
                    HTML += @"<td width= ""15%""></td>";
                    HTML += @"<td width= ""8%""></td>";
                    HTML += @"<td width= ""32%"">{GiftCardTurnOver}</td>";
                    HTML += @"<td width= ""4%"">D</td>";
                    HTML += "</tr>";

                    HTML += "</table><br><br>";

                    HTML += @"<table style =""border-spacing: 0 ;border-collapse: collapse;"">";

                    HTML += "<tr>";
                    HTML += @"<td width= ""11%"" style=""font-weight: bold;"">Retur:</td>";
                    HTML += @"<td width= ""30%""></td>";
                    HTML += @"<td width= ""15%""></td>";
                    HTML += @"<td width= ""8%""></td>";
                    HTML += @"<td width= ""32%""></td>";
                    HTML += @"<td width= ""4%""></td>";
                    HTML += "</tr>";

                    HTML += "</table><br>";

                    HTML += @"<table style =""border-spacing: 0 ;border-collapse: collapse;"">";

                    HTML += "<tr>";
                    HTML += @"<td width= ""11%""></td>";
                    HTML += @"<td width= ""30%"">Retur til kunde:</td>";
                    HTML += @"<td width= ""15%""></td>";
                    HTML += @"<td width= ""8%""></td>";
                    HTML += @"<td width= ""32%"">{ShopReturn}</td>";
                    HTML += @"<td width= ""4%"">K</td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width= ""11%""></td>";
                    HTML += @"<td width= ""30%"">Kontantuttak:</td>";
                    HTML += @"<td width= ""15%""></td>";
                    HTML += @"<td width= ""8%""></td>";
                    HTML += @"<td width= ""32%"">{CashWithdrawal}</td>";
                    HTML += @"<td width= ""4%"">K</td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width= ""11%""></td>";
                    HTML += @"<td width= ""30%"" style=""font-weight: bold;"">Total retur:</td>";
                    HTML += @"<td width= ""15%""></td>";
                    HTML += @"<td width= ""8%""></td>";
                    HTML += @"<td width= ""32%"" style=""font-weight: bold;"">{TotReturns}</td>";
                    HTML += @"<td width= ""4%""  style=""font-weight: bold;"">K</td>";
                    HTML += "</tr>";

                    HTML += "</table><br><br>";

                    HTML += @"<table style =""border-spacing: 0 ;border-collapse: collapse;"">";

                    HTML += "<tr>";
                    HTML += @"<td width= ""11%"" style=""font-weight: bold;"">Totaler:</td>";
                    HTML += @"<td width= ""30%""></td>";
                    HTML += @"<td width= ""15%""></td>";
                    HTML += @"<td width= ""8%""></td>";
                    HTML += @"<td width= ""32%""></td>";
                    HTML += @"<td width= ""4%""></td>";
                    HTML += "</tr>";

                    HTML += "</table><br>";

                    HTML += @"<table style =""border-spacing: 0 ;border-collapse: collapse;"">";

                    HTML += "<tr>";
                    HTML += @"<td width= ""11%""></td>";
                    HTML += @"<td width= ""30%"">Totalt salg:</td>";
                    HTML += @"<td width= ""15%""></td>";
                    HTML += @"<td width= ""8%""></td>";
                    HTML += @"<td width= ""32%"">{TotalTurnOver}</td>";
                    HTML += @"<td width= ""4%""></td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width= ""11%""></td>";
                    HTML += @"<td width= ""30%"">Totalt retur:</td>";
                    HTML += @"<td width= ""15%""></td>";
                    HTML += @"<td width= ""8%""></td>";
                    HTML += @"<td width= ""32%"">{TotReturns}</td>";
                    HTML += @"<td width= ""4%""></td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width= ""11%""></td>";
                    HTML += @"<td width= ""30%"">Totalt netto:</td>";
                    HTML += @"<td width= ""15%""></td>";
                    HTML += @"<td width= ""8%""></td>";
                    HTML += @"<td width= ""32%"">{NettoAmount}</td>";
                    HTML += @"<td width= ""4%""></td>";
                    HTML += "</tr>";

                    HTML += "</table><br><br>";

                    HTML += @"<table style =""border-spacing: 0 ;border-collapse: collapse;"">";

                    HTML += "<tr>";
                    HTML += @"<td width= ""11%"" style=""font-weight: bold;"">Diverse:</td>";
                    HTML += @"<td width= ""30%""></td>";
                    HTML += @"<td width= ""15%""></td>";
                    HTML += @"<td width= ""8%""></td>";
                    HTML += @"<td width= ""32%""></td>";
                    HTML += @"<td width= ""4%""></td>";
                    HTML += "</tr>";

                    HTML += "</table><br>";

                    HTML += @"<table style =""border-spacing: 0 ;border-collapse: collapse;"">";

                    HTML += "<tr>";
                    HTML += @"<td width= ""11%""></td>";
                    HTML += @"<td width= ""30%"">Salgskvitteringer:</td>";
                    HTML += @"<td width= ""15%""></td>";
                    HTML += @"<td width= ""8%"">{NrReceipts}</td>";
                    HTML += @"<td width= ""32%""></td>";
                    HTML += @"<td width= ""4%""></td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width= ""11%""></td>";
                    HTML += @"<td width= ""30%"">Kopikvitteringer:</td>";
                    HTML += @"<td width= ""15%""></td>";
                    HTML += @"<td width= ""8%"">{NrCopys}</td>";
                    HTML += @"<td width= ""32%"">{AmountCopys}</td>";
                    HTML += @"<td width= ""4%""></td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width= ""11%""></td>";
                    HTML += @"<td width= ""30%"">Returkvitteringer:</td>";
                    HTML += @"<td width= ""15%""></td>";
                    HTML += @"<td width= ""8%"">{NrReturns}</td>";
                    HTML += @"<td width= ""32%"">{AmountReturns}</td>";
                    HTML += @"<td width= ""4%""></td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width= ""11%""></td>";
                    HTML += @"<td width= ""30%"">Rabatter:</td>";
                    HTML += @"<td width= ""15%""></td>";
                    HTML += @"<td width= ""8%"">{NumberOfDiscounts}</td>";
                    HTML += @"<td width= ""32%"">{DiscountAmount}</td>";
                    HTML += @"<td width= ""4%""></td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width= ""11%""></td>";
                    HTML += @"<td width= ""30%"">Åpning av kasseskuff:</td>";
                    HTML += @"<td width= ""15%""></td>";
                    HTML += @"<td width= ""8%"">{NumberOfOpenCashDrawer}</td>";
                    HTML += @"<td width= ""32%""></td>";
                    HTML += @"<td width= ""4%""></td>";
                    HTML += "</tr>";

                    HTML += "</table><br><br>";

                    HTML += @"<div style=""font-weight: bold;"">Kommentar til dagsoppgjør:</div><br>";
                    HTML += @"<div style =""width: 500px; Height:100px; border:1px solid black;""></div><br><br>";

                    HTML += @"<table style =""border-spacing: 0 ;border-collapse: collapse;"">";

                    HTML += "<tr>";
                    HTML += @"<td width= ""10%"" style=""font-weight: bold;"">Signatur:</td>";
                    HTML += @"<td width= ""30%"" style =""width: 250px; border-bottom:1px dotted black;"" ></td>";
                    HTML += "</tr>";

                    HTML += "</table>";


                    break;

                case PdfTemplatesTypes.SPONSOR_INVOICE_EXCELINE:

                    HTML = @"<DOCTYPE html><html><body>";

                    HTML += $"<img height= \"{Height}\" width= \"{Width}\" " + $@"src=D:\Exceline\Instances\Pilot2\Pilot2Web\images\GymLogo\{gymId}.png><br><br><br>";

                    HTML += @"<table style = ""border-spacing: 0 ;border-collapse: collapse; "" > ";

                    HTML += "<tr>";
                    HTML += @"<td width= ""56%"" style= ""padding-left: 0cm"">{FirstName} {LastName}</td>";
                    HTML += @"<td width= ""15%"">Fakturanummer</td>";
                    HTML += @"<td style = ""margin-left: 12px"">: {InvoiceNo}</td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width= ""56%"" style= ""padding-left: 0cm"">{Address}</td>";
                    HTML += @"<td width= ""15%"">Forfallsdato</td>";
                    HTML += @"<td style = ""margin-left: 12px"">: {DueDate}</td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width= ""56%"" style= ""padding-left: 0cm"">{PostalAddress}</td>";
                    HTML += @"<td width= ""15%"">Fakturadato</td>";
                    HTML += @"<td style = ""margin-left: 12px"">: {InvoiceGeneratedDate}</td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width= ""56%"" style= ""padding-left: 0cm""></td>";
                    HTML += @"<td width= ""15%"">Kundenummer</td>";
                    HTML += @"<td style = ""margin-left: 12px"">: {OriginalCustomer}</td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width= ""56%"" style= ""padding-left: 0cm""></td>";
                    HTML += @"<td width= ""15%"">TIL KONTO</td>";
                    HTML += @"<td style = ""margin-left: 12px"">: {GymAccountNo}</td>";
                    HTML += "</tr>";

                    HTML += "<tr>";
                    HTML += @"<td width= ""56%"" style= ""padding-left: 0cm""></td>";
                    HTML += @"<td width= ""15%"">KID</td>";
                    HTML += @"<td style = ""margin-left: 12px"">: {InvoiceID}</td>";
                    HTML += "</tr>";

                    HTML += "</table>";

                    HTML += "<br><span> </span><br>";
                    HTML += "<br><br><span> </span><br><br>";

                    HTML += @"<span style = ""margin-left: 190px""> </span> 
                            <b style= ""font-size: 20px"">F A K T U R A</b><br>";

                    HTML += @"<span style = ""margin-left: 178px""> </span> 
                            <b style= ""font-size: 12px"">S P O N S E G R U N N L A G</b><br>"; 


                   HTML += "{ReplaceMembers}";


                        HTML += @"<br><br><br><br><br>";
                    

                    HTML += @"<span style= ""margin-left: 410px""></span> 
                            <span ><b style = ""border-top: 1px solid black"">SUM" + FillMe("{InvoiceAmount}") + "<b></span>";

                    HTML += @"<hr style= ""border-top: 1px solid black"">";

                    HTML += @"<div style = ""line-height: 4px; text-align: center;"" ><p   style = ""font-size: 10px; >{GymName} | {GymAddress1} <br>
                           Orgnr: {GymOrganizationNo}</p></div>";

                    HTML += "</body></html> ";
                    break;
            }


        }
    }

}
