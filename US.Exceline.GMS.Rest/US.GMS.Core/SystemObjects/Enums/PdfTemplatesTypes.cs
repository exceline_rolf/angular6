﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.GMS.Core.SystemObjects.Enums
{
  
    public enum PdfTemplatesTypes
    {
        INVOICE_EXCELINE,
        MEMBERCONTRACT_EXCELINE,
        INVOICE_SIO,
        DAILYSETTLEMENT_EXCELINE,
        SPONSOR_INVOICE_EXCELINE
    }
}
