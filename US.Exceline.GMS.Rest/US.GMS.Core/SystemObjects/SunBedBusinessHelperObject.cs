﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.GMS.Core.SystemObjects
{
    public class SunBedBusinessHelperObject
    {
        private int articleNo;

        public int ArticleNo
        {
            get { return articleNo; }
            set { articleNo = value; }
        }
        private int memberId;

        public int MemberId
        {
            get { return memberId; }
            set { memberId = value; }
        }

        private int terminalId;

        public int TerminalID
        {
            get { return terminalId; }
            set { terminalId = value; }
        }

        private string articleName;

        public string ArticleName
        {
            get { return articleName; }
            set { articleName = value; }
        }
        private int articleCategoryId;

        public int ArticleCategoryId
        {
            get { return articleCategoryId; }
            set { articleCategoryId = value; }
        }
        private decimal amount;

        public decimal Amount
        {
            get { return amount; }
            set { amount = value; }
        }
        private string user;

        public string User
        {
            get { return user; }
            set { user = value; }
        }
        private string gymCode;

        public string GymCode
        {
            get { return gymCode; }
            set { gymCode = value; }
        }
        private int branchId;

        public int BranchId
        {
            get { return branchId; }
            set { branchId = value; }
        }

        private int memberBranchId;

        public int MemberBranchId
        {
            get { return memberBranchId; }
            set { memberBranchId = value; }
        }
        private int salepointId;

        public int SalepointId
        {
            get { return salepointId; }
            set { salepointId = value; }
        }
        private decimal accountBalance;

        public decimal AccountBalance
        {
            get { return accountBalance; }
            set { accountBalance = value; }
        }
        private int paidAccessMode;

        public int PaidAccessMode
        {
            get { return paidAccessMode; }
            set { paidAccessMode = value; }
        }

        private GatpurchaseShopItem gatPurchaseShopItem;

        public GatpurchaseShopItem GatPurchaseShopItem
        {
            get { return gatPurchaseShopItem; }
            set { gatPurchaseShopItem = value; }
        }

        private decimal onAccountNegativeBalance;

        public decimal OnAccountNegativeBalance
        {
            get { return onAccountNegativeBalance; }
            set { onAccountNegativeBalance = value; }
        }

        private GatPurchasePaymentStatus paymentOrderStatus;

        public GatPurchasePaymentStatus PaymentOrderStatus
        {
            get { return paymentOrderStatus; }
            set { paymentOrderStatus = value; }
        }

        private decimal _onAccountPaymentAmount;

        public decimal OnAccountPaymentAmount
        {
            get { return _onAccountPaymentAmount; }
            set { _onAccountPaymentAmount = value; }
        }

        private decimal _nextOrderPaymentAmount;

        public decimal NextOrderPaymentAmount
        {
            get { return _nextOrderPaymentAmount; }
            set { _nextOrderPaymentAmount = value; }
        }

        private bool _onlyShopOrder = false;

        public bool OnlyShopOrder
        {
            get { return _onlyShopOrder; }
            set { _onlyShopOrder = value; }
        }
    }
}
