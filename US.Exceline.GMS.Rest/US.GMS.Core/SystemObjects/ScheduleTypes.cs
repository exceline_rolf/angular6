﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.GMS.Core.SystemObjects
{
    public enum ScheduleTypes
    {
        NONE = 0,
        DAILY = 1,
        MONTHLY = 2,
        WEEKLY = 3,
        YEARLY = 4,
        FIX = 5
    }
}
