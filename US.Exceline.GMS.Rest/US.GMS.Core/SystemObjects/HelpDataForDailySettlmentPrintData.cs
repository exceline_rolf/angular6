﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace US.GMS.Core.SystemObjects
{
    [DataContract]
    public class HelpDataForDailySettlmentPrintData
    {
        private List<SalesPerAccountData> _perAccount = new List<SalesPerAccountData>();
        [DataMember]
        public List<SalesPerAccountData> PerAccount
        {
            get { return _perAccount; }
            set { _perAccount = value; }
        }

        private List<SalesPerMVACodeData> _perMVACode = new List<SalesPerMVACodeData>();
        [DataMember]
        public List<SalesPerMVACodeData> PerMVACode
        {
            get { return _perMVACode; }
            set { _perMVACode = value; }
        }

        private List<SalesPerPaymentTypeData> _perPaymentType = new List<SalesPerPaymentTypeData>();
        [DataMember]
        public List<SalesPerPaymentTypeData> PerPaymentType
        {
            get { return _perPaymentType; }
            set { _perPaymentType = value; }
        }

        private ReceiptsPerPeriodeForDailySettlement _receiptsForPeriode = new ReceiptsPerPeriodeForDailySettlement();
        [DataMember]
        public ReceiptsPerPeriodeForDailySettlement ReceiptsForPeriode
        {
            get { return _receiptsForPeriode; }
            set { _receiptsForPeriode = value; }
        }

        private double _nettoAmount = -1;
        [DataMember]
        public double NettoAmount
        {
            get { return _nettoAmount; }
            set { _nettoAmount = value; }
        }

        private double _discountedAmount = -1;
        [DataMember]
        public double DiscountedAmount
        {
            get { return _discountedAmount; }
            set { _discountedAmount = value; }
        }

        private int _numberOfDiscounts = -1;
        [DataMember]
        public int NumberOfDiscounts
        {
            get { return _numberOfDiscounts; }
            set { _numberOfDiscounts = value; }
        }


        private int _numberOfOpenCashDrawer = -1;
        [DataMember]
        public int NumberOfOpenCashDrawer
        {
            get { return _numberOfOpenCashDrawer; }
            set { _numberOfOpenCashDrawer = value; }
        }
    }
}
