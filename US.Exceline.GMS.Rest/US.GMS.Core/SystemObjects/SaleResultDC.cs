﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.SystemObjects
{
    [DataContract]
    public class SaleResultDC
    {
        private int _aritemNo = -1;
        [DataMember]
        public int AritemNo
        {
            get { return _aritemNo; }
            set { _aritemNo = value; }
        }

        private string _invoiceNo = string.Empty;
         [DataMember]
        public string InvoiceNo
        {
            get { return _invoiceNo; }
            set { _invoiceNo = value; }
        }

         private DateTime? _dueDate;
          [DataMember]
         public DateTime? DueDate
         {
             get { return _dueDate; }
             set { _dueDate = value; }
         }


        private SaleErrors _saleStatus = SaleErrors.NONE;
        [DataMember]
        public SaleErrors SaleStatus
        {
            get { return _saleStatus; }
            set { _saleStatus = value; }
        }

        private PDFPrintResultDC _printResult = new PDFPrintResultDC();
         [DataMember]
        public PDFPrintResultDC PrintResult
        {
            get { return _printResult; }
            set { _printResult = value; }
        }

        private int _memberId = -1;
        [DataMember]
        public int MemberId
        {
            get { return _memberId; }
            set { _memberId = value; }
        }

        private string _custId = string.Empty;
        [DataMember]
        public string CustId
        {
            get { return _custId; }
            set { _custId = value; }
        }

        private int _nextOrderId;
        [DataMember]
        public int NextOrderId
        {
            get { return _nextOrderId; }
            set { _nextOrderId = value; }
        }

        private int _inkassoID = -1;
        [DataMember]
        public int InkassoID
        {
            get { return _inkassoID; }
            set { _inkassoID = value; }
        }

        private string _kid = string.Empty;
        [DataMember]
        public string KID
        {
            get { return _kid; }
            set { _kid = value; }
        }

        private bool _recover = false;
        [DataMember]
        public bool Recover
        {
            get { return _recover; }
            set { _recover = value; }
        }

        private int _summaryID = -1;
        [DataMember]
        public int SummaryID
        {
            get { return _summaryID; }
            set { _summaryID = value; }
        }

        private int _installmentID = -1;
        [DataMember]
        public int InstallmentID
        {
            get { return _installmentID; }
            set { _installmentID = value; }
        }



    }
}
