﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.SystemObjects
{
    [DataContract]
    public class BranchDetailsDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _gymDetailId = -1;
        [DataMember]
        public int GymDetailId
        {
            get { return _gymDetailId; }
            set { _gymDetailId = value; }
        }

        private int _creditorNo = -1;
        [DataMember]
        public int CreditorNo
        {
            get { return _creditorNo; }
            set { _creditorNo = value; }
        }

        private string _branchName = string.Empty;
        [DataMember]
        public string BranchName
        {
            get { return _branchName; }
            set { _branchName = value; }
        }

        private int _branchId = -1;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private string _gymCode = string.Empty;
        [DataMember]
        public string GymCode
        {
            get { return _gymCode; }
            set { _gymCode = value; }
        }

        private bool _invoiceProcessEnabled;
         [DataMember]
        public bool InvoiceProcessEnabled
        {
            get { return _invoiceProcessEnabled; }
            set { _invoiceProcessEnabled = value; }
        }

        private bool _isSelected = false;
        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; }
        }

      
    }
}
