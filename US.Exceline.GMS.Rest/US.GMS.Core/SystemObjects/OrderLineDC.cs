﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.GMS.Core.SystemObjects
{
    [DataContract]
    public class OrderLineDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _articleId = -1;
        [DataMember]
        public int ArticleId
        {
            get { return _articleId; }
            set { _articleId = value; }
        }

        private string _name = string.Empty;
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private decimal _unitPrice = 0;
        [DataMember]
        public decimal UnitPrice
        {
            get { return _unitPrice; }
            set { _unitPrice = value; }
        }

        private int _quantity = 0;
        [DataMember]
        public int Quantity
        {
            get { return _quantity; }
            set { _quantity = value; }
        }

        private decimal _price = 0;
        [DataMember]
        public decimal Price
        {
            get { return _price; }
            set { _price = value; }
        }

        private decimal _discount = 0;
        [DataMember]
        public decimal Discount
        {
            get { return _discount; }
            set { _discount = value; }
        }


        private bool _isDeleted = false;
        [DataMember]
        public bool IsDeleted
        {
            get { return _isDeleted; }
            set { _isDeleted = value; }
        }


        private decimal _creditNoteAmount = 0;
        [DataMember]
        public decimal CreditNoteAmount
        {
            get { return _creditNoteAmount; }
            set { _creditNoteAmount = value; }
        }
      
        private int _arItemNo = -1;
        [DataMember]
        public int ArItemNo
        {
            get { return _arItemNo; }
            set { _arItemNo = value; }
        }


        private int _cusId = -1;
        [DataMember]
        public int CusId
        {
            get { return _cusId; }
            set { _cusId = value; }
        }
        
    }
}
