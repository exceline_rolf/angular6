﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.GMS.Core.SystemObjects
{
    [DataContract]
    public class ExcelineInvoiceDetailDC
    {
        private int _arItemNo = -1;
        [DataMember]
        public int ArItemNO
        {
            get { return _arItemNo; }
            set { _arItemNo = value; }
        }

        private int _subCaseNo = -1;
         [DataMember]
        public int SubCaseNo
        {
            get { return _subCaseNo; }
            set { _subCaseNo = value; }
        }

        private int _custId = -1;
        [DataMember]
        public int CustId
        {
            get { return _custId; }
            set { _custId = value; }
        }

        private int _creditorNo = -1;
        [DataMember]
        public int CreditorNo
        {
            get { return _creditorNo; }
            set { _creditorNo = value; }
        }

        private string _customerName = string.Empty;
        [DataMember]
        public string CustomerName
        {
            get { return _customerName; }
            set { _customerName = value; }
        }

        private DateTime? _invoiceDueDate;
        [DataMember]
        public DateTime? InvoiceDueDate
        {
            get { return _invoiceDueDate; }
            set { _invoiceDueDate = value; }
        }

        private decimal _invoiceAmount = 0;
        [DataMember]
        public decimal InvoiceAmount
        {
            get { return _invoiceAmount; }
            set { _invoiceAmount = value; }
        }

        private string _contractNo = string.Empty;
        [DataMember]
        public string ContractNo
        {
            get { return _contractNo; }
            set { _contractNo = value; }
        }

        private string _orderNo = string.Empty;
        [DataMember]
        public string OrderNo
        {
            get { return _orderNo; }
            set { _orderNo = value; }
        }

        private int _invoiceId = 0;
        [DataMember]
        public int InvoiceId
        {
            get { return _invoiceId; }
            set { _invoiceId = value; }
        }

        private string _invoiceNo = string.Empty;
        [DataMember]
        public string InvoiceNo
        {
            get { return _invoiceNo; }
            set { _invoiceNo = value; }
        }

        private decimal _invoiceBalance = 0;
        [DataMember]
        public decimal InvoiceBalance
        {
            get { return _invoiceBalance; }
            set { _invoiceBalance = value; }
        }

        private int _invoiceType = -1;
        [DataMember]
        public int InvoiceType
        {
            get { return _invoiceType; }
            set { _invoiceType = value; }
        }

        private string _invoiceStatus = string.Empty;
        [DataMember]
        public string InvoiceStatus
        {
            get { return _invoiceStatus; }
            set { _invoiceStatus = value; }
        }

        private string _debtCollectionStatus = string.Empty;
        [DataMember]
        public string DebtCollectionStatus
        {
            get { return _debtCollectionStatus; }
            set { _debtCollectionStatus = value; }
        }

        private InvoiceBasicDetailsDC _basicDetails;
        [DataMember]
        public InvoiceBasicDetailsDC BasicDetails
        {
            get { return _basicDetails; }
            set { _basicDetails = value; }
        }

        private InvoiceOrderDetailsDC _otherDetails;
        [DataMember]
        public InvoiceOrderDetailsDC OtherDetails
        {
            get { return _otherDetails; }
            set { _otherDetails = value; }
        }

        private bool _isSponsorInvoice = false;
        [DataMember]
        public bool IsSponsorInvoice
        {
            get { return _isSponsorInvoice; }
            set { _isSponsorInvoice = value; }
        }

        private bool _transfered = false;
        [DataMember]
        public bool Transfered
        {
            get { return _transfered; }
            set { _transfered = value; }
        }

        private int _gymID = -1;
        [DataMember]
        public int GymID
        {
            get { return _gymID; }
            set { _gymID = value; }
        }

        private DateTime? _debtSentCollectionDate;
        [DataMember]
        public DateTime? DebtSentCollectionDate
        {
            get { return _debtSentCollectionDate; }
            set { _debtSentCollectionDate = value; }
        }
    }
}
