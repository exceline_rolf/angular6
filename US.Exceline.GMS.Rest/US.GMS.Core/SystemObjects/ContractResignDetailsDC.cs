﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.DomainObjects.Notification;

namespace US.GMS.Core.SystemObjects
{
    [DataContract]
    public class ContractResignDetailsDC
    {
        private int _memberContractId = -1;
        [DataMember]
        public int MemberContractId
        {
            get { return _memberContractId; }
            set { _memberContractId = value; }
        }

        private string _memberContractNo= string.Empty;
        [DataMember]
        public string MemberContractNo
        {
            get { return _memberContractNo; }
            set { _memberContractNo = value; }
        }


        private int _pendingOrders = -1;
        [DataMember]
        public int PendingOrders
        {
            get { return _pendingOrders; }
            set { _pendingOrders = value; }
        }


        private int _memberId = -1;
        [DataMember]
        public int MemberId
        {
            get { return _memberId; }
            set { _memberId = value; }
        }

        private decimal _amount = 0.00M;
        [DataMember]
        public decimal Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }


        private NotifyMethodType _notifyMethod = NotifyMethodType.NONE;
        [DataMember]
        public NotifyMethodType NotifyMethod
        {
            get { return _notifyMethod; }
            set { _notifyMethod = value; }
        }



         private DateTime? _resignDate;
         [DataMember]
         public DateTime? ResignDate
         {
             get { return _resignDate; }
             set { _resignDate = value; }
         }

         private DateTime? _contractEndDate;
         [DataMember]
         public DateTime? ContractEndDate
         {
             get { return _contractEndDate; }
             set { _contractEndDate = value; }
         }

        private int _lockInPeriod = 0;
        [DataMember]
        public int LockInPeriod
        {
            get { return _lockInPeriod; }
            set { _lockInPeriod = value; }
        }

        private DateTime? _lockUntillDate;
        [DataMember]
        public DateTime? LockUntillDate
        {
            get { return _lockUntillDate; }
            set { _lockUntillDate = value; }
        }

        private int _resignCategoryId = -1;
        [DataMember]
        public int ResignCategoryId
        {
            get { return _resignCategoryId; }
            set { _resignCategoryId = value; }
        }

        private List<int> _deletedOrders = new List<int>();
        [DataMember]
        public List<int> DeletedOrders
        {
            get { return _deletedOrders; }
            set { _deletedOrders = value; }
        }

        private List<InstallmentDC> _updatedInstallment = new List<InstallmentDC>();
        [DataMember]
        public List<InstallmentDC> UpdatedInstallment
        {
            get { return _updatedInstallment; }
            set { _updatedInstallment = value; }
        }

        private List<InstallmentDC> _remainingInstallments = new List<InstallmentDC>();
        [DataMember]
        public List<InstallmentDC> RemainingInstallments
        {
            get { return _remainingInstallments; }
            set { _remainingInstallments = value; }
        }

        private List<int> _mergedOrders = new List<int>();
        [DataMember]
        public List<int> MergedOrders
        {
            get { return _mergedOrders; }
            set { _mergedOrders = value; }
        }

        private int _memberFeeArticleID = -1;
        [DataMember]
        public int MemberFeeArticleID
        {
            get { return _memberFeeArticleID; }
            set { _memberFeeArticleID = value; }
        }

    }
}
