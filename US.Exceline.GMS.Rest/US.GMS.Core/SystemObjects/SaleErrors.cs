﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.GMS.Core.SystemObjects
{
    public enum SaleErrors
    {
        NONE,
        NOORDERFOUND, 
        NOSTOCK,
        ERROR,
        SUCCESS,
        INVOICEADDINGERROR, 
        PAYMENTADDINGERROR,
        ORDERLINEADDERROR,
        PAYMENTNOTFOUND,
        RETRY,
        DUPLICATEINVOICE,
        NORETURN,
        ADDCREDITNOTEERROR
    }
}
