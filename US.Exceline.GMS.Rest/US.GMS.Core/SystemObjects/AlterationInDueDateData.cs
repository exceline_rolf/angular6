﻿using System;
using System.Runtime.Serialization;

namespace US.GMS.Core.SystemObjects
{
    [DataContract]
    public class AlterationInDueDateData
    {

        private string _title = string.Empty;
        [DataMember]
        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        private string _description = string.Empty;
        [DataMember]
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private string _roleId = string.Empty;
        [DataMember]
        public string RoleId
        {
            get { return _roleId; }
            set { _roleId = value; }
        }

        private int _typeId = -1;
        [DataMember]
        public int TypeId
        {
            get { return _typeId; }
            set { _typeId = value; }
        }

        private int _severityId = -1;
        [DataMember]
        public int SeverityId
        {
            get { return _severityId; }
            set { _severityId = value; }
        }

        private int _statusId = -1;
        [DataMember]
        public int StatusId
        {
            get { return _statusId; }
            set { _statusId = value; }
        }

        private int _branchId = -1;
        [DataMember]
        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        private int _memberId = -1;
        [DataMember]
        public int MemberId
        {
            get { return _memberId; }
            set { _memberId = value; }
        }

        private int _isReplySelected = -1;
        [DataMember]
        public int IsReplySelected
        {
            get { return _isReplySelected; }
            set { _isReplySelected = value; }
        }

        private string _recordType = string.Empty;
        [DataMember]
        public string RecordType
        {
            get { return _recordType; }
            set { _recordType = value; }
        }

        private DateTime _lastAttendDate = DateTime.Now;
        [DataMember]
        public DateTime LastAttendDate
        {
            get { return _lastAttendDate; }
            set { _lastAttendDate = value; }
        }


    }
}
