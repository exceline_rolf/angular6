﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.GMS.Core.SystemObjects.UserOperations
{
    public class USPFeatureDC
    {
        public string ID { get; set; }
        public int FeatureId { get; set; }
        public string DisplayName { get; set; }
        public string HomeUIControl { get; set; }
        public string ThumbnailImage { get; set; }
        public string FeatureColor { get; set; }
        public int Priority { get; set; }
        private List<OperationDC> _operations = new List<OperationDC>();
        public List<OperationDC> Operations { get; set; }
        public string FeaturePath { get; set; }
        public string CulturalDisplayName { get; set; }
        public string FeatureHomeURL { get; set; }
    }
}
