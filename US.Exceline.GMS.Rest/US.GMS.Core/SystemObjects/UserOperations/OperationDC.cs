﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Common.Web.UI.Core.SystemObjects;

namespace US.GMS.Core.SystemObjects.UserOperations
{
    public class OperationDC
    {
        public string OperationHome { get; set; }
        public string ID { get; set; }
        public int OperationId { get; set; }
        public string DisplayName { get; set; }
        public string Group { get; set; }
        public string ThumbnailImage { get; set; }
        public string OperationColor { get; set; }
        public string Mode { get; set; }
        public int IsAddedToWorkStation { get; set; }
        public int RoleId { get; set; }
        public string OperationNameSpace { get; set; }
        public string OperationPath { get; set; }
        public OperationExtendedInfo ExtendedInfo { get; set; }
        public List<SubOperationDC> SubOperationList { get; set; }
        public string OperationPackage { get; set; }
        public string CulturalDisplayName { get; set; }
        public string OperationHomeURL { get; set; }
    }
}
