﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Common.Web.UI.Core.SystemObjects;
using US.GMS.Core.SystemObjects.UserOperations;

namespace US.GMS.Core.SystemObjects.USerOperations
{
    public class USPUserDC
    {
        private List<USPModuleDC> _moduleList = new List<USPModuleDC>();

        private List<USPModuleDC> _ModuleList = new List<USPModuleDC>();
        public List<USPModuleDC> ModuleList
        {
            get { return _moduleList; }
            set { _moduleList = value; }
        }
        public int UserID { get; set; }
        public string UserName { get; set; }
        public string DisplayName { get; set; }
        public string ExternalUserName { get; set; }
        public string RoleId { get; set; }
        public string HomePage { get; set; }
        public List<int> Branches { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string OldPassword { get; set; }
        public int LockTime { get; set; }
        public bool IsLoginFirstTime { get; set; }

        private List<USPSubOperation> _subOperationList = new List<USPSubOperation>();
        public List<USPSubOperation> SubOperationList
        {
            get { return _subOperationList; }
            set { _subOperationList = value; }
        }
        public bool IsActive { get; set; }
        public string Culture { get; set; }
        public string CulturalDisplayName { get; set; }
      
        public bool isPasswdChange { get; set; }
        public string loggedUser { get; set; }

    }
}
