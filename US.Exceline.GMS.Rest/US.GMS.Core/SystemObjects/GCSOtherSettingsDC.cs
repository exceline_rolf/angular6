﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.Admin.ManageClassSettings;

namespace US.GMS.Core.SystemObjects
{
    [DataContract]
    public class GCSOtherSettingsDC
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _registerUser = string.Empty;
        [DataMember]
        public string RegisteredUser
        {
            get { return _registerUser; }
            set { _registerUser = value; }
        }

        private DateTime _registeredDate;
        [DataMember]
        public DateTime RegisteredDate
        {
            get { return _registeredDate; }
            set { _registeredDate = value; }
        }
    }
}
