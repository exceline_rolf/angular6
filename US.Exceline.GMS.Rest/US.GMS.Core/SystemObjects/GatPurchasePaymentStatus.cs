﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.GMS.Core.SystemObjects
{
    public enum GatPurchasePaymentStatus
    {
        ONACCOUNTANDNEXTORDER,
        ONACCOUNTONLY,
        NEXTORDERONLY
    }
}
