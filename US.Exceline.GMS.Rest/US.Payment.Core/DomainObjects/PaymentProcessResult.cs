﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.DomainObjects
{
    public class PaymentProcessResult
    {
        private bool _totalProcessResult = false;

        public bool ResultStatus
        {
            get { return _totalProcessResult; }
            set { _totalProcessResult = value; }
        }
        private List<PaymentProcessStepResult> _processStepResultList = new List<PaymentProcessStepResult>();

        public List<PaymentProcessStepResult> ProcessStepResultList
        {
            get { return _processStepResultList; }
            set { _processStepResultList = value; }
        }
    }
}
