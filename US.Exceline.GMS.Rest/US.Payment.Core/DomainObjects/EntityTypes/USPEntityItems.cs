﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.DomainObjects.EntityTypes
{
    public class USPEntityItems
    {
        private List<USPEntityItem> _entityItems = new List<USPEntityItem>();
        public List<USPEntityItem> EntityItems
        {
            get { return _entityItems; }
            set { _entityItems = value; }
        }
    }
}
