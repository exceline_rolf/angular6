﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.DomainObjects
{
    public interface IUSPEntityHistory
    {
        int AddressNo{ get; set; }
        string Address1{ get; set; }
       string Address2{ get; set; }
        string Address3{ get; set; }
        string ZipCode{ get; set; }
        string ZipName{ get; set; }
        string CountryID{ get; set; }
        string AddressSource{ get; set; }
        string TelMobile{ get; set; }
        string TelWork{ get; set; }
       string TelHome{ get; set; }
       string Email{ get; set; }
       string Fax{ get; set; }
       string Msn{ get; set; }
       string Skype{ get; set; }
        string Sms{ get; set; }
       int IsDefualt{ get; set; }
        int EntNo{ get; set; }
        string UserId{ get; set; }
        string Type { get; set; }
       
    }
}
