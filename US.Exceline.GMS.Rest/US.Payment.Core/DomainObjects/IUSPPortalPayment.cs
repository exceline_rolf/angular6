﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.DomainObjects
{
   public interface IUSPPortalPayment
    {
        string PaymentRef{ get; set; }
        string PaidDate{ get; set; }
        double PaymentAmount{ get; set; }
        double PaymentBalance{ get; set; }
        string KID { get; set; }
    }
}
