﻿using System.Runtime.Serialization;
using System.Runtime.Serialization;

namespace US.Payment.Core.DomainObjects.RutineAndActivities
{
    [DataContract]
    public class USPActivity
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        private string _name = string.Empty;
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        private string _category = string.Empty;
        [DataMember]
        public string Category
        {
            get { return _category; }
            set { _category = value; }
        }
        private string _type = string.Empty;
        [DataMember]
        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }
        private string _settings = string.Empty;
        [DataMember]
        public string Settings
        {
            get { return _settings; }
            set { _settings = value; }
        }
        private string _categoryName = string.Empty;
        [DataMember]
        public string CategoryName
        {
            get { return _categoryName; }
            set { _categoryName = value; }
        }

        private string _workFlowType = string.Empty;
        [DataMember]
        public string WorkFlowType
        {
            get { return _workFlowType; }
            set { _workFlowType = value; }
        }

        private bool _isSelected = false;
        [DataMember]
        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; }
        }
        private bool _status = false;
        [DataMember]
        public bool Status
        {
            get { return _status; }
            set { _status = value; }
        }

        private bool _isShow = false;
        [DataMember]
        public bool IsShow
        {
            get { return _isShow; }
            set { _isShow = value; }
        }
    }
}
