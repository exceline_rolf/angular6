﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.Payment.Core.DomainObjects
{
    [DataContract]
    public class TransactionProfile
    {
        private int _id = -1;
        private string _name = string.Empty;
        private string _displayName = string.Empty;
        private string _findDestinationSp = string.Empty;
        private string _saveTransactionSp = string.Empty;
        private string _distributeTransactionSp = string.Empty;
        private string _userName = string.Empty;
        private string _modifiedUser = string.Empty;
        private DateTime _regDate = DateTime.Now;
        private DateTime _modifiedDate = DateTime.Now;

        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
       
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        
        [DataMember]
        public string DisplayName
        {
            get { return _displayName; }
            set { _displayName = value; }
        }
      
        [DataMember]
        public string FindDestinationSp
        {
            get { return _findDestinationSp; }
            set { _findDestinationSp = value; }
        }
        
        [DataMember]
        public string SaveTransactionSp
        {
            get { return _saveTransactionSp; }
            set { _saveTransactionSp = value; }
        }
        
        [DataMember]
        public string DistributeTransactionSp
        {
            get { return _distributeTransactionSp; }
            set { _distributeTransactionSp = value; }
        }

        [DataMember]
        public DateTime ModifiedDate
        {
            get { return _modifiedDate; }
            set { _modifiedDate = value; }
        }

        [DataMember]
        public DateTime RegDate
        {
            get { return _regDate; }
            set { _regDate = value; }
        }

        [DataMember]
        public string ModifiedUser
        {
            get { return _modifiedUser; }
            set { _modifiedUser = value; }
        }

        [DataMember]
        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }
    }
}
