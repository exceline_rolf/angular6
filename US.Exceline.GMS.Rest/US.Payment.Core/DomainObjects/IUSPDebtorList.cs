﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.DomainObjects
{
   public interface IUSPDebtorList
    {
       string EntId{ get; set; }
       string Name{ get; set; }
       string BirthDay{ get; set; }
       string PersonNo{ get; set; }
       int ARNo { get; set; }
       
    }
}
