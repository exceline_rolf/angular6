﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.DomainObjects
{
    public interface IUSPNewOrChanged
    {
        string FBOCounter{ get; set; }
        string RegisterType{ get; set; }
        string KID{ get; set; }
        string Warning { get; set; }
       
    }
}
