﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.DomainObjects
{
   public interface IUSPPaymentReceipt
    {
        string KID{ get; set; }
        bool ErrorOccured{ get; set; }
        string ErrorMessage{ get; set; }
        string Ref { get; set; }
    }
}
