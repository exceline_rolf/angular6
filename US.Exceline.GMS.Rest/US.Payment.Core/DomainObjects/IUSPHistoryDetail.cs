﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.DomainObjects
{
    public interface IUSPHistoryDetail
    {
        int Token{ get; set; }
        int PrincipleId{ get; set; }
        string PrincipleType{ get; set; }
        string LineType{ get; set; }
        int LineNumber { get; set; }
        
    }
}
