﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.DomainObjects
{
    public interface IUSPItemType
    {
         string TransCode{ get; set; }
        string Description{ get; set; }
        string ID { get; set; }
        
    }
}
