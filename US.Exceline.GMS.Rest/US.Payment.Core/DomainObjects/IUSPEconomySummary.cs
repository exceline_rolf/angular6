﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.DomainObjects
{
   public  interface IUSPEconomySummary
    {
       int OCRCount{ get; set; }
       int Invoices{ get; set; }
       int DirectPayment{ get; set; }
       decimal OCRAmount{ get; set; }
       decimal InvoiceAmount{ get; set; }
       decimal DirectPaymentAmount{ get; set; }
       int UnmappedOCRCount{ get; set; }
       int UnmappedDirectPayment{ get; set; }
       decimal UnmappedOCRAmount{ get; set; }
       decimal UnmappedDirectPaymentAmount { get; set; }
        
    }
}
