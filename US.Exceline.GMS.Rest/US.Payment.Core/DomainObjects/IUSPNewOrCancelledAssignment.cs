﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.DomainObjects
{
    public interface IUSPNewOrCancelledAssignment
    {
       int ID{ get; set; }
       string InkassoID{ get; set; }
       string BNumber{ get; set; }
       string CID{ get; set; }
       string KID{ get; set; }
       string Status{ get; set; }
       string Date{ get; set; }
       string Row { get; set; }
        
    }
}
