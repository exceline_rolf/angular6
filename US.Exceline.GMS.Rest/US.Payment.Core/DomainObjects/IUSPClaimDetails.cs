﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.DomainObjects
{
   public interface IUSPClaimDetails
    {
       
         int ARItemNo { get; set; }
       
         string ItemType { get; set; }
       
         int ItemTypeID { get; set; }
       
         string InkassoID { get; set; }
       
         string CustomerID { get; set; }
       
        string Name { get; set; }
       
        string NameOnContract { get; set; }
       
        string Address { get; set; }
       
        string ZipCode { get; set; }
       
        string PostPlace { get; set; }
       
        string Born { get; set; }
       
        string TelephonePrivate { get; set; }
       
        string TelephoneWork { get; set; }
       
        string TelephoneMobile { get; set; }
       
        string Email { get; set; }
       
        string CreditorName { get; set; }
       
        string CreditorAddress { get; set; }
       
        string CreditorZipCode { get; set; }
       
        string CreditorPostPlace { get; set; }
       
        string CreditorBorn { get; set; }
       
        string CreditorTelephonePrivate { get; set; }
       
        string CreditorTelephoneWork { get; set; }
       
        string CreditorTelephoneMobile { get; set; }
       
        string CreditorEmail { get; set; }
       
        string OrganizationNo { get; set; }
       
        string AccountNumber { get; set; }
       
        string CreditorAccountNumber { get; set; }
       
        string ContractNumber { get; set; }
       
        string ContractExpire { get; set; }
       
        string LastVisit { get; set; }
       
        string LastContact { get; set; }
       
        string ContractKID { get; set; }
       
        string StatusMandatory { get; set; }
       
        string InstalmentNumber { get; set; }
       
        string InvoiceNumber { get; set; }
       
        string TransactionText { get; set; }
       
        string InvoiceDate { get; set; }
       
        string DueDate { get; set; }
       
        string PaidDate { get; set; }
       
        string AIDPrintdate { get; set; }
       
        string OriginalDueDate { get; set; }
       
        string Amount { get; set; }
       
        string InvoiceCharge { get; set; }
       
        string ReminderFee { get; set; }
       
        string InstallmentKID { get; set; }
       
        string TransmissionNumberBBS { get; set; }
       
        string Balance { get; set; }
       
        string DueBalance { get; set; }
       
        string LastReminder { get; set; }
       
        string collectingStatus { get; set; }
       
        string CollectingText { get; set; }
       
        string Inkassotype { get; set; }
       
        string BranchNumber { get; set; }
       
        bool IsErrorOcurred { get; set; }
       
        string ErrorMessage { get; set; }
       
        List<IUSPOrderLine> OrderLineList { get; set; }
       
        string KID { get; set; }

    }
}
