﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.DomainObjects
{
    public interface IUSPNote
    {
        string DelayType{ get; set; }
        string Comment { get; set; }
        
    }
}
