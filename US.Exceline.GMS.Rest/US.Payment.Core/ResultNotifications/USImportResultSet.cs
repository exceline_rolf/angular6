﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace US.Payment.Core.ResultNotifications
{
    public class USImportResultSet<T> : USImportResultSet
    {
        public USImportResultSet()
        {
            _importResults = new List<USImportResult<T>>();
            _containErrors = false;
        }
        public void AddImportResult(USImportResult<T> importResult)
        {
            if (importResult.ErrorOccured)
                _containErrors = true;
            _importResults.Add(importResult);
        }
        private List<USImportResult<T>> _importResults;

        public new ReadOnlyCollection<USImportResult<T>> ImportResults
        {
            get
            {
                return new ReadOnlyCollection<USImportResult<T>>(_importResults);
            }
        }
    }
    /// <summary>
    /// Reprecent a method executing result when input to the method is a list
    /// </summary>
    public class USImportResultSet
    {
        public USImportResultSet()
        {
            _importResults = new List<USImportResult>();
            _containErrors = false;
        }
        public void AddImportResult(USImportResult importResult)
        {
            if (importResult.ErrorOccured)
                _containErrors = true;
            _importResults.Add(importResult);
        }

        protected bool _containErrors;
        /// <summary>
        /// Whether opertation is succeeded
        /// </summary>
        public bool ContainErrors
        {
            get
            {
                return _containErrors;
            }
        }

        private List<USImportResult> _importResults;

        /// <summary>
        /// List of result(one for each input item)
        /// </summary>
        public virtual ReadOnlyCollection<USImportResult> ImportResults
        {
            get
            {
                return new ReadOnlyCollection<USImportResult>(_importResults);
            }
        }
    }
}
