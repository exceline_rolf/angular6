﻿using System.Runtime.Serialization;
using US.Payment.Core.DomainObjects.RutineAndActivities;
using System.Collections.Generic;

namespace US.Payment.Core.SystemObjects
{
    [DataContract]
    public class SQLDBParameterInfo
    {
        private int _ordinalPosition = -1;
        /// <summary>
        /// Parameter position in list
        /// </summary>
        [DataMember]
        public int OrdinalPosition
        {
            get { return _ordinalPosition; }
            set { _ordinalPosition = value; }
        }
        
        private string _activityName = string.Empty;
        /// <summary>
        /// Activity Name
        /// </summary>
        [DataMember]
        public string ActivityName
        {
            get { return _activityName; }
            set { _activityName = value; }
        }
        private string _spName = string.Empty;
        /// <summary>
        /// Stored Procedure name
        /// </summary>
        [DataMember]
        public string SpName
        {
            get { return _spName; }
            set { _spName = value; }
        }
        private string _parameterDirection = string.Empty;
        /// <summary>
        /// IN/OUT/INOUT
        /// </summary>
        [DataMember]
        public string ParameterDirection
        {
            get { return _parameterDirection; }
            set { _parameterDirection = value; }
        }

        private string _parameterName = string.Empty;
        /// <summary>
        /// Stored Procedure parameter name
        /// </summary>
        [DataMember]
        public string ParameterName
        {
            get { return _parameterName; }
            set { _parameterName = value; }
        }

        private string _parameterDataType = string.Empty;
        /// <summary>
        /// Paramenter data type
        /// </summary>
        [DataMember]
        public string ParameterDataType
        {
            get { return _parameterDataType; }
            set { _parameterDataType = value; }
        }
        private int _dataTypeLength = -1;
        /// <summary>
        /// Parameter date type length
        /// </summary>
        [DataMember]
        public int DataTypeLength
        {
            get { return _dataTypeLength; }
            set { _dataTypeLength = value; }
        }

        private List<EntityProperty> _entityProperty = new List<EntityProperty>();
        [DataMember]
        public List<EntityProperty> EntityPropertyList
        {
          get { return _entityProperty; }
          set { _entityProperty = value; }
        }
        private ValueType _valueType = ValueType.Fix;
         [DataMember]
        public ValueType ValueType
        {
            get { return _valueType; }
            set { _valueType = value; }
        }
        private string _value = string.Empty;
         [DataMember]
        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }


    }
}
