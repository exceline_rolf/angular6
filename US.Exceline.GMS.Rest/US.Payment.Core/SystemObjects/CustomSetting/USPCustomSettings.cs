﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Payment.Core.DomainObjects;

namespace US.Payment.Core.SystemObjects.CustomSetting
{
    public class USPCustomSetting :IUSPCustomSetting
    {

        #region IUSPCustomSettings Members

        private int _id = -1;
        public int ID
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        private int _fieldID = -1;
        public int FieldID
        {
            get
            {
                return _fieldID;
            }
            set
            {
                _fieldID = value;
            }
        }

        private string _name = string.Empty;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        private string _displayName = string.Empty;
        public string DisplayName
        {
            get
            {
                return _displayName;
            }
            set
            {
                _displayName = value;
            }
        }

        public int _dataTypeID = -1;
        public int DataTypeID
        {
            get
            {
                return _dataTypeID;
            }
            set
            {
               _dataTypeID = value;
            }
        }

        public object _value = 0;
        public object Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
            }
        }

        private object _defaultValue = 0;
        public object DefaultValue
        {
            get
            {
                return _defaultValue;
            }
            set
            {
                _defaultValue = value;
            }
        }

        #endregion

        #region IUSPCustomSettings Members

        private string _dataType = string.Empty;
        public string DataType
        {
            get
            {
                return _dataType;
            }
            set
            {
                _dataType = value;
            }
        }

        #endregion
    }
}
