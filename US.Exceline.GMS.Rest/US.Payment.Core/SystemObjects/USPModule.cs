﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Reflection;
using System.IO;
using US.Payment.Core.RunTime;
using US.Payment.Core.DomainObjects.SystemObjects;


namespace US.Payment.Core.SystemObjects
{
    public abstract class  USPModule
    {
        public abstract string ID { get; set; }
        public abstract string DisplayName { get; set; }
        public abstract USPFeature DeafaultFeature { get; set; }
        public abstract string ThumbnailImage { get; set; }
        public abstract string ModuleHome { get; set; }
        public abstract string ModulePackage { get; set; }
        public List<USPFeature> Features 
        {
            get
            {
                return GetFeatureList();
            }
            set
            {

            }
        }

        private List<USPFeature> GetFeatureList()
        {
            List<USPFeature> featureList = new List<USPFeature>();
            string moduleFolder = ModulesAndFeatures.ModuleRoot +@"\" +ID +@"\Features";
            string[] featureFolders = Directory.GetDirectories(moduleFolder);
            foreach (string featureFolder in featureFolders)
            {
                string[] dllFiles = Directory.GetFiles(featureFolder, "*.dll");
                USPFeature feature = LoadFeature(dllFiles);
                if (feature != null)
                {
                    featureList.Add(feature);
                }
            }
            return featureList;
        }

        private USPFeature LoadFeature(string[] dllFiles)
        {
            foreach (string file in dllFiles)
            {
                Assembly assembly = Assembly.LoadFrom(file);
                foreach (Type tClass in assembly.GetTypes())
                {
                    if (tClass.BaseType == null)
                        continue;
                    if (tClass.BaseType.FullName == "US.Payment.Core.SystemObjects.USPFeature")
                    {
                        try
                        {
                            USPFeature feature = (USPFeature)assembly.CreateInstance(tClass.FullName);
                            return feature;
                        }
                        catch (Exception ex)
                        {
                            //File.AppendAllText(@"D:\Log.txt", "Feature Loading Error " + ex.Message );
                            //Need to make a Log And Notification record Here
                        }
                    }
                }
            }
            return null;
        }
        [OperationContract]
        public abstract List<USPParameter> GetParamteList();
        [OperationContract]
        public abstract object GetParamterValue(string name);
        public abstract List<IOperation> GetOperations();
    }
}
