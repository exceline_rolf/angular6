﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.SystemObjects.CustomSetting
{
   public class PaymentStatusView
    {
        private string _invoiceNo = string.Empty;
        public string InvoiceNo
        {
            get
            {
                return _invoiceNo;
            }
            set
            {
                _invoiceNo = value;
            }
        }

        private string _dueDate = string.Empty;
        public string DueDate
        {
            get
            {
                return _dueDate;
            }
            set
            {
                _dueDate = value;
            }
        }
       

        private Double _amount = 0.0;
        public Double Amount
        {
            get
            {
                return _amount;
            }
            set
            {
                _amount = value;
            }
        }


        private string _registerDate = string.Empty;
        public string RegisterDate
        {
            get
            {
                return _registerDate;
            }
            set
            {
                _registerDate = value;
            }
        }
        private string _creditorNo = string.Empty;
        public string CreditorNo
        {
            get
            {
                return _creditorNo;
            }
            set
            {
                _creditorNo = value;
            }
        }
        private string _creditorName = string.Empty;
        public string CreditorName
        {
            get
            {
                return _creditorName;
            }
            set
            {
                _creditorName = value;
            }
        }
        private int _debtorRecordID = 0;
        public int DebtorRecordID
        {
            get
            {
                return _debtorRecordID;
            }
            set
            {
                _debtorRecordID = value;
            }
        }

        private string _debtorName = string.Empty;
        public string DebtorName
        {
            get
            {
                return _debtorName;
            }
            set
            {
                _debtorName = value;
            }
        }



        private string _caseNo = string.Empty;
        public string CaseNo
        {
            get
            {
                return _caseNo;
            }
            set
            {
                _caseNo = value;
            }
        }
        private string _debtorNo = string.Empty;
        public string DebtorNo
        {
            get
            {
                return _debtorNo;
            }
            set
            {
                _debtorNo = value;
            }
        }
        private int _isSucceeded = 0;
        public int IsSucceeded
        {
            get
            {
                return _isSucceeded;
            }
            set
            {
                _isSucceeded = value;
            }
        }
        private string _fileName = string.Empty;
        public string FileName
        {
            get
            {
                return _fileName;
            }
            set
            {
                _fileName = value;
            }
        }

        private int _isFailed = 0;
        public int IsFailed
        {
            get
            {
                return _isFailed;
            }
            set
            {
                _isFailed = value;
            }
        }
    }
}
