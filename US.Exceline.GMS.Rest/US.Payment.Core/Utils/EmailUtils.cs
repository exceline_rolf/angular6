﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using Microsoft.Exchange.WebServices.Data;
//using Microsoft.Exchange.WebServices.Data;

namespace US.Payment.Core.Utils
{
    public class EmailUtils
    {
        public static KeyValuePair<string, EmailMessage> ReadNextEmail(string emailAccount, string password, string domain, string serverUrl, string sSyncState)
        {
            ExchangeService service = new ExchangeService();
            service.Credentials = new WebCredentials(emailAccount ,password ,domain );
            try
            {
                service.AutodiscoverUrl(emailAccount, ValidateRedirectionUrlCallback);
            }
            catch (AutodiscoverLocalException ee)
            {
                service.Url = new Uri(serverUrl);
            }

            ChangeCollection<ItemChange> icc = service.SyncFolderItems(new FolderId(WellKnownFolderName.Inbox), PropertySet.FirstClassProperties, null, 1, SyncFolderItemsScope.NormalItems, sSyncState);
            if (icc.Count == 0)
            {
                return new KeyValuePair<string, EmailMessage>(null, null);
            }
            else
            {
                foreach (ItemChange ic in icc)
                {
                    //Email message = new Email();
                    EmailMessage message = EmailMessage.Bind(service, ic.ItemId);
                    //if (ic.ChangeType == ChangeType.Create)
                    //{
                    //    //TODO: Create item on the client.
                    //}
                    //else if (ic.ChangeType == ChangeType.Update)
                    //{
                    //    //TODO: Update item on the client.
                    //}
                    //else if (ic.ChangeType == ChangeType.Delete)
                    //{
                    //    //TODO: Delete item on the client.
                    //}
                    //else if (ic.ChangeType == ChangeType.ReadFlagChange)
                    //{
                    //    //TODO: Update the item's read flag on the client.
                    //}

                    ////Console.WriteLine("ChangeType: " + ic.ChangeType.ToString());
                    ////Console.WriteLine("ItemId: " + ic.ItemId.UniqueId);
                    //if (ic.Item != null)
                    //{
                    //    //Console.WriteLine("Subject: " + ic.Item.Subject);
                    //}
                    ////Console.WriteLine("===========");
                    //Console.WriteLine(message.Body);

                    //message.Body = ic.Item.Body.Text;
                    //message.Subject = ic.Item.Subject;
                    //message.From = ic.Item.DisplayTo;
                    return new KeyValuePair<string, EmailMessage>(icc.SyncState, message);
                }
            }
            return new KeyValuePair<string, EmailMessage>(null, null); ;
        }

        private static bool ValidateRedirectionUrlCallback(string url)
        {
            return true;
        }

        public static void SendingEmailMessage(string fromAdd,string toAdd,string subject,string body)
        {
            MailMessage message = new MailMessage();
            message.From = new MailAddress(fromAdd);
            message.To.Add(new MailAddress(toAdd));
            message.Subject = subject;
            message.Body = body;

            SmtpClient client = new SmtpClient();
            client.Send(message);
        }
    }
}
