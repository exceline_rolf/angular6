﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.Exceptions
{
    internal class USPConfigurationReadException : Exception
    {
        public USPConfigurationReadException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
