﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Payment.Core.BusinessDomainObjects;

namespace US.Payment.SOR.CreateATGFile
{
    public class USPDDHistory : IUSPDDHistory
    {
        #region IUSPDDHistory Members
        private string _fileName = string.Empty;
        public string FileName
        {
            get { return _fileName; }
            set { _fileName = value; }
        }
        private string _orderId = string.Empty;
        public string OrderId
        {
            get { return _orderId; }
            set { _orderId = value; }
        }
        private string _transactionID = string.Empty;
        public string TransactionID
        {
            get { return _transactionID; }
            set { _transactionID = value; }
        }

        private string _cancellation = string.Empty;
        public string Cancellatrion
        {
            get { return _cancellation; }
            set { _cancellation = value; }
        }

        private int _arNo = -1;
        public int ArNo
        {
            get { return _arNo; }
            set { _arNo = value; }
        }

        #endregion
    }
}
