﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects
{
    public class USPUDFTransaction : IUSPUDFTransaction
    {
    #region IUSPUDFTransaction Members


private  string  _CID=string.Empty;
public string  CID
{
	  get 
	{ 
		 return _CID; 
	}
	  set 
	{ 
		_CID = value;
	}
}


private  bool  _IsSuccess=false;
public bool  IsSuccess
{
	  get 
	{ 
		 return _IsSuccess; 
	}
	  set 
	{ 
		_IsSuccess = value;
	}
}


private  string  _PID=string.Empty;
public string  PID
{
	  get 
	{ 
		 return _PID; 
	}
	  set 
	{ 
		_PID = value;
	}
}


private  double  _TAmount=0;
public double  TAmount
{
	  get 
	{ 
		 return _TAmount; 
	}
	  set 
	{ 
		_TAmount = value;
	}
}


private  string  _TCaseNumber=string.Empty;
public string  TCaseNumber
{
	  get 
	{ 
		 return _TCaseNumber; 
	}
	  set 
	{ 
		_TCaseNumber = value;
	}
}


private  string  _TCreditinvoice=string.Empty;
public string  TCreditinvoice
{
	  get 
	{ 
		 return _TCreditinvoice; 
	}
	  set 
	{ 
		_TCreditinvoice = value;
	}
}


private  string  _TDate=string.Empty;
public string  TDate
{
	  get 
	{ 
		 return _TDate; 
	}
	  set 
	{ 
		_TDate = value;
	}
}


private  string  _TDueDate=string.Empty;
public string  TDueDate
{
	  get 
	{ 
		 return _TDueDate; 
	}
	  set 
	{ 
		_TDueDate = value;
	}
}


private  string  _TInfo=string.Empty;
public string  TInfo
{
	  get 
	{ 
		 return _TInfo; 
	}
	  set 
	{ 
		_TInfo = value;
	}
}


private  double  _TInterest=0;
public double  TInterest
{
	  get 
	{ 
		 return _TInterest; 
	}
	  set 
	{ 
		_TInterest = value;
	}
}


private  string  _TKid=string.Empty;
public string  TKid
{
	  get 
	{ 
		 return _TKid; 
	}
	  set 
	{ 
		_TKid = value;
	}
}


private  string  _TNumber=string.Empty;
public string  TNumber
{
	  get 
	{ 
		 return _TNumber; 
	}
	  set 
	{ 
		_TNumber = value;
	}
}


private  string  _TReference1=string.Empty;
public string  TReference1
{
	  get 
	{ 
		 return _TReference1; 
	}
	  set 
	{ 
		_TReference1 = value;
	}
}


private  string  _TReference2=string.Empty;
public string  TReference2
{
	  get 
	{ 
		 return _TReference2; 
	}
	  set 
	{ 
		_TReference2 = value;
	}
}


private  string  _TStatus=string.Empty;
public string  TStatus
{
	  get 
	{ 
		 return _TStatus; 
	}
	  set 
	{ 
		_TStatus = value;
	}
}


private  string  _TType=string.Empty;
public string  TType
{
	  get 
	{ 
		 return _TType; 
	}
	  set 
	{ 
		_TType = value;
	}
}


private  int  _ARItemNo=-1;
public int  ARItemNo
{
	  get 
	{
        return _ARItemNo; 
	}
	  set 
	{
        _ARItemNo = value;
	}
}

#endregion
}
}
