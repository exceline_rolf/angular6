﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using US.Payment.Core.DomainObjects.DomainObjects.Common;

namespace US.Payment.Core.BusinessDomainObjects.Creditor
{
    [DataContract]
    public class CreditorGeneralInfo
    {

        #region creditor settings property


        private int _itemNo = -1;
        [DataMember]
        public int ItemNo
        {
            get { return _itemNo; }
            set { _itemNo = value; }
        }

        private string _creditorInkasoId = string.Empty;
        [DataMember]
        public string CreditorInkasoId
        {
            get { return _creditorInkasoId; }
            set { _creditorInkasoId = value; }
        }


        private string _creditorNo = string.Empty;
        [DataMember]
        public string CreditorNo
        {
            get { return _creditorNo; }
            set { _creditorNo = value; }
        }

        private decimal _balance = 0;
        [DataMember]
        public decimal Balance
        {
            get { return _balance; }
            set { _balance = value; }
        }

        private DateTime _atgDate;
        [DataMember]
        public DateTime AtgDate
        {
            get { return _atgDate; }
            set { _atgDate = value; }
        }

        private DateTime _regDate;
        [DataMember]
        public DateTime RegDate
        {
            get { return _regDate; }
            set { _regDate = value; }
        }

        private string _companyID = string.Empty;
        [DataMember]
        public string CompanyID
        {
            get { return _companyID; }
            set { _companyID = value; }
        }

        private string _directNo = string.Empty;
        [DataMember]
        public string DirectNo
        {
            get { return _directNo; }
            set { _directNo = value; }
        }

        private int _creditorGroupNo = -1;
        [DataMember]
        public int CreditorGroupNo
        {
            get { return _creditorGroupNo; }
            set { _creditorGroupNo = value; }
        }

        private string _creditorGroupName = string.Empty;
        [DataMember]
        public string CreditorGroupName
        {
            get { return _creditorGroupName; }
            set { _creditorGroupName = value; }
        }

        private string _accountNo = string.Empty;
        [DataMember]
        public string AccountNo
        {
            get { return _accountNo; }
            set { _accountNo = value; }
        }

        private DateTime _startDate;
        [DataMember]
        public DateTime StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }

        private string _productNo = string.Empty;
        [DataMember]
        public string ProductNo
        {
            get { return _productNo; }
            set { _productNo = value; }
        }

        private DateTime _sendBBSOkDate = new DateTime();
        [DataMember]
        public DateTime SendBBSOkDate
        {
            get { return _sendBBSOkDate; }
            set { _sendBBSOkDate = value; }
        }

        private DateTime _bBSOkDate = new DateTime();
        [DataMember]
        public DateTime BBSOkDate
        {
            get { return _bBSOkDate; }
            set { _bBSOkDate = value; }
        }

        private DateTime _sendExlineOkDate = new DateTime();
        [DataMember]
        public DateTime SendExlineOkDate
        {
            get { return _sendExlineOkDate; }
            set { _sendExlineOkDate = value; }
        }

        private DateTime _exlineOkDate = new DateTime();
        [DataMember]
        public DateTime ExlineOkDate
        {
            get { return _exlineOkDate; }
            set { _exlineOkDate = value; }
        }

        private DateTime _sendKundeOkDate = new DateTime();
        [DataMember]
        public DateTime SendKundeOkDate
        {
            get { return _sendKundeOkDate; }
            set { _sendKundeOkDate = value; }
        }

        private string _commpanyId = string.Empty;
        [DataMember]
        public string CommpanyId
        {
            get { return _commpanyId; }
            set { _commpanyId = value; }
        }

        private string _netsId = string.Empty;
        [DataMember]
        public string NetsId
        {
            get { return _netsId; }
            set { _netsId = value; }
        }

        private int _costaCatA = -1;
        [DataMember]
        public int CostaCatA
        {
            get { return _costaCatA; }
            set { _costaCatA = value; }
        }

        private int _costaCatB = -1;
        [DataMember]
        public int CostaCatB
        {
            get { return _costaCatB; }
            set { _costaCatB = value; }
        }

        private int _costaCatC = -1;
        [DataMember]
        public int CostaCatC
        {
            get { return _costaCatC; }
            set { _costaCatC = value; }
        }

        private int _costaCatD = -1;
        [DataMember]
        public int CostaCatD
        {
            get { return _costaCatD; }
            set { _costaCatD = value; }
        }

        private bool _allowPrint = false;
        [DataMember]
        public bool AllowPrint
        {
            get { return _allowPrint; }
            set { _allowPrint = value; }
        }

        private DateTime _allowPrintDate = new DateTime();
        [DataMember]
        public DateTime AllowPrintDate
        {
            get { return _allowPrintDate; }
            set { _allowPrintDate = value; }
        }

        private DateTime _printDisableDate = new DateTime();
        [DataMember]
        public DateTime PrintDisableDate
        {
            get { return _printDisableDate; }
            set { _printDisableDate = value; }
        }

        private string _printAllowComment = string.Empty;
        [DataMember]
        public string PrintAllowComment
        {
            get { return _printAllowComment; }
            set { _printAllowComment = value; }
        }

        private bool _creditorStatus = false;
        [DataMember]
        public bool CreditorStatus
        {
            get { return _creditorStatus; }
            set { _creditorStatus = value; }
        }

        private int _groupId = -1;
        [DataMember]
        public int GroupId
        {
            get { return _groupId; }
            set { _groupId = value; }
        }

        private string _groupName = string.Empty;
        [DataMember]
        public string GroupName
        {
            get { return _groupName; }
            set { _groupName = value; }
        }


        #endregion



        #region ent property region

        private string _creditorName = string.Empty;
        [DataMember]
        public string CreditorName
        {
            get { return _creditorName; }
            set { _creditorName = value; }
        }

        private string _firstName = string.Empty;
        [DataMember]
        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        private string _lastName = string.Empty;
        [DataMember]
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        private DateTime _bornDate = new DateTime();
        [DataMember]
        public DateTime BornDate
        {
            get { return _bornDate; }
            set { _bornDate = value; }
        }

        private string _personNo = string.Empty;
        [DataMember]
        public string PersonNo
        {
            get { return _personNo; }
            set { _personNo = value; }
        }

        private int _entNo = -1;
        [DataMember]
        public int EntNo
        {
            get { return _entNo; }
            set { _entNo = value; }
        }




        #endregion

        #region address

        private string _addressNo = string.Empty;
        [DataMember]
        public string AddressNo
        {
            get { return _addressNo; }
            set { _addressNo = value; }
        }

        private string _add1 = string.Empty;
        [DataMember]
        public string Add1
        {
            get { return _add1; }
            set { _add1 = value; }
        }

        private string _add2 = string.Empty;
        [DataMember]
        public string Add2
        {
            get { return _add2; }
            set { _add2 = value; }
        }

        private string _add3 = string.Empty;
        [DataMember]
        public string Add3
        {
            get { return _add3; }
            set { _add3 = value; }
        }

        private string _addressSource = string.Empty;
        [DataMember]
        public string AddressSource
        {
            get { return _addressSource; }
            set { _addressSource = value; }
        }



        private string _mobileNo = string.Empty;
        [DataMember]
        public string MobileNo
        {
            get { return _mobileNo; }
            set { _mobileNo = value; }
        }

        private string _teleWork = string.Empty;
        [DataMember]
        public string TeleWork
        {
            get { return _teleWork; }
            set { _teleWork = value; }
        }

        private string _teleHome = string.Empty;
        [DataMember]
        public string TeleHome
        {
            get { return _teleHome; }
            set { _teleHome = value; }
        }

        private string _emailID = string.Empty;
        [DataMember]
        public string EmailID
        {
            get { return _emailID; }
            set { _emailID = value; }
        }

        private string _fax = string.Empty;
        [DataMember]
        public string Fax
        {
            get { return _fax; }
            set { _fax = value; }
        }

        private string _skypeID = string.Empty;
        [DataMember]
        public string SkypeID
        {
            get { return _skypeID; }
            set { _skypeID = value; }
        }

        private string _msnID = string.Empty;
        [DataMember]
        public string MsnID
        {
            get { return _msnID; }
            set { _msnID = value; }
        }

        private string _sms = string.Empty;
        [DataMember]
        public string SMS
        {
            get { return _sms; }
            set { _sms = value; }
        }


        private bool _isDefaultAddress = false;
        [DataMember]
        public bool IsDefaultAddress
        {
            get { return _isDefaultAddress; }
            set { _isDefaultAddress = value; }
        }

        private string _city = string.Empty;
        [DataMember]
        public string City
        {
            get { return _city; }
            set { _city = value; }
        }

        private string _countyID = string.Empty;
        [DataMember]
        public string CountyID
        {
            get { return _countyID; }
            set { _countyID = value; }
        }

        private USPCountry _country = new USPCountry();
        [DataMember]
        public USPCountry Country
        {
            get { return _country; }
            set 
            { 
                _country = value;                
            }
        }


        private string _countyName = string.Empty;
        [DataMember]
        public string CountyName
        {
            get { return _countyName; }
            set { _countyName = value; }
        }

        #endregion

        #region EntRole

        private int _entRoleId = -1;
        [DataMember]
        public int EntRoleId
        {
            get { return _entRoleId; }
            set { _entRoleId = value; }
        }

        private string _roleId = string.Empty;
        [DataMember]
        public string RoleId
        {
            get { return _roleId; }
            set { _roleId = value; }
        }

        private string _ref = string.Empty;
        [DataMember]
        public string Ref
        {
            get { return _ref; }
            set { _ref = value; }
        }

        private int _entRoleRef = -1;
        [DataMember]
        public int EntRoleRef
        {
            get { return _entRoleRef; }
            set { _entRoleRef = value; }
        }

        private string _countryId = string.Empty;
        [DataMember]
        public string CountryId
        {
            get { return _countryId; }
            set { _countryId = value; }
        }

        private string _zipName = string.Empty;
        [DataMember]
        public string ZipName
        {
            get { return _zipName; }
            set { _zipName = value; }
        }

        private string _zipCode = string.Empty;
        [DataMember]
        public string ZipCode
        {
            get { return _zipCode; }
            set { _zipCode = value; }
        }

        private int _trace = -1;
        [DataMember]
        public int Trace
        {
            get { return _trace; }
            set { _trace = value; }
        }

        private int _notifyBank = -1;
        [DataMember]
        public int NotifyBank
        {
            get { return _notifyBank; }
            set { _notifyBank = value; }
        }

        private string _remitAccountNo = string.Empty;
        [DataMember]
        public string RemitAccountNo
        {
            get { return _remitAccountNo; }
            set { _remitAccountNo = value; }
        }

        private string _helpAccountNo = string.Empty;
        [DataMember]
        public string HelpAccountNo
        {
            get { return _helpAccountNo; }
            set { _helpAccountNo = value; }
        }
        #endregion
    }
}
