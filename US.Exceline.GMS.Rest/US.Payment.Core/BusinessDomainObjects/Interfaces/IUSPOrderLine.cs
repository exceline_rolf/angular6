﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects
{
    /// <summary>
    /// Reprecent order line record
    /// </summary>
    public interface IUSPOrderLine : ITagEnabled
    {

        string DiscountAmount
        {
            get;
            set;
        }
        string EmployeeNo
        {
            get;
            set;
        }
        string SponsorRef
        {
            get;
            set;
        }
        int Visit
        {
            get;
            set;
        }

        string LastVisit
        {
            get;
            set;
        }
        string Amount
        {
            get;
            set;
        }
        string AmountThisOrderLines
        {
            get;
            set;
        }

        int OrderLineId
        {
            get;
            set;
        }

        int ARItemNo
        {
            get;
            set;
        }

        string InkassoId
        {
            get;
            set;
        }

        string CustId
        {
            get;
            set;
        }

        string LastName
        {
            get;
            set;
        }
        string FirstName
        {
            get;
            set;
        }
        string NameOncontract
        {
            get;
            set;
        }
        string Address
        {
            get;
            set;
        }
        string ZipCode
        {
            get;
            set;
        }
        string PostPlace
        {
            get;
            set;
        }
    }
}
