﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects
{
  public interface IUSUDFTransferedResult
    {
      string ExternalCaseID { get; set; }
        List<ITransactionResult> TransactionResults { get; set; }

    }

  public interface ITransactionResult
  {
      int ARItemNo { get; set; }
      string ExternalTransID { get; set; }
  }
            
}



