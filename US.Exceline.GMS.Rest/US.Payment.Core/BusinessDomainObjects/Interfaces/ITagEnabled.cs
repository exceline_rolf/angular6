﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects
{
    /// <summary>
    /// Super class for Domain Object classes that have Tags as properties
    /// </summary>
    public interface ITagEnabled
    {
        string Tag1 { get; set; }
        string Tag2 { get; set; }
        string Tag3 { get; set; }
    }
}
