﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects
{
   public interface IUSPApplicationSetting
    {
        string Name{ get; set; }
        string TrancemeterID { get; set; }
        
    }
}
