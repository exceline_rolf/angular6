﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects.Interfaces
{
    public interface IUSPCompany
    {
        int companyId { get; set; }
        string CompanyName { get; set; }
        string Address1 { get; set; }
        string Address2 { get; set; }
        string City { get; set; }
        string state { get; set; }
        string Country { get; set; }
        string CountryId { get; set; }
        string Fax { get; set; }
        string Email { get; set; }
        string GeneralTelephoneNo { get; set; }
        string VATRegNo { get; set; }
        string Account { get; set; }
        string EnterpriseNo { get; set; }
        string ClientAccount { get; set; }
        string Currency { get; set; }
        int MarkForReminder { get; set; }
        int ReminderLetter { get; set; }
        int MarkForDebtCollection { get; set; }
        int TransferDebtCollectionDays { get; set; }
        int DefaultUnfreezeDaysForInvoice { get; set; }
        int DefaultUnfreezeDaysForDebtor { get; set; }
    }
}
