﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects
{
    public interface IUSPHistoryDetail : ITagEnabled
    {
        int Token{ get; set; }
        int PrincipleId{ get; set; }
        string PrincipleType{ get; set; }
        string LineType{ get; set; }
        int LineNumber { get; set; }
        string Message { get; set; }
        
    }
}
