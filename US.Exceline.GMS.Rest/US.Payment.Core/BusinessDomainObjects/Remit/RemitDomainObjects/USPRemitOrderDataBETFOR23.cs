﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects.Remit.RemitDomainObjects
{
    public class USPRemitOrderDataBETFOR23:IUSPRemitOrderDataBETFOR23 
    {
        #region IUSPRemitOrderDataBETFOR23 Members

        private string _applicationHeader = string.Empty;
        public string BETFOR23_ApplicationHeader
        {
            get { return _applicationHeader; }
            set { _applicationHeader = value; }
        }

        private string _transactionCode = string.Empty;
        public string BETFOR23_TransactionCode
        {
            get { return _transactionCode; }
            set { _transactionCode = value; }
        }

        private string _enterpriseNumber = string.Empty;
        public string BETFOR23_EnterpriseNumber
        {
            get { return _enterpriseNumber; }
            set { _enterpriseNumber = value; }
        }

        private string _accountNumber = string.Empty;
        public string BETFOR23_AccountNumber
        {
            get { return _accountNumber; }
            set { _accountNumber = value; }
        }

        private string _sequenceControlField = string.Empty;
        public string BETFOR23_SequenceControlField
        {
            get { return _sequenceControlField; }
            set { _sequenceControlField = value; }
        }

        private string _referenceNumber = string.Empty;
        public string BETFOR23_ReferenceNumber
        {
            get { return _referenceNumber; }
            set { _referenceNumber = value; }
        }

        private string _payeeRefInvoice1 = string.Empty;
        public string BETFOR23_PayeeRefInvoice1
        {
            get { return _payeeRefInvoice1; }
            set { _payeeRefInvoice1 = value; }
        }

        private string _payeeRefInvoice2 = string.Empty;
        public string BETFOR23_PayeeRefInvoice2
        {
            get { return _payeeRefInvoice2; }
            set { _payeeRefInvoice2 = value; }
        }

        private string _payeeRefInvoice3 = string.Empty;
        public string BETFOR23_PayeeRefInvoice3
        {
            get { return _payeeRefInvoice3; }
            set { _payeeRefInvoice3 = value; }
        }

        private string _customerID = string.Empty;
        public string BETFOR23_CustomerID
        {
            get { return _customerID; }
            set { _customerID = value; }
        }

        private string _ownReference = string.Empty;
        public string BETFOR23_OwnReference
        {
            get { return _ownReference; }
            set { _ownReference = value; }
        }

        private string _invoiceAmount = string.Empty;
        public string BETFOR23_InvoiceAmount
        {
            get { return _invoiceAmount; }
            set { _invoiceAmount = value; }
        }

        private string _creditCode = string.Empty;
        public string BETFOR23_Debit_CreditCode
        {
            get { return _creditCode; }
            set { _creditCode = value; }
        }

        private string _reserved1 = string.Empty;
        public string BETFOR23_Reserved1
        {
            get { return _reserved1; }
            set { _reserved1 = value; }
        }

        private string _serialNumber = string.Empty;
        public string BETFOR23_SerialNumber
        {
            get { return _serialNumber; }
            set { _serialNumber = value; }
        }

        private string _reserved2 = string.Empty;
        public string BETFOR23_Reserved2
        {
            get { return _reserved2; }
            set { _reserved2 = value; }
        }

        #endregion
    }
}
