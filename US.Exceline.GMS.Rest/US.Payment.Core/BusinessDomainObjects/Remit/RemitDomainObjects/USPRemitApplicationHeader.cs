﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects.Remit
{
    public class USPRemitApplicationHeader:IUSPRemitApplicationHeader
    {
        #region IUSPRemitApplicationHeader Members

        private string _id = string.Empty;//Position 1-2(2)
        public string AH_Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _version = string.Empty; // Position 3(1)
        public string AH_Version
        {
            get { return _version; }
            set { _version = value; }
        }

        private string _replyCode = string.Empty;// Position 4-5(2)
        public string AH_ReplyCode
        {
            get { return _replyCode; }
            set { _replyCode = value; }
        }

        private string _procedureId = string.Empty;// Position 6-9(4)
        public string AH_ProcedureId
        {
            get { return _procedureId; }
            set { _procedureId = value; }
        }

        private string _transDate = string.Empty;//Position 10-13(4)
        public string AH_TransDate
        {
            get { return _transDate; }
            set { _transDate = value; }
        }

        private string _daysSequenceNo = string.Empty; //Position 14-19(6)
        public string AH_DaysSequenceNo
        {
            get { return _daysSequenceNo; }
            set { _daysSequenceNo = value; }
        }

        private string _transCode = string.Empty; //Position 20-27(8)
        public string AH_TransCode
        {
            get { return _transCode; }
            set { _transCode = value; }
        }

        private string _endUserId = string.Empty; //Position 28-38(11)
        public string AH_EndUserId
        {
            get { return _endUserId; }
            set { _endUserId = value; }
        }

        private string _numberOf80 = string.Empty;//Position 39-40(
        public string AH_NumberOf80
        {
            get { return _numberOf80; }
            set { _numberOf80 = value; }
        }

        #endregion
    }
}
