﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects.Remit.RemitDomainObjects
{
    public class USPRemitOrderDataBETFOR00:IUSPRemitOrderDataBETFOR00 
    {
        #region IUSPRemitOrderDataBETFOR00 Members

        private string _applicationHeader = string.Empty;
        public string BETFOR00_ApplicationHeader
        {
            get { return _applicationHeader; }
            set { _applicationHeader = value; }
        }

        private string _transactionCode = string.Empty;
        public string BETFOR00_TransactionCode
        {
            get { return _transactionCode; }
            set { _transactionCode = value; }
        }

        private string _enterpriseNumber = string.Empty;
        public string BETFOR00_EnterpriseNumber
        {
            get { return _enterpriseNumber; }
            set { _enterpriseNumber = value; }
        }

        private string _division = string.Empty;
        public string BETFOR00_Division
        {
            get { return _division; }
            set { _division = value; }
        }

        private string _sequenceControlField = string.Empty;
        public string BETFOR00_SequenceControlField
        {
            get { return _sequenceControlField; }
            set { _sequenceControlField = value; }
        }

        private string _reserved1 = string.Empty;
        public string BETFOR00_Reserved1
        {
            get { return _reserved1; }
            set { _reserved1 = value; }
        }

        private string _productionDate = string.Empty;
        public string BETFOR00_ProductionDate
        {
            get { return _productionDate; }
            set { _productionDate = value; }
        }

        private string _password = string.Empty;
        public string BETFOR00_Password
        {
            get { return _password; }
            set { _password = value; }
        }

        private string _procedureVersion = string.Empty;
        public string BETFOR00_ProcedureVersion
        {
            get { return _procedureVersion; }
            set { _procedureVersion = value; }
        }

        private string _newPassword = string.Empty;
        public string BETFOR00_NewPassword
        {
            get { return _newPassword; }
            set { _newPassword = value; }
        }

        private string _operatorNumber = string.Empty;
        public string BETFOR00_OperatorNumber
        {
            get { return _operatorNumber; }
            set { _operatorNumber = value; }
        }

        private string _sealUseSIGILL = string.Empty;
        public string BETFOR00_SealUseSIGILL
        {
            get { return _sealUseSIGILL; }
            set { _sealUseSIGILL = value; }
        }

        private string _sealDate = string.Empty;
        public string BETFOR00_SealDate
        {
            get { return _sealDate; }
            set { _sealDate = value; }
        }

        private string _partialKey = string.Empty;
        public string BETFOR00_PartialKey
        {
            get { return _partialKey; }
            set { _partialKey = value; }
        }

        private string _sealHow = string.Empty;
        public string BETFOR00_SealHow
        {
            get { return _sealHow; }
            set { _sealHow = value; }
        }

        private string _reserved2 = string.Empty;
        public string BETFOR00_Reserved2
        {
            get { return _reserved2; }
            set { _reserved2 = value; }
        }

        private string _sealUseAEGIS = string.Empty;
        public string BETFOR00_SealUseAEGIS
        {
            get { return _sealUseAEGIS; }
            set { _sealUseAEGIS = value; }
        }

        private string _reserved3 = string.Empty;
        public string BETFOR00_Reserved3
        {
            get { return _reserved3; }
            set { _reserved3 = value; }
        }

        #endregion
    }
}
