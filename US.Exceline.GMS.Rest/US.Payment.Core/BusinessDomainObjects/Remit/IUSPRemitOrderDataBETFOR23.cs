﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects.Remit
{
    public interface IUSPRemitOrderDataBETFOR23
    {
        //Positon(Length) 1-40(40)
        //Value =
        string BETFOR23_ApplicationHeader
        {
            get;
            set;
        }

        //Positon(Length) 41-48(8)
        //Value =BETFOR23
        string BETFOR23_TransactionCode
        {
            get;
            set;
        }

        //Positon(Length) 49-59(11)
        //Value =Company Number
        string BETFOR23_EnterpriseNumber
        {
            get;
            set;
        }

        //Positon(Length) 60-70(11)
        //Value =Payer Account 
        string BETFOR23_AccountNumber
        {
            get;
            set;
        }

        //Positon(Length) 71-74(4)
        //Value =auto incremented 1 greater than before record
        string BETFOR23_SequenceControlField
        {
            get;
            set;
        }

        //Positon(Length) 75-80(6)
        //Value =??????
        string BETFOR23_ReferenceNumber
        {
            get;
            set;
        }

        //Positon(Length) 81-120(40)
        //Value =Message (If custormer Id field filled in, value= empty)
        string BETFOR23_PayeeRefInvoice1
        {
            get;
            set;
        }

        //Positon(Length) 121-160(40)
        //Value =Emty 
        string BETFOR23_PayeeRefInvoice2
        {
            get;
            set;
        }

        //Positon(Length) 161-200(40)
        //Value =Empty
        string BETFOR23_PayeeRefInvoice3
        {
            get;
            set;
        }

        //Positon(Length) 201-227(27)
        //Value =???? (If payee ref invoice field filled in, value= empty)
        string BETFOR23_CustomerID
        {
            get;
            set;
        }

        //Positon(Length) 228-257(30)
        //Value =??? Empty
        string BETFOR23_OwnReference
        {
            get;
            set;
        }

        //Positon(Length) 258-272(15)
        //Value =total Amount
        string BETFOR23_InvoiceAmount
        {
            get;
            set;
        }

        //Positon(Length) 273(1)
        //Value =???? D
        string BETFOR23_Debit_CreditCode
        {
            get;
            set;
        }

        //Positon(Length) 274-293(20)
        //Value =empty
        string BETFOR23_Reserved1
        {
            get;
            set;
        }

        //Positon(Length) 294-296(3)
        //Value =start with 001 allocated by bank
        string BETFOR23_SerialNumber
        {
            get;
            set;
        }

        //Positon(Length) 297-320(24)
        //Value =Empty '0' added to end of the line
        string BETFOR23_Reserved2
        {
            get;
            set;
        }

      
    }
}
