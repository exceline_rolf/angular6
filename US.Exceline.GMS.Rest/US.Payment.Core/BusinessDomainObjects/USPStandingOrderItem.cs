﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects
{
    public class USPStandingOrderItem : IUSPStandingOrderItem
    {
    #region IUSPStandingOrderItem Members


private  string  _CreName=string.Empty;
public string  CreName
{
	  get 
	{ 
		 return _CreName; 
	}
	  set 
	{ 
		_CreName = value;
	}
}


private  string  _DueDate=string.Empty;
public string  DueDate
{
	  get 
	{ 
		 return _DueDate; 
	}
	  set 
	{ 
		_DueDate = value;
	}
}


private  string  _Amount=string.Empty;
public string  Amount
{
	  get 
	{ 
		 return _Amount; 
	}
	  set 
	{ 
		_Amount = value;
	}
}


private  string  _ContractNo=string.Empty;
public string  ContractNo
{
	  get 
	{ 
		 return _ContractNo; 
	}
	  set 
	{ 
		_ContractNo = value;
	}
}


private  string  _KID=string.Empty;
public string  KID
{
	  get 
	{ 
		 return _KID; 
	}
	  set 
	{ 
		_KID = value;
	}
}


private  string  _aRitem=string.Empty;
public string  aRitem
{
	  get 
	{ 
		 return _aRitem; 
	}
	  set 
	{ 
		_aRitem = value;
	}
}


private  string  _Message=string.Empty;
public string  Message
{
	  get 
	{
        return _Message; 
	}
	  set 
	{
        _Message = value;
	}
}

#endregion
}
}
