﻿// --------------------------------------------------------------------------
// Copyright(c) <2011> Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USPayment
// Project Name      : US.Payment.Service
// Coding Standard   : US Coding Standards
// Author            : AAB
// Modified          : ISU
// Created Timestamp : 01/12/2011 HH:MM  PM
// --------------------------------------------------------------------------

using System.Runtime.Serialization;
namespace US.Payment.Core.BusinessDomainObjects.GeneralSearch
{
    [DataContract]
    public class USPDebtorSearch
    {
        private int _dataSource = -1;
        [DataMember]
        public int DataSource
        {
            get { return _dataSource; }
            set { _dataSource = value; }
        }
        private string _debtorNumber = string.Empty;
        [DataMember]
        public string DebtorNumber
        {
            get { return _debtorNumber; }
            set { _debtorNumber = value; }
        }

        private string _debtorName = string.Empty;
        [DataMember]
        public string DebtorName
        {
            get { return _debtorName; }
            set { _debtorName = value; }
        }

        private int _ArNo = -1;
        [DataMember]
        public int ArNo
        {
            get { return _ArNo; }
            set { _ArNo = value; }
        }
        private string _creditorNumber = string.Empty;
        [DataMember]
        public string CreditorNumber
        {
            get { return _creditorNumber; }
            set { _creditorNumber = value; }
        }
        private string _creditorName = string.Empty;
        [DataMember]
        public string CreditorName
        {
            get { return _creditorName; }
            set { _creditorName = value; }
        }
        private string _debtorHomeTelephone = string.Empty;
        [DataMember]
        public string DebtorHomeTelephone
        {
            get { return _debtorHomeTelephone; }
            set { _debtorHomeTelephone = value; }
        }
        private string _debtorMobile = string.Empty;
        [DataMember]
        public string DebtorMobile
        {
            get { return _debtorMobile; }
            set { _debtorMobile = value; }
        }
        private string _debtorWork = string.Empty;
        [DataMember]
        public string DebtorWork
        {
            get { return _debtorWork; }
            set { _debtorWork = value; }
        }
        private string _debtorEmail = string.Empty;
        [DataMember]
        public string DebtorEmail
        {
            get { return _debtorEmail; }
            set { _debtorEmail = value; }
        }
        private string _hit = string.Empty;
        [DataMember]
        public string Hit
        {
            get { return _hit; }
            set { _hit = value; }
        }


    }


}

   
