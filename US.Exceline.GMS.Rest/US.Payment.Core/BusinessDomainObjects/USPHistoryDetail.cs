﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects
{
    public class USPHistoryDetail : IUSPHistoryDetail
    {
    #region IUSPHistoryDetail Members


private  int  _Token=-1;
public int  Token
{
	  get 
	{ 
		 return _Token; 
	}
	  set 
	{ 
		_Token = value;
	}
}


private  int  _PrincipleId=-1;
public int  PrincipleId
{
	  get 
	{ 
		 return _PrincipleId; 
	}
	  set 
	{ 
		_PrincipleId = value;
	}
}


private  string  _PrincipleType=string.Empty;
public string  PrincipleType
{
	  get 
	{ 
		 return _PrincipleType; 
	}
	  set 
	{ 
		_PrincipleType = value;
	}
}


private string  _LineType=string.Empty;
public string  LineType
{
	  get 
	{ 
		 return _LineType; 
	}
	  set 
	{ 
		_LineType = value;
	}
}


private  int  _LineNumber=-1;
public int  LineNumber
{
	  get 
	{ 
		 return _LineNumber; 
	}
	  set 
	{ 
		_LineNumber = value;
	}
}


private string  _Message=string.Empty;
public string  Message
{
	  get 
	{ 
		 return _Message; 
	}
	  set 
	{ 
		_Message = value;
	}
}

#endregion

#region ITagEnabled Members


private  string  _Tag1=string.Empty;
public string  Tag1
{
	  get 
	{ 
		 return _Tag1; 
	}
	  set 
	{ 
		_Tag1 = value;
	}
}


private  string  _Tag2=string.Empty;
public string  Tag2
{
	  get 
	{ 
		 return _Tag2; 
	}
	  set 
	{ 
		_Tag2 = value;
	}
}


private  string  _Tag3=string.Empty;
public string  Tag3
{
	  get 
	{ 
		 return _Tag3; 
	}
	  set 
	{ 
		_Tag3 = value;
	}
}

#endregion
}
}
