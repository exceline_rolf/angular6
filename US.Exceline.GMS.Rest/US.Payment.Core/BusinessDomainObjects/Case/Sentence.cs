﻿using System;
using System.Runtime.Serialization;

namespace US.Payment.Core.BusinessDomainObjects.Case
{
    [DataContract]
    public class Sentence
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public int CaseNo { get; set; }

        [DataMember]
        public decimal CollectionFee { get; set; }

        [DataMember]
        public decimal CourtFee { get; set; }

        [DataMember]
        public decimal OtherCosts { get; set; }

        [DataMember]
        public decimal Interest { get; set; }

        [DataMember]
        public decimal CaseCost { get; set; }

        [DataMember]
        public decimal CaseInterest { get; set; }

        [DataMember]
        public DateTime FromDate { get; set; }

        [DataMember]
        public decimal MainAmount { get; set; }

        [DataMember]
        public DateTime MainamountDate { get; set; }

        [DataMember]
        public decimal MainAmountInterest { get; set; }

        [DataMember]
        public DateTime VoucherDate { get; set; }
        [DataMember]

        public DateTime VerdictPassed { get; set; }

        [DataMember]
        public DateTime TimeLimit { get; set; }

        [DataMember]
        public DateTime Caducity { get; set; }

        [DataMember]
        public int SentenceType { get; set; }

        [DataMember]
        public int Status { get; set; }
    }
}
