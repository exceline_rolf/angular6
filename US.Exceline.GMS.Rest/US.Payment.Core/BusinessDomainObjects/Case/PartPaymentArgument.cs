﻿// --------------------------------------------------------------------------
// Copyright(c) <2011> Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USPayment 
// Project Name      : US.Payment.PM.Dashboard
// Coding Standard   : US Coding Standards
// Author            : TMA
// Created Timestamp : 3/8/2012 HH:MM  PM
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace US.Payment.Core.BusinessDomainObjects.Case
{
    [DataContract]
    public class PartPaymentArgument
    {
        private DateTime _firstInstallmentDate = DateTime.Now;
        [DataMember]
        public  DateTime FirstInstallmentDate
        {
            get { return _firstInstallmentDate; }
            set { _firstInstallmentDate = value; }
        }

        private decimal _installmentAmount = 0;
        [DataMember]
        public  decimal InstallmentAmount
        {
            get { return _installmentAmount; }
            set { _installmentAmount = value; }
        }

        private int _noOfInstallment = 0;
        [DataMember]
        public  int NoOfInstallment
        {
            get { return _noOfInstallment; }
            set { _noOfInstallment = value; }
        }

        private string _sMSNo = "+xx-xx-xxxxxxx";
        [DataMember]
        public  string SMSNo
        {
            get { return _sMSNo; }
            set { _sMSNo = value; }
        }

        private int _selectedPeriod = -1;
        /// <summary>
        /// 7 - Month
        /// 8 - Year
        /// </summary>
        [DataMember]
        public  int SelectedPeriod
        {
            get { return _selectedPeriod; }
            set { _selectedPeriod = value; }
        }

        private int _caseNo = -1;
        [DataMember]
        public  int CaseNo
        {
            get { return _caseNo; }
            set { _caseNo = value; }
        }

        private int _writeToDB = 0;
         [DataMember]
        public  int WriteToDB
        {
            get { return _writeToDB; }
            set { _writeToDB = value; }
        }

        private int _timePeriodIncrement = 1;
        /// <summary>
        /// If increment is month then part payment will generate according to timeperiod increment. Default =1, that mean it will generate monthly installments
        /// If increment is yearly then part payment will generated yearly. Default =1, that mean yearly intalllments will generate.
        /// </summary>
        [DataMember]
        public  int TimePeriodIncrement
         {
             get { return _timePeriodIncrement; }
             set { _timePeriodIncrement = value; }
         }
        
    }
}
