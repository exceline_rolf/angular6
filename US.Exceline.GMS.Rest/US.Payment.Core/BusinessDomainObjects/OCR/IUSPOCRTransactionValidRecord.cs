﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects.OCR
{
    public interface IUSPOCRTransactionValidRecord : IUSPOCRTransactionRecord
    {


        IUSPOCRTransactionAmountItem1 OCRTransactionPost1
        {
            get;
            set;
        }

        
          IUSPOCRTransactionAmountItem2 OCRTransactionPost2
        {
            get;
            set;
        }

       
         int LineIndexOfFirstPart
        {
            get;
            set;

        }

         int LineIndexOfSecondPart
        {
            get;
            set;
        }

       
    }
}
