﻿// --------------------------------------------------------------------------
// Copyright(c) 2008 UnicornSolutions.
// Chandra silva mawatha,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USAR
// Project Name      : US Payment
// Coding Standard   : US Coding Standards
// Author            : MRA
// Created Timestamp : 25/11/2009 10:30  AM
// --------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects.OCR.OCRConcreteObjects
{
    public class OCRTransactionValidRecord : OCRTransactionRecord
    {
        private OCRTransactionAmountItem1 _oCRTransactionPost1;

        public OCRTransactionAmountItem1 OCRTransactionPost1
        {
            get { return _oCRTransactionPost1; }
            set { _oCRTransactionPost1 = value; }
        }

        private OCRTransactionAmountItem2 oCRTransactionPost2;

        public OCRTransactionAmountItem2 OCRTransactionPost2
        {
            get { return oCRTransactionPost2; }
            set { oCRTransactionPost2 = value; }
        }
    }
}
