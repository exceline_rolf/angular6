﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects.OCR
{
   public interface IUSPOCREndTransmissionRecord
    {
       string FormatCode
        {
            get ; 
            set ; 
        }

       string ServiceCode
        {
            get;
            set; 
        }
       string TaskType
        {
            get;
            set; 
        }

       US.Payment.Core.Enums.OCRRecordTypes RecordType
        {
            get;
            set; 
        }

        string NumberOfTrans
        {
            get;
            set; 
        }

        string NumberOfRecords
        {
            get;
            set; 
        }

       string TotalAmount
        {
            get;
            set; 
        }

        string DateOfSettlement
        {
            get;
            set; 
        }

    }
}
