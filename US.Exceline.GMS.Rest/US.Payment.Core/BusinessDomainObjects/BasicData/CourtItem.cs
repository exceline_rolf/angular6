﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USPayment
// Project Name      : US.Payment
// Coding Standard   : US Coding Standards
// Author            : GMA
// Created Timestamp : "22/08/2012 17:00:58
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System.Collections.Generic;
using System.Runtime.Serialization;
using System;

namespace US.Payment.Core.BusinessDomainObjects.BasicData
{
    [DataContract]
    public class CourtItem
    {
        private int _id = -1;
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        private int _typeNo=-1;
        [DataMember]
        public int TypeNo
        {
          get { return _typeNo; }
          set { _typeNo = value; }
        }
        private string _name = string.Empty;
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        private string _address1 = string.Empty;
        [DataMember]
        public string Address1
        {
            get { return _address1; }
            set { _address1 = value; }
        }
        private string _address2 = string.Empty;
        [DataMember]
        public string Address2
        {
            get { return _address2; }
            set { _address2 = value; }
        }
        private PostalArea _postalArea;
        [DataMember]
        public PostalArea PostalArea
        {
            get { return _postalArea; }
            set { _postalArea = value; }
        }
        private string _countryCode=string.Empty;
        [DataMember]
        public string CountryCode
        {
          get { return _countryCode; }
          set { _countryCode = value; }
        }
        private string _telephoneNumber = string.Empty;
        [DataMember]
        public string TelephoneNumber
        {
            get { return _telephoneNumber; }
            set { _telephoneNumber = value; }
        }
        private string _bankAccountNo = string.Empty;
        [DataMember]
        public string BankAccountNo
        {
            get { return _bankAccountNo; }
            set { _bankAccountNo = value; }
        }
        private string _postAccoutNo = string.Empty;
        [DataMember]
        public string PostAccoutNo
        {
            get { return _postAccoutNo; }
            set { _postAccoutNo = value; }
        }
        private string _user=string.Empty;
        [DataMember]
        public string User
        {
          get { return _user; }
          set { _user = value; }
        }
        private DateTime _regDate = DateTime.MinValue;
        [DataMember]
        public DateTime RegDate
        {
            get { return _regDate; }
            set { _regDate = value; }
        }

         private string _postalCode = string.Empty;
         [DataMember]
        public string PostalCode
        {
            get { return _postalCode; }
            set { _postalCode = value; }
        }
        private List<Municipality> _municipilityList = new List<Municipality>();
        [DataMember]
        public List<Municipality> MunicipilityList
        {
            get { return _municipilityList; }
            set { _municipilityList = value; }
        }
    }
}
