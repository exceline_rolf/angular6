﻿using System.Collections.Generic;

namespace US.Payment.Core.BusinessDomainObjects
{
    public class USPInternalPerson
    {
        private string name = string.Empty;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private string _RoleID = string.Empty;
        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }

        private string _Born = string.Empty;
        public string Born
        {
            get { return _Born; }
            set { _Born = value; }
        }

        private int _AddressNo;
        public int AddressNo
        {
            get { return _AddressNo; }
            set { _AddressNo = value; }
        }



        private int entNo;
        public int EntNo
        {
            get { return entNo; }
            set { entNo = value; }
        }


        private int entRoleId;
        public int EntRoleId
        {
            get { return entRoleId; }
            set { entRoleId = value; }
        }


        private string personNo = string.Empty;
        public string PersonNo
        {
            get { return personNo; }
            set { personNo = value; }
        }

        private string notification = string.Empty;
        public string Notification
        {
            get { return notification; }
            set { notification = value; }
        }       


        private List<USPAddress> address;
        public List<USPAddress> Address
        {
            get { return address; }
            set { address = value; }
        }
    }
}
