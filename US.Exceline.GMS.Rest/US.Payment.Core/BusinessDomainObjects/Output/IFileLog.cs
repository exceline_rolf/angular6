﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.BusinessDomainObjects.Output
{
   public interface IFileLog
    {
         int FileId { get; set; }
         string FileName { get; set; }
         string FileType { get; set; }
         string INOUT { get; set; }

         int FileDirection { get; set; }
         string SourcePath { get; set; }
         string DestinationPath { get; set; }
         string ProcessPath { get; set; }
         int Status { get; set; }
         DateTime SourceTime { get; set; }
         DateTime DestinationTime { get; set; }
         DateTime ScheduleTime { get; set; }
         DateTime ProcessTime { get; set; }
         string User { get; set; }
         string UserProcess { get; set; }
         string Message1 { get; set; }
         string Message2 { get; set; }
    }
}
