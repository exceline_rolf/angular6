﻿// --------------------------------------------------------------------------
// Copyright(c) <2011> Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USPayment
// Project Name      : US.Payment.WorkflowController.DomainObjects.Activity
// Coding Standard   : US Coding Standards
// Author            : AME
// Created Timestamp : 24/06/2011 HH:MM  PM
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.Workflows
{
    public class Parameter
    {
        private string _name = string.Empty;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private DBTypes _type;
        public DBTypes Type
        {
            get { return _type; }
            set { _type = value; } 
        }

        private ParameterValue _value = null;
        public ParameterValue Value
        {
            get { return _value; }
            set { _value = value; }
        }

    }
}
