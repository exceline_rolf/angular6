﻿// --------------------------------------------------------------------------
// Copyright(c) <2011> Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : USPayment
// Project Name      : US.Payment.WorkflowController.DomainObjects.Activity
// Coding Standard   : US Coding Standards
// Author            : AME
// Created Timestamp : 24/06/2011 HH:MM  PM
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Payment.Core.Workflows
{
    public class USEntity
    {
        private List<USActivity> _activityList = new List<USActivity>();
        public List<USActivity> ActivityList
        {
            get { return _activityList; }
            set { _activityList = value; }
        }
    }
}
