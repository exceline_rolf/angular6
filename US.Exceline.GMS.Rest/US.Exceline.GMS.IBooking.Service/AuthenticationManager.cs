﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using US.GMS.Core.Utils;

namespace US.Exceline.GMS.IBooking.Service
{
    public class AuthenticationManager
    {
        /*
        public static bool Authenticate(string userName, string password)
        {
            if (userName == "IExceline" && password == "1qaz2wsx@")
                return true;
            else
                return false;
        }
        */

        public static bool Authenticate(string endPoint, string apiKey, string systemId, string gymid)
        {
            var result = GetAPIAccess(endPoint, apiKey, systemId, gymid);
            if (result == "1")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool Authenticate(string endPoint, string apiKey, string systemId)
        {
            var result = GetAPIAccess(endPoint, apiKey, systemId);
            if (result == "1")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static string GetAPIAccess(string endPoint, string apiKey, string systemId, string gymid)
        {
            var action = new GetAPIAccessAction(endPoint, apiKey, systemId, gymid);
            return action.Execute(US_DataAccess.EnumDatabase.WorkStation);
        }

        public static string GetAPIAccess(string endPoint, string apiKey, string systemId)
        {
            var action = new GetAPIAccessAction(endPoint, apiKey, systemId);
            return action.Execute(US_DataAccess.EnumDatabase.WorkStation);
        }
    }
}