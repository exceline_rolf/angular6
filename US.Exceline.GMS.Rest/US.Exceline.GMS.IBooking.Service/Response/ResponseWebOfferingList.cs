﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.IBooking;

namespace US.Exceline.GMS.IBooking.Service.Response
{
    [DataContract]
    public class ResponseWebOfferingList
    {
        public ResponseWebOfferingList() { }
        public ResponseWebOfferingList(List<ExceIBookingWebOffering> webOfferingList, ResponseStatus status)
        {
            WebOfferings = webOfferingList;
            Status = status;
        }

        [DataMember(Order = 1)]
        public List<ExceIBookingWebOffering> WebOfferings { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}