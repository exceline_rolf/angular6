﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using US.GMS.Core.DomainObjects.IBooking;

namespace US.Exceline.GMS.IBooking.Service.Response
{
    [DataContract]
    public class ResponseMemberModifiedList
    {
        public ResponseMemberModifiedList() { }

        public ResponseMemberModifiedList(List<ExceIBookingModifiedMember> modifiedMemberList, ResponseStatus status)
        {
            Members = modifiedMemberList;
            Status = status;
        }
        [DataMember(Order = 1)]
        public List<ExceIBookingModifiedMember> Members { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}