﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using US.GMS.Core.DomainObjects.IBooking;
using System.Runtime.Serialization;

namespace US.Exceline.GMS.IBooking.Service.Response
{
    [DataContract]
    public class ResponseClassLevelList
    {
        public ResponseClassLevelList() { }
        public ResponseClassLevelList(List<ExceIBookingClassLevel> classLevelList, ResponseStatus status)
        {
            this.ClassLevels = classLevelList;
            this.Status = status;
        }

        [DataMember(Order=1)]
        public List<ExceIBookingClassLevel> ClassLevels { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}