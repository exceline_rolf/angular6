﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.IBooking;

namespace US.Exceline.GMS.IBooking.Service.Response
{
    [DataContract]
    public class ResponseBranchList
    {
        public ResponseBranchList() { }
        public ResponseBranchList(List<ExceIBookingGym> gyms, ResponseStatus status)
        {
            Gyms = gyms;
            Status = status;
        }

        [DataMember(Order=1)]
        public List<ExceIBookingGym> Gyms { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}