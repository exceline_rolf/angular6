﻿
using System.Collections.Generic;
using System.Runtime.Serialization;
using US.GMS.Core.DomainObjects.IBooking;

namespace US.Exceline.GMS.IBooking.Service.Response
{
    [DataContract]
    public class ResponseMemberGymDetailList
    {
        public ResponseMemberGymDetailList() { }
        public ResponseMemberGymDetailList(List<ExceIBookingMemberGymDetail> memberGymList, ResponseStatus status)
        {
            MembersGyms = memberGymList;
            Status = status;
        }

        [DataMember(Order=1)]
        public List<ExceIBookingMemberGymDetail> MembersGyms { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}