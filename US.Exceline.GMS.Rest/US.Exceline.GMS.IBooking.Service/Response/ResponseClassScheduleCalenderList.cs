﻿using System.Collections.Generic;
using US.GMS.Core.DomainObjects.IBooking;
using System.Runtime.Serialization;

namespace US.Exceline.GMS.IBooking.Service.Response
{
    [DataContract]
    public class ResponseClassScheduleCalenderList
    {
        public ResponseClassScheduleCalenderList() { }
        public ResponseClassScheduleCalenderList(List<ExceIBookingClassCalendar> classes, ResponseStatus status)
        {
            Classes = classes;
            Status = status;
        }

        [DataMember(Order = 1)]
        public List<ExceIBookingClassCalendar> Classes { get; set; }
        [DataMember(Order = 2)]
        public ResponseStatus Status { get; set; }
    }
}