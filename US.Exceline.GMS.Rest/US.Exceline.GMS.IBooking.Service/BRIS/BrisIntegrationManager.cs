﻿using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using US.Exceline.GMS.IBooking.API;
using US.Exceline.GMS.IBooking.Core;
using US.GMS.Core.DomainObjects.IBooking;
using US.GMS.Core.DomainObjects.ManageMemberships;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.IBooking.Service.BRIS
{
    public class BrisIntegrationManager
    {
        private BrukereOgBrukerskApi _brisInstance;
        private SkrivBrukerdataApi _skrivBrukerdataApi = null;
        private ITjenesteResourceApi _iTjenesteResourceApi = null;

        public bool SaveBrisMember(string apiKey, ExceIBookingNewMember member, int memberId, string gymCode)
        {
            //TODO
            try
            {
                var brukerDto = ConvertToBRISMember(member);
                var brukerKlientRequestDto = new BrukerKlientRequestDto(brukerDto);
                var response = _skrivBrukerdataApi.UpdateBruker(apiKey, brukerKlientRequestDto);
                BrukerDto root = JsonConvert.DeserializeObject<BrukerDto>(response.ToJson());
                GMSExcelineIBooking.AddStatus(member, 0, "BRIS Result " + response.ToString(), gymCode);

                if (!(root.AgressoId.Value > 0))
                {
                    root.AgressoId = GetAggressoID(root.BrukerId.Value, apiKey);
                }

                var brisMember = new OrdinaryMemberDC();
                brisMember.Id = memberId;
                brisMember.UserId = root.BrukerId.Value;
                brisMember.Ssn = root.Fnr;
                brisMember.Language = root.Sprak;
                brisMember.School = root.StudiestedNavn;
                brisMember.LastPaidSemesterFee = root.SemesteravgiftKode;
                brisMember.AgressoId = root.AgressoId;

                return SaveBrisMemberData(member, brisMember, gymCode);
            }
            catch (Exception ex)
            {
                GMSExcelineIBooking.AddStatus(member, 0, "BRIS Error " + ex.Message, gymCode);
                return false;
            }
        }

        private BrukerDto ConvertToBRISMember(ExceIBookingNewMember ibookingMember)
        {
            var brukerDto = new BrukerDto();
            brukerDto.Fornavn = ibookingMember.FirstName;
            brukerDto.Etternavn = ibookingMember.LastName;
            brukerDto.Mobiltelefon = ibookingMember.Mobile;
            brukerDto.Mobilprefiks = ibookingMember.CountryCode;
            brukerDto.Kjonn = ibookingMember.Gender == "FEMALE" ? 1 : 0;
            brukerDto.Fodselsdato = DateTime.ParseExact(ibookingMember.BirthDate,"yyyy.MM.dd",CultureInfo.InvariantCulture);
            brukerDto.StudiestedNavn = string.Empty;
            brukerDto.Epostadresse = ibookingMember.Email; // TODO
            brukerDto.Aktiv = true;
            brukerDto.SemesteravgiftKode = string.Empty;
            brukerDto.Fnr = string.Empty;
            return brukerDto;
        }

        public BrukerDto SearchMembers(int brisID, string apiKey)
        {
            _brisInstance = new BrukereOgBrukerskApi();
            var body = new BrukerSokBeanDto //search files are ok 
            {
                Brukerid = brisID.ToString(),
                Fornavn = string.Empty,
                Etternavn = string.Empty,
                Epostadresse = string.Empty,
                Mobiltelefon = string.Empty,
                Fodselsdato = string.Empty,
                Kortnummer = string.Empty,
                Personnummer = string.Empty
            };

            var response = _brisInstance.Search(apiKey, body);
            var root = JsonConvert.DeserializeObject<BRISSearchData>(response.ToJson());

            BrukerDto result = root.Results.ToList().FirstOrDefault();
            return result;
        }

        public int GetAggressoID(int bridId, string apiKey)
        {
            if (_iTjenesteResourceApi != null)
            {
                _iTjenesteResourceApi = new TjenesteResourceApi();
            }
           ApiResponse<AgressoOppslagResponse> aggressoResult = _iTjenesteResourceApi.GetAgressoIdWithHttpInfo(apiKey, bridId);
           return aggressoResult.Data.AgressoId.Value;
        }

        private bool SaveBrisMemberData(ExceIBookingNewMember IbookingMember, OrdinaryMemberDC member, string gymCode)
        {
            OperationResult<bool> result = GMSExcelineIBooking.UpdateBRISData(member, gymCode);
            if (result.ErrorOccured)
            {
                if (result.Notifications.Count > 0)
                {
                    GMSExcelineIBooking.AddStatus(IbookingMember,0, "Bris Member update Error : " +  result.Notifications[0].Message, gymCode);
                }
                return false;
            }
            return true;
        }

        public bool UpdateBrisData(ExceIBookingNewMember IbookingMember, BrukerDto brisMember, int memberID,  string gymCode)
        {
            var member = new OrdinaryMemberDC();
            member.Id = memberID;
            member.UserId = brisMember.BrukerId.Value;
            member.Ssn = brisMember.Fnr;
            member.Language = brisMember.Sprak;
            member.School = brisMember.StudiestedNavn;
            member.LastPaidSemesterFee = brisMember.SemesteravgiftKode;
            member.AgressoId = brisMember.AgressoId;
            return SaveBrisMemberData(IbookingMember, member, gymCode);
        }
    }
}