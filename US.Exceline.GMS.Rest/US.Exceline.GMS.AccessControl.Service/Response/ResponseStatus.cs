﻿using System.Runtime.Serialization;

namespace US.Exceline.GMS.AccessControl.Service.Response
{
    [DataContract]
    public class ResponseStatus
    {
        public ResponseStatus() { }

        public ResponseStatus(int statusCode, string statusMessage)
        {
            StatusCode = statusCode;
            StatusMessage = statusMessage;
        }

        [DataMember(Order = 1)]
        public int StatusCode { get; set; }
        [DataMember(Order = 1)]
        public string StatusMessage { get; set; }
    }
}