﻿using System;
using System.Runtime.Serialization;

namespace US.Exceline.GMS.Modules.Economy.Core.DomainObjects
{
    [DataContract]
    public class EconomyFileLogData
    {
        private DateTime _date;
        [DataMember]
        public DateTime Date
        {
            get { return _date; }
            set { _date = value; }
        }

        private string _uniqueID;
        [DataMember]
        public string UniqueID
        {
            get { return _uniqueID; }
            set { _uniqueID = value; }
        }

        private string _type;
        [DataMember]
        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }

        private string _typeCode;
        [DataMember]
        public string TypeCode
        {
            get { return _typeCode; }
            set { _typeCode = value; }
        }

        private decimal _totalDeduction;
        [DataMember]
        public decimal TotalDeduction
        {
            get { return _totalDeduction; }
            set { _totalDeduction = value; }
        }

        private string _fileName;
        [DataMember]
        public string FileName
        {
            get { return _fileName; }
            set { _fileName = value; }
        }

        private int _numberOfDeviations;
        [DataMember]
        public int NumberOfDeviations
        {
            get { return _numberOfDeviations; }
            set { _numberOfDeviations = value; }
        }

        private DateTime? _dateOfFirstOpening;
        [DataMember]
        public DateTime? DateOfFirstOpening
        {
            get { return _dateOfFirstOpening; }
            set { _dateOfFirstOpening = value; }
        }

        private string _status;
        [DataMember]
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        private string _batchID;
        [DataMember]
        public string BatchID
        {
            get { return _batchID; }
            set { _batchID = value; }
        }

        private bool _isDeviationLogVisible;
        [DataMember]
        public bool IsDeviationLogVisible
        {
            get { return _isDeviationLogVisible; }
            set { _isDeviationLogVisible = value; }
        }

        private string _kid;
        [DataMember]
        public string KID
        {
            get { return _kid; }
            set { _kid = value; }
        }
    }
}
