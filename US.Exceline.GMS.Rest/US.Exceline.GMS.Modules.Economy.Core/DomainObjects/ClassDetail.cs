﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.DomainObjects
{
    [DataContract]
    public class ClassDetail
    {
        private string _companyId;
        [DataMember]
        public string CompanyId
        {
            get { return _companyId; }
            set { _companyId = value; }
        }

        private string _payrollNumber = string.Empty;
        [DataMember]
        public string PayrollNumber
        {
            get { return _payrollNumber; }
            set { _payrollNumber = value; }
        }

        private string _categoryId;
        [DataMember]
        public string CategoryId
        {
            get { return _categoryId; }
            set { _categoryId = value; }
        }



        private string _artNumber = string.Empty;
        [DataMember]
        public string ArtNumber
        {
            get { return _artNumber; }
            set { _artNumber = value; }
        }

        private string _timer = string.Empty;
        [DataMember]
        public string Timer
        {
            get { return _timer; }
            set { _timer = value; }
        }

        private DateTime? _startDate;
        [DataMember]
        public DateTime? StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }








    }
}
