﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR
{
    public class OCRErrorRecord : OCRTransactionRecord
    {
        private string _errorMessage;

        public OCRErrorRecord(string errorMsg)
        {
            _errorMessage = errorMsg;
        }
        public string ErrorMessage
        {
            get { return _errorMessage; }
        }
    }
}
