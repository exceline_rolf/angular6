﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR
{
    public interface IUSPOCRAssignmentRecord
    {
        OCRRecordTypes RecordTypeOfFirstRecord
        {
            get;
            set;
        }
        IUSPOCRStartAssignmentRecord StartAssignmentRecord
        {
            get;
            set;
        }

        List<IUSPOCRTransactionRecord> TransactionRecords
        {
            get;
            set;
        }

        IUSPOCREndAssignmentRecord EndAssignmentRecord
        {
            get;
            set;
        }
    }
}
