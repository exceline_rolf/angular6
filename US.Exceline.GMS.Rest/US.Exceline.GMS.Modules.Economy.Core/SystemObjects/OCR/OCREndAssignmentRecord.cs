﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR
{
    public class OCREndAssignmentRecord : IUSPOCREndAssignmentRecord
    {
        public string FormatCode
        {
            get;
            set;
        }

        public string ServiceCode
        {
            get;
            set;
        }

        public string TaskType
        {
            get;
            set;
        }

        public OCRRecordTypes RecordType
        {
            get;
            set;
        }

        public string NumberOfTrans
        {
            get;
            set;
        }

        public string NumberOfRecords
        {
            get;
            set;
        }

        public string TotalAmount
        {
            get;
            set;
        }

        public string DateOfSettlement
        {
            get;
            set;
        }

        public string FirstDate
        {
            get;
            set;
        }

        public string LastDate
        {
            get;
            set;
        }
    }
}
