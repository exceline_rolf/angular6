﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects.OCR
{
    public class FileValidationStatus : IFileValidationStatus
    {
        private bool _Status = false;
        public bool Status
        {
            get
            {
                return _Status;
            }
            set
            {
                _Status = value;
            }
        }

        private List<IMessage> _Mesages = new List<IMessage>();
        public List<IMessage> Mesages
        {
            get
            {
                return _Mesages;
            }
            set
            {
                _Mesages = value;
            }
        }

        private bool _MoveToFail = true;
        public bool MoveToFail
        {
            get
            {
                return _MoveToFail;
            }
            set
            {
                _MoveToFail = value;
            }
        }

        public string FileName { get; set; }
        public string UpdatedFileName { get; set; }
        public int FileID { get; set; }
    }
}
