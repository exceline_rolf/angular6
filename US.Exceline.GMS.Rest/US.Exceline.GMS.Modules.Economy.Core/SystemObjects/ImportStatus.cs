﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public class ImportStatus
    {
        private int _id = -1;
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private DateTime? _regDate;
        public DateTime? RegDate
        {
            get { return _regDate; }
            set { _regDate = value; }
        }

        private string _recordType = string.Empty;
        public string RecordType
        {
            get { return _recordType; }
            set { _recordType = value; }
        }

        private string _pid = string.Empty;
        public string Pid
        {
            get { return _pid; }
            set { _pid = value; }
        }

        private string _bNumber = string.Empty;
        public string BNumber
        {
            get { return _bNumber; }
            set { _bNumber = value; }
        }

        private string _cid = string.Empty;
        public string Cid
        {
            get { return _cid; }
            set { _cid = value; }
        }

        private string _tNumber = string.Empty;
        public string TNumber
        {
            get { return _tNumber; }
            set { _tNumber = value; }
        }

        private decimal _amount = 0;
        public decimal Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

        private string _kid = string.Empty;
        public string KID
        {
            get { return _kid; }
            set { _kid = value; }
        }

        private string _remitDate = string.Empty;
        public string RemitDate
        {
            get { return _remitDate; }
            set { _remitDate = value; }
        }

        private int _referanceid = -1;
        public int Referanceid
        {
            get { return _referanceid; }
            set { _referanceid = value; }
        }

        private string _batchId = string.Empty;
        public string BatchId
        {
            get { return _batchId; }
            set { _batchId = value; }
        }

        private string _dataSource = string.Empty;
        public string DataSource
        {
            get { return _dataSource; }
            set { _dataSource = value; }
        }

        private string _fileName = string.Empty;
        public string FileName
        {
            get { return _fileName; }
            set { _fileName = value; }
        }

        private int _fileId = -1;
        public int FileId
        {
            get { return _fileId; }
            set { _fileId = value; }
        }
    }
}
