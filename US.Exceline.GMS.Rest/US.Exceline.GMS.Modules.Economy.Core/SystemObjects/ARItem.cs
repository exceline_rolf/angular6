﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public class ARItem
    {



        public ARItem()
        {
            _invoNo = string.Empty;
            Text = string.Empty;
            InvoicedDate = string.Empty;
            DueDate = string.Empty;
            Amount = 0;
            ARNo = 0;
            itemType = string.Empty;
            Ref = string.Empty;
            RegDate = string.Empty;
            RFee = string.Empty;
            Balance = string.Empty;
            KID = string.Empty;
            Delayed = 0;
            ARRole = string.Empty;
            CreEnt = 0;
            DebEnt = 0;
            _isPrinted = 0;
            Paid = string.Empty;
            incasso = string.Empty;
            cusID = string.Empty;
            subCaseNo = 0;
            PaymentId = 0;
            ExternalTarnsactionNumber = string.Empty;
            ARItemNo = 0;
            DStatus = 0;
            ItemTypeID = 0;
            Notified = 0;
            CancelDate = string.Empty;
            payDate = string.Empty;
            TransferDate = string.Empty;
            ExtTransNo = string.Empty;
            isValidCancel = false;
            _invoPath = string.Empty;
            _printDate = string.Empty;
            NoOfTotalInvoices = 0;
            NoOfLostInvoices = 0;
            NoOFNewInvoices = 0;
            OrderLineList = new List<OrderLine>();
            _contractKID = string.Empty;


        }

        private string _contractKID;


        public string ContractKID
        {
            get { return _contractKID; }
            set { _contractKID = value; }
        }

        private string fileName = string.Empty;

        public string FileName
        {
            get { return fileName; }
            set { fileName = value; }
        }
        /// <summary>
        /// Invoice number
        /// </summary>
        private string _invoNo;

        public string InvoiceNumber
        {
            get { return _invoNo; }
            set { _invoNo = value; }
        }

        private int _isPrinted = 0;
        public int IsPrinted
        {
            get { return _isPrinted; }
            set { _isPrinted = value; }
        }

        private string _printDate;

        public string PrintDate
        {
            get { return _printDate; }
            set { _printDate = value; }
        }

        private string _invoPath;

        public string InvoicePath
        {
            get { return _invoPath; }
            set { _invoPath = value; }
        }

        /// <summary>
        /// Discreption text for the transaction
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Date that issued the invoice
        /// </summary>
        public string InvoicedDate { get; set; }

        /// <summary>
        /// Last date that payable period of invoice
        /// </summary>
        public string DueDate { get; set; }

        /// <summary>
        /// Payable ammount
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// ARNo
        /// </summary>
        public int ARNo { get; set; }

        /// <summary>
        /// Itemtype
        /// </summary>
        public string itemType { get; set; }

        public string transType { get; set; }

        public string Ref { get; set; }
        public string RegDate { get; set; }
        public string RFee { get; set; }
        public string Balance { get; set; }
        public string KID { get; set; }
        public int Delayed { get; set; }
        public string ARRole { get; set; }
        public int CreEnt { get; set; }
        public int DebEnt { get; set; }
        public string Paid { get; set; }
        public string incasso { get; set; }
        public string cusID { get; set; }
        public int subCaseNo { get; set; }
        public int PaymentId { get; set; }
        public string ExternalTarnsactionNumber { get; set; }
        public int CaseNo { get; set; }
        public int ARItemNo { get; set; }
        public int DStatus { get; set; }
        public int ItemTypeID { get; set; }
        public int Notified { get; set; }
        public string CancelDate { get; set; }
        public string payDate { get; set; }
        public string TransferDate { get; set; }
        public string ExtTransNo { get; set; }

        public bool IsErrorOcurred { get; set; }
        public string ErrorMessage { get; set; }
        public bool isValidCancel { get; set; }
        public int NoOfTotalInvoices { get; set; }
        public int NoOfLostInvoices { get; set; }
        public int NoOFNewInvoices { get; set; }
        public List<OrderLine> OrderLineList { get; set; }


    }
}
