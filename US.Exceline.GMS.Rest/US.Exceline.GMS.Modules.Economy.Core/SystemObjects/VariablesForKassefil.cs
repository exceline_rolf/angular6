﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    [DataContract]
    public class VariablesForKassefil
    {
        private string _BranchName;
        [DataMember]
        public string BranchName
        {
            get { return _BranchName; }
            set { _BranchName = value; }
        }

        private string _VoucherDate;
        [DataMember]
        public string VoucherDate
        {
            get { return _VoucherDate; }
            set { _VoucherDate = value; }
        }

        private string _BranchId;
        [DataMember]
        public string BranchId
        {
            get { return _BranchId; }
            set { _BranchId = value; }
        }
        private string _CompanyIdSalary;
        [DataMember]
        public string CompanyIdSalary
        {
            get { return _CompanyIdSalary; }
            set { _CompanyIdSalary = value; }
        }
        private string _AccountNo;
        [DataMember]
        public string AccountNo
        {
            get { return _AccountNo; }
            set { _AccountNo = value; }
        }
        private string _AccountName;
        [DataMember]
        public string AccountName
        {
            get { return _AccountName; }
            set { _AccountName = value; }
        }
        private string _CategoryId;
        [DataMember]
        public string CategoryId
        {
            get { return _CategoryId; }
            set { _CategoryId = value; }
        }
        private string _CategoryName;
        [DataMember]
        public string CategoryName
        {
            get { return _CategoryName; }
            set { _CategoryName = value; }
        }
        private string _VATRate;
        [DataMember]
        public string VATRate
        {
            get { return _VATRate; }
            set { _VATRate = value; }
        }
        private string _TotalSum;
        [DataMember]
        public string TotalSum
        {
            get { return _TotalSum; }
            set { _TotalSum = value; }
        }
        private string _IsPayment;
        [DataMember]
        public string IsPayment
        {
            get { return _IsPayment; }
            set { _IsPayment = value; }
        }




    }
}