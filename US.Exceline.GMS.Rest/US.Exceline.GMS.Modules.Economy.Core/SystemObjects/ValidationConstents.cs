﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public static class ValidationConstents
    {
        public const string TEXT_LINE_NO_BEGIN = "[Line No :";
        public const string TEXT_LINE_NO_END = "] ";
        public const string ERROR_DEBTOR_NAME_EMPTY = "Debtor name is empty";
        public const string ERROR_DEBTOR_CUST_ID_EMPTY = "Debtor CustID is empty";
        public const string ERROR_Creditor_INKASSO_ID_EMPTY = "Creditor InkassoID is empty";
        public const string ERROR_DEBT_WARNING_RECORD_REF_EMPTY = "Debtor InkassoID is empty";
        public const string ERROR_DEBTOR_ADDRESS_CANNOT_READ = "Debtor Address cannot be read";

        public const string ERROR_EXLINE_DIRECT_PAYMENT_CUS_ID_EMPTY = "CustId is empty";
        public const string ERROR_EXLINE_DIRECT_PAYMENT_REF_EMPTY = "Ref is empty";

        public const string SHORT_TEXT_RECORD_TYPE_EXLINE_INVOICE = "IN";
        public const string SHORT_TEXT_RECORD_TYPE_EXLINE_DIRECT_DEDUCT = "DD";
        public const string SHORT_TEXT_RECORD_TYPE_EXLINE_DEBT_WARNING = "DW";
        public const string SHORT_TEXT_RECORD_TYPE_EXLINE_INVOICE_FOR_PRINT = "IP";
        public const string SHORT_TEXT_RECORD_TYPE_EXLINE_SPONSER = "SP";
        public const string SHORT_TEXT_RECORD_TYPE_EXLINE_PAYMENT_PRINT = "PP";
        public const string SHORT_TEXT_RECORD_TYPE_EXLINE_UNRECOGNIZED = "EXLINE UNRECOGNIZED";

        public const string SHORT_TEXT_RECORD_TYPE_EXLINE_DIRECT_PAYMENT_SM = "SM";
        public const string SHORT_TEXT_RECORD_TYPE_EXLINE_DIRECT_PAYMENT_DIRECT_PAYMENT = "DP";
        public const string SHORT_TEXT_RECORD_TYPE_EXLINE_DIRECT_PAYMENT_UNRECOGNIZED = "EXLINE DIRECT PAYMENT UNRECOGNIZED";

        public const string VALIDATION_STATUS_VALIDATED = "Validated";
        public const string VALIDATION_STATUS_INVALID = "Invalid";
        public const string VALIDATION_STATUS_IMPORTED = "Imported";
        public const string VALIDATION_STATUS_EXPORTED = "Exported";
        public const string VALIDATION_STATUS_FAILED = "Failed";
        public const string VALIDATION_STATUS_ABORTED = "Aborted";

        public const string SHORT_TEXT_RECORD_TYPE_OCR_UNRECOGNIZED = "OCR UNRECOGNIZED";
        public const string SHORT_TEXT_RECORD_TYPE_OCR_TRANS_1 = "OCR Trans 1";
        public const string SHORT_TEXT_RECORD_TYPE_OCR_TRANS_2 = "OCR Trans 2";
        public const string SHORT_TEXT_RECORD_TYPE_OCR_NEW_CANCELL = "OCR New Cancell";

        public const string SHORT_TEXT_RECORD_TYPE_SOR_TRANS = "SOR Trans";
        public const string SHORT_TEXT_RECORD_TYPE_SOR_UNRECOGNIZED = "SOR UNRECOGNIZED";
    }
}
