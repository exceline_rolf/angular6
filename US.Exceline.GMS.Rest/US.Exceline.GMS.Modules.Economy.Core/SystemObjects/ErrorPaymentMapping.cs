﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public class ErrorPaymentMapping
    {
        private int _errorPaymentId = -1;

        public int ErrorPaymentId
        {
            get { return _errorPaymentId; }
            set { _errorPaymentId = value; }
        }
        private int _aRItemNumber = -1;

        public int ARItemNumber
        {
            get { return _aRItemNumber; }
            set { _aRItemNumber = value; }
        }
    }
}
