﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public class ExlineDPValidationRecord
    {
        public bool HasCriticalError { get; set; }

        public string CreditorNo { get; set; }

        public string CriticalErrorMessage { get; set; }

        /// <summary>
        /// Create Direct payment validation record with given line index
        /// </summary>
        /// <param name="lineIndex">use -1 for if the validation record is not related with any line number</param>
        public ExlineDPValidationRecord(int lineIndex)
        {
            LineIndex = lineIndex;
            IsValidRecord = true;
            HasCriticalError = false;
        }

        public decimal Amount { get; set; }

        public int LineIndex { get; set; }

        public DPValidationRecordTypes ValidationRecordType { get; set; }

        public bool IsValidRecord { get; set; }

        public string ErrorMessage { get; set; }

    }
}
