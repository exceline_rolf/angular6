﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public interface IUSPStandingOrderItem
    {
        string CreName { get; set; }
        string DueDate { get; set; }
        string Amount { get; set; }
        string ContractNo { get; set; }
        string KID { get; set; }
        string aRitem { get; set; }
        string Message { get; set; }
    }
}
