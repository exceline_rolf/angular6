﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.US.Payment.Core.BusinessDomainObjects;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public interface IUSPCreditor
    {
        string CreditorInkassoID { get; set; }// Exline File field ID - 2
        string CreditorName { get; set; }
        string CreditorAccountNo { get; set; }
        int CreditorEntityID { get; set; }
        int CreditorEntityRoleID { get; set; }
        string CreditorPersonNo { get; set; }
        int CreditorDebtorCount { get; set; }
        List<IUSPAddress> CreditorAddress { get; set; }
        string CreditorBirthDay { get; set; }

        List<USPFileLog> CreditorFilesToPrint { get; set; }

        int CreditorPrintAllow { get; set; }


    }
}
