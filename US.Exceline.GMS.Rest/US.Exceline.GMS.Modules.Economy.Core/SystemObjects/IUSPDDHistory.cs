﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public interface IUSPDDHistory
    {
        string FileName { get; set; }
        string OrderId { get; set; }
        string TransactionID { get; set; }
        string Cancellatrion { get; set; }
        int ArNo { get; set; }
    }
}
