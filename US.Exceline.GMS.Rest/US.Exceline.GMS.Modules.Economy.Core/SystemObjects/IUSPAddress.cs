﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public interface IUSPAddress
    {
        int addrNo { get; set; }
        string Address1 { get; set; }// Exline File field ID - 7
        string Address2 { get; set; }
        string Address3 { get; set; }
        string ZipCode { get; set; }// Exline File field ID - 8
        string CountryCode { get; set; }
        string AddSource { get; set; }
        string City { get; set; }// Exline File field ID - 9
        string TelMobile { get; set; }// Exline File field ID - 13
        string TelWork { get; set; }// Exline File field ID - 12
        string TelHome { get; set; }// Exline File field ID - 11
        string Email { get; set; }// Exline File field ID - 14

        string Fax { get; set; }

        string MSN { get; set; }

        string Skype { get; set; }

        string SMS { get; set; }

        bool IsDefault { get; set; }

        string AddressName { get; set; }

        string Branch { get; set; }

        string PostCode { get; set; }

        string Municipality { get; set; }

    }
}
