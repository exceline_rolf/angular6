﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public class OrderLine
    {
        public OrderLine()
        {
            _orderLine = 0;
            _arItemNo = 0;
            _inkassoId = string.Empty;
            _cutId = string.Empty;
            _lastName = string.Empty;
            _firstName = string.Empty;
            _nameOnContract = string.Empty;
            _address = string.Empty;
            _zipCode = string.Empty;
            _postPlace = string.Empty;
            _discountAmount = string.Empty;
            _employeeNo = string.Empty;
            _sponsorRef = string.Empty;
            _visit = 0;
            _lastVisit = string.Empty;
            _amount = string.Empty;
            _amountThisOrderLine = string.Empty;
            _sourceInstallmentID = -1;

        }

        private int _orderLine;

        public int OrderLineID
        {
            get { return _orderLine; }
            set { _orderLine = value; }
        }
        private int _arItemNo;

        public int ArItemNo
        {
            get { return _arItemNo; }
            set { _arItemNo = value; }
        }
        private string _inkassoId;

        public string InkassoId
        {
            get { return _inkassoId; }
            set { _inkassoId = value; }
        }
        private string _cutId;

        public string CutId
        {
            get { return _cutId; }
            set { _cutId = value; }
        }
        private string _lastName;

        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }
        private string _firstName;

        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }
        private string _nameOnContract;

        public string NameOnContract
        {
            get { return _nameOnContract; }
            set { _nameOnContract = value; }
        }
        private string _address;

        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }
        private string _zipCode;

        public string ZipCode
        {
            get { return _zipCode; }
            set { _zipCode = value; }
        }
        private string _postPlace;

        public string PostPlace
        {
            get { return _postPlace; }
            set { _postPlace = value; }
        }
        private string _discountAmount;

        public string DiscountAmount
        {
            get { return _discountAmount; }
            set { _discountAmount = value; }
        }
        private string _employeeNo;

        public string EmployeeNo
        {
            get { return _employeeNo; }
            set { _employeeNo = value; }
        }
        private string _sponsorRef;

        public string SponsorRef
        {
            get { return _sponsorRef; }
            set { _sponsorRef = value; }
        }
        private int _visit;

        public int Visit
        {
            get { return _visit; }
            set { _visit = value; }
        }
        private string _lastVisit;

        public string LastVisit
        {
            get { return _lastVisit; }
            set { _lastVisit = value; }
        }
        private string _amount;

        public string Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }
        private string _amountThisOrderLine;

        public string AmountThisOrderLines
        {
            get { return _amountThisOrderLine; }
            set { _amountThisOrderLine = value; }
        }
        private string _ErrorMessage;

        public string ErrorMessage
        {
            get { return _ErrorMessage; }
            set { _ErrorMessage = value; }
        }
        private bool _isErrorOcurred;

        public bool IsErrorOcurred
        {
            get { return _isErrorOcurred; }
            set { _isErrorOcurred = value; }
        }

        private int _sourceInstallmentID;
        public int SourceInstallmentID
        {
            get { return _sourceInstallmentID; }
            set { _sourceInstallmentID = value; }
        }
    }
}
