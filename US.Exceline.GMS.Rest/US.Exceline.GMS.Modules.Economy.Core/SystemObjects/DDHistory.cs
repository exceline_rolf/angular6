﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public class DDHistory
    {
        private string fileName = string.Empty;
        public string FileName
        {
            get { return fileName; }
            set { fileName = value; }
        }

        private string orderId = string.Empty;
        public string OrderId
        {
            get { return orderId; }
            set { orderId = value; }
        }

        private string transactionID = string.Empty;
        public string TransactionID
        {
            get { return transactionID; }
            set { transactionID = value; }
        }

        private string cancellatrion = string.Empty;
        public string Cancellatrion
        {
            get { return cancellatrion; }
            set { cancellatrion = value; }
        }

        private int arNo = 0;
        public int ArNo
        {
            get { return arNo; }
            set { arNo = value; }
        }
    }
}
