﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public class DirectDeduct
    {
        public string AccountNo { get; set; }

        public string ContractNo { get; set; }

        public string ContractExpire { get; set; }

        public string LastContract { get; set; }

        public string ContractKID { get; set; }

        public DirectDeductStatus StatusMendatory { get; set; }

        public string InstallmentNo { get; set; }

        public string InstallmentKID { get; set; }

        public string TrancemissionNo { get; set; }

        public string Balance { get; set; }

        public string DueBalance { get; set; }

        public string LastReminder { get; set; }

        public CollectingStatus CollectingStatus { get; set; }

        public string CollectingText { get; set; }

        public string BranchNo { get; set; }

        public string amount { get; set; }
        public string message { get; set; }
        public string dueDate { get; set; }
        public string debtorName { get; set; }

        public string AritemNo { get; set; }


        public string nameInContract { get; set; }
        public string lastVisit { get; set; }
        public string printDate { get; set; }
        public string originalDue { get; set; }
        public string reminderfee { get; set; }
        public string InvoiceAmount { get; set; }

        public string InvoiceRef { get; set; }
        public string Group { get; set; }

    }
}
