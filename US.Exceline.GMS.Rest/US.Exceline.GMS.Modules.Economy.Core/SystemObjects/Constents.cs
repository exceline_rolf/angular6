﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    public class Constents
    {
        public const char DATE_SEPERATOR_DASH = '-';
        /// <summary>
        /// Dot date pat seperator char 
        /// </summary>
        public const char DATE_SEPERATOR_DOT = '.';

        /// <summary>
        /// Black slash date pat seperator char 
        /// </summary>
        public const char DATE_SEPERATOR_BLACK_SLASH = '/';

        /// <summary>
        /// Field Delimeter of the file
        /// </summary>
        public const char VALUE_SEPERATOR = ';';


    }
}
