﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.Exceline.GMS.Modules.Economy.Core.SystemObjects
{
    [DataContract]
    public class KeyData
    {
        

        private int _KeyVersion;
        [DataMember]
        public int KeyVersion
        {
            get { return _KeyVersion; }
            set { _KeyVersion = value; }
        }

        private string _PrivateKey;
        [DataMember]
        public string PrivateKey
        {
            get { return _PrivateKey; }
            set { _PrivateKey = value; }
        }

        private string _PublicKey;
        [DataMember]
        public string PublicKey
        {
            get { return _PublicKey; }
            set { _PublicKey = value; }
        }
    }
}