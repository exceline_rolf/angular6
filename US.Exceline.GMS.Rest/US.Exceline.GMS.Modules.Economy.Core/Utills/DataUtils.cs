﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using US.Exceline.GMS.Modules.Economy.Core.SystemObjects.Enums;

namespace US.Exceline.GMS.Modules.Economy.Core.Utills
{
    public class DataUtils
    {
        public static string ObjectToString(object obj)
        {
            if (obj == null)
            {
                return string.Empty;
            }
            else
            {
                return obj.ToString();
            }
        }

        public static decimal IsValidDecimal(string val)
        {
            string tempval = string.Empty;
            string currentCul = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
            if (currentCul.Equals("nb-NO"))
            {
                tempval = val.Replace(".", ",");
            }
            else
            {
                tempval = val;
            }
            try
            {
                decimal decVal = decimal.Parse(tempval);
                return decVal;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public static InvoiceTypes GetItemTypeEnumValue(string itemType)
        {
            switch (itemType)
            {
                case "DD":
                    return InvoiceTypes.DirectDeduct;
                case "IP":
                    return InvoiceTypes.InvoiceForPrint;
                case "IN":
                     return InvoiceTypes.Invoice;
                case "PP":
                    return InvoiceTypes.PaymentDocumentForPrint; 
                case "DW":
                    return InvoiceTypes.DebtWarning;
                case "OP":
                    return InvoiceTypes.OP; 
                case "SP":
                    return InvoiceTypes.Sponser;
                case "OL":
                    return InvoiceTypes.OL;
                case "DB":
                    return InvoiceTypes.DB;
                case "SM":
                    return InvoiceTypes.SM;
                case "CL":
                    return InvoiceTypes.CL;
                default:
                     return InvoiceTypes.Default;
            }
        }
        public static bool IsValiedInteger(string stringValue)
        {
            Regex objIntPattern = new Regex("^-[0-9]+$|^[0-9]+$");
            return objIntPattern.IsMatch(stringValue);
        }

    }
}
