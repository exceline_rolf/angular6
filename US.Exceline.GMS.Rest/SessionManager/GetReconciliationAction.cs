﻿using System;
using System.Data.Common;
using US_DataAccess;

namespace SessionManager
{
    public class GetReconciliationAction : USDBActionBase<int>
    {
        private readonly string _sessionKey;
        private readonly string _user;
        private readonly int _dailySettlementID;

        public GetReconciliationAction(string sessionKey, int dailySettlementID, string user)
        {
            _sessionKey = sessionKey;
            _user = user;
            _dailySettlementID = dailySettlementID;
        }

        protected override int Body(DbConnection connection)
        {
            const string spName = "ExceGMSGetReconciliationIdBySession";
            try
            {
                var command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@SessionKey", System.Data.DbType.String, _sessionKey));
                command.Parameters.Add(DataAcessUtils.CreateParam("@dailySettlementID", System.Data.DbType.Int32, _dailySettlementID));
                command.Parameters.Add(DataAcessUtils.CreateParam("@User", System.Data.DbType.String, _user));
                var value = Convert.ToInt32(command.ExecuteScalar());
                return value;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
    }
}
