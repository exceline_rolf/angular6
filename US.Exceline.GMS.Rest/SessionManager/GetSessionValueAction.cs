﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using US_DataAccess;

namespace SessionManager
{
    public class GetSessionValueAction : USDBActionBase<string>
    {
        private string _sessionKey = string.Empty;
        public GetSessionValueAction(string sessionKey)
        {
            _sessionKey = sessionKey;
        }

        protected override string Body(System.Data.Common.DbConnection connection)
        {
            string value = string.Empty;
            string spName = "ExceGMSGetSessionValue";
            try
            {
                DbCommand command = CreateCommand(System.Data.CommandType.StoredProcedure, spName);
                command.Parameters.Add(DataAcessUtils.CreateParam("@SessionKey", System.Data.DbType.String, _sessionKey));
                value = command.ExecuteScalar().ToString();
                return value;
            }
            catch (Exception ex)
            {
                return string.Empty;
               // throw;
            }
        }
    }
}
