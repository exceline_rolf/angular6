// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.
//
// To add a suppression to this file, right-click the message in the 
// Code Analysis results, point to "Suppress Message", and click 
// "In Suppression File".
// You do not need to add suppressions to this file manually.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "pr", Scope = "member", Target = "US.Exceline.GMS.Modules.Operations.BusinessLogic.Operations.OperationsManager.#GetARXMembers(System.Int32,System.String)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "accesTimes", Scope = "member", Target = "US.Exceline.GMS.Modules.Operations.BusinessLogic.Operations.OperationsManager.#GetARXMembers(System.Int32,System.String)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "xmlString", Scope = "member", Target = "US.Exceline.GMS.Modules.Operations.BusinessLogic.Operations.OperationsManager.#GetARXMembers(System.Int32,System.String)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "members", Scope = "member", Target = "US.Exceline.GMS.Modules.Operations.BusinessLogic.Operations.OperationsManager.#GetARXMembers(System.Int32,System.String)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "pCode", Scope = "member", Target = "US.Exceline.GMS.Modules.Operations.BusinessLogic.Operations.OperationsManager.#GetARXMembers(System.Int32,System.String)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "pinCode", Scope = "member", Target = "US.Exceline.GMS.Modules.Operations.BusinessLogic.Operations.OperationsManager.#GetARXMembers(System.Int32,System.String)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "itemList", Scope = "member", Target = "US.Exceline.GMS.Modules.Operations.BusinessLogic.Operations.OperationsManager.#GetARXMembers(System.Int32,System.String)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "p", Scope = "member", Target = "US.Exceline.GMS.Modules.Operations.BusinessLogic.Operations.OperationsManager.#GetARXMembers(System.Int32,System.String)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "val", Scope = "member", Target = "US.Exceline.GMS.Modules.Operations.BusinessLogic.Operations.OperationsManager.#GetARXMembers(System.Int32,System.String)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "xmlString", Scope = "member", Target = "US.Exceline.GMS.Modules.Operations.BusinessLogic.Operations.OperationsManager.#GetARXMembers(System.Int32,System.String,System.Boolean)")]
