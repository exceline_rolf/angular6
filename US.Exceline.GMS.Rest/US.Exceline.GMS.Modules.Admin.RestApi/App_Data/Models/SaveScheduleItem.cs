﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.DomainObjects.ScheduleManagement;

namespace US.Exceline.GMS.Modules.Admin.RestApi.Models
{
    public class SaveScheduleItem
    {
        public ScheduleItemDC scheduleItem { get; set; }
        public int resId { get; set; }
        public int branchId { get; set; }

    }
} 