﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using US.GMS.Core.DomainObjects.AccessControl.ARX;
using US.GMS.Core.DomainObjects.Admin.ManageGymSettings;
using US.GMS.Core.DomainObjects.Common;

namespace US.Exceline.GMS.Modules.Admin.RestApi.Models
{
    public class GymSettingsLists
    {
        public int BranchId { get; set; }
        public List<GymOpenTimeDC> GymOpenTimeList { get; set; }

        public List<ExceHardwareProfileDC> HardwareProfileList { get; set; }

        public GymIntegrationExcSettingsDC GymIntegrationSettings { get; set; }

        public OtherIntegrationSettingsDC OtherIntegrationSettings { get; set; }

        public ExceArxFormatDetail arxFormatDetail { get; set; }

        public string accessContolTypes { get; set; }





    }
}