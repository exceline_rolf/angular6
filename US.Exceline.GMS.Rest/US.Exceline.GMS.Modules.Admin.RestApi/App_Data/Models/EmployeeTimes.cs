﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using US.GMS.Core.DomainObjects.Common;

namespace US.Exceline.GMS.Modules.Admin.RestApi.Models
{
    public class EmployeeTimes
    {

        public int employeeId { get; set; }

        public bool isAllApproved { get; set; }
        public string timeIdString { get; set; }

    }
} 