﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using US.GMS.Core.DomainObjects.Common;

namespace US.Exceline.GMS.Modules.Admin.RestApi.Models
{
   
    public class UpdateApprovalStatus
    {
        public bool IsApproved { get; set; }

        public int EntityActiveTimeID { get; set; }

    }
} 