﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Exceline.GMS.Modules.Admin.RestApi.Model
{
    public class BookingPaymnet
    {
        public int ActivetimeId { get; set; }
        public int ArticleId { get; set; }
        public int MemberId { get; set; }
        public bool Paid { get; set; }
        public int AritemNo { get; set; }
        public decimal Amount { get; set; }
        public string PaymentType { get; set; }
    }
}