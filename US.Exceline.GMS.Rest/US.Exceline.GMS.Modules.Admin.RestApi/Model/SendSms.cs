﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Exceline.GMS.Modules.Admin.RestApi.Model
{
    public class SendSms
    {
        public Dictionary<int, string> MemberIdList { get; set; }
        public int BranchId { get; set; }
        public string Message { get; set; }
        public string MobileNo { get; set; }
        public string MemberName { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string ResName { get; set; }
        public DateTime BookingDate { get; set; }
    }
}