﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using US.GMS.Core.DomainObjects.Common;
using US.GMS.Core.SystemObjects;

namespace US.Exceline.GMS.Modules.Admin.RestApi.Model
{
    public class ArticleSearch
    {
        public int BranchId { get; set; }
        public ArticleTypes CategpryType { get; set; }
        public string Keyword { get; set; }
        public CategoryDC Category { get; set; }
        public int ActivityId { get; set; }
        public bool? IsObsalate { get; set; }
        public bool IsActive { get; set; }
        public bool FilterByGym { get; set; }
    }
}