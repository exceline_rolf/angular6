﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace US.Exceline.GMS.IBooking.Core
{
    [DataContract]
    public class MemberVisitStatus
    {
        [DataMember(Order = 1)]
        public string CustomerID { get; set; }
        [DataMember(Order = 2)]
        public string Status { get; set; }
    }
}
