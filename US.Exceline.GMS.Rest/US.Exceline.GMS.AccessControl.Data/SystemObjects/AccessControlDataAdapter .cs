﻿
using System.Collections.Generic;
using US.GMS.Core.DomainObjects.AccessControl;

namespace US.Exceline.GMS.AccessControl.Data.SystemObjects
{
    public interface IAccessControlDataAdapter
    {
        ExceAccessControlAuthenticationDC CheckAuthentication(int branchId, string cardNo, string gymCode,int terminalId);
        List<ExceAccessControlTerminalDetailDC> GetTerminalDetails(int branchId, string gymCode);
        bool UpdateTerminalStatus(string gymCode, ExceAccessControlUpdateTerminalStatusDC terminalStatusDetail);
        bool RegisterPurchase(string gymCode, ExceAccessControlRegisterPurchaseDC purchaseDetail);        
        bool RegisterVendingEvent(string gymCode, ExceAccessControlVendingEventDC vendingEventDetail);
        bool RegisterAccessControlEvent(string gymCode, ExceAccessControlEventDC accessControlEventDetail);
        string GetGymCodeByCompanyId(string companyId);
        ExceAccessControlTerminalDetailDC GetTypeIdByTerminalId(int terminalId, string gymCode);
        ExceAccessControlAuthenticationDC GetMemberDetailForSunBedPurchase(string cardNumber, string gymCode);
        ExceAccessControlAuthenticationDC CheckAutheniticationForGatPurchase(string cardNo, int branchId, int terminalId, string gymCode);
    }
}
