﻿using System.Collections.Generic;
using US.Exceline.GMS.AccessControl.Data.DataAdapters.SQLServer;
using US.Exceline.GMS.AccessControl.Data.SystemObjects;
using US.GMS.Core.DomainObjects.AccessControl;

namespace US.Exceline.GMS.AccessControl.Data.DataAdapters
{
    public class ExcelineAccessControlFacade
    {
        private static IAccessControlDataAdapter GetDataAdapter()
        {
            return new SQLServerAccessControlDataAdapter();
        }

        public static ExceAccessControlAuthenticationDC CheckAuthentication(int branchId, string cardNo, string gymCode,int terminalId)
        {
            return GetDataAdapter().CheckAuthentication(branchId, cardNo, gymCode,terminalId);
        }

        public static List<ExceAccessControlTerminalDetailDC> GetTerminalDetails(int branchId, string gymCode)
        {
            return GetDataAdapter().GetTerminalDetails(branchId, gymCode);
        }

        public static bool UpdateTerminalStatus(string gymCode, ExceAccessControlUpdateTerminalStatusDC terminalStatusDetail)
        {
            return GetDataAdapter().UpdateTerminalStatus(gymCode, terminalStatusDetail);
        }

        public static bool RegisterPurchase(string gymCode, ExceAccessControlRegisterPurchaseDC purchaseDetail)
        {
            return GetDataAdapter().RegisterPurchase(gymCode, purchaseDetail);
        }      

        public static bool RegisterVendingEvent(string gymCode, ExceAccessControlVendingEventDC vendingEventDetail)
        {
            return GetDataAdapter().RegisterVendingEvent(gymCode, vendingEventDetail);
        }

        public static bool RegisterAccessControlEvent(string gymCode, ExceAccessControlEventDC accessControlEventDetail)
        {
            return GetDataAdapter().RegisterAccessControlEvent(gymCode, accessControlEventDetail);
        }

        public static string GetGymCodeByCompanyId(string companyId)
        {
            return GetDataAdapter().GetGymCodeByCompanyId(companyId);
        }

        public static ExceAccessControlTerminalDetailDC GetTypeIdByTerminalId(int terminalId, string gymCode)
        {
            return GetDataAdapter().GetTypeIdByTerminalId(terminalId, gymCode);
        }

        public static ExceAccessControlAuthenticationDC GetMemberDetailForSunBedPurchase(string cardNo, string gymCode)
        {
            return GetDataAdapter().GetMemberDetailForSunBedPurchase(cardNo, gymCode);
        }

        public static ExceAccessControlAuthenticationDC CheckGatPurchaseAuthentication(string cardNo, int branchId, int terminalId, string gymCode)
        {
            return GetDataAdapter().CheckAutheniticationForGatPurchase(cardNo, branchId, terminalId, gymCode);
        }
    }
}
