﻿using System.Collections.Generic;
using US.Exceline.GMS.AccessControl.Data.DataAdapters.SQLServer.Commands;
using US.Exceline.GMS.AccessControl.Data.SystemObjects;
using US.GMS.Core.DomainObjects.AccessControl;
using US_DataAccess;

namespace US.Exceline.GMS.AccessControl.Data.DataAdapters.SQLServer
{
    public class SQLServerAccessControlDataAdapter : IAccessControlDataAdapter
    {
        public ExceAccessControlAuthenticationDC CheckAuthentication(int branchId, string cardNo, string gymCode,int terminalId)
        {
            CheckAuthenticationAction action = new CheckAuthenticationAction(branchId, cardNo, terminalId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public List<ExceAccessControlTerminalDetailDC> GetTerminalDetails(int branchId, string gymCode)
        {
            GetTerminalDetailsAction action = new GetTerminalDetailsAction(branchId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool UpdateTerminalStatus(string gymCode, ExceAccessControlUpdateTerminalStatusDC terminalStatusDetail)
        {
            UpdateTerminalStatusAction action = new UpdateTerminalStatusAction(terminalStatusDetail);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool RegisterPurchase(string gymCode, ExceAccessControlRegisterPurchaseDC purchaseDetail)
        {
            RegisterPurchaseAction action = new RegisterPurchaseAction(purchaseDetail);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }        

        public bool RegisterVendingEvent(string gymCode, ExceAccessControlVendingEventDC vendingEventDetail)
        {
            RegisterVendingEventAction action = new RegisterVendingEventAction(vendingEventDetail);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public bool RegisterAccessControlEvent(string gymCode, ExceAccessControlEventDC accessControlEventDetail)
        {
            RegisterAccessControlEventAction action = new RegisterAccessControlEventAction(accessControlEventDetail);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public string GetGymCodeByCompanyId(string companyId)
        {
            GetGymCodeByCompanyIdAction action = new GetGymCodeByCompanyIdAction(companyId);
            return action.Execute(EnumDatabase.WorkStation);
        }

        public ExceAccessControlTerminalDetailDC GetTypeIdByTerminalId(int terminalId, string gymCode)
        {
            GetGetTypeIdByTerminalIdAction action = new GetGetTypeIdByTerminalIdAction(terminalId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }

        public ExceAccessControlAuthenticationDC GetMemberDetailForSunBedPurchase(string cardNumber, string gymCode)
        {
            GetMemberDetailForSunbedPurchaseAction action = new GetMemberDetailForSunbedPurchaseAction(cardNumber);
            ExceAccessControlAuthenticationDC result =  action.Execute(EnumDatabase.Exceline, gymCode);

            GetTimeMachineArticleAction timeMachineAction = new GetTimeMachineArticleAction();
            result.TimeMachineArticle = timeMachineAction.Execute(EnumDatabase.Exceline, gymCode);

            return result;
        }

        public ExceAccessControlAuthenticationDC CheckAutheniticationForGatPurchase(string cardNo, int branchId, int terminalId, string gymCode)
        {
            CheckGatPurchaseAuthenticationAction action = new CheckGatPurchaseAuthenticationAction(cardNo, branchId, terminalId);
            return action.Execute(EnumDatabase.Exceline, gymCode);
        }
    }
}
