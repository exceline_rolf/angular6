﻿using System;
using System.Data;
using System.Data.Common;
using US_DataAccess;

namespace US.Exceline.GMS.AccessControl.Data.DataAdapters.SQLServer.Commands
{
    public class GetIBookingGymCodeByCompanyIdAction : USDBActionBase<string>
    {
        private string _gymCode;
        private readonly string _companyId;
        public GetIBookingGymCodeByCompanyIdAction(string systemId)
        {
            _companyId = systemId;
        }
        protected override string Body(DbConnection connection)
        {
            const string storedProcedure = "ExceWorkStationGetGymCodeByGymID";

            try
            {
                DbCommand cmd = CreateCommand(CommandType.StoredProcedure, storedProcedure);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@gymId", DbType.Int32, _companyId));
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    _gymCode = reader["GymCode"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _gymCode;
        }
    }
}
