﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using US.Common.Logging.API;
using US.Exceline.GMS.Modules.Admin.API.ManageResources;
using US.Exceline.GMS.Modules.Class.API;
using US.Exceline.GMS.Modules.Class.RestApi.Models;
using US.GMS.API;
using US.GMS.API.ManageSystemSettings;
using US.GMS.Core.DomainObjects.Admin.ManageEmployees;
using US.GMS.Core.DomainObjects.Admin.ManageResources;
using US.GMS.Core.DomainObjects.ManageClasses;
using US.GMS.Core.DomainObjects.ScheduleManagement;
using US.GMS.Core.ResultNotifications;
using US.GMS.Core.SystemObjects;
using US.GMS.Core.Utils;

namespace US.Exceline.GMS.Modules.Class.RestApi.Controllers
{
    [RoutePrefix("api/Class")]
    public class ClassController : ApiController
    {

        [HttpGet]
        [Route("SearchClass")]
        [Authorize]
        public HttpResponseMessage SearchClass(string category, string searchText)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSClass.SearchClass(category, searchText, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<ExcelineClassDC>(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExcelineClassDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpGet]
        [Route("SearchClass")]
        [Authorize]
        public HttpResponseMessage SearchClass(string category, string searchText, DateTime startDate)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSClass.SearchClass(category, searchText, startDate, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<ExcelineClassDC>(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExcelineClassDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpGet]
        [Route("SearchClass")]
        [Authorize]
        public HttpResponseMessage SearchClass(string category, DateTime startDate, DateTime endDate, string searchText)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSClass.SearchClass(category, searchText, startDate, endDate, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<ExcelineClassDC>(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExcelineClassDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


        [HttpGet]
        [Route("GetNextClassId")]
        [Authorize]
        public HttpResponseMessage GetNextClassId()
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSClass.GetNextClassId(ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


        [HttpPost]
        [Route("SaveClass")]
        [Authorize]
        public HttpResponseMessage SaveClass(SaveClassReq saveClassReq)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSClass.SaveClass(saveClassReq.ExcelineClass, saveClassReq.BranchId, ExceConnectionManager.GetGymCode(user), user, saveClassReq.Culture);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


        [HttpPost]
        [Route("UpdateCalenderActiveTimes")]
        [Authorize]
        public HttpResponseMessage UpdateCalenderActiveTimes(UpdateActiveTimesReq req)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSSchedule.UpdateCalenderActiveTimes(req.ActiveTimeList, req.BranchId, user, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpPost]
        [Route("UpdateClass")]
        [Authorize]
        public HttpResponseMessage UpdateClass(ExcelineClassDC excelineClass)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSClass.UpdateClass(excelineClass, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


        [HttpGet]
        [Route("DeleteClass")]
        [Authorize]
        public HttpResponseMessage DeleteClass(int classId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSClass.DeleteClass(classId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }
                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));
            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpGet]
        [Route("GetClasses")]
        [Authorize]
        public HttpResponseMessage GetClasses(string className, int branchId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSClass.GetClasses( className, branchId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<ExcelineClassDC>(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExcelineClassDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpGet]
        [Route("GetMembersByActiveTimeId")]
        [Authorize]
        public HttpResponseMessage GetMembersByActiveTimeId(int branchId, int activeTimeId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSClass.GetMembersByActiveTimeId(branchId, activeTimeId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<ExcelineClassMemberDC>(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExcelineClassMemberDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpGet]
        [Route("GetScheduleItemsByClass")]
        [Authorize]
        public HttpResponseMessage GetScheduleItemsByClass(int classId, int branchId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSClass.GetScheduleItemsByClass(classId, branchId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<ScheduleItemDC>(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ScheduleItemDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


        [HttpPost]
        [Route("GetScheduleItemsByClassIds")]
        [Authorize]
        public HttpResponseMessage GetScheduleItemsByClassIds(ScheduleItemsByClassIdsReq req)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSClass.GetScheduleItemsByClassIds(req.ClassIds, req.BranchId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<ScheduleItemDC>(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ScheduleItemDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpPost]
        [Route("GetEntityActiveTimes")]
        [Authorize]
        public HttpResponseMessage GetEntityActiveTimes(GetEntityActiveTimes req)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSSchedule.GetEntityActiveTimes(req.BranchId, req.StartDate, req.EndDate, req.EntityList, req.EntityRoleType, ExceConnectionManager.GetGymCode(user), user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<EntityActiveTimeDC>(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<EntityActiveTimeDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }
        }


        [HttpGet]
        [Route("GetExcelineClassActiveTimes")]
        [Authorize]
        public HttpResponseMessage GetExcelineClassActiveTimes(int branchId, DateTime startTime, DateTime endTime, int entNO)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSClass.GetExcelineClassActiveTimes(branchId, startTime, endTime, entNO, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<ExcelineClassActiveTimeDC>(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ExcelineClassActiveTimeDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


        [HttpGet]
        [Route("GetInstructorsForClass")]
        [Authorize]
        public HttpResponseMessage GetInstructorsForClass(int classId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSClass.GetInstructorsForClass(classId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<InstructorDC>(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<InstructorDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


        [HttpGet]
        [Route("GetResourceForClass")]
        [Authorize]
        public HttpResponseMessage GetResourceForClass(int classId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSClass.GetResourceForClass(classId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<ResourceDC>(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ResourceDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


        [HttpGet]
        [Route("GetClassSchedule")]
        [Authorize]
        public HttpResponseMessage GetClassSchedule(int classId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSClass.GetClassSchedule(classId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new ScheduleDC(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new ScheduleDC(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpGet]
        [Route("GetExcelineClassIdByName")]
        [Authorize]
        public HttpResponseMessage GetExcelineClassIdByName(string className)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSClass.GetExcelineClassIdByName(className, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


        [HttpPost]
        [Route("UpdateTimeTableScheduleItems")]
        [Authorize]
        public HttpResponseMessage UpdateTimeTableScheduleItems(UpdateTimeTableScheduleItemsReq req)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSClass.UpdateTimeTableScheduleItems(req.SceduleItemList, req.BranchId, ExceConnectionManager.GetGymCode(user), user, req.Culture);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpPost]
        [Route("CheckActiveTimeOverlapWithClass")]
        [Authorize]
        public HttpResponseMessage CheckActiveTimeOverlapWithClass(ScheduleItemDC scheduleItem)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSClass.CheckActiveTimeOverlapWithClass(scheduleItem, scheduleItem.Culture, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(string.Empty, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(string.Empty, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


        [HttpPost]
        [Route("SaveScheduleItem")]
        [Authorize]
        public HttpResponseMessage SaveScheduleItem(SaveScheduleItemReq req)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSClass.SaveScheduleItem(req.ScheduleItem, req.BranchId, ExceConnectionManager.GetGymCode(user), req.Culture, user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpPost]
        [Route("DeleteScheduleItem")]
        [Authorize]
        public HttpResponseMessage DeleteScheduleItem(DeleteScheduleItemReq req)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSClass.DeleteScheduleItem(req.ScheduleItemIdList, ExceConnectionManager.GetGymCode(user), user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


        [HttpPost]
        [Route("DeleteSchedule")]
        [Authorize]
        public HttpResponseMessage DeleteSchedule(ExcelineClassDC exceClass)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSClass.DeleteSchedule(exceClass, ExceConnectionManager.GetGymCode(user), user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpPost]
        [Route("SaveActiveTimes")]
        [Authorize]
        public HttpResponseMessage SaveActiveTimes(SaveActiveTimesReq req)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSClass.SaveActiveTimes(req.ScheduleItemList, req.StartDate, req.EndDate, ExceConnectionManager.GetGymCode(user), req.Culture);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpPost]
        [Route("UpdateClassCalendarActiveTime")]
        [Authorize]
        public HttpResponseMessage UpdateClassCalendarActiveTime(EntityActiveTimeDC activeTime)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSClass.UpdateClassCalendarActiveTime(activeTime, user, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


        [HttpGet]
        [Route("UpdateSeasonEndDateWithClassActiveTimes")]
        [Authorize]
        public HttpResponseMessage UpdateSeasonEndDateWithClassActiveTimes(int branchId, int seasonId, DateTime endDate)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSClass.UpdateSeasonEndDateWithClassActiveTimes( branchId, seasonId, endDate, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


        [HttpGet]
        [Route("DeleteClassActiveTime")]
        [Authorize]
        public HttpResponseMessage DeleteClassActiveTime(int actTimeId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSClass.DeleteClassActiveTime(actTimeId, ExceConnectionManager.GetGymCode(user), user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(false, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpPost]
        [Route("UpdateClassActiveTime")]
        [Authorize]
        public HttpResponseMessage UpdateClassActiveTime(UpdateClassActiveTimeHelperDC helperObj)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSClass.UpdateClassActiveTime(helperObj, ExceConnectionManager.GetGymCode(user), user);
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(-1, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


        [HttpGet]
        [Route("GetActiveTimesForClassScheduleItem")]
        [Authorize]
        public HttpResponseMessage GetActiveTimesForClassScheduleItem(int scheduleItemId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSClass.GetActiveTimesForClassScheduleItem(scheduleItemId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<EntityActiveTimeDC>(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<EntityActiveTimeDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


        [HttpGet]
        [Route("GetListOrCalendarInitialViewByUser")]
        [Authorize]
        public HttpResponseMessage GetListOrCalendarInitialViewByUser()
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSClass.GetListOrCalendarInitialViewByUser(user, ExceConnectionManager.GetGymCode(user));
                

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


        [HttpGet]
        [Route("GetResources")]
        [Authorize]
        public HttpResponseMessage GetResources(int branchId, string searchText)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSResources.GetResources(branchId, searchText, -1, -1, 2, true, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<ResourceDC>(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ResourceDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }

        [HttpGet]
        [Route("GetSeasonAndClassInfo")]
        [Authorize]
        public HttpResponseMessage GetSeasonAndClassInfo(int branchId,int seasonId)
        {
            try
            {
                string user = Request.Headers.GetValues("UserName").First();
                var result = GMSClass.GetSeasonAndClassInfo( branchId, seasonId, ExceConnectionManager.GetGymCode(user));
                if (result.ErrorOccured)
                {
                    List<string> errorMsg = new List<string>();
                    foreach (NotificationMessage message in result.Notifications)
                    {
                        USLogError.WriteToFile(message.Message, new Exception(), user);
                        errorMsg.Add(message.Message);
                    }

                    return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(new List<ResourceDC>(), ApiResponseStatus.ERROR, errorMsg));
                }

                return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(result.OperationReturnValue, ApiResponseStatus.OK));

            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(new List<ResourceDC>(), ApiResponseStatus.ERROR, exceptionMsg));
            }

        }


        [HttpGet]
        [Route("GetGymSettings")]
        [Authorize]
        public HttpResponseMessage GetGymSettings(int branchId, GymSettingType gymSettingType, string systemName)
        {

            try
            {
                string user = Request.Headers.GetValues("UserName").First();

                switch (gymSettingType)
                {
                    case GymSettingType.ACCESSTIME:
                        var resultAT = GMSManageGymSetting.GetGymAccessTimeSettings(branchId, ExceConnectionManager.GetGymCode(user), gymSettingType, user, systemName);
                        if (resultAT.ErrorOccured)
                        {
                            List<string> errorMsg = new List<string>();
                            foreach (NotificationMessage message in resultAT.Notifications)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), user);
                                errorMsg.Add(message.Message);
                            }
                            return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(resultAT.OperationReturnValue, ApiResponseStatus.OK));
                        break;
                    case GymSettingType.GYMOPENTIME:
                        var resultG = GMSManageGymSetting.GetGymOpenTimes(branchId, ExceConnectionManager.GetGymCode(user), gymSettingType, user, systemName);
                        if (resultG.ErrorOccured)
                        {
                            List<string> errorMsg = new List<string>();
                            foreach (NotificationMessage message in resultG.Notifications)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), user);
                                errorMsg.Add(message.Message);
                            }
                            return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(resultG.OperationReturnValue, ApiResponseStatus.OK));

                        break;
                    case GymSettingType.REQUIRED:
                        var resultR = GMSManageGymSetting.GetGymRequiredSettings(branchId, ExceConnectionManager.GetGymCode(user), gymSettingType, user, systemName);
                        if (resultR.ErrorOccured)
                        {
                            List<string> errorMsg = new List<string>();
                            foreach (NotificationMessage message in resultR.Notifications)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), user);
                                errorMsg.Add(message.Message);
                            }
                            return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(resultR.OperationReturnValue, ApiResponseStatus.OK));



                        break;
                    case GymSettingType.MEMBERSEARCH:
                        var resultM = GMSManageGymSetting.GetGymMemberSearchSettings(branchId, ExceConnectionManager.GetGymCode(user), gymSettingType, user, systemName);
                        if (resultM.ErrorOccured)
                        {
                            List<string> errorMsg = new List<string>();
                            foreach (NotificationMessage message in resultM.Notifications)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), user);
                                errorMsg.Add(message.Message);
                            }
                            return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(resultM.OperationReturnValue, ApiResponseStatus.OK));

                        break;
                    case GymSettingType.HARDWARE:
                        var resultH = GMSManageGymSetting.GetHardwareProfiles(branchId, ExceConnectionManager.GetGymCode(user), gymSettingType, user, systemName);
                        if (resultH.ErrorOccured)
                        {
                            List<string> errorMsg = new List<string>();
                            foreach (NotificationMessage message in resultH.Notifications)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), user);
                                errorMsg.Add(message.Message);
                            }
                            return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(resultH.OperationReturnValue, ApiResponseStatus.OK));

                        break;
                    case GymSettingType.ECONOMY:
                        var resultE = GMSManageGymSetting.GetGymEconomySettings(branchId, ExceConnectionManager.GetGymCode(user), gymSettingType, user, systemName);
                        if (resultE.ErrorOccured)
                        {
                            List<string> errorMsg = new List<string>();
                            foreach (NotificationMessage message in resultE.Notifications)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), user);
                                errorMsg.Add(message.Message);
                            }
                            return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(resultE.OperationReturnValue, ApiResponseStatus.OK));

                        break;
                    case GymSettingType.SMS:
                        var resultS = GMSManageGymSetting.GetGymSmsSettings(branchId, ExceConnectionManager.GetGymCode(user), gymSettingType, user, systemName);
                        if (resultS.ErrorOccured)
                        {
                            List<string> errorMsg = new List<string>();
                            foreach (NotificationMessage message in resultS.Notifications)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), user);
                                errorMsg.Add(message.Message);
                            }
                            return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(resultS.OperationReturnValue, ApiResponseStatus.OK));

                        break;
                    case GymSettingType.OTHER:
                        var resultO = GMSManageGymSetting.GetGymOtherSettings(branchId, ExceConnectionManager.GetGymCode(user), gymSettingType, user, systemName);
                        if (resultO.ErrorOccured)
                        {
                            List<string> errorMsg = new List<string>();
                            foreach (NotificationMessage message in resultO.Notifications)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), user);
                                errorMsg.Add(message.Message);
                            }
                            return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(resultO.OperationReturnValue, ApiResponseStatus.OK));

                        break;
                    case GymSettingType.INTEGRATION:
                        var resultI = GMSManageGymSetting.GetGymIntegrationSettings(branchId, ExceConnectionManager.GetGymCode(user), gymSettingType, user, systemName);
                        if (resultI.ErrorOccured)
                        {
                            List<string> errorMsg = new List<string>();
                            foreach (NotificationMessage message in resultI.Notifications)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), user);
                                errorMsg.Add(message.Message);
                            }
                            return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(resultI.OperationReturnValue, ApiResponseStatus.OK));

                        break;
                    case GymSettingType.EXCINTEGRATION:
                        var resultEI = GMSManageGymSetting.GetGymIntegrationExcSettings(branchId, ExceConnectionManager.GetGymCode(user), gymSettingType, user, systemName);
                        if (resultEI.ErrorOccured)
                        {
                            List<string> errorMsg = new List<string>();
                            foreach (NotificationMessage message in resultEI.Notifications)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), user);
                                errorMsg.Add(message.Message);
                            }
                            return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(resultEI.OperationReturnValue, ApiResponseStatus.OK));

                        break;
                    case GymSettingType.OTHERINTEGRATION:
                        var resultOI = GMSManageGymSetting.GetOtherIntegrationSettings(branchId, ExceConnectionManager.GetGymCode(user), gymSettingType, user, systemName);
                        if (resultOI.ErrorOccured)
                        {
                            List<string> errorMsg = new List<string>();
                            foreach (NotificationMessage message in resultOI.Notifications)
                            {
                                USLogError.WriteToFile(message.Message, new Exception(), user);
                                errorMsg.Add(message.Message);
                            }
                            return Request.CreateResponse(HttpStatusCode.NotFound, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, errorMsg));
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, ApiUtils.CreateApiResponse(resultOI.OperationReturnValue, ApiResponseStatus.OK));

                        break;

                }



            }
            catch (Exception ex)
            {
                List<string> exceptionMsg = new List<string>();
                exceptionMsg.Add(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ApiUtils.CreateApiResponse(null, ApiResponseStatus.ERROR, exceptionMsg));
            }
            return null;
        }


    }
}
