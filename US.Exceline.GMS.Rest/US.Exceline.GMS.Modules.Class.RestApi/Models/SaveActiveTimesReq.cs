﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using US.GMS.Core.DomainObjects.ScheduleManagement;

namespace US.Exceline.GMS.Modules.Class.RestApi.Models
{
    public class SaveActiveTimesReq
    {

        private List<ScheduleItemDC> _scheduleItemList;

        public List<ScheduleItemDC> ScheduleItemList
        {
            get { return _scheduleItemList; }
            set { _scheduleItemList = value; }
        }

        private DateTime _startDate;

        public DateTime StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }


        private DateTime _endDate;

        public DateTime EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }


        private string _culture;

        public string Culture
        {
            get { return _culture; }
            set { _culture = value; }
        }

    }
}