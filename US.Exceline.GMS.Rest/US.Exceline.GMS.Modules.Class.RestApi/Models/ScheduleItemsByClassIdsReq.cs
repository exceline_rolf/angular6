﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace US.Exceline.GMS.Modules.Class.RestApi.Models
{
    public class ScheduleItemsByClassIdsReq
    {
        private List<int> _classIds;

        public List<int> ClassIds
        {
            get { return _classIds; }
            set { _classIds = value; }
        }


        private int _branchId;

        public int BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }


    }
}