﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using US.USIE.UtilityLibray.Logging;

namespace US.Exceline.GMS.EventLogDelete
{
    public static class LogginManager
    {
        private const string DEFAULT_LOG_FILE_NAME = "Log.txt";
        private const int MAXIMUM_SIZE_OF_LOG_FILE_IN_KB = 1000;
        private static List<LogRecord> _taskLogs = new List<LogRecord>();
        private static string _logFileName = DEFAULT_LOG_FILE_NAME;
        private static string _taskDisplayName;
        public static List<LogRecord> TaskLogs
        {
            get
            {
                return _taskLogs;
            }
            set
            {
                _taskLogs = value;
            }
        }
        /// <summary>
        /// Attach a logging object for UIPO task logging
        /// </summary>
        public static void RegisterTaskLog(string taskDisplayName)
        {
            _taskDisplayName = taskDisplayName;
            _taskLogs = new List<LogRecord>();
        }

        /// <summary>
        /// Attach a file for loggin
        /// </summary>
        public static void RegisterLogFile(string fileName)
        {
            _logFileName = fileName;
            if (string.IsNullOrEmpty(_logFileName))
                _logFileName = DEFAULT_LOG_FILE_NAME;
        }

        /// <summary>
        /// Add log entry to task Log
        /// </summary>
        public static void LogToTaskLog(string message1, string message2)
        {
            _taskLogs.Add(UDIELogger.FillLogRecord(_taskDisplayName,
                            message1, message2));
        }

        /// <summary>
        /// Write log entry to log file
        /// </summary>
        //public static void LogErrorToLogFile(string message)
        //{
        //    message = "ERROR - " + message;
        //    FileManager.WriteToLogFile(_logFileName, message);
        //}

        public static void LogErrorToLogFile(string message)
        {
            try
            {
                string fileName = DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + ".txt";
                string taskLogPath = US.UIPO.RuntimeVariables.TaskDLLDirectoryPath + @"\Log\EventLogDelete\";
                string fullPath = taskLogPath + fileName;
                if (!File.Exists(fullPath))
                {
                    if (!Directory.Exists(taskLogPath))
                        Directory.CreateDirectory(taskLogPath);
                }
                using (StreamWriter sw = new StreamWriter(fullPath, true))
                {
                    sw.WriteLine(DateTime.Now.ToString() + " " + message + "\n");
                    sw.Close();
                }
            }
            catch (Exception)
            {
            }
        }
    }
}
