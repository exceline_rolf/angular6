﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : US.Exceline.GMS
// Project Name      : US.Exceline.GMS.Modules.Notifications.Data
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 19/6/2012 4:27:11 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US.GMS.Core.DomainObjects.Notification;

namespace US.Exceline.GMS.Modules.Notifications.Data.SystemObjects
{
    interface INotificationsDataAdapter
    {
        #region  Notification
        int SaveNotification(NotificationDC notification, string createdUser, int branchId, string gymCode);
        int SaveNotificationTemplate(NotificationTemplateDC notificationTemplate, string createdUser, int branchId, string gymCode);
        int SaveNotificationMethod(NotificationMethodDC notificationMethod, string createdUser, int branchId, string gymCode);
        List<NotificationMethodDC> GetNotifications(int branchId, string gymCode);
        List<NotificationMethodDC> GetNotificationsByMember(int branchId, int memeberId, string gymCode);
        int UpdateIsNotified(int notificationMethodId, string createdUser, int branchId, string gymCode);
        bool DeleteNotificationMethod(int notificationMethodId, string createdUser, int branchId, string gymCode);
        List<NotificationTemplateDC> GetNotificationTemplates(int branchId, string gymCode);
        #endregion
    }
}
