﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : US.Exceline.GMS
// Project Name      : US.Exceline.GMS.Modules.Notifications.Data
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 19/6/2012 4:27:11 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using System.Data.Common;
using US.GMS.Core.DomainObjects.Notification;
using System.Data.SqlClient;
using System.Data;
using US.GMS.Core.DomainObjects.ManageMemberships;

namespace US.Exceline.GMS.Modules.Notifications.Data.DataAdapters.SQLServer.Commands
{
    public class SaveNotificationAction : USDBActionBase<int>
    {
        private NotificationDC _notification = new NotificationDC();
        private string _createdUser = string.Empty;
        private int _branchId = -1;

        public SaveNotificationAction(NotificationDC notification, string createdUser, int branchId)
        {
            _notification = notification;
            _createdUser = createdUser;
            _branchId = branchId;

        }

        protected override int Body(DbConnection connection)
        {
            int result = -1;
            string StoredProcedureName = "USExceGMSNotificationsSaveNotification";

            try
            {

                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@Id", DbType.Int32, _notification.Id));
                if (_notification.SourceID > 0)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@sourceID", DbType.Int32, _notification.SourceID));
                }
                if (_notification.Source != null)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@source", DbType.String, _notification.Source));
                if (_notification.ReceiverEntityID != 0)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@receiverEntityID", DbType.Int32, _notification.ReceiverEntityID));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@createdDate", DbType.DateTime, DateTime.Now));
                if (!String.IsNullOrEmpty(_createdUser) )
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@createdUser", DbType.String, _createdUser));
                if (_branchId != -1)
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchID", DbType.Int32, _branchId));
                if (_notification.Role != MemberRole.NONE)
                {
                    cmd.Parameters.Add(DataAcessUtils.CreateParam("@role", DbType.String, _notification.Role));
                }
                DbParameter outputPara = new SqlParameter();
                outputPara.DbType = DbType.Int32;
                outputPara.ParameterName = "@OutID";
                outputPara.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outputPara);
                cmd.ExecuteNonQuery();

                result = Convert.ToInt32(outputPara.Value);

            }

            catch (Exception ex)
            {
                throw ex;
            }
            return result;

        }
    }
}
