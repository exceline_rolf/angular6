﻿// --------------------------------------------------------------------------
// Copyright(c) <2012> "Unicorn Solutions.
// Chandra silva mawath,pagoda Rd,Nugegoda, Sri lanka.
// All rights reserved.
//
// Solution Name     : Exceline
// Project Name      : US.Exceline.GMS
// Coding Standard   : US Coding Standards
// Author            : NAD
// Created Timestamp : 5/7/2012 4:19:11 PM
// --------------------------------------------------------------------------
// Edit Author       : 
// Edit Timestamp    : 
// Comment           : 
// --------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using US_DataAccess;
using US.GMS.Core.DomainObjects.Notification;
using System.Data.Common;
using System.Data;

namespace US.Exceline.GMS.Modules.Notifications.Data.DataAdapters.SQLServer.Commands
{
    public class GetNotificationsActionByMemberID : USDBActionBase<List<NotificationMethodDC>>
    {
        private int _branchId = 0;
        private int _memberID = -1;


        public GetNotificationsActionByMemberID(int branchId, int memberID)
        {
            _memberID = memberID;
            _branchId = branchId;

        }

        protected override List<NotificationMethodDC> Body(DbConnection connection)
        {
            List<NotificationMethodDC> _notificationList = new List<NotificationMethodDC>();
            string StoredProcedureName = "USExceGMSNotificationsGetNotificationsByMemberID";

            try
            {
                DbCommand cmd = CreateCommand(System.Data.CommandType.StoredProcedure, StoredProcedureName);

                cmd.Parameters.Add(DataAcessUtils.CreateParam("@branchID", DbType.Int32, _branchId));
                cmd.Parameters.Add(DataAcessUtils.CreateParam("@memberId", DbType.Int32, _memberID));

                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    NotificationMethodDC _notificationMethod = new NotificationMethodDC();

                    if (!string.IsNullOrEmpty(Convert.ToString(reader["MethodID"]).Trim()))
                    {
                        _notificationMethod.Id = Convert.ToInt32(reader["MethodID"]);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["MethodDueDate"]).Trim()))
                    {
                        _notificationMethod.DueDate = Convert.ToDateTime(reader["MethodDueDate"]);

                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["Occurance"]).Trim()))
                    {
                        string occuranceType = reader["Occurance"].ToString();
                        _notificationMethod.Occurance = (OccuranceType)Enum.Parse(typeof(OccuranceType), occuranceType);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["Method"]).Trim()))
                    {
                        string methodType = reader["Method"].ToString();
                        _notificationMethod.Method = (NotifyMethodType)Enum.Parse(typeof(NotifyMethodType), methodType);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["Notification"]).Trim()))
                    {
                        _notificationMethod.Notification = Convert.ToString(reader["Notification"]);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["NotificationID"]).Trim()))
                    {
                        _notificationMethod.NotificationID = Convert.ToInt32(reader["NotificationID"]);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["NextDueDate"]).Trim()))
                    {
                        _notificationMethod.NextDueDate = Convert.ToDateTime(reader["NextDueDate"]);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(reader["SourceID"]).Trim()))
                    {
                        _notificationMethod.SourceID = Convert.ToInt32(reader["SourceID"]);
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(reader["Source"]).Trim()))
                    {
                        _notificationMethod.Source = Convert.ToString(reader["Source"]);
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(reader["ReceiverEntityID"]).Trim()))
                    {
                        _notificationMethod.ReceiverEntityID = Convert.ToInt32(reader["ReceiverEntityID"]);
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(reader["Role"]).Trim()))
                    {
                        _notificationMethod.Role = Convert.ToString(reader["Role"]);
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(reader["IsNotified"]).Trim()))
                    {
                        try
                        {
                            _notificationMethod.IsNew = !(Convert.ToBoolean(reader["IsNotified"]));
                        }
                        catch
                        {
                        }
                    }

                    _notificationList.Add(_notificationMethod);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _notificationList;
        }
    }
}

