export interface IMessageOptions {
    messageTitle?: string;
    messageBody?: string;
    msgBoxId?: string;
    optionalData?: any;
    IsVisibleYes?: boolean;
    IsVisibleNo?: boolean;
    IsVisibleOk?: boolean;
}

