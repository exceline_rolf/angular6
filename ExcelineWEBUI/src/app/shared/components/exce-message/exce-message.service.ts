import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { IMessageOptions } from '../../../shared/components/exce-message/i-message-options';

@Injectable()
export class ExceMessageService {
  private componentMethodCallSource = new Subject<any>();
  public yesCalled = new Subject<any>();
  public noCalled = new Subject<any>();
  public okCalled = new Subject<any>();
  componentMethodCalled$ = this.componentMethodCallSource.asObservable();
  yesCalledHandle = this.yesCalled.asObservable();
  noCalledHandle = this.noCalled.asObservable();
  okCalledHandle = this.okCalled.asObservable();


  openMessageBox(msgType: string, options: IMessageOptions = {}) {
    this.componentMethodCallSource.next(
      {
        msgType: msgType,
        msgTitle: options.messageTitle,
        msgBody: options.messageBody,
        msgBoxId: options.msgBoxId,
        optionalData: options.optionalData,
        IsVisibleYes: options.IsVisibleYes,
        IsVisibleNo: options.IsVisibleYes,
        IsVisibleOk: options.IsVisibleOk

      });
  }

  yesClickHandler(Id, optionalData?: any) {
    this.yesCalled.next({ id: Id, optionalData: optionalData });
  }

  noClickHandler(Id) {
    this.noCalled.next({ id: Id });
  }

  okClickHandler(Id) {
    this.okCalled.next({ id: Id });
  }

}
