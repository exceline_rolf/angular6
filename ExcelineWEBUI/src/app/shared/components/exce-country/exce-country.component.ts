import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ExceCountryService } from 'app/shared/components/exce-country/exce-country.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-exce-country',
  templateUrl: './exce-country.component.html',
  styleUrls: ['./exce-country.component.scss']
})
export class ExceCountryComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  public countryDataForm: FormGroup;
  constructor(private fb: FormBuilder, private countryService: ExceCountryService) { }

  ngOnInit() {
    this.countryDataForm = this.fb.group({
      'Id': [null, Validators.required],
      'Name': [null, Validators.required],
      'CountryCode': [null, Validators.required]
    });
  }

  saveCountryData() {
    const countryData = this.countryDataForm.value;
    this.countryService.addCountryDetails(countryData).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        if (result) {
          // this.countries.push({ Id: countryData.Id, Name: countryData.Name, CountryCode: countryData.CountryCode })
          // const selectedCountry = this.countries.filter(x => x.Id === countryData.Id);
          // if (selectedCountry.length > 0) {
          // }
        }
      }
    )
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
