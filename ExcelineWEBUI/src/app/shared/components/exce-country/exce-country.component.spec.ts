import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExceCountryComponent } from './exce-country.component';

describe('ExceCountryComponent', () => {
  let component: ExceCountryComponent;
  let fixture: ComponentFixture<ExceCountryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExceCountryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExceCountryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
