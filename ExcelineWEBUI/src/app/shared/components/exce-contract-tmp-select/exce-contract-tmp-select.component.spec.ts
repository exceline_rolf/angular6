import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExceContractTmpSelectComponent } from './exce-contract-tmp-select.component';

describe('ExceContractTmpSelectComponent', () => {
  let component: ExceContractTmpSelectComponent;
  let fixture: ComponentFixture<ExceContractTmpSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExceContractTmpSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExceContractTmpSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
