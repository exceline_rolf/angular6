import {
  Component, Input, Output, EventEmitter, ElementRef, ContentChildren, QueryList,
  TemplateRef, ContentChild, ViewChildren, OnInit, ViewChild, OnDestroy
} from '@angular/core';
import { DataTableResource } from 'app/shared/components/us-data-table/tools/data-table-resource';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ExceEntityselectionService } from 'app/shared/components/exce-entityselection/exce-entityselection.service';
import { Member } from 'app/shared/SystemObjects/Common/Member';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'exce-entityselection',
  templateUrl: './exce-entityselection.component.html',
  styleUrls: ['./exce-entityselection.component.scss']
})
export class ExceEntityselectionComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  public memberSearchForm: FormGroup;
  public basicInfoMember: Member;
  private addMemberRegisterView: any;
  private _itemcount = 0;
  private _itemList = [];

  config: any = { 'placeholder': '', 'sourceField': ['searchText'] };
  roleList = [{ id: 'ALL', name: 'COMMON.AllC' }, { id: 'MEM', name: 'COMMON.MemberC' }, { id: 'COM', name: 'COMMON.CompanyC' }, { id: 'SPO', name: 'COMMON.SponsorC' }];
  statusList = [{ id: 10, name: 'COMMON.AllC' }, { id: 1, name: 'COMMON.ActiveC' }, { id: 9, name: 'COMMON.InactiveC' }];
  private searchVal = '';
  private searchText = ''
  private selectedBranch: any;
  private selectedRole: any;
  private selectedStatus: any;
  private selectedRowItem: any;
  butDisabled: Boolean = true;
  items = [];
  itemCount = 0;
  selectedList = [];
  limit = 50;
  @Input() isBranchVisible = true;
  @Input() isStatusVisible = true;
  @Input() isRoleVisible = true;
  @Input() isAddBtnVisible = true;
  @Input() isBasicInforVisible = true;
  @Input() isDisabledRole = true;
  @Input() isDisabledStatus = false;
  @Input() isDisabledBranch = false;
  @Input() defaultRole = 'ALL'
  @Input() defaultStatus = 10
  @Input() defaultBranch = -2
  @Input() entitySelectionType: string;
  @Input() multipleSelect = false;
  @Input() branches = [];
  @Input() auotocompleteItems: any;
  @Input() columns: any;
  @Input() isDbPagination: boolean;
  @Input() clickable = true;
  private itemResource?: any;
  @ViewChild('memberList') public dataTable;
  @Output() selectedItem = new EventEmitter();
  @Output() selectedItems = new EventEmitter();
  @Output() searchEvent = new EventEmitter();
  @Output() addMemberEvent = new EventEmitter();
  @Output() closeViewEvent = new EventEmitter();
  @ViewChild('autobox') public autocomplete;
  @Input() showOkButton = true;


  @Input()
  get itemcount() {
    return this._itemcount;
  }
  set itemcount(value) {
    this._itemcount = value;
  }

  @Input()
  get itemList() {
    return this.items;
  }
  set itemList(value) {
    this.items = value;
    if (!this.isDbPagination) {
      this.itemResource = new DataTableResource(value);
      this.itemResource.query({ offset: 0, limit: this.limit }).then(items => this.items = items);
      this.itemcount = Number(this.items.length);
    }
  }

  constructor(
    private elRef: ElementRef,
    private fb: FormBuilder,
    private entityService: ExceEntityselectionService,
    private translateService: TranslateService) {
    this.memberSearchForm = fb.group({
      'branchSelected': [this.selectedBranch],
      'roleSelected': [this.selectedRole],
      'statusSelected': [this.selectedStatus],
      'searchVal': [this.searchVal]
    });
  }

  ngOnInit() {
    // this.isDisabledRole = true;
    this.autocomplete.items = this.auotocompleteItems;
    this.autocomplete.setSearchValue(this.auotocompleteItems);
    let all = '';
    this.translateService.get('COMMON.AllC').pipe(
      takeUntil(this.destroy$)
    ).subscribe(tranlstedValue => {
      all = tranlstedValue
    }, null, null);
    const branchList = [{ BranchId: -2, BranchName: all }];
    this.branches = branchList;
    if (this.isBranchVisible) {
      this.entityService.getBranches(-1).pipe(
        takeUntil(this.destroy$)
      ).subscribe(branches => {
        if (branches) {
          branches.Data.map(item => {
            return {
              BranchId: item.Id,
              BranchName: item.BranchName
            }
          }).forEach(item => branchList.push(item));
          this.branches = branchList;
        }
      }, null, null);
    }
    this.selectedRole = this.roleList.find(x => x.id === this.defaultRole);
    this.selectedStatus = this.statusList.find(x => x.id === this.defaultStatus);
    this.selectedBranch = this.branches.find(x => x.BranchId === this.defaultBranch);
    this.isDisabledRole ? this.memberSearchForm.get('roleSelected').disable() : this.memberSearchForm.get('roleSelected').enable();
    this.isDisabledStatus ? this.memberSearchForm.get('statusSelected').disable() : this.memberSearchForm.get('statusSelected').enable();
    this.isDisabledBranch ? this.memberSearchForm.get('branchSelected').disable() : this.memberSearchForm.get('branchSelected').enable()
  }

  multipleSelectItem() {
    this.dataTable.selectedRows.forEach(element => {
      this.selectedList.push(element.item);
    });
    if (this.multipleSelect) {
      this.selectedItems.emit(this.selectedList);
    } else {
      this.selectedItem.emit(this.selectedRowItem);
    }
  }

  cancelView() {
    this.closeViewEvent.emit();
  }

  rowClick(rowEvent) {
    this.selectedRowItem = rowEvent.row.item;
  }

  rowDoubleClick(rowEvent) {
    if (this.clickable) {
      this.selectedRowItem = rowEvent.row.item;
      this.selectedItem.emit(this.selectedRowItem);
      // this.cancelView(); // Close signal sent from add-new-booking instead
    }
  }

  rowTooltip(item) { return item.jobTitle; }

  clickButton(item) {
  }

  onSelect(item: any) {
    this.searchVal = item.searchText;
    this.memberSearch(null);
  }

  onInputChangedEvent(val: string) {
  }

  closedSearch() {
    this.searchVal = '';
    this.searchText = '';
    this.autocomplete.textValue = '';
    this.memberSearch(null);
  }

  memberSearch(value: any) {
    this.dataTable._offset = 0;
    const searchDetails = [{
      'hit': 1, 'roleSelected': this.selectedRole, 'statusSelected': this.selectedStatus
      , 'branchSelected': this.selectedBranch, 'searchVal': this.searchVal, 'entitySelectionType': this.entitySelectionType
    }]
    this.searchEvent.emit(searchDetails[0]);
  }

  addMemberModal() {
    this.addMemberEvent.emit();
  }

  reloadItems(params) {
    if (this.isDbPagination) {
      const hit = (params.offset / params.limit) + 1;
      const searchDetails = [{
        'hit': hit, 'roleSelected': this.selectedRole, 'statusSelected': this.selectedStatus,
        'branchSelected': this.selectedBranch, 'searchVal': this.searchVal, 'entitySelectionType': this.entitySelectionType
      }]
      this.searchEvent.emit(searchDetails[0]);
    } else {
      this.itemResource.query(params).then(items => this.items = items);
      this.itemResource.count().then(count => this.itemcount = count);
    }
  }

  openNav(item: any) {
    this.entityService.getMemberBasicInfo(item.Id).pipe(
      takeUntil(this.destroy$)
      ).subscribe( result => {
        if (result) {
          document.getElementById('mb-s-basicinfo-slide').style.width = '300px';
          this.elRef.nativeElement.querySelector('.off-canvas-panel-ani').style.marginRight = '300px';
          this.basicInfoMember = result.Data;
        } else {
        }
      }
      )

  }
  closeNav() {
    document.getElementById('mb-s-basicinfo-slide').style.width = '0';
    this.elRef.nativeElement.querySelector('.off-canvas-panel-ani').style.marginRight = '0';
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
