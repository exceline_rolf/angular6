import { Component, OnInit, OnDestroy } from '@angular/core';
import { ExceBreadcrumbService } from './exce-breadcrumb.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { PlatformLocation } from '@angular/common';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-exce-breadcrumb',
  templateUrl: './exce-breadcrumb.component.html',
  styleUrls: ['./exce-breadcrumb.component.scss']
})
export class ExceBreadcrumbComponent implements OnInit, OnDestroy {
  breadCrumbs = [];
  private destroy$ = new Subject<void>();
  // reomveShopInstance: Subscription;

  constructor(
    private location: PlatformLocation,
    private router: Router,
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {
    location.onPopState(() => {
      if (this.breadCrumbs.length > 0) {
        this.breadCrumbs.pop();
        this.breadCrumbs.pop();
      } else {
        this.router.navigateByUrl('/');
      }
    });
  }

  ngOnInit() {
    this.exceBreadcrumbService.addBreadCumbItem.pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      newBreadCrumb => {
        /* Check if the potential breadcrumb already exists  */
        if (this.breadCrumbs.findIndex(existingBreadcrumbs => existingBreadcrumbs.name === newBreadCrumb.name) === -1) {
          this.breadCrumbs.push(newBreadCrumb);
        }
      }, null, null);
    // this.reomveShopInstance = this.exceBreadcrumbService.reomveShopInstance.subscribe(
    //   result => {
    //     const len = this.breadCrumbs.length - 1
    //     const lastEl = this.breadCrumbs[len]
    //     if (lastEl.name === 'SHOP') {
    //       this.breadCrumbs.pop();
    //     }
    //   }
    // )
  }

  navigateTo(item: any, index: number): void {
    this.breadCrumbs = this.breadCrumbs.slice(0, index + 1);
    this.router.navigateByUrl(item.url);
  }

  navigateToHome(): void {
    this.router.navigateByUrl('/');
    this.breadCrumbs = [];
  }

  closeClicked() {
    this.breadCrumbs.splice(-1, 1);
    if (this.breadCrumbs.length > 0) {
      this.router.navigateByUrl(this.breadCrumbs[this.breadCrumbs.length - 1].url);
      this.breadCrumbs.pop();
      this.breadCrumbs.pop();
    } else {
      this.router.navigateByUrl('/');
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
