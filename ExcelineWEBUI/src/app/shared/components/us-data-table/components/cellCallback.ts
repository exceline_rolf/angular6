import { DataTableRow } from './row/row.component';
import { DataTableColumn } from './column/column.component';

export type CellCallback = (item: any, row: DataTableRow, column: DataTableColumn, index: number) => string;