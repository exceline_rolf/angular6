import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExcePostalcodeComponent } from './exce-postalcode.component';

describe('ExcePostalcodeComponent', () => {
  let component: ExcePostalcodeComponent;
  let fixture: ComponentFixture<ExcePostalcodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExcePostalcodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExcePostalcodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
