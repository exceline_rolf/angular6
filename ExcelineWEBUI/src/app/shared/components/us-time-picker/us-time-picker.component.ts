import {
  Component, ElementRef, OnInit, Renderer2, Input, forwardRef,
  ViewContainerRef, ViewChild, EventEmitter, Output, HostListener, ChangeDetectionStrategy, OnChanges
} from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor, } from '@angular/forms';
import * as moment from 'moment';

export interface TimeSelectorConfig {
  format: string;
  is24hourFormat?: boolean;
  hoursStep?: number;
  minutesStep?: number;
}

export interface ITimeInputFieldChanged {
  value: string;
  dateFormat: string;
  valid: boolean;
}

enum TimeType {
  HOURS,
  MINUTES,
  SECONDS,
  PERIOD
}
enum CalToggle { Open = 1, CloseByDateSel = 2, CloseByCalBtn = 3, CloseByOutClick = 4, CloseByEsc = 5, CloseByApi = 6 }

const noop = () => {
};

export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => UsTimePickerComponent),
  multi: true
};

@Component({
  selector: 'us-time-picker',
  templateUrl: './us-time-picker.component.html',
  styleUrls: ['./us-time-picker.component.scss'],
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR],
  // changeDetection: ChangeDetectionStrategy.OnPush
})

export class UsTimePickerComponent implements ControlValueAccessor, OnInit, OnChanges {
  isDisabled: boolean;

  public showSelector = false;
  private timeOptions = [];

  private hours: string;
  private minutes: string;
  private seconds: string;
  private period: string;

  private thisElement: HTMLElement;
  inputElement: HTMLInputElement;
  private tabIndex: number;
  private focusedIdx = 48; // Sets the focused element to 12:00 by default. 4 units/hour * 12 hour = 48 units

  @Input()
  private selectedTime?= '';
  private curserPosition = 0;

  @Input() private isDateTime = false
  @Input() disableButton = false;

  private onTouchedCallback: () => void = noop;
  private onChangeCallback: (_: any) => void = noop;


  @ViewChild('options') optionsList: ElementRef;

  private optionsTargetEl: ElementRef;

  @Input()
  private config?: TimeSelectorConfig = { format: 'MM:mm', is24hourFormat: true };

  @ViewChild('options') optionsEl: ElementRef;
  @ViewChild('time') timeEl: ElementRef;
  @Output() timeChanged: EventEmitter<string> = new EventEmitter<string>();
  @Output() inputFieldChanged: EventEmitter<ITimeInputFieldChanged> = new EventEmitter<ITimeInputFieldChanged>();
  constructor(public _elementRef: ElementRef,
    private _renderer: Renderer2) {
    this.thisElement = _elementRef.nativeElement;
    _renderer.listen('document', 'click', (event: any) => {
      if (this.showSelector && event.target && this._elementRef.nativeElement !== event.target && !this._elementRef.nativeElement.contains(event.target)) {
        this.showSelector = false;
      }
    });

  }

  ngOnInit(): void {
    this.inputElement = <HTMLInputElement>(this.thisElement.querySelector('input'));
    let hourOptions: any[] = [];
    let minuteOptions: any[] = [];
    let step: number;

    step = (this.config && this.config.hoursStep) ? this.config.hoursStep : 1;
    const end: number = (this.config && this.config.is24hourFormat) ? 24 : 13;
    const start: number = (this.config && this.config.is24hourFormat) ? 0 : 1;
    hourOptions = this.getOptions(start, end, step);

    step = (this.config && this.config.minutesStep) ? this.config.minutesStep : 15;
    minuteOptions = this.getOptions(0, 60, step);

    hourOptions.forEach(hour => {
      minuteOptions.forEach(minute => {
        this.timeOptions.push({
          hour: hour,
          minute: minute
        });
      });
    });
    this.inputElement.focus();


    if (this.isDateTime) {
      if (this.selectedTime) {
        if (this.selectedTime.length > 5) {
          let timeO = this.selectedTime.split('T')[1]
          let hourMins = timeO.split(':')[0] + ':' + timeO.split(':')[1]
          this.selectedTime = hourMins
          this.value = hourMins
        }
      }

    }
  }

  ngOnChanges() {

    if (this.isDateTime) {
      setTimeout(() => {
        if (this.selectedTime) {

          if (this.selectedTime.length > 5) {
            let timeO = this.selectedTime.split('T')[1]
            let hourMins = timeO.split(':')[0] + ':' + timeO.split(':')[1]
            this.selectedTime = hourMins

            this.value = hourMins
            this.writeValue(this.value);
          }
        }

      }, 100);

    }
  }


  // get accessor
  get value(): any {
    return this.selectedTime
  };

  // set accessor including call the onchange callback
  set value(v: any) {
    if (v !== this.selectedTime) {
      if (this.isDateTime) {
        this.selectedTime = '2017-09-08T' + v + ':00';
        this.value = '2017-09-08T' + v + ':00'
      } else {
        this.selectedTime = v
      }
      this.onChangeCallback(v);
      this.timeChanged.emit(this.selectedTime)
      if (this.inputElement) {
        this.inputElement.selectionEnd = this.curserPosition;
      }
    }
  }



  // Set touched on blur
  onBlur() {
    this.onTouchedCallback();
  }

  buildValue() {
    if (this.hours === undefined) {
      return;
    }
    let hrs: number = Number(this.hours);
    if (!this.config.is24hourFormat && this.period) {
      if (this.period === 'AM') {
        hrs = (hrs === 12) ? 0 : hrs;
      } else {
        hrs = (hrs === 12) ? hrs : hrs + 12;
      }
    }
    return moment().hours(hrs).minutes(this.minutes ? Number(this.minutes) : 0).
      seconds(this.seconds ? Number(this.seconds) : 0).format(this.config.format);
  }

  writeValue(value: any): void {
    if (value !== this.selectedTime) {
      this.selectedTime = value;
      this._renderer.setProperty(this._elementRef.nativeElement, 'value', this.value);
    }
  }

  updateValue() {
    this.writeValue(this.selectedTime);
  }

  setFromDate(): void {
    const date = moment(this.value, this.config.format);
    this.hours = date.format(this.config.is24hourFormat ? 'HH' : 'hh');
    this.minutes = date.format('mm');
    this.period = !this.config.is24hourFormat ? date.format('A') : '';
  }


  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }

  private getOptions(start: number, end: number, step: number): string[] {
    const options: string[] = [];
    for (let i = start; i < end; i += step) {
      options.push(i < 10 ? '0' + i.toString() : i.toString());
    }
    return options;
  }

  registerOnTouched(fn: () => any): void { this.onTouchedCallback = fn; }

  setDisabledState?(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }

  onTriggerClicked() {
    this.showSelector = true;
    const that = this;

    if (this.selectedTime) {
      setTimeout(function () {
        that
          .optionsList
          .nativeElement
          .getElementsByTagName('li')[that.focusedIdx]
          .focus();
      }, 10);
    } else {
      setTimeout(function () {
        that
          .optionsList
          .nativeElement
          .getElementsByTagName('li')[0]
          .focus();
      }, 10);
    }
  }

  optionSelected(index, option) {
    this.focusedIdx = Number(index);
    this.showSelector = false;
    this.hours = option.hour;
    this.minutes = option.minute;
    this.value = this.hours + ':' + this.minutes;
    this.updateValue();
  }

  handleDownKeyEvent(event: Event, item): void {
    if ((this.focusedIdx >= 0) && (this.focusedIdx < (this.timeOptions.length - 1))) {
      this.focusedIdx++;
      const target = this
        .optionsList
        .nativeElement
        .getElementsByTagName('li')[this.focusedIdx];
      target.focus();
      const hour = this.timeOptions[this.focusedIdx].hour;
      const minute = this.timeOptions[this.focusedIdx].minute;
      this.value = ((String(hour).length === 1) ? ('0' + hour) : hour) + ':' +
        ((String(minute).length === 1) ? ('0' + minute) : minute);
      this.updateValue();
    }

  }

  onScroll(event) {
    // event.preventDefault();
    // event.stopPropagation();
    // event.srcElement.scrollTop = 25 * this.focusedIdx;
  }
  // mouseWheelUpFunc(event) {
  // }

  handleUpKeyEvent(event: Event, item): void {
    if ((this.focusedIdx >= 1) && (this.focusedIdx < (this.timeOptions.length))) {
      this.focusedIdx--;
      const target = this
        .optionsList
        .nativeElement
        .getElementsByTagName('li')[this.focusedIdx];
      target.focus();
      const hour = this.timeOptions[this.focusedIdx].hour;
      const minute = this.timeOptions[this.focusedIdx].minute;
      this.value = ((String(hour).length === 1) ? ('0' + hour) : hour) + ':' +
        ((String(minute).length === 1) ? ('0' + minute) : minute);
      this.updateValue();
    }
  }

  onKeyDownUp(event) {
    event.preventDefault();
    event.stopPropagation();
    this._keyUp(event.target);

  }

  onUpKeyPress(event, target) {
    event.preventDefault();
    event.stopPropagation();
    this._keyUp(target);
    target.focus();
    target.selectionEnd = this.curserPosition;
    this.setCurser();
  }

  private _keyUp(target) {
    this.curserPosition = target.selectionEnd;
    let [hour, minute] = target.value.split(':');
    if (
      (target.selectionEnd === 0) ||
      (target.selectionEnd === 1) ||
      (target.selectionEnd === 2)
    ) {
      if (Number(hour) === 23) {
        hour = 0;
      } else {
        hour = Number(hour) + 1;
      }
    } else if (
      (target.selectionEnd === 3) ||
      (target.selectionEnd === 4) ||
      (target.selectionEnd === 5)
    ) {
      if (Number(minute) === 59) {
        minute = 0;
        if (Number(hour) === 23) {
          hour = 0;
        } else {
          hour = Number(hour) + 1;
        }
      } else {
        minute = Number(minute) + 1;
      }
    }

    this.value = ((String(hour).length === 1) ? ('0' + hour) : hour) + ':' +
      ((String(minute).length === 1) ? ('0' + minute) : minute);
    this.updateValue();
  }

  onKeyDownDown(event) {
    event.preventDefault();
    event.stopPropagation();
    this._keyDown(event.target);
  }

  onDownKeyPress(event, target) {
    event.preventDefault();
    event.stopPropagation();
    this._keyDown(target);
    target.focus();
    target.selectionEnd = this.curserPosition;
    this.setCurser();

  }

  setCurser() {
    setTimeout(() => {
      if (this.inputElement) {
        this.inputElement.selectionEnd = this.curserPosition;
      }
    }, 100);
  }

  private _keyDown(target) {
    this.curserPosition = target.selectionEnd;
    let [hour, minute] = target.value.split(':');
    if (
      (target.selectionEnd === 0) ||
      (target.selectionEnd === 1) ||
      (target.selectionEnd === 2)
    ) {
      if (Number(hour) === 0) {
        hour = 23;
      } else {
        hour = Number(hour) - 1;
      }

    } else if (
      (target.selectionEnd === 3) ||
      (target.selectionEnd === 4) ||
      (target.selectionEnd === 5)
    ) {
      if (Number(minute) === 0) {
        minute = 59;
        if (Number(hour) === 0) {
          hour = 23;
        } else {
          hour = Number(hour) - 1;
        }
      } else {
        minute = Number(minute) - 1;
      }

    }
    this.value = ((String(hour).length === 1) ? ('0' + hour) : hour) + ':' +
      ((String(minute).length === 1) ? ('0' + minute) : minute);
    this.updateValue();
  }

  onKeyUp(event) {
    event.preventDefault();
    event.stopPropagation();
    event.target.selectionEnd = this.curserPosition;
  }

}

export class TimeSelectorOptions {
  private _timeOptions: string[] = [];
  private _top: number;
  private _left: number;
  private _targetCls: string;

  public optionSelected: (value: string) => void = () => { };

  set timeOptions(timeOptions: string[]) {
    this._timeOptions = timeOptions;
  }

  get timeOptions() {
    return this._timeOptions;
  }


  set top(top: number) {
    this._top = top;
  }

  get top() {
    return this._top;
  }

  set left(left: number) {
    this._left = left;
  }

  get left() {
    return this._left;
  }

  set targetCls(targetCls: string) {
    this._targetCls = targetCls;
  }

  get targetCls() {
    return this._targetCls;
  }

}
