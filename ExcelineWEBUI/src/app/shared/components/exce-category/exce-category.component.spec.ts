import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExceCategoryComponent } from './exce-category.component';

describe('ExceCategoryComponent', () => {
  let component: ExceCategoryComponent;
  let fixture: ComponentFixture<ExceCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExceCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExceCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
