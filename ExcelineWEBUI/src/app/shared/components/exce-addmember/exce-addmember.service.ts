import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CommonHttpService } from '../../../shared/services/common-http.service';
import { ConfigService } from '@ngx-config/core';

@Injectable()
export class ExceAddmemberService {
  private userApiURL: string;
  private memberApiURL: string;
  // private componentMethodCallSource = new Subject<any>();
  // componentMethodCalled$ = this.componentMethodCallSource.asObservable();

  constructor(private commonHttpService: CommonHttpService,
    private config: ConfigService) {
    this.userApiURL = this.config.getSettings('EXCE_API.COMMON');
    this.memberApiURL = this.config.getSettings('EXCE_API.MEMBER');
  }

  saveMember(member: any): Observable<any> {
    const url = this.memberApiURL + '/SaveMember'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: member
    });
  }

  getCountries(): Observable<any> {
    const url = this.memberApiURL + '/GetCountries';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  getSelectedGymSettings(settingParams): Observable<any> {
    const url = this.memberApiURL + '/GetSelectedGymSettings';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: settingParams
    })
  }

  getCategories(type: string): Observable<any> {
    const url = this.memberApiURL + '/GetCategories?type=' + type;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  getCityForPostalCode(postalCode: string): Observable<any> {
    const url = this.memberApiURL + '/GetCityForPostalCode?postalCode=' + postalCode;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  getMembers(branchID: number, searchText: string, statuse: number, searchType: any, memberRole: string, hit: number, isHeaderClick: boolean,
    isAscending: boolean, sortName: string): Observable<any> {
    const url = this.memberApiURL + '/GetMembers?branchID=' + branchID + '&searchText=' + searchText + '&statuse=' + statuse
      + '&searchType=' + searchType + '&memberRole=' + memberRole
      + '&hit=' + hit + '&isHeaderClick=' + isHeaderClick + '&isAscending=' + isAscending + '&sortName=' + sortName

    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  getBranches(branchId: number): Observable<any> {
    const url = this.memberApiURL + '/GetBranches?branchId=' + branchId

    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  checkEmailExits(email: string): boolean {
    if (email === 'nuwan@gmail.com') {
      return true;
    } else {
      return false;
    }

  }

  validateMobile(mobile: string, mobilePrifix: string): Observable<any> {
    const url = this.memberApiURL + '/ValidateMobile?mobile=' + mobile + '&mobilePrefix=' + mobilePrifix;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

}
