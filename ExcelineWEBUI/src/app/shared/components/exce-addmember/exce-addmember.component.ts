import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl, ValidationErrors } from '@angular/forms';
import { ExceAddmemberService } from 'app/shared/components/exce-addmember/exce-addmember.service';
import { Cookie } from 'ng2-cookies/src/services';
import { BankAccountValidator } from 'app/shared/directives/exce-error/util/validators/bank-account-validator';
import { ColumnDataType, MemberSearchType } from 'app/shared/enums/us-enum';
import { UsbModal } from 'app/shared/components/us-modal/us-modal';
import { Member } from 'app/shared/SystemObjects/Common/Member';
import { UsErrorService } from 'app/shared/directives/us-error/us-error.service';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil, mergeMap, count } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'exce-addmember',
  templateUrl: './exce-addmember.component.html',
  styleUrls: ['./exce-addmember.component.scss']
})
export class ExceAddmemberComponent implements OnInit, OnDestroy {
  @Input() roleType: any;
  @Input() IsFollowUp: any;
  @Output() addItemEvent = new EventEmitter();
  @Output() canceleAddNewMember = new EventEmitter();
  public myFormSubmited = false;
  public isEmailBlur = false;
  private registerMember: Member;
  private loggedBranchId: number;
  private memberBirthdate: any;
  public memberForm: FormGroup;
  intlOptions: any;
  countries: any;
  selectedCountry: any;
  gymSettings: any;
  selectedGender = 1;
  postalDataView: any;
  hit = 1;
  isAddBtnVisible = false;
  isDisabledRole = false;
  entitySelectionViewTitle: string;
  selectedGymsettings: string[] = ['ReqAddressRequired', 'ReqMobileRequired', 'ReqZipCodeRequired', 'OtherIsShopAvailable', 'OtherPhoneCode'];
  public isMobileBlur = false;
  isPrivateBlur = false;
  public isWorkBlur = false;
  public phoneNumberDetail: string;
  private isMobileNumberValid = true;
  private isPrivateNumberValid = true;
  private isWorkNumberValid = true;
  private mobileExitMsg: string;
  private destroy$ = new Subject<void>();

  items = [];
  branches: any;
  private newMemberRegisterView: any;
  auotocompleteItems = [{ id: 'NAME', name: 'NAME :', isNumber: false }, { id: 'ID', name: 'Id :', isNumber: true }]
  columns = [{ property: 'FirstName', header: 'First Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
  { property: 'LastName', header: 'Last Name', sortable: false, resizable: false, width: '150px', type: ColumnDataType.DEFAULT },
  { property: 'IntCustId', header: 'Number', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
  { property: 'Age', header: 'Age', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
  { property: 'TelMobile', header: 'Mobile', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
  { property: 'StatusName', header: 'Status', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
  { property: 'BranchName', header: 'Branch Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT }
  ]

  formErrors = {
    'FirstName': '',
    'LastName': '',
    'Address1': '',
    'Email': '',
    'AccountNo': '',
    'Mobile': '',
    'PrivateTeleNo': '',
    'WorkTeleNo': '',
    'intelNumber': '',
    'PostCode': '',
  };

  validationMessages = {
    'FirstName': {
      'required': 'First Name is required.'
    },
    'LastName': {
      'required': 'Last Name is required.'
    },
    'Address1': {
      'required': 'Address is required.'
    },
    'Email': {
      'email': 'Please enter valid email address',
      'emailTaken': 'email exit'
    },
    'PostCode': {
      'required': 'PostCode is required.'
    },
    'Mobile': {
      'required': 'Mobile is required.',
      'phoneNumber': 'invalid phone number',
      'pattern': 'Phone number can only have numbers',
      'mobileTaken': 'Mobile exit'
    },
    'PrivateTeleNo': {
      'phoneNumber': 'invalid phone number',
      'pattern': 'Phone number can only have numbers',
    },
    'WorkTeleNo': {
      'phoneNumber': 'invalid phone number',
      'pattern': 'Phone number can only have numbers',
    },
    'AccountNo': {
      'BankAccount': 'Invalid Bank Account No.'
    },
    'intelNumber': {
      'required': 'phone number is required.',
      'phoneNumber': 'invalid phone number'
    }
  };

  listOfCountryId: any;

  constructor(
    private fb: FormBuilder,
    private modalService: UsbModal,
    private memberService: ExceAddmemberService,
    private translateService: TranslateService
  ) {

  }
  ngOnInit() {
    this.loggedBranchId = JSON.parse(Cookie.get('selectedBranch')).BranchId;

    this.registerMember = new Member();
    this.memberForm = this.fb.group({
      // 'Status': [this.selectedStatus],
      'LastName': [this.registerMember.LastName, this.roleType === 'MEM' ? Validators.required : null],
      'FirstName': [this.registerMember.FirstName, this.roleType === 'MEM' ? Validators.required : null],
      'CompanyName': [this.registerMember.CompanyName, this.roleType === 'COM' ? Validators.required : null],
      'Address1': [this.registerMember.Address1],
      'Address2': [this.registerMember.Address2],
      'PostCode': [this.registerMember.PostCode],
      'PostPlace': [this.registerMember.PostPlace],
      'country': [null],
      'MemberCardNo': [this.registerMember.MemberCardNo],
      'MobilePrefix': [this.registerMember.MobilePrefix],
      // 'Mobile': [this.registerMember.Mobile, [Validators.required], [this.phoneNumberValidator.bind(this)] ],
      'Mobile': [this.registerMember.Mobile],
      'PrivateTeleNoPrefix': [this.registerMember.PrivateTeleNoPrefix],
      'PrivateTeleNo': [this.registerMember.PrivateTeleNo, [], [this.privateNumberValidator.bind(this)]],
      'WorkTeleNoPrefix': [this.registerMember.WorkTeleNoPrefix],
      'WorkTeleNo': [this.registerMember.WorkTeleNo, [], [this.workNumberValidator.bind(this)]],
      // 'Email': [this.registerMember.Email, Validators.email, this.validateEmailNotTaken.bind(this)],
      'Email': [this.registerMember.Email, this.customEmailValidator.bind(this)],
      'AccountNo': [this.registerMember.AccountNo, [BankAccountValidator.BankAccount]],
      'CompanyEmail': [this.registerMember.CompanyEmail],
      'BirthDate': [{ date: this.memberBirthdate }],
      'FromDate': [this.registerMember.FromDate],
      'ToDate': [this.registerMember.ToDate],
      'Age': [this.registerMember.Age],
      'IsFollowUp': [this.registerMember.IsFollowUp],
      'LinkMemberName': [this.registerMember.LinkMemberName]
    })

    this.memberService.getSelectedGymSettings({ settingNames: this.selectedGymsettings, isGymSetting: true, branchId: this.loggedBranchId }).pipe(
      mergeMap(result => {
        if (result) {
          this.gymSettings = result.Data;
          if (this.gymSettings.ReqAddressRequired === true) {
            this.memberForm.get('Address1').setValidators(Validators.required);
            this.memberForm.get('Address1').updateValueAndValidity();
          }

          if (this.gymSettings.ReqZipCodeRequired === true) {
            this.memberForm.get('PostCode').setValidators(Validators.required);
            this.memberForm.get('PostCode').updateValueAndValidity();
          }

          if (this.gymSettings.ReqMobileRequired === true) {
            this.memberForm.get('Mobile').setValidators(Validators.required);
            this.memberForm.get('Mobile').updateValueAndValidity();
          }
          this.memberForm.get('Mobile').setAsyncValidators(this.mobileNumberValidator.bind(this));
          this.memberForm.get('Mobile').updateValueAndValidity();

          return this.memberForm.statusChanges;
        }
      }),
      takeUntil(this.destroy$)
    ).subscribe(result => {
      UsErrorService.onValueChanged(this.memberForm, this.formErrors, this.validationMessages);
    }, null, null);


    // this.memberService.getSelectedGymSettings({ settingNames: this.selectedGymsettings, isGymSetting: true, branchId: this.loggedBranchId }).pipe(
    //   takeUntil(this.destroy$)
    // ).subscribe(
    //   result => {
    //     if (result) {
    //       this.gymSettings = result.Data;
    //       if (this.gymSettings.ReqAddressRequired === true) {
    //         this.memberForm.get('Address1').setValidators(Validators.required);
    //         this.memberForm.get('Address1').updateValueAndValidity();
    //       }

    //       if (this.gymSettings.ReqZipCodeRequired === true) {
    //         this.memberForm.get('PostCode').setValidators(Validators.required);
    //         this.memberForm.get('PostCode').updateValueAndValidity();
    //       }

    //       if (this.gymSettings.ReqMobileRequired === true) {
    //         this.memberForm.get('Mobile').setValidators(Validators.required);
    //         this.memberForm.get('Mobile').updateValueAndValidity();
    //       }
    //       this.memberForm.get('Mobile').setAsyncValidators(this.mobileNumberValidator.bind(this));
    //       this.memberForm.get('Mobile').updateValueAndValidity();

    //       this.memberForm.statusChanges.subscribe(_ => {
    //         UsErrorService.onValueChanged(this.memberForm, this.formErrors, this.validationMessages)
    //       });
    //     }
    //   }
    // )

    // this.memberForm.statusChanges.subscribe(_ => {
    //   UsErrorService.onValueChanged(this.memberForm, this.formErrors, this.validationMessages)
    // });

    // this.memberService.getCountries().subscribe(
    //   result => {
    //     if (result) {
    //       this.countries = result.Data;
    //       const selectedCountry = this.countries.filter(x => x.Id === 'NO');
    //       if (selectedCountry.length > 0) {
    //         this.getSelectedCountry(selectedCountry[0].Name, false);
    //       }
    //     }
    //   });

    this.memberService.getCountries().pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        if (result) {
          const tempList = [];
          this.countries = result.Data;
          const selectedCountry = this.countries.filter(x => x.Id === 'NO');
          this.countries.forEach(element => {
            let tempName;
            tempList.push(element);
            const translateExpression = 'COUNTRIES.' + element.Id;
            this.translateService.get(translateExpression).subscribe(tranlstedValue => {
              tempName = tranlstedValue
            }, error => {

            }, () => {

            });
            element.DisplayName = tempName;
          });
          this.listOfCountryId = tempList;
          if (selectedCountry.length > 0) {
            this.getSelectedCountry(selectedCountry[0].Name, false);
          }
        }
      }, null, null
    );

      this.intlOptions = {
        initialCountry: 'no',
        formatOnDisplay: true,
        separateDialCode: true,
        onlyCountries: this.listOfCountryId,
        autoPlaceholder: 'off'
      };

      this.registerMember.Gender = 1;

    this.memberService.getCategories('MEMBER').pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        if (result) {
          this.registerMember.Role = this.roleType;
          if (this.roleType === 'COM') {
            this.registerMember.MemberCategory = result.Data.filter(x => x.Code === 'COMPANY');
          } else if (this.roleType === 'MEM') {
            this.registerMember.MemberCategory = result.Data.filter(x => x.Code === 'MEMBER');
          }
        }
      }, null, null
    );
  }

  mobileNumberValidator(control: AbstractControl) {
    return new Promise(resolve => {
      setTimeout(() => {
        if (!this.isMobileNumberValid) {
          resolve({
            'phoneNumber': true
          })
        } else {
          resolve(null);
        }
      }, 10);
    })
  }
  privateNumberValidator(control: AbstractControl) {
    return new Promise(resolve => {
      setTimeout(() => {
        if (!this.isPrivateNumberValid) {
          resolve({
            'phoneNumber': true
          })
        } else {
          resolve(null);
        }
      }, 10);
    })
  }
  workNumberValidator(control: AbstractControl) {
    return new Promise(resolve => {
      setTimeout(() => {
        if (!this.isWorkNumberValid) {
          resolve({
            'phoneNumber': true
          })
        } else {
          resolve(null);
        }
      }, 10);
    })
  }
  validateEmailNotTaken(control: AbstractControl) {
    //   return new Promise(resolve => {
    //     setTimeout(() => {
    //       if (this.isValid) {
    //         resolve({
    //           'emailTaken': true
    //         })
    //       } else {
    //         resolve(null);
    //       }
    //     }, 10);
    //   })
  }

  private customEmailValidator(control: AbstractControl): ValidationErrors {
    if (!control.value) {
      return null;
    }
    return Validators.email(control);
  }

  emailBlur(val: any) {
    this.isEmailBlur = true;
    if (this.memberForm.controls['Email'].valid) { // email is exits check after valid email
      const isEmailExits = this.memberService.checkEmailExits(val);
      if (isEmailExits) {
        this.memberForm.controls['Email'].setErrors({
          'emailTaken': true
        });
      } else {
        //  this.memberForm.controls['Email'].setErrors(
        //   null
        // );
      }
    }
  }

  emailFocus() {
    this.isEmailBlur = false
  }

  fullValueChange(event: any, type: string) {
    this.phoneNumberDetail = event;
    if (type === 'MOBILE') {
      this.memberForm.patchValue({
        Mobile: event.value,
      });
      this.memberForm.patchValue({
        MobilePrefix: event.extension
      });
      this.isMobileNumberValid = event.numberType === 1 && event.valid;
    } else if (type === 'PRIVATE') {
      this.memberForm.patchValue({
        PrivateTeleNo: event.value,
      });
      this.memberForm.patchValue({
        PrivateTeleNoPrefix: event.extension
      });
      this.isPrivateNumberValid = event.value === '' || event.valid;
    } else if (type === 'WORK') {
      this.memberForm.patchValue({
        WorkTeleNo: event.value,
      });
      this.memberForm.patchValue({
        WorkTeleNoPrefix: event.extension
      });
      this.isWorkNumberValid = event.value === '' || event.valid;
    }
  }
  mobileBlur(val: any, type: string) {
    let msg: string;
    this.translateService.get('MEMBERSHIP.NumberTaken').pipe(
      takeUntil(this.destroy$)
    ).subscribe(translatedValue => {
      msg = translatedValue
    }, null, null);
    switch (type) {
      case 'MOBILE':
        this.isMobileBlur = true;
        if (this.memberForm.controls['Mobile'].errors == null || (!this.memberForm.controls['Mobile'].errors.required &&
          !this.memberForm.controls['Mobile'].errors.phoneNumber)) {
          this.memberService.validateMobile(this.memberForm.value.Mobile, this.memberForm.value.MobilePrefix).pipe(
            takeUntil(this.destroy$)
          ).subscribe(
            result => {
              const mobileTakenInfo = result.Data;
              if (mobileTakenInfo != null && mobileTakenInfo.Item1 !== -1 /* MemberId is set to -1 if no duplicate is found */) {
                const outputMemberId = mobileTakenInfo.Item1;
                const outputName = mobileTakenInfo.Item2;
                const errorMsg = outputMemberId + ' - ' + outputName + ' ' + msg;
                this.mobileExitMsg = errorMsg;
                this.memberForm.controls['Mobile'].setErrors({
                  'mobileTaken': true
                });
                this.formErrors.Mobile = errorMsg;
              }
            }, null, null)
        }
        break;
      case 'PRIVATE':
        this.isPrivateBlur = true;
        break;
      case 'WORK':
        this.isWorkBlur = true;
        break;
      default:
        console.error('Unrecognizable case')
    }

  }
/*   mobileBlur(val: any, type: string) {
    switch (type) {
      case 'MOBILE':
        this.isMobileBlur = true;
        if (this.memberForm.controls['Mobile'].errors == null || (!this.memberForm.controls['Mobile'].errors.required &&
          !this.memberForm.controls['Mobile'].errors.phoneNumber)) { // email is exits check after valid email
          this.memberService.validateMobile(this.memberForm.value.Mobile, this.memberForm.value.MobilePrefix).subscribe(
            result => {
              if (result) {
                const outPutval = result.Data.split(':')[0].trim()
                if (Number(outPutval) === 0) {
                  const memberName = result.Data.split(':')[1].trim()
                  const errorMsg = memberName + ' ' + 'has same mobile number'
                  this.mobileExitMsg = errorMsg;
                  this.memberForm.controls['Mobile'].setErrors({
                    'mobileTaken': true
                  });
                  this.formErrors.Mobile = errorMsg;
                } else {
                }
              }
            }
          )
        }
        break;
      case 'PRIVATE':
        this.isPrivateBlur = true;
        break;
      case 'WORK':
        this.isWorkBlur = true;
        break;
    }

  } */

  mobileFocus(type: string) {
    switch (type) {
      case 'MOBILE':
        this.isMobileBlur = false
        break;
      case 'PRIVATE':
        this.isPrivateBlur = false
        break;
      case 'WORK':
        this.isWorkBlur = false
        break;
    }
  }

  saveMember() {
    this.myFormSubmited = true;
    if (this.memberForm.valid) {
      let memberId;
      const member = Object.assign({}, this.memberForm.value); // this.memberForm.value.clone();
      member.Gender = this.registerMember.Gender;
      member.Role = this.roleType;
      member.MemberStatuse = 2;
      member.CountryId = this.registerMember.CountryId;
      member.BirthDate = this.registerMember.BirthDate;
      member.MemberCategory = this.registerMember.MemberCategory[0];
      if (this.memberForm.value.FromDate !== null) {
        const fromDate = this.memberForm.value.FromDate.date;
        member.FromDate = fromDate.year + '-' + fromDate.month + '-' + fromDate.day;
      }

      if (this.memberForm.value.ToDate !== null) {
        const toDate = this.memberForm.value.ToDate.date;
        member.ToDate = toDate.year + '-' + toDate.month + '-' + toDate.day;
      }

      member.GroupId = this.registerMember.LinkMemberId;
      if (member.Role === 'MEM') {
      member.Name = member.FirstName + ' ' + member.LastName;
      } else {
        member.Name = member.CompanyName;
        member.LastName = member.CompanyName;
      }

      member.BranchId = this.loggedBranchId;
      member.InfoCategoryList = null;

      const memberModel = {
        Member: member,
        BranchId: this.loggedBranchId,
        NotificationTitle: ''
      }
      this.memberService.saveMember(memberModel).pipe(
        takeUntil(this.destroy$)
      ).subscribe(
        result => {
          if (result) {
            let sTextArray = result.Data.split(':');
            if (sTextArray.length > 0) {
              memberId = sTextArray[0];
              if (sTextArray.length > 2) {
                member.CustId = sTextArray[2];
              }
            }
            if (memberId > 0) {
              member.Id = memberId;
              this.addItemEvent.emit(member);
            } else if (memberId = -4) {
              let mobileExitMemName;
              if (sTextArray.Length > 1) {
                mobileExitMemName = sTextArray[1];
                // _addMemberView.SetMobileValidation(mobileExitMemName + " " + USGMSResources.ExitMobileNumber);
              }
            } else if (memberId == -5) {
              let emailExitMemName;
              if (sTextArray.Length > 1) {
                if (sTextArray.Length > 2) {
                  emailExitMemName = sTextArray[2];
                }
              }
              //  _addMemberView.SetEmailValidation(emailExitMemName + " " + USGMSResources.ExitEmailAddress, typeOfEmail);
            } else {
              // USMessage.Show(USGMSResources.Errorsaving);
            }
          }
        }, null, null);
    } else {
      UsErrorService.validateAllFormFields(this.memberForm, this.formErrors, this.validationMessages);
    }
  }


  getSelectedCountry(selectCountry, initialLoad: boolean) {
    if (this.countries) {
      let country: any;
      if (initialLoad === true) {
        country = this.countries.filter(x => x.Id === selectCountry);
      } else {
        country = this.countries.filter(x => x.Name === selectCountry);
      }

      if (country && country.length > 0) {
        this.selectedCountry = country[0];
        this.registerMember.CountryId = country[0].Id;
        this.registerMember.CountryName = country[0].Name;
        this.registerMember.MobilePrefix = country[0].CountryCode;
        this.registerMember.PrivateTeleNoPrefix = country[0].CountryCode;
        this.registerMember.WorkTeleNoPrefix = country[0].CountryCode;
        this.memberForm.patchValue({ MobilePrefix: this.selectedCountry.CountryCode })
        this.memberForm.patchValue({ PrivateTeleNoPrefix: this.selectedCountry.CountryCode })
        this.memberForm.patchValue({ WorkTeleNoPrefix: this.selectedCountry.CountryCode })
      }
    }
  }

  getCityforPostalCode(postalCode: any) {
    if (postalCode && postalCode.length >= 4) {
      this.memberService.getCityForPostalCode(postalCode).pipe(
        takeUntil(this.destroy$)
      ).subscribe(
        result => {
          if (result) {
            this.memberForm.patchValue({ PostCode: postalCode, PostPlace: result.Data });
          }
        }, null, null)
    } else {
      this.memberForm.patchValue({ PostPlace: '' });
    }
  }

  setGender(genderType: number) {
    this.registerMember.Gender = genderType;
  }

  openPostalCodeModal(content: any) {
    this.postalDataView = this.modalService.open(content, { width: '300' });
  }

  onbirthdayDateChange(value) {
    this.registerMember.BirthDate = value.date.year + '-' + value.date.month + '-' + value.date.day;
    const bDate = new Date(this.registerMember.BirthDate);
    this.memberBirthdate = new Date(value.date.year + '-' + value.date.month + '-' + value.date.day);
    const age = this.getMemberAge(bDate);
    this.memberForm.patchValue({ Age: age })
  }



  updateNameFormat(name: string, nameType: string) {
    let formatedName = '';
    if (typeof name !== 'undefined' && name) {
      for (let i = 0; i < name.length; i++) {
        if (i > 0) {
          formatedName = name[i - 1] === '-' || name[i - 1] === ' ' || name[i - 1] === '&' || name[i - 1] === '/' ?
            formatedName + name[i].toUpperCase() : formatedName + name[i].toLowerCase();
        } else {
          formatedName += name[i].toUpperCase();
        }

        if (nameType === 'FNAME') {
          this.memberForm.patchValue({ FirstName: formatedName });
        } else if (nameType === 'LNAME') {
          this.memberForm.patchValue({ LastName: formatedName });
        }
      }
    }
  }

  getMemberAge(birthDate: Date) {
    if (birthDate !== new Date(1900, 1, 1)) {
      const today: any = new Date();
      const ageFidMs: any = today - birthDate.getTime();
      const ageDate: Date = new Date(ageFidMs);
      const age: number = ageDate.getUTCFullYear() - 1970;
      return age;
    }
  }
  addPostalCode(postalCodeItem: any) {
    this.memberForm.patchValue({ PostCode: postalCodeItem.postalCode })
    this.memberForm.patchValue({ PostPlace: postalCodeItem.postalName })
    this.postalDataView.close();
  }

  searchDeatail(val: any) {
    const searchType = MemberSearchType[MemberSearchType.LINKCUSTOMER];
    this.memberService.getMembers(val.branchSelected.BranchId, val.searchVal, val.statusSelected.id, searchType, val.roleSelected.id, val.hit, false, false, '')
    .pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        if (result) {
          this.items = result.Data;
        } else {
        }
      }, null, null)
  }

  onPhoneNumberChange(type: string, value: any) {
    switch (type) {
      case 'MOBILE': {
        this.registerMember.Mobile = value;
        break;
      }
      case 'PRIVATE': {
        this.registerMember.PrivateTeleNo = value;
        break;
      }
      case 'WORK': {
        this.registerMember.WorkTeleNo = value;
        break;
      }
      case 'MOBILEPRE': {
        this.registerMember.MobilePrefix = value;
        break;
      }
      case 'PRIVATEPRE': {
        this.registerMember.PrivateTeleNoPrefix = value;
        break;
      }
      case 'WORKPRE': {
        this.registerMember.WorkTeleNoPrefix = value;
        break;
      }
    }
  }

  selectedRowItem(item) {
    this.registerMember.LinkMemberName = item.Name;
    this.registerMember.LinkMemberId = item.Id;
    this.memberForm.patchValue({ LinkMemberName: item.Name })
    this.newMemberRegisterView.close();
  }

  newMemberModal(content: any) {

    this.translateService.get('COMMON.MemberSelection').pipe(
      takeUntil(this.destroy$)
    ).subscribe(tranlstedValue => {
      this.entitySelectionViewTitle = tranlstedValue
    }, null, null);

    const searchType = MemberSearchType[MemberSearchType.LINKCUSTOMER];
    const branchId = -2;
    const statusId = 10;
    const searchVal = '';

    this.memberService.getBranches(-1).pipe(
      mergeMap(branches => {
        if (branches) {
          const branchList = [{ BranchId: -2, BranchName: 'ALL '}];
          branches.Data.map(item => {
            return {
              BranchId: item.BranchId,
              BranchName: item.BranchName
            }
          }).forEach(item => branchList.push(item));
          this.branches = branchList;

          return this.memberService.getMembers(branchId, searchVal, statusId, searchType, this.roleType, this.hit, false, false, '');
        }
      }),
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.items = result.Data;
        this.newMemberRegisterView = this.modalService.open(content, { width: '1000' });
      }
    }, null, null)


    // this.memberService.getBranches(-1).subscribe(branches => {
    //   if (branches) {
    //     const branchList = [{ BranchId: -2, BranchName: 'ALL' }];
    //     branches.Data.map(item => { return { BranchId: item.BranchId, BranchName: item.BranchName } })
    //       .forEach(item => branchList.push(item));
    //     this.branches = branchList;

    //     this.memberService.getMembers(branchId, searchVal, statusId, searchType, this.roleType, this.hit, false, false, '')
    //       .subscribe(
    //       result => {
    //         if (result) {
    //           this.items = result.Data;
    //           this.newMemberRegisterView = this.modalService.open(content, { width: '1000' });
    //         } else {
    //         }
    //       }
    //       )

    //   }
    // },
    //   error => {

    //   });
  }

  closeNewMemberRegisterView() {
    this.canceleAddNewMember.emit();
  }

  closePostalAreaModal() {
    this.postalDataView.close();
  }

  closeMemberSelection() {
    this.newMemberRegisterView.close();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
