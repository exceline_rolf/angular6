import { Component, OnInit, Input, Output, EventEmitter, ViewChild, OnDestroy } from '@angular/core';
import { ExceAddFollowUpService } from 'app/shared/components/exce-followup/exce-add-follow-up.service';
import { Cookie } from 'ng2-cookies/src/services';
import { ExceFollowupCommonService } from 'app/shared/components/exce-followup/exce-followup-common.service';
import { FormGroup, FormBuilder, AbstractControl, Validators, ValidationErrors, ValidatorFn, FormControl } from '@angular/forms';
import { UsErrorService } from 'app/shared/directives/us-error/us-error.service';
import { ExceMessageService } from 'app/shared/components/exce-message/exce-message.service';
import { TranslateService } from '@ngx-translate/core';
import { McBasicInfoService } from 'app/modules/membership/member-card/mc-basic-info/mc-basic-info.service';
import { ExceLoginService } from '../../../../modules/login/exce-login/exce-login.service';
import { ExceMemberService } from 'app/modules/membership/services/exce-member.service';
import { takeUntil, mergeMap } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'add-follow-up',
  templateUrl: './exce-add-follow-up.component.html',
  styleUrls: ['./exce-add-follow-up.component.scss']
})

export class ExceAddFollowUpComponent implements OnInit, OnDestroy {
  FollowUpDetailList = [];
  BranchId: any;
  MemberId: any;

  dataUpdateMember: any;
  excelineTaskCategory: any;
  followUpCategoryList: any;

  @Input() isEnabledFollowUp: boolean;
  @Input() Mobile: string;
  @Input() Email: string;
  @Input() MobilePrefix: string;
  @Input() Name: string;

  @Output() emailBlurEvent = new EventEmitter();
  @Output() mobileBlurEvent = new EventEmitter();
  @Output() closed = new EventEmitter();
  isTemplateDisable = false;
  loggedBranchId: number;
  isTemplateVisible: boolean;
  templates: any[];
  followUpDetailList: any[];
  followupTaskList: any[];
  selectedTemplate: any;
  selectedFollowUpCategory: any;
  // selectedCategoryTitle: any;
  public followUpForm: FormGroup;
  myFormSubmited = false;
  isMobileBlur = false;
  isEmailBlur = false;
  isEmail = true;
  isSms = true;
  isMobileNumberValid = true;

  private destroy$ = new Subject<void>();

/* exce-fade-in-n-out */
  aniMessage: string;
  type: string;
  isShowInfo: boolean;
/*  */
  @ViewChild('followUpTemplate') public followUpTemplate;

  formErrors = {
    'Mobile': '',
    'Email': ''
  };

  validationMessages = {
    'Mobile': {
      'required': 'Mobile is required.',
      'phoneNumber': 'invalid phone number',
      'mobileTaken': 'Mobile exit'
    },
    'Email': {
      'required': 'email is required.',
      'email': 'Please enter valid email address'
    }
  };
  constructor(
    private fb: FormBuilder,
    private followUpService: ExceAddFollowUpService,
    private followUpCommonService: ExceFollowupCommonService,
    private exceMessageService: ExceMessageService,
    private basicInfoService: McBasicInfoService,
    private translate: TranslateService,
    private loginservice: ExceLoginService,
    private memberService: ExceMemberService
  ) {
/*     this.followUpForm = this.fb.group({
      'Id': [null],
      'templateSelected': [{ value: this.selectedTemplate, disabled: true }],
      'followUpSelected': [{ value: this.selectedFollowUpCategory }],
      // 'title': [this.selectedCategoryTitle],
      'Mobile': [this.Mobile],
      'Email': [this.Email],
      'MobilePrefix': [this.MobilePrefix]
    });

    if (this.followUpService.IsDisableFollowupProfileList) {
      if (this.followUpTemplate !== null && this.followUpTemplate !== undefined) {
        this.followUpTemplate.setDisabledField(true);
      }
      this.followUpForm.get('templateSelected').enable();
      this.followUpForm.get('followUpSelected').enable();
    } else {
      this.followUpForm.get('templateSelected').disable();
      this.followUpForm.get('followUpSelected').disable();
    }
    this.followUpCommonService.followupDataUpdateEvent.subscribe(res => {
      this.dataUpdateMember = res;
    }); */
  }


  ngOnInit() {

    this.followUpForm = this.fb.group({
      'Id': [null],
      'templateSelected': [{ value: this.selectedTemplate, disabled: true }],
      'followUpSelected': [{ value: this.selectedFollowUpCategory }],
      // 'title': [this.selectedCategoryTitle],
      'Mobile': [this.Mobile, Validators.required],
      'Email': [this.Email, Validators.required],
      'MobilePrefix': [this.MobilePrefix, Validators.required]
    });
    if (this.followUpService.IsDisableFollowupProfileList) {
      if (this.followUpTemplate !== null && this.followUpTemplate !== undefined) {
        this.followUpTemplate.setDisabledField(true);
      }
      this.followUpForm.get('templateSelected').enable();
      this.followUpForm.get('followUpSelected').enable();
    } else {
      this.followUpForm.get('templateSelected').disable();
      this.followUpForm.get('followUpSelected').disable();
    }
    this.followUpCommonService.followupDataUpdateEvent.pipe(
      takeUntil(this.destroy$)
    ).subscribe(res => {
      this.dataUpdateMember = res;
    }, null, null);

  }

  initView() {
    this.isTemplateVisible = false;
    this.loggedBranchId = this.loginservice.SelectedBranch.BranchId;
    this.followUpService.getFollowUpTemplates(this.loggedBranchId).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        if (result) {
          this.templates = result.Data;
          this.selectedTemplate = this.templates[0];
          this.followUpCommonService.IsTemplateVisible = true;
          this.templateChangeEvent(this.selectedTemplate.Id);
        }
      }, null, null);

    this.basicInfoService.currentMember.pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      member => {
        if (member) {
          this.MemberId = member.Id;
          this.BranchId = member.BranchId;
          this.followUpForm.patchValue({
            MobilePrefix: this.MobilePrefix,
            Mobile: this.Mobile
          })
        }
      }, null, null);

  }

  setDisabledField(val: boolean) {
    if (this.followUpTemplate !== null && this.followUpTemplate !== undefined) {
      this.followUpTemplate.setDisabledField(val);
    }
    if (val) {
      this.followUpForm.get('templateSelected').enable();
      this.followUpForm.get('followUpSelected').enable();
    } else {
      this.followUpForm.get('templateSelected').disable();
      this.followUpForm.get('followUpSelected').disable();
    }
  }

  setValue(val: boolean) {
    this.isTemplateDisable = val
  }

  fullValueChange(event: any) {
    this.followUpForm.patchValue({
      Mobile: event.value,
    });
    this.followUpForm.patchValue({
      MobilePrefix: event.extension
    });
    this.isMobileNumberValid = event.numberType === 1 && event.valid;
  }

  mobileNumberValidator(control: AbstractControl, type: string) {
    return new Promise(resolve => {
      setTimeout(() => {
        if (!this.isMobileNumberValid) {
          resolve({
            'phoneNumber': true
          })
        } else {
          resolve(null);
        }
      }, 10);
    })
  }

  onTemplateChange(template) {
    if (template) {
      this.templateChangeEvent(template.Id);
      this.selectedFollowUpCategory = null;
    }
    const frmVal = this.followUpForm.getRawValue();
    this.selectedTemplate = frmVal.templateSelected;
    this.templateChangeEvent(this.selectedTemplate.Id);
    this.selectedFollowUpCategory = null;
  }

  templateChangeEvent(templateId) {
    if (templateId) {
      this.followUpService.getFollowUpTaskByTemplateIds(templateId).pipe(
        mergeMap(result => {
          if (result) {

            this.followupTaskList = result.Data;
            this.followUpDetailList = [];
            let id = 0;

            result.Data.forEach(i => {

              this.followUpDetailList.push({
                'Id': id--, 'FollowUpId': 1, 'TaskCategoryId': i.ExcelineTaskCategory.Id, 'TaskCategoryName': i.ExcelineTaskCategory.Name
                , 'NumOfDays': i.NumberOfDays, 'IsNextFollowUpValue': i.IsNextFollowUpValue, 'Name': i.Text, 'CreatedUser': '',
                'CreatedDateTime': new Date(), 'PlannedDate': {
                  date: {
                    year: new Date().getFullYear(),
                    month: new Date().getMonth() + 1,
                    day: new Date().getDate()
                  }
                }, 'CompletedDate': null, 'IsStartTime': i.ExcelineTaskCategory.IsStartTime
                , 'StartTime': i.ExcelineTaskCategory.IsStartTime ? '8:30' : null, 'IsEndTime': i.ExcelineTaskCategory.IsEndTime
                , 'EndTime': i.ExcelineTaskCategory.IsEndTime ? '10:30' : null, 'AssignedEmpName': '', 'AssignedEmpId': 0,
                'CompletedEmpName': '', 'CompletedEmpId': 0, 'RoleType': '', 'EntRoleId': 0, 'Comment': '', 'Discription': '', 'DueDate': null,
                'DueTime': null, 'FoundDate': null, 'IsPopUp': false, 'Phoneno': '', 'ReturnDate': null, 'Sms': '', 'StartDate': null,
                'EndDate': null, 'AutomatedEmail': '', 'ExtendedFieldsList': null
              });
            });

            if (this.followupTaskList.find(x => x.ExcelineTaskCategory.IsAutomatedEmail) !== null) {
              this.isEmail = true;
              this.followUpForm.get('Email').setValidators(Validators.required);
              this.followUpForm.get('Email').updateValueAndValidity();
              this.followUpForm.get('Email').setValidators(this.emailValidator.bind(this));
              this.followUpForm.get('Email').updateValueAndValidity();
            } else {
              this.isEmail = false;
              this.followUpForm.get('Email').setValidators(null);
              this.followUpForm.get('Email').updateValueAndValidity();
            }

            if (this.followupTaskList.find(x => x.ExcelineTaskCategory.IsAutomatedSMS) !== null) {
              this.isSms = true;
              this.followUpForm.get('Mobile').setValidators(Validators.required);
              this.followUpForm.get('Mobile').updateValueAndValidity();
              this.followUpForm.get('Mobile').setAsyncValidators(this.mobileNumberValidator.bind(this));
              this.followUpForm.get('Mobile').updateValueAndValidity();
            } else {
              this.followUpForm.get('Mobile').setValidators(null);
              this.followUpForm.get('Mobile').updateValueAndValidity();
              this.isSms = false;
            }

            if (!this.selectedFollowUpCategory) {
              this.selectedFollowUpCategory = this.followupTaskList[0].ExcelineTaskCategory;
              this.isTemplateVisible = true;
            }

            const templateData = this.followUpDetailList.find(x => x.TaskCategoryId === this.selectedFollowUpCategory.Id);
            this.followUpCommonService.FollowUpTemplate = {
              template: this.selectedFollowUpCategory,
              data: templateData
            };

            return this.followUpForm.statusChanges;
          }
        }),
          takeUntil(this.destroy$)
        ).subscribe(_ => {
          UsErrorService.onValueChanged(this.followUpForm, this.formErrors, this.validationMessages)
        }, null, null);
    }
  }
/*
  // when change followup template
  templateChangeEvent(templateId) {

    if (templateId) {
      this.followUpService.getFollowUpTaskByTemplateIds(templateId).subscribe(
        result => {
          if (result) {

            this.followupTaskList = result.Data;
            this.followUpDetailList = [];
            let id = 0;

            result.Data.forEach(i => {

              this.followUpDetailList.push({
                'Id': id--, 'FollowUpId': 1, 'TaskCategoryId': i.ExcelineTaskCategory.Id, 'TaskCategoryName': i.ExcelineTaskCategory.Name
                , 'NumOfDays': i.NumberOfDays, 'IsNextFollowUpValue': i.IsNextFollowUpValue, 'Name': i.Text, 'CreatedUser': '',
                'CreatedDateTime': new Date(), 'PlannedDate': {
                  date: {
                    year: new Date().getFullYear(),
                    month: new Date().getMonth() + 1,
                    day: new Date().getDate()
                  }
                }, 'CompletedDate': null, 'IsStartTime': i.ExcelineTaskCategory.IsStartTime
                , 'StartTime': i.ExcelineTaskCategory.IsStartTime ? '8:30' : null, 'IsEndTime': i.ExcelineTaskCategory.IsEndTime
                , 'EndTime': i.ExcelineTaskCategory.IsEndTime ? '10:30' : null, 'AssignedEmpName': '', 'AssignedEmpId': 0,
                'CompletedEmpName': '', 'CompletedEmpId': 0, 'RoleType': '', 'EntRoleId': 0, 'Comment': '', 'Discription': '', 'DueDate': null,
                'DueTime': null, 'FoundDate': null, 'IsPopUp': false, 'Phoneno': '', 'ReturnDate': null, 'Sms': '', 'StartDate': null,
                'EndDate': null, 'AutomatedEmail': '', 'ExtendedFieldsList': null
              });
            });

            if (this.followupTaskList.find(x => x.ExcelineTaskCategory.IsAutomatedEmail) !== null) {
              this.isEmail = true;
              this.followUpForm.get('Email').setValidators(Validators.required);
              this.followUpForm.get('Email').updateValueAndValidity();
              this.followUpForm.get('Email').setValidators(this.emailValidator.bind(this));
              this.followUpForm.get('Email').updateValueAndValidity();
            } else {
              this.isEmail = false;
              this.followUpForm.get('Email').setValidators(null);
              this.followUpForm.get('Email').updateValueAndValidity();
            }

            if (this.followupTaskList.find(x => x.ExcelineTaskCategory.IsAutomatedSMS) !== null) {
              this.isSms = true;
              this.followUpForm.get('Mobile').setValidators(Validators.required);
              this.followUpForm.get('Mobile').updateValueAndValidity();
              this.followUpForm.get('Mobile').setAsyncValidators(this.mobileNumberValidator.bind(this));
              this.followUpForm.get('Mobile').updateValueAndValidity();
            } else {
              this.followUpForm.get('Mobile').setValidators(null);
              this.followUpForm.get('Mobile').updateValueAndValidity();
              this.isSms = false;
            }

            if (!this.selectedFollowUpCategory) {
              this.selectedFollowUpCategory = this.followupTaskList[0].ExcelineTaskCategory;
              this.isTemplateVisible = true;
            }

            const templateData = this.followUpDetailList.find(x => x.TaskCategoryId === this.selectedFollowUpCategory.Id);
            this.followUpCommonService.FollowUpTemplate = {
              template: this.selectedFollowUpCategory,
              data: templateData
            };

            this.followUpForm.statusChanges.subscribe(_ => UsErrorService.onValueChanged(this.followUpForm, this.formErrors, this.validationMessages));
          }
        });
    }
  }
 */
  private emailValidator(control: AbstractControl): ValidationErrors {
    if (!control.value) {
      return null;
    }
    return Validators.email(control);
  }


  // when change followup category
  followUpChangeEvent(selectedFollowUpCategory: any, type: number) {
    this.selectedFollowUpCategory = selectedFollowUpCategory.ExcelineTaskCategory;
    this.templateChangeEvent(selectedFollowUpCategory.FollowUpTemplateId);
    this.isTemplateVisible = true;
  }

  // save follow up when member register
  saveFollowUpFromMember(value) {
    // debugger

    this.myFormSubmited = true;

    const followUpdata = this.followUpTemplate.getSaveData();
    this.setFollowUpDetail(followUpdata);

    this.followUpDetailList.forEach(element => {
      if (element.IsStartTime) {
        element.StartTime = '2017-01-01T' + element.StartTime + ':00'
      }
      if (element.IsEndTime) {
        element.EndTime = '2017-01-01T' + element.EndTime + ':00'
      }
      if (element.DueTime !== null && element.DueTime !== undefined) {
        element.DueTime = '2017-01-01T' + element.DueTime + ':00'
      }
    });

    followUpdata.ActiveStatus = true;
    followUpdata.PlannedDate = followUpdata.PlannedDate.date.year + '-' + followUpdata.PlannedDate.date.month + '-' + followUpdata.PlannedDate.date.day + 'T00:00:00';

    this.FollowUpDetailList.push(followUpdata);

    const saveFollowUpList = [{
      MemberId: value.MemberId,
      FollowUpTemplateId: this.selectedTemplate.Id,
      FollowUpDetailList: this.FollowUpDetailList,
      SmsPhoneNo: value.Mobile,
      SmsPrefix: value.MobilePrefix,
      EmailAddress: value.Email,
      BranchId: this.BranchId,
      IsInsert: true
    }];

   /*  this.followUpService.saveFollowUp(saveFollowUpList).pipe(
      mergeMap(result => {
        this.type = 'SUCCESS';
        this.isShowInfo = true;
        setTimeout(() => {
          this.isShowInfo = false;
        }, 4000);
        return this.translate.get('MEMBERSHIP.SaveSuccessful');
      }, error => {
        this.type = 'DANGER';
        this.isShowInfo = true;
        setTimeout(() => {
          this.isShowInfo = false;
        }, 4000);
        return this.translate.get('MEMBERHSIP.SaveNotSuccessful');
      }),
      takeUntil(this.destroy$)
    ).subscribe(trans => {
      this.aniMessage = trans;
    }, null, null) */

    this.followUpService.saveFollowUp(saveFollowUpList).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        this.translate.get('MEMBERSHIP.SaveSuccessful').pipe(
          takeUntil(this.destroy$)
        ).subscribe(trans => this.aniMessage = trans);
        this.type = 'SUCCESS';
        this.isShowInfo = true;
        setTimeout(() => {
          this.isShowInfo = false;
        }, 4000);
      }, error => {
        this.translate.get('MEMBERSHIP.SaveNotSuccessful').pipe(
          takeUntil(this.destroy$)
        ).subscribe(trans => this.aniMessage = trans);
        this.type = 'DANGER';
        this.isShowInfo = true;
        setTimeout(() => {
          this.isShowInfo = false;
        }, 4000);
      });

    // close the popup aftersave the data
    this.closeView();
  }


  // save followup profile
  saveFollowUp(value) {
    // debugger

    this.myFormSubmited = true;

    const followUpdata = this.followUpTemplate.getSaveData();
    this.setFollowUpDetail(followUpdata);

    this.followUpDetailList.forEach(element => {
      if (element.IsStartTime) {
        element.StartTime = '2017-01-01T' + element.StartTime + ':00'
      }
      if (element.IsEndTime) {
        element.EndTime = '2017-01-01T' + element.EndTime + ':00'
      }
      if (element.DueTime !== null && element.DueTime !== undefined) {
        element.DueTime = '2017-01-01T' + element.DueTime + ':00'
      }
    });

    followUpdata.ActiveStatus = true;
    followUpdata.PlannedDate = followUpdata.PlannedDate.date.year + '-' + followUpdata.PlannedDate.date.month + '-' + followUpdata.PlannedDate.date.day + 'T00:00:00';

    this.FollowUpDetailList.push(followUpdata);

    const saveFollowUpList = [{
      MemberId: this.MemberId,
      FollowUpTemplateId: this.selectedTemplate.Id,
      FollowUpDetailList: this.FollowUpDetailList,
      SmsPhoneNo: value.Mobile,
      SmsPrefix: value.MobilePrefix,
      EmailAddress: value.Email,
      BranchId: this.BranchId,
      IsInsert: true
    }];

    this.followUpService.saveFollowUp(saveFollowUpList).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      null, null, null);

    // close the popup aftersave the data
    this.closeView();
  }

  setFollowUpDetail(item: any) {
    const followUpList = [];
    this.followUpDetailList.forEach(x => { followUpList.push(x) });
    for (let i = 0; i < this.followUpDetailList.length; i++) {
      if ((item.Id <= 0 && this.followUpDetailList[i].Id === item.Id) ||
        (item.Id > 0 && item.Id === this.followUpDetailList[i].Id)) {
        followUpList[i] = item;
      }

      if (this.followUpDetailList[i].Id <= 0 && this.followUpDetailList[i].IsStartTime && this.followUpDetailList[i].IsEndTime
        && this.followUpDetailList[i].StartTime != null && this.followUpDetailList[i].EndTime != null) {
        followUpList[i].IsShowInCalandar = true;
      }

      if (this.followUpDetailList[i].AssignedEmpId === 0 && this.followUpDetailList[i].EntRoleId === 0) {
        followUpList[i].AssignedEmpId = item.AssignedEmpId;
        followUpList[i].AssignedEmpName = item.AssignedEmpName;

        followUpList[i].EntRoleId = item.EntRoleId;
        followUpList[i].RoleType = item.RoleType;
      }
    }
    this.followUpDetailList = followUpList;

  }

  validateFollowUpProfileForMember() {
    this.myFormSubmited = true;

    const isTitleValid = this.toCheckAllCategoryTitleAssigned();
    if (this.followUpForm.valid && isTitleValid) { // && isValidFollowUp
      return true;
    } else {
      UsErrorService.validateAllFormFields(this.followUpForm, this.formErrors, this.validationMessages);
      return false;
    }
  }

  setMemberValue(member: any) {
    this.followUpForm.patchValue({
      Mobile: member.Mobile,
      Email: member.Email
    })
  }

  mobileBlur(val: any) {
/*     if (this.followUpForm.controls['Mobile'].errors == null || (!this.followUpForm.controls['Mobile'].errors.required &&
      !this.followUpForm.controls['Mobile'].errors.phoneNumber)) { // email is exits check after valid email
      this.followUpService.validateMobile(this.followUpForm.value.Mobile, this.followUpForm.value.MobilePrefix).subscribe(
        result => {
          if (result) {
            const outPutval = result.Data.split(':')[0].trim()
            if (Number(outPutval) === 0) {
              const memberName = result.Data.split(':')[1].trim()
              const errorMsg = memberName + ' ' + 'has same mobile number'
              this.followUpForm.controls['Mobile'].setErrors({
                'mobileTaken': true
              });
              this.formErrors.Mobile = errorMsg;
            } else {

              this.mobileBlurEvent.emit({ Mobile: this.followUpForm.value.Mobile, MobilePrefix: this.followUpForm.value.MobilePrefix });
            }
          }
        }
      )
    } */
    this.isMobileBlur = true;
    if (this.followUpForm.controls['Mobile'].errors == null ||
    (!this.followUpForm.controls['Mobile'].errors.required && !this.followUpForm.controls['Mobile'].errors.phoneNumber)) {
      // må levere inn prefix uten +
      let index = this.followUpForm.value.MobilePrefix.indexOf('+')
      let valu = this.followUpForm.value.MobilePrefix;
      if (index === 0) {
        index = this.followUpForm.value.MobilePrefix.split('+')[1];
        valu = index;
      }
      this.memberService.validateMobile(this.followUpForm.value.Mobile, valu).pipe(
        takeUntil(this.destroy$)
      ).subscribe(
        result => {
          let msg: string;
          this.translate.get('MEMBERSHIP.NumberTaken').pipe(
            takeUntil(this.destroy$)
          ).subscribe(tvalue => {
            msg = tvalue
          }, null, null);
          const mobileTakenInfo = result.Data;
          if (mobileTakenInfo != null &&  mobileTakenInfo.Item1 !== -1 && mobileTakenInfo.Item2 !== this.Name) {
            const outputMemberId = mobileTakenInfo.Item1;
            const outputMemberName = mobileTakenInfo.Item2;
            const errorMsg = outputMemberId + '-' + outputMemberName + ' ' + msg;
            this.followUpForm.controls['Mobile'].setErrors({
              'mobileTaken': true
            });
            this.formErrors.Mobile = errorMsg;
          }
        }, null, null
      )
    }
  }

  prefixChange(val) {
    if (val.length > 0) {
      const index = val.indexOf('+')
      let valu = val;
      let length = 0;
      if (index === 0) {
        if (val.split('+').length === 2) {
          // nothin to see here, move along
        } else {
          this.followUpForm.patchValue({ MobilePrefix: '+47'})
          this.translate.get('MEMBERSHIP.PreFixError').pipe(
            takeUntil(this.destroy$)
          ).subscribe(trans => {
            this.aniMessage = trans
          }, null, null);
          this.type = 'DANGER';
          this.isShowInfo = true;
          setTimeout(() => {
            this.isShowInfo = false;
          }, 4000);
        }
        length = val.split('+')[1];
        valu = index;
      } else if (index > 0) {
        this.followUpForm.patchValue({ MobilePrefix: '+47'})
        this.translate.get('MEMBERSHIP.PreFixError').pipe(
          takeUntil(this.destroy$)
        ).subscribe(trans => {
          this.aniMessage = trans
        }, null, null);
        this.type = 'DANGER';
        this.isShowInfo = true;
        setTimeout(() => {
          this.isShowInfo = false;
        }, 4000);
      } else {
        // missing +
        this.followUpForm.patchValue({ MobilePrefix: '+' + val})
        this.translate.get('MEMBERSHIP.PreFixError').pipe(
          takeUntil(this.destroy$)
        ).subscribe(trans => {
          this.aniMessage = trans
        }, null);
        this.type = 'WARNING';
        this.isShowInfo = true;
        setTimeout(() => {
          this.isShowInfo = false;
        }, 4000);

      }
      if (length > 0) {

      }
    } else {
      this.followUpForm.patchValue({ MobilePrefix: '+47'})
    }
  }

  mobileFocus() {
    this.isMobileBlur = false
  }

  emailBlur() {
    this.isEmailBlur = true;
    if (this.followUpForm.controls['Email'].errors == null) {
      this.emailBlurEvent.emit(this.followUpForm.value.Email)
    }
  }

  emailFocus() {
    this.isEmailBlur = false;
  }

  toCheckAllCategoryTitleAssigned() {
    if (this.followUpDetailList) {
      this.followUpDetailList.forEach(item => {
        if (!item.Name) {
          let msg = '';
          let msgType = '';
          this.translate.get('MEMBERSHIP.Warning').pipe(
            takeUntil(this.destroy$)
          ).subscribe(translatedValue => {
            msgType = translatedValue
          }, null, null);
          this.translate.get('MEMBERSHIP.MsgShouldSetAllTitleToCatagory').pipe(
            takeUntil(this.destroy$)
          ).subscribe(translatedValue => {
            msg = translatedValue
          }, null, null);
          this.exceMessageService.openMessageBox('WARNING', {
            messageTitle: msgType,
            messageBody: msg
          });
          return false;
        }
      })
      return true;
    }
    return false;
  }


  closeView() {
    this.closed.emit();
  }

  updateFollowUpMember(selectedMember: any) {

  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
