import { TestBed, inject } from '@angular/core/testing';

import { ExceAddFollowUpService } from './exce-add-follow-up.service';

describe('ExceAddFollowUpService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExceAddFollowUpService]
    });
  });

  it('should be created', inject([ExceAddFollowUpService], (service: ExceAddFollowUpService) => {
    expect(service).toBeTruthy();
  }));
});
