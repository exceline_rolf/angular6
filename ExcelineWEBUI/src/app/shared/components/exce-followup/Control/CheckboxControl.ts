import { ControlBase } from 'app/shared/components/exce-followup/Control/ControlBase';
export class CheckboxControl extends ControlBase<string> {
  controlType = 'checkbox';
  type: string;

  constructor(options: {} = {}) {
    super(options);
    this.type = options['type'] || '';
  }
}
