import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExceFixedFollowUpComponent } from './exce-fixed-follow-up.component';

describe('ExceFixedFollowUpComponent', () => {
  let component: ExceFixedFollowUpComponent;
  let fixture: ComponentFixture<ExceFixedFollowUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExceFixedFollowUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExceFixedFollowUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
