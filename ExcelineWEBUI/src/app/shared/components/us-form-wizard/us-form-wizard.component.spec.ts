import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsFormWizardComponent } from './us-form-wizard.component';

describe('UsFormWizardComponent', () => {
  let component: UsFormWizardComponent;
  let fixture: ComponentFixture<UsFormWizardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsFormWizardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsFormWizardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
