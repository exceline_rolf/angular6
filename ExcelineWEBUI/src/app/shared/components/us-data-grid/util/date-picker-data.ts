import { Injectable } from '@angular/core';
import {NgbDatepickerI18n} from '@ng-bootstrap/ng-bootstrap';

const I18N_VALUES = {
  en: {
    weekdays: ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'],
    months: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    monthsfull: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
  },
  es: {
    weekdays: ['dom', 'lun', 'mar', 'mié', 'jue', 'vie', 'sáb'],
    months: ['ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dic'],
    monthsfull: ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre']
  },
  nb: {
    weekdays: [ 'søn', 'man', 'tir', 'ons', 'tor', 'fre', 'lør'],
    months: ['jan', 'feb', 'mar', 'apr', 'mai', 'jun', 'jul', 'aug', 'sep', 'okt', 'nov', 'des'],
    monthsfull: [ 'januar', 'februar', 'mars', 'april', 'mai', 'juni', 'juli', 'august', 'september', 'oktober', 'november', 'desember']
  }
};


export class DatePickerObject {
  public readonly year: number;
  public readonly month: number;
  public readonly day: number;
  constructor(yearPara,monthPara,dayPara){
    this.year = yearPara;
    this.month = monthPara;
    this.day = dayPara;
  }
}

@Injectable()
export class I18n {
  language = 'en';
}

@Injectable()
export class DatePickerData extends NgbDatepickerI18n {
  getDayAriaLabel(date: import("@ng-bootstrap/ng-bootstrap").NgbDateStruct): string {
    throw new Error("Method not implemented.");
  }
    constructor(private _i18n: I18n) {
        super();
    }

    getWeekdayShortName(weekday: number): string {
        try {
            return I18N_VALUES[this._i18n.language].weekdays[weekday - 1];
        } catch (err) {
            return I18N_VALUES['en'].weekdays[weekday - 1];
        }
    }
    getMonthShortName(month: number): string {
        try {
            return I18N_VALUES[this._i18n.language].months[month - 1];
        } catch (err) {
            return I18N_VALUES['en'].months[month - 1];
        }
    }
    getMonthFullName(monthfull: number): string {
        try {
            return I18N_VALUES[this._i18n.language].monthsfull[monthfull - 1];
        } catch (err) {
            return I18N_VALUES['en'].monthsfull[monthfull - 1];
        }
    }

}

export interface Headers {
  HeaderName: string;
  showInFilter: true;
  dataType: string;
  isEditable: boolean;
  canSort: boolean;
  orderNo: number;
  inputType: string;
}
