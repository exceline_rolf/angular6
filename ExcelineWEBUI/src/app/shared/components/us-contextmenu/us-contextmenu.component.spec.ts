import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsContextmenuComponent } from './us-contextmenu.component';

describe('UsContextmenuComponent', () => {
  let component: UsContextmenuComponent;
  let fixture: ComponentFixture<UsContextmenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsContextmenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsContextmenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
