import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'us-contextmenu',
  templateUrl: './us-contextmenu.component.html',
  styleUrls: ['./us-contextmenu.component.scss']
})
export class UsContextmenuComponent implements OnInit {
  @Input() x = 0;
  @Input() y = 0;

  constructor() { }

  ngOnInit() {
  }

}
