import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'p-footer',
  template: '<ng-content></ng-content>'
})
export class FooterComponent {}
