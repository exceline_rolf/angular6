import {
  NgModule,
  Component,
  Input,
  Output,
  EventEmitter,
  AfterContentInit,
  ElementRef,
  ContentChild,
  IterableDiffers,
  ChangeDetectorRef,
  ContentChildren,
  QueryList,
  Inject,
  forwardRef,
  OnInit,
  Renderer2,
  ViewChild
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Subscription } from 'rxjs';
import { DomHandler } from './dom-handler';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ColumnComponent } from './column/column.component';
import { TreeNode } from './tree-node';


@Component({
  selector: 'us-tree-table',
  templateUrl: './us-tree-table.component.html',
  styleUrls: ['./us-tree-table.component.scss'],
  providers: [DomHandler]
})
export class UsTreeTableComponent implements AfterContentInit {

  @Input() value: TreeNode[];

  @Input() selectionMode: string;

  @Input() selection: any;

  @Input() style: any;

  @Input() styleClass: string;

  @Input() labelExpand = 'Expand';

  @Input() labelCollapse = 'Collapse';

  @Input() metaKeySelection = true;

  @Input() contextMenu: any;

  @Input() toggleColumnIndex = 0;

  @Input() tableStyle: any;

  @Input() tableStyleClass: string;

  @Input() collapsedIcon = 'icon-caret-right';

  @Input() expandedIcon = 'icon-caret-down';

  @Output() onRowDblclick: EventEmitter<any> = new EventEmitter();

  @Output() selectionChange: EventEmitter<any> = new EventEmitter();

  @Output() onNodeSelect: EventEmitter<any> = new EventEmitter();

  @Output() onNodeUnselect: EventEmitter<any> = new EventEmitter();

  @Output() onNodeExpand: EventEmitter<any> = new EventEmitter();

  @Output() onNodeCollapse: EventEmitter<any> = new EventEmitter();

  @Output() onContextMenuSelect: EventEmitter<any> = new EventEmitter();

  @ContentChild(HeaderComponent) header: HeaderComponent;

  @ContentChild(FooterComponent) footer: FooterComponent;

  @ContentChildren(ColumnComponent) cols: QueryList<ColumnComponent>;

  @ViewChild('tbl') tableViewChild: ElementRef;


  public rowTouched: boolean;

  public columns: ColumnComponent[];

  private treeTableVal:any

  columnsSubscription: Subscription;

  constructor(public el: ElementRef, public domHandler: DomHandler, public changeDetector: ChangeDetectorRef, public renderer: Renderer2) {




  }

  ngAfterContentInit() {
    this.initColumns();

    this.columnsSubscription = this.cols.changes.subscribe(_ => {
      this.initColumns();
      this.changeDetector.markForCheck();
    });







  }

  initColumns(): void {
    this.columns = this.cols.toArray();
  }

  onRowClick(event: MouseEvent, node: TreeNode) {
    const eventTarget = (<Element>event.target);
    if (eventTarget.className && eventTarget.className.indexOf('ui-treetable-toggler') === 0) {
      return;
    } else if (this.selectionMode) {
      if (node.selectable === false) {
        return;
      }

      const metaSelection = this.rowTouched ? false : this.metaKeySelection;
      const index = this.findIndexInSelection(node);
      const selected = (index >= 0);

      if (this.isCheckboxSelectionMode()) {
        if (selected) {
          this.propagateSelectionDown(node, false);
          if (node.parent) {
            this.propagateSelectionUp(node.parent, false);
          }
          this.selectionChange.emit(this.selection);
          this.onNodeUnselect.emit({ originalEvent: event, node: node });
        } else {
          this.propagateSelectionDown(node, true);
          if (node.parent) {
            this.propagateSelectionUp(node.parent, true);
          }
          this.selectionChange.emit(this.selection);
          this.onNodeSelect.emit({ originalEvent: event, node: node });
        }
      } else {
        if (metaSelection) {
          const metaKey = (event.metaKey || event.ctrlKey);

          if (selected && metaKey) {
            if (this.isSingleSelectionMode()) {
              this.selectionChange.emit(null);
            } else {
              this.selection = this.selection.filter((val, i) => i !== index);
              this.selectionChange.emit(this.selection);
            }

            this.onNodeUnselect.emit({ originalEvent: event, node: node });
          } else {
            if (this.isSingleSelectionMode()) {
              this.selectionChange.emit(node);
            } else if (this.isMultipleSelectionMode()) {
              this.selection = (!metaKey) ? [] : this.selection || [];
              this.selection = [...this.selection, node];
              this.selectionChange.emit(this.selection);
            }

            this.onNodeSelect.emit({ originalEvent: event, node: node });
          }
        } else {
          if (this.isSingleSelectionMode()) {
            if (selected) {
              this.selection = null;
              this.onNodeUnselect.emit({ originalEvent: event, node: node });
            } else {
              this.selection = node;
              this.onNodeSelect.emit({ originalEvent: event, node: node });
            }
          } else {
            if (selected) {
              this.selection = this.selection.filter((val, i) => i !== index);
              this.onNodeUnselect.emit({ originalEvent: event, node: node });
            } else {
              this.selection = [...this.selection || [], node];
              this.onNodeSelect.emit({ originalEvent: event, node: node });
            }
          }

          this.selectionChange.emit(this.selection);
        }
      }
    }

    this.rowTouched = false;
  }

  onRowTouchEnd() {
    this.rowTouched = true;
  }

  onRowRightClick(event: MouseEvent, node: TreeNode) {
    if (this.contextMenu) {
      const index = this.findIndexInSelection(node);
      const selected = (index >= 0);

      if (!selected) {
        if (this.isSingleSelectionMode()) {
          this.selection = node;
        } else if (this.isMultipleSelectionMode()) {
          this.selection = [node];
          this.selectionChange.emit(this.selection);
        }

        this.selectionChange.emit(this.selection);
      }

      this.contextMenu.show(event);
      this.onContextMenuSelect.emit({ originalEvent: event, node: node });
    }
  }

  findIndexInSelection(node: TreeNode) {
    let index = -1;

    if (this.selectionMode && this.selection) {
      if (this.isSingleSelectionMode()) {
        index = (this.selection === node) ? 0 : - 1;
      } else {
        for (let i = 0; i < this.selection.length; i++) {
          if (this.selection[i] === node) {
            index = i;
            break;
          }
        }
      }
    }

    return index;
  }

  propagateSelectionUp(node: TreeNode, select: boolean) {
    if (node.children && node.children.length) {
      let selectedCount = 0;
      let childPartialSelected = false;
      for (const child of node.children) {
        if (this.isSelected(child)) {
          selectedCount++;
        } else if (child.partialSelected) {
          childPartialSelected = true;
        }
      }

      if (select && selectedCount === node.children.length) {
        this.selection = [...this.selection || [], node];
        node.partialSelected = false;
      } else {
        if (!select) {
          const index = this.findIndexInSelection(node);
          if (index >= 0) {
            this.selection = this.selection.filter((val, i) => i !== index);
          }
        }

        if (childPartialSelected || selectedCount > 0 && selectedCount !== node.children.length) {
          node.partialSelected = true;
        } else {
          node.partialSelected = false;
        }
      }
    }

    const parent = node.parent;
    if (parent) {
      this.propagateSelectionUp(parent, select);
    }
  }

  propagateSelectionDown(node: TreeNode, select: boolean) {
    const index = this.findIndexInSelection(node);

    if (select && index === -1) {
      this.selection = [...this.selection || [], node];
    } else if (!select && index > -1) {
      this.selection = this.selection.filter((val, i) => i !== index);
    }

    node.partialSelected = false;

    if (node.children && node.children.length) {
      for (const child of node.children) {
        this.propagateSelectionDown(child, select);
      }
    }
  }

  isSelected(node: TreeNode) {
    return this.findIndexInSelection(node) !== -1;
  }

  isSingleSelectionMode() {
    return this.selectionMode && this.selectionMode === 'single';
  }

  isMultipleSelectionMode() {
    return this.selectionMode && this.selectionMode === 'multiple';
  }

  isCheckboxSelectionMode() {
    return this.selectionMode && this.selectionMode === 'checkbox';
  }

  hasFooter() {
    if (this.columns) {
      const columnsArr = this.cols.toArray();
      for (let i = 0; i < columnsArr.length; i++) {
        if (columnsArr[i].footer) {
          return true;
        }
      }
    }
    return false;
  }
}
