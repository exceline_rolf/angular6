import { error } from 'util';
import { Component, OnInit, Output, EventEmitter, Input, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ExceMemberService } from 'app/modules/membership/services/exce-member.service';
import { NotificationMethodType, TextTemplateEnum, PaymentTypes } from 'app/shared/enums/us-enum';
import { ExceLoginService } from 'app/modules/login/exce-login/exce-login.service';
import { McBasicInfoService } from 'app/modules/membership/member-card/mc-basic-info/mc-basic-info.service';
import { UsErrorService } from 'app/shared/directives/us-error/us-error.service';
import { ExcelineMember } from 'app/modules/membership/models/ExcelineMember';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-exce-notify-invoice',
  templateUrl: './exce-notify-invoice.component.html',
  styleUrls: ['./exce-notify-invoice.component.scss']
})
export class ExceNotifyInvoiceComponent implements OnInit, OnDestroy {

  @Input() selectedOrder: any;
  @Output() closeEvent: EventEmitter<any> = new EventEmitter();
  @Output() smsEmailSuccessEvent: EventEmitter<any> = new EventEmitter();
  private destroy$ = new Subject<void>();
  private _selectedMember: ExcelineMember;
  private isSelected: any;
  private text: any;
  private senderDetail: any;


  _user: any;
  _branchId: any;
  SelectedEmailTemplate: any;
  EmailContent: any;
  IsEmailAddress: boolean;
  SelectedSMSTemplate: any[] = [];
  SMSContent: any;
  IsSMSNo: boolean;
  EmailTemplate: string;
  EmailAddress: string;
  SMSTemplate: string;
  SmsNo: string;
  EmailChecked: boolean = true;
  SMSChecked: boolean = true;
  editTemplateForm: FormGroup;

  formErrors = {
    'EmailAddress': '',
    'SmsNo': ''

  }

  validationMessages = {
    'EmailAddress': {
      'email': 'MEMBERSHIP.MsgInvalidEmail'
    },
    'SmsNo': {
      'pattern': 'MEMBERSHIP.InvalidSMSNo'
    }
  }
  constructor(
    private formBuilder: FormBuilder,
    private memberService: ExceMemberService,
    private loginservice: ExceLoginService,
    private basicInfoService: McBasicInfoService
  ) { }

  ngOnInit() {
    this._branchId = this.loginservice.SelectedBranch.BranchId;
    this.basicInfoService.currentMember.pipe(
      takeUntil(this.destroy$)
    ).subscribe((currentMember: ExcelineMember) => this._selectedMember = currentMember);
    this._user = this.loginservice.CurrentUser.username;
    this.editTemplateForm = this.formBuilder.group({
      'IsSMSChecked': [null],
      'IsEmailChecked': [null],
      'SmsNo': [null, Validators.pattern('^[0-9 ]*$')], // { value: null, disabled: true }
      'SelectedSMSTemplate': [null],
      'SMSContent': [null],
      'EmailAddress': [null, Validators.email],
      'SelectedEmailTemplate': [null],
      'EmailContent': [null],
    });

    this.editTemplateForm.statusChanges.pipe(
      takeUntil(this.destroy$)
    ).subscribe(_ => {
      UsErrorService.onValueChanged(this.editTemplateForm, this.formErrors, this.validationMessages);
    });

    //update the form
    this.editTemplateForm.patchValue({
      IsSMSChecked: true,
      IsEmailChecked: true,
      SmsNo: this._selectedMember.GuardianId > 0 ? this._selectedMember.GuardianMobile : this._selectedMember.Mobile,
      EmailAddress: this._selectedMember.GuardianId > 0 ? this._selectedMember.GuardianEmail : this._selectedMember.Email,
    });

    // Get SMS Template List
    this.memberService.getNotificationTemplateListByMethod(NotificationMethodType[NotificationMethodType.SMS],
      TextTemplateEnum[TextTemplateEnum.SMSINVOICE], this._selectedMember.BranchId).pipe(
        takeUntil(this.destroy$)
      ).subscribe(
      result => {
        if (result) {
          this.SelectedSMSTemplate = result.Data;
          this.SMSContent = this.SelectedSMSTemplate[0].Text;
          this.editTemplateForm.patchValue({
            SMSContent: this.SelectedSMSTemplate[0].Text
          });
        }
      }
      );

    // Get Email Template List
    this.memberService.getNotificationTemplateListByMethod(NotificationMethodType[NotificationMethodType.EMAIL],
      TextTemplateEnum[TextTemplateEnum.EMAILINVOICE], this._selectedMember.BranchId).pipe(
        takeUntil(this.destroy$)
      ).subscribe(
      result => {
        if (result) {
          this.SelectedEmailTemplate = result.Data;
          this.EmailContent = this.SelectedEmailTemplate[0].Text;
          this.editTemplateForm.patchValue({
            EmailContent: this.SelectedEmailTemplate[0].Text
          });
        }
      });
  }

  changeSmsEvent(event) {
    if (event.target.checked) {
      this.editTemplateForm.patchValue({
        SMSContent: this.SMSContent
      });
      this.editTemplateForm.get('SmsNo').enable();
    } else {
      this.editTemplateForm.patchValue({
        SMSContent: ''
      });
      this.editTemplateForm.get('SmsNo').disable();
    }
  }

  changeEmailEvent(event) {
    if (event.target.checked) {
      this.editTemplateForm.patchValue({
        EmailContent: this.EmailContent
      });
      this.editTemplateForm.get('EmailAddress').enable();
    } else {
      this.editTemplateForm.patchValue({
        EmailContent: ''
      });
      this.editTemplateForm.get('EmailAddress').disable();
    }
  }

  closeView() {
    this.closeEvent.emit();
  }

  onTemplateChange(type: string, selectedItem: any) {
    if (type === 'EMAIL') {
      this.EmailContent = selectedItem.Text
      this.editTemplateForm.patchValue({
        EmailContent: this.EmailContent
      });
    } else if (type === 'SMS') {
      this.SMSContent = selectedItem.Text
      this.editTemplateForm.patchValue({
        SMSContent: this.SMSContent
      });
    }
  }

  sendSMSOrEmail() {
    if (this.editTemplateForm.valid) {

      const value = this.editTemplateForm.value;

      if (value.IsSMSChecked === null) {
        value.IsSMSChecked = false;
      }
      if (value.IsEmailChecked === null) {
        value.IsEmailChecked = false;
      }

      if (value.IsSMSChecked && value.SMSContent != null && value.SmsNo != null) {
        value.IsSMSChecked = true;
      } else {
        value.IsSMSChecked = false;
      }

      if (value.IsEmailChecked && value.EmailContent != null && value.EmailAddress != null) {
        value.IsEmailChecked = true;
      } else {
        value.IsEmailChecked = false;
      }

      const generateAndSendInvoiceSMSANDEmail = {
        invoiceBranchId: this._selectedMember.BranchId,
        selectedOrder: this.selectedOrder,
        paymentDetails: { PaymentSource: PaymentTypes[PaymentTypes.INSTALLMENTS] },
        isSMSselected: value.IsSMSChecked,
        isEmailSelected: value.IsEmailChecked,
        SMStext: value.SMSContent,
        EmailText: value.EmailContent,
        EmailAddress: value.EmailAddress,
        SMSNo: value.SmsNo,
        memberBranchID: this._selectedMember.BranchId

      }

      this.memberService.generateOrderAndSMSANDEmail(generateAndSendInvoiceSMSANDEmail).pipe(
        takeUntil(this.destroy$)
      ).subscribe(result => {
        if (result) {
          if (result.Data) {
            this.smsEmailSuccessEvent.emit(true);
          } else {
            this.smsEmailSuccessEvent.emit(false);
          }
        } else {
          this.smsEmailSuccessEvent.emit(false);
        }
      },
        error => {
          if (error) {
            this.smsEmailSuccessEvent.emit(false);
          }
        }
      );
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

