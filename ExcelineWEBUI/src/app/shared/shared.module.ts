import { Inject, NgModule, ModuleWithProviders } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { UsBreadcrumbComponent } from './components/us-breadcrumb/us-breadcrumb.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ExceError, ExceErrorWindow } from './directives/exce-error/exce-error';
import { ExceErrorConfig } from './directives/exce-error/exce-error-config';
import { ExceSchedulerComponent } from './components/exce-scheduler/exce-scheduler.component';
import { UsTabComponent } from './components/us-tab/us-tab.component';

/** data gird related import start >>>>>>>>>>>>>>>>>>>>>*/
import { USDataGridComponent } from './components/us-data-grid/us-data-grid.component';
import { MetaDataService } from './components/us-data-grid/util/meta-data.service';
import { FilterPipe } from './components/us-data-grid/util/filter.pipe';
import { NgbDateFrParserFormatterService } from './components/us-data-grid/util/ngb-date-fr-parser-formatter.service';
import { TranslateDataService } from './components/us-data-grid/util/translate-data.service';
import { ValidatorService } from './components/us-data-grid/util/validator.service';
/** <<<<<<<<<<<<<<<<<<<<<< data gird related import end */

import { UsIconButtonComponent } from './components/us-icon-button/us-icon-button.component';
import { TabContentComponent } from './components/us-tab/tab-content/tab-content.component';
import { UsTabVerticleComponent } from './components/us-tab-verticle/us-tab-verticle.component';
import { VtabContentComponent } from './components/us-tab-verticle/vtab-content/vtab-content.component';
import { UsDataTableComponent } from './components/us-data-table/us-data-table.component';
import { DataTableColumn } from './components/us-data-table/components/column/column.component';
import { DataTableRow } from './components/us-data-table/components/row/row.component';
import { DataTablePagination } from './components/us-data-table/components/pagination/pagination.component';
import { DataTableHeader } from './components/us-data-table/components/header/header.component';

import { PixelConverter } from './components/us-data-table/utils/px';
import { Hide } from './components/us-data-table/utils/hide';
import { MinPipe } from './components/us-data-table/utils/min';
import { UsPopover, UsPopoverWindow } from './components/us-popover/us-popover.component';
import { UsPopoverConfig } from './components/us-popover/us-popover-config';
import { DataTableFilterPipe } from './components/us-data-table/tools/data-table-filter-pipe';
import { AmountConverterPipe } from './pipes/amount-converter.pipe';
export { ExceError } from './directives/exce-error/exce-error';
export { ExceErrorConfig } from './directives/exce-error/exce-error-config';
import { ExceColorPickerService } from './components/exce-color-picker/exce-color-picker.service';
import { ExceColorPickerComponent } from './components/exce-color-picker/exce-color-picker.component';
import { UsTypeheadComponent } from './components/us-typehead/us-typehead.component';
import { UsbModal } from './components/us-modal/us-modal'
import { UsbModalBackdrop } from './components/us-modal/modal-backdrop';
import { UsbModalWindow } from './components/us-modal/modal-window';
import { UsbModalStack } from './components/us-modal/modal-stack';
import { UsDatePickerComponent } from './components/us-date-picker/us-date-picker.component';
import { FocusDirective } from './components/us-date-picker/directives/my-date-picker.focus.directive';
import { UsAutocompleteComponent } from './components/us-autocomplete/us-autocomplete.component'
import { ExceMessageComponent } from './components/exce-message/exce-message.component';
import { UsScrollDirective } from './directives/us-scroll/us-scroll.directive';
import { UsFormWizardComponent } from './components/us-form-wizard/us-form-wizard.component';
import { WizardStepComponent } from './components/us-form-wizard/wizard-step/wizard-step.component';
import { ExceMobileDirective } from './directives/exce-mobile/exce-mobile.directive'
import { UsTimePickerComponent } from './components/us-time-picker/us-time-picker.component';
import { AgGridModule } from 'ag-grid-angular';
import { MouseWheelDirective } from './directives/mouse-wheel/mouse-wheel.directive';
import { ExceEntityselectionComponent } from './components/exce-entityselection/exce-entityselection.component';
import { ExceMemberlistComponent } from './components/exce-memberlist/exce-memberlist.component';
import { ExceAddmemberComponent } from './components/exce-addmember/exce-addmember.component';
import { ExceCategoryComponent } from './components/exce-category/exce-category.component';
import { UsDtabComponent } from './components/us-dtab/us-dtab.component';
import { TabContainerComponent } from './components/us-dtab/tab-container/tab-container.component';
import { UsErrorDirective } from './directives/us-error/us-error.component';
import { ErrorWindowComponent } from './directives/us-error/error-window/error-window.component';
import { ExceContractTmpSelectComponent } from './components/exce-contract-tmp-select/exce-contract-tmp-select.component';
import { ExcePostalcodeComponent } from './components/exce-postalcode/exce-postalcode.component';
import { IntlTelInputDirective } from './directives/intl-tel-input/intl-tel-input.directive';
import { IntlTelInputComponent } from './components/intl-tel-input/intl-tel-input/intl-tel-input.component';
import { ExceEntityselectionService } from 'app/shared/components/exce-entityselection/exce-entityselection.service';
import { ExceCountryComponent } from './components/exce-country/exce-country.component';
import { ExceAddFollowUpComponent } from './components/exce-followup/exce-add-follow-up/exce-add-follow-up.component';
import { ExceFollowUpTemplateComponent } from './components/exce-followup/exce-follow-up-template/exce-follow-up-template.component';
import { ExceAddFollowUpService } from 'app/shared/components/exce-followup/exce-add-follow-up.service';
import { AmountConverterDirective } from './directives/amount-converter/amount-converter.directive';
import { UsAmountInputComponent } from './components/us-amount-input/us-amount-input/us-amount-input.component';
import { ExceContracttmpService } from './components/exce-contract-tmp-select/exce-contracttmp.service';
import { ExcePdfViewerComponent } from './components/exce-pdf-viewer/exce-pdf-viewer.component';
import { OnlyNumbers } from './directives/only-numbers/only-numbers.directive';
import { UsTreeTableComponent } from './components/us-tree-table/us-tree-table.component';
import { TreeRowComponent } from './components/us-tree-table/tree-row/tree-row.component';
import { HeaderComponent } from './components/us-tree-table/header/header.component';
import { FooterComponent } from './components/us-tree-table/footer/footer.component';
import { ColumnComponent } from './components/us-tree-table/column/column.component';
import { ColumnBodyTemplateLoaderComponent } from './components/us-tree-table/column-body-template-loader/column-body-template-loader.component';
import { ColumnHeaderTemplateLoaderComponent } from './components/us-tree-table/column-header-template-loader/column-header-template-loader.component';
import { ColumnFooterTemplateLoaderComponent } from './components/us-tree-table/column-footer-template-loader/column-footer-template-loader.component';
import { TreeviewComponent } from './components/us-checkbox-tree/treeview.component'
import { TreeviewItemComponent } from './components/us-checkbox-tree/treeview-item.component'
import { DropdownTreeviewComponent } from './components/us-checkbox-tree/dropdown-treeview.component'
import { TreeviewPipe, DropdownDirective, DropdownToggleDirective } from 'app/shared/components/us-checkbox-tree';
import { DropdownMenuDirective } from 'app/shared/components/us-checkbox-tree/dropdown-menu.directive';
import { TreeviewI18n, TreeviewI18nDefault } from 'app/shared/components/us-checkbox-tree/treeview-i18n';
import { TreeviewConfig } from 'app/shared/components/us-checkbox-tree/treeview-config';
import { TreeviewEventParser, DefaultTreeviewEventParser } from 'app/shared/components/us-checkbox-tree/treeview-event-parser';
import { ExcePwResetComponent } from 'app/shared/components/exce-pw-reset/exce-pw-reset.component';
import { MemberNotificationDetailComponent } from 'app/shared/components/member-notification-detail/member-notification-detail.component';
import { TranslateModule, TranslateService, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { UsContextmenuComponent } from './components/us-contextmenu/us-contextmenu.component';
import { ExceFixedFollowUpComponent } from './components/exce-followup/exce-fixed-follow-up/exce-fixed-follow-up.component';
import { Observable, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { UsCropImageComponent } from './components/us-crop-image/us-crop-image.component';
import { ReportViewerComponent } from './components/exce-report-viewer/reportviewer.component';
import { ExceNotifyInvoiceComponent } from './components/exce-notify-invoice/exce-notify-invoice.component';
import { TextDirective } from './components/exce-color-picker/text.directive';
import { SliderDirective } from './components/exce-color-picker/slider.directive';
import { DialogComponent } from './components/exce-color-picker/dialog/dialog.component';
import { ExceBreadcrumbComponent } from './components/exce-breadcrumb/exce-breadcrumb.component';
import { MoveNextOnMaxlengthDirective } from './directives/move-next-on-maxlength/move-next-on-maxlength.directive';
import { ExceFadeInNOutComponent } from './components/exce-fadeinandout/exce-fade-in-n-out.component';
import { DebounceClickDirective } from './directives/debounce-click/debounce-click.directive';
// export function createTranslateLoader(http: HttpClient) {
//   return new TranslateHttpLoader(http, './assets/i18n/', '.json');
// }

export function translateLoader(http: HttpClient) {

  return new MultiTranslateHttpLoader(http, [
    { prefix: './assets/i18n/', suffix: '.json' },
    { prefix: './assets/i18n/modules/admin/', suffix: '.json' },
    { prefix: './assets/i18n/modules/class/', suffix: '.json' },
    { prefix: './assets/i18n/modules/common-notification/', suffix: '.json' }
  ]);
}

export class MultiTranslateHttpLoader implements TranslateLoader {

  constructor(private http: HttpClient,
    public resources: { prefix: string, suffix: string }[] = [
      {
        prefix: '/assets/i18n/',
        suffix: '.json'
      }
    ]) { }

  /**
   * Gets the translations from the server
   * @param lang
   * @returns {any}
   */
  public getTranslation(lang: string): any {

    return forkJoin(this.resources.map(config => {
      return this.http.get(`${config.prefix}${lang}${config.suffix}`);
    })).pipe(
      map(response => {
        return response.reduce((a, b) => {
          return Object.assign(a, b);
        });
    }));
  }
}




@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AgGridModule.withComponents(null),
    NgbModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (TranslateLoader),
        deps: [HttpClient]
      }
    })
  ],
  declarations: [
    UsBreadcrumbComponent,
    ExceError,
    ExceErrorWindow,
    ExceSchedulerComponent,
    USDataGridComponent,
    FilterPipe,
    UsTabComponent,
    TabContentComponent,
    UsIconButtonComponent,
    UsTabVerticleComponent,
    VtabContentComponent,
    UsDataTableComponent,
    DataTableColumn,
    DataTableRow,
    DataTablePagination,
    DataTableHeader,
    PixelConverter,
    Hide,
    MinPipe,
    UsPopover,
    UsPopoverWindow,
    DataTableFilterPipe,
    ExceColorPickerComponent,
    AmountConverterPipe,
    UsTypeheadComponent,
    UsbModalBackdrop,
    UsbModalWindow,
    UsDatePickerComponent,
    FocusDirective,
    UsAutocompleteComponent,
    ExceMessageComponent,
    UsScrollDirective,
    UsFormWizardComponent,
    WizardStepComponent,
    ExceMobileDirective,
    UsTimePickerComponent,
    MouseWheelDirective,
    ExceCategoryComponent,
    ExceEntityselectionComponent,
    ExceMemberlistComponent,
    ExceAddmemberComponent,
    UsDtabComponent,
    TabContainerComponent,
    UsErrorDirective,
    ErrorWindowComponent,
    ExceContractTmpSelectComponent,
    ExcePostalcodeComponent,
    IntlTelInputDirective,
    IntlTelInputComponent,
    ExceCountryComponent,
    ExceFollowUpTemplateComponent,
    ExceAddFollowUpComponent,
    AmountConverterDirective,
    UsAmountInputComponent,
    ExcePdfViewerComponent,
    OnlyNumbers,
    UsTreeTableComponent,
    TreeRowComponent,
    HeaderComponent,
    FooterComponent,
    ColumnComponent,
    ColumnBodyTemplateLoaderComponent,
    ColumnHeaderTemplateLoaderComponent,
    ColumnFooterTemplateLoaderComponent,
    TreeviewComponent,
    TreeviewItemComponent,
    DropdownTreeviewComponent,
    TreeviewPipe,
    DropdownDirective,
    DropdownMenuDirective,
    DropdownToggleDirective,
    MemberNotificationDetailComponent,
    ExcePwResetComponent,
    UsContextmenuComponent,
    ExceFixedFollowUpComponent,
    UsCropImageComponent,
    ReportViewerComponent,
    ExceNotifyInvoiceComponent,
    TextDirective,
    SliderDirective,
    DialogComponent,
    ExceBreadcrumbComponent,
    MoveNextOnMaxlengthDirective,
    ExceFadeInNOutComponent,
    DebounceClickDirective

  ],
  exports: [
    UsBreadcrumbComponent,
    TranslateModule,
    ExceError,
    ExceErrorWindow,
    ExceSchedulerComponent,
    USDataGridComponent,
    UsTabComponent,
    TabContentComponent,
    UsIconButtonComponent,
    UsTabVerticleComponent,
    VtabContentComponent,
    UsDataTableComponent,
    DataTableColumn,
    UsPopover,
    ExceColorPickerComponent,
    UsTypeheadComponent,
    UsDatePickerComponent,
    FocusDirective,
    UsAutocompleteComponent,
    ExceMessageComponent,
    UsScrollDirective,
    UsFormWizardComponent,
    WizardStepComponent,
    ExceMobileDirective,
    UsTimePickerComponent,
    MouseWheelDirective,
    AmountConverterPipe,
    ExceEntityselectionComponent,
    ExceMemberlistComponent,
    ExceCategoryComponent,
    ExceAddmemberComponent,
    UsDtabComponent,
    TabContainerComponent,
    UsErrorDirective,
    ExcePostalcodeComponent,
    IntlTelInputDirective,
    IntlTelInputComponent,
    ExceFollowUpTemplateComponent,
    ExceAddFollowUpComponent,
    AmountConverterDirective,
    UsAmountInputComponent,
    ExceContractTmpSelectComponent,
    ExcePdfViewerComponent,
    OnlyNumbers,
    UsTreeTableComponent,
    TreeRowComponent,
    HeaderComponent,
    FooterComponent,
    ColumnComponent,
    ColumnBodyTemplateLoaderComponent,
    ColumnHeaderTemplateLoaderComponent,
    ColumnFooterTemplateLoaderComponent,
    TreeviewComponent,
    TreeviewItemComponent,
    DropdownTreeviewComponent,
    MemberNotificationDetailComponent,
    ExcePwResetComponent,
    UsContextmenuComponent,
    ExceFixedFollowUpComponent,
    ReportViewerComponent,
    ExceNotifyInvoiceComponent,
    UsCropImageComponent,
    ExceFixedFollowUpComponent,
    ExceBreadcrumbComponent,
    MoveNextOnMaxlengthDirective,
    ExceFadeInNOutComponent,
    DebounceClickDirective

  ],
  providers: [
    ExceErrorConfig,
    UsPopoverConfig,
    MetaDataService,
    FilterPipe,
    NgbDateFrParserFormatterService,
    TranslateDataService,
    ValidatorService,
    DataTableFilterPipe,
    ExceColorPickerService,
    TranslateService,
    ExceEntityselectionService,
    ExceContracttmpService

  ],
  entryComponents: [
    ExceErrorWindow,
    ErrorWindowComponent,
    UsPopoverWindow,
    UsbModalBackdrop,
    UsbModalWindow,
    UsAutocompleteComponent,
    ExceMessageComponent,
    DialogComponent
  ]
})

export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [ExceErrorConfig, UsPopoverConfig, UsbModal, UsbModalStack,
        TreeviewConfig,
        { provide: TreeviewI18n, useClass: TreeviewI18nDefault },
        { provide: TreeviewEventParser, useClass: DefaultTreeviewEventParser }
      ]
    };
  }

}
