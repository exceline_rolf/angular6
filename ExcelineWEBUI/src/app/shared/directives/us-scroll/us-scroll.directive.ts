
import { Directive, ViewContainerRef, HostListener, OnInit, Renderer2, Inject, Input, EventEmitter, Output, OnDestroy } from '@angular/core';
import { SlimScrollOptions } from './classes/slimscroll-options.class';
import { SlimScrollEvent } from './classes/slimscroll-event.class';
import { Observable, Subject, fromEvent, merge } from 'rxjs';
import { takeUntil, mergeMap, map } from 'rxjs/operators';
import { UsScrollService } from './us-scroll.service';

export const easing: { [key: string]: Function } = {
  linear: (t: number) => { return t; },
  inQuad: (t: number) => { return t * t; },
  outQuad: (t: number) => { return t * (2 - t); },
  inOutQuad: (t: number) => { return t < .5 ? 2 * t * t : -1 + (4 - 2 * t) * t; },
  inCubic: (t: number) => { return t * t * t; },
  outCubic: (t: number) => { return (--t) * t * t + 1; },
  inOutCubic: (t: number) => { return t < .5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1; },
  inQuart: (t: number) => { return t * t * t * t },
  outQuart: (t: number) => { return 1 - (--t) * t * t * t; },
  inOutQuart: (t: number) => { return t < .5 ? 8 * t * t * t * t : 1 - 8 * (--t) * t * t * t; },
  inQuint: (t: number) => { return t * t * t * t * t; },
  outQuint: (t: number) => { return 1 + (--t) * t * t * t * t; },
  inOutQuint: (t: number) => { return t < .5 ? 16 * t * t * t * t * t : 1 + 16 * (--t) * t * t * t * t; }
}

@Directive({
  selector: '[usScroll]',
  exportAs: 'usScroll'
})

export class UsScrollDirective implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();

  @Input() options: SlimScrollOptions;
  @Input() scrollEvents: EventEmitter<SlimScrollEvent>;
  @Input() reloadScroll = new EventEmitter();

  el: HTMLElement;
  wrapper: HTMLElement;
  grid: HTMLElement;
  bar: HTMLElement;
  body: HTMLElement;
  pageY: number;
  top: number;
  dragging: boolean;
  mutationThrottleTimeout: number;
  mutationObserver: MutationObserver;
  lastTouchPositionY: number;
  visibleTimeout: any;

  constructor(
    private usScrollService: UsScrollService,
    @Inject(ViewContainerRef) private viewContainer: ViewContainerRef,
    @Inject(Renderer2) private renderer: Renderer2
  ) {
    if (typeof window === 'undefined') {
      return;
    }
    this.viewContainer = viewContainer;
    this.el = viewContainer.element.nativeElement;
    this.body = document.documentElement.querySelector('body');
    this.mutationThrottleTimeout = 50;
  }

  ngOnInit(): void {
    if (typeof window === 'undefined') {
      return;
    }
    this.options = new SlimScrollOptions(this.options);
    this.destroy();
    this.setElementStyle();
    this.wrapContainer();
    this.initGrid();
    this.initBar();
    this.getBarHeight();
    this.initWheel();
    this.initDrag();
    if (!this.options.alwaysVisible) {
      this.hideBarAndGrid();
    }
    if (MutationObserver) {
      this.mutationObserver = new MutationObserver(() => {
        if (this.mutationThrottleTimeout) {
          clearTimeout(this.mutationThrottleTimeout);
          this.mutationThrottleTimeout = setTimeout(this.onMutation.bind(this), 50);
        }
      });
      this.mutationObserver.observe(this.el, { subtree: true, childList: true });
    }
    if (this.scrollEvents && this.scrollEvents instanceof EventEmitter) {
      this.scrollEvents.pipe(
        takeUntil(this.destroy$)
        ).subscribe((event: SlimScrollEvent) => this.handleEvent(event));
    }
    if (this.usScrollService.reloadScroll) {
      this.usScrollService.reloadScroll.pipe(
        takeUntil(this.destroy$)
        ).subscribe((event) => {
        this.getBarHeight();
      });
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  handleEvent(e: SlimScrollEvent): void {
    if (e.type === 'scrollToBottom') {
      const y = this.el.scrollHeight - this.el.clientHeight;
      this.scrollTo(y, e.duration, e.easing);
    } else if (e.type === 'scrollToTop') {
      const y = 0;
      this.scrollTo(y, e.duration, e.easing);
    } else if (e.type === 'scrollToPercent' && (e.percent >= 0 && e.percent <= 100)) {
      const y = Math.round(((this.el.scrollHeight - this.el.clientHeight) / 100) * e.percent);
      this.scrollTo(y, e.duration, e.easing);
    } else if (e.type === 'scrollTo') {
      const y = e.y;
      if (y <= this.el.scrollHeight - this.el.clientHeight && y >= 0) {
        this.scrollTo(y, e.duration, e.easing);
      }
    }
  }

  setElementStyle(): void {
    const el = this.el;
    this.renderer.setStyle(el, 'overflow', 'hidden');
    this.renderer.setStyle(el, 'position', 'relative');
    this.renderer.setStyle(el, 'display', 'block');
  }

  onMutation() {
    this.getBarHeight();
  }

  wrapContainer(): void {
    this.wrapper = document.createElement('div');
    const wrapper = this.wrapper;
    const el = this.el;

    this.renderer.addClass(wrapper, 'slimscroll-wrapper');
    this.renderer.setStyle(wrapper, 'position', 'relative');
    this.renderer.setStyle(wrapper, 'overflow', 'hidden');
    this.renderer.setStyle(wrapper, 'display', 'inline-block');
    this.renderer.setStyle(wrapper, 'margin', getComputedStyle(el).margin);
    this.renderer.setStyle(wrapper, 'width', 'inherit');
    this.renderer.setStyle(wrapper, 'height', getComputedStyle(el).height);

    el.parentNode.insertBefore(wrapper, el);
    wrapper.appendChild(el);
  }

  initGrid(): void {
    this.grid = document.createElement('div');
    const grid = this.grid;

    this.renderer.addClass(grid, 'slimscroll-grid');
    this.renderer.setStyle(grid, 'position', 'absolute');
    this.renderer.setStyle(grid, 'top', '0');
    this.renderer.setStyle(grid, 'bottom', '0');
    this.renderer.setStyle(grid, this.options.position, '0');
    this.renderer.setStyle(grid, 'width', `${this.options.gridWidth}px`);
    this.renderer.setStyle(grid, 'background', this.options.gridBackground);
    this.renderer.setStyle(grid, 'opacity', this.options.gridOpacity);
    this.renderer.setStyle(grid, 'display', 'block');
    this.renderer.setStyle(grid, 'cursor', 'pointer');
    this.renderer.setStyle(grid, 'z-index', '99');
    this.renderer.setStyle(grid, 'border-radius', `${this.options.gridBorderRadius}px`);
    this.renderer.setStyle(grid, 'margin', this.options.gridMargin);

    this.wrapper.appendChild(grid);
  }

  initBar(): void {
    this.bar = document.createElement('div');
    const bar = this.bar;

    this.renderer.addClass(bar, 'slimscroll-bar');
    this.renderer.setStyle(bar, 'position', 'absolute');
    this.renderer.setStyle(bar, 'top', '0');
    this.renderer.setStyle(bar, this.options.position, '0');
    this.renderer.setStyle(bar, 'width', `${this.options.barWidth}px`);
    this.renderer.setStyle(bar, 'background', this.options.barBackground);
    this.renderer.setStyle(bar, 'opacity', this.options.barOpacity);
    this.renderer.setStyle(bar, 'display', 'block');
    this.renderer.setStyle(bar, 'cursor', 'pointer');
    this.renderer.setStyle(bar, 'z-index', '100');
    this.renderer.setStyle(bar, 'border-radius', `${this.options.barBorderRadius}px`);
    this.renderer.setStyle(bar, 'margin', this.options.barMargin);

    this.wrapper.appendChild(bar);
  }

  getBarHeight(): void {
    setTimeout(() => {
      const barHeight = Math.max((this.el.offsetHeight / this.el.scrollHeight) * this.el.offsetHeight, 30) + 'px';
      const display = parseInt(barHeight, 10) === this.el.offsetHeight ? 'none' : 'block';

      this.renderer.setStyle(this.bar, 'height', barHeight);
      this.renderer.setStyle(this.bar, 'display', display);
      this.renderer.setStyle(this.grid, 'display', display);
    }, 1);
  }

  scrollTo(y: number, duration: number, easingFunc: string): void {
    const start = Date.now();
    const from = this.el.scrollTop;
    const maxTop = this.el.offsetHeight - this.bar.offsetHeight;
    const maxElScrollTop = this.el.scrollHeight - this.el.clientHeight;
    const barHeight = Math.max((this.el.offsetHeight / this.el.scrollHeight) * this.el.offsetHeight, 30);

    if (from === y) {
      return;
    }

    const scroll = (timestamp: number) => {
      const currentTime = Date.now();
      const time = Math.min(1, ((currentTime - start) / duration));
      const easedTime = easing[easingFunc](time);

      this.el.scrollTop = (easedTime * (y - from)) + from;

      const percentScroll = this.el.scrollTop / maxElScrollTop;
      const delta = Math.round(Math.round(this.el.clientHeight * percentScroll) - barHeight);
      if (delta > 0) {
        this.renderer.setStyle(this.bar, 'top', `${delta}px`);
      }

      if (time < 1) {
        requestAnimationFrame(scroll);
      }
    }

    requestAnimationFrame(scroll);
  }

  scrollContent(y: number, isWheel: boolean, isJump: boolean): void {
    let delta = y;
    const maxTop = this.el.offsetHeight - this.bar.offsetHeight;
    let percentScroll: number;
    const bar = this.bar;
    const grid = this.grid;
    const el = this.el;

    if (isWheel) {
      delta = parseInt(getComputedStyle(bar).top, 10) + y * 20 / 100 * bar.offsetHeight;
      delta = Math.min(Math.max(delta, 0), maxTop);
      delta = (y > 0) ? Math.ceil(delta) : Math.floor(delta);
      this.renderer.setStyle(bar, 'top', delta + 'px');
    }

    percentScroll = parseInt(getComputedStyle(bar).top, 10) / (el.offsetHeight - bar.offsetHeight);
    delta = percentScroll * (el.scrollHeight - el.offsetHeight);

    el.scrollTop = delta;

    this.showBarAndGrid();

    if (!this.options.alwaysVisible) {
      if (this.visibleTimeout) {
        clearTimeout(this.visibleTimeout);
      }

      this.visibleTimeout = setTimeout(() => {
        this.hideBarAndGrid();
      }, this.options.visibleTimeout);
    }
  }

  initWheel = () => {
    const dommousescroll = fromEvent(this.el, 'DOMMouseScroll');
    const mousewheel = fromEvent(this.el, 'mousewheel');

    merge(...[dommousescroll, mousewheel]).pipe(
      takeUntil(this.destroy$)
      ).subscribe((e: MouseWheelEvent) => {
      let delta = 0;
      if (e.deltaY) {
        delta = -e.deltaY / 120;
      }
      if (e.detail) {
        delta = e.detail / 3;
      }
      this.scrollContent(delta, true, false);
      if (e.preventDefault) {
        e.preventDefault();
      }
    });
  }

  initDrag = () => {
    const bar = this.bar;

    const mousemove = fromEvent(document.documentElement, 'mousemove');
    const touchmove = fromEvent(document.documentElement, 'touchmove');

    const mousedown = fromEvent(bar, 'mousedown');
    const touchstart = fromEvent(this.el, 'touchstart');
    const mouseup = fromEvent(document.documentElement, 'mouseup');
    const touchend = fromEvent(document.documentElement, 'touchend');

    const mousedrag = mousedown.pipe(
      mergeMap((e: MouseEvent) => {
        this.pageY = e.pageY;
        this.top = parseFloat(getComputedStyle(bar).top);

        return mousemove.pipe(
          map((emove: MouseEvent) => {
            emove.preventDefault();
            return this.top + emove.pageY - this.pageY;
          })
        )}),
      takeUntil(mouseup));

    const touchdrag = touchstart.pipe(
      mergeMap((e: TouchEvent) => {
        this.pageY = e.targetTouches[0].pageY;
        this.top = -parseFloat(getComputedStyle(bar).top);

        return touchmove.pipe(
          map((tmove: TouchEvent) => {
            return -(this.top + tmove.targetTouches[0].pageY - this.pageY);
          })
      )}),
      takeUntil(touchend));

    merge(...[mousedrag, touchdrag]).pipe(
      takeUntil(this.destroy$)
    ).subscribe((top: number) => {
      this.body.addEventListener('selectstart', this.preventDefaultEvent, false);
      this.renderer.setStyle(this.bar, 'top', `${top}px`);
      this.scrollContent(0, true, false);
    });

    merge(...[mouseup, touchend]).pipe(
      takeUntil(this.destroy$)
    ).subscribe(() => {
      this.body.removeEventListener('selectstart', this.preventDefaultEvent, false);
    });
  };

  showBarAndGrid(): void {
    this.renderer.setStyle(this.grid, 'background', this.options.gridBackground);
    this.renderer.setStyle(this.bar, 'background', this.options.barBackground);
  }

  hideBarAndGrid(): void {
    this.renderer.setStyle(this.grid, 'background', 'transparent');
    this.renderer.setStyle(this.bar, 'background', 'transparent');
  }

  preventDefaultEvent = (e: MouseEvent) => {
    e.preventDefault();
    e.stopPropagation();
  };

  destroy(): void {
    if (this.mutationObserver) {
      this.mutationObserver.disconnect();
      this.mutationObserver = null;
    }

    if (this.el.parentElement.classList.contains('slimscroll-wrapper')) {
      const wrapper = this.el.parentElement;
      const bar = this.el.querySelector('.slimscroll-bar');
      this.el.removeChild(bar);
      this.unwrap(wrapper);
    }
  }

  unwrap(wrapper: HTMLElement): void {
    const docFrag = document.createDocumentFragment();
    while (wrapper.firstChild) {
      const child = wrapper.removeChild(wrapper.firstChild);
      docFrag.appendChild(child);
    }
    wrapper.parentNode.replaceChild(docFrag, wrapper);
  }

  @HostListener('window:resize', ['$event'])
  onResize($event: any) {
    this.getBarHeight();
  }
}
