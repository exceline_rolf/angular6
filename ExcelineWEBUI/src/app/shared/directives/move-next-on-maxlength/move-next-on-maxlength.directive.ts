import { Directive, ElementRef,  Renderer2,  OnInit,  HostListener } from '@angular/core';

@Directive({
  selector: '[appMoveNextOnMaxlength]'
})
export class MoveNextOnMaxlengthDirective implements OnInit {
/*
Use by setting a maxlength on the input field and naming them : #part1 #part2 #part3
as it will define the jump sequence
*/
  constructor(
    private elem: ElementRef,
    private rend: Renderer2
  ) {}

    @HostListener('input') oneinput() {
    const l = this.elem.nativeElement.value.length;
    const k = this.elem.nativeElement.getAttribute('id');
    if (l === Number(this.elem.nativeElement.getAttribute('maxlength'))) {
        const nextElement = this.rend.selectRootElement('#' + k.slice(0, 4) + (Number(k.slice(4, 5) ) + 1));
        nextElement.focus();
    }
    }

   ngOnInit() {
  }

}
