import { Directive, ElementRef, Input, HostListener } from '@angular/core';
import { ExceService } from '../exce-error/exce-error';
import { TranslateService } from '@ngx-translate/core';


@Directive({
    selector: '[exceMobile]'
})
export class ExceMobileDirective {
    @Input() mobileNo: any;
    @Input() mobilePrefix: any;
    @Input() phoneType: string;
    element: ElementRef;
    countryCodeError: string;
    phoneNumberIncorrectError: string;

    constructor(
        el: ElementRef,
        private messageService: ExceService,
        private translateService: TranslateService
    ) {
        this.element = el;
        this.translateService.get('MEMBERSHIP.crCountryCodeFormat').subscribe(tranlstedValue => this.countryCodeError = tranlstedValue);
        this.translateService.get('MEMBERSHIP.crPhoneNumberIncorrect').subscribe(tranlstedValue => this.phoneNumberIncorrectError = tranlstedValue);
    }

    @HostListener('blur') onblur() {
        const validationResult = this.IsNorwayTelephoneNoValid(this.mobileNo, this.mobilePrefix);
        if (validationResult === 1) {
            this.messageService.openMessage(this.countryCodeError, this.element);
        } else if (validationResult === 2) {
            this.messageService.openMessage(this.phoneNumberIncorrectError, this.element);
        }
    }

    public IsNorwayTelephoneNoValid(mobilePhoneNo: any, mobilePhoneNoPrifix: any): number {
        let validResult = this.PrefixNumberValid(mobilePhoneNoPrifix);
        if (validResult === 0 && mobilePhoneNo !== 'undefined' && mobilePhoneNo) {
            if (isNaN(mobilePhoneNo)) {
                validResult = 2;
            } else {
                if (mobilePhoneNoPrifix === '+47') {
                    if (!(mobilePhoneNo.length === 8)) {
                        validResult = 2;
                    } else {
                        if (this.phoneType === 'MOB') {
                            if (mobilePhoneNo.startsWith('4') || mobilePhoneNo.startsWith('9')) {
                                validResult = 0;
                            } else {
                                validResult = 2;
                            }
                        } else {
                            validResult = 0;
                        }
                    }
                } else if (!(mobilePhoneNo.trim().length <= 12)) {
                    validResult = 2;
                }
            }
        }
        return validResult;
    }

    public PrefixNumberValid(mobilePhoneNoPrifix: string): number {
        let validResult = 0;
        if (mobilePhoneNoPrifix !== 'undefined' && mobilePhoneNoPrifix && mobilePhoneNoPrifix.length >= 2) {
            if (this.IsNumberDigitsValid(mobilePhoneNoPrifix.substring(1, mobilePhoneNoPrifix.length))) {

            } else {
                validResult = 1;
            }
        } else {
            validResult = 1;
        }
        return validResult;
    }

    private IsNumberDigitsValid(imputNumber: any): boolean {
        let isNumberValid = true;
        if (isNaN(imputNumber)) {
            isNumberValid = false;
        }
        return isNumberValid;
    }


}
