

import {
  Component,
  Directive,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  OnInit,
  OnDestroy,
  Injector,
  Renderer2,
  ComponentRef,
  ElementRef,
  TemplateRef,
  ViewContainerRef,
  ComponentFactoryResolver,
  NgZone,
  OnChanges,
  SimpleChanges,
  ViewChild,
  HostListener,
  Injectable
} from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator, ValidatorFn, Validators, FormControl } from '@angular/forms';

import { listenToTriggers } from './util/triggers';
import { positionElements } from './util/positioning';
import { PopupService } from './util/popup';
import { ExceErrorConfig } from './exce-error-config';
import { ExceToolbarService } from '../../../modules/common/exce-toolbar/exce-toolbar.service';
import { Subject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';



@Injectable()
export class ExceService {

  public invokeEvent: Subject<any> = new Subject();
  public messageEvent: EventEmitter<any> = new EventEmitter();
  public EventEmitter
  constructor() {

  }

  callComponent(value) {
    this.invokeEvent.next({ some: value })
  }

  openMessage(messageText: string, elementref: ElementRef) {
    this.messageEvent.emit({message: messageText, element: elementref});
  }
}



let nextId = 0;

/** A hero's name can't match the given regular expression */
export function forbiddenNameValidator(nameRe: RegExp): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } => {
    const name = control.value;
    const no = nameRe.test(name);
    return no ? { 'forbiddenName': { name } } : null;
  };
}


@Component({
  selector: 'exce-error-window',
  changeDetection: ChangeDetectionStrategy.OnPush,
  host: { '[class]': '"popover exce-validate exce-validate-" + placement', 'role': 'tooltip', '[id]': 'id' },
  template: `
  <div class="arrow"></div><div class="exce-validate-content"><ng-content></ng-content></div>`,
  styles: [`
      :host.exce-validate-top .arrow, :host.exce-validate-bottom .arrow {
          left: 50%;
       }
       :host.bexce-validate-left .arrow, :host.exce-validate-right .arrow {
         top: 50%;
       }
     `]
})
export class ExceErrorWindow {
  @Input() placement: 'top' | 'bottom' | 'left' | 'right' = 'top';
  @Input() title: string;
  @Input() id: string;
}

/**
 * A lightweight, extensible directive for fancy popover creation.
 */
@Directive({
  selector: '[exceError]', exportAs: 'exceError',
  providers: [{ provide: NG_VALIDATORS, useExisting: ExceError, multi: true },
    // TranslateService
  ]
})


export class ExceError implements OnInit, OnDestroy, Validator, OnChanges {

  private valFn = Validators.nullValidator;
  private _ngbPopoverWindowId = `exce-error-${nextId++}`;
  private _popupService: PopupService<ExceErrorWindow>;
  private _windowRef: ComponentRef<ExceErrorWindow>;
  private _unregisterListenersFn;
  private _zoneSubscription: any;


  private generalMessages = {
    requiredStr: ``,
    minLengthStr: ``,
    maxLengthStr: ``,
    patternStr: ``
  }

  // if adding any pattern messages then add them to the localizatino json file in sample forlder
  // and update the functions getAllTranslated() getStringTranslated() appropriately
  private patternMessages = {
    email: ``,
    postalcode: ``,
    bankAccount: '',
    creditCollector: '',
    duplicateName:''
  }

  /**
   * Content to be displayed as popover.
   */
  @Input() exceError: FormControl; // string | TemplateRef<any>
  /**
   * Title of a popover.
   */
  @Input() popoverTitle: string;
  /**
   * Placement of a popover. Accepts: "top", "bottom", "left", "right"
   */
  @Input() placement: 'top' | 'bottom' | 'left' | 'right';
  /**
   * Specifies events that should trigger. Supports a space separated list of event names.
   */
  @Input() triggers = 'manual';
  /**
   * A selector specifying the element the popover should be appended to.
   * Currently only supports "body".
   */
  @Input() container: string;
  /**
   * Emits an event when the popover is shown
   */
  @Output() shown = new EventEmitter();
  /**
   * Emits an event when the popover is hidden
   */
  @Output() hidden = new EventEmitter();

  @Input() showError: Boolean;

  @Input() validationType: string;  // whether it is an email/postalcode
  @Input() validationMesge: string; // specify the custom messages
  @Input() reqValidationMesge: string; // specify the custom messages



  @HostListener('focusout') onFocusOut() {
    this.validate(this.exceError);  // checkes the FormControl Object of this.exceError

  }

  @HostListener('blur') onblur() {
    this.validate(this.exceError);  // checkes the FormControl Object of this.exceError
  }

  @HostListener('keypress') onkeypress() {
    this.close();
  }

  @HostListener('click', ['$event'])
  onClick(e) {
}


  constructor(
    private _translate: TranslateService,
    private _elementRef: ElementRef,
    private _renderer: Renderer2,
    private injector: Injector,
    private componentFactoryResolver: ComponentFactoryResolver,
    private viewContainerRef: ViewContainerRef,
    private config: ExceErrorConfig,
    private ngZone: NgZone,
    private _injector: Injector,
    private _viewContainerRef: ViewContainerRef,
    private _exceToolbarService: ExceToolbarService,
    private _exceService: ExceService,
  ) {
    this.placement = config.placement;
    this.triggers = config.triggers;
    this.container = config.container;
    this._exceService.invokeEvent.subscribe((value) => {
      this.validate(value.some)
    });
    this._popupService = new PopupService<ExceErrorWindow>(
      ExceErrorWindow, injector, viewContainerRef, _renderer, componentFactoryResolver);

    this._zoneSubscription = ngZone.onStable.subscribe(() => {
      if (this._windowRef) {
        positionElements(
          this._elementRef.nativeElement, this._windowRef.location.nativeElement, this.placement,
          this.container === 'body');
      }
    });

    if (this.showError) {
      this.open('display error');
    }

    if (!this.placement) {
      this.placement = 'top'
    }
    if (!this.container) {
      this.container = 'body'
    }

    this._exceService.messageEvent.subscribe(msg => {
      this._elementRef = msg.element;
      this.open(msg.message);
    })
  }

  ngOnChanges(changes: SimpleChanges): void {
    const change = 'bob' // changes['forbiddenName'];
    if (change) {
      const val: string | RegExp = /bob/ // change.currentValue;
      const re = val instanceof RegExp ? val : new RegExp(val, 'i');
      this.valFn = forbiddenNameValidator(re);
    } else {
      this.valFn = Validators.nullValidator;
    }
  }

  // the validate function will fire for every key press
  validate(control: AbstractControl): { [key: string]: any } {
    var returnVal = this.valFn(control);

    if (!this.exceError.valid || returnVal != null) { // error
      let errorMessageString = ``;
      if ('required' in this.exceError.errors) {
        errorMessageString = this.genarateErrorMessage(errorMessageString, this.reqValidationMesge);
      }
      if ('pattern' in this.exceError.errors) {
        if (this.validationMesge) { // if the custom validation message is requried
          errorMessageString = this.genarateErrorMessage(errorMessageString, this.validationMesge);

        } else if (this.validationType) {  // if the validation type is specified then go with it
          if (this.validationType === `email`) {
            errorMessageString = this.genarateErrorMessage(errorMessageString, this.patternMessages.email)
          } else if (this.validationType === `postalcode`) {
            errorMessageString = this.genarateErrorMessage(errorMessageString, this.patternMessages.postalcode)
          }

        } else { // go with the genral error message for validation if the validation type is not specified
          errorMessageString = this.genarateErrorMessage(errorMessageString, this.generalMessages.patternStr);
        }
      }
      if ('minlength' in this.exceError.errors) {
        // tslint:disable-next-line:max-line-length
        errorMessageString = this.genarateErrorMessage(errorMessageString, this.generalMessages.minLengthStr) + this.exceError.errors.minlength.requiredLength; // minlength.actualLength; // to get the current inputs in the field
      }
      if ('maxlength' in this.exceError.errors) {
        // tslint:disable-next-line:max-line-length
        errorMessageString = this.genarateErrorMessage(errorMessageString, this.generalMessages.maxLengthStr) + this.exceError.errors.maxlength.requiredLength;  // maxlength.actualLength; // to get the current inputs in the field

      }
      if ('email' in this.exceError.errors) {
        errorMessageString = this.genarateErrorMessage(errorMessageString, this.patternMessages.email)
      }
      if ('BankAccount' in this.exceError.errors) {
        errorMessageString = this.genarateErrorMessage(errorMessageString, this.patternMessages.bankAccount);
      }
      if ('creditCollector' in this.exceError.errors) {

        errorMessageString = this.genarateErrorMessage(errorMessageString, this.patternMessages.creditCollector);
      }
      if ('duplicateName' in this.exceError.errors) {
          errorMessageString = this.genarateErrorMessage(errorMessageString, this.patternMessages.duplicateName);
       }

      const domPassed = new DOMParser().parseFromString(errorMessageString, 'text/xml');
      this.open(errorMessageString);
    } else {
      this.close();
    }
    return returnVal;
  }

  genarateErrorMessage(errorMessage: string, msgToAdd: string): string {
    let errorMessageToReturn = errorMessage;
    if (errorMessage === ``) {  // if the custom validation message is requried
      errorMessageToReturn = msgToAdd;
    } else {
      errorMessageToReturn += `\n` + ` & ` + msgToAdd;
    }
    return errorMessageToReturn;
  }

  /**
   * Opens an element’s popover. This is considered a “manual” triggering of the popover.
   * The context is an optional value to be injected into the popover template when it is created.
   */
  open(errorMessage: string, context?: any) {
    if (!this._windowRef) {
      this._windowRef = this._popupService.open(errorMessage, context);
      this._windowRef.instance.placement = this.placement;
      this._windowRef.instance.title = this.popoverTitle;
      this._windowRef.instance.id = this._ngbPopoverWindowId;

      this._renderer.setAttribute(this._elementRef.nativeElement, 'aria-describedby', this._ngbPopoverWindowId);

      if (this.container === 'body') {
        window.document.querySelector(this.container).appendChild(this._windowRef.location.nativeElement);
      }

      // we need to manually invoke change detection since events registered via
      // Renderer::listen() are not picked up by change detection with the OnPush strategy
      this._windowRef.changeDetectorRef.markForCheck();
      this.shown.emit();
    }
  }

  /**
   * Closes an element’s popover. This is considered a “manual” triggering of the popover.
   */
  close(): void {
    if (this._windowRef) {
      this._renderer.removeAttribute(this._elementRef.nativeElement, 'aria-describedby');
      this._popupService.close();
      this._windowRef = null;
      this.hidden.emit();
    }
  }

  /**
   * Toggles an element’s popover. This is considered a “manual” triggering of the popover.
   */
  toggle(): void {
    if (this._windowRef) {
      this.close();
    }
    // } else {
    //   this.open('test open');
    // }
  }

  /**
   * Returns whether or not the popover is currently being shown
   */
  isOpen(): boolean { return this._windowRef != null; }

  ngOnInit() {
    this._unregisterListenersFn = listenToTriggers(
      this._renderer, this._elementRef.nativeElement, this.triggers, this.open.bind(this), this.close.bind(this),
      this.toggle.bind(this));

    this._translate.use(this._exceToolbarService.getSelectedLanguage().id); // set the langusge for the ng2 translater
    // to get the messages translated
    this.getAllTranslated();


    this._exceToolbarService.langUpdated.subscribe(
      (lang) => {
        this._translate.use(this._exceToolbarService.getSelectedLanguage().id); // set the langusge for the ng2 translater
        // to get the messages translated
        this.getAllTranslated();
      }
    );

  }
  testval: string;

  getStringTranslated(id: string) {
    this._translate.get(id).subscribe((val: string) => {
      switch (id) {
        // for genaral messages
        case 'generalMessages.maxLengthStr':
          this.generalMessages.maxLengthStr = val;
          break;
        case 'generalMessages.minLengthStr':
          this.generalMessages.minLengthStr = val;
          break;
        case 'generalMessages.patternStr':
          this.generalMessages.patternStr = val;
          break;
        case 'generalMessages.requiredStr':
          this.generalMessages.requiredStr = val;
          break;

        // for pattern messages
        case 'patternMessages.email':
          this.patternMessages.email = val;
          break;
        case 'patternMessages.postalcode':
          this.patternMessages.postalcode = val;
          break;

        case 'patternMessages.bankAccount':
          this.patternMessages.bankAccount = val;
          break;
        case 'patternMessages.creditCollector':
          this.patternMessages.creditCollector = val;
          break;
        case 'patternMessages.duplicateName':
        this.patternMessages.duplicateName = val;
        break;

        default:
          break;
      }
    })
  }

  getAllTranslated() {
    // set of genaral messages
    this.getStringTranslated('generalMessages.maxLengthStr');
    this.getStringTranslated('generalMessages.minLengthStr');
    this.getStringTranslated('generalMessages.patternStr');
    this.getStringTranslated('generalMessages.requiredStr');

    // // set of specific messages
    this.getStringTranslated('patternMessages.email');
    this.getStringTranslated('patternMessages.postalcode');
    this.getStringTranslated('patternMessages.bankAccount');
    this.getStringTranslated('patternMessages.creditCollector');
    this.getStringTranslated('patternMessages.duplicateName');
  }

  ngOnDestroy() {
    this.close();
    this._unregisterListenersFn();
    this._zoneSubscription.unsubscribe();
  }
}
