import { Injectable, EventEmitter, OnInit } from '@angular/core';
import { Observable, throwError } from 'rxjs';
// import 'rxjs/add/operator/map'
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { Router } from '@angular/router';
import { ConfigService } from '@ngx-config/core';
import { PreloaderService } from './preloader.service';
import { mergeMap, filter, scan, tap, catchError, map } from 'rxjs/operators';
import { HttpClient, HttpParams, HttpHeaders, HttpResponse } from '@angular/common/http';
import { OperationService } from 'app/modules/operations/services/operation-service';
import { _ } from 'ag-grid-community';
@Injectable()
export class CommonHttpService {
  private currentUser: any;
  constructor(
    private router: Router,
    private http: HttpClient,
    private config: ConfigService
  ) { }

  /**
 * Make http  call with logging
 * @param url url of the service call
 * @param options the http options
 * @returns {Observable<any>}
 * @private
 */
  public makeHttpCall(url, options): Observable<any> {
  //  PreloaderService.showPreLoader();
    if (options.params) {
    } else {
    }

    if (options.auth) {
      if (!options.headers) {
        if (!this.CurrentUser) {
          this.router.navigate(['logout']);
        } else {
          const token = this.CurrentUser.token;

          const headers = new HttpHeaders()
            .set('Access-Control-Allow-Origin', '*')
            .set('Content-Type', 'application/json')
            .set('Authorization', 'bearer ' + token)
            .set('UserName', this.CurrentUser.username)

          // const headers = new HttpHeaders();

          // // headers.append('X-Frame-Options', 'deny');
          // // headers.append('Access-Control-Allow-Headers',
          // //   'Origin, X-Requested-With, Content-Type, Accept, Authorization, If-Modified-Since, Cache-Control, Pragma');
          // // headers.append('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
          // headers.append('Access-Control-Allow-Origin', '*');
          // headers.append('Content-Type', 'application/json');
          // headers.append('Authorization', 'bearer ' + token);
          // headers.append('UserName', this.CurrentUser.username);
          options.headers = headers;
        }
      }
    }

    return this.http.request(options.method, url, options).pipe(
      catchError(this.handleError.bind(this))
    );

    // return this.http.request(url, options).pipe(
    //   map(this.extractData),
    //   catchError(this.handleError.bind(this)),
    //   tap( // Log the result or error
    //     data => _ // PreloaderService.hidePreLoader()
    //     ,
    //     error => _ // PreloaderService.hidePreLoader()
    //   )
    // );

    // this.http.request(url, options).map(this.extractData).catch(this.handleError.bind(this)).subscribe(result => {
    // })

    /**
     * @depricated as of update to Angular 6
     */
    // return this.http.request(url, options).map(this.extractData).catch(this.handleError.bind(this)).pipe(
    //   tap( // Log the result or error
    //     data => _ //PreloaderService.hidePreLoader()
    //     ,
    //     error => _ //PreloaderService.hidePreLoader()
    //   )
    // );
  }

  public GetSessionValue(url: any, options: any, gymCode: any, userLogged: boolean): Observable<any> {
    if (options.auth) {
      if (!options.headers) {
        const token = this.CurrentUser ? this.CurrentUser.token : '';

        const headers = new HttpHeaders()
          .set('Access-Control-Allow-Origin', '*')
          .set('Content-Type', 'application/json')
          .set('Authorization', 'bearer ' + token)
          .set('UserName', userLogged ? this.CurrentUser.username : gymCode);
        options.headers = headers;

        // const headers = new HttpHeaders();

        // headers.append('Access-Control-Allow-Origin', '*');
        // headers.append('Content-Type', 'application/json');
        // headers.append('Authorization', 'bearer ' + token);
        // headers.append('UserName', userLogged ? this.CurrentUser.username : gymCode);
        // options.headers = headers;
      }
    }
    return this.http.request(options.method, url, options)/* .pipe(
      map(result => {
        return this.extractData
      }),
      catchError(this.handleError.bind(this))
    ) */
    /**
     * @depricated as of update to Angular 6
     */
    // return this.http.request(url, options).map(this.extractData).catch(this.handleError.bind(this)).pipe(
    // );
  }

  public downLoadFile(url): Observable<Blob> {
    const token = this.CurrentUser.token
    const headers = new HttpHeaders()
      .set('Access-Control-Allow-Origin', '*')
      .set('Content-Type', 'application/json')
      .set('Authorization', 'bearer ' + token)
      .set('UserName', this.CurrentUser.username);

    const options = {
      headers: headers,
      responseType: 'blob' as 'blob'
    }

    return this.http.get(url, options).pipe(
      map((response) => response)
    )
    /**
     * @depricated as of update to Angular 6
     */
    // return this.http.get(url, options).map((response) => response);

    // const options = new RequestOptions
    //   ({
    //     responseType: ResponseContentType.Blob,
    //     headers: headers
    //   });
    // return this.http.get(url, options).map((response) => response.blob());
  }

  public uploadFile(url: any, fileData): Observable<any> {
    const token = this.CurrentUser.token
    const headers = new HttpHeaders()
      .append('Access-Control-Allow-Origin', '*')
      .append('Authorization', 'bearer ' + token)
      .append('UserName', this.CurrentUser.username);

    const options = {
      headers: headers
    }

    return this.http.post(url, fileData, options)
      .pipe(catchError(error => Observable.throw(error)))

    // const options = new RequestOptions();
    // options.headers = headers;
    // return this.http.post(url, fileData, options)
    //   .map(res => res.json())
    //   .catch(error => Observable.throw(error))
  }

  private extractData(res: HttpResponse<any>) {
    const body = res;
    return body || {};
  }

  private handleError(error: ErrorEvent | any): Observable<never> {
    let errMsg: string;
    if (error instanceof ErrorEvent) {
      console.log('Error occured in handleError@common-http: ', error)
      const body = error.error || '';
      const err = body.error.message || body;
      errMsg = `${error.error.status} - ${error.error.statusText || ''} ${err}`;
      if (error.error.status === 401 || error.error.status === 403) {
        this.router.navigate(['logout']);
      }
    } else {
      errMsg = error.error.message ? error.error.message : error.toString();
    }
    return throwError(errMsg);
  }

  // private handleError(error: Response | any) {
  //   let errMsg: string;
  //   if (error instanceof Response) {
  //     const body = error.json() || '';
  //     const err = body.error || JSON.stringify(body);
  //     errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
  //     if (error.status === 401 || error.status === 403) {
  //       this.router.navigate(['logout']);
  //       // return Observable.of([]);
  //     }
  //   } else {
  //     errMsg = error.message ? error.message : error.toString();
  //   }
  //   return Observable.throw(errMsg);
  // }

  set CurrentUser(user) {
    this.currentUser = user;
  }

  get CurrentUser() {
    if (this.config.getSettings('ENV') === 'DEV') {
      return JSON.parse(Cookie.get('currentUser'));
    } else {
      return this.currentUser;
    }
  }


}


