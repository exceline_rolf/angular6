import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';
import { HttpUrlEncodingCodec } from '@angular/common/http';

@Injectable()
export class CommonEncryptionService {
    private key = CryptoJS.enc.Utf8.parse('7061737323313233');
    private iv = CryptoJS.enc.Utf8.parse('7061737323313233');

    encryptValue(value: string) {
        const encrypted = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(value), this.key,
            {
                keySize: 128 / 8,
                iv: this.iv,
                mode: CryptoJS.mode.CBC,
                padding: CryptoJS.pad.Pkcs7
            });
        return encodeURIComponent(encrypted.toString());
    }

    decryptValue(value: string) {
        const decrypted = CryptoJS.AES.decrypt(value, this.key, {
            keySize: 128 / 8,
            iv: this.iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });
        return decrypted.toString();
    }
}


