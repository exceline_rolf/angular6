import { Injectable } from '@angular/core';
import { Subscription } from 'rxjs';

/**
 * Service to handle all all subscriptions
 */

@Injectable()
export class SubHandlerService {
  private subscriptions: Subscription [];
  private subscribedFrom: String [];

  constructor() {
    this.subscriptions = [];
    this.subscribedFrom = [];
  }

  /**
   * @param sub The actual subscription
   * @param subFrom Name of component the subscription originated
  */
  push(sub: Subscription, subFrom: String) {
    this.subscriptions.push(sub);
    this.subscribedFrom.push(subFrom);
  }

  unsubscribe(sub: Subscription) {
    if (this.subscriptions.includes(sub)) {
      const subIndex = this.subscriptions.indexOf(sub, 0);

      const tounsub = this.subscriptions.find(x => x === sub);

      if (subIndex > -1) {
        this.subscriptions.splice(subIndex, 1);
        this.subscribedFrom.splice(subIndex, 1);
        tounsub.unsubscribe();
      }
    }
  }

  unsubscribeAll() {
    for (let i = 0; i < this.subscriptions.length; i++) {
      this.subscriptions[i].unsubscribe();
      this.subscribedFrom.pop();
    }
  }

  /**
   * @returns The number of active subscriptions
   */
  numberOfActiveSubscriptions(): number {
    let numberOfActiveSubscriptions = 0;

    this.subscriptions.forEach((sub) => {
      if (!sub.closed) {
        numberOfActiveSubscriptions++;
      }
    });
    return numberOfActiveSubscriptions;
  }

  /**
   * @returns A map of subscriptions and where they originated from
   */
  getActiveSubscriptions(): Map<Subscription, String> {
    const totalSubscriptions = new Map();

    const tempSub: Subscription[] = [];
    const tempSubFrom: String[] = [];

    for (let i = 0; i < this.subscriptions.length; i++) {
      if (!this.subscriptions[i].closed) {
        tempSubFrom.push(this.subscribedFrom[i]);
        tempSub.push(this.subscriptions[i])
      }
    }

    for (let i = 0; i < tempSub.length; i++) {
      totalSubscriptions.set(tempSub[i], tempSubFrom[i]);
    }

    return totalSubscriptions;
  }

  printTotalSubscriptions() {
    console.log('=======Total Subs=======');
    const totalSubscriptions = new Map();

    const tempNoSub: number[] = [];
    const tempSubFrom: String[] = [];
    let index = 0;

    for (let i = 0; i < this.subscribedFrom.length; i++) {
      if (!tempSubFrom.includes(this.subscribedFrom[i])) {
        tempSubFrom.push(this.subscribedFrom[i]);
        tempNoSub.push(1);

        index = tempNoSub.length - 1;
      } else {
        tempNoSub[index] += 1;
      }
    }

    for (let i = 0; i < tempNoSub.length; i++) {
      totalSubscriptions.set(tempSubFrom[i], tempNoSub[i]);
    }

    Array.from(totalSubscriptions.entries()).forEach(entry => {
      console.log(entry[0] + ': ' + entry[1]);
    });
    console.log('========================');
  }

  printTotalActiveSubscriptions() {
    console.log('=======Total Active Subs=======')
    const totalSubscriptions = new Map();

    const tempNoSub: number[] = [];
    const tempSubFrom: String[] = [];
    let index = 0;

    for (let i = 0; i < this.subscribedFrom.length; i++) {
      if (!this.subscriptions[i].closed) {
        if (!tempSubFrom.includes(this.subscribedFrom[i])) {
          tempSubFrom.push(this.subscribedFrom[i]);
          tempNoSub.push(1);

          index = tempNoSub.length - 1;
        } else {
          tempNoSub[index] += 1;
        }
      }
    }

    for (let i = 0; i < tempNoSub.length; i++) {
      totalSubscriptions.set(tempSubFrom[i], tempNoSub[i]);
    }

    Array.from(totalSubscriptions.entries()).forEach(entry => {
      console.log(entry[0] + ': ' + entry[1]);
    });
    console.log('==============================');
  }

  /**
   *
   * @param unsubFrom Name of component where all subscriptions should be unsubscribed
   */
  unsubAllActiveFromCurrentComponent(unsubFrom: String) {
    const activeSubs = this.getActiveSubscriptions();

    Array.from(activeSubs.entries()).forEach(entry => {
      if (entry[1] === unsubFrom) {
        entry[0].unsubscribe();
      }
    })
  }
}
