import { ExceToolbarService } from '../../common/exce-toolbar/exce-toolbar.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn, AbstractControl } from '@angular/forms';
import { UsErrorService } from 'app/shared/directives/us-error/us-error.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-new-form',
  templateUrl: './new-form.component.html',
  styleUrls: ['./new-form.component.scss']
})
export class NewFormComponent implements OnInit, OnDestroy {
  curserPosition: any;
  private destroy$ = new Subject<void>();
  public myForm: FormGroup;
  myFormSubmited = false;

  formErrors = {
    'firstName': '',
    'lastName': '',
    'email': '',
    'intelNumber': ''
  };

  validationMessages = {
    'firstName': {
      'required': 'Name is required.',
      'minlength': 'Name must be at least 4 characters long.',
      'maxlength': 'Name cannot be more than 24 characters long.',
      'forbiddenName': 'Forbidden name'
    },
    'lastName': {

    },
    'email': {
      'required': 'email is required.',
      'email': 'email must valid.'
    },
    'intelNumber': {
      'required': 'phone number is required.',
      'phoneNumber': 'invalid phone number'
    }
  };
  private isPhoneNumberValid = true;
  phoneNumberDetail: string;
  private defaultPrice = 0;
  private locale: string;

  constructor(
    private fb: FormBuilder,
  ) {

  }

  public ngOnInit(): void {
    this.myForm = this.fb.group({
      firstName: [null, [Validators.required, Validators.minLength(4), Validators.maxLength(24), forbiddenNameValidator(/bob/i)]],
      lastName: [null],
      email: [null, [Validators.required, Validators.email]],
      intelNumber: [null, null, [this.phoneNumberValidator.bind(this)]],
      extension: [null],
      amount: [null]
    });
    this.myForm.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(_ => {
      UsErrorService.onValueChanged(this.myForm, this.formErrors, this.validationMessages)
    });
  }

  submitForm() {
    this.myFormSubmited = true;
    if (this.myForm.valid) {
    } else {
      UsErrorService.validateAllFormFields(this.myForm, this.formErrors, this.validationMessages);
    }
  }

  reset() {
    this.myFormSubmited = false;
    this.myForm.reset();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  fullValueChange(event) {
    this.phoneNumberDetail = event;
    this.myForm.patchValue({
      intelNumber: event.value,
      extension: event.extension
    });
    this.isPhoneNumberValid = event.value === '' || event.valid;
  }

  phoneNumberValidator(control: AbstractControl) {
    return new Promise(resolve => {
      setTimeout(() => {
        if (!this.isPhoneNumberValid) {
          resolve({
            'phoneNumber': true
          })
        } else {
          resolve(null);
        }
      }, 10);
    })
  }


}

function forbiddenNameValidator(nameRe: RegExp): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } => {
    const forbidden = nameRe.test(control.value);
    return forbidden ? { 'forbiddenName': { value: control.value } } : null;
  };
}




