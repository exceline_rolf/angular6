import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColTableSampleComponent } from './col-table-sample.component';

describe('ColTableSampleComponent', () => {
  let component: ColTableSampleComponent;
  let fixture: ComponentFixture<ColTableSampleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColTableSampleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColTableSampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
