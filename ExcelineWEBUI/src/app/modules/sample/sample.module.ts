import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from '../../shared/shared.module';

import { SampleRoutingModule } from './sample-routing.module';
import { sampleRoutingComponents } from './sample-routing.module';
import { ExceCategoryService } from '../../shared/components/exce-category/exce-category.service';
import { ExceAddmemberService } from 'app/shared/components/exce-addmember/exce-addmember.service';
import { ExcePostalcodeService } from 'app/shared/components/exce-postalcode/exce-postalcode.service';
import { TreeTableComponent } from './tree-table/tree-table.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { CollapsibleTableComponent } from './collapsible-table/collapsible-table.component';
import { ColTableSampleComponent } from './col-table-sample/col-table-sample.component';
import { CropImageComponent } from './crop-image/crop-image.component';

export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/modules/sample/', '.json');
}

@NgModule({
    imports: [
        CommonModule,
        NgbModule,

        SharedModule,
        SampleRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule.forChild({
            loader: {
              provide: TranslateLoader,
              useFactory: (createTranslateLoader),
              deps: [HttpClient]
            },
            isolate: false
          })

    ],
    declarations: [
        sampleRoutingComponents,
        TreeTableComponent,
        CollapsibleTableComponent,
        ColTableSampleComponent,
        CropImageComponent,
    ],
    providers: [
        ExceCategoryService,
        ExceAddmemberService,
        ExcePostalcodeService
    ]
})
export class SampleModule { }
