import { Component, ViewChild, TemplateRef, Output, EventEmitter, OnInit, Input } from '@angular/core';
import * as $ from 'jquery';
import { NgbPopover } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-calandar-test',
    templateUrl: './calandar-test.component.html',
    styleUrls: ['./calandar-test.component.scss']
})
export class CalandarTestComponent implements OnInit {
    editable: boolean;
    businessHours: boolean;
    defaultDate: string;
    resourceLabelText: string;
    resourceAreaWidth: string;
    buttonText: { prev: string; next: string; };
    buttonIcons: { prev: string; next: string; };
    theame: boolean;
    events: any[];

    @ViewChild('p') public popover: NgbPopover;

    resources: any[];

    header: any;

    event: MyEvent;

    Height: number = 630;

    dialogVisible: boolean = false;

    idGen: number = 100;

    viewType: string;

    Currentevent: string;

    views: any;

    allDayText = 'AAAAAA day';

    constructor() {

    }
    ngOnInit() {
        // this.viewType = 'agendaDay';
        this.viewType = 'agendaWeek';
        this.theame = false;
        this.header = {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek'
          };
          this.defaultDate = '2018-04-03';
          this.businessHours = true; // display business hours
          this.editable = true;
          this.events = [
            {
              title: 'Business Lunch',
              start: '2018-04-02T13:00:00',
              constraint: 'availableForMeeting'
            },
            {
              title: 'Business Lunch',
              start: '2018-04-03T13:00:00',
              end: '2018-04-03T13:45:00',
              constraint: 'availableForMeeting'
            },
            // {
            //   title: 'Meeting',
            //   start: '2018-04-13T10:00:00',
            //   end: '2018-04-13T11:00:00',
            //   constraint: 'availableForMeeting', // defined below
            //   color: '#257e4a'
            // },
            {
              title: 'Conference',
              start: '2018-04-18',
              end: '2018-04-20'
            },
            {
              title: 'Party',
              start: '2018-04-29T20:00:00'
            },

            // areas where "Meeting" must be dropped
            {
              id: 'availableForMeeting',
              start: '2018-04-05T10:00:00',
              end: '2018-04-05T16:00:00',
              rendering: 'background',
              backgroundColor: '#ef512d'
            },
            {
              id: 'availableForMeeting',
              start: '2018-04-03T10:00:00',
              end: '2018-04-03T16:00:00',
              rendering: 'background',
              backgroundColor: '#ef512d'
            },

            // red areas where no events can be dropped
            {
              start: '2018-04-24',
              end: '2018-04-28',
              overlap: false,
              rendering: 'background',
              color: '#ff9f89'
            },
            {
              start: '2018-04-06',
              end: '2018-04-08',
              overlap: false,
              rendering: 'background',
              color: '#ff9f89'
            }
          ]
        // this.header = {
        //     right: 'prev, today, next',
        //     center: 'title',
        //     left: 'timelineDay,agendaDay,agendaTwoDay,agendaWeek,month,list,listDay,listWeek'
        // };
        // this.buttonIcons = {
        //     prev: 'icon-back',
        //     next: 'icon-next',
        // };
        // this.buttonText = {
        //     prev: '<i class="glyphicon glyphicon-triangle-left"></i>',
        //     next: '<i class="glyphicon glyphicon-triangle-right"></i>'
        // }

        // this.allDayText = '';
        // this.resourceAreaWidth = '25%';
        // this.resourceLabelText = 'Rooms';

        // this.views = {
        //     agendaTwoDay: {
        //         type: 'agenda',
        //         duration: { days: 1 },

        //         // views that are more than a day will NOT do this behavior by default
        //         // so, we need to explicitly enable it
        //         groupByResource: false,

        //         //// uncomment this line to group by day FIRST with resources underneath
        //         groupByDateAndResource: false
        //     },
        //     listDay: { buttonText: 'list day' },
        //     listWeek: { buttonText: 'list week' }
        // };

        // this.resources = [
        //     { id: 'a', title: 'Room A' },
        //     { id: 'b', title: 'Room B', eventColor: '#ADFF2F' },
        //     { id: 'c', title: 'Room C', eventColor: '#FF69B4' },
        //     { id: 'd', title: 'Room D', eventColor: '#FFD700' }
        // ],

        //     this.events = [
        //         { id: '1', resourceId: 'a', start: '2017-05-06', end: '2017-05-08', title: 'event 1' },
        //         { id: '2', resourceId: 'a', start: '2017-05-07T06:00:00', end: '2017-05-07T09:00:00', title: 'event 2', color: '#257e4a' },
        //         { id: '3', resourceId: 'b', start: '2017-05-07T12:00:00', end: '2017-05-08T06:00:00', title: 'event 3', color: '#ff9f89' },
        //         { id: '4', resourceId: 'c', start: '2017-05-07T07:30:00', end: '2017-05-07T09:30:00', title: 'event 4', color: '#FF69B4' },
        //         { id: '5', resourceId: 'd', start: '2017-05-07T10:00:00', end: '2017-05-07T15:00:00', title: 'event 5', color: '#FFD700' }
        //     ];


    }

    onSelectResource(event) {
    }

    handleDayClick(event) {
        $('.contextMenu').remove();
        // this.event = new MyEvent();
        // this.event.start = event.date.format();
        // this.dialogVisible = true;
    }

    onRightClick(event) {
        let contextmenu = `
            <div  class="contextMenu dropdown clearfix">
                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu" style="display:block;position:static;margin-bottom:5px;">
                    <li data-action = "first"><a tabindex="-1" role="button" >First Action</a></li>
                </ul>
            </div>`;

        $('.contextMenu').remove();
        let $contextMenu = $(contextmenu).appendTo('body');
        $($contextMenu).css('top', event.jsEvent.pageY);
        $($contextMenu).css('left', event.jsEvent.pageX);
        $($contextMenu).css('position', 'absolute');
        $($contextMenu).css('overflow', 'hidden');
        $($contextMenu).css('z-index', 10000);

        $($contextMenu).on("click", "li", function (ele) {
            switch ($(this).attr("data-action")) {
                // A case for each action. Your actions here
                case "first": alert("first"); break;
                case "second": alert("second"); break;
                case "third": alert("third"); break;
            }
            $($contextMenu).hide();
        });


    }

    onEventRightclick(event) {
    }



    handleEventClick(e) {
        $('.contextMenu').remove();

        this.event = new MyEvent();
        this.event.title = e.calEvent.title;

        let start = e.calEvent.start;
        let end = e.calEvent.end;
        if (e.view.name === 'month' || e.view.name === 'basicWeek' || e.view.name === 'basicDay') {
            start.stripTime();
        }

        if (end) {
            end.stripTime();
            this.event.end = end.format();
        }

        this.event.id = e.calEvent.id;
        this.event.start = start.format();
        this.event.allDay = e.calEvent.allDay;
        this.dialogVisible = true;
    }


    onEventDragStart(event) {
    }

    handleOnDrop(event) {
    }

    dragStop(e) {
        // alert("r u srue?");
    }

    dayRender(date, cell) {

    }

    eventRender(event, element) {
$(element).data('eventDetail', event);

        let contextmenu = `
            <div  class="contextMenu dropdown clearfix">
                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu" style="display:block;position:static;margin-bottom:5px;">
                    <li data-action = "first"><a tabindex="-1" role="button" >First Action</a></li>
                    <li data-action = "second"> <a tabindex="-1" >Second Action</a></li>
                    <li data-action = "third"><a tabindex="-1">Third Action</a></li>
                </ul>
            </div>`;

        $(element).contextmenu(function (e) {
            $('.contextMenu').remove();
            let $contextMenu = $(contextmenu).appendTo('body');
            $($contextMenu).css('top', e.pageY);
            $($contextMenu).css('left', e.pageX);
            $($contextMenu).css('position', 'absolute');
            $($contextMenu).css('overflow', 'hidden');
            $($contextMenu).css('z-index', 10000);

            $($contextMenu).on("click", "li", function (ele) {
                switch ($(this).attr("data-action")) {
                    // A case for each action. Your actions here
                    case "first": alert("first"); break;
                    case "second": alert("second"); break;
                    case "third": alert("third"); break;
                }
                $($contextMenu).hide();
            });
            e.preventDefault();
        });
    }

    actionClicked() {
    }



    mouseOver(event) {
        let tooltip = '<div class="tooltipevent" style="overflow:hidden;border-radius:5px;width:150px;height:150px;background:#ccc;position:absolute;z-index:10001;">' +
            event.calEvent.start._d + '</br>' +
            event.calEvent.title + '</div>';
        let $tooltip = $(tooltip).appendTo('body');

        $(event.jsEvent.currentTarget).mouseover(function (e) {
            $(event.jsEvent.currentTarget).css('z-index', 10000);
            $tooltip.fadeIn('500');
            $tooltip.fadeTo('10', 1.9);
        }).mousemove(function (e) {
            $tooltip.css('top', e.pageY + 10);
            $tooltip.css('left', e.pageX + 20);
        });
    }

    mouseOut(event) {
        $(event.jsEvent.currentTarget).css('z-index', 8);
        $('.tooltipevent').remove();
    }


}

export class MyEvent {
    id: number;
    title: string;
    start: string;
    end: string;
    allDay: boolean = true;
}
