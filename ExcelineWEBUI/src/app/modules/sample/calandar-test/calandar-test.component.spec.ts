import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalandarTestComponent } from './calandar-test.component';

describe('CalandarTestComponent', () => {
  let component: CalandarTestComponent;
  let fixture: ComponentFixture<CalandarTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalandarTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalandarTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
