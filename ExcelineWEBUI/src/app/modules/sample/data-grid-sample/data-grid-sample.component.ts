import { Component, OnInit, DoCheck, AfterViewChecked, AfterViewInit, OnDestroy } from '@angular/core';
import { ExceToolbarService } from '../../common/exce-toolbar/exce-toolbar.service';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-data-grid-sample',
  templateUrl: './data-grid-sample.component.html',
  styleUrls: ['./data-grid-sample.component.scss']
})
export class DataGridSampleComponent implements OnInit, DoCheck, AfterViewChecked, AfterViewInit, OnDestroy {
  private destroy$ = new Subject<void>();
  title = 'Common Data Grid';
  supportedLanguages: any[];
  selectedLanguage: string;
  translatedHeaders: string[]; // collects the translated headers for the child component
  viewingHeaders: string[];
  filterHeaders:string[];
  translatedFilters:string[];

  deleteRequired:boolean; // to show whether the delete button is required

  /*****************************************************************************************************************************************
    * 25/05/2017  UNICORN-SOLUTIONS
    * this metadata array will bear the headers that need to appear in the data grid
    * all the object fileds need to be of the same name used here (HeaderName,showInFilter,dataType,isEditable,canSort,orderNo,inputType)
    * only the above mentioned metadata array object properties are taken into concern up to this date
    * @HeaderName : the name of the header need to appear
    * @showInFilter : hava a filter of this HeaderName
    * @dataType : define the type of data included under this header
    * @isEditable : define whether the data need to have the capability to edit data ob double click
    * @canSort : tell whether the data need to have the ability to get sorted on clicking this header
    * @orderNo : gives the priority order of the headers need to arrange. the headers will be shown with lowest number to left
    * @inputType : to support for the data input on double click
  *****************************************************************************************************************************************/

  tableHeaderMetaData: Object[] = [
    {
      HeaderName: 'InvoiceId',
      showInFilter: true,
      dataType: 'number',
      isEditable: false,
      canSort: true,
      orderNo: 1,
      inputType: 'number'
    },
    {
      HeaderName: 'InvoiceBearer',
      showInFilter: true,
      dataType: 'string',
      isEditable: true,
      canSort: true,
      orderNo: 2,
      inputType: 'text'
    },
    {
      HeaderName: 'InvoiceAmount',
      showInFilter: true,
      dataType: 'currency',
      isEditable: true,
      canSort: true,
      orderNo: 5,
      inputType: 'number'
    },
    {
      HeaderName: 'InvoiceValidity',
      showInFilter: false,
      dataType: 'boolean',
      isEditable: false,
      canSort: false,
      orderNo: 4,
      inputType: 'text'
    },
    {
      HeaderName: 'Date',
      showInFilter: true,
      dataType: 'date',
      isEditable: true,
      canSort: true,
      orderNo: 3,
      inputType: 'date'
    },
    {
      HeaderName: 'Edit',
      showInFilter: false,
      dataType: 'button',
      isEditable: false,
      canSort: false,
      orderNo: 6,
      columnType: 'button'
    }
  ];


  /*****************************************************************************************************************************************
    * 02/06/2017  UNICORN-SOLUTIONS
    * this data array will bear all the data that need to appear in the data grid
    * the object fields need not to have the following specific names
    * the filed with ID need to have 'ID' name in it and ID at the end of property filed <<<<<<< THIS IS COMPULSARY
  *****************************************************************************************************************************************/
  dataArray: Object[] = [
    {
      InvoiceId: 1,
      InvoiceBearer: 'Bearer One',
      InvoiceAmount: 2875789.95, // parseFloat('12312.00').toFixed(2),
      InvoiceCreater: 'Creater One',
      InvoicePlace: 'Place One',
      InvoiceValidity: 'valid',
      Date: new Date(1627534667393) // .toLocaleDateString('ko-KR')
    },
    {
      InvoiceId: 2,
      InvoiceBearer: 'Bearer Two',
      InvoiceAmount: 1637496.09,
      InvoiceCreater: 'Creater Two',
      InvoicePlace: 'Place Two',
      InvoiceValidity: 'valid',
      Date: new Date(1572897926200)
    },
    {
      InvoiceId: 3,
      InvoiceBearer: 'Bearer Three',
      InvoiceAmount: 3913432.87,
      InvoiceCreater: 'Creater Three',
      InvoicePlace: 'Place Three',
      InvoiceValidity: 'valid',
      Date: new Date(1628070052197)
    },
    {
      InvoiceId: 4,
      InvoiceBearer: 'Bearer Four',
      InvoiceAmount: 2397610.58,
      InvoiceCreater: 'Creater Four',
      InvoicePlace: 'Place Four',
      InvoiceValidity: 'valid',
      Date: new Date(1628025769799)
    },
    {
      InvoiceId: 5,
      InvoiceBearer: 'Bearer Five',
      InvoiceAmount: 290421.71,
      InvoiceCreater: 'Creater Five',
      InvoicePlace: 'Place Five',
      InvoiceValidity: 'valid',
      Date: new Date(1670528279347)
    },
    {
      InvoiceId: 6,
      InvoiceBearer: 'Bearer Six',
      InvoiceAmount: 2510926.66, // parseFloat('12312.00').toFixed(2),
      InvoiceCreater: 'Creater Six',
      InvoicePlace: 'Place Six',
      InvoiceValidity: 'valid',
      Date: new Date(1545910853430) // .toLocaleDateString('ko-KR')
    },
    {
      InvoiceId: 7,
      InvoiceBearer: 'Bearer Seven',
      InvoiceAmount: 2987170.91,
      InvoiceCreater: 'Creater Seven',
      InvoicePlace: 'Place Seven',
      InvoiceValidity: 'valid',
      Date: new Date(1571913790517)
    },
    {
      InvoiceId: 8,
      InvoiceBearer: 'Bearer Eight',
      InvoiceAmount: 4019130.44,
      InvoiceCreater: 'Creater Eight',
      InvoicePlace: 'Place Eight',
      InvoiceValidity: 'valid',
      Date: new Date(1645750122319)
    },
    {
      InvoiceId: 9,
      InvoiceBearer: 'Bearer Nine',
      InvoiceAmount: 540895.27,
      InvoiceCreater: 'Creater Nine',
      InvoicePlace: 'Place Nine',
      InvoiceValidity: 'valid',
      Date: new Date(1637346926801)
    },
    {
      InvoiceId: 10,
      InvoiceBearer: 'Bearer Ten',
      InvoiceAmount: 2183330.00,
      InvoiceCreater: 'Creater Ten',
      InvoicePlace: 'Place Ten',
      InvoiceValidity: 'valid',
      Date: new Date(1556380243946)
    },
    {
      InvoiceId: 11,
      InvoiceBearer: 'Bearer Eleven',
      InvoiceAmount: 5057755.94, // parseFloat('12312.00').toFixed(2),
      InvoiceCreater: 'Creater Eleven',
      InvoicePlace: 'Place Eleven',
      InvoiceValidity: 'valid',
      Date: new Date(1551150708117) // .toLocaleDateString('ko-KR')
    },
    {
      InvoiceId: 12,
      InvoiceBearer: 'Bearer Twelve',
      InvoiceAmount: 2342829.44,
      InvoiceCreater: 'Creater Twelve',
      InvoicePlace: 'Place Twelve',
      InvoiceValidity: 'valid',
      Date: new Date(1553949275808)
    },
    {
      InvoiceId: 13,
      InvoiceBearer: 'Bearer Thirteen',
      InvoiceAmount: 4015740.73,
      InvoiceCreater: 'Creater Thirteen',
      InvoicePlace: 'Place Thirteen',
      InvoiceValidity: 'valid',
      Date: new Date(1633612916142)
    },
    {
      InvoiceId: 14,
      InvoiceBearer: 'Bearer Fourteen',
      InvoiceAmount: 488001.64,
      InvoiceCreater: 'Creater Fourteen',
      InvoicePlace: 'Place Fourteen',
      InvoiceValidity: 'valid',
      Date: new Date(1659234048723)
    },
    {
      InvoiceId: 15,
      InvoiceBearer: 'Bearer Fifteen',
      InvoiceAmount: 6698848.97,
      InvoiceCreater: 'Creater Fifteen',
      InvoicePlace: 'Place Fifteen',
      InvoiceValidity: 'valid',
      Date: new Date(1617586586228)
    },
    {
      InvoiceId: 16,
      InvoiceBearer: 'Bearer Sixteen',
      InvoiceAmount: 2424780.45, // parseFloat('12312.00').toFixed(2),
      InvoiceCreater: 'Creater Sixteen',
      InvoicePlace: 'Place Sixteen',
      InvoiceValidity: 'valid',
      Date: new Date(1565068877721) // .toLocaleDateString('ko-KR')
    },
    {
      InvoiceId: 17,
      InvoiceBearer: 'Bearer Seventeen',
      InvoiceAmount: 2450745.50,
      InvoiceCreater: 'Creater Seventeen',
      InvoicePlace: 'Place Seventeen',
      InvoiceValidity: 'valid',
      Date: new Date(1622561448859)
    },
    {
      InvoiceId: 18,
      InvoiceBearer: 'Bearer Eighteen',
      InvoiceAmount: 6384207.36,
      InvoiceCreater: 'Creater Eighteen',
      InvoicePlace: 'Place Eighteen',
      InvoiceValidity: 'valid',
      Date: new Date(1607579063118)
    },
    {
      InvoiceId: 19,
      InvoiceBearer: 'Bearer Nineteen',
      InvoiceAmount: 6768322.28,
      InvoiceCreater: 'Creater Nineteen',
      InvoicePlace: 'Place Nineteen',
      InvoiceValidity: 'valid',
      Date: new Date(1648200818409)
    },
    {
      InvoiceId: 20,
      InvoiceBearer: 'Bearer Twenty',
      InvoiceAmount: 5478969.24,
      InvoiceCreater: 'Creater Twenty',
      InvoicePlace: 'Place Twenty',
      InvoiceValidity: 'valid',
      Date: new Date(1610946142252)
    },
    {
      InvoiceId: 21,
      InvoiceBearer: 'Bearer Twentyone',
      InvoiceAmount: 104728.49, // parseFloat('12312.00').toFixed(2),
      InvoiceCreater: 'Creater Twentyone',
      InvoicePlace: 'Place Twentyone',
      InvoiceValidity: 'valid',
      Date: new Date(1629696685368) // .toLocaleDateString('ko-KR')
    },
    {
      InvoiceId: 22,
      InvoiceBearer: 'Bearer Twentytwo',
      InvoiceAmount: 6319407.12,
      InvoiceCreater: 'Creater Twentytwo',
      InvoicePlace: 'Place Twentytwo',
      InvoiceValidity: 'valid',
      Date: new Date(1634435387195)
    },
    {
      InvoiceId: 23,
      InvoiceBearer: 'Bearer Twentythree',
      InvoiceAmount:1934025.34,
      InvoiceCreater: 'Creater Twentythree',
      InvoicePlace: 'Place Twentythree',
      InvoiceValidity: 'valid',
      Date: new Date(1677624913873)
    },
    {
      InvoiceId: 24,
      InvoiceBearer: 'Bearer Twentyfour',
      InvoiceAmount: 5932284.35,
      InvoiceCreater: 'Creater Twentyfour',
      InvoicePlace: 'Place Twentyfour',
      InvoiceValidity: 'valid',
      Date: new Date(1579761616644)
    },
    {
      InvoiceId: 25,
      InvoiceBearer: 'Bearer Twentyfive',
      InvoiceAmount: 12312.00,
      InvoiceCreater: 'Creater Twentyfive',
      InvoicePlace: 'Place Twentyfive',
      InvoiceValidity: 'valid',
      Date: new Date(1559323052121)
    },
    {
      InvoiceId: 26,
      InvoiceBearer: 'Bearer Twentysix',
      InvoiceAmount: 12332.00, // parseFloat('12312.00').toFixed(2),
      InvoiceCreater: 'Creater Twentysix',
      InvoicePlace: 'Place Twentysix',
      InvoiceValidity: 'valid',
      Date: new Date(1552114666140) // .toLocaleDateString('ko-KR')
    },
    {
      InvoiceId: 27,
      InvoiceBearer: 'Bearer Twentyseven',
      InvoiceAmount: 12312.23,
      InvoiceCreater: 'Creater Twentyseven',
      InvoicePlace: 'Place Twentyseven',
      InvoiceValidity: 'valid',
      Date: new Date(1616943439659)
    },
    {
      InvoiceId: 28,
      InvoiceBearer: 'Bearer Twentyeight',
      InvoiceAmount: 12312.54,
      InvoiceCreater: 'Creater Twentyeight',
      InvoicePlace: 'Place Twentyeight',
      InvoiceValidity: 'valid',
      Date: new Date(1553393069675)
    },
    {
      InvoiceId: 29,
      InvoiceBearer: 'Bearer Twentynine',
      InvoiceAmount: 12312.98,
      InvoiceCreater: 'Creater Twentynine',
      InvoicePlace: 'Place Twentynine',
      InvoiceValidity: 'valid',
      Date: new Date(1672833478962)
    },
    {
      InvoiceId: 30,
      InvoiceBearer: 'Bearer Thirty',
      InvoiceAmount: 2023528.80,
      InvoiceCreater: 'Creater Thirty',
      InvoicePlace: 'Place Thirty',
      InvoiceValidity: 'valid',
      Date: new Date(1638843783556)
    },
  ];

  constructor(
    private _exceToolbarService: ExceToolbarService,
    private _translateService: TranslateService
  ) {}
  ngOnInit() {

    this.selectedLanguage = this._exceToolbarService.getSelectedLanguage().id// this._exceToolbarService.getSelectedLanguage().id; // notify the data grid about the language selected
  }

  getHeadersTranslated(id:string){
    this._translateService.get(id).pipe(takeUntil(this.destroy$)).subscribe((val:string)=>{
      this.translatedHeaders.push(val);
    })
  }

  getFiltersTranslated(id:string){
    this._translateService.get(id).pipe(takeUntil(this.destroy$)).subscribe((value:string)=>{
      this.translatedFilters.push(value);
    })
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  getHeadersFilersTranslated() {
    this.translatedHeaders = new Array<string>();
    this.translatedFilters = new Array<string>();

    // get headers translated
    if (this.viewingHeaders !== undefined) {
      for (var index = 0; index < this.viewingHeaders.length; index++) {
        this.getHeadersTranslated(this.viewingHeaders[index]);
        // termpHeaders.push(this._translate.instant(this.viewingHeaders[index]));
      }
    }

    if(this.filterHeaders !== undefined){
      for (var index2 = 0; index2 < this.filterHeaders.length; index2++) {
        this.getFiltersTranslated(this.filterHeaders[index2]);
        // tempFilters.push(this._translate.instant(this.filterHeaders[index2]));
      }
    }

  }

  ngDoCheck() {
    if (this._exceToolbarService.getSelectedLanguage().id === 'en') {
      this.selectedLanguage = 'en-US'// this._exceToolbarService.getSelectedLanguage().id; // notify the data grid about the
    } else if (this._exceToolbarService.getSelectedLanguage().id === 'no') {
      this.selectedLanguage = 'nb';
    }
    this._translateService.use(this._exceToolbarService.getSelectedLanguage().id); // set the langusge for the ng2 translater
    // to get the messages translated
    this.getHeadersFilersTranslated();

    // listing to any changes in the language translation
    this._exceToolbarService.langUpdated.pipe(takeUntil(this.destroy$)).subscribe(
      (lang) => {
        if(this._exceToolbarService.getSelectedLanguage().id === 'en'){
          this.selectedLanguage ='en-US'// this._exceToolbarService.getSelectedLanguage().id; // notify the data grid about the
        }else if (this._exceToolbarService.getSelectedLanguage().id === 'no'){
          this.selectedLanguage = 'nb';
        }
        this._translateService.use(this._exceToolbarService.getSelectedLanguage().id); // set the langusge for the ng2 translater
        // to get the messages translated
        this.getHeadersFilersTranslated();
      }
    );
  }

  ngAfterViewInit() {

  }
  ngAfterViewChecked() {
    // can not load the tranlated headers as child inputs are checked here
    // this.getHeadersTranslated();
  }

  getLang() {
    return this._translateService.currentLang;
  }

  showMessage(message: any) {
  }

  storeHeaders(headers: string[]) {
    this.viewingHeaders = headers;
  }

  storeFilters(filters: string[]) {
    this.filterHeaders = filters;
  }

  uponUpdation(dataObj: any) {
  }

  testing(anythig: any) {
  }

}


