import { Component, OnInit } from '@angular/core';
import { TreeNode } from 'app/shared/components/us-tree-table/tree-node';


@Component({
  selector: 'app-tree-table',
  templateUrl: './tree-table.component.html',
  styleUrls: ['./tree-table.component.scss']
})
export class TreeTableComponent implements OnInit {
  private data2: any = []
  nodes: TreeNode[] = [];
  files: TreeNode[] =
    [
      {
        'data': {
          'name': 'Documents'

        },
        'children': [
          {
            'data': {
              'name': 'Work',
              'size': '55kb',
              'type': 'Folder'
            },
            'children': [
              {
                'data': {
                  'name': 'Expenses.doc',
                  'size': '30kb',
                  'type': 'Document'
                }
              },
              {
                'data': {
                  'name': 'Resume.doc',
                  'size': '25kb',
                  'type': 'Resume'
                }
              }
            ]
          },
          {
            'data': {
              'name': 'Home',
              'size': '20kb',
              'type': 'Folder'
            },
            'children': [
              {
                'data': {
                  'name': 'Invoices',
                  'size': '20kb',
                  'type': 'Text'
                }
              }
            ]
          }
        ]
      },
      {
        'data': {
          'name': 'Pictures'
        },
        'children': [
          {
            'data': {
              'name': 'barcelona.jpg',
              'size': '90kb',
              'type': 'Picture'
            },
          },
          {
            'data': {
              'name': 'primeui.png',
              'size': '30kb',
              'type': 'Picture'
            }
          },
          {
            'data': {
              'name': 'optimus.jpg',
              'size': '30kb',
              'type': 'Picture'
            }
          }
        ]
      }
    ]

  constructor() { }

  ngOnInit() {
    this.data2 = [{ 'Id': '1', 'name': 'xxx', 'age': '22' },
    { 'Id': '1', 'name': 'yyy', 'age': '15' },
    { 'Id': '5', 'name': 'zzz', 'age': '59' }];
  }

  nodeSelect(event) {
  }

  nodeUnselect(event) {
  }

  nodeExpand(event) {
    if (event.node) {
    }
  }

  viewNode(node: TreeNode) {
  }

  deleteNode(node: TreeNode) {
  }

}
