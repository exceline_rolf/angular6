


import { Component, OnInit, OnDestroy } from '@angular/core';
import { UsbModal } from '../../../shared/components/us-modal/us-modal';
import { DataTableResource } from '../../../shared/components/us-data-table/tools/data-table-resource';
// import { AdminService } from '.../services/admin.service';
import { ReportingService } from '../reporting-service'
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ExceMessageService } from '../../../shared/components/exce-message/exce-message.service';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common/src/pipes';
import { DataTableFilterPipe } from '../../../shared/components/us-data-table/tools/data-table-filter-pipe';
import { TranslateService } from '@ngx-translate/core';
import { TreeNode } from 'app/shared/components/us-tree-table/tree-node';
import * as _ from 'lodash';
import { ExceLoginService } from '../../../modules/login/exce-login/exce-login.service';
import { HttpClient } from '@angular/common/http';
import * as moment from 'moment';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { ConfigService } from '@ngx-config/core';
import { ExceBreadcrumbService } from '../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import * as CryptoJS from 'crypto-js';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
// import { UsErrorService } from 'app/shared/directives/us-error/us-error.service';

@Component({
  selector: 'app-reporting-home',
  templateUrl: './reporting-home.component.html',
  styleUrls: ['./reporting-home.component.scss']
})
export class ReportingHomeComponent implements OnInit, OnDestroy {

  private reports: any;
  private items = [];
  private repDeleteModel;
  private reportModalRef?: any;
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  files: TreeNode[] = [];
  filteredReports
  currentReport
  currentUser
  reportModal
  reportUrl
  reportServer
  deletingItem
  FixRport = true
  parameters: any = {
  };
  selection = 'HOME'
  parentRoute = '/reporting/reporting-home/'
  currentFileEvent
  ReportCategoryList
  reportServiceUrl
  reportCategories
  uploadForm
  userRoles
  modelReference
  formSubmited = false
  isReUpload = false
  gymCode = ''
  branchId = 1


  private destroy$ = new Subject<void>();
  private message: string;
  private type: string;
  private isShowInfo: boolean;

  constructor(
    private modalService: UsbModal,
    private reportingService: ReportingService,
    // private fb: FormBuilder,
    private router: Router,
    private translateService: TranslateService,
    private exceMessageService: ExceMessageService,
    private exceLoginService: ExceLoginService,
    private config: ConfigService,
    private http: HttpClient,
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'COMMON.ReportsC',
      url: '/reporting/reporting-home'
    });

    this.reportServiceUrl = this.config.getSettings('EXCE_API.REPORTING');
    this.exceMessageService.yesCalledHandle.pipe(
      takeUntil(this.destroy$)
      ).subscribe((value) => {
      if (value.id === 'DELETE_REPORT') {
        this.deleteReport();
      }
    });

    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      (branch) => {
        this.branchId = branch.BranchId;

      }
    );
    this.currentUser = this.exceLoginService.CurrentUser
  }

  ngOnInit() {
    if (this.config.getSettings('ENV') === 'DEV') {
      this.gymCode = this.config.getSettings('GYMCODE')
    } else {
      this.gymCode = this.exceLoginService.CompanyCode;
    }
    this.getReports();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    if (this.modelReference) {
      this.modelReference.close();
    }
  }


  getReports() {
    this.files = [];
    this.reportingService.ViewAllReports().pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
      if (result) {
        this.reports = result.Data;
        this.items = result.Data;
        // tslint:disable-next-line:prefer-const
        this.prepareDataForTreeTable(this.reports);
      }
    }, null, null);
  }

  prepareDataForTreeTable(reportItems) {
    const sortedArr = (reportItems.reduce(function (res, current) {
      res[current.Category] = res[current.Category] || [];
      res[current.Category].push(current);
      return res;
    }, {}));

    // tslint:disable-next-line:prefer-const
    let files = []
    this.files = []
    // tslint:disable-next-line:prefer-const
    // tslint:disable-next-line:forin
    for (const key in sortedArr) {
      const children = []
      const constArr = sortedArr[key]
      constArr.forEach(element => {
        children.push({ 'data': element })
      });
      this.files.push({
        'data': { 'Category': key },
        'children': children
      });
    }
  }


  filterInput(catValue: string, nameValue: string, desValue: string, branchValue: any, isEdit: any) {
    this.filteredReports = JSON.parse(JSON.stringify(this.reports))
    if (isEdit && isEdit !== 'ALL') {
      this.filteredReports = this.filterpipe.transform('EditOrFixed', 'SELECT', this.filteredReports, isEdit);
    }
    if (nameValue && nameValue !== '') {
      this.filteredReports = this.filterpipe.transform('Name', 'NAMEFILTER', this.filteredReports, nameValue);
    }
    if (desValue && desValue !== '') {
      this.filteredReports = this.filterpipe.transform('Description', 'NAMEFILTER', this.filteredReports, desValue);
    }
    if (catValue && catValue !== '') {
      this.filteredReports = this.filterpipe.transform('Category', 'NAMEFILTER', this.filteredReports, catValue);
    }
    this.prepareDataForTreeTable(this.filteredReports)
  }

  getReportData(item, modal) {
    this.reportModal = modal
    this.reportingService.GetReportById(item.Id).pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
      if (result.Data) {
        this.currentReport = result.Data
        this.HandleViewReportCommand()
      }
    })
  }

  // reUpload(item,content){
  // this.uploadForm.patchValue(item)
  // this.modelReference = this.modalService.open(content, { width: '1000' });
  // }

  NewReport() {
    const routeUrl = '/reporting/add-new-report'
    this.router.navigate([routeUrl]);
  }

  closeUploadModal() {
    this.modelReference.close();
    // this.currentFileEvent = {}
  }

  openUplloadModel(content, isReupload?, item?) {
    if (isReupload) {
      this.isReUpload = true
      this.currentReport = item;
      this.modelReference = this.modalService.open(content, { width: '1000' });
    } else {
      this.isReUpload = false
      this.modelReference = this.modalService.open(content, { width: '1000' });
    }
  }

  HandleViewReportCommand() {
    this.reportingService.GetExcelineReportServerPath().pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
      if (result.Data) {
        const path = result.Data;
        this.GetExcelineReportServerPathCompleted(path)
      } else {
        const path = '';
        this.GetExcelineReportServerPathCompleted(path)
      }
    })
  }

  SetCookie() {
    //const tokenKey = '1qaz2wsx@';
    const now = new Date()
    const dateString = moment(now).add(45, 'seconds').format('YYYY-MM-DD HH:mm:ss').toString();
    const accessKey = 'Authenticated,' + dateString;
   var Key = CryptoJS.enc.Utf8.parse("PSVJQRk9QTEpNVU1DWUZCRVFGV1VVT0="); //secret key
   var IV = CryptoJS.enc.Utf8.parse("2314345645678765"); //16 digit
   const newCookie = CryptoJS.AES.encrypt(accessKey, Key, {keySize: 128 / 8,iv: IV, mode: CryptoJS.mode.CBC, padding:CryptoJS.pad.Pkcs7});
   //console.log(CryptoJS.enc.Base64.stringify(newCookie.ciphertext));
   Cookie.set("AuthenticatedUser", CryptoJS.enc.Base64.stringify(newCookie.ciphertext),null,"/");
  }

  GetExcelineReportServerPathCompleted(path) {
    this.SetCookie();
    let reportPath = '';
    let serverPath = '';
    serverPath = (path.trim() + '/USPReportViewer.aspx?ReportPath=').trim();
    if (this.currentReport && this.currentReport.FixReport) {
      reportPath = '/' + this.gymCode + '/' + this.currentReport.Category + '/' + this.currentReport.Name + '^' + this.branchId + '^FIXED^'
    } else {
      reportPath = '/' + this.gymCode + '/' + this.currentReport.Category + '/' + this.currentReport.Name + '^' + this.branchId + '^NOTFIXED^'
    }
    // window.open(serverPath + reportPath, '_blank');
    const newWindow = window.open(serverPath + reportPath);
  }

  viewOperation() {
    this.reportingService.GetAllEntitiesWithProperties().pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
      if (result.Data) {
        const latestEntityList = result.Data
        this.currentReport.EntityList = this.UpdateEntityProertyWidthValues(this.currentReport, latestEntityList)
        this.AddPrimaryColumnWithReportEntities(this.currentReport, latestEntityList);
      }
    })

    let _hasUserParameters = false;
    this.currentReport.WhereConditionList.forEach(whereCondition => {
      if (whereCondition.Value.toString().StartsWith('@')) {
        _hasUserParameters = true;
      }
    });
  }

  AddPrimaryColumnWithReportEntities(reportInfo, latestReportEntityList) {

    latestReportEntityList.forEach(rre => {
      for (let i = 0; i < reportInfo.EntityList.length; i++) {
        if (rre.DisplayName === reportInfo.EntityList[i].DisplayName) {
          reportInfo.EntityList[i].PrimaryColumn = rre.PrimaryColumn;
          reportInfo.EntityList[i].BasedOn = rre.BasedOn;
        }
      }
    });
  }

  UpdateEntityProertyWidthValues(reportInfo, latestReportEntityList) {
    const tempEntityList = []
    if (reportInfo && reportInfo.EntityList) {
      reportInfo.forEach(element => {
        const entity = this.UpdateEntityProperty(element, latestReportEntityList);
        tempEntityList.push(entity)
      });
    }
    return tempEntityList;
  }

  UpdateEntityProperty(rre, latestReportEntityList) {
    const tempReportEntity = rre;
    latestReportEntityList.array.forEach(entity => {
      if (entity.DisplayName.ToUpper().Trim() == rre.DisplayName.ToUpper().Trim()) {
        const old = rre.Properties
        const newL = entity.Properties
        const old_1 = []
        rre.Properties.forEach(element => {
          old_1.push(element.DataColumnName)
        });

        const new_1 = []
        entity.Properties.forEach(element => {
          new_1.push(element.DataColumnName)
        });

        const newlyAddedColumns = newL.filter(x => !(x.DataColumnName in old_1))
        const intersectedColumns = old.filter(x => x.DataColumnName in new_1)
        const columnUnion = _.union(intersectedColumns, newlyAddedColumns);

        tempReportEntity.Properties = columnUnion
      }
    });
    return tempReportEntity;
  }

  editQuery(item) {
    // this.selection = ''
    const routeUrl = '/reporting/reporting-home/edit-report-query/' + item.Id
    this.router.navigate([routeUrl]);
  }

  editReport(item) {
    // this.selection = ''
    const routeUrl = '/reporting/reporting-home/edit-report/' + item.Id
    this.router.navigate([routeUrl]);
  }

  loadOperations(item) {
    // this.selection = ''
    const routeUrl = '/reporting/reporting-home/report-operations/' + item.Id
    this.router.navigate([routeUrl]);
  }

  DeleteReportMessage(item, content) {
    this.modelReference = this.modalService.open(content, { width: '400' });
    this.deletingItem = item;
    // this.deletingItem = item;
    // let confirmTitle = '';
    // let confirmBody = '';
    // (this.translateService.get('REPORT.ReportDeleteMsg').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => confirmBody = tranlstedValue));
    // (this.translateService.get('COMMON.Confirm').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => confirmTitle = tranlstedValue));
    // // (this.translateService.get('ADMIN.MsgDeleteVatVersionConfirm').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => confirmBody = tranlstedValue));
    // this.repDeleteModel = this.exceMessageService.openMessageBox('CONFIRM',
    //   {
    //     messageTitle: confirmTitle,
    //     messageBody: confirmBody,
    //     msgBoxId: 'DELETE_REPORT',
    //     optionalData: item
    //   });
  }

  closeDel() {
    this.modelReference.close();
  }

  deleteReport() {
    this.reportingService.DeleteReport({
      reportIdToBeDeleted: this.deletingItem.Id,
      item: '/' + this.deletingItem.Category + '/' + this.deletingItem.Name
    }).pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
      this.closeDel()
      if (result.Data) {
        this.getReports();
        this.translateService.get('REPORT.ReportDeletedSuccessText').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => this.message = translatedValue);
        this.type = 'SUCCESS';
        this.isShowInfo = true;
        setTimeout(() => {
          this.isShowInfo = false;
        }, 4000);
      }
    }, error => {
      this.translateService.get('REPORT.ReportDeleteFailText').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => this.message = translatedValue);
      this.type = 'DANGER';
      this.isShowInfo = true;
      setTimeout(() => {
        this.isShowInfo = false;
      }, 4000);
    });
  }
}
