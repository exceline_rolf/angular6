

import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ReportingService } from '../../reporting-service'
import { headersToString } from 'selenium-webdriver/http';
import { element } from 'protractor';
import { TranslateService } from '@ngx-translate/core';
import { DataTableResource } from '../../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from '../../../../shared/components/us-data-table/tools/data-table-filter-pipe'
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
import { ExceMemberService } from '../../../../modules/membership/services/exce-member.service';
import { ExceLoginService } from '../../../login/exce-login/exce-login.service';
import { AdminService } from '../../../admin/services/admin.service';
import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-report-operations',
  templateUrl: './report-operations.component.html',
  styleUrls: ['./report-operations.component.scss']
})
export class ReportOperationsComponent implements OnInit, OnDestroy {
  reportId: number
  headers = []
  columns = []

  tableData
  tableDataJSON
  private itemResource: any = 0;
  items = [];
  itemCount = 0;
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  selectedList = []
  filteredData
  currentReport: any;
  modelReference
  emailEditorValue
  smsTemplates
  smsTemplateView
  smsText
  isReminder = false
  addReminderFee = false
  dueDate
  branchId
  username
  entityList
  isGetReply
  smsSettings: any;
  currentId: any;
  currentName: any;
  gridApi: any;
  columnDefs = [];
  considerGDPR = true;
  private destroy$ = new Subject<void>();

  constructor(private reportingService: ReportingService,
    private route: ActivatedRoute,
    private modalService: UsbModal,
    private memberService: ExceMemberService,
    private adminService: AdminService,
    private translateService: TranslateService,
    private exceMessageService: ExceMessageService,
    private exceLoginService: ExceLoginService, ) {
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.username = this.exceLoginService.CurrentUser.username
    this.exceLoginService.branchSelected.pipe(takeUntil(this.destroy$)).subscribe(branch => {
        this.branchId = branch.BranchId;
      });
  }

  ngOnInit() {
    this.reportId = this.route.snapshot.params['Id'];
    this.reportingService.GetReportById(this.reportId).pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result.Data) {
        this.currentReport = result.Data
        this.viewOperations();
        // this.reportingService.GetAllEntitiesWithProperties().pipe(takeUntil(this.destroy$)).subscribe(entities => {
        //   if (entities.Data) {
        //     this.currentReport.EntityList = entities.Data
        //     this.viewOperations();
        //   }
        // })

      }
    }, error => {
    });

    this.fetchData(this.branchId);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    if (this.modelReference) {
      this.modelReference.close();
    }
  }

  fetchData(branch) {
    this.adminService.getGymSettings('SMS', null, branch).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          this.smsSettings = result.Data;
          this.currentId = this.smsSettings[0].Id;
          this.currentName = this.smsSettings[0].SenderName;
        } else {}
      });
    }

  viewOperations() {
    let _hasUserParameters = false;
    // foreach (var whereCondition in _currentProcessingReport.WhereConditionList)
    // {
    //     if (whereCondition.Value.StartsWith("@"))
    //     {
    //         _hasUserParameters = true;
    //         break;
    //     }
    // }
    for (const whereCondition of this.currentReport.WhereConditionList) {
      if (whereCondition.Value.startsWith('@')) {
        _hasUserParameters = true;
        break;
      }
    }

    if (_hasUserParameters) {
      // ViewOperation qev = new ViewOperation(_currentProcessingReport);
      // qev.ViewModel.CurruntProccessingReport = _currentProcessingReport;
      // qev.ViewModel.ReportSuccessfullySavedEvent1 += new ReportSuccessfullySaved1(viewModel_ReportSuccessfullySavedEvent1);
      // qev.ViewModel.CloseReportSaveDialogEvent1 += new CloseEditedQueryReportSaveDialog(viewModel_CloseReportSaveDialogEvent1);
      // //_viewBox = new ReportingCommonLightBox();
      // //_viewBox.Title = "View Operation Result";
      // //_viewBox.Width = 1000;
      // //_viewBox.Height = 500;
      // //_viewBox.Content = qev;

      // if (_homeControl != null) {
      //   _homeControl.SubOperationOpened((IUSSOperation)qev);
      // }
    } else {
      // USContentPanel.ViewProgressBar(ReportResource.DataRetrievingProgress);
      // _progressTesting = new USGMSPrograssBar();
      // _progressTesting.Show();
      let QueryFixedPart = this.currentReport.QueryForReport.split('FROM')[0] + ' FROM';
      const QueryEditablePart = this.currentReport.QueryForReport.split('FROM')[1];
      for (let i = this.currentReport.EntityList.length - 1; i >= 0; i--) {
        if (QueryFixedPart.includes(this.currentReport.EntityList[i].PrimaryColumn)) {
          QueryFixedPart = this.GetPrimaryColumnToBegining(this.currentReport.EntityList[i].PrimaryColumn, QueryFixedPart);
        } else {
          QueryFixedPart = this.AddPrimaryColumnToBegining(this.currentReport.EntityList[i].PrimaryColumn, QueryFixedPart);
        }
      }
      this.reportingService.TestQuery({ query: QueryFixedPart + QueryEditablePart }).pipe(takeUntil(this.destroy$)).subscribe(testResults => {
        if (testResults.Data) {
          this.tableData = testResults.Data
          // this.tableData=this.tableData.filter(item)
          const headerTitles = this.tableData[0].split(';')
          headerTitles.forEach(header => {
            this.headers.push({ headerVal: header, text: '' })
          });

          headerTitles.forEach(head => {
            if (this.columnDefs.length === 0) {
              this.columnDefs.push(      {headerName: head, field: head, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
              filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
                sortAscending: '<i class="icon-sort-ascending"/>',
                sortDescending: '<i class="icon-sort-descending"/>'
              }, suppressMovable: true, autoHeight: true,
              headerCheckboxSelection: true,
              headerCheckboxSelectionFilteredOnly: true,
              checkboxSelection: true})
            } else {
              this.columnDefs.push({headerName: head, field: head, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
                filterParams: {newRowsAction: 'keep'},
                cellStyle: {'cursor': 'pointer'}, icons: {
                sortAscending: '<i class="icon-sort-ascending"/>',
                sortDescending: '<i class="icon-sort-descending"/>'
              }, suppressMovable: true, autoHeight: true})
            }
            // this.columns.push({ header: head })
          })
          this.gridApi.setColumnDefs(this.columnDefs)
          this.gridApi.sizeColumnsToFit();
          this.tableData.splice(0, 1)
          this.tableDataJSON = []
          let jsonObj_1
          this.tableData.forEach(row => {
            const rowData = row.split(';')
            jsonObj_1 = {}
            for (let i = 0; i < headerTitles.length; i++) {
              jsonObj_1[headerTitles[i]] = rowData[i]

            }
            this.tableDataJSON.push(jsonObj_1)
          });

          this.itemResource = new DataTableResource(this.tableDataJSON);
          this.itemResource.count().then(count => this.itemCount = count);
          this.items = this.tableDataJSON;
          this.itemCount = Number(this.tableDataJSON.length);

        }
      });
    }
  }

  onGridReady(params) {
    this.gridApi = params.api;
    // auto fits columns to fit screen
    params.api.sizeColumnsToFit();
    // hiding loading message on load
    this.gridApi.gridOptionsWrapper.gridOptions.api.hideOverlay();
  }

  AddPrimaryColumnToBegining(p, fixedPart): string {
    let finalString = '';
    const list1 = fixedPart.split('.');
    const list2 = list1[0].split(' ');

    // list2.insert(list2.length - 1, " ");
    // list2.insert(list2.length - 1, p);
    // list2.insert(list2.length - 1, " AS ");
    // list2.insert(list2.length - 1, p.Split('.')[1] + ",");
    // list2.insert(list2.length - 1, " ");

    list2.splice(list2.length - 1, 0, ' ');
    list2.splice(list2.length - 1, 0, p);
    list2.splice(list2.length - 1, 0, ' AS ');
    list2.splice(list2.length - 1, 0, p.split('.')[1] + ', ');
    list2.splice(list2.length - 1, 0, ' ');

    list2.forEach(item => {
      finalString = finalString + ' ' + item;
    });
    for (let i = 1; i < list1.length; i++) {
      finalString = finalString + '.' + list1[i];
    }
    return finalString;
  }

  // Array.prototype.insert = function (index, item) {
  //   this.splice(index, 0, item);
  // };

  GetPrimaryColumnToBegining(p, fixedPart): string {
    // tslint:disable-next-line:max-line-length
    // string aa = "SELECT DISTINCT TOP 1000 ci.Name AS CreditorName,ci.AddrNo AS CreditorAddrNo,ci.CreditorInkassoID AS CreditorInkassoID,ci.Addr1 AS CreditorAddr1,di.Name AS DebtorName,di.Addr1 AS DebtorAddr1 FROM";
    // string bb = "ci.CreditorInkassoID";
    let finalString = '';
    let addFromWord = false;
    let dd = '';
    let indexOfItem = -1;

    const list0 = fixedPart.split(',');

    for (const listItem of list0) {
      if (listItem.includes(p)) {
        indexOfItem = list0.indexOf(listItem);
        break;
      }
    }
    if (indexOfItem === (list0.length - 1)) {
      addFromWord = true;
    }
    if (indexOfItem === 0) {
      return fixedPart;
    } else {
      // list0.RemoveAt(indexOfItem);
      list0.splice(indexOfItem, 1);
      list0.forEach(item => {
        if (dd !== '') {
          dd = dd + ',' + item;
        } else {
          dd = item;
        }
      });
      // foreach (var item in list0)
      // {
      //     dd = dd + "," + item;
      // }
      // dd = dd.trim(',');
      dd.trim()

      const list1 = dd.split('.');
      const list2 = list1[0].split(' ');
      // list2.insert(list2.length - 1, " ");
      // list2.insert(list2.length - 1, p);
      // list2.insert(list2.length - 1, " AS ");
      // list2.insert(list2.length - 1, p.Split('.')[1] + ",");
      // list2.insert(list2.length - 1, " ");

      list2.splice(list2.length - 1, 0, ' ');
      list2.splice(list2.length - 1, 0, p);
      list2.splice(list2.length - 1, 0, ' AS ');
      list2.splice(list2.length - 1, 0, p.split('.')[1] + ',');
      list2.splice(list2.length - 1, 0, ' ');

      list2.forEach(item => {
        finalString = finalString + ' ' + item;
      });
      // foreach (var item in list2)
      // {
      //     finalString = finalString + " " + item;
      // }
      for (let i = 1; i < list1.length; i++) {
        finalString = finalString + '.' + list1[i];
      }

      if (!addFromWord) {
        return finalString;

      } else {
        return finalString + ' FROM ';
      }
    }
  }

  filterInput() {
    this.filteredData = this.tableDataJSON
    // if (branchValue && branchValue !== 'ALL') {
    //   this.filteredData = this.filterpipe.transform('BranchId', 'SELECT', this.filteredData, branchValue);
    // }
    this.headers.forEach(header => {
      if (header.text && header.text !== '') {
        this.filteredData = this.filterpipe.transform(header.headerVal, 'NAMEFILTER', this.filteredData, header.text);
      }
    });

    // if (val && val !== '') {
    //   this.filteredData = this.filterpipe.transform(prop, 'NAMEFILTER', this.filteredData, val)
    // }

    // if (catValue && catValue !== 'ALL') {
    //   this.filteredData = this.filterpipe.transform('TaskCategoryId', 'SELECT', this.filteredData, catValue);
    // }

    this.itemResource = new DataTableResource(this.filteredData);
    this.itemResource.count().then(count => this.itemCount = count);
    this.reloadItems(this.filteredData);
  }

  reloadItems(params): void {
    if (this.itemResource) {
      this.itemResource.query(params).then(items => this.items = items);
    }
  }

  // selectRow(event) {
  //   if (event.selected) {
  //     if (!(event.item in this.selectedList)) {
  //       this.selectedList.push(event.item)
  //     }
  //   } else {
  //     this.selectedList = this.selectedList.filter(item => item !== event.item)
  //   }
  // }

  openEmailModal(content) {
    this.considerGDPR = true;
    if (this.modelReference) {
      this.modelReference.close()
      this.modelReference = this.modalService.open(content, { width: '1000' });
    } else {
      this.modelReference = this.modalService.open(content, { width: '1000' });
    }
  }

  openprintModal(content) {
    if (this.modelReference) {
      this.modelReference.close()
      this.modelReference = this.modalService.open(content, { width: '300' });
    } else {
      this.modelReference = this.modalService.open(content, { width: '300' });
    }
  }

  openMobileSendSmsModal(content) {
    this.considerGDPR = true;
    if (this.currentName === '' || this.currentName === null || this.currentName.length === 0) {
      let messageTitle = '';
      let messageBody = '';
      this.translateService.get('ADMIN.ErrorGettingGymSmsSetting').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageTitle = tranlstedValue);
      this.translateService.get('ADMIN.SenderNameMissing').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageBody = tranlstedValue);
      this.exceMessageService.openMessageBox('ERROR',
      {
        messageTitle: messageTitle,
        messageBody: messageBody,
        msgBoxId: 'MSG_EMAIL'
      });
    } else {
    this.memberService.getSMSNotificationTemplateByEntity().pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          this.smsTemplates = result.Data;
          if (this.modelReference) {
            this.modelReference.close()
            this.modelReference = this.modalService.open(content, { width: '1000' });
          } else {
            this.modelReference = this.modalService.open(content, { width: '1000' });
          }
        }
      });
    }
  }

  setSMSText(templateName: string) {
    const selectedTemplate = this.smsTemplates.filter(x => x.Name === templateName);
    if (selectedTemplate && selectedTemplate.length > 0) {
      this.smsText = selectedTemplate[0].Text;
    }
  }

  sendEmailorSMS(subject, type) {
    let dueDate = ''
    if (this.isReminder) {
      dueDate = this.dateConverter(this.dueDate)
    }
    const primaryColumn = this.currentReport.EntityList[0].PrimaryColumn.split('.')[1]
    const basedOn = this.currentReport.EntityList[0].BasedOn
    const recipientList = []
    const primaryColumnName = this.columnDefs[0].field;
    this.selectedList = this.gridApi.getSelectedRows();
    this.selectedList.forEach(reciver => {
      recipientList.push(Number(reciver[primaryColumnName]))
    });

    const senderObj = {
      subject: subject,
      body: '',
      isSelected: true,
      outputType: 0,
      recipientList: recipientList,
      primaryColumn: primaryColumn,
      basedOn: basedOn,
      reminder: this.isReminder,
      addreminderFee: this.addReminderFee,
      branchID: this.branchId,
      ReminderDueDate: dueDate,
      considerationGDPR: this.considerGDPR

    }
    if (type === 'EMAIL') {
      senderObj.outputType = 0
      senderObj.body = this.emailEditorValue
    } else if (type === 'SMS') {
      senderObj.outputType = 1
      senderObj.body = this.smsText
    } else {
      senderObj.outputType = 2
    }
    this.reportingService.SendReportOutPut(senderObj).pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result.Data) {
        this.modelReference.close();
        this.emailEditorValue = ''
        this.isReminder = false
        this.addReminderFee = false
      }
    }, error => {
    })
  }

  dateConverter(dateObj: any) {
    let dateS = '';
    if (dateObj) {
      if (dateObj.formatted) {
        const splited = dateObj.formatted.split('/')
        if (splited.length > 1) {
          dateS = splited[2] + '-' + splited[1] + '-' + splited[0] + 'T00:00:00'
        }
      }
    }
    return dateS
  }
}
