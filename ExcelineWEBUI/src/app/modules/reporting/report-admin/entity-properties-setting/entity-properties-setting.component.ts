import { Component, OnInit } from '@angular/core';
import { ExceBreadcrumbService } from '../../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';

@Component({
  selector: 'app-entity-properties-setting',
  templateUrl: './entity-properties-setting.component.html',
  styleUrls: ['./entity-properties-setting.component.scss']
})
export class EntityPropertiesSettingComponent implements OnInit {

  constructor(
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'COMMON.SettingsC',
      url: '/reporting/entity-properties-setting'
    });
  }

  ngOnInit() {
  }

}
