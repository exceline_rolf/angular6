import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntityPropertiesSettingComponent } from './entity-properties-setting.component';

describe('EntityPropertiesSettingComponent', () => {
  let component: EntityPropertiesSettingComponent;
  let fixture: ComponentFixture<EntityPropertiesSettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EntityPropertiesSettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntityPropertiesSettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
