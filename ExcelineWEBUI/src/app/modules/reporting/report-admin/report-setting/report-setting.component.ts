import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ReportingService } from '../../reporting-service';
import { TranslateService } from '@ngx-translate/core';
import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { ExceBreadcrumbService } from '../../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-report-setting',
  templateUrl: './report-setting.component.html',
  styleUrls: ['./report-setting.component.scss']
})
export class ReportSettingComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  ReportSettingsForm: FormGroup;

  private message: string;
  private type: string;
  private isShowInfo: boolean;

  constructor(
    private _exceMessageService: ExceMessageService,
    private translateService: TranslateService,
    private fb: FormBuilder,
    private reportingService: ReportingService,
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'COMMON.AdministrationC',
      url: '/reporting/report-setting'
    });

    this.ReportSettingsForm = this.fb.group({
      'ReportServiceURL': [null],
      'FileUploadHandlerPath': [null],
      'UserName': [null],
      'RecordCount': [null],
      'Password': [null]
    });
  }

  ngOnInit() {
    this.getReportSettingDetails();
  }

  getReportSettingDetails() {
    this.reportingService.GetReportSetting().pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        result.Data.ReportSettingList.forEach(element => {
          if (element.Key == 'ReportServiceURL') {
            this.ReportSettingsForm.controls['ReportServiceURL'].setValue(element.KeyValue);
          } else if (element.Key == 'FileUploadHandlerPath') {
            this.ReportSettingsForm.controls['FileUploadHandlerPath'].setValue(element.KeyValue);
          } else if (element.Key == 'UserName') {
            this.ReportSettingsForm.controls['UserName'].setValue(element.KeyValue);
          } else if (element.Key == 'RecordCount') {
            this.ReportSettingsForm.controls['RecordCount'].setValue(element.KeyValue);
          } else if (element.Key == 'Password') {
            this.ReportSettingsForm.controls['Password'].setValue(element.KeyValue);
          }
        });
      } else {
        let messageTitle = '';
        let messageBody = '';
        this.translateService.get('Error').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageTitle = tranlstedValue);
        this.translateService.get('REPORT.SettingRetrieveFailedText').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageBody = tranlstedValue);
        this._exceMessageService.openMessageBox('ERROR', {
          messageTitle: messageTitle,
          messageBody: messageBody,
          msgBoxId: 'ERROR_GET_REPORT'
        });
      }
    });
  }

  saveReportSettingDetails(value) {
    const ReportSettingList = [];
    ReportSettingList.push(
      { Key: 'ReportServiceURL', KeyValue: value.ReportServiceURL },
      { Key: 'UserName', KeyValue: value.UserName },
      { Key: 'Password', KeyValue: value.Password },
      { Key: 'FileUploadHandlerPath', KeyValue: value.FileUploadHandlerPath },
      { Key: 'RecordCount', KeyValue: value.RecordCount },
    );

    const settingItem = {
      ReportSettingList: ReportSettingList
    }

    this.reportingService.SaveReportSetting(settingItem).pipe(takeUntil(this.destroy$)).subscribe(res => {
        this.translateService.get('REPORT.SettingSaveSuccessText').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => this.message = translatedValue);
        this.type = 'SUCCESS';
        this.isShowInfo = true;
        setTimeout(() => {
          this.isShowInfo = false;
        }, 4000);
    },
      error => {
        this.translateService.get('REPORT.SettingSaveFailedText').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => this.message = translatedValue);
        this.type = 'DANGER';
        this.isShowInfo = true;
        setTimeout(() => {
          this.isShowInfo = false;
        }, 4000);
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }


}
