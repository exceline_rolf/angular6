import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { ReportingRoutingModule } from './reporting-routing.module';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { Observable, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { ReportRoutingComponents } from './reporting-routing.module';
import { GymUsageReportSettingComponent } from './gym-usage-report-setting/gym-usage-report-setting.component';
import { ReportingHomeComponent } from './reporting-home/reporting-home.component';
import { ReportSettingComponent } from './report-admin/report-setting/report-setting.component';
import { EntityPropertiesSettingComponent } from './report-admin/entity-properties-setting/entity-properties-setting.component';
import { SharedModule } from '../../shared/shared.module';
import {ReportingService} from './reporting-service';
import { ReportingEditQueryComponent } from './reporting-home/reporting-edit-query/reporting-edit-query.component';
import { AddNewReportComponent } from './reporting-home/add-new-report/add-new-report.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ReportOperationsComponent } from './reporting-home/report-operations/report-operations.component';
// import { AngularSplitModule } from 'angular-split';
import { AngularSplitModule } from 'angular-split-ng6';
import { SaveReportViewComponent } from './reporting-home/save-report-view/save-report-view.component';
import { ReportingHomeTilesComponent } from './reporting-home-tiles/reporting-home-tiles.component';
import { ExceCommonModule } from '../common/exce-common.module';
import { CKEditorModule } from 'ngx-ckeditor';
import { ExceMemberService } from '../../modules/membership/services/exce-member.service';
import { AgGridModule } from 'ag-grid-angular';

export function translateLoader(http: HttpClient) {

  return new MultiTranslateHttpLoader(http, [
    { prefix: './assets/i18n/', suffix: '.json' }
    // { prefix: './assets/i18n/modules/reporting/', suffix: '.json' }
  ]);
}

export class MultiTranslateHttpLoader implements TranslateLoader {

  constructor(private http: HttpClient,
    public resources: { prefix: string, suffix: string }[] = [
      {
        prefix: '/assets/i18n/',
        suffix: '.json'
      }
    ]) { }

  /**
   * Gets the translations from the server
   * @param lang
   * @returns {any}
   */
  public getTranslation(lang: string): any {

    return forkJoin(this.resources.map(config => {
      return this.http.get(`${config.prefix}${lang}${config.suffix}`);
    })).pipe(
      map(response => {
        return response.reduce((a, b) => {
          return Object.assign(a, b);
        });
      })
    )
  }
}

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ExceCommonModule,
    ReportingRoutingModule,
    FormsModule,
    AngularSplitModule,
    AgGridModule.withComponents(null),
    CKEditorModule,
    ReactiveFormsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (translateLoader),
        deps: [HttpClient]
      }
    })
  ],
  declarations: [
    ReportRoutingComponents,
    GymUsageReportSettingComponent,
    ReportingHomeComponent,
    ReportSettingComponent,
    EntityPropertiesSettingComponent,
    ReportingEditQueryComponent,
    AddNewReportComponent,
    ReportOperationsComponent,
    SaveReportViewComponent,
    ReportingHomeTilesComponent
  ],
  providers:
  [
    ReportingService,
    ExceMemberService

  ]
})

export class ReportingModule { }
