import { Injectable } from '@angular/core';
import { CommonHttpService } from '../../shared/services/common-http.service';
import { ConfigService } from '@ngx-config/core';
import { Observable } from 'rxjs';
import { ExceLoginService } from '../../modules/login/exce-login/exce-login.service';

@Injectable()
export class ReportingService {
  categoryObj;
  private reportServiceUrl: string;
  private memberServiceUrl: string;
  private branchId: number;
  constructor(private commonHttpService: CommonHttpService, private config: ConfigService, private exceLoginService: ExceLoginService) {
    this.reportServiceUrl = this.config.getSettings('EXCE_API.REPORTING');
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.subscribe(
      (branch) => {
        this.branchId = branch.BranchId;
      }
    );
  }

  ViewAllReports(): Observable<any> {
    const url = this.reportServiceUrl + 'ViewAllReports'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetReportById(reportId): Observable<any> {
    const url = this.reportServiceUrl + 'GetReportById?reportId=' + reportId
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetAllEntitiesWithProperties(): Observable<any> {
    const url = this.reportServiceUrl + 'GetAllEntitiesWithProperties'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  TestQuery(query): Observable<any> {
    const url = this.reportServiceUrl + 'RapportQuery'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: query
    })
  }

  GetExcelineReportServerPath(): Observable<any> {
    const url = this.reportServiceUrl + 'GetExcelineReportServerPath'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }


  DeleteReport(delReport): Observable<any> {
    const url = this.reportServiceUrl + 'DeleteReport'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: delReport
    })
  }


  SaveReportConfiguration(saveReport): Observable<any> {
    const url = this.reportServiceUrl + 'SaveReportConfiguration'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: saveReport
    })
  }

  SaveEditedReportConfiguration(saveReport): Observable<any> {
    const url = this.reportServiceUrl + 'SaveEditedReportConfiguration'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: saveReport
    })
  }

  GetAllRoles(): Observable<any> {
    const url = this.reportServiceUrl + 'GetAllRoles'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetAllReportCategories(): Observable<any> {
    const url = this.reportServiceUrl + 'GetAllReportCategories'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  SaveAsReportConfiguration(saveReport): Observable<any> {
    const url = this.reportServiceUrl + 'SaveAsReportConfiguration'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: saveReport
    })
  }
  //   SaveWorkItem(workItem: any): Observable<any> {
  //     const url = this.reportServiceUrl + 'Settings/SaveWorkItem'
  //     return this.commonHttpService.makeHttpCall(url, {
  //       method: 'POST',
  //       auth: true,
  //       body: workItem
  //     })
  //   }

  GetReportSetting(): Observable<any> {
    const url = this.reportServiceUrl + 'GetReportSetting'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  SaveReportSetting(settingItem): Observable<any> {
    const url = this.reportServiceUrl + 'SaveReportSetting'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: settingItem
    })
  }

  SendReportOutPut(sendReport): Observable<any> {
    const url = this.reportServiceUrl + 'SendReportOutPut'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: sendReport
    })
  }

  SaveCategory(name): Observable<any> {
    const url = this.reportServiceUrl + 'SaveCategory?name=' + name
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      //body: name
    })
  }

  UpdateCategory(editCategory): Observable<any> {
    const url = this.reportServiceUrl + 'UpdateCategory'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: editCategory
    })
  }

  GetScheduleTemplateList(dataObj): Observable<any> {
    const url = this.reportServiceUrl + 'GetScheduleTemplateList'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: dataObj
    })
  }


}
