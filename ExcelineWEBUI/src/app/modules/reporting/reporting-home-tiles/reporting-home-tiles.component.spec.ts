import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportingHomeTilesComponent } from './reporting-home-tiles.component';

describe('ReportingHomeTilesComponent', () => {
  let component: ReportingHomeTilesComponent;
  let fixture: ComponentFixture<ReportingHomeTilesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportingHomeTilesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportingHomeTilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
