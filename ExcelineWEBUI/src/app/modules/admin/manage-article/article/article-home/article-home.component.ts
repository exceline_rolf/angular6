import { Component, OnInit, ViewChild, OnDestroy, Input, ElementRef } from '@angular/core';
import { UsbModal } from '../../../../../shared//components/us-modal/us-modal';
import { AdminService } from '../../../services/admin.service';
import { DataTableResource } from '../../../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from '../../../../../shared/components/us-data-table/tools/data-table-filter-pipe';
import { TranslateService } from '@ngx-translate/core';
import { ExceMessageService } from '../../../../../shared/components/exce-message/exce-message.service';
import { ExceLoginService } from '../../../../login/exce-login/exce-login.service';
import { PreloaderService } from '../../../../../shared/services/preloader.service';
import { ExceBreadcrumbService } from '../../../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { FormGroup } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-article-home',
  templateUrl: './article-home.component.html',
  styleUrls: ['./article-home.component.scss']
})
export class ArticleHomeComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private articleMode: string;
  private modelRefNewArticle: any
  private modalUpdateStock: any;
  private selectedArticle: any;
  private articleItems = [];
  private selectedCategory = { Id: -1, Code: 'ALL', Name: 'All' };
  private isObsolete = false;
  private branchId = 1;
  private itemResource?: DataTableResource<any>;
  private searchText: string;
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  private loginUseRoleId: number;
  AType: any;
  title: any;
  serviceCategoryList: any;
  itemCategoryList: any;
  articleCategoryList: any = [];
  activityCategoryList: any = [];
  articleList: any[];
  articleCategory = 'ALL';
  articleType = 'ALL';
  activityCategory = '-1';
  itemCount = 0;
  @ViewChild('autobox') public autocomplete;
  textValue: string;

  auotocompleteItems = [
    { id: 'ArticleNo', name: 'Article No :', isNumber: true },
    { id: 'Name', name: 'Name :', isNumber: false },
    { id: 'Barcode', name: 'Barcode :', isNumber: false }
  ];
  config: any = { 'placeholder': '', 'sourceField': ['searchText'] };

  itemArNo: any;
  addCategoryList: any;
  deductCategoryList: any;
  tempList: any;
  updateStockCat: any;
  updateStockPurchasePrice: any;
  quantityChange: any;
  updateAddVat = 0;
  updateSelectedItem: any;
  updateChoice = 'add';
  IsStockAdd = true;
  articleDefs: any;

  @ViewChild('upDateStockModal') upDateStockModal: ElementRef;
  @ViewChild('newArticle') newArticle: ElementRef;

  names = {
    Article: 'Artikkelnummer',
    ArticleName: 'Artikkelnavn',
    Speeddial: 'Hurtigtast',
    RevenueAccount: 'Inntekstkonto',
    Vat: 'Momskode',
    Category: 'Kategori',
    IsStock: 'Lagervare',
    PurchasedPrice: 'Innkjøpspris',
    Price: 'Pris',
  }

  colNamesNo = {
    Article: 'Artikkelnummer',
    ArticleName: 'Artikkelnavn',
    Speeddial: 'Hurtigtast',
    RevenueAccount: 'Inntekstkonto',
    Vat: 'Momskode',
    Category: 'Kategori',
    IsStock: 'Lagervare',
    PurchasedPrice: 'Innkjøpspris',
    Price: 'Pris',
  }

  colNamesEn = {
    Article: 'ArticleNo',
    ArticleName: 'Article name',
    Speeddial: 'Shortcut key',
    RevenueAccount: 'Revenue account',
    Vat: 'Vat code',
    Category: 'Category',
    IsStock: 'Stock',
    PurchasedPrice: 'Purchased price',
    Price: 'Price',
  }
  gridApi: any;



  constructor(
    private modalService: UsbModal,
    private adminService: AdminService,
    private translate: TranslateService,
    private exceMessageService: ExceMessageService,
    private loginservice: ExceLoginService,
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ADMIN.GoodsC',
      url: '/adminstrator/article-home'
    });
  }

  ngOnInit() {
    this.loginUseRoleId = this.loginservice.LoggedUserDetails.RoleId;
    this.autocomplete.setSearchValue(this.auotocompleteItems);
    this.adminService.getCategories('SERVICE').pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      resService => {
        this.serviceCategoryList = resService.Data;

        this.adminService.getCategories('ITEM').pipe(
          takeUntil(this.destroy$)
        ).subscribe(
          resItem => {
            this.itemCategoryList = resItem.Data;
            // this.serviceCategoryList.forEach(serviceCat => {
            //   this.articleCategoryList.push(serviceCat);
            // });
            // this.itemCategoryList.forEach(itemCat => {
            //   this.articleCategoryList.push(itemCat);
            // });
            this.articleCategoryList = this.serviceCategoryList.concat(this.itemCategoryList);
          }, err => {
            console.log(err);
          });

      }, err => {
        console.log(err);
      });

    this.adminService.getCategories('ACTIVITY').pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      res => {
        this.activityCategoryList = res.Data;
      }, err => {
        console.log(err);
      });
    this.searchArticles(this.articleType, '', this.selectedCategory, -1, false)
    this.exceMessageService.yesCalledHandle.pipe(
      takeUntil(this.destroy$)
    ).subscribe((value) => {
      if (value.id === 'DELETE_ARTICLE') {
        this.deleteArticleConfirm(value.optionalData);
      }
    });

    this.adminService.getCategories('ADDCATEGORY').pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      resAddCat => {
        if (resAddCat) {
          this.addCategoryList = resAddCat.Data
          this.tempList = this.addCategoryList;
          this.updateStockCat = this.tempList[0].Id
          this.adminService.getCategories('DEDUCTCATEGORY').pipe(
            takeUntil(this.destroy$)
          ).subscribe(
            resDeductCat => {
              if (resDeductCat) {
                this.deductCategoryList = resDeductCat.Data
              }
            }
          );
        }
      }
    );
    //   this.searchArticles();


    this.articleDefs = [
      {headerName: this.names.Article, field: 'ArticleNo', width: 70, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true},
      {headerName: this.names.ArticleName, field: 'Description', resizable: true, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true},
      {headerName: this.names.Speeddial, field: 'SortCutKey', width: 50, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true},
      {headerName: this.names.Vat, field: 'VatCode', width: 60, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true},
      {headerName: this.names.Category, field: 'Category', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true},
      {headerName: this.names.IsStock, field: 'IsStock', suppressMenu: true, width: 60, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'center'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        if (!params.data.StockStatus && params.data.ArticleType === 'ITEM') {
          return '<span><svg version="1.1" id="outOfStock" xmlns="http://www.w3.org/2000/svg"' +
          'xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 24 24" width="24px" height="24px" xml:space="preserve">' +
          '<path class="fill-gary" d="M12,1C5.9,1,1,5.9,1,12c0,6.1,4.9,11,11,11c6.1,0,11-4.9,11-11C23,5.9,18.1,1,12,1 M17.8,14.9c0.8,0.8,0.8,2.2,0,3' +
          'c-0.8,0.8-2.2,0.8-3,0l-2.8-2.8L9.2,18c-0.8,0.8-2.2,0.8-3,0c-0.8-0.8-0.8-2.2,0-3l2.9-2.9L6,9C5.2,8.2,5.2,6.9,6,6' +
          'c0.8-0.8,2.2-0.8,3,0l3.1,3.1L15,6.2c0.8-0.8,2.2-0.8,3,0c0.8,0.8,0.8,2.2,0,3L15,12.1L17.8,14.9z" /></svg>'
        } else if (!params.data.StockStatus && params.data.ArticleType === 'ITEMS') {
          return '<svg version="1.1" id="inStock" xmlns="http://www.w3.org/2000/svg"' +
          'xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 24 24" width="24px" height="24px" xml:space="preserve">' +
          '<path fill="#fff" d="M12,1C5.9,1,1,5.9,1,12s4.9,11,11,11s11-4.9,11-11S18.1,1,12,1" />' +
          '<path fill="#000" d="M9.4,18.2l-3.9-3.9c-0.6-0.6-0.6-1.5,0-2.1c0.6-0.6,1.5-0.6,2.1,0l1.8,1.8l7-7c0.6-0.6,1.5-0.6,2.1,0' +
          'c0.6,0.6,0.6,1.5,0,2.1L9.4,18.2z" /></svg>'
        } else {
          return;
        }
        }
      },
      {headerName: this.names.PurchasedPrice, field: 'PurchasedPrice', width: 50, suppressMenu: true, suppressSorting: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'right'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        if (params.value) {
          const val = Number(params.value)
          return val.toFixed(2);
        } else {
          return;
        }}
      },
      {headerName: this.names.Price, field: 'DefaultPrice', width: 50, suppressMenu: true, suppressSorting: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'right'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        if (params.value) {
          const val = Number(params.value)
          return val.toFixed(2);
        } else {
          return;
        }}
      },
      {headerName: '', field: 'IsBelongToBranch', suppressMenu: true, width: 40, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'center'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        if (params.data.IsBelongToBranch) {
          return '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"  y="0px" viewBox="0 0 24 24"' +
          'width="24px" height="24px">' +
          '<path class="fill-primary " d="M5.8,16.7h2.7c0.4,0,0.8,0.4,0.8,0.8c0,0.5-0.4,0.8-0.8,0.8H5.8c-0.4,0-0.8-0.4-0.8-0.8C5,17.1,5.3,16.7,5.8,16.7z' +
          'M5.8,13.3h4.8c0.4,0,0.8,0.4,0.8,0.8c0,0.5-0.4,0.8-0.8,0.8H5.8C5.3,15,5,14.7,5,14.2C5,13.7,5.3,13.3,5.8,13.3z M5.8,10h4.8' +
          'c0.4,0,0.8,0.4,0.8,0.8c0,0.5-0.4,0.8-0.8,0.8H5.8c-0.4,0-0.8-0.4-0.8-0.8C5,10.3,5.3,10,5.8,10z M5.8,6.6h4.8' +
          'c0.4,0,0.8,0.4,0.8,0.8c0,0.5-0.4,0.8-0.8,0.8H5.8C5.3,8.3,5,7.9,5,7.4C5,6.9,5.3,6.6,5.8,6.6z M17.9,1L17.9,1C20.8,1,23,3.4,23,6.3' +
          'v5.2c0,2.5-1.6,4.3-3.8,4.3c-1.3,0-3.6-0.9-3.6-4.2V6.9c0-1.3,1-2.4,2.3-2.4c1.3,0,2.3,1.1,2.3,2.4v4.7c0,0.5-0.4,0.8-0.8,0.8' +
          's-0.8-0.4-0.8-0.8V6.9c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.7v4.7c0,2.3,1.4,2.5,1.9,2.5l0,0l0.1,0c1.3-0.1,2.1-1.1,2.1-2.6' +
          'V6.3c0-2-1.5-3.6-3.4-3.6h-0.1c-1.9,0-3.4,1.6-3.4,3.6v8c0,0.5-0.4,0.8-0.8,0.8c-0.4,0-0.8-0.4-0.8-0.8v-8C13,3.4,15.2,1,17.9,1z' +
          'M6,1h5.2c0.4,0,0.8,0.4,0.8,0.8c0,0.5-0.4,0.8-0.8,0.8H6c-1.9,0-3.4,1.6-3.4,3.6v11.4c0,2,1.5,3.6,3.4,3.6h10.5' +
          'c1.9,0,3.4-1.6,3.4-3.6c0-0.5,0.4-0.8,0.8-0.8c0.4,0,0.8,0.4,0.8,0.8c0,2.9-2.2,5.3-5,5.3H6c-2.7,0-5-2.4-5-5.3V6.3C1,3.4,3.2,1,6,1z" /></svg>'
        } else {
          return;
        }
        }
      },
      {headerName: '', field: 'IsStock2', suppressMenu: true, width: 40, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'center'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        if (!params.data.StockStatus) {
          return '<button disabled class="btn btn-primary btn-icon btn-icon-min mr-1" title="Oppdater varebeholdning"><span class="icon-update"></span></button>'
        } else if (params.data.StockStatus) {
          return '<button class="btn btn-primary btn-icon btn-icon-min mr-1" title="Oppdater varebeholdning"><span class="icon-update"></span></button>'
        } else {
          return;
        }
        }
      },

      //{headerName: '', field: 'Delete', suppressMenu: true, width: 40, floatingFilterComponentParams: {suppressFilterButton: true},
      //filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'center'}, icons: {
        //sortAscending: '<i class="icon-sort-ascending"/>',
        //sortDescending: '<i class="icon-sort-descending"/>'
      //}, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        //return  '<button class="btn btn-danger btn-icon btn-icon-min" title="Slett"><span class="icon-cancel"></span>'
        //}
      //},
    ]
  }

  onGridReady(params) {
    this.gridApi = params.api;
    // this.gridColumnApi = params.columnApi;
    // auto fits columns to fit screen
    this.gridApi.sizeColumnsToFit();
    // hiding loading message on load
    this.gridApi.gridOptionsWrapper.gridOptions.api.hideOverlay();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    if (this.modelRefNewArticle) {
      this.modelRefNewArticle.close();
    }
    if (this.modalUpdateStock) {
      this.modalUpdateStock.close();
    }
  }

  cellClicked(event) {
    const column = event.column.colDef.field;
    switch (column) {
      case 'IsStock2':
      if (event.data.StockStatus) {
        this.upDateStock(event.data, this.upDateStockModal);
      }
      break;
      case 'Delete':
      this.deleteConfirmMsg(event.data)
    }
  }

  upDateStock(item, content) {
    this.updateChoice = 'add';
    this.IsStockAdd = true;
    this.updateSelectedItem = item;
    this.itemArNo = item.ArticleNo
    this.modalUpdateStock = this.modalService.open(content, { width: '300' });
  }

  closeUpdate() {
    this.modalUpdateStock.close();
  }

  onCategorySelect(val) {
    this.updateStockCat = val;
  }

  stockUpdateMethod(val) {
    if (val === '1') {
      this.updateChoice = 'dee';
      this.IsStockAdd = false;
      this.tempList = this.deductCategoryList;
      this.updateStockCat = this.tempList[0].Id
    } else {
      this.updateChoice = 'add'
      this.IsStockAdd = true;
      this.tempList = this.addCategoryList;
      this.updateStockCat = this.tempList[0].Id
    }
  }

  addVat(val) {
    this.updateAddVat = val
  }

  stockLevelChange(val) {
    this.quantityChange = val;
  }

  purchasePriceChange(val) {
    this.updateStockPurchasePrice = Number.parseFloat(val).toFixed(2);
  }

  upDateStockSubmit() {
    if (!this.updateStockPurchasePrice && this.updateChoice === 'add') {
      let messageTitle = '';
      let messageBody = '';
      this.translate.get('ADMIN.MissingInputError').pipe(
        takeUntil(this.destroy$)
      ).subscribe(tranlstedValue => messageTitle = tranlstedValue);
      this.translate.get('ADMIN.PurchasesPriceErrorNoInput').pipe(
        takeUntil(this.destroy$)
      ).subscribe(tranlstedValue => messageBody = tranlstedValue);
      this.exceMessageService.openMessageBox('ERROR', {
        messageTitle: messageTitle,
        messageBody: messageBody,
        msgBoxId: 'MSG_EMAIL'
      });
      return
    } else if (!this.quantityChange) {
      let messageTitle = '';
      let messageBody = '';
      this.translate.get('ADMIN.MissingInputError').pipe(
        takeUntil(this.destroy$)
      ).subscribe(tranlstedValue => messageTitle = tranlstedValue);
      this.translate.get('ADMIN.QuantityErrorNoInput').pipe(
        takeUntil(this.destroy$)
      ).subscribe(tranlstedValue => messageBody = tranlstedValue);
      this.exceMessageService.openMessageBox('ERROR', {
        messageTitle: messageTitle,
        messageBody: messageBody,
        msgBoxId: 'MSG_EMAIL'
      });
      return
    }
    if (Number(this.quantityChange) > this.updateSelectedItem.StockLevel && this.updateChoice === 'dee') {
      let messageTitle = '';
      let messageBody = '';
      (this.translate.get('ADMIN.StocklevelError').pipe(
        takeUntil(this.destroy$)
      ).subscribe(tranlstedValue => messageTitle = tranlstedValue));
      (this.translate.get('ADMIN.StLevToLow').pipe(
        takeUntil(this.destroy$)
      ).subscribe(tranlstedValue => messageBody = tranlstedValue));
      this.exceMessageService.openMessageBox('ERROR', {
        messageTitle: messageTitle,
        messageBody: messageBody,
        msgBoxId: 'MSG_EMAIL'
      });
      return
    }
    const article = {
      'Id': Number(this.itemArNo),
      'ArticleNo': this.itemArNo,
      'StockLevel': Number(this.quantityChange),
      'IsVatAdded': this.updateAddVat,
      'PurchasedPrice': this.updateStockPurchasePrice,
      'StockCategoryId': this.updateStockCat,
      'BranchId': this.loginservice.SelectedBranch.BranchId,
      'IsStockAdd': this.IsStockAdd
    }
    this.adminService.upDateStock(article).pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      res => {
        if (res.Status === 'OK') {
          this.searchArticles(this.articleType, '', this.selectedCategory, -1, false)
        }
      }
    )
    this.closeUpdate();
  }

  deleteArticleConfirm(articleId) {
    const delObj: any = {};
    delObj.ArticleId = articleId;
    delObj.BranchId = this.branchId;
    delObj.IsAdminUser = this.loginUseRoleId === 1;
    PreloaderService.showPreLoader();
    this.adminService.deleteArticle(delObj).pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      res => {
        PreloaderService.hidePreLoader();
        if (res.Data > 0) {
          const indx = this.articleList.findIndex(x => x.Id === articleId);
          if (indx !== -1) {
            this.articleList.splice(indx, 1);
          }
          this.articleItems = this.articleList;
          this.itemResource = new DataTableResource(this.articleList);
          this.itemResource.count().then(count => this.itemCount = count);
          this.reloadArticleList({ offset: 0 })
        }
      },
      err => {
        PreloaderService.hidePreLoader();
        this.exceMessageService.openMessageBox('ERROR', { messageTitle: 'Exceline', messageBody: 'ADMIN.ErrorDeletingArticle' });
      });
  }

  onArticleTypeChange(value) {
    this.articleCategoryList = [];
    if (value === 'SERVICE') {
      this.serviceCategoryList.forEach(serviceCat => {
        this.articleCategoryList.push(serviceCat);
      });
    } else if (value === 'ITEM') {
      this.itemCategoryList.forEach(itemCat => {
        this.articleCategoryList.push(itemCat);
      });
    } else if (value === 'ALL') {
      this.articleCategoryList = this.serviceCategoryList.concat(this.itemCategoryList);
      // this.serviceCategoryList.forEach(serviceCat => {
      //   this.articleCategoryList.push(serviceCat);
      // });
      // this.itemCategoryList.forEach(itemCat => {
      //   this.articleCategoryList.push(itemCat);
      // });
    }
    this.searchArticles(this.articleType, this.searchText, this.selectedCategory, this.activityCategory, this.isObsolete);
    // this.isDisableActivityChange = false;
    // this.shopArticlesService.$articlelType = value;
    // this.activityCategory = 'ALL';
    // if (value === 'ITEM') {
    //   this.isDisableActivityChange = true;
    // }
  }

  onArtleCategoryChange(value) {
    this.selectedCategory = this.articleCategoryList.find(x => x.Code === this.articleCategory);
    this.selectedCategory = this.selectedCategory === undefined ? { Id: -1, Code: 'ALL', Name: 'All' } : this.selectedCategory;
    this.searchArticles(this.articleType, this.searchText, this.selectedCategory, this.activityCategory, false)
  }

  onActivityChange(value) {
    this.searchArticles(this.articleType, this.searchText, this.selectedCategory, value, false)
  }

  obsoleteChange(e) {
    this.isObsolete = e.target.checked;
    this.searchArticles(this.articleType, this.searchText, this.selectedCategory, this.activityCategory, this.isObsolete)
    // if (e.target.checked) {

    // }

  }
  searchArticles(articleType, searchText, selectedCategory, activityId, IsObsolete) {
    this.adminService.getArticles(articleType, searchText === undefined ? '' : searchText,
      selectedCategory, activityId, true, false, this.loginservice.SelectedBranch.BranchId, IsObsolete).pipe(
        takeUntil(this.destroy$)
        ).subscribe(
      res => {
        this.articleList = res.Data;
        this.articleList.forEach(element => {
          element.VatCode = (element.VatCode === -1) ? '' : element.VatCode
        });
        this.articleItems = this.articleList;
        this.itemResource = new DataTableResource(this.articleList);
        this.itemResource.count().then(count => this.itemCount = count);
        this.reloadArticleList({ offset: 0 })
        // PreloaderService.hidePreLoader();
      },
      err => {
        // PreloaderService.hidePreLoader();

      });
  }

  reloadArticleList(params) {
    if (this.itemResource) {
      this.itemResource.query(params).then(items => this.articleList = items);
    }
  }

  filterArticle(accNameValue?: any, vendorNameValue?: any) {
    let filteredArticlebyAccount = this.articleItems;
    filteredArticlebyAccount = this.filterpipe.transform('RevenueAccountName', 'NAMEFILTER', filteredArticlebyAccount, accNameValue);
    let filteredArticlebyVendor = filteredArticlebyAccount;
    filteredArticlebyVendor = this.filterpipe.transform('VendorName', 'NAMEFILTER', filteredArticlebyVendor, vendorNameValue);
    this.itemResource = new DataTableResource(filteredArticlebyVendor);
    this.reloadArticleList(filteredArticlebyVendor);
  }

  newArticleOpen(Content) {
    this.AType = null;
    this.title = 'ADMIN.RegisterArticleHMAddNewArticle';
    this.articleMode = 'ADD';
    this.modelRefNewArticle = this.modalService.open(Content, { width: '900' });
  }

  importArticleOpen(Content) {
    this.modalService.open(Content, { width: '900' });
  }

  findArticles( ) {
    const selectedSearchItem = this.autocomplete.getTextValue();
    if (selectedSearchItem.id !== '') {
      this.searchText = selectedSearchItem.id + ' :' + selectedSearchItem.searchVal;
    } else {
      this.searchText = selectedSearchItem.searchVal;
    }
    this.searchArticles(this.articleType, this.searchText, this.selectedCategory, this.activityCategory, this.isObsolete)
    // this.shopArticlesService.$searchText = this.searchText;
  }

  onSelectSearchItem(item: any) {
    this.searchText = item.searchText;
    this.searchArticles(this.articleType, this.searchText, this.selectedCategory, this.activityCategory, this.isObsolete)
    // this.shopArticlesService.$searchText = item.searchText;
  }

  closedSearch() {
    this.autocomplete.textValue = '';
    this.textValue = '';
    this.searchText = '';
    this.searchArticles(this.articleType, this.searchText, this.selectedCategory, this.activityCategory, this.isObsolete)
  }

  saveCategory(outputVal) {
    if (outputVal.type === 'SERVICE') {
      this.serviceCategoryList.push(outputVal.category)
    } else if (outputVal.type === 'ITEM') {
      this.itemCategoryList.push(outputVal.category)
    }
    this.articleCategoryList = this.serviceCategoryList.concat(this.itemCategoryList);

  }

  savedArticleEventHandler() {
    this.modelRefNewArticle.close();
    this.searchArticles(this.articleType, this.searchText, this.selectedCategory, this.activityCategory, this.isObsolete)
  }

  closeArticleEventHandler() {
    this.modelRefNewArticle.close();
  }

  articleListRowDoubleClick(rowEvent) {
    if (rowEvent.data.ArticleType === 'ITEM') {
      this.AType = 'ITEM';
    }

    if (rowEvent.data.ArticleType === 'SERVICE') {
      this.AType = 'SERVICE';
    }

    this.title = 'ADMIN.EditArticleC';
    this.selectedArticle = rowEvent.data;
    this.articleMode = 'EDIT';
    this.modelRefNewArticle = this.modalService.open(this.newArticle, { width: '900' });
  }

  deleteConfirmMsg(rowEvent: any) {
    // this.paymentId = rowEvent.PaymentArItemNo;
    let msgType = '';
    let msg = '';
    this.translate.get('COMMON.Confirm').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => msgType = translatedValue);
    this.translate.get('ADMIN.ConfirmMessage').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => msg = translatedValue);
    this.exceMessageService.openMessageBox('CONFIRM',
      {
        messageTitle: msgType,
        messageBody: msg,
        msgBoxId: 'DELETE_ARTICLE',
        optionalData: rowEvent.Id
      }
    );
  }
}
