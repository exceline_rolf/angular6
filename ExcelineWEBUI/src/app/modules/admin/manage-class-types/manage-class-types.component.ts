import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { DataTableResource } from './../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from './../../../shared/components/us-data-table/tools/data-table-filter-pipe'
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { UsbModal } from './../../../shared/components/us-modal/us-modal';
import { ExceMemberService } from '../../membership/services/exce-member.service'
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { AdminService } from '../services/admin.service'
import { ExceLoginService } from '../../../modules/login/exce-login/exce-login.service';
import { ExceMessageService } from '../../../shared/components/exce-message/exce-message.service';
import { ExceBreadcrumbService } from '../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { takeUntil, mergeMap } from 'rxjs/operators';
import { Subject } from 'rxjs';
const now = new Date();


@Component({
  selector: 'app-manage-class-types',
  templateUrl: './manage-class-types.component.html',
  styleUrls: ['./manage-class-types.component.scss']
})
export class ManageClassTypesComponent implements OnInit, OnDestroy {
  private branchId: number;
  private classTypes: any;
  private filteredClassTypes: any;
  private itemResource: any = 0;
  items = [];
  itemCount = 0;
  private branchResource: any = 0;
  private branches = [];
  private branchCount = 0;
  private resources: any;
  private obsVal = false;
  private actvVal = false;
  private allVal = true;
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  private modalReference: any;
  private classTypeForm: any;
  private formSubmited = false;
  private currentColor: string;
  private timeCat = []
  private classGroupCat = []
  private classLevelCat = []
  private isLocal = false
  private subModelReference: any;
  private selectedBranches = []
  private branchesList = []
  private kwResource: any = 0;
  private kw = [];
  private kwCount = 0;
  private kwModelReference: any;
  private selectedKw = []
  private kwList = []
  private selectedCat = []
  private CatList = []
  private CatModelReference: any;
  private cat = [];
  private catCount = 0;
  private catResource: any = 0;
  private catModellReference: any;
  private rowItem: any;
  private isAddNew = true;
  private CatModalReference: any;
  private model: any;
  private destroy$ = new Subject<void>();

  formErrors = {
    'Code': '',
    'Name': '',
    'ClassGroupId': '',
    'ClassLevelId': '',
    'TimeCategoryId': '',
    'Colour': '',
    'ObsoleteLevel': '',
    'TimeDuration': '',
    'MaxNoOfBookings': '',
    'HoursToBePaid': '',
    'Comment': ''

  };

  validationMessages = {
    'Code': {
      'required': 'Date is required.',
    },
    'Name': { 'required': 'Date is required.', },
    'ClassGroupId': { 'required': 'Date is required.', },
    'ClassLevelId': { 'required': 'Date is required.', },
    'TimeCategoryId': { 'required': 'Date is required.', },
    'Colour': { 'required': 'Date is required.', },
    'ObsoleteLevel': { 'required': 'Date is required.', },
    'TimeDuration': { 'required': 'Date is required.', },
    'MaxNoOfBookings': { 'required': 'Date is required.', },
    'Comment': { 'required': 'Date is required.', }
  };
  constructor(private exceMemberService: ExceMemberService,
    private modalService: UsbModal,
    private fb: FormBuilder,
    private adminService: AdminService,
    private exceLoginService: ExceLoginService,
    private exceMessageService: ExceMessageService,
    private exceBreadcrumbService: ExceBreadcrumbService

  ) {
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      (branch) => {
        this.branchId = branch.BranchId;
      }, null, null
    );
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ADMIN.CLASSTYPES',
      url: '/adminstrator/manage-class-types'
    });
  }

  ngOnInit() {
    this.getClassTypes();
    this.classTypeForm = this.fb.group({
      'Code': [null],
      'Name': [null, [Validators.required]],
      'ClassGroupId': [null],
      'ClassLevelId': [null],
      'TimeCategoryId': [null],
      'Colour': [null],
      'ObsoleteLevel': [null],
      'TimeDuration': [0],
      'HoursToBePaid': [0.00],
      'MaxNoOfBookings': [0],
      'Comment': [null]
    });
  }

  getClassTypes() {
    this.adminService.getClassTypes().pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.classTypes = result.Data;
        this.itemResource = new DataTableResource(this.classTypes);
        this.itemResource.count().then(count => this.itemCount = count);
        this.items = this.classTypes;
        this.itemCount = Number(result.Data.length);
      }
    }, null, null)
  }
  filterInput(nameValue: any, grpValue: any, lvlValue: any, kwValue: any, catValue: any) {
    this.filteredClassTypes = this.classTypes
    if (!this.allVal) {
      if (this.obsVal) {
        this.filteredClassTypes = this.filterpipe.transform('ObsoleteLevel', 'SELECT', this.filteredClassTypes, true);
      } else {
        this.filteredClassTypes = this.filterpipe.transform('ObsoleteLevel', 'SELECT', this.filteredClassTypes, false);
      }
    }
    if (nameValue && nameValue !== '') {
      this.filteredClassTypes = this.filterpipe.transform('Name', 'NAMEFILTER', this.filteredClassTypes, nameValue);
    }
    if (grpValue !== '' || grpValue != null) {
      this.filteredClassTypes = this.filterpipe.transform('ClassGroup', 'NAMEFILTER', this.filteredClassTypes, grpValue);
    }
    if (lvlValue !== '' || lvlValue != null) {
      this.filteredClassTypes = this.filterpipe.transform('ClassLevel', 'NAMEFILTER', this.filteredClassTypes, lvlValue);
    }
    if (kwValue !== '' || kwValue != null) {
      this.filteredClassTypes = this.filterpipe.transform('ClassGroup', 'NAMEFILTER', this.filteredClassTypes, kwValue);
    }
    if (catValue !== '' || catValue != null) {
      this.filteredClassTypes = this.filterpipe.transform('ClassCategoryString', 'NAMEFILTER', this.filteredClassTypes, catValue);
    }
    this.itemResource = new DataTableResource(this.filteredClassTypes);
    this.itemResource.count().then(count => this.itemCount = count);
    this.reloadItems(this.filteredClassTypes);
  }

  radioValue(event) {
    if (event.target.value === 'ACTIVE') {
      this.allVal = false;
      this.obsVal = false;
      this.actvVal = true
    } else if (event.target.value === 'OBSOLETE') {
      this.allVal = false;
      this.obsVal = true;
      this.actvVal = false
    } else {
      this.allVal = true;
      this.obsVal = false;
      this.actvVal = false
    }
  }

  reloadItems(params): void {
    if (this.itemResource) {
      this.itemResource.query(params).then(items => this.items = items);
    }
  }

  ngOnDestroy() {
    if (this.modalReference) {
      this.modalReference.close();
    }
    if (this.CatModalReference) {
      this.CatModalReference.close();
    }
    if (this.subModelReference) {
      this.subModelReference.close();
    }
    if (this.kwModelReference) {
      this.kwModelReference.close();
    }
    if (this.CatModelReference) {
      this.CatModelReference.close();
    }
    this.destroy$.next();
    this.destroy$.complete();
  }

  openModel(content: any, isAddNew: boolean) {
    this.isAddNew = isAddNew
    this.modalReference = this.modalService.open(content, { width: '1000' });

  }

  openCatModel(cat) {
    this.CatModalReference = this.modalService.open(cat, { width: '600' });
  }

  submitForm(value) {
    this.formSubmited = true;
    this.edistSave(value)

  }


  loadCategories(model, event?) {
    this.adminService.getCategoriesByType('TIME').pipe(
      mergeMap(result => {
        this.timeCat = result.Data;
        return this.adminService.getCategoriesByType('CLASSGROUP').pipe(
          takeUntil(this.destroy$)
        );
      }),
      mergeMap(result => {
        this.classGroupCat = result.Data;
        return this.adminService.getCategoriesByType('CLASSLEVEL').pipe(
          takeUntil(this.destroy$)
        );
      }),
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if(result) {
        this.classLevelCat = result.Data;
        if(event) {
          this.rowItemLoad(event, model);
        } else {
          this.openModel(model, true);
        }
      }
    }, null, null)
    // this.adminService.getCategoriesByType('TIME').subscribe(result => {
    //   if (result) {
    //     this.timeCat = result.Data;
    //     this.adminService.getCategoriesByType('CLASSGROUP').subscribe(resGroup => {
    //       if (resGroup) {
    //         this.classGroupCat = resGroup.Data;
    //         this.adminService.getCategoriesByType('CLASSLEVEL').subscribe(resLevel => {
    //           if (resLevel) {
    //             this.classLevelCat = resLevel.Data;
    //             if (event) {
    //               this.rowItemLoad(event, model)
    //             } else {
    //               this.openModel(model, true)
    //             }

    //           }

    //         })
    //       }

    //     })
    //   }

    // })


  }

  rowItemLoad(event, model) {
    this.rowItem = event.row.item
    if (this.rowItem.ExcelineBranchList.length > 0) {
      this.isLocal = true
    }
    this.classTypeForm.patchValue(event.row.item)
    this.currentColor = this.rowItem.Colour
    this.branchesList = this.rowItem.ExcelineBranchList
    this.kwList = this.rowItem.ClassKeywordList
    this.CatList = this.rowItem.ClassCategoryList
    this.openModel(model, false)
  }

  colorChange(color) {
    this.currentColor = color;
  }

  openSubModels(content: any) {
    this.adminService.getBranches(-1).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.branches = result.Data;
        this.branches.forEach(element => {
          element.IsCheck = false;
          this.selectedBranches.forEach(ele => {
            if (element.Id === ele.Id) {
              element.IsCheck = true
            }
          });
        });
        this.branchResource = new DataTableResource(this.branches);
        this.branchResource.count().then(count => this.itemCount = count);
        this.branchCount = Number(result.Data.length);
      }
    }, null, null)
    this.subModelReference = this.modalService.open(content, { width: '600' });
  }
  onCheckChange(event, rowItem) {
    if (event.target.checked) {
      this.selectedBranches.push(rowItem)
    } else {
      this.selectedBranches = this.selectedBranches.filter(item => item.Id !== rowItem.Id)
    }
  }

  adBranches() {
    const duplicateBranches = <any>JSON.parse(JSON.stringify(this.selectedBranches));
    this.branchesList = duplicateBranches
    this.subModelReference.close()
  }

  onCheckChangeKw(event, rowItem) {
    if (event.target.checked) {
      this.selectedKw.push(rowItem)
    } else {
      this.selectedKw = this.selectedKw.filter(item => item.Id !== rowItem.Id)
    }
  }

  adKw() {
    const duplicateKw = <any>JSON.parse(JSON.stringify(this.selectedKw));
    this.kwList = duplicateKw
    this.kwModelReference.close()
  }

  openKWModels(content: any) {
    this.adminService.getCategoriesByType('KEYWORD').pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.kw = result.Data;
        this.kw.forEach(element => {
          element.IsCheck = false;
          this.selectedKw.forEach(ele => {
            if (element.Id === ele.Id) {
              element.IsCheck = true
            }
          });
        });
        this.kwResource = new DataTableResource(this.kw);
        this.kwResource.count().then(count => this.itemCount = count);

        this.kwCount = Number(result.Data.length);
      }
    }, null, null)
    this.kwModelReference = this.modalService.open(content, { width: '600' });
  }

  onCheckChangeCat(event, rowItem) {
    if (event.target.checked) {
      this.selectedCat.push(rowItem)
    } else {
      this.selectedCat = this.selectedCat.filter(item => item.Id !== rowItem.Id)
    }
  }

  adCat() {
    const duplicateCat = <any>JSON.parse(JSON.stringify(this.selectedCat));
    this.CatList = duplicateCat
    this.CatModelReference.close()
  }

  closeView() {
    this.CatModalReference.close();
  }

  openCatModels(content: any) {
    this.adminService.getCategoriesByType('CLASS').pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.cat = result.Data;
        this.cat.forEach(element => {
          element.IsCheck = false;
          this.selectedCat.forEach(ele => {
            if (element.Id === ele.Id) {
              element.IsCheck = true
            }
          });


        });
        this.catResource = new DataTableResource(this.cat);
        this.catResource.count().then(count => this.itemCount = count);

        this.catCount = Number(result.Data.length);
      }
    }, null, null)
    this.CatModelReference = this.modalService.open(content, { width: '600' });
  }

  edistSave(value) {
    let classType: any;
    if (!this.isAddNew) {
      for (let key in value) {
        if (this.rowItem[key] || this.rowItem[key] === 0) {
          this.rowItem[key] = value[key]
        }
      }
      classType = this.rowItem
    } else {
      classType = value
    }
    classType.Colour = this.currentColor;
    classType.ExcelineBranchList = this.branchesList
    classType.ClassKeywordList = this.kwList
    classType.ClassCategoryList = this.CatList
    classType.ClassCategoryString = this.generateString(this.CatList)
    classType.ClassKeywordString = this.generateString(this.kwList)
    classType.ClassCategoryStringList = this.generateString(this.CatList)
    this.adminService.AddEditClassType(classType).subscribe(result => {
      if (result) {
        this.getClassTypes()
        this.closeForm()
      }
    })

  }
  generateString(list) {
    let listString = '';
    list.forEach(element => {
      listString = listString + element.Name + ','
    });
    return listString
  }

  generateStringList(list) {
    const stringList = []
    list.forEach(element => {
      stringList.push(element.Name)
    });
  }

  closeForm() {
    this.isLocal = false
    this.classTypeForm.reset()
    this.branchesList = []
    this.CatList = []
    this.kwList = []
    this.modalReference.close()
    this.formSubmited = false;
    this.currentColor = '#fff'
  }


  deleteClassType(value) {
    this.adminService.DeleteClassType(value.Code).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.closeForm();
      }
    },
      error => {
        this.deleteErrorMessage('Exceline', 'The Class type is associated with active schedules.First delete them')
      }, null
    )
  }

  deleteErrorMessage(title, body) {
    this.model = this.exceMessageService.openMessageBox('WARNING',
      {
        messageTitle: title,
        messageBody: body,
        msgBoxId: ' DELETE_ERROR'

      }
    );
  }

}
