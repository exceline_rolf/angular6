import { Component, OnInit } from '@angular/core';
import { ExceClassService } from 'app/modules/class/services/exce-class.service';
import { AdminService } from 'app/modules/admin/services/admin.service';
import { ExceMemberService } from 'app/modules/membership/services/exce-member.service';
import { UsbModal } from 'app/shared/components/us-modal/us-modal';
import { UsbModalRef } from 'app/shared/components/us-modal/modal-ref';
import { ClassHomeService } from 'app/modules/class/class-home/class-home.service';
import * as $ from 'jquery';
import * as moment from 'moment';
import { UsBreadcrumbService } from 'app/shared/components/us-breadcrumb/us-breadcrumb.service';
import { ManageSeasonsService } from 'app/modules/admin/manage-seasons/manage-seasons.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { PreloaderService } from 'app/shared/services/preloader.service';
import { ExceMessageService } from 'app/shared/components/exce-message/exce-message.service';
import { ExceBreadcrumbService } from '../../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { IMyDpOptions } from '../../../../shared/components/us-date-picker/interfaces';
import { UsErrorService } from '../../../../shared/directives/us-error/us-error.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
const now = new Date();

@Component({
  selector: 'app-season-calandar',
  templateUrl: './season-calandar.component.html',
  styleUrls: ['./season-calandar.component.scss']
})
export class SeasonCalandarComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  addSeasonFormSubmited: boolean;
  IsClassDeleted: any;
  IsCopyClass: boolean;
  isCopySheduleItem: boolean;
  savingMode: string;
  dateTimes: any;
  isDateRangeValid: boolean;
  isDisplayCopyOk: boolean;
  isDisplayDelete: boolean;
  isDisplaySave: boolean;
  fromDateInvalid: any;
  toDateinvalid: any;
  addSeasonForm: any;
  isCopyClass: boolean;
  deletedScheduleItemList: any[] = [];
  deletedScheduleItemIdList: any[] = [];
  newScheduleItemList: any[] = [];
  deletedEvent: any;
  editedScheduleItemList: any[] = [];
  activeTimesBase: any;
  activeTimes: any;
  deleteEventId: any;
  activeItemsForWeekShedule: any[] = [];
  defaultDate: any;
  selectedStartTime: any;
  classBookings: any;
  addNewClassModel: UsbModalRef;
  dayOfWeek: number;
  fromDate: { date: { year: any; month: any; day: any; }; };
  toDate: { date: { year: any; month: any; day: any; }; };
  startTime: string;
  isNewBooking: any;
  eventContextmenuY: any;
  eventontextmenuX: any;
  selectedActiveTime: any;
  scheduler: any;
  nohandlerSubs: Subscription;
  yeshandlerSubs: Subscription;
  model: any;
  schedule: any;
  newEndTime: Date;
  newStartTime: Date;
  contextmenuY: any;
  contextmenuX: any;
  isShowEventContextMenu: boolean;
  isShowDayContextMenu: boolean;
  classActiveTimes: any[];
  selectedSeason: any;
  seasonId: number;

  classIds: any[];
  classTypeList: any;
  editViewModal: UsbModalRef;
  listViewModal: UsbModalRef;
  resourceRecords: any;
  instructorList: any;
  keyWordList: any;
  classLevelList: any;
  classGroupList: any;

  // calandar related properties
  theame: boolean;
  events: any[];
  resources: any[];
  header: any;
  event: any = {};
  Height = 630;
  dialogVisible = false;
  idGen = 100;
  viewType: string;
  Currentevent: string;
  views: any;
  scrollTime: string;
  updateClassActiveTimeHelper: any = {};
  today: { year: number; month: number; day: number; };

  isCalandarView = false;
  filterCriteria: any = {
    keyWord: '',
    instructor: '',
    group: '',
    type: '',
    level: '',
    location: ''
  }

  formErrors = {
    'name': '',
    'fromDate': '',
    'toDate': ''
  };

  validationMessages = {
    'name': {
      'required': 'CLASS.NameReq'
    },
    'fromDate': {
      'required': 'CLASS.StartDateReq',
      'fromDateRangeInvalid': 'CLASS.DateRangeInvalid'
    },
    'toDate': {
      'required': 'CLASS.EndDateReq',
      'toDateInvalid': 'CLASS.ToDateInvalid',
      'dateRangeInvalid': 'CLASS.DateRangeInvalid',
      'startDateLowerThanMinClassesStartDate': 'CLASS.StartDateLowerThanMinClassesStartDate'

    }
  };

  fromDatePickerOptions: IMyDpOptions = {
    showClearDateBtn: false,
    disableUntil: { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() },
  };

  toDatePickerOptions: IMyDpOptions = {
    showClearDateBtn: false,
    disableUntil: { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() }
  };
  defaultStartDate: any;
  defaultEndDate: any;

  constructor(
    private modalService: UsbModal,
    private route: ActivatedRoute,
    private exceClassService: ExceClassService,
    private adminService: AdminService,
    private classHomeService: ClassHomeService,
    private fb: FormBuilder,
    private exceMessageService: ExceMessageService,
    private exceMemberService: ExceMemberService,
    private manageSeasonsService: ManageSeasonsService,
    private exceBreadcrumbService: ExceBreadcrumbService,
    private exceRouter: Router
  ) {

    this.isCalandarView = false;
    this.scrollTime = moment().format('hh:mm:ss');
    this.isCopyClass = false;


    this.exceMessageService.yesCalledHandle.pipe(takeUntil(this.destroy$)).subscribe((value) => {
      if (value.id === 'CalenderChangeConfirmation') {
        // this.yesHandler();
      } else if (value.id === 'ClassEntityProcessMsg') {
      } else if (value.id === 'SaveUnsaved') {
        this.saveUpdatedClassList();
      } else if (value.id === 'DeleteConfirm') {
        this.deletedScheduleItemList.push(this.deletedEvent);
        this.events = this.events.filter(x => x.activeTimeDetail.Id !== this.deletedEvent.Id);
        // this.exceClassService.DeleteClass({ classId: this.deleteEventId }).pipe(takeUntil(this.destroy$)).subscribe(
        //   res => {
        //     this.searchClasses();
        //   }
        // )
      }
    });

    this.exceMessageService.noCalledHandle.pipe(takeUntil(this.destroy$)).subscribe((value) => {
      // this.noHandler();
    });
  }

  ngOnInit() {
    /* Fix to prevent undefined behavior on pagereload */
    if (!this.manageSeasonsService.SelectedSeason) {
      this.exceRouter.navigateByUrl('adminstrator/manage-seasons')
    } else {
      this.seasonId = this.manageSeasonsService.SelectedSeason.Schedule.Id
    }
   // ;
    this.addSeasonForm = this.fb.group({
      name: [null, [Validators.required]],
      fromDate: [{ date: this.today }, [Validators.required], [this.fromDateRangeInvalidValidator.bind(this)]],
      toDate: [{ date: this.today }, [Validators.required], [this.toDateRangeInvalidValidator.bind(this)]]
    });
    this.addSeasonForm.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(__ => {
      UsErrorService.onValueChanged(this.addSeasonForm, this.formErrors, this.validationMessages)
    });
    this.isDisplaySave = true;
    this.isDisplayDelete = true;
    this.isDisplayCopyOk = false;

    if (this.manageSeasonsService.SelectedSeason) {
      const season = this.manageSeasonsService.SelectedSeason;
      this.selectedSeason = this.manageSeasonsService.SelectedSeason;
      this.exceBreadcrumbService.addBreadCumbItem.next({
        name: season.Name + ' SEASON (' + moment(season.Schedule.StartDate).format('M-D-YYYY') +
          '-' + moment(season.Schedule.EndDate).format('M-D-YYYY') + ')',
        url: '/adminstrator/manage-seasons/season-calandar'
      });
      this.defaultStartDate = this.selectedSeason.ScheduleStartDate;
      this.defaultEndDate = this.selectedSeason.ScheduleEndDate;
    }
    this.manageSeasonsService.selectSeason.pipe(takeUntil(this.destroy$)).subscribe(
    season => {
      // this.breadcrumbService.addFriendlyNameForRoute(
      //   '/adminstrator/manage-seasons/season-calandar/' + this.route.snapshot.params['Id'],
      //   season.Name + ' SEASON (' + moment(season.Schedule.StartDate).format('M-D-YYYY') +
      //   '-' + moment(season.Schedule.EndDate).format('M-D-YYYY') + ')'
      // );
      this.exceBreadcrumbService.addBreadCumbItem.next({
        name: season.Name + ' SEASON (' + moment(season.Schedule.StartDate).format('M-D-YYYY') +
          '-' + moment(season.Schedule.EndDate).format('M-D-YYYY') + ')',
        url: '/adminstrator/manage-seasons/season-calandar'
      });
    });

    this.adminService.getCategories('CLASSGROUP').pipe(takeUntil(this.destroy$)).subscribe(
      res => {
        this.classGroupList = res.Data;
      }, null, null);
    this.adminService.getCategories('CLASSLEVEL').pipe(takeUntil(this.destroy$)).subscribe(
      res => {
        this.classLevelList = res.Data;
      }, null, null);
    this.adminService.getCategories('KEYWORD').pipe(takeUntil(this.destroy$)).subscribe(
      res => {
        this.keyWordList = res.Data;
      }, null, null);
    this.exceMemberService.getInstructors('', true).pipe(takeUntil(this.destroy$)).subscribe(
      res => {
        this.instructorList = res.Data;
        this.classHomeService.InstructorList = this.instructorList;
      }, null, null);
    this.exceClassService.GetResources({ searchText: '' }).pipe(takeUntil(this.destroy$)).subscribe(
      res => {
        this.resourceRecords = res.Data;
        this.classHomeService.ResourceRecords = this.resourceRecords;
      }, null, null);
    this.exceClassService.GetClassTypes().pipe(takeUntil(this.destroy$)).subscribe(
      classTypes => {
        this.classTypeList = classTypes.Data;
        this.classHomeService.ClassTypeList = this.classTypeList;
        this.exceClassService.GetClasses({ className: '' }).pipe(takeUntil(this.destroy$)).subscribe(
          classes => {
            this.classIds = [];
            classes.Data.forEach(clsss => {
              this.classIds.push(clsss.Id)
            });
            this.classHomeService.ExcelineClassList = classes.Data;
            this.classHomeService.ClassIds = this.classIds;
            this.exceClassService.GetListOrCalendarInitialViewByUser().pipe(takeUntil(this.destroy$)).subscribe(
              res => {
                if (res.Data === 'NEW') {
                  if (!this.selectedSeason.IsCalendarPriority) {
                    // ClassInstanceView classInstanceView = new ClassInstanceView(season);
                    // childObject = classInstanceView;
                    // _classInstanceView = classInstanceView;
                    // classInstanceView.Loaded += classInstanceView_Loaded;
                    // classInstanceView.StartDateChangedEvent += ClassCalenderHomeView_StartDateChangedEvent;
                    // grdCalander.Children.Add(childObject as System.Windows.UIElement);
                  } else {
                    // _calenderLoaderView = new CalenderLoaderView(season, false);
                    // childObject = _calenderLoaderView;
                    // _calenderLoaderView.Loaded += _calenderLoaderView_Loaded;
                    // grdCalander.Children.Add(childObject as System.Windows.UIElement);
                  }
                } else if (res.Data === 'List') {
                  // ClassInstanceView classInstanceView = new ClassInstanceView(season);
                  // childObject = classInstanceView;
                  // _classInstanceView = classInstanceView;
                  // classInstanceView.Loaded += classInstanceView_Loaded;
                  // classInstanceView.StartDateChangedEvent += ClassCalenderHomeView_StartDateChangedEvent;
                  // grdCalander.Children.Add(childObject as System.Windows.UIElement);
                } else {
                  // _calenderLoaderView = new CalenderLoaderView(season, false);
                  // childObject = _calenderLoaderView;
                  // _calenderLoaderView.Loaded += _calenderLoaderView_Loaded;
                  // grdCalander.Children.Add(childObject as System.Windows.UIElement);
                  this.getScheduleItemsByClass();
                }
              }, null, null
            );
          }, null, null);
      }, null, null);

    this.exceClassService.GetListOrCalendarInitialViewByUser().pipe(takeUntil(this.destroy$)).subscribe(
      res => {
      }, null, null);

    this.exceClassService.GetGymSettings({ gymSettingType: 'OTHER' }).pipe(takeUntil(this.destroy$)).subscribe(
      res => {
      }, null, null);

    this.viewType = 'agendaTwoweeks';
    this.theame = false;
    this.header = {
      right: '',
      center: '',
      left: ''
    };

    this.views = {
      agendaTwoweeks: {
        type: 'agendaWeek',
        columnFormat: 'ddd',
        duration: { days: 14 },
        // views that are more than a day will NOT do this behavior by default
        // so, we need to explicitly enable it
        groupByResource: false,
        //// uncomment this line to group by day FIRST with resources underneath
        groupByDateAndResource: false
      }
    };
    this.defaultDate = moment().startOf('week');
  }

  private getScheduleItemsByClass() {
    PreloaderService.showPreLoader();
    this.exceClassService.GetScheduleItemsByClass({ classId: this.route.snapshot.params['Id'] }).pipe(
      takeUntil(this.destroy$)
      ).subscribe(activeTimes => {
        this.activeTimes = activeTimes.Data;
        this.activeTimesBase = activeTimes.Data;
        this.generateWeekCalandarData(this.activeTimes);
        this.createClassResources();
        PreloaderService.hidePreLoader();
      }, err => {
        PreloaderService.hidePreLoader();
      }
    )
  }

  private generateWeekCalandarData(activeTimes: any) {
    const temp = new Array();
    activeTimes.forEach(element => {
      const newEle = {};
      if (element.WeekType === 2) {
        newEle['isAllEven'] = true;
        // tslint:disable-next-line:forin
        for (const key in element) {
          newEle[key] = element[key];
        }
        temp.push(newEle);
      }
    });
    this.activeItemsForWeekShedule = this.activeTimes.concat(temp);
    this.activeItemsForWeekShedule.forEach(times => {
      const cltype = this.classTypeList.filter(x => x.Id === times.ClassTypeId);
      const startTime = moment(times.StartTime).format('HH:mm');
      const endTime = moment(times.EndTime).format('HH:mm');
      const defaultDate = moment().startOf('week');
      switch (times.Day) {
        case 'Sunday':
          const sunday = (times.isAllEven) ? defaultDate.add(7, 'days') : (times.WeekType === 0) ? defaultDate.add(7, 'days') : defaultDate;
          times.StartTime = moment(sunday.format('YYYY-M-D') + ' ' + startTime);
          times.EndTime = moment(sunday.format('YYYY-M-D') + ' ' + endTime);
          break;
        case 'Monday':
          const monday = (times.isAllEven) ? defaultDate.add(8, 'days') : (times.WeekType === 0) ? defaultDate.add(8, 'days') : defaultDate.add(1, 'days');
          times.StartTime = moment(monday.format('YYYY-M-D') + ' ' + startTime);
          times.EndTime = moment(monday.format('YYYY-M-D') + ' ' + endTime);
          break;
        case 'Tuesday':
          const tuesday = (times.isAllEven) ? defaultDate.add(9, 'days') : (times.WeekType === 0) ? defaultDate.add(9, 'days') : defaultDate.add(2, 'days');
          times.StartTime = moment(tuesday.format('YYYY-M-D') + ' ' + startTime);
          times.EndTime = moment(tuesday.format('YYYY-M-D') + ' ' + endTime);
          break;
        case 'Wednesday':
          const wednesday = (times.isAllEven) ? defaultDate.add(10, 'days') : (times.WeekType === 0) ? defaultDate.add(10, 'days') : defaultDate.add(3, 'days');
          times.StartTime = moment(wednesday.format('YYYY-M-D') + ' ' + startTime);
          times.EndTime = moment(wednesday.format('YYYY-M-D') + ' ' + endTime);
          break;
        case 'Thursday':
          const thursday = (times.isAllEven) ? defaultDate.add(11, 'days') : (times.WeekType === 0) ? defaultDate.add(11, 'days') : defaultDate.add(4, 'days');
          times.StartTime = moment(thursday.format('YYYY-M-D') + ' ' + startTime);
          times.EndTime = moment(thursday.format('YYYY-M-D') + ' ' + endTime);
          break;
        case 'Friday':
          const friday = (times.isAllEven) ? defaultDate.add(12, 'days') : (times.WeekType === 0) ? defaultDate.add(12, 'days') : defaultDate.add(5, 'days');
          times.StartTime = moment(friday.format('YYYY-M-D') + ' ' + startTime);
          times.EndTime = moment(friday.format('YYYY-M-D') + ' ' + endTime);
          break;
        case 'Saturday':
          const saturday = (times.isAllEven) ? defaultDate.add(13, 'days') : (times.WeekType === 0) ? defaultDate.add(13, 'days') : defaultDate.add(6, 'days');
          times.StartTime = moment(saturday.format('YYYY-M-D') + ' ' + startTime);
          times.EndTime = moment(saturday.format('YYYY-M-D') + ' ' + endTime);
          break;
      }
      times.ClassType = cltype[0];
      times.Color = cltype[0].Colour;
    });
    this.classHomeService.ClassActiveTimes = this.activeItemsForWeekShedule;
  }

  private createClassResources() {
    this.events = [];
    this.resources = [];
    if (this.classHomeService.ClassActiveTimes.length > 0) {
      this.classActiveTimes = this.classHomeService.ClassActiveTimes;
      this.activeItemsForWeekShedule.forEach((activeTimes, index) => {

        this.resources.push({
          id: activeTimes.ClassTypeId,
          activeTimesId: activeTimes.Id,
          title: activeTimes.ClassType.Name,
          eventColor: activeTimes.Color
        });

        this.events.push({
          id: index,
          resourceId: activeTimes.ClassTypeId,
          activeTimesId: activeTimes.Id,
          start: new Date(activeTimes.StartTime),
          end: new Date(activeTimes.EndTime),
          title: activeTimes.ClassType.Name,
          color: activeTimes.Color,
          activeTimeDetail: activeTimes
        });

      });
    }
  }

  saveNewClassSuccess(event) {
    if (!this.isNewBooking) {
      event.Id = this.selectedActiveTime.Id;
    }
    event.IsNew = true;
    event.ScheduleId = this.selectedSeason.Schedule.Id;

    this.activeTimes = this.activeTimes.filter(x => x.Id !== event.Id);
    this.activeTimes.push(event);
    this.generateWeekCalandarData(this.activeTimes);
    this.createClassResources();

    this.editedScheduleItemList = this.editedScheduleItemList.filter(x => x.Id !== event.Id);
    event.StartTime = event.StartTime.format('YYYY-M-D HH:mm');
    event.EndTime = event.EndTime.format('YYYY-M-D HH:mm');
    this.editedScheduleItemList.push(event);
    this.addNewClassModel.close();
    if (this.isNewBooking) {
      this.newScheduleItemList.push(event);
    }
  }

  saveUpdatedClassList() {
    const activeTimeList = [];
    this.newScheduleItemList.forEach(event => {
      event.StartTime = event.StartTime.format('YYYY-M-D HH:mm');
      event.EndTime = event.EndTime.format('YYYY-M-D HH:mm');
      this.editedScheduleItemList.push(event);
    });
    this.exceClassService.UpdateTimeTableScheduleItems({
      SceduleItemList: this.editedScheduleItemList
    }).pipe(
      takeUntil(this.destroy$)
      ).subscribe(res => {
        this.editedScheduleItemList = [];
        this.newScheduleItemList = [];
        if (res.Data === 0) {
          if (this.deletedScheduleItemList.length > 0) {
            this.deletedScheduleItemList.forEach(element => {
              this.deletedScheduleItemIdList.push(element.Id);
            });
            this.exceClassService.DeleteScheduleItem({ ScheduleItemIdList: this.deletedScheduleItemIdList }).pipe(takeUntil(this.destroy$)).subscribe(
              resDel => {
                this.deletedScheduleItemIdList = [];
                this.undoAddingClasses();
              },
              errDel => {
                this.undoAddingClasses();
              }
            );
          }
        } else if (res.Data === -1) {
          this.model = this.exceMessageService.openMessageBox('CONFIRM',
            {
              messageTitle: 'Confirm',
              messageBody: 'CLASS.ScheduleOverlapWithInstructor'
            }
          );
          this.undoAddingClasses();
        } else if (res.Data === -2) {
          this.undoAddingClasses();
          this.model = this.exceMessageService.openMessageBox('CONFIRM',
            {
              messageTitle: 'Confirm',
              messageBody: 'CLASS.ScheduleOverlapWithResource'
            }
          );
        } else {
          this.undoAddingClasses();
        }
      },
      err => {
        this.editedScheduleItemList = [];
        this.newScheduleItemList = [];
        this.deletedScheduleItemIdList = [];
      }
    )
  }

  openListView() {
    this.isCalandarView = false;
  }

  OpenCalandarView() {
    this.isCalandarView = true;
  }

  OpenEditView(content) {
    this.toDateinvalid = false;
    this.fromDateInvalid = false;
    const fromDtDetail = moment(this.selectedSeason.ScheduleStartDate);
    const toDateDetail = moment(this.selectedSeason.ScheduleEndDate);
    this.editViewModal = this.modalService.open(content, { width: '500' });
    const addme = (new Date(this.selectedSeason.ScheduleStartDate).getDate() - 1) === 0 ? 0 : 1

    this.fromDatePickerOptions = {
      showClearDateBtn: true,
      disableUntil: { year: Number(3000), month: Number(1), day: Number(1) },
      disableSince: { year: Number(1900), month: Number(1), day: Number(1) }
    };
    this.toDatePickerOptions = {
      showClearDateBtn: false,
      disableUntil: { year: Number(3000), month: Number(1), day: Number(1) },
      disableSince: { year: Number(1900), month: Number(1), day: Number(1) }
    };

    // this.checkDateRangeValid(fromDtDetail, toDateDetail);
    this.addSeasonForm.patchValue({
      name: this.selectedSeason.Name,
      fromDate: { date: { year: fromDtDetail.format('YYYY'), month: fromDtDetail.format('M'), day: fromDtDetail.format('D') } },
      toDate: { date: { year: toDateDetail.format('YYYY'), month: toDateDetail.format('M'), day: toDateDetail.format('D') } }
    });
    this.isDisplaySave = true;
    this.isDisplayDelete = this.selectedSeason.IsClassDeleted;
    this.IsClassDeleted = this.selectedSeason.IsClassDeleted;
    this.IsCopyClass = false;
    this.isDisplayCopyOk = false;
    this.isCopySheduleItem = false;
    this.savingMode = 'Edit';
  }

  onSelectResource(event) {
  }

  handleDayClick(event) {
    this.isShowDayContextMenu = false;
    this.isShowEventContextMenu = false;
  }

  onRightClick(event, dayContextMenu) {
    this.selectedStartTime = event.date.format();
    this.contextmenuX = event.jsEvent.pageX;
    this.contextmenuY = event.jsEvent.pageY;
    this.isShowDayContextMenu = true;
    this.isShowEventContextMenu = false;
  }

  openAddNewClassModel(content, isNewBooking) {
    this.isNewBooking = isNewBooking;
    this.isShowDayContextMenu = false;
    this.isShowEventContextMenu = false;
    if (this.isNewBooking) {
      this.startTime = moment(this.selectedStartTime).format('HH:mm');
    } else {
      this.startTime = this.selectedActiveTime.StartTime.format('HH:mm');
    }
    const season = this.manageSeasonsService.SelectedSeason
    if (season) {
      this.fromDate = {
        date: {
          year: moment(season.Schedule.StartDate).format('YYYY'),
          month: moment(season.Schedule.StartDate).format('M'),
          day: moment(season.Schedule.StartDate).format('D')
        }
      };
      this.toDate = {
        date: {
          year: moment(season.Schedule.EndDate).format('YYYY'),
          month: moment(season.Schedule.EndDate).format('M'),
          day: moment(season.Schedule.EndDate).format('D')
        }
      };
    }
    this.dayOfWeek = moment(this.selectedStartTime).day();
    this.addNewClassModel = this.modalService.open(content, { width: '1000' });
  }

  openViewBookingsModel(content) {
    this.isShowDayContextMenu = false;
    this.isShowEventContextMenu = false;
    this.addNewClassModel = this.modalService.open(content, { width: '700' });
    PreloaderService.showPreLoader();
    this.exceClassService.GetMembersByActiveTimeId({ activeTimeId: this.selectedActiveTime.SheduleItemId }).pipe(takeUntil(this.destroy$)).subscribe(
      res => {
        this.classBookings = res.Data;
        PreloaderService.hidePreLoader();
      }, err => {
        PreloaderService.hidePreLoader();
      }
    )
  }

  hideContextMenu() {
    this.isShowDayContextMenu = false;
    this.isShowEventContextMenu = false;
  }

  handleEventClick(e) {
    this.isShowDayContextMenu = false;
    this.isShowEventContextMenu = false;
    this.event = {};
    this.event.title = e.calEvent.title;
    const start = e.calEvent.start;
    const end = e.calEvent.end;
    if (e.view.name === 'month' || e.view.name === 'basicWeek' || e.view.name === 'basicDay') {
      start.stripTime();
    }
    if (end) {
      end.stripTime();
      this.event.end = end.format();
    }
    this.event.id = e.calEvent.id;
    this.event.start = start.format();
    this.dialogVisible = true;
  }


  onEventDragStart(event) {
    this.isShowDayContextMenu = false;
    this.isShowEventContextMenu = false;
  }

  handleOnDrop(event) {
    this.newStartTime = new Date(event.event.start.format());
    this.newEndTime = new Date(event.event.end.format());
    event.event.activeTimeDetail.Day = event.event.start.format('dddd');
    event.event.activeTimeDetail.StartTime = event.event.start.format('YYYY-M-D HH:mm');
    event.event.activeTimeDetail.EndTime = event.event.end.format('YYYY-M-D HH:mm');
    this.editedScheduleItemList = this.editedScheduleItemList.filter(x => x.id !== event.event.id);
    this.editedScheduleItemList.push(event.event.activeTimeDetail);
  }

  eventResizeStop(event) {
    this.newStartTime = new Date(event.event.start.format());
    this.newEndTime = new Date(event.event.end.format());
    event.event.activeTimeDetail.Day = event.event.start.format('dddd');
    event.event.activeTimeDetail.StartTime = event.event.start.format('YYYY-M-D HH:mm');
    event.event.activeTimeDetail.EndTime = event.event.end.format('YYYY-M-D HH:mm');
    this.editedScheduleItemList = this.editedScheduleItemList.filter(x => x.id !== event.event.id);
    this.editedScheduleItemList.push(event.event.activeTimeDetail);
  }

  SaveScheduleItem() {
    this.exceClassService.UpdateClassActiveTime(this.updateClassActiveTimeHelper).pipe(takeUntil(this.destroy$)).subscribe(
      res => {
        PreloaderService.hidePreLoader();
        if (res.Data === 0) {
          this.model = this.exceMessageService.openMessageBox('WARNING',
            {
              messageTitle: 'Exceline',
              messageBody: 'CLASS.UpdatedSuccessfully'
            }
          );
          // this.updateClassEvent.emit(res);
        } else if (res.Data === -1) {
          this.model = this.exceMessageService.openMessageBox('WARNING',
            {
              messageTitle: 'Exceline',
              messageBody: 'CLASS.ScheduleOverlapWithInstructor'
            }
          );
          this.createClassResources();

        } else if (res.Data === -2) {
          this.model = this.exceMessageService.openMessageBox('WARNING',
            {
              messageTitle: 'Exceline',
              messageBody: 'CLASS.ScheduleOverlapWithResource'
            }
          );
          this.createClassResources();

        }
      }, err => {
        PreloaderService.hidePreLoader();
        this.createClassResources();

      });
  }

  dragStop(event, scheduler) {
    this.scheduler = scheduler;
  }

  dayRender(date, cell) {

  }

  onEventRightclick(event) {
    this.selectedActiveTime = event.date.activeTimeDetail;
    this.eventontextmenuX = event.jsEvent.pageX;
    this.eventContextmenuY = event.jsEvent.pageY;
    this.isShowEventContextMenu = true;
    this.isShowDayContextMenu = false;
  }

  eventRender(event, element) {
    element.find('.fc-content').append(`<span class="badge badge-pill badge-light fc-exe-badge">`
      + `</span><i  class=\'icon-close float-right fc-close\' id=\'` + event.activeTimeDetail.Id + `\'></i>`);
  }

  deleteEvent(event): void {
    this.deletedEvent = event;
    if (!this.model) {
      this.model = this.exceMessageService.openMessageBox('CONFIRM',
        {
          messageTitle: 'Exceline',
          messageBody: 'CLASS.DeleteConfirm',
          msgBoxId: 'DeleteConfirm'
        }
      );
    }
  }

  mouseOver(event, scheduler) {
    const self = this;
    this.scheduler = scheduler;
    let instructerListHtml = '';
    let resourceLstHtml = '';
    if (event.calEvent.activeTimeDetail.InstructorNameListStr) {
      instructerListHtml = '<ul class="m-0 p-0 fc-exc-list fc-exc-list-instructor">';
      instructerListHtml = instructerListHtml + '<li>' + event.calEvent.activeTimeDetail.InstructorNameListStr + '</li>';
      instructerListHtml = instructerListHtml + '</ul>';
    }
    if (event.calEvent.activeTimeDetail.LocationNameListStr) {
      resourceLstHtml = '<ul class="m-0 p-0 fc-exc-list fc-exc-list-location">';
      resourceLstHtml = resourceLstHtml + '<li>' + event.calEvent.activeTimeDetail.LocationNameListStr + '</li>';
      resourceLstHtml = resourceLstHtml + '</ul>';
    }

    $('#' + event.calEvent.activeTimeDetail.Id).click(function () {
      // self.deleteEvent(event.calEvent.activeTimeDetail.SheduleItemId);
      self.deleteEvent(event.calEvent.activeTimeDetail);
    });

    let weekType = '';
    if (event.calEvent.activeTimeDetail.WeekType === 2) {
      weekType = 'ALL';
    } else if (event.calEvent.activeTimeDetail.WeekType === 1) {
      weekType = 'ODD';
    } else if (event.calEvent.activeTimeDetail.WeekType === 0) {
      weekType = 'EVEN';
    }

    const tooltip =
      `<div class="event-tooltip">
      <div class="mb-1">
        <strong>` + event.calEvent.title + `</strong>
      </div>
      <div><span class="text-muted">Number :</span>
        <span>` + event.calEvent.activeTimeDetail.ClassNumber + `</span>
      </div>
      <div><span class="text-muted">Day :</span>
        <span>` + moment(event.calEvent.start._d).format('dddd') + `</span>
        <span> | </span>
        <span>` + moment(event.calEvent.start._d).format('h:mm a') + `
        <span> - </span>` + moment(event.calEvent.end._d).format('h:mm a') + `
      </span>
      </div>
      <div><span class="text-muted">Week Type :</span>
        <span>
          ` + weekType + `
        </span>
      </div>
      <div><span class="text-muted">Max No of Bookings :</span>
        <span>
          ` + event.calEvent.activeTimeDetail.MaxNoOfBooking + `
        </span>
      </div>
      <div><span class="text-muted">Instructors</span></div>
      <div>` + instructerListHtml + `
      </div>
      <div class="dropdown-divider"></div>
      <div ><span class="text-muted">Location</span></div>
      <div>` + resourceLstHtml + `
      </div>
    </div>`;

    const $tooltip = $(tooltip).appendTo('body');
    let clientHeight = 0;
    let clientWidth = 0;
    $(event.jsEvent.currentTarget).mouseover(function (e) {
      $(event.jsEvent.currentTarget).css('z-index', 10000);
      $tooltip.fadeIn('500');
      $tooltip.fadeTo('10', 1.9);
      clientHeight = document.body.clientHeight;
      clientWidth = document.body.clientWidth;

    }).mousemove(function (e) {

      this.isShowTooltip = true;


      if ((e.pageX + $('.event-tooltip').width() + 70) > clientWidth) {
        this.tooltipX = e.pageX - $('.event-tooltip').width();
        $tooltip.css('left', e.pageX - ($('.event-tooltip').width() + 40));
      } else {
        this.tooltipX = e.pageX + 20;
        $tooltip.css('left', e.pageX + 20);
      }

      if ((e.pageY + $('.event-tooltip').height() + 30) > clientHeight) {
        this.tooltipY = e.pageY - ($('.event-tooltip').height() + 30);
        $tooltip.css('top', e.pageY - ($('.event-tooltip').height() + 30));

      } else {
        $tooltip.css('top', e.pageY + 10);
        this.tooltipY = e.pageY + 10;
      }
    });
  }

  mouseOut(event, element) {
    $(event.jsEvent.currentTarget).css('z-index', 8);
    $('.event-tooltip').remove();
  }

  ViewRender(view, element) {
  }

  cancelAddingNewCls() {
    this.addNewClassModel.close();
  }

  onTypeChange(value) {
    this.filterCriteria.type = value;
    this.classHomeService.headerFilter.next(this.filterCriteria);
  }

  onInstructorChange(value) {
    this.filterCriteria.instructor = value;
    this.classHomeService.headerFilter.next(this.filterCriteria);
  }

  onKeywordChange(value) {
    this.filterCriteria.keyWord = value;
    this.classHomeService.headerFilter.next(this.filterCriteria);
  }

  onGroupChange(value) {
    this.filterCriteria.group = value;
    this.classHomeService.headerFilter.next(this.filterCriteria);
  }

  onLevelChange(value) {
    this.filterCriteria.level = value;
    this.classHomeService.headerFilter.next(this.filterCriteria);
  }

  onLocationChange(value) {
    this.filterCriteria.location = value;
    this.classHomeService.headerFilter.next(this.filterCriteria);
  }

  undoAddingClasses(): void {
    this.getScheduleItemsByClass();
    this.createClassResources();
  }

  copyClass(): void {
    this.isCopyClass = true;
    this.isShowEventContextMenu = false;
  }

  pasteClass(): void {
    const copiedObj = JSON.parse(JSON.stringify(this.selectedActiveTime));
    copiedObj.Id = -1;
    delete copiedObj.isAllEven;
    this.isShowDayContextMenu = false;
    this.isCopyClass = true;
    copiedObj.Day = moment(this.selectedStartTime).format('dddd');
    copiedObj.StartTime = moment(this.selectedStartTime).format('YYYY-M-D HH:mm');
    copiedObj.EndTime = (moment(this.selectedStartTime).add(copiedObj.ClassType.TimeDuration, 'minute')).format('YYYY-M-D HH:mm');

    this.activeTimes.push(copiedObj);
    this.generateWeekCalandarData(this.activeTimes);
    this.createClassResources();
    copiedObj.StartTime = copiedObj.StartTime.format('YYYY-M-D HH:mm');
    copiedObj.EndTime = copiedObj.EndTime.format('YYYY-M-D HH:mm');
    this.editedScheduleItemList.push(copiedObj);
  }

  fromDateRangeInvalidValidator(control: AbstractControl) {
    return new Promise(resolve => {
      setTimeout(() => {
        if (this.fromDateInvalid) {
          resolve({
            'fromDateRangeInvalid': true
          })
        } else {
          resolve(null);
        }
      }, 100);
    });
  }

  toDateRangeInvalidValidator(control: AbstractControl) {
    return new Promise(resolve => {
      setTimeout(() => {
        if (this.toDateinvalid) {
          resolve({
            'dateRangeInvalid': true
          })
        } else {
          resolve(null);
        }
      }, 100);
    });
  }

  checkDateRangeValid(fromDate, toDate) {
    this.isDateRangeValid = (fromDate < toDate) ? true : false;
    if (this.isDateRangeValid) {
      this.dateTimes = this.dateTimes.filter(x => x.classItemId !== this.selectedSeason.Id);
      this.dateTimes.forEach(item => {
        const stDate = item.scheduleDateList[0];
        const endDate = item.scheduleDateList[1];
        if (!(fromDate < moment(stDate)) || !(toDate < moment(stDate))) {
          if (!(!toDate || toDate > moment(endDate)) || !(fromDate > moment(endDate))) {
            if (!(fromDate < moment(stDate)) && !(fromDate > moment(endDate))) {
              this.fromDateInvalid = true;
            } else {
              this.toDateinvalid = true;
            }
            return false;
          }
        }
      });
    }

  }

  saveNewSeasons(value) {
    this.addSeasonFormSubmited = true;
    if (this.addSeasonForm.valid) {
      PreloaderService.showPreLoader();

      const body = {
        ExcelineClass: {
          Id: this.selectedSeason.Id,
          Name: value.name,
          IsClassDeleted: this.IsClassDeleted,
          ClassCategory: null,
          ClassType: null,
          Description: '',
          Mode: this.savingMode,
          Schedule: {
            Id: (this.selectedSeason.Id === 0) ? 0 : this.selectedSeason.Schedule.Id,
            Occurrence: 'NONE',
            StartDate: this.geserateDateSubmitFormat(value.fromDate),
            EndDate: this.geserateDateSubmitFormat(value.toDate),
            SheduleItemList: null
          },
          IsCopyClass: this.IsCopyClass
        }
      };

      this.exceClassService.SaveClass(body).pipe(takeUntil(this.destroy$)).subscribe(
        res => {
          const ccResult = res.Data.split(' ');
          if (ccResult.length > 0 && Number(ccResult[0])) {
            body.ExcelineClass.Id = Number(ccResult[0]);
          }
          if (ccResult.length > 1) {
            body.ExcelineClass.Schedule.Id = Number(ccResult[1]);
          }

          this.editViewModal.close();
          // this.getClasses();
          PreloaderService.hidePreLoader();
        },
        err => {
          PreloaderService.hidePreLoader();
        }
      );
    } else {
      UsErrorService.validateAllFormFields(this.addSeasonForm, this.formErrors, this.validationMessages);
    }
  }

  private geserateDateSubmitFormat(date): string {
    return date.date.year + '-' + date.date.month + '-' + date.date.day;
  }


  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
    if (this.addNewClassModel) {
      this.addNewClassModel.close();
    }
    if (this.editViewModal) {
      this.editViewModal.close();
    }
    if (this.editViewModal) {
      this.editViewModal.close();
    }
    if (this.editedScheduleItemList.length > 0 || this.deletedScheduleItemIdList.length > 0 || this.newScheduleItemList.length > 0) {
      this.model = this.exceMessageService.openMessageBox('CONFIRM',
        {
          messageTitle: 'Confirm',
          messageBody: 'CLASS.TimeTableSaveConf',
          msgBoxId: 'SaveUnsaved'
        }
      );
    }
  }

}
