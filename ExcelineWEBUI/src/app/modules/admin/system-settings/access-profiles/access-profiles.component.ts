import { Component, OnInit, OnDestroy } from '@angular/core';
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
import { DataTableResource } from '../../../../shared/components/us-data-table/tools/data-table-resource';
import { AdminService } from 'app/modules/admin/services/admin.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';
import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { UsErrorService } from '../../../../shared/directives/us-error/us-error.service';
import { Dictionary } from 'lodash';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

const now = new Date();
@Component({
  selector: 'access-profiles',
  templateUrl: './access-profiles.component.html',
  styleUrls: ['./access-profiles.component.scss']
})
export class AccessProfilesComponent implements OnInit, OnDestroy {

  private popOver: any;
  private accessProfiles: any;
  private originalAccessProfiles?: any;
  private profile: any;
  private selectedAccessTime: any;
  private deletingAccessTime: any;
  private deletingItem: any;
  private model: any;
  private destroy$ = new Subject<void>();
  private itemResource: any;
  private itemCount = 0;
  accessProfileView: any;
  newCountryIdList?: any[] = [];
  newRegionIdList?: any[] = [];
  newBranchIdList?: any[] = [];
  objectKeys = Object.keys;
  loadBranchesSub: any;
  selectedGymBranchesList?: any[] = [];
  gymAvailableView: any;
  IsEdit = false;
  dayList?: any[] = [];
  warningModalDay: any;
  warningModalTime: any;
  warningModaltimes: any;
  selectedAccessProfile: any;
  accessProfilesList = [];
  isAddAccessProf: any;
  isEditAccessTime: any;
  regionList = [];
  branchList = [];
  countryList = [];
  public newAccessProfileForm: FormGroup;
  selectedGymBranchCount: number;
  selectedRegionCount: number;
  selectedCountryCount: number;
  myFormSubmited = false;

  formErrors = {
    'name': ''
  };
  validationMessages = {
    'name': {
      'required': 'ADMIN.PleaseSpecify'
    }
  };

  constructor(
    private modalService: UsbModal,
    private adminService: AdminService,
    private fb: FormBuilder,
    private exceMessageService: ExceMessageService
  ) { }

  ngOnInit() {
    this.getAccessProfiles();
    this.newAccessProfileForm = this.fb.group({
      'name': [null],
      // 'isGenderMale': [null],
      // 'isGenderFemale': [null],
      'Gender': [null],
      'isMonday': [false],
      'isTuesday': [false],
      'isWednesday': [false],
      'isThursday': [false],
      'isFriday': [false],
      'isSaturday': [false],
      'isSunday': [false],
      'fromTime': [null],
      'toTime': [null]
    });

    this.newAccessProfileForm.statusChanges.pipe(
      takeUntil(this.destroy$)
    ).pipe(takeUntil(this.destroy$)).subscribe(_ => {
      UsErrorService.onValueChanged(this.newAccessProfileForm, this.formErrors, this.validationMessages)
    }, null, null);
    // this.loadBranches();
  }

  getAccessProfiles() {
    this.accessProfilesList = [];
    this.adminService.GetAccessProfiles().pipe(
      takeUntil(this.destroy$)
    ).pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.accessProfiles = result.Data;
        this.originalAccessProfiles = result.Data;
        this.accessProfiles.forEach(element => {
          this.profile = null;
          this.profile = element;

          this.profile.AccessTimeList.forEach(item => {

            item.AccessProfileName = this.profile.AccessProfileName;
            item.AccessProfileId = this.profile.Id;

            if (item.Monday) {
              this.profile.isMonday = true;
            }
            if (item.Tuesday) {
              this.profile.isTuesday = true;
            }
            if (item.Wednesday) {
              this.profile.isWednesday = true;
            }
            if (item.Thursday) {
              this.profile.isThursday = true;
            }
            if (item.Friday) {
              this.profile.isFriday = true;
            }
            if (item.Saturday) {
              this.profile.isSaturday = true;
            }
            if (item.Sunday) {
              this.profile.isSunday = true;
            }
          });
          this.accessProfilesList.push(this.profile);
        });
      } else {
      }
    },
      error => {
      }, () => {

      });


    this.exceMessageService.yesCalledHandle.pipe(
      takeUntil(this.destroy$)
    ).pipe(takeUntil(this.destroy$)).subscribe((value) => {
      if (value.id === 'DELETE_ACCESS_TIME') {
        this.deletAccessTimeHandeler();
      } else if (value.id === 'DELETE_ACCESS_PROFILE') {
        this.deleteAccessProfile();
      }
    }, null, null);
  }

  openNewAccessProfileModel(content) {
    this.newAccessProfileForm.reset();
    this.selectedGymBranchCount = 0;
    this.selectedRegionCount = 0;
    this.selectedCountryCount = 0;
    this.isAddAccessProf = true;
    this.modalService.open(content, { width: '1150' });
    this.countryList = null;
    this.regionList = null;
    this.branchList = null;
    this.selectedAccessProfile = null;
    this.newAccessProfileForm.reset();
  }

  // for Edit the Access profile
  openViewAccessProfileModel(content, viewdProfile, isDelete) {
    this.isAddAccessProf = false;
    this.isEditAccessTime = false;
    this.selectedAccessTime = null;
    this.newAccessProfileForm.reset();
    this.selectedAccessProfile = this.originalAccessProfiles.find(data1 => data1.Id === viewdProfile.Id);
    this.selectedAccessProfile.AccessTimeList.forEach(item => {
      item.FromTime = new Date(item.FromTime);
      item.FromTime.setYear(now.getFullYear());
      item.ToTime = new Date(item.ToTime);
      item.ToTime.setYear(now.getFullYear());
    });
    if (this.selectedAccessProfile !== null) {
      this.selectedGymBranchCount = this.selectedAccessProfile.BranchIdList.length;
      // tslint:disable-next-line:forin
      for (let key in this.selectedAccessProfile.RegionIdList) {
        const value = this.selectedAccessProfile.RegionIdList[key];
        this.selectedRegionCount = value;
      }
      // tslint:disable-next-line:forin
      for (let key in this.selectedAccessProfile.CountryIdList) {
        const value = this.selectedAccessProfile.CountryIdList[key];
        if (value == 0) {
          this.selectedCountryCount = 0;
        } else {
          this.selectedCountryCount = 1;
        }
      }
    }

    if (isDelete) {
      this.selectedAccessTime.ActiveStatus = false;
      // this.SaveAccessProfile();
    } {
      this.newAccessProfileForm.patchValue({
        name: this.selectedAccessProfile.AccessProfileName,
      });
      this.accessProfileView = this.modalService.open(content, { width: '1150' });
    }
  }

  closeAccessProfileView() {
    this.accessProfileView.close();
    this.newAccessProfileForm.reset();
    this.selectedGymBranchCount = 0;
    this.selectedRegionCount = 0;
    this.selectedCountryCount = 0;
    this.selectedGymBranchesList = [];
  }

  editAccessProfileTime(accessTime) {
    this.isEditAccessTime = true;
    this.selectedAccessTime = accessTime;
    this.newAccessProfileForm.patchValue({
      isMonday: this.selectedAccessTime.Monday,
      isTuesday: this.selectedAccessTime.Tuesday,
      isWednesday: this.selectedAccessTime.Wednesday,
      isThursday: this.selectedAccessTime.Thursday,
      isFriday: this.selectedAccessTime.Friday,
      isSaturday: this.selectedAccessTime.Saturday,
      isSunday: this.selectedAccessTime.Sunday,
      fromTime: moment(this.selectedAccessTime.FromTime).format('HH:mm'),
      toTime: moment(this.selectedAccessTime.ToTime).format('HH:mm')
    });
  }

  rowClick(rowEvent) {
  }

  // get regions available
  loadRegions() {
    this.adminService.getRegions('NO', -1).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.regionList = result.Data;
        if (this.selectedAccessProfile.RegionIdList !== null) {
          // tslint:disable-next-line:forin
          for (let key in this.selectedAccessProfile.RegionIdList) {
            const value = this.selectedAccessProfile.RegionIdList[key];
            this.newRegionIdList = [];
            this.regionList.forEach(element => {
              if (value == element.Id) {
                element.IsCheck = true;
                this.newRegionIdList.push(element);
              }
            });
          }
        }
      }
    }, null, null);
  }

  // get gyms available
  loadBranches() {
    this.loadBranchesSub = this.adminService.getBranches(-1).pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
      if (result) {
        this.branchList = result.Data;
        if (this.selectedGymBranchesList) {
        }
        this.selectedGymBranchesList.forEach(element => {
        });
        if (this.selectedAccessProfile !== null) {
          if (this.selectedAccessProfile.BranchIdList !== null) {
            // tslint:disable-next-line:forin
            for (let key in this.selectedAccessProfile.BranchIdList) {
              const value = this.selectedAccessProfile.BranchIdList[key];
              this.newBranchIdList = [];
              this.branchList.forEach(element => {
                if (value == element.Id) {
                  element.IsCheck = true;
                  this.newBranchIdList.push(element);
                }
              });
            }
          }
        }
      }
    }, null, null);
  }

  // get countries available
  loadCountries() {
    this.adminService.getCategoriesByType('COUNTRY').pipe(
      takeUntil(this.destroy$)
    ).pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.countryList = result.Data;
        // tslint:disable-next-line:forin
        for (let key in this.selectedAccessProfile.CountryIdList) {
          const value = this.selectedAccessProfile.CountryIdList[key];
          this.newCountryIdList = [];
          this.countryList.forEach(element => {
            if (value == element.Code) {
              element.IsCheck = true;
              this.newCountryIdList.push(element);
            }
          });
        }
      }
    }, null, null);
  }

  // get change event for gym available
  getSelectedBranches(branch, event) {
    if (branch) {
      if (event.target.checked === true) {
        this.selectedGymBranchesList.push(branch);
      }
      if (event.target.checked === false) {
        this.selectedGymBranchesList.forEach(element => {
          if (element.Id === branch.Id) {
            this.selectedGymBranchesList.splice(branch, 1);
          }
        });
      }
    }
    this.selectedGymBranchCount = this.selectedGymBranchesList.length;
  }

  updateAccessProfileTime() {
  }


  addAccessProfileTime() {
    const index: number = this.selectedAccessProfile.AccessTimeList.indexOf(this.selectedAccessTime);
    if (index !== -1) {
      this.selectedAccessProfile.AccessTimeList.splice(index, 1);
    }
    const _accessTime = {
      Id: (this.isEditAccessTime === true) ? this.selectedAccessTime.Id : -1,
      Monday: this.newAccessProfileForm.value.isMonday,
      Tuesday: this.newAccessProfileForm.value.isTuesday,
      Wednesday: this.newAccessProfileForm.value.isWednesday,
      Thursday: this.newAccessProfileForm.value.isThursday,
      Friday: this.newAccessProfileForm.value.isFriday,
      Saturday: this.newAccessProfileForm.value.isSaturday,
      Sunday: this.newAccessProfileForm.value.isSunday,
      FromTime: '2017-01-01T' + this.newAccessProfileForm.value.fromTime + ':00',
      ToTime: '2017-01-01T' + this.newAccessProfileForm.value.toTime + ':00'
    }

    this.dayList = [];

    if (_accessTime.Monday === true) {
      this.dayList.push('Monday');
    }
    if (_accessTime.Tuesday === true) {
      this.dayList.push('Tuesday');
    }
    if (_accessTime.Wednesday === true) {
      this.dayList.push('Wednesday');
    }
    if (_accessTime.Thursday === true) {
      this.dayList.push('Thursday');
    }
    if (_accessTime.Friday === true) {
      this.dayList.push('Friday');
    }
    if (_accessTime.Saturday === true) {
      this.dayList.push('Saturday');
    }
    if (_accessTime.Sunday === true) {
      this.dayList.push('Sunday');
    }

    if (this.newAccessProfileForm.value.fromTime == null || this.newAccessProfileForm.value.toTime == null) {
      this.warningModalTime = this.exceMessageService.openMessageBox('WARNING', { messageTitle: 'Exceline', messageBody: 'ADMIN.MsgTimeDuration' });
    } else if (this.newAccessProfileForm.value.fromTime > this.newAccessProfileForm.value.toTime) {
      this.warningModaltimes = this.exceMessageService.openMessageBox('WARNING', { messageTitle: 'Exceline', messageBody: 'ADMIN.EndTimeShouldBeGreaterThenStartTime' });
    } else if (this.dayList.length === 0) {
      this.warningModalDay = this.exceMessageService.openMessageBox('WARNING', { messageTitle: 'Exceline', messageBody: 'Please check atleast one day.' });
    } else {
      if (this.selectedAccessProfile !== null) {
        this.selectedAccessProfile.AccessTimeList.push(_accessTime);
        this.newAccessProfileForm.controls['isMonday'].setValue(false);
        this.newAccessProfileForm.controls['isTuesday'].setValue(false);
        this.newAccessProfileForm.controls['isWednesday'].setValue(false);
        this.newAccessProfileForm.controls['isThursday'].setValue(false);
        this.newAccessProfileForm.controls['isFriday'].setValue(false);
        this.newAccessProfileForm.controls['isSaturday'].setValue(false);
        this.newAccessProfileForm.controls['isSunday'].setValue(false);
        this.newAccessProfileForm.controls['fromTime'].setValue(false);
        this.newAccessProfileForm.controls['toTime'].setValue(false);
        this.isEditAccessTime = false;
      }
    }

  }

  deletAccessTime(item) {
    this.deletingAccessTime = item;
    this.openExceDeleteMessage('CONFIRM', 'COMMON.AddCategoryConfirmDelete')
  }

  openExceDeleteMessage(title, body) {
    this.model = this.exceMessageService.openMessageBox('CONFIRM',
      {
        messageTitle: 'COMMON.ConfirmS',
        messageBody: body,
        msgBoxId: 'DELETE_ACCESS_TIME'
      });
  }

  deletAccessTimeHandeler() {
    if (this.selectedAccessProfile !== null) {
      this.selectedAccessProfile.AccessTimeList = this.selectedAccessProfile.AccessTimeList.filter(item => item !== this.deletingAccessTime);
    }
  }

  changeGymAvailble() {
    this.selectedGymBranchCount = (this.branchList.filter(data => data.IsCheck === true)).length;
  }

  changeRegionAvailble() {
    this.selectedRegionCount = (this.regionList.filter(data => data.IsCheck === true)).length;
  }

  changeCountryAvailble() {
    this.selectedCountryCount = (this.countryList.filter(data => data.IsCheck === true)).length;
  }

  // for view employees
  viewLoad(popOver) {
    if (this.popOver) {
      this.popOver.close();
    }
    this.popOver = popOver;
    this.popOver.open();
  }

  closePopOver() {
    this.popOver.close();
  }

  SaveAccessProfile(value) {
    if (value.name !== null) {
      value.name = value.name.trim();
    }
    if (value.name == null || value.name === '') {
      this.newAccessProfileForm.controls['name'].setErrors({ 'required': true });
    } else {
      this.myFormSubmited = false;
    }
    this.selectedAccessProfile.AccessTimeList.forEach(element => {
      const sTime = new Date(element.FromTime);
    });
    // this.selectedAccessProfile.BranchIdList = (this.branchList.filter(data => data.IsCheck === true)).map(item => item.Id);
    // this.selectedAccessProfile.RegionIdList = (this.regionList.filter(data => data.IsCheck === true)).map(item => item.Id);
    // this.selectedAccessProfile.CountryIdList = (this.countryList.filter(data => data.IsCheck === true)).map(item => item.Id);

    // this.myFormSubmited = true;
    // if (this.newAccessProfileForm.valid) {
    //   this.adminService.updateSystemAccessProfiles(this.selectedAccessProfile).pipe(takeUntil(this.destroy$)).subscribe(result => {
    //     this.myFormSubmited = false;
    //     this.closeAccessProfileView();
    //   },
    //     error => {
    //     });
    // } else {
    //   UsErrorService.validateAllFormFields(this.newAccessProfileForm, this.formErrors, this.validationMessages);
    // }
  }

  deletAccessProfile(item) {
    this.deletingItem = item;
    this.openExceDeleteProfileMessage('CONFIRM', 'COMMON.AddCategoryConfirmDelete');
  }

  openExceDeleteProfileMessage(title, body) {
    this.model = this.exceMessageService.openMessageBox('CONFIRM',
      {
        messageTitle: 'COMMON.ConfirmS',
        messageBody: body,
        msgBoxId: 'DELETE_ACCESS_PROFILE'
      });
  }

  deleteAccessProfile() {
    if (this.deletingItem) {
      this.deletingItem.IsActive = false
      this.deletingItem.IsDeleted = true
      // this.deletingItem.IsEdited = true
      this.adminService.updateSystemAccessProfiles(this.deletingItem).pipe(
        takeUntil(this.destroy$)
      ).pipe(takeUntil(this.destroy$)).subscribe(result => {
        this.getAccessProfiles();
      }, null, null);
    }
  }

  getGenderChange(gender) {
  }

  getSelectedRegions(region) {
  }

  getSelectedCountries(country) {
  }

  ngOnDestroy(): void {
    if (this.accessProfileView) {
      this.accessProfileView.close();
    }
    this.destroy$.next();
    this.destroy$.complete();
  }

}
