import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessProfilesComponent } from './access-profiles.component';

describe('AccessProfilesComponent', () => {
  let component: AccessProfilesComponent;
  let fixture: ComponentFixture<AccessProfilesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccessProfilesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccessProfilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
