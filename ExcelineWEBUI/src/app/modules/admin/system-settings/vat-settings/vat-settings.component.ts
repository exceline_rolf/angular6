import { Component, OnInit, OnDestroy } from '@angular/core';
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
import { DataTableResource } from '../../../../shared/components/us-data-table/tools/data-table-resource';
import { AdminService } from '../../services/admin.service';
import { Observable } from 'rxjs';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn, AbstractControl } from '@angular/forms';
import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common/src/pipes';
import { DataTableFilterPipe } from '../../../../shared/components/us-data-table/tools/data-table-filter-pipe';
import { TranslateService } from '@ngx-translate/core';
import { TreeNode } from 'app/shared/components/us-tree-table/tree-node';
import { UsErrorService } from '../../../../shared/directives/us-error/us-error.service';
import { IMyDpOptions } from 'app/shared/components/us-date-picker/interfaces';
import * as moment from 'moment';
import { isNull } from 'util';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

const now = new Date();

@Component({
  selector: 'vat-settings',
  templateUrl: './vat-settings.component.html',
  styleUrls: ['./vat-settings.component.scss']
})

export class VatSettingsComponent implements OnInit, OnDestroy {
  private vatSettings: any;
  private revenueList: any;
  private selectedVatCode: any;
  private items = [];
  private vatDeleteModel;
  private isUpdateVatVersion: any;
  private revenueItems = [];
  private revenueItemsSource: DataTableResource<any[]>;
  private vatCodeList = [];
  private uniqueVatCodeList = [];
  private editVatdetails: any;
  private revenueAccount: any;
  private selectedRevAccount: any;
  private isNewVatCode: any = true;
  private isViewVatCodeList: any = true;
  private revAccountModelRef?: any;
  private vatAccntDetailsModelRef?: any;
  private filteredAccounts: any;
  private filteredAccountsbyName: any;
  private selectedVatVersion: any;
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  private selectedVal: any;
  private myFormSubmited: boolean;
  private isDatesValid = true;
  private vatCodeStartDate: any
  private vatCodeEndDate: any
  private destroy$ = new Subject<void>();

  public selectRevAccountForm: FormGroup;
  public newVatCodeVersionForm: FormGroup;
  public files: TreeNode[] = [];

  formErrors = {
    'StartDate': '',
    'EndDate': '',
    'VatCode': ''
  };

  validationMessages = {
    'StartDate': {
      'required': 'ADMIN.PleaseSpecify',
      'dateRange': 'ADMIN.DateOverlap'
    },
    'EndDate': {
      'required': 'ADMIN.PleaseSpecify',
      'dateRange': 'ADMIN.DateOverlap'
    },
    'VatCode': {
      'required': 'Select Value'
    }
  };

  private fromDatePickerOptions: IMyDpOptions = {
    disableUntil: { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() - 1 },
  };

  private toDatePickerOptions: IMyDpOptions = {
    disableUntil: { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() - 1 }
  };

  constructor(
    private modalService: UsbModal,
    private adminService: AdminService,
    private fb: FormBuilder,
    private router: Router,
    private translateService: TranslateService,
    private exceMessageService: ExceMessageService
  ) {
    this.exceMessageService.yesCalledHandle.pipe(
      takeUntil(this.destroy$)
    ).subscribe((value) => {
      this.yesHandler();
    }, null, null);
    this.exceMessageService.noCalledHandle.pipe(
      takeUntil(this.destroy$)
    ).subscribe((value) => {
      this.noHandler();
    }, null, null);
    this.exceMessageService.okCalledHandle.pipe(
      takeUntil(this.destroy$)
    ).subscribe((value) => {
      this.okHandler();
    }, null, null);
  }

  dateValidator(control: AbstractControl) {
    return new Promise(resolve => {
      setTimeout(() => {
        if (!this.isDatesValid) {
          resolve({
            'dateRange': true
          })
        } else {
          resolve(null);
        }
      }, 1);
    })
  }


  ngOnInit() {
    this.newVatCodeVersionForm = this.fb.group({
      'Description': [null],
      'EndDate': [null, [Validators.required], [this.dateValidator.bind(this)]],
      'FormattedName': [null],
      'Id': [null],
      'IsActive': [null],
      'IsCurrentVersion': [null],
      'Name': [{ value: null, disabled: true }],
      'Rate': [null],
      'RegisteredDate': [null],
      'RegisteredUser': [null],
      'StartDate': [null, [Validators.required], [this.dateValidator.bind(this)]],
      'Status': [null],
      'VatCode': [null, [Validators.required]],
      'VersionId': [null],
      // -------more  details---------
      'VatAccountNo': [{ value: null, disabled: true }],
      'VatAccountName': [{ value: null, disabled: true }],
      'VevAccountId': [null],
      'VatCodeNew': [null],
      'VatAccount': {}
    })

    this.selectRevAccountForm = this.fb.group({
      'AccountNo': [null],
      'AccountName': [null],
      'Id': [null]
    })

    this.newVatCodeVersionForm.statusChanges.pipe(
      takeUntil(this.destroy$)
    ).subscribe(_ => {
      UsErrorService.onValueChanged(this.newVatCodeVersionForm, this.formErrors, this.validationMessages)
    }, null, null);
    this.getVATSettings();
  }

  ngOnDestroy() {
    if (this.vatAccntDetailsModelRef) {
      this.vatAccntDetailsModelRef.close();
    }
    if (this.revAccountModelRef) {
      this.revAccountModelRef.close();
    }
    this.destroy$.next();
    this.destroy$.complete();
  }

  getVATSettings() {
    this.files = [];
    this.adminService.GetGymCompanySettings('VAT').pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.vatSettings = result.Data;
        this.items = this.vatSettings;
        // tslint:disable-next-line:prefer-const
        let sortedArr = (this.vatSettings.reduce(function (res, current) {
          res[current.VatCode] = res[current.VatCode] || [];
          res[current.VatCode].push(current);
          return res;
        }, {}));

        this.loadUniqueVatCodes();
        // tslint:disable-next-line:prefer-const
        let files = []
        // tslint:disable-next-line:prefer-const
        // tslint:disable-next-line:forin
        for (let key in sortedArr) {


          let children = []
          const constArr = sortedArr[key]

          constArr.forEach(element => {
            children.push({ 'data': element })
          });
          this.files.push({
            'data': { 'VatCode': key },
            'children': children
          });

        }
      }
    }, null, null);
  }

  // this segment for find unique vat code list
  loadUniqueVatCodes() {
    const unique = {};
    this.uniqueVatCodeList = [];
    for (let i in this.vatSettings) {
      if (typeof (unique[this.vatSettings[i].Id]) == 'undefined') {
        this.uniqueVatCodeList.push(this.vatSettings[i]);
      }
      unique[this.vatSettings[i].Id] = 0;
    }
  }


  openDetailsVatCodeModel(content: any) {
    this.isUpdateVatVersion = true;
    this.isNewVatCode = false;
    this.vatAccntDetailsModelRef = this.modalService.open(content, { width: '1000' });
  }

  openNewVatCodeModel(content: any) {
    this.myFormSubmited = false;
    this.isDatesValid = true;
    this.vatCodeStartDate = null
    this.vatCodeEndDate = null
    this.newVatCodeVersionForm.reset()
    this.newVatCodeVersionForm.value.VatCode = null;
    this.vatAccntDetailsModelRef = this.modalService.open(content, { width: '1000' });
    this.isUpdateVatVersion = false;
    this.isNewVatCode = true;
  }

  openRevAccountModel(content) {
    this.adminService.GetRevenueAccounts().pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.revenueList = result.Data;
        this.revenueItems = this.revenueList;
      } else {
      }
    }, null, null);
    this.revAccountModelRef = this.modalService.open(content, { width: '1000' });
  }

  reloadItems(params) {
  }

  rowClick(rowEvent, isItem?) {
    if (isItem) {
      this.editVatdetails = rowEvent;
      this.selectedVatVersion = rowEvent;
    } else {
      this.editVatdetails = rowEvent.node.data;
      this.selectedVatVersion = rowEvent.node.data;
    }
    this.patchVatCodeDetails(this.editVatdetails);
  }

  editRevAccountGrid(content) {
  }

  setNewVatCodeView() {
    this.newVatCodeVersionForm.reset();
    this.isNewVatCode = false;
    this.isViewVatCodeList = false;
    this.newVatCodeVersionForm.controls['Name'].enable();
  }
  setUpdateVatCodeView() {
    this.newVatCodeVersionForm.controls['Name'].reset();
    this.newVatCodeVersionForm.controls['Name'].disable();
    this.isNewVatCode = true;
    this.isViewVatCodeList = true;
  }

  rowClickAccount(rowEvent) {
    this.revenueAccount = rowEvent.row.item;
    this.selectedRevAccount = this.revenueAccount;
    this.selectRevAccountForm.patchValue({
      AccountNo: this.selectedRevAccount.AccountNo,
      AccountName: this.selectedRevAccount.Name,
      Id: this.selectedRevAccount.ID,
    });
  }

  rowDoubleClick(rowEvent) {
    this.editVatdetails = rowEvent.row.item;
    this.patchVatCodeDetails(this.editVatdetails);
  }

  closeRevAccountForm() {
    this.revAccountModelRef.close();
  }

  revAccountSelected() {
    this.revAccountModelRef.close();
    this.newVatCodeVersionForm.patchValue({
      VatAccount: this.revenueAccount,
      VatAccountNo: this.revenueAccount.AccountNo,
      VatAccountName: this.revenueAccount.Name,
      RevAccountId: this.revenueAccount.ID
    });
  }

  closeVatDetailsForm() {
    this.newVatCodeVersionForm.reset();

    this.myFormSubmited = false;
    this.isDatesValid = true;
    this.vatCodeStartDate = null
    this.vatCodeEndDate = null
    this.newVatCodeVersionForm.controls['Name'].reset();
    this.vatAccntDetailsModelRef.close();
  }

  rowTooltip(item) { return item.Name; }

  patchVatCodeDetails(vatdetails: any) {

    this.selectedVatCode = vatdetails.VatCode;
    const sDate = new Date(vatdetails.StartDate);
    const eDate = new Date(vatdetails.EndDate);
    this.newVatCodeVersionForm.patchValue({
      Description: vatdetails.Description,
      EndDate: {
        date: {
          year: eDate.getFullYear(),
          month: eDate.getMonth() + 1,
          day: eDate.getDate()
        }
      },
      FormattedName: vatdetails.FormattedName,
      Id: vatdetails.Id,
      IsActive: true,
      IsCurrentVersion: vatdetails.IsCurrentVersion,
      Name: vatdetails.Name,
      Rate: vatdetails.Rate,
      RegisteredDate: vatdetails.RegisteredDate,
      RegisteredUser: vatdetails.RegisteredUser,
      StartDate: {
        date: {
          year: sDate.getFullYear(),
          month: sDate.getMonth() + 1,
          day: sDate.getDate()
        }
      },
      Status: vatdetails.Status,
      VatAccount: vatdetails.VatAccount,
      VatCode: vatdetails.VatCode,
      VersionId: vatdetails.VersionId,
      VatAccountNo: vatdetails.VatAccount.AccountNo,
      VatAccountName: vatdetails.VatAccount.Name,
    });
  }

  cancelUpdateRevAccount() {
    this.selectRevAccountForm.reset();
  }

  filterInput(accNameValue?: any, accNoValue?: any) {
    this.filteredAccountsbyName = this.revenueList;
    this.filteredAccountsbyName = this.filterpipe.transform('Name', 'NAMEFILTER', this.revenueList, accNameValue);
    this.filteredAccounts = this.filteredAccountsbyName;
    this.filteredAccounts = this.filterpipe.transform('AccountNo', 'NAMEFILTER', this.filteredAccountsbyName, accNoValue);
    this.revenueItemsSource = new DataTableResource(this.filteredAccounts);
    this.reloadRevAccItems(this.filteredAccounts);
  }

  reloadRevAccItems(params) {
    if (this.revenueItemsSource) {
      this.revenueItemsSource.query(params).then(items => this.revenueItems = items);
    }
  }

  // Disable invalid dates and open end date picker
  onStartDateChange(date, end) {
    this.toDatePickerOptions = {
      disableUntil: date.date
    };
    end.showSelector = true;
    this.vatCodeStartDate = date.jsdate;
    this.vatValidation();
  }

  onEndDateChange(date, end) {
    this.vatCodeEndDate = date.jsdate;
    this.vatValidation();
  }

  vatValidation() {
    this.myFormSubmited = true;
    this.newVatCodeVersionForm.value.Id = 0;
    this.newVatCodeVersionForm.value.VersionId = 0;
    this.newVatCodeVersionForm.value.IsActive = 1;

    let selectedVatCode = this.newVatCodeVersionForm.get("VatCode").value
    if (this.vatCodeEndDate && this.vatCodeStartDate) {

      for (var i in this.vatSettings) {

        if (this.vatSettings[i].VatCode == selectedVatCode) {

          this.vatSettings[i].StartDate = new Date(this.vatSettings[i].StartDate)
          this.vatSettings[i].EndDate = new Date(this.vatSettings[i].EndDate)

          if ((moment(this.vatCodeStartDate).isSameOrBefore(this.vatSettings[i].StartDate, 'day') && moment(this.vatCodeEndDate).isSameOrAfter(this.vatSettings[i].StartDate, 'day')) ||
            (moment(this.vatCodeStartDate).isSameOrBefore(this.vatSettings[i].EndDate, 'day') && moment(this.vatCodeEndDate).isSameOrAfter(this.vatSettings[i].EndDate, 'day')) ||
            (moment(this.vatCodeStartDate).isSameOrBefore(this.vatSettings[i].StartDate, 'day') && moment(this.vatCodeEndDate).isSameOrAfter(this.vatSettings[i].EndDate, 'day'))) {
            this.isDatesValid = false
            return;
          } else {
            this.isDatesValid = true;
          }
        }
      }
    }
  }

  preAddNewVatCodeVersion() {
    this.myFormSubmited = true;
    this.newVatCodeVersionForm.value.IsActive = true;

    if (this.vatCodeEndDate && this.vatCodeStartDate && this.isDatesValid) {
      this.newVatCodeVersionForm.value.EndDate =
        this.newVatCodeVersionForm.value.EndDate.date.year + '/' +
        this.newVatCodeVersionForm.value.EndDate.date.month + '/' +
        this.newVatCodeVersionForm.value.EndDate.date.day;
      this.newVatCodeVersionForm.value.StartDate =
        this.newVatCodeVersionForm.value.StartDate.date.year + '/' +
        this.newVatCodeVersionForm.value.StartDate.date.month + '/' +
        this.newVatCodeVersionForm.value.StartDate.date.day;
      this.newVatCodeVersionForm.value.Name = this.newVatCodeVersionForm.getRawValue().Name;

      if (!this.isViewVatCodeList) {
        this.newVatCodeVersionForm.value.VatCode = this.newVatCodeVersionForm.value.VatCodeNew;
      }

      this.adminService.saveGymCompanyVATSettings(this.newVatCodeVersionForm.value).pipe(
        takeUntil(this.destroy$)
      ).subscribe(result => {
        if (result.Data) {
          this.getVATSettings();
          this.vatAccntDetailsModelRef.close()
          this.newVatCodeVersionForm.reset()
        }
      }, null, null);
    }
  }


  updateVatVersion() {
    let vatId: any;
    const vatCode = this.editVatdetails.VatCode;
    for (let key in this.newVatCodeVersionForm.value) {
      if (this.editVatdetails[key] || this.editVatdetails[key] === 0) {
        this.editVatdetails[key] = this.newVatCodeVersionForm.value[key]
      }
    }
    this.editVatdetails.VatCode = vatCode
    this.editVatdetails.EndDate =
      this.newVatCodeVersionForm.value.EndDate.date.year + '/' +
      this.newVatCodeVersionForm.value.EndDate.date.month + '/' +
      this.newVatCodeVersionForm.value.EndDate.date.day;
    this.editVatdetails.StartDate =
      this.newVatCodeVersionForm.value.StartDate.date.year + '/' +
      this.newVatCodeVersionForm.value.StartDate.date.month + '/' +
      this.newVatCodeVersionForm.value.StartDate.date.day;
    this.editVatdetails.IsActive = 1


    this.adminService.saveGymCompanyVATSettings(this.editVatdetails).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result.Data) {
        vatId = result.Data;
        this.getVATSettings();
        this.vatAccntDetailsModelRef.close();
      }
    }, null, null);
  }

  openVatDeleteMessage() {
    let confirmTitle = '';
    let confirmBody = '';
    (this.translateService.get('COMMON.Confirm').subscribe(tranlstedValue => confirmTitle = tranlstedValue));
    (this.translateService.get('ADMIN.MsgDeleteVatVersionConfirm').subscribe(tranlstedValue => confirmBody = tranlstedValue));
    this.vatDeleteModel = this.exceMessageService.openMessageBox('CONFIRM',
      {
        messageTitle: confirmTitle,
        messageBody: confirmBody
      }
    );
  }

  yesHandler() {
    let vatId = 0;
    if (this.editVatdetails) {
      this.editVatdetails.isActive = 0;
      this.adminService.saveGymCompanyVATSettings(this.editVatdetails).pipe(
        takeUntil(this.destroy$)
      ).subscribe(result => {
        if (result.Data) {
          vatId = result.Data;
          this.getVATSettings();
        }
      }, null, null);
    }

  }

  noHandler() {
  }

  okHandler() {
  }

  deleteVatCode(rowEvent) {
    this.openVatDeleteMessage();
  }

  // add new vatcode
  fillPopup() {
    let selectedVatCode = this.newVatCodeVersionForm.controls['VatCode'].value;
    let vatCodeDetail
    for (var i in this.uniqueVatCodeList) {
      if (this.uniqueVatCodeList[i].VatCode == selectedVatCode) {
        vatCodeDetail = this.uniqueVatCodeList[i];
      }
    }

    this.newVatCodeVersionForm.patchValue({
      Id: vatCodeDetail.Id,
      VersionId: vatCodeDetail.VersionId,
      VatCode: vatCodeDetail.VatCode,
      Name: vatCodeDetail.Name,
      Rate: vatCodeDetail.Rate,
      RegisteredUser: vatCodeDetail.RegisteredUser,
      RegisteredDate: vatCodeDetail.RegisteredDate,
      VatAccount: {
        ID: vatCodeDetail.VatAccount.ID,
        AccountNo: vatCodeDetail.VatAccount.AccountNo,
        Name: vatCodeDetail.VatAccount.Name
      },
      VatAccountNo: vatCodeDetail.VatAccount.AccountNo,
      VatAccountName: vatCodeDetail.VatAccount.Name,
      Description: vatCodeDetail.Description
    });
  }

}
