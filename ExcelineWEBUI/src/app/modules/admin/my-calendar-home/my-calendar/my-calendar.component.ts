import { Component, OnInit, Input } from '@angular/core';
import * as $ from 'jquery';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import * as moment from 'moment';
import * as _ from 'lodash';
import { TranslateService } from '@ngx-translate/core';
import { AdminService } from '../../services/admin.service';
import { GymEmployeeHomeService } from '../../manage-employees/manage-gym-employees/gym-employee-home/gym-employee-home.service';
import { PreloaderService } from '../../../../shared/services/preloader.service';

@Component({
  selector: 'app-my-calendar',
  templateUrl: './my-calendar.component.html',
  styleUrls: ['./my-calendar.component.scss']
})
export class MyCalendarComponent implements OnInit {

  followUpTaskList: any;
  jobTaskList: any;
  calendarFollowUpList: any[] = [];
  isOpenRightPanel: boolean;
  @Input() entityId: number;
  activeTimes: any;
  employeeId: any;
  events: any[] = [];
  resources: any[] = [];
  viewType: string;
  header: any;
  resourceLabelText: string;
  resourceAreaWidth: string;
  views: any;
  isShowTooltip: boolean;
  theame: boolean;
  self: this;
  scrollTime: string;
  today: any;
  scheduler: any;
  calandarDate: any;
  dateFilterForm: FormGroup;
  calandarDateRange: any;
  isShowTimeDuration: boolean;
  isShowEventContextMenu: boolean;
  isShowDayContextMenu: boolean;
  taskTypes: any[] = [];

  constructor(
    private adminService: AdminService,
    private gymEmployeeHomeService: GymEmployeeHomeService,
    private fb: FormBuilder,
    private translate: TranslateService

  ) {

    this.self = this;
    this.scrollTime = moment().format('hh:mm:ss');
    this.isOpenRightPanel = false;
  }

  ngOnInit() {

    this.taskTypes = [
      { id: -1, name: 'ADMIN.AllC' },
      { id: 0, name: 'ADMIN.JobC' },
      { id: 1, name: 'ADMIN.WorkC' },
      { id: 2, name: 'ADMIN.FollowupC' },
      { id: 3, name: 'ADMIN.ClassC' },
      { id: 4, name: 'ADMIN.BookingC' }
    ]

    this.isShowDayContextMenu = false;
    this.isShowEventContextMenu = false;
    this.isShowTooltip = false;
    this.isShowTimeDuration = false;
    this.calandarDate = new Date();
    const now = new Date();
    this.today = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
    this.dateFilterForm = this.fb.group({
      fromDate: [{ date: this.today }, [Validators.required]],
      toDate: [{ date: this.today }, [Validators.required]]
    });

    this.theame = false;
    this.viewType = 'agendaDay';

    this.header = {
      right: '',
      center: '',
      left: ''
    };

    this.resourceAreaWidth = '20%';
    this.resourceLabelText = 'Classes';

    this.views = {
      agendaDayWithoutResources: {
        type: 'agenda',
        duration: { days: 1 },
        groupByResource: false,
        groupByDateAndResource: false
      }
    };


    this.getActiveTimes();

    this.dateFilterForm.statusChanges.subscribe(__ => {
      this.getActiveTimes();
    });

    this.loadFollowupByEmployeeId();
    if (this.scheduler) {
      this.scheduler.refetchEvents();

    }
  }

  private loadFollowupByEmployeeId() {
  }

  private getActiveTimes() {
    PreloaderService.showPreLoader();
    const fromDate = this.dateFilterForm.value.fromDate.date.year + '-' +
      this.dateFilterForm.value.fromDate.date.month + '-' +
      this.dateFilterForm.value.fromDate.date.day;
    const toDate = this.dateFilterForm.value.toDate.date.year + '-' +
      this.dateFilterForm.value.toDate.date.month + '-' +
      this.dateFilterForm.value.toDate.date.day;
    this.adminService.GetEntityActiveTimes({
      StartDate: fromDate,
      EndDate: toDate,
      EntityList: [this.entityId],
      EntityRoleType: 'EMP'
    }).subscribe(activeTimes => {

      this.activeTimes = activeTimes.Data;
      this.activeTimes.forEach(activeTime => {
        switch (activeTime.TaskType) {
          case 0:
            activeTime.DisplayName = activeTime.Text;
            activeTime.Color = '#2196F3';
            break;
          case 1:
            activeTime.DisplayName = activeTime.ResName;
            activeTime.Color = '#FFEB3B';
            break;
          case 2:
            activeTime.DisplayName = activeTime.Text;
            activeTime.Color = '#9E9E9E';
            break;
          case 3:
            activeTime.DisplayName = activeTime.ClassType;
            activeTime.Color = '#00E676';
            break;
          case 4:
            activeTime.DisplayName = activeTime.ResName;
            activeTime.Color = '#9C27B0';
            break;
        }
      });

      this.gymEmployeeHomeService.$activeTimes = this.activeTimes;
      this.createClassResources();

      PreloaderService.hidePreLoader();
    }, err => {
      PreloaderService.hidePreLoader();
    });
  }

  private createClassResources() {
    this.events = [];
    this.resources = [];

    if (this.gymEmployeeHomeService.$activeTimes.length > 0 && this.gymEmployeeHomeService.$selectedGymEmployee) {
      // this.classActiveTimes = this.classHomeService.ClassActiveTimes;
      this.activeTimes.forEach((activeTimes, index) => {

        this.resources.push({
          id: this.gymEmployeeHomeService.$selectedGymEmployee.Id,
          activeTimesId: this.activeTimes.Id,
          title: this.gymEmployeeHomeService.$selectedGymEmployee.Name,
          eventColor: '#C133FF'
        });


        this.events.push({
          id: index,
          resourceId: activeTimes.SheduleItemId,
          activeTimesId: activeTimes.Id,
          start: activeTimes.StartDateTime,
          end: activeTimes.EndDateTime,
          title: activeTimes.DisplayName,
          color: activeTimes.Color,
          activeTimeDetail: activeTimes
        });

      });

    }
  }

  onButtonGroupClick($event) {
    const clickedElement = $event.target || $event.srcElement;
    if (clickedElement.nodeName === 'BUTTON') {

      const isCertainButtonAlreadyActive = clickedElement.parentElement.querySelector('.active');
      // if a Button already has Class: .active

      if (isCertainButtonAlreadyActive) {
        isCertainButtonAlreadyActive.classList.remove('active');
      }
      clickedElement.className += ' active';
    }
  }

  loadResourceView(scheduler) {
    scheduler.changeView('timelineDay');
  }

  selectDateRange(val, scheduler) {
    this.scheduler = scheduler;
    switch (val) {
      case 'TODAY':
        this.dateFilterForm.patchValue({
          fromDate: { date: this.today },
          toDate: { date: this.today }
        });
        scheduler.changeView('agendaDayWithoutResources');
        scheduler.today();
        this.generateTitle(scheduler);

        break;

      case 'DAYVIEW':
        this.dateFilterForm.patchValue({
          fromDate: { date: this.today },
          toDate: { date: this.today }
        });
        scheduler.changeView('agendaDayWithoutResources');
        scheduler.today();
        this.generateTitle(scheduler);

        break;
      case 'WEEKVIEW':

        const startOfWeek = moment().startOf('week').toDate();
        const endOfWeek = moment().endOf('week').toDate();

        this.dateFilterForm.patchValue({
          fromDate: { date: { year: startOfWeek.getFullYear(), month: startOfWeek.getMonth() + 1, day: startOfWeek.getDate() } },
          toDate: { date: { year: endOfWeek.getFullYear(), month: endOfWeek.getMonth() + 1, day: endOfWeek.getDate() } }
        });
        scheduler.changeView('agendaWeek', startOfWeek, endOfWeek);
        this.generateTitle(scheduler);

        break;
      case 'MONTHVIEW':
        const startOfMonth = moment().startOf('month').toDate();
        const endOfMonth = moment().endOf('month').toDate();

        this.dateFilterForm.patchValue({
          fromDate: { date: { year: startOfMonth.getFullYear(), month: startOfMonth.getMonth() + 1, day: startOfMonth.getDate() } },
          toDate: { date: { year: endOfMonth.getFullYear(), month: endOfMonth.getMonth() + 1, day: endOfMonth.getDate() } }
        });
        scheduler.changeView('month');
        this.generateTitle(scheduler);

        break;
      default:
        break;
    }

  }

  private generateTitle(scheduler: any) {
    if (scheduler.getView() === 'agendaWeek' || scheduler.getView() === 'month') {
      this.isShowTimeDuration = true;
      this.calandarDateRange = scheduler.getCalendarDateRange();
      const start = { year: this.calandarDateRange.start.getFullYear(), month: this.calandarDateRange.start.getMonth() + 1, day: this.calandarDateRange.start.getDate() };
      const end = { year: this.calandarDateRange.end.getFullYear(), month: this.calandarDateRange.end.getMonth() + 1, day: this.calandarDateRange.end.getDate() };
      this.dateFilterForm.patchValue({
        fromDate: { date: start },
        toDate: { date: end }
      });

    } else {
      this.calandarDate = scheduler.getDate();
      this.isShowTimeDuration = false;
      const date = new Date(scheduler.getDate());
      const newDate = { year: date.getFullYear(), month: date.getMonth() + 1, day: date.getDate() };
      this.dateFilterForm.patchValue({
        fromDate: { date: newDate },
        toDate: { date: newDate }
      });
    }
  }

  onDateChanged(event, scheduler) {
    scheduler.changeView('agendaDayWithoutResources');
    scheduler.gotoDate(event.jsdate);
    this.generateTitle(scheduler);

  }

  loadPriviousDay(scheduler) {
    scheduler.prev();
    this.generateTitle(scheduler);
  }

  loadNextDay(scheduler) {
    scheduler.next();
    this.generateTitle(scheduler);
  }

  hideContextMenu() {
    this.isShowDayContextMenu = false;
    this.isShowEventContextMenu = false;

  }

  dayRender(date, cell) {

  }

  onEventRightclick(event) {
    // this.selectedActiveTime = event.date.activeTimeDetail;
    // this.eventontextmenuX = event.jsEvent.pageX;
    // this.eventContextmenuY = event.jsEvent.pageY;
    this.isShowEventContextMenu = true;
    this.isShowDayContextMenu = false;
  }

  eventRender(this, event, element) {
    const self = this;
    // const scheduler = this.scheduler;
    // element.find('.fc-content').append(`<span class="badge badge-pill badge-light fc-exe-badge">`
    //   + event.activeTimeDetail.NumberOfMembers + `/` + event.activeTimeDetail.MaxNoOfBookings +
    //   `</span><span class="icon-calendar-changed text-light exc-cal-changed-icon" title="Changed calendar"></span>
    //   <i  class=\'icon-close float-right fc-close\' id=\'` + event.activeTimeDetail.Id + `\'></i>`);

  }



  mouseOver(event, scheduler) {
    const self = this;
    this.scheduler = scheduler;
    let instructerListHtml = '';
    let resourceLstHtml = '';
    if (event.calEvent.activeTimeDetail.InstructorList && event.calEvent.activeTimeDetail.InstructorList.length > 0) {
      instructerListHtml = '<ul class="m-0 p-0 fc-exc-list fc-exc-list-instructor">';
      event.calEvent.activeTimeDetail.InstructorList.forEach(ins => {
        instructerListHtml = instructerListHtml + '<li>' + ins.DisplayName + '</li>';
      });
      instructerListHtml = instructerListHtml + '</ul>';
    }
    if (event.calEvent.activeTimeDetail.ResourceLst && event.calEvent.activeTimeDetail.ResourceLst.length > 0) {
      resourceLstHtml = '<ul class="m-0 p-0 fc-exc-list fc-exc-list-location">';
      event.calEvent.activeTimeDetail.ResourceLst.forEach(ins => {
        resourceLstHtml = resourceLstHtml + '<li>' + ins.DisplayName + '</li>';
      });
      resourceLstHtml = resourceLstHtml + '</ul>';
    }

    let weektype = '';
    if (event.calEvent.activeTimeDetail.WeekType === 0) {
      weektype = 'EVEN';
    } else if (event.calEvent.activeTimeDetail.WeekType === 1) {
      weektype = 'ODD';
    } else if (event.calEvent.activeTimeDetail.WeekType === 2) {
      weektype = 'ALL';
    }
    let tooltip = '';
    let taskType = '';
    switch (event.calEvent.activeTimeDetail.TaskType) {
      case 0:
        taskType = 'JOB';
        tooltip =
          `<div class="event-tooltip">
        <div class="mb-1">
          <strong> ` + taskType + ` </strong>
        </div>
        <div><span class="text-muted">Day :</span>
          <span>` + moment(event.calEvent.start._d).format('dddd') + `</span>
          <span> | </span>
          <span>` + moment(event.calEvent.start._d).format('h:mm a') + `
          <span> - </span>` + moment(event.calEvent.end._d).format('h:mm a') + `
        </span>
        </div>
        <div><span class="text-muted">Week Type :</span>
          <span> ` + weektype + ` </span>
        </div>
        <div class="mt-1"><span class="text-muted">Start Date :</span>
          <span> ` + moment(event.calEvent.activeTimeDetail.StartDate).format('YYYY-MM-DD') + ` </span>
        </div>
        <div><span class="text-muted">End Date :</span>
        <span> ` + moment(event.calEvent.activeTimeDetail.EndDate).format('YYYY-MM-DD') + ` </span>
        </div>
      </div>`;

        break;
      case 1:
        taskType = 'WORK';
        tooltip =
          `<div class="event-tooltip">
        <div class="mb-1">
          <strong> ` + taskType + ` </strong>
        </div>
        <div><span class="text-muted">Day :</span>
          <span>` + moment(event.calEvent.start._d).format('dddd') + `</span>
          <span> | </span>
          <span>` + moment(event.calEvent.start._d).format('h:mm a') + `
          <span> - </span>` + moment(event.calEvent.end._d).format('h:mm a') + `
        </span>
        </div>
        <div><span class="text-muted">Week Type :</span>
          <span> ` + weektype + ` </span>
        </div>
        <div class="mt-1"><span class="text-muted">Start Date :</span>
          <span> ` + moment(event.calEvent.activeTimeDetail.StartDate).format('YYYY-MM-DD') + ` </span>
        </div>
        <div><span class="text-muted">End Date :</span>
        <span> ` + moment(event.calEvent.activeTimeDetail.EndDate).format('YYYY-MM-DD') + ` </span>
        </div>
      </div>`;
        break;
      case 2:
        taskType = 'FOLLOW-UP';
        tooltip =
          `<div class="event-tooltip">
        <div class="mb-1">
          <strong> ` + taskType + ` </strong>
        </div>
        <div><span class="text-muted">Day :</span>
          <span>` + moment(event.calEvent.start._d).format('dddd') + `</span>
          <span> | </span>
          <span>` + moment(event.calEvent.start._d).format('h:mm a') + `
          <span> - </span>` + moment(event.calEvent.end._d).format('h:mm a') + `
        </span>
        </div>
        <div><span class="text-muted">Week Type :</span>
          <span> ` + weektype + ` </span>
        </div>
        <div class="mt-1"><span class="text-muted">Start Date :</span>
          <span> ` + moment(event.calEvent.activeTimeDetail.StartDate).format('YYYY-MM-DD') + ` </span>
        </div>
        <div><span class="text-muted">End Date :</span>
        <span> ` + moment(event.calEvent.activeTimeDetail.EndDate).format('YYYY-MM-DD') + ` </span>
        </div>
      </div>`;
        break;
      case 3:
        taskType = 'CLASS';
        tooltip =
          `<div class="event-tooltip">
        <div class="mb-1">
          <strong> ` + taskType + ` </strong>
        </div>
        <div><span class="text-muted">Day :</span>
          <span>` + moment(event.calEvent.start._d).format('dddd') + `</span>
          <span> | </span>
          <span>` + moment(event.calEvent.start._d).format('h:mm a') + `
          <span> - </span>` + moment(event.calEvent.end._d).format('h:mm a') + `
        </span>
        </div>
        <div><span class="text-muted">Week Type :</span>
          <span> ` + weektype + ` </span>
        </div>
        <div class="mt-1"><span class="text-muted">Start Date :</span>
          <span> ` + moment(event.calEvent.activeTimeDetail.StartDate).format('YYYY-MM-DD') + ` </span>
        </div>
        <div><span class="text-muted">End Date :</span>
        <span> ` + moment(event.calEvent.activeTimeDetail.EndDate).format('YYYY-MM-DD') + ` </span>
        </div>
      </div>`;
        break;
      case 4:
        taskType = 'BOOKING';
        tooltip =
          `<div class="event-tooltip">
        <div class="mb-1">
          <strong> ` + taskType + ` </strong>
        </div>
        <div><span class="text-muted">Day :</span>
          <span>` + moment(event.calEvent.start._d).format('dddd') + `</span>
          <span> | </span>
          <span>` + moment(event.calEvent.start._d).format('h:mm a') + `
          <span> - </span>` + moment(event.calEvent.end._d).format('h:mm a') + `
        </span>
        </div>
        <div><span class="text-muted">Week Type :</span>
          <span> ` + weektype + ` </span>
        </div>
        <div class="mt-1"><span class="text-muted">Start Date :</span>
          <span> ` + moment(event.calEvent.activeTimeDetail.StartDate).format('YYYY-MM-DD') + ` </span>
        </div>
        <div><span class="text-muted">End Date :</span>
        <span> ` + moment(event.calEvent.activeTimeDetail.EndDate).format('YYYY-MM-DD') + ` </span>
        </div>
        <div class="dropdown-divider"></div>
        <div><span class="text-muted">Member No and Name :</span>
        </div>
        <div><span> ` + event.calEvent.activeTimeDetail.BookingMemberList[0].CustId +
          ` - ` + event.calEvent.activeTimeDetail.BookingMemberList[0].Name + `</span></div>
        <div class="dropdown-divider"></div>
        <div><span class="text-muted">Resource :</span>
        </div>
        <div><span> ` + event.calEvent.activeTimeDetail.ResName + ` </span></div>
      </div>`;

        break;
    }


    const $tooltip = $(tooltip).appendTo('body');
    let clientHeight = 0;
    let clientWidth = 0;
    $(event.jsEvent.currentTarget).mouseover(function (e) {
      $(event.jsEvent.currentTarget).css('z-index', 10000);
      $tooltip.fadeIn('500');
      $tooltip.fadeTo('10', 1.9);
      clientHeight = document.body.clientHeight;
      clientWidth = document.body.clientWidth;

    }).mousemove(function (e) {

      this.isShowTooltip = true;


      if ((e.pageX + $('.event-tooltip').width() + 70) > clientWidth) {
        this.tooltipX = e.pageX - $('.event-tooltip').width();
        $tooltip.css('left', e.pageX - ($('.event-tooltip').width() + 40));
      } else {
        this.tooltipX = e.pageX + 20;
        $tooltip.css('left', e.pageX + 20);
      }

      if ((e.pageY + $('.event-tooltip').height() + 30) > clientHeight) {
        this.tooltipY = e.pageY - ($('.event-tooltip').height() + 30);
        $tooltip.css('top', e.pageY - ($('.event-tooltip').height() + 30));

      } else {
        $tooltip.css('top', e.pageY + 10);
        this.tooltipY = e.pageY + 10;

      }



    });
  }

  mouseOut(event) {
    $(event.jsEvent.currentTarget).css('z-index', 8);
    $('.event-tooltip').remove();
  }

  openRightPanel() {
    this.isOpenRightPanel = !this.isOpenRightPanel;
  }



}
