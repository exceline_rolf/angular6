import { Component, OnInit } from '@angular/core';
import { GymEmployeeHomeService } from '../gym-employee-home.service';
import { AdminService } from '../../../../services/admin.service';
import { PreloaderService } from 'app/shared/services/preloader.service';

@Component({
  selector: 'app-employee-calendar',
  templateUrl: './employee-calendar.component.html',
  styleUrls: ['./employee-calendar.component.scss']
})
export class EmployeeCalendarComponent implements OnInit {

  EmployeeActiveTime: any;
  selectedEmployeeId: number;
  constructor(
    private adminService: AdminService,
    private gymEmployeeHomeService: GymEmployeeHomeService

  ) { }

  ngOnInit() {
    // TODO - remove hardcoded emp Id
    this.selectedEmployeeId = 30;
    // this.selectedEmployeeId = this.gymEmployeeHomeService.$selectedGymEmployee.Id;


  }



}
