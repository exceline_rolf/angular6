

import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { AdminService } from '../../../services/admin.service'
import { EmployeeBasicInfoService } from '../employee-basic-info/employee-basic-info.service';
import { ExceLoginService } from '../../../../../modules/login/exce-login/exce-login.service';
import { DataTableResource } from '../../../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from '../../../../../shared/components/us-data-table/tools/data-table-filter-pipe'
import { error } from 'util';
import { PreloaderService } from '../../../../../shared/services/preloader.service'
import { UsbModal } from 'app/shared/components/us-modal/us-modal';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators'

@Component({
  selector: 'app-employee-info-bookings',
  templateUrl: './employee-info-bookings.component.html',
  styleUrls: ['./employee-info-bookings.component.scss']
})

export class EmployeeInfoBookingsComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private followUpCategories: any
  private filteredBookings = []
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  private branchId = 1;
  private itemResource: any = 0;

  private bookingItems = []
  public items = [];
  public Resource: any;
  public rowTooltip: any;
  public itemCount = 0;
  public branches = []
  selectedItem: any;
  addNewTimeView
  popOver
  empList
  public isThumbsUp = false;
  currentEmployee
  // @ViewChild('followUpProfile') public followUpProfile;
  // @ViewChild('updateMember') public updateMember;

  constructor(
    private employeeBasicInfoService: EmployeeBasicInfoService,
    private adminService: AdminService,
    private exceLoginService: ExceLoginService,
    private modalService: UsbModal
  ) {
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      (branch) => {
        this.branchId = branch.BranchId;

      });
    this.adminService.getBranches(-1).pipe(
      takeUntil(this.destroy$)
      ).subscribe(branch => {
      if (branch.Data) {
        this.branches = branch.Data;
      }
    })

    this.adminService.getTaskCategories(this.branchId).pipe(
      takeUntil(this.destroy$)
      ).subscribe(category => {
      if (category.Data) {
        this.followUpCategories = category.Data;
        (this.followUpCategories)
      }
    })

  }

  ngOnInit() {
    // load the data to the table
    this.employeeBasicInfoService.currentMember.pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      employee => {
        if (employee) {
          this.currentEmployee = employee
          this.getBookings()
        }
      });
  }


  getBookings() {
    PreloaderService.showPreLoader();
    this.adminService.GetEmployeeBookings(this.currentEmployee.Id).pipe(
      takeUntil(this.destroy$)
      ).subscribe(event => {
      if (event.Data) {
        this.bookingItems = event.Data
        // const today = now.getFullYear() + '-' + now.getMonth() + 1 + '-' + now.getDate()
        this.itemResource = new DataTableResource(this.bookingItems);
        this.itemResource.count().then(count => this.itemCount = count);
        this.items = this.bookingItems;
        this.itemCount = Number(this.bookingItems.length);
        PreloaderService.hidePreLoader();
        this.filterInput(this.isThumbsUp, '', 'ALL');
      } else {
        PreloaderService.hidePreLoader();
      }
    }, Error => {
      PreloaderService.hidePreLoader();
    }
    );
  }
  viewLoad(rowItem, popOver) {
    (rowItem);
    if (this.popOver) {
      this.popOver.close();
    }
    this.popOver = popOver;
    this.popOver.open();
    // this.empList = [];
    this.empList = rowItem.MemberList
    (this.empList);
  }

  closePopOver() {
    this.popOver.close();
  }



  filterInput(isApproved: boolean, resourceVal: string, branchValue: string) {
    this.filteredBookings = this.bookingItems
    if (branchValue && branchValue !== 'ALL') {
      (branchValue.split('_')[1])

      this.filteredBookings = this.filterpipe.transform('BranchId', 'SELECT', this.filteredBookings, branchValue);

    }

    if (resourceVal && resourceVal !== '') {

      this.filteredBookings = this.filterpipe.transform('Resource', 'NAMEFILTER', this.filteredBookings, resourceVal);

    }



   this.filteredBookings = this.filterpipe.transform('IsApproved', 'SELECT', this.filteredBookings, isApproved);


    this.itemResource = new DataTableResource(this.filteredBookings);
    this.itemResource.count().then(count => this.itemCount = count);
    this.reloadItems(this.filteredBookings);
  }

  reloadItems(params): void {
    if (this.itemResource) {
      this.itemResource.query(params).then(items => this.items = items);
    }
  }


  setSelectedItemRow(item, content) {
    this.selectedItem = item
    this.openFollowUpModal(content)
  }

  rowDoubleClick(event, content) {
    this.selectedItem = event.row.item
    this.openFollowUpModal(content)
  }
  openFollowUpModal(content: any) {
    this.modalService.open(content, { width: '600' });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

}







