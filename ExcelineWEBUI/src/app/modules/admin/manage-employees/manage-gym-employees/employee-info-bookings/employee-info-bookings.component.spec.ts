import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeInfoBookingsComponent } from './employee-info-bookings.component';

describe('EmployeeInfoBookingsComponent', () => {
  let component: EmployeeInfoBookingsComponent;
  let fixture: ComponentFixture<EmployeeInfoBookingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeInfoBookingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeInfoBookingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
