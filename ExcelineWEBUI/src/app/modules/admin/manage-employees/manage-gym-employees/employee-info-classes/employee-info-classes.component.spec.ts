import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeInfoClassesComponent } from './employee-info-classes.component';

describe('EmployeeInfoClassesComponent', () => {
  let component: EmployeeInfoClassesComponent;
  let fixture: ComponentFixture<EmployeeInfoClassesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeInfoClassesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeInfoClassesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
