import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewTimeEntriesComponent } from './view-time-entries.component';

describe('ViewTimeEntriesComponent', () => {
  let component: ViewTimeEntriesComponent;
  let fixture: ComponentFixture<ViewTimeEntriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewTimeEntriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewTimeEntriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
