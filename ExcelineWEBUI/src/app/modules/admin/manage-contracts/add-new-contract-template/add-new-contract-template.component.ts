import { Component, OnInit, Input, Output, ViewChild, ElementRef, EventEmitter, OnDestroy } from '@angular/core';
import { UsbModal } from 'app/shared/components/us-modal/us-modal';
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { AdminService } from '../../services/admin.service'
import { ExceLoginService } from '../../../../modules/login/exce-login/exce-login.service';
import { element } from 'protractor';
import { DataTableResource } from '../../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from '../../../../shared/components/us-data-table/tools/data-table-filter-pipe';
import { ContractTemplateModel } from './contract-template-model';
import { forEach } from '@angular/router/src/utils/collection';
import { UsErrorService } from '../../../../shared/directives/us-error/us-error.service'
import { Dictionary } from 'lodash';
import { UsDatePickerComponent } from 'app/shared/components/us-date-picker/us-date-picker.component';
import { ExceMessageService } from 'app/shared/components/exce-message/exce-message.service';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'new-contract-template',
  templateUrl: './add-new-contract-template.component.html',
  styleUrls: ['./add-new-contract-template.component.scss']
})
export class AddNewContractTemplateComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private branchId: number;
  private articles: any;
  private countries: any;
  private branches: any;
  private regions: any;
  private selectedBranches = [];
  private selectedCountries = []
  private selectedRegions = []
  private articleType: any;
  private startUpResource: any = 0;
  private startUps: any;
  private selectedContractValue = [];
  private selectedMemberFeeArticle = []
  private isMemberFee = false;
  private contracts: any;
  private filteredContracts: any;
  private contractResource: any = 0;
  private contractItems = [];
  private contractCount = 0;
  private conditions: any;
  private filteredConditions: any;
  private conditionResource: any = 0;
  private conditionItems = [];
  private conditionCount = 0;
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  private everyMonthResource: any = [];
  private contractObj;
  private tempActivityId: number;
  private previousArticleId = -1;
  contType = 'CONTRACT'
  monthlyServiceModal: any;
  nexttemplateModal: any;
  contractConditionModal: any;
  @Input() selectedItem: any;
  @Input() ContractPackageList = [];
  @Input() isEdit = false;
  @Input() activityCats = [];
  @Output() saveSuccess: EventEmitter<any> = new EventEmitter();
  @Output() canceled: EventEmitter<any> = new EventEmitter();
  private contractCats: any;
  BasicInfoForm: any;
  TempSettingsForm: any;
  AvailableAccessForm: any;
  accesProfiles: any;
  startUpItems = [];
  startUpItemCount = 0;
  packageCategories: any;
  selectedTemplate: ContractTemplateModel = new ContractTemplateModel();
  everyMonthItems = [];
  everyMonthItemCount = 0;
  newTemplateNo: number;
  activitiesSelected = 0;
  selectedContractType = 0;
  formSubmited = false;
  _numberOfOrders = 12;
  everyMonthItemsPrice = 0;
  startUpItemPrice = 0;
  packagePrice = 0;

  @ViewChild('CategoryList') CategoryList: ElementRef;
  @ViewChild('SalesToDate') SalesToDate: UsDatePickerComponent;
  @ViewChild('SalesFromDate') SalesFromDate: UsDatePickerComponent;
  @ViewChild('FirstDueDate') FirstDueDate: UsDatePickerComponent;
  @ViewChild('SecondDueDate') SecondDueDate: UsDatePickerComponent;
  @ViewChild('StartDateContract') StartDateContract: UsDatePickerComponent;
  @ViewChild('EndDateContract') EndDateContract: UsDatePickerComponent;


  formErrors = {
    'PackageName': '',
    'ArticleId': '',
    'ActivityId': '',
    'StartDate': '',
    'EndDate': '',

  };

  validationMessages = {
    'PackageName': { 'required': 'value required.', },
    'ArticleId': { 'required': 'value required.', },
    'ActivityId': { 'required': 'value required.', },
    'StartDate': { 'required': 'value required.', },
    'EndDate': { 'required': 'value required.', },
  };
  tempActivityName: any;
  activityModal: any;
  columnDefs: any;
  gridApi: any;
  gridColumnApi: any;
  columnArticles: any[];
  tempArticleDesc: any;
  selectedTempName: any;
  ArticleDesc: any;
  tempArticleSelected: any;
  okDisabled = true;
  saveErrorMsg: string;

  constructor(
    private modalService: UsbModal,
    private adminService: AdminService,
    private exceLoginService: ExceLoginService,
    private translateService: TranslateService,
    private exceMessage: ExceMessageService,
    private fb: FormBuilder) {
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      (branch) => {
        this.branchId = branch.BranchId;
      }
    );
  }

  ngOnInit() {
    let now = new Date;
    this.BasicInfoForm = this.fb.group({
      'TemplateNumber': [null],
      'PackageName': [null],
      'StartDate': [{
        date: {
          year: now.getFullYear(),
          month: now.getMonth() + 1,
          day: now.getDate()
        }
      }],
      'EndDate': [{
        date: {
          year: now.getFullYear() + 100,
          month: now.getMonth() + 1,
          day: now.getDate()
        }
      }],
      'AmountForATG': [0],
      'SortingNo': [1],
      'ATGStatus': [0],
      'IsATG': [null],
      'IsShownOnNet': [null],
      'RestPlusMonth': [null],
      'IsInvoiceDetail': [null],
      'AutoRenew': [null],
      'DiscountRate': [0],
      'ActivityId': [null],
      'ArticleId': [null],
      'ActivityName': [null],
    });
    this.TempSettingsForm = this.fb.group({
      'NumOfInstallments': [0],
      'EveryMonthItemsPrice': [0],
      'StartUpItemPrice': [0],
      'PackagePrice': [0],
      'NumOfVisits': [0],
      'NoOfMonths': [0],
      'NoOfDays': [0],
      'PriceGuaranty': [0],
      'MaxAge': [0],
      'CreditDueDays': [0],
      'FirstDueDate': [null],
      'SecondDueDate': [null],
      'FixStartDateOfContract': [null],
      'FixDateOfContract': [null],
      'LockInPeriod': [null]
    });
    this.AvailableAccessForm = this.fb.group({
      'IsInStock': [null],
      'InStock': [0],
      'AccessProfileId': [null],
      'IsNoClasses': [null],
      'ExpressAvailable': [null],
    });
    this.adminService.GetAccessProfiles().pipe(takeUntil(this.destroy$)).subscribe(accessProfs => {
      if (accessProfs.Data) {
        this.accesProfiles = accessProfs.Data
      }
    })
    this.adminService.getCategoriesByType('PACKAGE').pipe(takeUntil(this.destroy$)).subscribe(packResult => {
      if (packResult.Data) {
        this.packageCategories = packResult.Data
      }
    });
    this.adminService.getCategoriesByType('COUNTRY').pipe(takeUntil(this.destroy$)).subscribe(CountResult => {
      if (CountResult.Data) {
        this.countries = CountResult.Data;

        this.adminService.getBranches(-1).pipe(takeUntil(this.destroy$)).subscribe(BranchResult => {
          if (BranchResult.Data) {
            this.branches = BranchResult.Data;

            this.adminService.getRegions('').pipe(takeUntil(this.destroy$)).subscribe(RegResult => {
              if (RegResult.Data) {
                this.regions = RegResult.Data;
                this.adminService.getCategoriesByType('CONTRACT').pipe(takeUntil(this.destroy$)).subscribe(result => {
                  if (result) {
                    this.contractCats = result.Data;
                    this.selectedTemplate.PackageCategory = this.contractCats.filter(a => a.Code === this.contType)[0];
                    if (this.isEdit) {
                      this.loadInitialData();
                    }
                  }
                });
              }
            })
          }
        })
      }
    });

    this.selectedTemplate.BranchId = this.branchId;
    const maxTemplateNo = this.ContractPackageList.reduce((max, p) => p.IntTemplateNumber > max ? p.IntTemplateNumber : max, this.ContractPackageList[0].PackageId);
    this.newTemplateNo = maxTemplateNo + 1;


    this.columnDefs = [
      {headerName: 'Aktivitet', field: 'Name', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
        filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
    ]

    this.columnArticles = [
      {headerName: 'Artikkel', field: 'Description', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
        filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true},
    ]
    if (!this.isEdit) {
      this.selectedTempName = '';
    }
  }


  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    // auto fits columns to fit screen
    params.api.sizeColumnsToFit();
    // hiding loading message on load
    this.gridApi.gridOptionsWrapper.gridOptions.api.hideOverlay();
  }


  loadInitialData() {
    this.selectedTempName = this.selectedItem.ActivityName;
    this.selectedTemplate.NextTemplateId = this.selectedItem.NextTemplateId
    this.selectedTemplate.NextContractTemplateName = this.selectedItem.NextContractTemplateName
    this.selectedTemplate.ContractConditionId = this.selectedItem.ContractConditionId
    this.selectedTemplate.ContractCondition = this.selectedItem.ContractCondition
    this.selectedContractType = this.selectedItem.ContractTypeValue.Id
    this.selectedTemplate.ContractTypeValue = this.selectedItem.ContractTypeValue
    this.selectedTemplate.MemberFeeArticle = this.selectedItem.MemberFeeArticle
    this.selectedTemplate.ActivityId = this.selectedItem.ActivityId
    this.selectedTemplate.ActivityName = this.selectedItem.ActivityName
    this.selectedTemplate.ArticleId = this.selectedItem.ArticleId
    this.previousArticleId = this.selectedItem.ArticleId;
    // this.selectedTemplate.ContractActivity = this.selectedItem.ContractActivity
    for (let i = 0; i < this.activityCats.length; i++) {
      if (this.activityCats[i].Id === this.selectedItem.ActivityId) {
        this.selectedItem.ContractActivity = this.activityCats[i]
        this.getArticles(this.selectedItem.ActivityId)

      }
    }
    this.selectedTemplate.PackageCategory = this.selectedItem.PackageCategory
    this.selectedTemplate.EveryMonthItemList = this.selectedItem.EveryMonthItemList
    this.selectedTemplate.EveryMonthItemList.forEach(ele => {
      ele.isSelected = true;
    });
    this.selectedTemplate.StartUpItemList = this.selectedItem.StartUpItemList
    this.selectedTemplate.StartUpItemList.forEach(ele => {
      ele.isSelected = true;
    });
    for (const cat of this.contractCats) {
      if (cat.Id === this.selectedItem.PackageCategory.Id) {
        if (cat.Code === 'CONTRACT') {
          this.contType = 'CONTRACT';
        } else if (cat.Code === 'PUNCHCARD') {
          this.contType = 'PUNCHCARD';
        }
      }
    }
    if (this.selectedItem) {
      this.selectedTemplate.BranchIdList = this.selectedItem.BranchIdList
      this.selectedTemplate.CountryIdList = this.selectedItem.CountryIdList
      this.selectedTemplate.RegionIdList = this.selectedItem.RegionIdList
      this.activitiesSelected = 1;

      this.getArticles(this.selectedItem.ActivityId)
      let selectedItemClone = JSON.parse(JSON.stringify(this.selectedItem));
      selectedItemClone = this.getDates(selectedItemClone);
      this.patchValues(selectedItemClone)

      this.everyMonthResource = new DataTableResource(this.selectedTemplate.EveryMonthItemList);
      this.everyMonthResource.count().then(count => this.everyMonthItemCount = count);
      this.everyMonthItems = this.selectedTemplate.EveryMonthItemList;
      this.everyMonthItemCount = Number(this.everyMonthItems.length);

      this.startUpResource = new DataTableResource(this.selectedTemplate.StartUpItemList);
      this.startUpResource.count().then(count => this.startUpItemCount = count);
      this.startUpItems = this.selectedTemplate.StartUpItemList;
      this.startUpItemCount = Number(this.startUpItems.length);
    }
  }


  closeModal() {
    this.canceled.emit();
  }

  getDates(selectedItemClone) {
    if (selectedItemClone.EndDate) {
      const tempDate = new Date(selectedItemClone.EndDate);
      selectedItemClone.EndDate = {
                  date: {
                    year: tempDate.getFullYear(),
                    month: tempDate.getMonth() + 1,
                    day: tempDate.getDate()
                  }
                };
    }
    if (selectedItemClone.StartDate) {
      const tempDate = new Date(selectedItemClone.StartDate);
      selectedItemClone.StartDate = {
                  date: {
                    year: tempDate.getFullYear(),
                    month: tempDate.getMonth() + 1,
                    day: tempDate.getDate()
                  }
                };
    }
    if (selectedItemClone.FixDateOfContract) {
      const tempDate = new Date(selectedItemClone.FixDateOfContract);
      selectedItemClone.FixDateOfContract = {
                  date: {
                    year: tempDate.getFullYear(),
                    month: tempDate.getMonth() + 1,
                    day: tempDate.getDate()
                  }
                };
    }
    if (selectedItemClone.FirstDueDate) {
      const tempDate = new Date(selectedItemClone.FirstDueDate);
      selectedItemClone.FirstDueDate = {
                  date: {
                    year: tempDate.getFullYear(),
                    month: tempDate.getMonth() + 1,
                    day: tempDate.getDate()
                  }
                };
    }
    if (selectedItemClone.SecondDueDate) {
      const tempDate = new Date(selectedItemClone.SecondDueDate);
      selectedItemClone.SecondDueDate = {
                  date: {
                    year: tempDate.getFullYear(),
                    month: tempDate.getMonth() + 1,
                    day: tempDate.getDate()
                  }
                };
    }
    if (selectedItemClone.FixStartDateOfContract) {
      const tempDate = new Date(selectedItemClone.FixStartDateOfContract);
      selectedItemClone.FixStartDateOfContract = {
                  date: {
                    year: tempDate.getFullYear(),
                    month: tempDate.getMonth() + 1,
                    day: tempDate.getDate()
                  }
                };
    }

    return selectedItemClone;
  }
  patchValues(item) {
    this.everyMonthItemsPrice = item.EveryMonthItemsPrice
    this.startUpItemPrice = item.StartUpItemPrice
    this.packagePrice = item.PackagePrice
    this.BasicInfoForm.patchValue(item)
    this.TempSettingsForm.patchValue(item)
    this.AvailableAccessForm.patchValue(item)
  }

  changeContType(event) {
    this.contType = event;
    this.selectedTemplate.PackageCategory = this.contractCats.filter(a => a.Code === this.contType)[0];
  }

  getArticles(activityId: number, isFirst?) {
    this.tempActivityId = activityId;
    this.adminService.GetArticlesForCategory(activityId, this.branchId).pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
      if (result.Data) {
        this.articles = result.Data
        if (isFirst) {
          for (let i = 0; i < this.articles.length; i++) {
            if (this.articles[i].Id === this.selectedItem.ActivityId) {
              this.selectedItem.ContractActivity.Article = this.articles[i]
            }
          }
        }
      }
    })
  }

  openMonthlyServiceModal(content: any, type: string, isMemberFee) {
    this.articleType = type;
    this.isMemberFee = isMemberFee
    this.monthlyServiceModal = this.modalService.open(content);
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
    if (this.contractConditionModal) {
      this.contractConditionModal.close();
    }
    if (this.monthlyServiceModal) {
      this.monthlyServiceModal.close();
    }
    if (this.nexttemplateModal) {
      this.nexttemplateModal.close();
    }
    if (this.activityModal) {
      this.activityModal.close();
    }
    if (this.selectedItem) {
      this.selectedItem = null;
    }
  }

  closeMonthlyServiceModel() {
    this.monthlyServiceModal.close()
  }

  openNextTemplateeModal(content: any) {
    this.getContracts()
    this.nexttemplateModal = this.modalService.open(content);
  }

  openContractConditionModal(content: any) {
    this.getConditions();
    this.contractConditionModal = this.modalService.open(content, { width: '800' });
  }

  getContracts() {
    this.adminService.GetContracts({ 'searchText': '', 'searchType': 1, 'branchId': this.branchId }).pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
      if (result) {
        this.contracts = result.Data;
        this.contractResource = new DataTableResource(this.contracts);
        this.contractResource.count().then(count => this.contractCount = count);
        this.contractItems = this.contracts;
        this.contractCount = Number(result.Data.length);
      }
    })
  }


  nextTemplateSelect(event) {
    this.selectedTemplate.NextTemplateId = +event.row.item.PackageId
    this.selectedTemplate.NextContractTemplateName = event.row.item.PackageName
    this.nexttemplateModal.close();
  }

  getConditions() {

    this.adminService.GetGymCompanySettings('CONTRACT').pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
      if (result) {
        this.conditions = result.Data;
        this.conditionResource = new DataTableResource(this.conditions);
        this.conditionResource.count().then(count => this.conditionCount = count);
        this.conditionItems = this.conditions;
        this.conditionCount = Number(result.Data.length);
      }
    })
  }


  conditionSelect(event) {
    this.selectedTemplate.ContractConditionId = event.row.item.Id
    this.selectedTemplate.ContractCondition = event.row.item.Name
    this.contractConditionModal.close();
  }

  filterTemplates(numberVal: string, nameVal: string) {
    this.filteredContracts = this.contracts;
    if (numberVal && numberVal !== '') {
      this.filteredContracts = this.filterpipe.transform('TemplateNumber', 'TEXT', this.filteredContracts, numberVal);
    }
    if (nameVal !== '' || nameVal != null) {
      this.filteredContracts = this.filterpipe.transform('PackageName', 'TEXT', this.filteredContracts, nameVal);
    }
    this.contractItems = this.filteredContracts;
  }

  changeActivity(event) {
    this.tempActivityId = event.data.Id;
    this.tempActivityName = event.data.Name;
    this.getArticles(event.data.Id)
    this.okDisabled = true;
  }

  changeSelectedArticle(event) {
    this.tempArticleSelected = event;
    this.validateDisabledBtn();
  }

  validateDisabledBtn() {
    if (this.tempActivityId && this.tempActivityName && this.tempArticleSelected) {
      if (Number(this.tempActivityId) === Number(this.tempArticleSelected.data.ActivityId)) {
        this.okDisabled = false;
      } else {
        this.okDisabled = true;
      }
    } else {
      this.okDisabled = true;
    }
  }

  activityArticleSelect() {
    const event = this.tempArticleSelected;
    this.selectedTempName = this.tempActivityName;
    this.BasicInfoForm.patchValue({ 'ActivityName': this.tempActivityName });
    this.selectedTemplate.ArticleId = event.data.Id;
    this.selectedTemplate.ActivityId = this.tempActivityId;
    this.BasicInfoForm.patchValue({ 'ArticleId': event.data.Id });
    this.activitiesSelected = 1;
    for (let i = 0; i < this.activityCats.length; i++) {
      if (this.activityCats[i].Id === this.tempActivityId) {
        this.selectedTemplate.ActivityName = this.activityCats[i].Description
        this.selectedTemplate.ContractActivity = this.activityCats[i]
        if (this.isEdit) {
          this.selectedItem.ContractActivity = this.activityCats[i]
        }

      }
    }
    for (let i = 0; i < this.articles.length; i++) {
      if (this.articles[i].Id === event.data.Id) {
        this.selectedTemplate.ContractActivity.Article = this.articles[i]
        if (this.isEdit) {
          this.selectedItem.ContractActivity.Article = this.articles[i]
        }
        this.selectedTemplate.EveryMonthItemList.push(
          {
            'ArticleId': this.articles[i].Id,
            'ArticleNo': this.articles[i].ArticleNo,
            'CategoryId': this.articles[i].CategoryId,
            'ItemName': this.articles[i].Description,
            'UnitPrice': this.articles[i].DefaultPrice,
            'StockLevel': this.articles[i].StockLevel,
            'Price': this.articles[i].Price,
            'Quantity': 1,
            'isSelected': true
          })

        this.selectedTemplate.EveryMonthItemList = this.selectedTemplate.EveryMonthItemList.filter(item => item.ArticleId !== this.previousArticleId);
        this.SetStartEndOrders();
        this.everyMonthResource = new DataTableResource(this.selectedTemplate.EveryMonthItemList);
        this.everyMonthResource.count().then(count => this.everyMonthItemCount = count);
        this.everyMonthItems = this.selectedTemplate.EveryMonthItemList;
        this.everyMonthItemCount = Number(this.selectedTemplate.EveryMonthItemList.length);
        this.previousArticleId = this.articles[i].Id;
      }
  }
  }

  scrollPopOverContent(content) {
    this.activityModal = this.modalService.open(content, { width: '800' })
  }

  selectItems(event) {
    this.monthlyServiceModal.close();
    const eventItems = event;
    if (this.isMemberFee) {
      this.selectedTemplate.MemberFeeArticle = eventItems[0]
    } else {
      eventItems.forEach(serItem => {
        serItem.Price = serItem.Quantity * serItem.UnitPrice
      });
      if (this.articleType === 'ITEM') {
        eventItems.forEach(elementStartUp => {
          elementStartUp.isSelected = true;
          this.selectedTemplate.StartUpItemList.push(elementStartUp)
        });
        this.startUpResource = new DataTableResource(this.selectedTemplate.StartUpItemList);
        this.startUpResource.count().then(count => this.startUpItemCount = count);
        this.startUpItems = this.selectedTemplate.StartUpItemList;
        this.startUpItemCount = Number(this.startUpItems.length);
        this.calculateStartUpItemPrice()

      } else {
        eventItems.forEach(elementEveryMonth => {
          elementEveryMonth.isSelected = true;
          this.selectedTemplate.EveryMonthItemList.push(elementEveryMonth)
        });
        this.SetStartEndOrders()
        this.everyMonthResource = new DataTableResource(this.selectedTemplate.EveryMonthItemList);
        this.everyMonthResource.count().then(count => this.everyMonthItemCount = count);
        this.everyMonthItems = this.selectedTemplate.EveryMonthItemList;
        this.everyMonthItemCount = Number(this.selectedTemplate.EveryMonthItemList.length);
        this.claculateEveryMonthItemPrice()
      }
    }
  }

  claculateEveryMonthItemPrice() {
    let itemPrice = 0
    this.everyMonthItems.forEach(monthItem => {
      itemPrice = itemPrice + (monthItem.Price * monthItem.Quantity)
    })
    this.TempSettingsForm.controls['EveryMonthItemsPrice'].setValue(itemPrice)
    this.everyMonthItemsPrice = itemPrice
    this.setTotalprice()
  }

  calculateStartUpItemPrice() {
    let itemPrice = 0
    this.startUpItems.forEach(startUpItem => {
      itemPrice = itemPrice + (startUpItem.Price * startUpItem.Quantity)
    })
    this.TempSettingsForm.controls['StartUpItemPrice'].setValue(itemPrice)
    this.startUpItemPrice = itemPrice
    this.setTotalprice()
  }

  setTotalprice() {
    const total = this.TempSettingsForm.get('EveryMonthItemsPrice').value + this.TempSettingsForm.get('StartUpItemPrice').value
   this.TempSettingsForm.controls['PackagePrice'].setValue(total)
   this.packagePrice = total
  }

  addBranch(event) {
    if (event.target.checked) {
      this.selectedTemplate.BranchIdList.push(+event.target.value)
    } else {
      this.selectedTemplate.BranchIdList = this.selectedTemplate.BranchIdList.filter(item => item !== +event.target.value);
    }
  }

  addCountry(event) {
    if (event.target.checked) {
      this.selectedTemplate.CountryIdList.push(+event.target.value)
    } else {
      this.selectedTemplate.CountryIdList = this.selectedTemplate.CountryIdList.filter(item => item !== +event.target.value);
    }
  }

  addRegion(event) {
    if (event.target.checked) {
      this.selectedTemplate.RegionIdList.push(+event.target.value)
    } else {
      this.selectedTemplate.RegionIdList = this.selectedTemplate.RegionIdList.filter(item => item !== +event.target.value);
    }
  }

  SetStartEndOrders() {
    this.selectedTemplate.EveryMonthItemList.forEach(elementEveryMonth => {

      const itemCount = this.selectedTemplate.EveryMonthItemList.filter(x => x.ArticleId === elementEveryMonth.ArticleId).length;
      const interval = this._numberOfOrders / itemCount;

      let count = 0;
      this.selectedTemplate.EveryMonthItemList.filter(x => x.ArticleId === elementEveryMonth.ArticleId).forEach(item => {
        item.StartOrder = (interval * count) + 1;

        item.EndOrder = (interval * count) + interval;
        if (count === this.selectedTemplate.EveryMonthItemList.filter(x => x.ArticleId === elementEveryMonth.ArticleId).length - 1)
        // tslint:disable-next-line:one-line
        {
          item.EndOrder = this._numberOfOrders;
        }
        item.NumberOfOrders = item.EndOrder - item.StartOrder + 1;
        count++;
      });
    });
  }

  copyEveryMonthItem(item) {
    this.selectedTemplate.EveryMonthItemList.push(JSON.parse(JSON.stringify(item)));
    this.SetStartEndOrders()
  }

  deleteEveryMonthItem(item) {
    this.selectedTemplate.EveryMonthItemList = this.selectedTemplate.EveryMonthItemList.filter(x =>
      !((x.ArticleId === item.ArticleId) && (x.StartOrder === item.StartOrder)));
    this.SetStartEndOrders()
    this.everyMonthResource = new DataTableResource(this.selectedTemplate.EveryMonthItemList);
    this.everyMonthResource.count().then(count => this.everyMonthItemCount = count);
    this.everyMonthItems = this.selectedTemplate.EveryMonthItemList;
    this.everyMonthItemCount = Number(this.selectedTemplate.EveryMonthItemList.length);
  }

validateSave(basicFormVal, tempVal, avilAccessVal) {
  this.saveErrorMsg = '';
  if (!basicFormVal.PackageName) {
    let translateVal;
    this.translateService.get('ADMIN.AddNewTemplateTxbTemplateName').pipe(
      takeUntil(this.destroy$)
    ).subscribe(translatedValue => translateVal = translatedValue);
    this.saveErrorMsg = this.saveErrorMsg + '"' +  translateVal + '" ';
  }
  if (!basicFormVal.ActivityName) {
    let translateVal;
    this.translateService.get('ADMIN.Activity').pipe(
      takeUntil(this.destroy$)
    ).subscribe(translatedValue => translateVal = translatedValue);
    this.saveErrorMsg = this.saveErrorMsg + '"' +  translateVal + '" ';
  }
  if (!this.selectedContractType) {
    let translateVal;
    this.translateService.get('ADMIN.Category').pipe(
      takeUntil(this.destroy$)
    ).subscribe(translatedValue => translateVal = translatedValue);
    this.saveErrorMsg = this.saveErrorMsg + '"' +  translateVal + '" ';
  }
  if (!basicFormVal.StartDate) {
    let translateVal;
    this.translateService.get('ADMIN.AddNewTemplateTxbSalesFrom').pipe(
      takeUntil(this.destroy$)
    ).subscribe(translatedValue => translateVal = translatedValue);
    this.saveErrorMsg = this.saveErrorMsg + '"' +  translateVal + '" ';
  }
  if (!basicFormVal.EndDate) {
    let translateVal;
    this.translateService.get('ADMIN.MngTemplatesHeaderSalesTo').pipe(
      takeUntil(this.destroy$)
    ).subscribe(translatedValue => translateVal = translatedValue);
    this.saveErrorMsg = this.saveErrorMsg + '"' +  translateVal + '" ';
  }
  if (!avilAccessVal.AccessProfileId) {
    let translateVal;
    this.translateService.get('ADMIN.ManageGymSettingsAccessProfileBtn').pipe(
      takeUntil(this.destroy$)
    ).subscribe(translatedValue => translateVal = translatedValue);
    this.saveErrorMsg = this.saveErrorMsg + '"' +  translateVal + '" ';
  }
  return !basicFormVal.StartDate || !basicFormVal.EndDate || !basicFormVal.PackageName
    || !basicFormVal.ActivityName || !this.selectedContractType || !avilAccessVal.AccessProfileId;
}

  saveContract(basicFormVal, tempVal, avilAccessVal) {
    if (this.validateSave(basicFormVal, tempVal, avilAccessVal)) {
      let translateVal;
      this.translateService.get('ADMIN.TemplateSaveErros').pipe(
        takeUntil(this.destroy$)
      ).subscribe(translatedValue => translateVal = translatedValue);
      this.exceMessage.openMessageBox('ERROR',
      {
        messageTitle: translateVal,
        messageBody: this.saveErrorMsg
      });
      return
    }
    for (let i = 0; i < this.packageCategories.length; i++) {
      if (this.packageCategories[i].Id = this.selectedContractType) {
        this.selectedTemplate.ContractTypeValue = this.packageCategories[i]
      }
    }
    // this.selectedTemplate.ContractActivity.Id = this.selectedTemplate.ActivityId
    basicFormVal.StartDate = this.dateConverter(this.SalesFromDate.selectionDayTxt)
    basicFormVal.EndDate = this.dateConverter(this.SalesToDate.selectionDayTxt)
    tempVal.FirstDueDate = this.dateConverter(this.FirstDueDate.selectionDayTxt)
    tempVal.SecondDueDate = this.dateConverter(this.SecondDueDate.selectionDayTxt)
    tempVal.FixStartDateOfContract = this.dateConverter(this.StartDateContract.selectionDayTxt)
    tempVal.FixDateOfContract = this.dateConverter(this.EndDateContract.selectionDayTxt)
    this.contractObj = Object.assign(basicFormVal, tempVal, avilAccessVal, this.selectedTemplate);
    if (this.contractObj) {
      if ((!this.contractObj.SortingNo) || (this.contractObj.SortingNo < 0)) {
        this.contractObj.SortingNo = 1;
      }
      if (this.isEdit) {
        for (const key in this.contractObj) {
          if (this.contractObj[key] !== null) {
            this.selectedItem[key] = this.contractObj[key];
          }
        }
        this.adminService.saveContract(this.selectedItem).pipe(
          takeUntil(this.destroy$)
        ).subscribe(result => {
          if (result.Data) {
            this.saveSuccess.emit();
          }
        })
      } else {
        this.adminService.saveContract(this.contractObj).pipe(
          takeUntil(this.destroy$)
        ).subscribe(result => {
          if (result.Data) {
            this.SetPriorityToPackageList();
            if (this.ContractPackageList.length > 0) {
              const maxPackageID = this.ContractPackageList.reduce((max, p) => p.PackageId > max ? p.PackageId : max, this.ContractPackageList[0].PackageId);
              this.contractObj.PackageId = maxPackageID + 1;
              this.contractObj.TemplateNumber = this.newTemplateNo
              this.ContractPackageList.push(this.contractObj);
              this.ContractPackageList.sort(function (a, b) {
                return a.SortingNo - b.SortingNo
              });
              // this.ContractPackageList =this.ContractPackageList.sort((a, b) => a.SortingNo.localeCompare(b.SortingNo));
              this.updatePriority();
            }
            this.saveSuccess.emit();
          }
        })
      }
    }
  }

  updatePriority() {
    const priorityList = this.SetSortingValues()
    this.adminService.updatePriority(priorityList).pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
      if (result.Data) {
        this.saveSuccess.emit()
      }
    })
  }

  dateConverter(dateObj: any) {
    let dateS = '';
    if (dateObj.length > 0) {
      const splited = dateObj.split('.')
      if (splited.length > 1) {
        dateS = splited[2] + '-' + splited[1] + '-' + splited[0] + 'T00:00:00'
      }
    }
    return dateS
  }

  SetPriorityToPackageList() {
    if (this.ContractPackageList.length > 0) {
      //  const lastSortedNo = this.ContractPackageList.min(x => x.SortingNo);
      const lastSortedNo = this.ContractPackageList.reduce((max, p) => p.SortingNo > max ? p.SortingNo : max, this.ContractPackageList[0].SortingNo);
      const firstSortedNo = this.ContractPackageList.reduce((min, p) => p.SortingNo < min ? p.SortingNo : min, this.ContractPackageList[0].SortingNo);
      // const firstSortedNo = this.ContractPackageList.Math.min(x => x.SortingNo);
      const newSortingNo = this.contractObj.SortingNo;
      if (newSortingNo >= lastSortedNo || newSortingNo < firstSortedNo || newSortingNo <= 0) {
        this.contractObj.SortingNo = lastSortedNo + 1;
      } else {
        let sortedID = lastSortedNo;
        while (sortedID >= newSortingNo) {
          const packageItem = this.ContractPackageList.filter(y => y.SortingNo === (sortedID))[0];
          if (packageItem != null) {
            packageItem.SortingNo = sortedID + 1;
          }
          sortedID--;
        }
      }
    }
  }

  SetSortingValues() {
    let sortingNo = 1;
    const priorityValues: Dictionary<number> = {};
    // tslint:disable-next-line:forin
    for (const pack of this.ContractPackageList) {
      pack.SortingNo = sortingNo;
      // priorityValues.Add(package.PackageId, package.SortingNo);
      priorityValues[pack.PackageId + ''] = pack.SortingNo;
      sortingNo++;
    }
    return priorityValues;
  }

  validateUserForm(basicFormVal, tempVal, avilAccessVal) {
    basicFormVal.ArticleId = this.selectedTemplate.ArticleId;
    if (this.activitiesSelected < 1) {
      this.BasicInfoForm.controls['ArticleId'].setErrors({
        'required': false,
      });
    }
    this.submitUserForm(basicFormVal, tempVal, avilAccessVal);
  }

  submitUserForm(basicFormVal, tempVal, avilAccessVal) {
    this.formSubmited = true;
    // UsErrorService.validateAllFormFields(this.BasicInfoForm, this.formErrors, this.validationMessages);
    this.saveContract(basicFormVal, tempVal, avilAccessVal)
    // if (this.BasicInfoForm.valid) {
    //   this.saveContract(basicFormVal, tempVal, avilAccessVal)
    // }
  }
}



