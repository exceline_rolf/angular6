
import { Component, OnInit, ViewChild, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { DataTableResource } from '../../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from '../../../../shared/components/us-data-table/tools/data-table-filter-pipe'
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
import { ExceMemberService } from '../../../membership/services/exce-member.service'
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { AdminService } from '../../services/admin.service'
import { ExceLoginService } from '../../../../modules/login/exce-login/exce-login.service';
import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'contract-item-select',
  templateUrl: './contract-item-select.component.html',
  styleUrls: ['./contract-item-select.component.scss']
})
export class ContractItemSelectComponent implements OnInit, OnDestroy {
  private branchId: number;
  private destroy$ = new Subject<void>();
  private itemResource: any = 0;
  private resources: any;
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  private model: any;
  private getContractsObj: any;
  private selectedItem: any;
  private isEdit: boolean;
  private articles = [];
  private filteredArticles: any;
  private searchText: any;
  private searchVal: any;
  items = [];
  itemCount = 0;
  @Input() articleType = 'SERVICE';
  @Input() isMemberFee = false;
  @Output() onClickOk: EventEmitter<any> = new EventEmitter()
  @Output() canceled: EventEmitter<any> = new EventEmitter()
  categories: any;
  searchTypeForm: any;
  @ViewChild('autobox') public autocomplete;
  auotocompleteItems = [{ id: 'ArticleNo', name: 'Article no :', isNumber: true }, { id: 'Name', name: 'Name :', isNumber: false },
  { id: 'Barcode', name: 'Barcode :', isNumber: false }, { id: 'Vendor', name: 'Vendor :', isNumber: false }]
  config: any = { 'placeholder': '', 'sourceField': ['searchText'] };
  constructor(private exceMemberService: ExceMemberService,
    private modalService: UsbModal,
    private fb: FormBuilder,
    private adminService: AdminService,
    private exceLoginService: ExceLoginService,
    private exceMessageService: ExceMessageService,
  ) {
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      (branch) => {
        this.branchId = branch.BranchId;
      }
    );
    this.getContractsObj = {
      'searchCriteria': '',
      'branchId': this.branchId,
      'searchText': '',
      'searchType': 0
    };
  }

  ngOnInit() {
    this.searchTypeForm = this.fb.group({
      'searchVal': [this.searchVal]
    });
    this.getArticles('', 'ALL')
    this.getCategoriesByType(this.articleType)
    this.autocomplete.items = this.auotocompleteItems;
    this.autocomplete.setSearchValue(this.auotocompleteItems);
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  onSelect(item: any, value: any) {
    this.searchText = item.searchText; // this.getSearchText(item.name);
    this.searchVal = item.searchVal;
    this.articleSearch(value);
  }

  articleSearch(value: any) {
    const selectedSearchItem = this.autocomplete.getTextValue();
    if (selectedSearchItem.id !== '') {
      this.searchText = selectedSearchItem.id + ' :' + selectedSearchItem.searchVal;
    } else {
      this.searchText = selectedSearchItem.searchVal;
    }
    this.getArticles(this.searchText, 'ALL');

  }

  closedSearch() {
    this.searchVal = '';
    // this.filterName = '';
    this.searchText = '';
    this.autocomplete.textValue = '';
    this.getArticles('', 'ALL');
  }

  getArticles(keyword: string, category: string) {
    this.adminService.getArticles(this.articleType, keyword, category, -1, true, false, this.branchId, false, ).pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
      if (result) {
        this.articles = [];
        const tempArticles = result.Data;
        tempArticles.forEach(tempArticle => {
          this.articles.push(
            {
              'ArticleId': tempArticle.Id,
              'ArticleNo': tempArticle.ArticleNo,
              'CategoryId': tempArticle.CategoryId,
              'ItemName': tempArticle.Description,
              'UnitPrice': tempArticle.DefaultPrice,
              'StockLevel': tempArticle.StockLevel,
              'Quantity': 1,
              'isSelected': false
            }
          )
        });
        this.itemResource = new DataTableResource(this.articles);
        this.itemResource.count().then(count => this.itemCount = count);
        this.items = this.articles;
        this.itemCount = Number(this.articles.length);
      }
    })
  }

  getCategoriesByType(type: string) {
    this.adminService.getCategoriesByType(type).pipe(
      takeUntil(this.destroy$)
      ).subscribe(cats => {
      if (cats.Data) {
        this.categories = cats.Data;
      }
    })
  }

  getSelectedItems() {
    const filteredArt = this.articles.filter(x => x.isSelected === true)
    if (this.isMemberFee) {
      if (filteredArt.length > 1) {
        this.openMaxItemCountmessage('Confirm', 'Please Select only one member fee article')
      } else {
        this.onClickOk.emit(filteredArt)
      }
    } else {
      this.onClickOk.emit(filteredArt)
    }
  }

  openMaxItemCountmessage(title, body) {
    this.model = this.exceMessageService.openMessageBox('WARNING',
      {
        messageTitle: 'title',
        messageBody: body,
      }
    );
  }

  filterInput(categoryTypeValue: string, categoryValue: string) {
    this.filteredArticles = this.articles
    if (categoryValue !== 'All') {
      if (categoryValue !== '' || categoryValue != null) {
        this.filteredArticles = this.filterpipe.transform('CategoryId', 'SELECT', this.filteredArticles, categoryValue);
      }
    }
    this.itemResource = new DataTableResource(this.filteredArticles);
    this.itemResource.count().then(count => this.itemCount = count);
    this.reloadItems(this.filteredArticles);
  }

  reloadItems(params): void {
    if (this.itemResource) {
      this.itemResource.query(params).then(items => this.items = items);
    }
  }

canceledEvent() {
  this.canceled.emit()
}

}
