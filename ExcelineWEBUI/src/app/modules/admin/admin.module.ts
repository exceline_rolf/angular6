import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminRoutingComponents } from './admin-routing.module';
import { AdminService } from 'app/modules/admin/services/admin.service';
import { ActivityUnavailabletimeComponent } from './manage-activities/activity-unavailabletime/activity-unavailabletime.component';
import { GymInfoComponent } from './gym-settings/gym-info/gym-info.component';
import { GymOpentimeComponent } from './gym-settings/gym-opentime/gym-opentime.component';
import { SmsSettingsComponent } from './gym-settings/sms-settings/sms-settings.component';
import { GymSettingsComponent } from '../admin/gym-settings/gym-settings.component';
import { PosHwProfileComponent } from './gym-settings/pos-hw-profile/pos-hw-profile.component';
import { RequiredSettingsComponent } from './gym-settings/required-settings/required-settings.component';
import { EcconomySettingsComponent } from './gym-settings/ecconomy-settings/ecconomy-settings.component';
import { SearchSettingsComponent } from './gym-settings/search-settings/search-settings.component';
import { IntegrationSettingsComponent } from './gym-settings/integration-settings/integration-settings.component';
import { AccessSettingsComponent } from './gym-settings/access-settings/access-settings.component';
import { OtherSettingsComponent } from './gym-settings/other-settings/other-settings.component';
import { GymListComponent } from './gym-settings/gym-list/gym-list.component';
import { GymSettingsService } from '../admin/gym-settings/gym-settings.service'
import { ManageTasksComponent } from './manage-followups/manage-followups-tasks/manage-followups-tasks.component';
import { ManageTemplatesComponent } from './manage-followups/manage-followups-templates/manage-followups-templates.component';
import { GatIntegrationComponent } from '../admin/gym-settings/integration-settings/gat-integration/gat-integration.component';
import { AccessControlIntegrationComponent } from '../admin/gym-settings/integration-settings/access-control-integration/access-control-integration.component';
import { WellnessComponent } from '../admin/gym-settings/integration-settings/wellness/wellness.component';
import { ExorliveComponent } from '../admin/gym-settings/integration-settings/exorlive/exorlive.component';
import { InfoGymComponent } from '../admin/gym-settings/integration-settings/info-gym/info-gym.component';
import { ArxComponent } from '../admin/gym-settings/integration-settings/arx/arx.component';
import { AccessControlTypeComponent } from '../admin/gym-settings/integration-settings/access-control-type/access-control-type.component';
import { AccessControlTypeService } from './gym-settings/integration-settings/access-control-type-service';
import { ManageAnnonymizingComponent } from './manage-annonymizing/manage-annonymizing.component';
import { ManageClassTypesComponent } from './manage-class-types/manage-class-types.component';
import { NewClassTypeComponent } from './manage-class-types/new-class-type/new-class-type.component';
import { NotificationHomeComponent } from './common-notification/notification-home/notification-home.component';
import { ManageNotificationTemplateComponent } from './common-notification/manage-notification-template/manage-notification-template.component';
import { ChangeLogComponent } from './common-notification/change-log/change-log.component';
import { AccessDeniedLogComponent } from './common-notification/access-denied-log/access-denied-log.component';
import { ViewTimeEntriesComponent } from './manage-employees/manage-time-entries/manage-members/view-time-entries/view-time-entries.component';
import { NotificationService } from 'app/modules/admin/services/notification.service';
import { ContractTemplateHomeComponent } from './manage-contracts/contract-template-home/contract-template-home.component';
import { AddNewContractTemplateComponent } from './manage-contracts/add-new-contract-template/add-new-contract-template.component';
import { ArticleHomeComponent } from './manage-article/article/article-home/article-home.component';
import { InventoryHomeComponent } from './manage-article/inventory/inventory-home/inventory-home.component';
import { InventoryDetailsComponent } from './manage-article/inventory/inventory-details/inventory-details.component';
import { MembershipModule } from '../membership/membership.module';
import { NewArticleComponent } from './manage-article/article/article-home/new-article/new-article.component'
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Observable, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { CKEditorModule } from 'ngx-ckeditor';
import { MyCalendarHomeComponent } from './my-calendar-home/my-calendar-home.component';
import { ManageSeasonsComponent } from './manage-seasons/manage-seasons.component';
import { SeasonCalandarComponent } from './manage-seasons/season-calandar/season-calandar.component';
import { ClassModule } from 'app/modules/class/class.module';
import { ManageSeasonsService } from 'app/modules/admin/manage-seasons/manage-seasons.service';
import { ContractItemSelectComponent } from './manage-contracts/contract-item-select/contract-item-select.component';
import { GymEmployeeHomeComponent } from './manage-employees/manage-gym-employees/gym-employee-home/gym-employee-home.component';
import { AddGymEmployeeComponent } from './manage-employees/manage-gym-employees/add-gym-employees/add-gym-employee.component';
import { GymEmployeeInfoComponent } from './manage-employees/manage-gym-employees/gym-employee-info/gym-employee-info.component';
import { ResourceHomeComponent } from './manage-resources/resource-home/resource-home.component';
import { ResourceBookingComponent } from './manage-resources/resource-booking/resource-booking.component';
import { ManageTimesComponent } from './manage-resources/manage-times/manage-times.component';
import { EmployeeInfoRolesComponent } from './manage-employees/manage-gym-employees/employee-info-roles/employee-info-roles.component';
import { EmployeeInfoWorkComponent } from './manage-employees/manage-gym-employees/employee-info-work/employee-info-work.component';
import { EmployeeInfoBookingsComponent } from './manage-employees/manage-gym-employees/employee-info-bookings/employee-info-bookings.component';
import { EmployeeInfoClassesComponent } from './manage-employees/manage-gym-employees/employee-info-classes/employee-info-classes.component';
import { EmployeeInfoFollowupComponent } from './manage-employees/manage-gym-employees/employee-info-followup/employee-info-followup.component';
import { EmployeeInfoJobComponent } from './manage-employees/manage-gym-employees/employee-info-job/employee-info-job.component';
import { EmployeeInfoEventsComponent } from './manage-employees/manage-gym-employees/employee-info-events/employee-info-events.component';
import { EmployeeInfoTimeComponent } from './manage-employees/manage-gym-employees/employee-info-time/employee-info-time.component';
import { GymEmployeeTimesComponent } from './manage-employees/gym-employee-times/gym-employee-times.component';
import { ManageJobsComponent } from './manage-jobs/manage-jobs.component';
import { ManageSponsorshipsComponent } from './manage-sponsorships/manage-sponsorships.component';
import { ManageOperationsComponent } from './manage-operations/manage-operations.component';
import { JobScheduleComponent } from './job-schedule/job-schedule.component';
import { EmployeeCardComponent } from './manage-employees/manage-gym-employees/employee-card.component';
import { EmployeeCardService } from './manage-employees/manage-gym-employees/employee-card.service';
import { EmployeeBasicInfoComponent } from './manage-employees/manage-gym-employees/employee-basic-info/employee-basic-info.component';
import { EmployeeBasicInfoService } from './manage-employees/manage-gym-employees/employee-basic-info/employee-basic-info.service';

import { ResourceCardComponent } from './manage-resources/resource-card/resource-card.component';
import { RcBasicInfoComponent } from './manage-resources/resource-card/rc-basic-info/rc-basic-info.component';
import { RcScheduleComponent } from './manage-resources/resource-card/rc-schedule/rc-schedule.component';
import { RcBasicInfoService } from './manage-resources/resource-card/rc-basic-info/rc-basic-info.service';
import { ResourceCardService } from './manage-resources/resource-card/resource-card.service';
import { EmployeeCalendarComponent } from './manage-employees/manage-gym-employees/gym-employee-home/employee-calendar/employee-calendar.component';
import { GymEmployeeHomeService } from './manage-employees/manage-gym-employees/gym-employee-home/gym-employee-home.service';
import { AddNewBookingComponent } from './manage-resources/resource-booking/add-new-booking/add-new-booking.component';
import { CalRightPanelComponent } from './my-calendar-home/cal-right-panel/cal-right-panel.component';
import { MyCalendarComponent } from './my-calendar-home/my-calendar/my-calendar.component';
// import { AngularSplitModule } from 'angular-split';
import { AngularSplitModule } from 'angular-split-ng6';
import { CalMemberFollowupComponent } from './my-calendar-home/cal-member-followup/cal-member-followup.component';
import { ManageDiscountComponent } from './manage-discount/manage-discount.component';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { ExceCommonModule } from '../common/exce-common.module';
import { ManageClassKeywordsComponent } from './manage-class-keywords/manage-class-keywords.component';
import { ShopModule } from '../shop/shop.module';
import { McShopComponent } from '../membership/member-card/mc-shop/mc-shop.component';
import { McShopAccountComponent } from '../membership/member-card/mc-shop/mc-shop-account/mc-shop-account.component';
import { AgGridModule } from 'ag-grid-angular';

// export function createTranslateLoader(http: HttpClient) {
//   return new TranslateHttpLoader(http, './assets/i18n/modules/admin/', '.json');
// }

export function translateLoader(http: HttpClient) {

  return new MultiTranslateHttpLoader(http, [
    { prefix: './assets/i18n/', suffix: '.json' },
    { prefix: './assets/i18n/modules/admin/', suffix: '.json' },
    { prefix: './assets/i18n/modules/class/', suffix: '.json' },
    { prefix: './assets/i18n/modules/membership/', suffix: '.json' },
    { prefix: './assets/i18n/modules/common-notification/', suffix: '.json' }
  ]);
}

export class MultiTranslateHttpLoader implements TranslateLoader {

  constructor(private http: HttpClient,
    public resources: { prefix: string, suffix: string }[] = [
      {
        prefix: '/assets/i18n/',
        suffix: '.json'
      }
    ]) { }

  /**
   * Gets the translations from the server
   * @param lang
   * @returns {any}
   */
  public getTranslation(lang: string): any {

    return forkJoin(this.resources.map(config => {
      return this.http.get(`${config.prefix}${lang}${config.suffix}`);
    })).pipe(
      map(response => {
        return response.reduce((a, b) => {
          return Object.assign(a, b);
        });
      }))
  }
}


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ShopModule,
    ClassModule.forRoot(),
    ReactiveFormsModule,
    AdminRoutingModule,
    AgGridModule.withComponents(null),
    MembershipModule,
    AngularSplitModule,
    CKEditorModule,
    ExceCommonModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (translateLoader),
        deps: [HttpClient]
      }
    })
  ],
  declarations: [
    AdminRoutingComponents,
    ActivityUnavailabletimeComponent,
    GymInfoComponent,
    GymOpentimeComponent,
    SmsSettingsComponent,
    GymSettingsComponent,
    PosHwProfileComponent,
    RequiredSettingsComponent,
    EcconomySettingsComponent,
    SearchSettingsComponent,
    IntegrationSettingsComponent,
    AccessSettingsComponent,
    OtherSettingsComponent,
    GymListComponent,
    ManageTasksComponent,
    ManageTemplatesComponent,
    GatIntegrationComponent,
    AccessControlIntegrationComponent,
    WellnessComponent,
    ExorliveComponent,
    InfoGymComponent,
    ArxComponent,
    AccessControlTypeComponent,
    ManageAnnonymizingComponent,
    ManageClassTypesComponent,
    NewClassTypeComponent,
    NotificationHomeComponent,
    ManageNotificationTemplateComponent,
    ChangeLogComponent,
    AccessDeniedLogComponent,
    ViewTimeEntriesComponent,
    ContractTemplateHomeComponent,
    AddNewContractTemplateComponent,
    ArticleHomeComponent,
    InventoryHomeComponent,
    InventoryDetailsComponent,
    NewArticleComponent,
    MyCalendarHomeComponent,
    ManageSeasonsComponent,
    SeasonCalandarComponent,
    ContractItemSelectComponent,
    GymEmployeeHomeComponent,
    AddGymEmployeeComponent,
    GymEmployeeInfoComponent,
    ResourceHomeComponent,
    ResourceBookingComponent,
    ManageTimesComponent,
    EmployeeInfoRolesComponent,
    EmployeeInfoWorkComponent,
    EmployeeInfoBookingsComponent,
    EmployeeInfoClassesComponent,
    EmployeeInfoFollowupComponent,
    EmployeeInfoJobComponent,
    EmployeeInfoEventsComponent,
    EmployeeInfoTimeComponent,
    GymEmployeeTimesComponent,
    ManageJobsComponent,
    ManageSponsorshipsComponent,
    ManageOperationsComponent,
    JobScheduleComponent,
    EmployeeCardComponent,
    EmployeeBasicInfoComponent,
    ResourceCardComponent,
    RcBasicInfoComponent,
    RcScheduleComponent,
    EmployeeCalendarComponent,
    AddNewBookingComponent,
    CalRightPanelComponent,
    MyCalendarComponent,
    CalMemberFollowupComponent,
    ManageDiscountComponent,
    AdminHomeComponent,
    ManageClassKeywordsComponent

  ],
  providers:
    [
      AdminService,
      GymSettingsService,
      AccessControlTypeService,
      NotificationService,
      ManageSeasonsService,
      EmployeeCardService,
      EmployeeBasicInfoService,
      RcBasicInfoService,
      ResourceCardService,
      TranslateService,
      GymEmployeeHomeService
    ]
})

export class AdminModule { }

