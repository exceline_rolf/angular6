import { ManageClassKeywordsComponent } from './manage-class-keywords/manage-class-keywords.component';
import { NgModule, Inject } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IAppConfig } from '../../app-config/app-config.interface';
import { APP_CONFIG } from '../../app-config/app-config.constants';
import { ExceToolbarService } from '../common/exce-toolbar/exce-toolbar.service';
import { SystemSettingsComponent } from './system-settings/system-settings.component';
import { AccessProfilesComponent } from './system-settings/access-profiles/access-profiles.component';
import { BasicInfoComponent } from './system-settings/basic-info/basic-info.component';
import { ContractConditionsComponent } from './system-settings/contract-conditions/contract-conditions.component';
import { EconomySettingsComponent } from './system-settings/economy-settings/economy-settings.component';
import { OtherSettingsComponent } from './system-settings/other-settings/other-settings.component';
import { VatSettingsComponent } from './system-settings/vat-settings/vat-settings.component';
import { ManageCategoriesComponent } from './manage-categories/manage-categories.component';
import { ManageActivitiesComponent } from './manage-activities/manage-activities.component';
import { ManageBranchesComponent } from './manage-branches/manage-branches.component';
import { ManageTimeComponent } from './manage-time/manage-time.component';
import { ActivityUnavailabletimeComponent } from './manage-activities/activity-unavailabletime/activity-unavailabletime.component';
import { GymSettingsComponent } from '../admin/gym-settings/gym-settings.component';
import { GymInfoComponent } from './gym-settings/gym-info/gym-info.component';
import { ManageTasksComponent } from './manage-followups/manage-followups-tasks/manage-followups-tasks.component';
import { ManageTemplatesComponent } from './manage-followups/manage-followups-templates/manage-followups-templates.component';
import { ManageAnnonymizingComponent } from './manage-annonymizing/manage-annonymizing.component'
import { ManageClassTypesComponent } from './manage-class-types/manage-class-types.component';
import { NewClassTypeComponent } from './manage-class-types/new-class-type/new-class-type.component'
import { NotificationHomeComponent } from './common-notification/notification-home/notification-home.component';
import { ManageNotificationTemplateComponent } from './common-notification/manage-notification-template/manage-notification-template.component';
import { ChangeLogComponent } from './common-notification/change-log/change-log.component';
import { AccessDeniedLogComponent } from './common-notification/access-denied-log/access-denied-log.component';
import { ViewTimeEntriesComponent } from './manage-employees/manage-time-entries/manage-members/view-time-entries/view-time-entries.component';
import { ContractTemplateHomeComponent } from 'app/modules/admin/manage-contracts/contract-template-home/contract-template-home.component';
import { ArticleHomeComponent } from 'app/modules/admin/manage-article/article/article-home/article-home.component';
import { InventoryHomeComponent } from 'app/modules/admin/manage-article/inventory/inventory-home/inventory-home.component';
import { TranslateService } from '@ngx-translate/core';
import { MyCalendarHomeComponent } from './my-calendar-home/my-calendar-home.component';
import { NewArticleComponent } from './manage-article/article/article-home/new-article/new-article.component';
import { ManageSeasonsComponent } from './manage-seasons/manage-seasons.component';
import { SeasonCalandarComponent } from './manage-seasons/season-calandar/season-calandar.component';
import { ResourceHomeComponent } from './manage-resources/resource-home/resource-home.component';
import { ResourceBookingComponent } from './manage-resources/resource-booking/resource-booking.component';
import { ManageTimesComponent } from './manage-resources/manage-times/manage-times.component';
import { GymEmployeeHomeComponent } from './manage-employees/manage-gym-employees/gym-employee-home/gym-employee-home.component';
import { AddGymEmployeeComponent } from './manage-employees/manage-gym-employees/add-gym-employees/add-gym-employee.component';
import { GymEmployeeInfoComponent } from './manage-employees/manage-gym-employees/gym-employee-info/gym-employee-info.component';
import { ManageJobsComponent } from './manage-jobs/manage-jobs.component';
import { ManageSponsorshipsComponent } from './manage-sponsorships/manage-sponsorships.component';
import { ManageOperationsComponent } from './manage-operations/manage-operations.component';

import { EmployeeInfoRolesComponent } from './manage-employees/manage-gym-employees/employee-info-roles/employee-info-roles.component';
import { EmployeeInfoWorkComponent } from './manage-employees/manage-gym-employees/employee-info-work/employee-info-work.component';
import { EmployeeInfoBookingsComponent } from './manage-employees/manage-gym-employees/employee-info-bookings/employee-info-bookings.component';

import { EmployeeInfoClassesComponent } from './manage-employees/manage-gym-employees/employee-info-classes/employee-info-classes.component';
import { EmployeeInfoFollowupComponent } from './manage-employees/manage-gym-employees/employee-info-followup/employee-info-followup.component';
import { EmployeeInfoJobComponent } from './manage-employees/manage-gym-employees/employee-info-job/employee-info-job.component';
import { EmployeeInfoEventsComponent } from './manage-employees/manage-gym-employees/employee-info-events/employee-info-events.component';
import { EmployeeInfoTimeComponent } from './manage-employees/manage-gym-employees/employee-info-time/employee-info-time.component';

import { GymEmployeeTimesComponent } from './manage-employees/gym-employee-times/gym-employee-times.component';
import { JobScheduleComponent } from './job-schedule/job-schedule.component';
import { EmployeeCardComponent } from 'app/modules/admin/manage-employees/manage-gym-employees/employee-card.component';

import { ResourceCardComponent } from './manage-resources/resource-card/resource-card.component';
import { RcBasicInfoComponent } from './manage-resources/resource-card/rc-basic-info/rc-basic-info.component'
import { RcScheduleComponent } from './manage-resources/resource-card/rc-schedule/rc-schedule.component';
import { InventoryDetailsComponent } from './manage-article/inventory/inventory-details/inventory-details.component';
import { EmployeeCalendarComponent } from 'app/modules/admin/manage-employees/manage-gym-employees/gym-employee-home/employee-calendar/employee-calendar.component';
import { CalMemberFollowupComponent } from './my-calendar-home/cal-member-followup/cal-member-followup.component';
import { ManageDiscountComponent } from './manage-discount/manage-discount.component';
import { AdminHomeComponent } from './admin-home/admin-home.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: AdminHomeComponent },
  { path: 'system-settings', component: SystemSettingsComponent },
  { path: 'manage-activities', component: ManageActivitiesComponent },
  { path: 'manage-categories', component: ManageCategoriesComponent },
  { path: 'manage-branches', component: ManageBranchesComponent },
  { path: 'manage-time', component: ManageTimeComponent },
  { path: 'activity-unavailabletime', component: ActivityUnavailabletimeComponent },
  { path: 'gym-settings', component: GymSettingsComponent },
  { path: 'gym-info', component: GymInfoComponent },
  { path: 'manage-followups-tasks', component: ManageTasksComponent },
  { path: 'manage-followups-templates', component: ManageTemplatesComponent },
  { path: 'manage-annonymizing', component: ManageAnnonymizingComponent },
  { path: 'manage-class-types', component: ManageClassTypesComponent },
  { path: 'notification-home', component: NotificationHomeComponent },
  { path: 'manage-notification-template', component: ManageNotificationTemplateComponent },
  { path: 'change-log', component: ChangeLogComponent },
  { path: 'access-denied-log', component: AccessDeniedLogComponent },
  { path: 'view-time-entries', component: ViewTimeEntriesComponent },
  { path: 'contract-template-home', component: ContractTemplateHomeComponent },
  { path: 'article-home', component: ArticleHomeComponent },
  { path: 'inventory-home', component: InventoryHomeComponent },
  { path: 'my-calandar-home', component: MyCalendarHomeComponent },
  { path: 'manage-seasons', component: ManageSeasonsComponent },
  { path: 'manage-seasons/season-calandar/:Id', component: SeasonCalandarComponent },
  { path: 'gym-employee-home', component: GymEmployeeHomeComponent },
  { path: 'gym-employee-home/calendar', component: EmployeeCalendarComponent },
  { path: 'gym-employee-home/calendar/:employeename', component: CalMemberFollowupComponent },
  { path: 'add-gym-employee', component: AddGymEmployeeComponent },
  { path: 'gym-employee-info', component: GymEmployeeInfoComponent },
  { path: 'resource-home', component: ResourceHomeComponent },
  { path: 'resource-booking', component: ResourceBookingComponent },
  { path: 'manage-times', component: ManageTimesComponent },
  { path: 'gym-employee-times', component: GymEmployeeTimesComponent },
  { path: 'manage-jobs', component: ManageJobsComponent },
  { path: 'manage-sponsorships', component: ManageSponsorshipsComponent },
  { path: 'manage-operations', component: ManageOperationsComponent },
  { path: 'job-schedule', component: JobScheduleComponent },
  { path: 'manage-discount', component: ManageDiscountComponent },
  { path: 'inventory-home/inventory-detail/:Id', component: InventoryDetailsComponent },
  { path: 'employee-card/:Id/:BranchId', component: EmployeeCardComponent,
    children: [
      { path: 'employee-info-roles', component: EmployeeInfoRolesComponent },
      { path: 'employee-info-work', component: EmployeeInfoWorkComponent },
      { path: 'employee-info-bookings', component: EmployeeInfoBookingsComponent },
      { path: 'employee-info-classes', component: EmployeeInfoClassesComponent },
      { path: 'employee-info-followup', component: EmployeeInfoFollowupComponent },
      { path: 'employee-info-job', component: EmployeeInfoJobComponent },
      { path: 'employee-info-events', component: EmployeeInfoEventsComponent },
      { path: 'employee-info-time', component: EmployeeInfoTimeComponent },
    ]
  },

  { path: 'resource-card/:Id', component: ResourceCardComponent,
    children: [
      { path: 'rc-schedule', component: RcScheduleComponent },
    ]
  },
  {path: 'manage-class-keywords', component: ManageClassKeywordsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {
  constructor(
    private translate: TranslateService,
    private exceToolbarService: ExceToolbarService,
    @Inject(APP_CONFIG) private config: IAppConfig
  ) {
    const langs = [];
    this.config.LANGUAGES.map(ln => {
      langs.push(ln.ID);
    })
    this.translate.addLangs(langs);
    translate.setDefaultLang(this.config.LANGUAGE_DEFAULT.ID);

    const lang = this.exceToolbarService.getSelectedLanguage();
    this.translate.use(lang.id);
    this.exceToolbarService.langUpdated.subscribe(
      (res) => {
        this.translate.use(res.id);
      }
    );
  }
}

export const AdminRoutingComponents = [
  SystemSettingsComponent,
  AccessProfilesComponent,
  BasicInfoComponent,
  ContractConditionsComponent,
  EconomySettingsComponent,
  OtherSettingsComponent,
  VatSettingsComponent,
  ManageCategoriesComponent,
  ManageActivitiesComponent,
  ManageBranchesComponent,
  ManageCategoriesComponent,
  ManageBranchesComponent,
  ManageTimeComponent,
  ActivityUnavailabletimeComponent,
  GymSettingsComponent,
  GymInfoComponent,
  ManageTasksComponent,
  ManageTemplatesComponent,
  ManageAnnonymizingComponent,
  ManageClassTypesComponent,
  NewClassTypeComponent,
  NotificationHomeComponent,
  ManageNotificationTemplateComponent,
  ChangeLogComponent,
  AccessDeniedLogComponent,
  ViewTimeEntriesComponent,
  ContractTemplateHomeComponent,
  ArticleHomeComponent,
  InventoryHomeComponent,
  NewArticleComponent,
  GymEmployeeHomeComponent,
  AddGymEmployeeComponent,
  GymEmployeeInfoComponent,
  ResourceHomeComponent,
  ResourceBookingComponent,
  ManageTimesComponent,
  EmployeeInfoRolesComponent,
  EmployeeInfoWorkComponent,
  EmployeeInfoBookingsComponent,
  EmployeeInfoClassesComponent,
  EmployeeInfoFollowupComponent,
  EmployeeInfoJobComponent,
  EmployeeInfoEventsComponent,
  EmployeeInfoTimeComponent,
  GymEmployeeTimesComponent,
  ManageJobsComponent,
  ManageSponsorshipsComponent,
  ManageOperationsComponent,
  JobScheduleComponent,
  EmployeeCardComponent,
  ResourceCardComponent,
  RcBasicInfoComponent,
  RcScheduleComponent,
  ManageDiscountComponent
]
