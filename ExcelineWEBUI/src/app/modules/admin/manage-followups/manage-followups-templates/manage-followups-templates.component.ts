import { Component, OnInit, OnDestroy } from '@angular/core';
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { AdminService } from '../../services/admin.service';
import { DataTableFilterPipe } from '../../../../shared/components/us-data-table/tools/data-table-filter-pipe';
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { TranslateService } from '@ngx-translate/core';
import { ExceBreadcrumbService } from '../../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { UsErrorService } from 'app/shared/directives/us-error/us-error.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-manage-followups-templates',
  templateUrl: './manage-followups-templates.component.html',
  styleUrls: ['./manage-followups-templates.component.scss']
})

export class ManageTemplatesComponent implements OnInit, OnDestroy {
  private branchId: number = JSON.parse(Cookie.get('selectedBranch')).BranchId;
  private items = [];
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  private Category: any[];
  private CategoryId: any[];
  private manageTemplateObj: any;
  private Id: any;
  private Name: any;
  private DefaultValue: any;
  private data?: any[] = [];
  private typeValue: any;
  private newFollowUpItems = [];
  private modalReferenceTemplateModal?: any;
  private modalReferenceTaskModel?: any;
  private selectedCategory: any;
  private isEdit = false;
  private IsNextFollowUpChecked = false;
  private isDelete = true;
  private allValues = [];
  private taskListObj: any;
  private deletedItem: any;
  private deletedItemId: any;
  private modal: any;
  private destroy$ = new Subject<void>();

  templates?: any[] = [];
  categoriesByType: any;
  taskCategories: any;
  addNewFollowUpTemplateForm: FormGroup;
  addFollowUpTaskForm: FormGroup;
  selectedTaskCategory: any;
  myFormSubmited = false;

  formErrors = {
    'Text': ''
  };
  validationMessages = {
    'Text': {
      'required': 'ADMIN.Required'
    }
  };

  constructor(
    private modalService: UsbModal,
    private adminService: AdminService,
    private fb: FormBuilder,
    private translateService: TranslateService,
    private exceMessageService: ExceMessageService,
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ADMIN.MonitoringTemplatesC',
      url: '/adminstrator/manage-followups-templates'
    });

    this.addNewFollowUpTemplateForm = fb.group({
      'Id': [-1],
      'Name': [null],
      'Category': [this.selectedCategory],
      'IsDefault': [null]
    });

    this.addFollowUpTaskForm = fb.group({
      'Id': [-1],
      'Text': [null, [Validators.pattern('.*\\S.*'), Validators.required]],
      'ExcelineTaskCategory': [this.selectedTaskCategory],
      'NumberOfDays': [0]
    });

    this.exceMessageService.yesCalledHandle.pipe(
      takeUntil(this.destroy$)
    ).subscribe((value) => {
      this.yesHandler();
    });
    this.exceMessageService.noCalledHandle.pipe(
      takeUntil(this.destroy$)
    ).subscribe((value) => {
      this.noHandler();
    });
    this.exceMessageService.okCalledHandle.pipe(
      takeUntil(this.destroy$)
    ).subscribe((value) => {
      this.okHandler();
    });
  }

  ngOnInit() {

    this.getFollowUpTemplates();
    this.getCategoryNames();
    this.getTaskCategories();
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngAfterContentInit() {
    this.addFollowUpTaskForm.statusChanges.pipe(
      takeUntil(this.destroy$)
    ).subscribe(_ => {
      UsErrorService.onValueChanged(this.addFollowUpTaskForm, this.formErrors, this.validationMessages)
    });
  }

  // get Task Categories data for the data table
  getFollowUpTemplates() {
    this.adminService.getFollowUpTemplates(this.branchId).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.templates = [];
        for (let i = 0; i < result.Data.length; i++) {
          this.Category = result.Data[i].Category.Name;
          this.CategoryId = result.Data[i].Category.Id;
          this.Id = result.Data[i].Id;
          this.Name = result.Data[i].Name;
          this.DefaultValue = result.Data[i].IsDefault;
          this.allValues = result.Data[i];

          this.manageTemplateObj = {
            Id: this.Id,
            Name: this.Name,
            Category: this.Category,
            CategoryId: this.CategoryId,
            IsDefault: this.DefaultValue,
            template: this.allValues
          }
          this.templates.push(this.manageTemplateObj);
          this.items = this.templates;
        }
      }
    }, error => {
      console.log(error);
    });
  }

  // get the category names for search drop box and drop box in ad new follow up template
  getCategoryNames() {
    this.adminService.getCategoriesByType('FOLLOWUP').pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.categoriesByType = result.Data;
      }
    }, error => {
      console.log(error);
    });
  }

  // get Task Categories for the drop box in add follow up task
  getTaskCategories() {
    this.adminService.getTaskCategories(this.branchId).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.taskCategories = result.Data;
      }
    }, error => {
      console.log(error);
    });
  }

  // to open new template modal
  openNewTemplateModal(content: any, isEdit: boolean, isDelete: boolean) {
    this.newFollowUpItems = [];
    this.addNewFollowUpTemplateForm.reset();
    this.isEdit = isEdit;
    this.isDelete = isDelete;
    this.selectedCategory = this.categoriesByType[0];
    this.modalReferenceTemplateModal = this.modalService.open(content, { width: '600' });
  }

  // to open followup task modal
  openFollowupTaskModel(content: any, IsNextFollowUpChecked: boolean) {
    this.IsNextFollowUpChecked = IsNextFollowUpChecked;
    this.selectedTaskCategory = this.taskCategories[0];
    this.addFollowUpTaskForm.controls['NumberOfDays'].setValue(0);
    this.modalReferenceTaskModel = this.modalService.open(content, { width: '400' });
  }

  // filter by name (search field)
  filterInputByName(param?: any) {
    this.templates = this.filterpipe.transform('Name', 'NAMEFILTER', this.items, param);
  }

  // filter by type (drop down list)
  filterInputBySelect(typeValue?: any) {
    if (typeValue === 'ALL') {
      this.templates = this.items;
    } else {
      this.templates = this.filterpipe.transform('Category', 'SELECT', this.items, typeValue);
    }
  }

  // to add new follow up tasks to the table
  newFollowUpTask(value) {

    if (value.Text !== null) {
      value.Text = value.Text.trim();
    }

    if (value.Text == null || value.Text == '') {
      this.addFollowUpTaskForm.controls['Text'].setErrors({ 'required': true });
    } else {
      this.addFollowUpTaskForm.controls['Text'].setErrors(null);
    }

    this.myFormSubmited = true;
    if (this.addFollowUpTaskForm.valid) {

      value.TaskCategoryName = this.selectedTaskCategory.Name;
      value.Id = this.selectedTaskCategory.Id;
      this.newFollowUpItems.push(value);
      this.addFollowUpTaskForm.reset();
      this.modalReferenceTaskModel.close();
      this.myFormSubmited = false;
    } else {
      UsErrorService.validateAllFormFields(this.addFollowUpTaskForm, this.formErrors, this.validationMessages);
    }

  }

  closeAddFollowupTaskForm() {
    this.addFollowUpTaskForm.reset();
    this.myFormSubmited = false;
  }

  // to get IsNextFollowUp value from the change event
  getIsNextFollowUpValue(value) {
    if (value.ExcelineTaskCategory.IsNextFollowUp === true) {
      this.IsNextFollowUpChecked = true;
    } else {
      this.IsNextFollowUpChecked = false;
    }
  }

  // delete tasks from the data table
  deleteTask(item) {
    this.newFollowUpItems.splice(this.newFollowUpItems.indexOf(item), 1);
  }

  // to save and update new task category to the database
  addFollowUpTemplate(value): any {
    value.FollowUpTaskList = this.newFollowUpItems;

    if (this.newFollowUpItems.length === 0) {
      let messageTitle = '';
      let messageBody = '';
      (this.translateService.get('Warning').pipe(
        takeUntil(this.destroy$)
      ).subscribe(tranlstedValue => messageTitle = tranlstedValue));
      (this.translateService.get('ADMIN.FollowUpTasksEmpty').pipe(
        takeUntil(this.destroy$)
      ).subscribe(tranlstedValue => messageBody = tranlstedValue));
      this.modal = this.exceMessageService.openMessageBox('WARNING',
        {
          messageTitle: messageTitle,
          messageBody: messageBody
        }
      );
    } else {
      if (value.IsDefault == null) {
        value.IsDefault = false;
      }

      const followUpTemplateObject = {
        template: value,
        branchId: this.branchId
      }

      this.adminService.addFollowUpTemplate(followUpTemplateObject).pipe(
        takeUntil(this.destroy$)
      ).subscribe(result => {
        if (result) {
          this.manageTemplateObj = {
            Id: -1,
            Name: followUpTemplateObject.template.Name,
            Category: followUpTemplateObject.template.Category.Name,
            CategoryId: followUpTemplateObject.template.Category.Id,
            DefaultValue: followUpTemplateObject.template.IsDefault
          }
          if (value.Id <= 0) {
            this.templates.push(this.manageTemplateObj);
          } else {
          }
          this.getFollowUpTemplates();
        }
      }, null, null);
      this.addNewFollowUpTemplateForm.reset();
      this.modalReferenceTemplateModal.close();
    }
  }

  rowDoubleClick(rowEvent, value) {
    const selectedItem = rowEvent.row.item;
    const selectedTemplate = rowEvent.row.item.template;

    /* get the IsDefault value and set Delete button visible or not
     if (IsDefault === true) do not show delete button else show */
    if (selectedItem.IsDefault === true) {
      this.isDelete = false;
    } else {
      this.isDelete = true;
    }
    const templateObj = this.templates.find(x => x.Id === selectedItem.Id);

    this.adminService.getFollowUpTaskByTemplateId(selectedItem.Id).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.newFollowUpItems = [];
        for (let i = 0; i < result.Data.length; i++) {
          const text = result.Data[i].Text;
          const days = result.Data[i].NumberOfDays;
          const taskCategory = result.Data[i].ExcelineTaskCategory;
          const id = result.Data[i].TaskCategoryId;
          const ExcelineTaskCategory = result.Data[i];

          this.taskListObj = {
            Id: id,
            Text: text,
            TaskCategoryName: result.Data[i].ExcelineTaskCategory.Name,
            ExcelineTaskCategory: taskCategory,
            NumberOfDays: days,
          }
          this.newFollowUpItems.push(this.taskListObj);
        }
      }
    }, error => {
      console.log(error);
    });

    this.selectedCategory = this.categoriesByType.find(x => x.Id === selectedTemplate.Category.Id);
    this.addNewFollowUpTemplateForm.patchValue(selectedItem);
  }

  // to delete template from the database
  deleteTemplate(value) {
    this.deletedItem = value;
    this.deletedItemId = value.Id;

    let messageTitle = '';
    let messageBody = '';
    (this.translateService.get('Confirm').pipe(
      takeUntil(this.destroy$)
    ).subscribe(tranlstedValue => messageTitle = tranlstedValue));
    (this.translateService.get('ADMIN.ConfirmMessage').pipe(
      takeUntil(this.destroy$)
    ).subscribe(tranlstedValue => messageBody = tranlstedValue));
    this.modal = this.exceMessageService.openMessageBox('CONFIRM',
      {
        messageTitle: messageTitle,
        messageBody: messageBody
      }
    );
  }

  yesHandler() {
    const index: number = this.items.indexOf(this.deletedItem);
    if (index !== -1) {
      this.items.splice(index, 1);
    } else {
    }

    this.adminService.deleteFollowUpTemplate(this.deletedItemId).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
    }, error => {
      console.log(error);
    });
    this.templates.splice(this.templates.indexOf(index), 1);
    this.closeNewTemplateModal();
  }

  noHandler() {
  }

  okHandler() {
  }

  closeNewTemplateModal() {
    this.addNewFollowUpTemplateForm.reset();
    this.modalReferenceTemplateModal.close();
  }

  ngOnDestroy(): void {
    if (this.modal) {
      this.modal.unsubscribe();
    }
    this.destroy$.next();
    this.destroy$.complete();
  }
}


