import { Injectable } from '@angular/core';
import { CommonHttpService } from '../../../shared/services/common-http.service';
import { ConfigService } from '@ngx-config/core';
import { Observable } from 'rxjs';
import { ExceLoginService } from '../../../modules/login/exce-login/exce-login.service';

@Injectable()
export class AdminService {

  categoryObj;
  private adminServiceUrl: string;
  private memberServiceUrl: string;
  private branchId: number;

  constructor(
    private commonHttpService: CommonHttpService,
    private config: ConfigService,
    private exceLoginService: ExceLoginService
  ) {
    this.adminServiceUrl = this.config.getSettings('EXCE_API.ADMIN');
    this.memberServiceUrl = this.config.getSettings('EXCE_API.MEMBER');
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.subscribe(
      (branch) => {
        this.branchId = branch.BranchId;
      });
  }

  getCategories(type: string): Observable<any> {
    const url = this.adminServiceUrl + 'Util/GetCategory?type=' + type
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  getCategoryTypes(branchId: number, type: string): Observable<any> {
    const url = this.adminServiceUrl + 'Util/GetCategoryTypes?branchId=' + branchId + '&&name' + type
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  saveCategory(newCategory: any): Observable<any> {
    const url = this.adminServiceUrl + 'Util/SaveCategory'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: newCategory
    });
  }

  updateCategory(editCategory: any): Observable<any> {
    const url = this.adminServiceUrl + 'Util/UpdateCategory'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: editCategory
    });
  }

  deleteCategory(deleteCategory: any): Observable<any> {
    const url = this.adminServiceUrl + 'Util/DeleteCategory'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: deleteCategory
    });
  }

  getArticles(categpryType: any, keyword: string, category: any, activityId: number,
    isActive: boolean, filterByGym: boolean, branchId?: number, isObsalate?: boolean): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetArticles'
    // const url = this.adminServiceUrl + 'Settings/GetArticles?branchId=' + (branchId ? branchId : this.branchId) + '&categpryType='
    //   + categpryType + '&keyword=' + keyword + '&category=' + category + '&activityId=' + activityId + '&isObsalate='
    //   + (isObsalate ? isObsalate : '') + '&isActive=' + isActive + '&filterByGym=' + filterByGym
    // return this.commonHttpService.makeHttpCall(url, {
    //   method: 'GET',
    //   auth: true
    // })

    const articleSearch: any = {};
    articleSearch.BranchId = branchId ? branchId : this.branchId
    articleSearch.CategpryType = categpryType;
    articleSearch.Keyword = keyword;
    articleSearch.Category = category;
    articleSearch.ActivityId = activityId;
    articleSearch.IsObsalate = isObsalate;
    articleSearch.IsActive = isActive;
    articleSearch.FilterByGym = filterByGym;

    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: articleSearch
    });
  }

  // used in manage-activities.component.ts --> ManageActivitiesComponent (get activity names)
  getActivities(branchId?): Observable<any> {
    const url = this.adminServiceUrl + 'Util/GetActivities?branchId=' + (branchId ? branchId : this.branchId);
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetTerminalTypes(): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetTerminalTypes';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetGymCompanySettings(settingType: string): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetGymCompanySettings?gymCompanySettingType=' + settingType
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  // used in manage-activities.component.ts --> Activity Settings
  AddActivitySettings(addActivity: any): Observable<any> {
    const url = this.adminServiceUrl + 'Util/AddActivitySettings'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: addActivity
    });
  }

  GetAccessProfiles(): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetAccessProfiles'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetRevenueAccounts(): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetRevenueAccounts'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  saveGymCompanyBasicSettings(companyBasicInfo: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/SaveGymCompanyBasicSettings'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: companyBasicInfo
    });
  }

  upDateStock(article: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/UpdateStock';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: article
    });
  }

  getCategoriesByType(type: string): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetCategories?type=' + type
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  getARXMembers(isallmember: any, branchId: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetArxMembers?isallmember=' + isallmember + '&branchId=' + branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetArxSettingDetail(branchId: number): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetArxSettingDetail?branchId=' + branchId
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetArxFormatType(): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetArxFormatType'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetSelectedGymSettings(gymSetColNames = [], isGymSettings, branchId): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetSelectedGymSettings?gymSetColNames=' + gymSetColNames + '&&isGymSettings=' + isGymSettings + '&&branchId=' + branchId
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }
  GetSelectedGymSettingsPost(gymSettings: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetSelectedGymSettings'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: gymSettings
    });
  }

  SaveArxSettingDetail(arxSettings: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/SaveArxSettingDetail'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: arxSettings
    });
  }

  saveGymCompanyOtherSettings(companyOtherSettings: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/SaveGymCompanyOtherSettings'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: companyOtherSettings
    });
  }

  saveGymCompanyVATSettings(vatCodeDetail: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/SaveGymCompanyVATSettings'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: vatCodeDetail
    });
  }

  saveGymCompanyEconomySettings(economySetting: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/SaveGymCompanyEconomySettings'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: economySetting
    });
  }

  SaveGymOpenTimes(openTimeSettings: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/SaveGymOpenTimes'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: openTimeSettings
    });
  }

  SaveGymSmsSettings(gymSmsSettings: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/SaveGymSmsSettings'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: gymSmsSettings
    });
  }

  SaveGymMemberSearchSettings(searchSettings: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/SaveGymMemberSearchSettings'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: searchSettings
    });
  }

  SaveGymRequiredSettings(requiredSettings: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/SaveGymRequiredSettings'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: requiredSettings
    });
  }

  SaveHardwareProfile(hardwareSettings: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/SaveHardwareProfile'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: hardwareSettings
    });
  }

  SaveGymEconomySettings(ecconomySettings: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/SaveGymEconomySettings'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: ecconomySettings
    });
  }

  SaveGymAccessSettings(accessSettings: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/SaveGymAccessSettings'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: accessSettings
    });
  }

  SaveGymOtherSettings(otherSettings: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/SaveGymOtherSettings'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: otherSettings
    });
  }
  SaveGymIntegrationSettings(GATSettings: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/SaveGymIntegrationSettings'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: GATSettings
    });
  }

  SaveGymIntegrationExcSettings(ExSettings: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/SaveGymIntegrationExcSettings'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: ExSettings
    });
  }

  AddUpdateOtherIntegrationSettings(otherSettings: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/AddUpdateOtherIntegrationSettings'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: otherSettings
    });
  }

  DeleteIntegrationSettingById(id: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/DeleteIntegrationSettingById'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: id
    });
  }

  getBranches(branchId?: number): Observable<any> {
    const url = this.adminServiceUrl + 'Util/GetBranches?branchId=' + (branchId ? branchId : this.branchId)
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  getBranchGroups(): Observable<any> {
    const url = this.adminServiceUrl + 'Util/GetBranchGroups'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  getRegions(countryId: any, branchId?: any): Observable<any> {
    const url = this.adminServiceUrl + 'Util/GetRegions?branchId=' + (branchId ? branchId : this.branchId) + '&&countryId=' + countryId
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  getCountryDetails(): Observable<any> {
    const url = this.adminServiceUrl + 'Util/GetCountryDetails'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  getGymSettings(gymSettingType: any, systemName: any, branchId?: any): Observable<any> {
    console.log('BRANCHID IN ADMIN SERVICE: branchId: ', branchId, ' this.branchId: ', this.branchId);
    const url = this.adminServiceUrl + 'Settings/GetGymSettings?branchId=' + (branchId ? branchId : this.branchId) + '&&gymSettingType='
      + gymSettingType + '&&systemName=' + systemName
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  updateSystemAccessProfiles(accessProfileList: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/UpdateAccessProfiles'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: accessProfileList
    });
  }

  ValidateCreditorNo(creditorNo: string): Observable<any> {
    const url = this.adminServiceUrl + 'Util/ValidateCreditorNo?branchId=' + this.branchId + '&&creditorNo=' + creditorNo
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  saveBranchDetails(newBranch: any): Observable<any> {
    const url = this.adminServiceUrl + 'Util/SaveBranchDetails'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: newBranch
    });
  }

  updateBranchDetails(editBranch: any): Observable<any> {
    const url = this.adminServiceUrl + 'Util/UpdateBranchDetails'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: editBranch
    });
  }

  // used in activity-unavailable.component.ts (fill the data table)
  getActivityTimes(isUnavailableTimes: boolean): Observable<any> {
    const url = this.adminServiceUrl + 'Util/getActivityTimes?branchId=' + this.branchId + '&&isUnavailableTimes=' + isUnavailableTimes
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  // add activity times for activity-unavailable time component
  AddActivityTimes(addActivityTime: any): Observable<any> {
    const url = this.adminServiceUrl + 'Util/AddActivityTimes'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: addActivityTime
    });
  }

  addUpdateCountryDetails(country: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/AddCountryDetails'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: country
    });
  }

  addUpdateRegionDetails(region: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/AddRegionDetails'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: region
    });
  }

  getTaskCategories(branchId: any): Observable<any> {
    const url = this.adminServiceUrl + 'Util/GetTaskCategories?branchId=' + (branchId ? branchId : this.branchId);
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  getExtFieldsByCategory(categoryId: any, categoryType: any): Observable<any> {
    const url = this.adminServiceUrl + 'Util/GetExtFieldsByCategory?categoryId=' + categoryId + '&&categoryType=' + categoryType;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  saveTaskCategory(taskCategory: any): Observable<any> {
    const url = this.adminServiceUrl + 'Util/SaveTaskCategory'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: taskCategory
    });
  }

  deleteTaskCategory(taskCategoryId: any): Observable<any> {
    const url = this.adminServiceUrl + 'Util/DeleteTaskCategory?taskCategoryId=' + taskCategoryId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  getFollowUpTemplates(branchId: any): Observable<any> {
    const url = this.adminServiceUrl + 'Util/GetFollowUpTemplates?branchId=' + (branchId ? branchId : this.branchId);
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  getClassTypes(): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetClassTypes'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  // to save follow up template
  addFollowUpTemplate(followUpTemplate: any): Observable<any> {
    const url = this.adminServiceUrl + 'Util/AddFollowUpTemplate'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: followUpTemplate
    });
  }

  // to get follow up tasks
  getFollowUpTaskByTemplateId(followUpTemplateId: any): Observable<any> {
    const url = this.adminServiceUrl + 'Util/GetFollowUpTaskByTemplateId?followUpTemplateId=' + followUpTemplateId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  // to Delete FollowUp Template from the DB
  deleteFollowUpTemplate(followUpTemplateId: any): Observable<any> {
    const url = this.adminServiceUrl + 'Util/DeleteFollowUpTemplate?followUpTemplateId=' + followUpTemplateId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  DeleteAnonymizingData(annonymizing): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/DeleteAnonymizingData'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: annonymizing
    });
  }

  GetAnonymizingData(): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetAnonymizingData'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  AddEditClassType(classType): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/AddEditClassType'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: classType
    });
  }

  DeleteClassType(id): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/DeleteClassType'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: id
    });
  }

  GetAccessPoints(): Observable<any> {
    const url = this.adminServiceUrl + 'Util/GetAccessPoints'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetMemberVisits(params: any): Observable<any> {
    const url = this.adminServiceUrl + 'Util/GetMemberVisits?startDate=' + params.startDate + '&&endDate=' + params.endDate
      + '&&branchId=' + this.branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  AddUpdateGymCompanySettings(GymCompanySettings): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/AddUpdateGymCompanySettings'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: GymCompanySettings
    });
  }

  SaveGymCompanyContractSettings(GymCompanySettings): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/SaveGymCompanyContractSettings'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: GymCompanySettings
    });
  }

  GetContracts(getCon: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetContracts'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: getCon
    })
  }

  GetArticlesForCategory(categoryID: number, branchID: number): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetArticlesForCategory?categoryID=' + categoryID + '&&branchID=' + branchID
      + '&&branchId=' + this.branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  saveContract(contract: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/SaveContract'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: contract
    })
  }

  updatePriority(priorityList: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/UpdateContractPriority'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: priorityList
    })
  }

  GetGymEmployees(branchId: number, searchText: string, isActive: boolean, roleId: number): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetGymEmployees?branchId=' + branchId + '&&searchText=' + searchText + '&&isActive=' + isActive + '&&roleId=' + roleId
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetGymEmployeeByEmployeeId(branchId: number, employeeId: number): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetGymEmployeeByEmployeeId?branchId=' + branchId + '&&employeeId=' + employeeId
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetGymEmployeeRolesById(branchId: number, employeeId: number): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetGymEmployeeRolesById?employeeId=' + employeeId + '&&branchId=' + branchId
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  SaveEmployeesRoles(employee: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/SaveEmployeesRoles'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: employee
    })
  }

  // to get booking resources
  GetResources(branchId: number, searchText: string, categoryId: number, activityId: number, equipmentid: number, isActive: boolean): Observable<any> {
    const url = this.adminServiceUrl + 'Util/GetResources?branchId=' + this.branchId + '&&searchText=' + searchText + '&&categoryId=' + categoryId
      + '&&activityId=' + activityId + '&&equipmentid=' + equipmentid + '&&isActive=' + isActive;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  // change resource on booking
  ChangeResourceOnBooking(scheduleItemId: number, resourceId: number, comment: string): Observable<any> {
    const url = this.adminServiceUrl + 'Util/ChangeResourceOnBooking?scheduleItemId=' + scheduleItemId + '&&resourceId=' + resourceId + '&&comment=' + comment +
    '&&branchId=' + this.branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  SaveGymEmployee(employee: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/SaveGymEmployee';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: employee
    })
  }

  GetExcelineEmpRoles(): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetExcelineEmpRoles'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  ValidateEmployeeFollowUp(followUp: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/ValidateEmployeeFollowUp'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: followUp
    })
  }

  DeleteGymEmployee(gymEmp: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/DeleteGymEmployee'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: gymEmp
    })
  }

  SaveResources(resource: any): Observable<any> {
    const url = this.adminServiceUrl + 'Util/SaveResources'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: resource
    });
  }

  GetArticleForResouce(activityId: number): Observable<any> {
    const url = this.adminServiceUrl + 'Util/GetArticleForResouce?activityId=' + activityId + '&&branchId=' + this.branchId
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  GetBookingsOnDeletedScheduleItem(resourceId: number): Observable<any> {
    const url = this.adminServiceUrl + 'Util/GetBookingsOnDeletedScheduleItem?resourceId=' + resourceId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  GetResourceDetailsById(resourceId: number): Observable<any> {
    const url = this.adminServiceUrl + 'Util/GetResourceDetailsById?resourceId=' + resourceId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  AddEventToNotifications(Notification: any): Observable<any> {
    const url = this.adminServiceUrl + 'GMSJobs/AddEventToNotifications';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: Notification
    });
  }

  GetEmployeeEvents(employeeId: number): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetEmployeeEvents?employeeId=' + employeeId
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetJobCategories(branchId: number): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetJobCategories?branchId=' + branchId
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }
  GetEmployeeJobs(employeeId: number): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetEmployeeJobs?employeeId=' + employeeId
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetEmployeeClasses(empId: number): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetEmployeeClasses?empId=' + empId
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  UpdateApproveStatus(classObj: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/UpdateApproveStatus'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: classObj
    })
  }

  GetFollowUpDetailByEmpId(empId: number): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetFollowUpDetailByEmpId?empId=' + empId
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  UpdateResource(resource: any): Observable<any> {
    const url = this.adminServiceUrl + 'Util/UpdateResource'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: resource
    });
  }

  GetEmployeeTimeEntries(employeeId: number): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetEmployeeTimeEntries?employeeId=' + employeeId
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  SaveEntityTimeEntry(timeEntry: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/SaveEntityTimeEntry'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: timeEntry
    });
  }

  GetContractSummariesByEmployee(employeeId: number, branchId: number, createdDate: string): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetContractSummariesByEmployee?employeeId=' + employeeId + '&&branchId=' + branchId + '&&createdDate=' + createdDate
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  AddEmployeeTimeEntry(timeEntry: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/AddEmployeeTimeEntry'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: timeEntry
    });
  }

  ValidateArticleByActivityId(activityId: number, articleId: number): Observable<any> {
    const url = this.adminServiceUrl + 'Util/ValidateArticleByActivityId?activityId=' + activityId + '&&articleId=' + articleId
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  SaveArticle(articleObj: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/SaveArticle'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: articleObj
    })
  }

  getMembers(branchID: number, searchText: string, statuse: number, searchType: any, memberRole: string, hit: number, isHeaderClick: boolean,
    isAscending: boolean, sortName: string): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetMembers?branchID=' + branchID + '&searchText=' + searchText + '&activeState=' + statuse
      + '&searchType=' + searchType + '&memberRole=' + memberRole
      + '&hit=' + hit + '&sortName=' + sortName + '&isAscending=' + isAscending;

    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  deleteArticle(delObj: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/DeleteArticle'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: delObj
    });
  }

  GetInventory(): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetInventory?branchId=' + this.branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  SaveInventory(inventory: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/SaveInventory'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: inventory
    });
  }

  GetInventoryDetail(inventoryId: number): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetInventoryDetail?branchId=' + this.branchId + '&inventoryId=' + inventoryId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetEmployeeBookings(employeeId: number): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetEmployeeBookings?employeeId=' + employeeId
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetGymEmployeeWorkPlan(employeeId: number): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetGymEmployeeWorkPlan?employeeId=' + employeeId
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetGymEmployeeApprovals(employeeId: number, approvalType: string): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetGymEmployeeApprovals?employeeId=' + employeeId + '&&approvalType=' + approvalType
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetNetValueForArticle(inventoryId: number, articleId: number, counted: number, branchId: number): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetNetValueForArticle?inventoryId=' + inventoryId + '&articleId=' + articleId + '&counted=' + counted + '&branchId=' + branchId
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  SaveInventoryDetail(inventoryDetails: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/SaveInventoryDetail'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: inventoryDetails
    });
  }

  PrintInventoryList(branchId: number, inventoryId: number): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/PrintInventoryList?branchId=' + this.branchId + '&inventoryId=' + inventoryId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }
  GetResourceScheduleItems(resId: number, roleTpye: string): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetResourceScheduleItems?resId=' + resId + '&roleTpye=' + roleTpye
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  SaveWorkItem(workItem: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/SaveWorkItem'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: workItem
    })
  }

  AdminApproveEmployeeWork(work: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/AdminApproveEmployeeWork'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: work
    })
  }

  ApproveEmployeeTimes(time: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/ApproveEmployeeTimes'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: time
    })
  }

  DeleteInventory(inventoryId: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/DeleteInventory?inventoryId=' + inventoryId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  GetResourceBookingViewMode(): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetResourceBookingViewMode';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  DeleteResourcesScheduleItem(scheduleItemId: number): Observable<any> {
    const url = this.adminServiceUrl + 'Util/DeleteResourcesScheduleItem?scheduleItemId=' + scheduleItemId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  SaveScheduleItem(saveScheduleItem: any): Observable<any> {
    const url = this.adminServiceUrl + 'Util/SaveScheduleItem'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: saveScheduleItem
    });
  }

  GetResourceCalender(branchId: any, hit: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetResourceCalender?branchId=' + branchId + '&hit=' + hit;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetResourceCalenderScheduleItems(schedule: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetResourceCalenderScheduleItems';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: schedule
    })
  }

  GetEntityActiveTimes(entityActiveTime: any): Observable<any> {
    entityActiveTime.Branchid = this.branchId;
    const url = this.adminServiceUrl + 'Settings/GetEntityActiveTimes';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: entityActiveTime
    })
  }

  GetFollowUpByEmployeeId(params: any): Observable<any> {
    const url = this.adminServiceUrl + 'GMSJobs/GetFollowUpByEmployeeId?branchId='
      + this.branchId + '&employeeId=' + params.employeeId + '&hit=' + params.hit;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetJobsByEmployeeId(params: any): Observable<any> {
    const url = this.adminServiceUrl + 'GMSJobs/GetJobsByEmployeeId?branchId='
      + this.branchId + '&employeeId=' + params.employeeId + '&hit=' + params.hit;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  UpdateContractVisits(selectedContract: any, punchedContract: any, bookingId: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/UpdateContractVisits?selectedContractId=' + selectedContract + '&punchedContractId=' + punchedContract + '&bookingId=' + bookingId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true
    })
  }

  GetPunchcardContractsOnMember(memberId: any, activity: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetPunchcardContractsOnMember?memberId=' + memberId + '&activity=' + activity;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  IsArticlePunchcard(articleId: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/IsArticlePunchcard?articleId=' + articleId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true,
    })
  }

  SaveResourceActiveTimeRepeatable(activeTimesList): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/SaveResourceActiveTimeRepeatable';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: activeTimesList
    });
  }

  SaveResourceActiveTimeRepeatablePunchcard(contractId: any, activeTimesList: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/SaveResourceActiveTimeRepeatablePunchcard?contractId=' + contractId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: activeTimesList
    });
  }

  SaveResourceActiveTimePunchcard(entityActiveTime: any, contractID: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/SaveResourceActiveTimePunchcard?contractId=' + contractID;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: entityActiveTime
    });
  }

  SaveResourceActiveTime(entityActiveTime: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/SaveResourceActiveTime'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: entityActiveTime
    });
  }

  GetArticleForResouceBooking(articleSearch: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetArticleForResouceBooking'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: articleSearch
    });
  }

  ValidatePunchcardContractMember(bookingValidate: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/ValidatePunchcardContractMember'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: bookingValidate
    });
  }

  AssignTaskToEmployee(body: any): Observable<any> {
    body.BranchId = this.branchId;
    const url = this.adminServiceUrl + 'GMSJobs/AssignTaskToEmployee';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }

  GetDiscountList(discountType: any): Observable<any> {
    const url = this.adminServiceUrl + 'Util/GetDiscountList?branchId=' + this.branchId + '&discountType=' + discountType;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  GetResourceBooking(scheduleId: any, startDateTime: any, endDateTime: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/GetResourceBooking?scheduleId=' + scheduleId + '&startDateTime=' + startDateTime + '&endDateTime=' + endDateTime;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  GetVenderList(searchText?: string): Observable<any> {
    const url = this.adminServiceUrl + 'Util/GetVenderList?branchId=' + this.branchId + '&searchText=' + searchText;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  DeleteShopSponsorDiscount(deleteShopSponsorDiscount: any): Observable<any> {
    const url = this.adminServiceUrl + 'Util/DeleteShopSponsorDiscount'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: deleteShopSponsorDiscount
    });
  }

  CheckUnusedCancelBooking(bookingValidate: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/CheckUnusedCancelBooking'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: bookingValidate
    });
  }

  DeleteResourceActiveTime(entityActiveTime: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/DeleteResourceActiveTime'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: entityActiveTime
    });
  }

  SendSmsForBooking(smsObj: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/SendSmsForBooking'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: smsObj
    });
  }

  AddFreeDefineNotification(smsObj: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/AddFreeDefineNotification'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: smsObj
    });
  }

  getSponsors(branchId: any, startDueDate: any, endDueDate: any): Observable<any> {
    const url = this.memberServiceUrl + '/Contract/GetSponsors?branchId=' + branchId + '&startDueDate=' + startDueDate + '&endDueDate=' + endDueDate;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  SaveDiscount(saveDiscount: any): Observable<any> {
    const url = this.adminServiceUrl + 'Util/SaveDiscount'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: saveDiscount
    });
  }

  DeleteJobCategory(jobCategoryId: number): Observable<any> {
    const url = this.adminServiceUrl + '/GMSJobs/DeleteJobCategory?jobCategoryId=' + jobCategoryId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  SaveJobCategory(jobCategoryObj: any): Observable<any> {
    const url = this.adminServiceUrl + '/GMSJobs/SaveJobCategory'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: jobCategoryObj
    });
  }

  GetJobScheduleItems(branchId: number): Observable<any> {
    const url = this.adminServiceUrl + '/GMSJobs/GetJobScheduleItems?branchId=' + branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  DeleteJobScheduleItem(list: any): Observable<any> {
    const url = this.adminServiceUrl + '/GMSJobs/DeleteJobScheduleItem';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: list
    });
  }

  SaveExcelineJob(jobScheduleItemObj: any): Observable<any> {
    const url = this.adminServiceUrl + '/GMSJobs/SaveExcelineJob';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: jobScheduleItemObj
    });
  }

  UnavailableResourceBooking(unavailable: any): Observable<any> {
    const url = this.adminServiceUrl + 'Util/UnavailableResourceBooking'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: unavailable
    });
  }

  addRevenueAccount(account: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/AddUpdateRevenueAccounts';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: account
    });
  }

  deleteRevenueAccount(account: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/DeleteRevenueAccounts';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: account
    });
  }

  SavePaymentBookingMember(bookingPayment: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/SavePaymentBookingMember';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: bookingPayment
    });
  }

  ExcelineManualSponsoringGeneration( SponsorData: any): Observable<any> {
    const url = this.adminServiceUrl + 'Settings/ExcelineManualSponsoringGeneration';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: SponsorData
    });
  }

}
