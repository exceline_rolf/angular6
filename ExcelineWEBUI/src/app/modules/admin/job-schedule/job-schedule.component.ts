import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { UsbModal } from 'app/shared/components/us-modal/us-modal';
import { ExceBreadcrumbService } from '../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { ExceLoginService } from '../../login/exce-login/exce-login.service';
import { AdminService } from '../services/admin.service';
import { TranslateService } from '@ngx-translate/core';
import { ExceMessageService } from '../../../shared/components/exce-message/exce-message.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EntitySelectionType, ColumnDataType } from '../../../shared/enums/us-enum';
import { UsErrorService } from '../../../shared/directives/us-error/us-error.service';
import value from '*.json';
import { USSAdminService } from '../../uss-admin/services/uss-admin-service';
import * as moment from 'moment';
import { IMyDpOptions } from '../../../shared/components/us-date-picker/interfaces';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

const now = new Date();

@Component({
  selector: 'app-job-schedule',
  templateUrl: './job-schedule.component.html',
  styleUrls: ['./job-schedule.component.scss']
})
export class JobScheduleComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  selectedJobCategoryId: number;
  weekType: number;
  selectedRoleId: number;
  selectedEmployeeId: number;
  uspRoles?: any[] = [];
  roleSelectionView: any;
  empBtn: boolean;
  roleBtn: boolean;
  isRoleVisible: boolean;
  selectedJobCategory: any;
  jobCatTitle: any;
  jobCategories?: any[] = [];
  employeesList?: any[] = [];
  defaultRole = 'ALL'
  items = [];
  itemCount = 0;
  employeeSelectionView: any;
  private entitySelectionViewTitle: string;
  entitySelectionType: string;
  entitySearchType: string;
  columns: any;
  isDbPagination = true;
  IsEmp: any;
  JobScheduleForm: FormGroup
  editJobScheduleView: any;
  delMsgBoxModal: any;
  deletedItemId: any;
  deletedItem: any;
  jobScheduleItems?: any[] = [];
  newJobScheduleView: any;
  private _branchId: number;
  myFormSubmited = false;
  private _user: any;

  auotocompleteItemsForEmpSelection = [{ id: 'NAME', name: 'Name :', isNumber: false }, { id: 'ID', name: 'Id :', isNumber: true }];
  auotocompleteItems = [{ id: 'Name', name: 'Name :', isNumber: false }];
  config: any = { 'placeholder': '', 'sourceField': ['searchText'] };
  @ViewChild('autobox') public autocomplete;

  formErrors = {
    'EmpName': '',
    'Day': '',
  };
  validationMessages = {
    'EmpName': {
      'required': 'COMMON.MsgPleaseAssignEmployee'
    },
    'Day': {
      'required': 'ADMIN.MsgPleaseSelectDay'
    }
  };

  constructor(
    private loginService: ExceLoginService,
    private ussAdminService: USSAdminService,
    private fb: FormBuilder,
    private exceMessageService: ExceMessageService,
    private translateService: TranslateService,
    private adminService: AdminService,
    private exceLoginService: ExceLoginService,
    private modalService: UsbModal,
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ADMIN.WorkingC',
      url: '/adminstrator/job-schedule'
    });

    this._branchId = this.exceLoginService.SelectedBranch.BranchId;
    this._user = this.loginService.CurrentUser.username;

    this.exceMessageService.yesCalledHandle.pipe(
      takeUntil(this.destroy$)
      ).subscribe((value) => {
      this.yesHandler();
    });

    this.JobScheduleForm = fb.group({
      'Id': [null],
      'JobCategoryName': [null], // Text
      'EmpName': [null, [Validators.required]],
      'RoleName': [null],
      'Occurrence': ['2'],
      'Day': [null, [Validators.required]],
      'StartDate': [null],
      'EndDate': [null],
      'StartTime': [null],
      'EndTime': [null],
      'JobCategory': [null],
      'ScheduleId': [null]
    });
  }

  ngOnInit() {
    this.JobScheduleForm.statusChanges.pipe(
      takeUntil(this.destroy$)
      ).subscribe(_ => {
      UsErrorService.onValueChanged(this.JobScheduleForm, this.formErrors, this.validationMessages)
    });
    this.getJobScheduleItems();
    this.getExtFieldsByTaskCategory();
  }

  getJobScheduleItems() {
    this.adminService.GetJobScheduleItems(this._branchId).pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
      if (result) {
        this.jobScheduleItems = result.Data;
      }
    });
  }

  DeleteScheduleItem(item) {
    this.deletedItem = item;
    this.deletedItemId = item.Id;
    if (item) {
      let messageTitle = '';
      let messageBody = '';
      (this.translateService.get('Confirm').pipe(
        takeUntil(this.destroy$)
        ).subscribe(tranlstedValue => messageTitle = tranlstedValue));
      (this.translateService.get('ADMIN.ConfirmMessage').pipe(
        takeUntil(this.destroy$)
        ).subscribe(tranlstedValue => messageBody = tranlstedValue));
      this.delMsgBoxModal = this.exceMessageService.openMessageBox('CONFIRM',
        {
          messageTitle: messageTitle,
          messageBody: messageBody
        });
    }
  }

  // delete yes call handle
  yesHandler() {
    const index: number = this.jobScheduleItems.indexOf(this.deletedItem);
    this.adminService.DeleteJobScheduleItem({ ScheduleItemIdList: [this.deletedItemId] }).pipe(
      takeUntil(this.destroy$)
      ).subscribe(data => {
      if (index !== -1) {
        this.jobScheduleItems.splice(index, 1);
      }
    }, null, null);
  }

  // ---------------------------------------------------------------------------------------------------------

  openNewJobScheduleView(content: any) {
    this.selectedJobCategory = this.jobCategories[0];
    this.jobCatTitle = this.jobCategories[0].Name;
    this.isRoleVisible = (this.jobCategories[0].IsRole == true) ? true : false;
    this.IsEmp = 'emp';
    this.empBtn = true;
    const today = new Date();
    const sDate = {
      date: {
        year: today.getFullYear(),
        month: today.getMonth() + 1,
        day: today.getDate()
      }
    }
    const eDate = {
      date: {
        year: today.getFullYear(),
        month: today.getMonth() + 2,
        day: today.getDate()
      }
    }
    this.JobScheduleForm.patchValue({
      StartDate: sDate,
      EndDate: eDate,
      StartTime: '8:30',
      EndTime: '10:30'
    });
    this.newJobScheduleView = this.modalService.open(content);
  }

  occurenceChange(val) {
    this.weekType = val;
  }

  dayChange(value) {
  }

  getExtFieldsByTaskCategory() {
    this.adminService.GetJobCategories(this._branchId).pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
      if (result) {
        this.jobCategories = result.Data;
      }
    }, null, null);
  }

  getJobCatChange(item) {
    if (item) {
      this.jobCatTitle = item.Name;
      this.selectedJobCategory = item;
      this.selectedJobCategoryId = item.Id;
      if (this.selectedJobCategory.IsRole == true) {
        this.isRoleVisible = true;
      } else {
        this.isRoleVisible = false;
      }
    }
  }

  getRadioBtnChange(event) {
    if (event.target.value == 'emp') {
      this.empBtn = true;
      this.roleBtn = false;
    } else if (event.target.value == 'role') {
      this.roleBtn = true;
      this.empBtn = false;
    }
  }

  getUSPRoles(content: any) {
    this.ussAdminService.GetUSPRoles('', true).pipe(
      takeUntil(this.destroy$)
      ).subscribe(roles => {
      this.uspRoles = roles.Data;
    }, null, null);
    this.roleSelectionView = this.modalService.open(content, { width: '500' });
  }

  getSelectedRole(event) {
    this.selectedRoleId = event.row.item.Id;
    this.JobScheduleForm.controls['RoleName'].setValue(event.target.textContent);
    this.roleSelectionView.close();
  }

  // ----------------------------------------------------------------------------------------------------------

  // Employee Selection
  employeeSelectionModal(content: any) {
    this.translateService.get('MEMBERSHIP.SelectEmployee').pipe(
      takeUntil(this.destroy$)
      ).subscribe(tranlstedValue => this.entitySelectionViewTitle = tranlstedValue);
    this.isDbPagination = true;
    this.entitySelectionType = EntitySelectionType[EntitySelectionType.EMP];
    this.columns = [
      { property: 'Name', header: 'Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
      { property: 'CustId', header: 'Number', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT }
    ];
    this.getGymEmployees('');
    this.employeeSelectionView = this.modalService.open(content, { width: '990' });
  }

  getGymEmployees(searchText) {
    this.adminService.GetGymEmployees(this._branchId, searchText, true, -1).pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      result => {
        if (result) {
          this.items = result.Data;
          this.items.forEach(date => {
            date.CreatedDate = date.CreatedDate === '0001-01-01T00:00:00' ? ' ' : date.CreatedDate;
          });
          this.itemCount = result.Data.length;
        }
      }, null, null);
  }

  // Add selected employee to the text field
  selectedEmployee(item: any) {
    this.selectedEmployeeId = item.Id;
    if (item) {
      this.JobScheduleForm.controls['EmpName'].setValue(item.Name);
    }
    this.closeEmployeeSelectionView();
  }

  // Search an employee from employee selection
  searchEmployee(val: any): void {
    this.getGymEmployees(val.searchVal);
  }

  closeEmployeeSelectionView() {
    this.employeeSelectionView.close();
  }

  // Save job schedule
  saveJobSchedule(value) {
    value.JobCategoryName = value.JobCategoryName.trim();
    if (value.EmpName == null) {
      this.JobScheduleForm.controls['EmpName'].setErrors({ 'required': true });
    } else {
      this.JobScheduleForm.controls['EmpName'].setErrors(null);
    }
    if (value.Day == null) {
      this.JobScheduleForm.controls['Day'].setErrors({ 'required': true });
    } else {
      this.JobScheduleForm.controls['Day'].setErrors(null);
    }
    this.myFormSubmited = true;
    const stDate = this.getDateConverter(value.StartDate.date);
    const enDate = this.getDateConverter(value.EndDate.date);
    if ((moment(stDate).isAfter(enDate, 'day')) == true) {
      this.exceMessageService.openMessageBox('WARNING', { messageTitle: 'Exceline', messageBody: 'ADMIN.AddEmStartDateInvalid' });
    } else if (value.StartTime >= value.EndTime) {
      this.exceMessageService.openMessageBox('WARNING', { messageTitle: 'Exceline', messageBody: 'ADMIN.EndTimeShouldBeGreaterThenStartTime' });
    } else {
      this.selectedRoleId = (this.selectedRoleId == null) ? -1 : this.selectedRoleId;

      if (this.JobScheduleForm.valid) {
        const excelineJob = {
          Title: value.JobCategoryName,
          JobCategoryId: this.selectedJobCategoryId,
          JobCategoryName: value.JobCategory
          // ExtendedFieldsList: []
        }

        value.Id = (value.Id == undefined) ? -1 : value.Id

        const scheduleItemObj = {
          Id: (value.Id == null) ? -1 : value.Id,
          ScheduleId: (value.ScheduleId == null) ? -1 : value.ScheduleId,
          Occurrence: 'WEEKLY',
          Day: value.Day,
          StartTime: '2018-05-14' + ' ' + value.StartTime + ':00',
          EndTime: '2018-05-14' + ' ' + value.EndTime + ':00',
          CreatedUser: this._user,
          LastModifiedUser: this._user,
          WeekType: value.Occurrence,
          EmpId: this.selectedEmployeeId,
          RoleId: (this.selectedRoleId == undefined) ? -1 : this.selectedRoleId,
          StartDate: this.getDateConverter(value.StartDate.date),
          EndDate: this.getDateConverter(value.EndDate.date),
          EntityRoleType: 'EMP',
          ActiveStatus: true,
          ExcelineJob: excelineJob
        }

        const jobScheduleItemObj = {
          scheduleItem: scheduleItemObj,
          branchId: this._branchId
        }

        // this.adminService.SaveExcelineJob(jobScheduleItemObj).pipe(takeUntil(this.destroy$)).subscribe(obj => {
        //   if (obj) {
        //     this.closeJobScheduleModal();
        //     this.getExtFieldsByTaskCategory();
        //     this.getJobScheduleItems();
        //     this.myFormSubmited = false;
        //   }
        // });
      } else {
        UsErrorService.validateAllFormFields(this.JobScheduleForm, this.formErrors, this.validationMessages);
      }
    }
  }

  // convert date js object in to string
  getDateConverter(datee: any) {
    const dd = (datee.day < 10 ? '0' : '') + datee.day;
    const MM = ((datee.month + 1) < 10 ? '0' : '') + datee.month;
    const yyyy = datee.year;
    return (yyyy + '-' + MM + '-' + dd);
  }

  // ----------------------------------------------------------------------------------------------------------

  // Edit selected Job Scedule Item
  editJobScheduleItem(item, content) {
    this.jobCatTitle = item.ExcelineJob.JobCategoryName
    this.IsEmp = 'emp';
    this.empBtn = true;
    const startDate = new Date(item.StartDate);
    const sDate = {
      date: {
        year: startDate.getFullYear(),
        month: startDate.getMonth() + 1,
        day: startDate.getDate()
      }
    }
    const endDate = new Date(item.EndDate);
    const eDate = {
      date: {
        year: endDate.getFullYear(),
        month: endDate.getMonth() + 1,
        day: endDate.getDate()
      }
    }
    const sTime = moment(item.StartTime).format('HH:mm');
    const eTime = moment(item.EndTime).format('HH:mm');
    this.selectedEmployeeId = item.EmpId;
    this.selectedJobCategoryId = item.ExcelineJob.JobCategoryId;

    this.JobScheduleForm.patchValue({
      Id: item.Id,
      JobCategoryName: item.ExcelineJob.Title,
      EmpName: item.EmpName,
      RoleName: item.RoleName,
      Occurrence: item.WeekType,
      Day: item.Day,
      StartDate: sDate,
      EndDate: eDate,
      StartTime: sTime,
      EndTime: eTime,
      EmpId: this.selectedEmployeeId,
      JobCategory: item.ExcelineJob.JobCategoryName,
      ScheduleId: item.ScheduleId
    });
    this.editJobScheduleView = this.modalService.open(content);
  }

  // -----------------------------------------------------------------------------------------------------------

  // to close the job schedule modal (for delete btn and cancel btn)
  closeJobScheduleModal() {
    if (this.editJobScheduleView) {
      this.editJobScheduleView.close();
      this.JobScheduleForm.reset();
    }
    if (this.newJobScheduleView) {
      this.newJobScheduleView.close();
      this.JobScheduleForm.reset();
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();

    if (this.newJobScheduleView) {
      this.newJobScheduleView.close();
    }
    if (this.roleSelectionView) {
      this.roleSelectionView.close();
    }
    if (this.employeeSelectionView) {
      this.employeeSelectionView.close();
    }
    if (this.editJobScheduleView) {
      this.editJobScheduleView.close();
    }
  }

}
