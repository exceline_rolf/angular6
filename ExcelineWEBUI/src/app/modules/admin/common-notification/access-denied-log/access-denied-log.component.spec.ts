import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessDeniedLogComponent } from './access-denied-log.component';

describe('AccessDeniedLogComponent', () => {
  let component: AccessDeniedLogComponent;
  let fixture: ComponentFixture<AccessDeniedLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccessDeniedLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccessDeniedLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
