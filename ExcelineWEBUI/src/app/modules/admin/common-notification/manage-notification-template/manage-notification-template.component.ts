
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
import { NotificationService } from '../../services/notification.service'
import { ExceLoginService } from '../../../../modules/login/exce-login/exce-login.service';
import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { ExceBreadcrumbService } from '../../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
const now = new Date();


@Component({
  selector: 'manage-notification-template',
  templateUrl: './manage-notification-template.component.html',
  styleUrls: ['./manage-notification-template.component.scss']
})
export class ManageNotificationTemplateComponent implements OnInit, OnDestroy {
  private branchId: number;
  private modalReference: any;
  private username: string;
  entityList: any
  catTypes: any;
  catMethods: any;
  entityTypes: any
  textAreaText: String = '';
  isEmailButtondisabled = true;
  private editorValue: any;
  private model: any;
  private selectedEntityId: number;
  selectedMethod: any;
  selectedType: any;
  selectedEntity: any;
  private templateTextObj: any;
  private addCatModel: any;
  saveButtonIsDisabled = true
  private destroy$ = new Subject<void>();

  constructor(
    private modalService: UsbModal,
    private notificationService: NotificationService,
    private exceLoginService: ExceLoginService,
    private exceMessageService: ExceMessageService,
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ADMIN.PrinterC',
      url: '/adminstrator/manage-notification-template'
    });

    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.username = this.exceLoginService.CurrentUser.username
    this.exceLoginService.branchSelected.pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      (branch) => {
        this.branchId = branch.BranchId;
      }, error => {},
      () => {

      }
    );

    this.exceMessageService.yesCalledHandle.pipe(
      takeUntil(this.destroy$)
    ).subscribe((value) => {
      if (value.id === 'EMAIL_CONFIRM') {
        this.yesEmailHandler();
      }
    }, null, null);
  }

  ngOnInit() {
    this.getEntityList();
    this.getCategoryTypesType();
    this.getCategoryTypesMethod();
  }

  ngOnDestroy() {
    if (this.modalReference) {
      this.modalReference.close();
    }
    if (this.addCatModel) {
      this.addCatModel.close();
    }
    this.destroy$.next();
    this.destroy$.complete();
  }


  getEntityList() {
    this.notificationService.GetEntityList().pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result.Data) {
        this.entityList = result.Data;
      }
    }, null, null)
  }

  getCategoryTypesMethod() {

    this.notificationService.GetCategoriesByType(this.branchId, 'NOTIFYMETHOD', '').pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result.Data) {
        this.catMethods = result.Data;
      }
    }, null, null)
  }

  getCategoryTypesType() {

    this.notificationService.GetCategoriesByType(this.branchId, 'NOTIFICATIONTEMPLATE', '').pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result.Data) {
        this.catTypes = result.Data;
      }
    }, null, null)
  }


  getTypeByEntity(Id) {
    this.selectedEntityId = Id;
    this.notificationService.GetTemplateTextKeyByEntity(Id, 'NOTIFICATIONKEY').pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result.Data) {
        this.entityTypes = result.Data;
      }
    }, null, null)

  }

  entChange(Id) {
    this.getTypeByEntity(Id)
  }

  clickEntityType(value, code) {
    this.textAreaText = value;
    this.textAreaText = this.textAreaText + ' [[' + code + ']] ';
    if (this.selectedMethod && this.selectedType) {
      this.saveButtonIsDisabled = false;
    }
  }

  getMethodCode(val) {
    for (let i = 0; i < this.catMethods.length; i++) {
      // tslint:disable-next-line:triple-equals comparing strings and ints
      if (this.catMethods[i].Id == val) {
        return this.catMethods[i].Code;
      }
    }
  }

  getTypeCode(val) {
    for (let i = 0; i < this.catTypes.length; i++) {
      // tslint:disable-next-line:triple-equals comparing strings and ints
      if (this.catTypes[i].Id == val) {
        return this.catTypes[i].Code;
      }
    }
  }

  typeChange(methodVal: any, typeVal: any) {

    if (this.selectedMethod && this.selectedType) {
      if (this.selectedMethod.Code === 'EMAIL') {
        this.isEmailButtondisabled = false;
      }
      this.notificationService.GetNotificationTemplateText(this.selectedMethod.Code, this.selectedType.Code).pipe(
        takeUntil(this.destroy$)
      ).subscribe(result => {
        if (result.Data) {
          this.templateTextObj = result.Data
          this.textAreaText = this.templateTextObj[0] ? this.templateTextObj[0].Text : '';
        }
      }, null, null)
    }
  }

  openEmail(content) {
    const duplicateText = <any>JSON.parse(JSON.stringify(this.textAreaText));
    this.editorValue = duplicateText;
    this.modalReference = this.modalService.open(content, { width: '600' });
  }

  emailTemplateConfirm() {
    this.modalReference.close();
    this.openEmailConfirmMessage('Confirm', 'Do you want to add/update this with report entity');
  }

  openAddCat(content) {
    this.addCatModel = this.modalService.open(content, { width: '600' });
  }

  openEmailConfirmMessage(title, body) {
    this.model = this.exceMessageService.openMessageBox('CONFIRM',
      {
        messageTitle: title,
        messageBody: body,
        msgBoxId: 'EMAIL_CONFIRM'
      });
  }

  yesEmailHandler() {
    const duplicateEditorVal = <any>JSON.parse(JSON.stringify(this.editorValue));
    this.textAreaText = duplicateEditorVal;
    this.SaveNotificationTextTemplate()
  }

  SaveNotificationTextTemplate() {
    const templateText = {
      'Id': this.templateTextObj[0] ? this.templateTextObj[0].Id : '',
      'Text': this.textAreaText,
      'TypeID': this.selectedType.Id,
      'MethodID': this.selectedMethod.Id
    }
    this.notificationService.SaveNotificationTextTemplate({
      'entityId': this.selectedEntityId,
      'templatetText': templateText,
      'branchId': this.branchId
    }).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result.Data) {
        if (!this.templateTextObj[0]) {
          this.templateTextObj.push({ 'Id': result.Data });
        }
        this.saveButtonIsDisabled = true;
      }
    }, null, null)
  }

  enableSaveButton() {
    if (this.selectedMethod && this.selectedType) {
      this.saveButtonIsDisabled = false;
    }
  }

}
