import { TestBed, inject } from '@angular/core/testing';

import { RcBasicInfoService } from './rc-basic-info.service';

describe('RcBasicInfoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RcBasicInfoService]
    });
  });

  it('should be created', inject([RcBasicInfoService], (service: RcBasicInfoService) => {
    expect(service).toBeTruthy();
  }));
});
