import { Component, OnInit, OnDestroy, EventEmitter, Output, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as moment from 'moment';
import { PreloaderService } from '../../../../../shared/services/preloader.service';
import { MemberSearchType, ColumnDataType, EntitySelectionType } from '../../../../../shared/enums/us-enum';
import { UsbModal } from '../../../../../shared/components/us-modal/us-modal';
import { AdminService } from '../../../services/admin.service';
import { ExceLoginService } from '../../../../login/exce-login/exce-login.service';
import { ExceMessageService } from '../../../../../shared/components/exce-message/exce-message.service';
import { TranslateService } from '@ngx-translate/core';
import { ShopHomeService } from '../../../../shop/shop-home/shop-home.service';
import { ExceShopService } from '../../../../shop/services/exce-shop.service';
import { ExceGymSetting } from '../../../../../shared/SystemObjects/Common/ExceGymSetting';
import { UsbModalRef } from '../../../../../shared/components/us-modal/modal-ref';
import { defaultIfEmpty, elementAt } from '../../../../../../../node_modules/rxjs/operators';
import { DataTableFilterPipe } from '../../../../../shared/components/us-data-table/tools/data-table-filter-pipe';
import { DataTableResource } from '../../../../../shared/components/us-data-table/tools/data-table-resource';
import { UsErrorService } from 'app/shared/directives/us-error/us-error.service';
import { resolve } from 'url';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'add-booking',
  templateUrl: './add-new-booking.component.html',
  styleUrls: ['./add-new-booking.component.scss']
})
export class AddNewBookingComponent implements OnInit, OnDestroy {
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  private articleItemSource: DataTableResource<any[]>;
  private shopLoginData?: any = {};
  private exceGymSetting: ExceGymSetting = new ExceGymSetting();
  private entitySelectionViewTitle: string;
  private destroy$ = new Subject<void>();

  @Output() saveBookingEvent = new EventEmitter();
  @Output() cancelBookingEvent = new EventEmitter();
  @Input() activeTimeDetail: any;
  @Input() activityUnAvelabelList: any;
  @Input() unAvelabelList: any;
  @Input() resScheduleItem: any;
  @Input() bookingList: any = [];
  @Input() isMemberSelected: boolean;
  @Input() bookingType: string;
  shopModalReference: UsbModalRef;
  public bookingForm: FormGroup;

  bookingMemberList: any = [];
  memberList: any = [];
  bookingResourceList: any = [];
  resList: any = []
  punchcardContract: any = [];
  preBookingContractList: any = [];
  resourceBooking: any = [];
  groupContractBookingMemberList: any = [];
  entitySelectionType: string;
  punchcardCustomerSelected: any;
  punchcardCustomerSelectedName: any;
  multiplePunchcardBooking = false;
  articleIsPunchcard = false;
  isBranchVisible = true;
  isStatusVisible = true;
  isRoleVisible = true;
  isAddBtnVisible = true;
  isBasicInforVisible = true;
  isDisabledRole = false;
  isDbPagination = false;
  clickable = true;
  defaultRole = 'ALL'
  multipleRowSelect = false;
  columns: any;
  items = [];
  itemCount: number;
  articleList = [];
  articleViewList = [];
  categoryList = []
  branchId: number;
  memberSelectView: any;
  addMemberView: any;
  chooseContractView: any;
  chooseContractContent: any;
  articleModel: any;
  categoryModel: any;
  auotocompleteItems = [{ id: 'NAME', name: 'NAME :', isNumber: false }, { id: 'ID', name: 'Idd :', isNumber: true }];
  yesHandlerSubscription: any;
  noHandlerSubscription: any;
  selectedMember: any;
  shopPaymentContent: any;
  shopPaymentView: any;
  payMember: any;
  punchCardResBooking: any;

  formErrors = {
    'BookingCategoryName': '',
  };

  validationMessages = {
    'BookingCategoryName': {
      'required': 'ADMIN.BookingCategoryReq',
    }
  }

  endDateSelect: any;
  dayOptions: any;
  monthOptions: any;
  yearOptions: any;
  recurrenceOption: any;
  bookingScheduleItem = [];
  isRepeatable = false;
  frequency = 'd';
  repeatableForm: FormGroup;
  weekDays = [['Sunday', false], ['Monday', false], ['Tuesday', false], ['Wednesday', false], ['Thursday', false], ['Friday', false], ['Saturday', false]];


  private selectedGymsettings = [
    'EcoNegativeOnAccount',
    'EcoSMSInvoiceShop',
    'EcoIsPrintInvoiceInShop',
    'EcoIsPrintReceiptInShop',
    'EcoIsShopOnNextOrder',
    'EcoIsOnAccount',
    'EcoIsDefaultCustomerAvailable',
    'EcoIsPayButtonsAvailable',
    'EcoIsPettyCashSameAsUserAllowed',
    'OtherIsShopAvailable',
    'OtherLoginShop',
    'HwIsShopLoginWithCard',
    'ReqMemberListRequired',
    'OtherIsLogoutFromShopAfterSale',
    'EcoIsBankTerminalIntegrated',
    'EcoIsDailySettlementForAll',
    'OtherIsValidateShopGymId'
  ];
  tempDate: Date;
  punchcardContracts: any;
  punchcardContractId: any;
  activeItemsList: any[];
  categorySelectedVal = false;
  logbranchAccountNo: any;

  isBookingformSubmitted: boolean = false;

  constructor(private fb: FormBuilder, private modalService: UsbModal, private adminService: AdminService, private loginservice: ExceLoginService,
    private exceMessageService: ExceMessageService, private translateService: TranslateService, private shopHomeService: ShopHomeService,
    private shopService: ExceShopService) { }

  ngOnInit() {

    this.branchId = this.loginservice.SelectedBranch.BranchId;
    this.adminService.getBranches(this.branchId).pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      result => {
        this.logbranchAccountNo = result.Data[0].BankAccountNo
      }
    );
    this.bookingForm = this.fb.group({
      Id: [this.activeTimeDetail.Id],
      IsTask: [this.activeTimeDetail.IsTask],
      EntityId: [this.activeTimeDetail.EntityId],
      ActivityName: [this.activeTimeDetail.ActivityName],
      ActivityId: [this.activeTimeDetail.ActivityId],
      ArticleName: [this.activeTimeDetail.ArticleName],
      ArticleId: [this.activeTimeDetail.ArticleId],
      IsContractBooking: [this.activeTimeDetail.IsContractBooking],
      IsPunchCard: [this.activeTimeDetail.IsPunchCard],
      ArticleNoOfMinutes: [this.activeTimeDetail.ArticleNoOfMinutes],
      CategoryId: [this.activeTimeDetail.CategoryId],
      CategoryName: [this.activeTimeDetail.CategoryName],
      BookingCategoryName: [this.activeTimeDetail.BookingCategoryName, [Validators.required]],
      BookingCategoryId: [this.activeTimeDetail.BookingCategoryId],
      BookingCategoryColor: [this.activeTimeDetail.BookingCategoryColor],
      Comment: [this.activeTimeDetail.Comment],
      StartTime: [moment(this.activeTimeDetail.StartDateTime).format('HH:mm')],
      // EndTime: [moment(this.activeTimeDetail.EndDateTime).format('HH:mm')],
      /* If new booking, set endTime to starttime + 1 hour. Else (isEdit), get endTime */
      EndTime: [this.bookingType === 'NewBooking' ? moment(this.activeTimeDetail.StartDateTime).add(1, 'hours').format('HH:mm') : moment(this.activeTimeDetail.EndDateTime).format('HH:mm')],
      StartDate: [this.activeTimeDetail.StartDateTime],
      EndDate: [this.activeTimeDetail.EndDateTime],
      IsSmsReminder: [this.activeTimeDetail.IsSmsReminder],
      SheduleItemId: [this.activeTimeDetail.SheduleItemId]
    });
    this.repeatableForm = this.fb.group(
      {
        'frequency': [this.frequency],
        // day/week/month/year
        'recurEvery': [null],
        // 0(false) for recurEvery, 1(true) for on weekdays --> number instead of boolean do to modifiability
        'daysFrequencyOptions': [null],
        'monthFrequencyOptions': [null],
        'weeksdays': [[['Sunday', false], ['Monday', false], ['Tuesday', false], ['Wednesday', false], ['Thursday', false], ['Friday', false], ['Saturday', false]]],
        'dayOfMonth': [null],
        // 1 == first, 2 == second ...
        'ordinalFreq': [null],
        // name of day
        'daySelected': [null],
        // value represent month number
        'monthSelected': [null],
        'recurrenceOption': [null],
        'occurences': [null],
        'endDate': [null],
        'yearFrequencyOptions': [null]
      }
    );
    if (this.activeTimeDetail.BookingMemberList) {
      this.memberList = this.activeTimeDetail.BookingMemberList;
      this.bookingMemberList = Object.assign([], this.activeTimeDetail.BookingMemberList);
    }
    if (this.activeTimeDetail.BookingResourceList) {
      this.resList = this.activeTimeDetail.BookingResourceList;
      this.bookingResourceList = Object.assign([], this.activeTimeDetail.BookingResourceList);
    }
    this.exceMessageService.yesCalledHandle.pipe(
      takeUntil(this.destroy$)
      ).subscribe((value) => {
      if (value.id === 'DLETE_PUNCHCARD_BOOKING') {
        this.punchCardContractBookingCancelYesHandler(value.optionalData);
      } else if (value.id === 'DLETE_PAID_BOOKING') {
        this.paidBookingCancelYesHandler(value.optionalData);
      } else if (value.id === 'BOOKING_OVERLAP') {
        this.bookingOverlapYes(value.optionalData);
      }
    });

    this.exceMessageService.noCalledHandle.pipe(
      takeUntil(this.destroy$)
      ).subscribe((value) => {
      if (value.id === 'DLETE_PUNCHCARD_BOOKING') {
        this.punchCardContractBookingCancelNoHandler(value.optionalData);
      } else if (value.id === 'DLETE_PAID_BOOKING') {
        this.paidBookingCancelNoHandler(value.optionalData);
      }
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    if (this.shopModalReference) {
      this.shopModalReference.close();
    }
    if (this.shopPaymentView) {
      this.shopPaymentView.close();
    }
    if (this.memberSelectView) {
      this.memberSelectView.close();
    }
    if (this.chooseContractView) {
      this.chooseContractView.close();
    }
    if (this.categoryModel) {
      this.categoryModel.close();
    }
    if (this.articleModel) {
      this.articleModel.close();
    }
  }

  getScheduleItems(resBooking) {
    this.bookingScheduleItem.push(resBooking.SheduleItemId)
  }

  giveMeABooking(date, rb, value) {
    return {
      StartDate: moment(moment(date).format('YYYY-MM-DD') + ' ' + moment(rb.StartDate).format('HH:mm:ss')).format('YYYY-MM-DD HH:mm:ss'),
      EndDate: moment(moment(date).format('YYYY-MM-DD') + ' ' + moment(rb.EndDate).format('HH:mm:ss')).format('YYYY-MM-DD HH:mm:ss'),
      StartDateTime: moment(moment(date).format('YYYY-MM-DD') + ' ' + value.StartTime).format('YYYY-MM-DD HH:mm:ss'),
      EndDateTime: moment(moment(date).format('YYYY-MM-DD') + ' ' +  value.EndTime).format('YYYY-MM-DD HH:mm:ss'),
      ActivityId: rb.ActivityId,
      ActivityName: rb.ActivityName,
      ArticleId: rb.ArticleId,
      ArticleName: rb.ArticleName,
      ArticleNoOfMinutes: rb.ArticleNoOfMinutes,
      BookingCategoryColor: rb.BookingCategoryColor,
      BookingCategoryId: rb.BookingCategoryId,
      BookingCategoryName: rb.BookingCategoryName,
      BookingMemberList: rb.BookingMemberList,
      BookingResourceList: rb.BookingResourceList,
      BranchId: rb.BranchId,
      CategoryId: rb.CategoryId,
      CategoryName: rb.CategoryName,
      Comment: rb.Comment,
      EndTime: rb.EndTime,
      EntityId: rb.EntityId,
      Id: rb.Id,
      IsContractBooking: rb.IsContractBooking,
      IsPunchCard: rb.IsPunchCard,
      IsSmsReminder: rb.IsSmsReminder,
      IsTask: rb.IsTask,
      RoleType: rb.RoleType,
      SheduleItemId: rb.SheduleItemId,
      StartTime: rb.StartTime
    }
  }

  repeatableSave(resBooking: any, value: any, content) {
    const isPunch = resBooking.IsPunchCard
    resBooking.SheduleItemId = null;
    const bookingList = [];
    const dates = this.getDatesAndTimeOnRepeatable(this.repeatableForm, resBooking)
    dates.forEach(date => {
      const booking = this.giveMeABooking(date, resBooking, value)
      bookingList.push(booking)
    });

    this.activeItemsList = bookingList;
    if (isPunch) {
      // if booking is group different functionality than single
      if (this.memberList.length > 1) {
        this.exceMessageService.openMessageBox('ERROR', { messageTitle: 'ADMIN.BookingError', messageBody: 'ADMIN.ToManyMembersPunch' });
      } else {
        const member = this.memberList[0];
        this.punchcardCustomerSelected = this.memberList[0]
        this.punchcardCustomerSelectedName = member.Name.toString();
        this.adminService.GetPunchcardContractsOnMember(member.Id, this.activeTimeDetail.ActivityName).pipe(
          takeUntil(this.destroy$)
          ).subscribe(
          res => {
            this.punchcardContracts = res.Data;
            const bookingsLength = bookingList.length
            const punchcards = res.Data;
            const activePunches = punchcards.filter(contract => Number(contract.AvailableVisits) > bookingsLength)
            this.punchcardContract = activePunches;
            if (activePunches.length > 1) {
              this.chooseContractModal(content);
            } else if (activePunches.length === 1) {
              this.chooseContractModal(content);
              // this.saveBookingEvent.emit();
            } else {
              // this.saveBookingEvent.emit();
              this.exceMessageService.openMessageBox('ERROR', { messageTitle: 'ADMIN.BookingError', messageBody: 'ADMIN.PunchCardLimitError' });
            }
          }
        );
      }
    } else {
      this.adminService.SaveResourceActiveTimeRepeatable(bookingList).pipe(
        takeUntil(this.destroy$)
        ).subscribe(
        result => {
          if (result) {
            this.saveBookingEvent.emit()
          }
        }
      );
    }
  }

  saveBooking(value, content) {
    const resBooking = value;
    const sDateTime = moment(moment(this.activeTimeDetail.StartDateTime).format('YYYY-MM-DD') + ' ' + value.StartTime).format('YYYY-MM-DD HH:mm:ss');
    const eDateTime = moment(moment(this.activeTimeDetail.EndDateTime).format('YYYY-MM-DD') + ' ' + value.EndTime).format('YYYY-MM-DD HH:mm:ss');
    if (moment(sDateTime) >= moment(eDateTime) || moment(sDateTime) >= moment(this.resScheduleItem.EndDateTime)
      || moment(eDateTime) <= moment(this.resScheduleItem.StartDateTime)) {
      this.exceMessageService.openMessageBox('ERROR', { messageTitle: 'Exceline', messageBody: 'ADMIN.TimePeriodNotValid' });
      return;
    }

    const filterActivityUnAvelableItem = this.activityUnAvelabelList.find
      (x =>
        moment(sDateTime).isBetween(x.StartDateTime, x.EndDateTime)
        || moment(eDateTime).isBetween(x.StartDateTime, x.EndDateTime)
        || moment(x.StartDateTime).isBetween(sDateTime, eDateTime));

    const filterUnAvelableItem = this.unAvelabelList.find
      (x => moment(sDateTime).isBetween(x.StartDateTime, x.EndDateTime)
        || moment(eDateTime).isBetween(x.StartDateTime, x.EndDateTime)
        || moment(x.StartDateTime).isBetween(sDateTime, eDateTime));

    if (!filterActivityUnAvelableItem && !filterUnAvelableItem) {
      if ((!value.IsTask && this.memberList.length > 0) || value.IsTask) {
        const overlapItem = this.bookingList.find(x =>
          moment(sDateTime).isBetween(x.StartDateTime, x.EndDateTime)
          || moment(eDateTime).isBetween(x.StartDateTime, x.EndDateTime)
          || moment(x.StartDateTime).isBetween(sDateTime, eDateTime)
          || moment(sDateTime) === x.StartDateTime
          || moment(eDateTime) === x.EndDateTime);

        if (overlapItem) {
          this.setBookingMembers();
          this.setBookingResource();
          resBooking.BranchId = this.branchId;
          resBooking.StartDateTime = moment(moment(value.StartDate).format('YYYY-MM-DD') + ' ' + value.StartTime).format('YYYY-MM-DD HH:mm:ss');
          resBooking.EndDateTime = moment(moment(value.EndDate).format('YYYY-MM-DD') + ' ' + value.EndTime).format('YYYY-MM-DD HH:mm:ss');
          resBooking.RoleType = 'BOOKING';
          resBooking.SheduleItemId = this.activeTimeDetail.SheduleItemId;
          resBooking.BookingMemberList = this.bookingMemberList;
          resBooking.BookingResourceList = this.bookingResourceList;
          let msg: string;
          let msgType: string;
          this.translateService.get('COMMON.Confirm').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => msgType = translatedValue);
          this.translateService.get('ADMIN.ResourceBusyMsg').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => msg = translatedValue);
          this.exceMessageService.openMessageBox('CONFIRM',
            {
              messageTitle: msgType,
              messageBody: msg,
              msgBoxId: 'BOOKING_OVERLAP',
              optionalData: resBooking,
              IsVisibleYes: true,
              IsVisibleNo: true,
              IsVisibleOk: false
            });
        } else {
          this.setBookingMembers();
          this.setBookingResource();
          resBooking.IsPunchCard = this.articleIsPunchcard;
          resBooking.BranchId = this.branchId;
          resBooking.StartDateTime = moment(moment(value.StartDate).format('YYYY-MM-DD') + ' ' + value.StartTime).format('YYYY-MM-DD HH:mm:ss');
          resBooking.EndDateTime = moment(moment(value.EndDate).format('YYYY-MM-DD') + ' ' + value.EndTime).format('YYYY-MM-DD HH:mm:ss');
          resBooking.RoleType = 'BOOKING';
          resBooking.SheduleItemId = this.activeTimeDetail.SheduleItemId;
          resBooking.BookingMemberList = this.bookingMemberList;
          resBooking.BookingResourceList = this.bookingResourceList;

          this.submitBooking(resBooking, value, content);
        }
      } else {
        this.exceMessageService.openMessageBox('ERROR', { messageTitle: 'Exceline', messageBody: 'ADMIN.MemberSelectionWarning' });
      }
    } else {
      this.exceMessageService.openMessageBox('', { messageTitle: 'Exceline', messageBody: 'ADMIN.TimePeriodNotValid' });
    }
  }

  submitBooking(resBooking, value, content) {
    this.isBookingformSubmitted = true;
    if (this.bookingForm.valid) {
      if  (this.isRepeatable) {
        this.repeatableSave(resBooking, value, content);
      } else {
        this.saveEvent(resBooking, content);
      }
      this.isBookingformSubmitted = false;
    } else {
      UsErrorService.validateAllFormFields(this.bookingForm, this.formErrors, this.validationMessages);
    }
  }

  bookingOverlapYes(resBooking) {
    this.saveEvent(resBooking, null);
  }

  chooseContractOnGroupBooking(resBooking, content) {
    // checking if there are members left in list
    if (this.groupContractBookingMemberList.length > 0) {
      // choose member and pop from list
      const length = this.groupContractBookingMemberList.length
      const memberId: number = this.groupContractBookingMemberList[length - 1]
      this.groupContractBookingMemberList.pop()
      // for each member, check if there are an match
      this.preBookingContractList.forEach(element => {
        const memId = element[0];
        const memName = element[1];
        const memContracts = element[2];
        if (memContracts.length > 1 && memberId === memId) {
          // choose contract on member --> multiple contract on activity found
          this.punchcardContract = memContracts;
          this.punchcardCustomerSelected = memId;
          this.punchcardCustomerSelectedName = memName;
          this.chooseContractModal(content);
        } else {
          // member don't have multiple contracts on activity
          this.chooseContractOnGroupBooking(resBooking, this.chooseContractContent)
        }
      });
    } else {
      // no members left. go back to resource calendar
      this.saveBookingEvent.emit(resBooking);
    }
  }

  multiplePunchcardbooking(resBooking, content) {
    // setting contract modal content
    this.chooseContractContent = content;
    PreloaderService.showPreLoader();
    // get state of contracts on members before submit of booking
    this.memberList.forEach(mem => {
      this.adminService.GetPunchcardContractsOnMember(mem.Id, this.activeTimeDetail.ActivityName).pipe(takeUntil(this.destroy$)).subscribe(
        res => {
          if (res.Data.length > 1) {
            const element = [mem.Id, mem.Name, res.Data]
            this.preBookingContractList.push(element)
          }
        }
      );
    });
    // complete booking
    this.adminService.SaveResourceActiveTime(resBooking).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        PreloaderService.hidePreLoader();
        if (result.Status === 'OK') {
          if (resBooking.Id < 0) {
            resBooking.Id = result.Data;
          }
          // get booking beeing booked, to manipulate if there are changes in punchcard selected
          this.adminService.GetResourceBooking(resBooking.SheduleItemId, resBooking.StartDateTime, resBooking.EndDateTime).pipe(takeUntil(this.destroy$)).subscribe(
            res => {
              PreloaderService.hidePreLoader();
              if (res) {
                this.resourceBooking = res.Data;
                // get members of booking
                this.resourceBooking.forEach(element => {
                  this.groupContractBookingMemberList.push(element.EntityId)
                });
                // choose contract on members where there are multiple contracts on activity.
                // will go in loop to over all members in group booking
                this.chooseContractOnGroupBooking(resBooking, content)
              };
            }
          );
        }
      },
      err => {
        PreloaderService.hidePreLoader();
        this.exceMessageService.openMessageBox('ERROR', { messageTitle: 'Exceline', messageBody: 'ADMIN.ErrorSavingBooking' });
      }
    );
    this.saveBookingEvent.emit(resBooking);
  }

  saveEvent(resBooking, content) {
    this.punchCardResBooking = resBooking;
    // *********** punchcard ************
    if (resBooking.IsPunchCard) {
      PreloaderService.showPreLoader();
      // if booking is group different functionality than single
      if (this.memberList.length > 1) {
        // first save booking, then updateing booking and contracts on contract choice for every
        // member where there are multiple active punchcard contracts on activity
        this.multiplePunchcardBooking = true;
        this.multiplePunchcardbooking(resBooking, content);
      } else {
        // save after choosing contract
          const member = this.memberList[0];
          this.punchcardCustomerSelected = this.memberList[0]
          this.punchcardCustomerSelectedName = member.Name.toString();
          this.adminService.GetPunchcardContractsOnMember(member.Id, this.activeTimeDetail.ActivityName).pipe(takeUntil(this.destroy$)).subscribe(
            res => {
              this.punchcardContract = res.Data;
              if (res.Data.length > 1) {
              this.chooseContractModal(content);
              } else {
                PreloaderService.showPreLoader();
                this.adminService.SaveResourceActiveTime(resBooking).pipe(takeUntil(this.destroy$)).subscribe(
                  res => {
                    PreloaderService.hidePreLoader();
                    if (res.Data > 0) {
                      if (resBooking.Id < 0) {
                        resBooking.Id = res.Data;
                      }
                      this.saveBookingEvent.emit(resBooking);
                    }
                  },
                  err => {
                    PreloaderService.hidePreLoader();
                    this.exceMessageService.openMessageBox('ERROR', { messageTitle: 'Exceline', messageBody: 'ADMIN.ErrorSavingBooking' });
                  }
                );
              }
            }
          )
        }
    // ********** not punchard *********
    // save booking
    } else {
      PreloaderService.showPreLoader();
      this.adminService.SaveResourceActiveTime(resBooking).pipe(takeUntil(this.destroy$)).subscribe(
        res => {
          PreloaderService.hidePreLoader();
          if (res.Data > 0) {
              resBooking.Id = res.Data;
            this.saveBookingEvent.emit(resBooking);
          }
        },
        err => {
          PreloaderService.hidePreLoader();
          this.exceMessageService.openMessageBox('ERROR', { messageTitle: 'Exceline', messageBody: 'ADMIN.ErrorSavingBooking' });
        }
      );
    }
  }

  setBookingMembers() {
    if (this.memberList) {
      if (this.bookingMemberList) {
        const deletedMemList: any = [];
        this.bookingMemberList.forEach(bookingMem => {
          const index: number = this.memberList.findIndex(i => i.Id === bookingMem.Id);
          if (index === -1) {
            bookingMem.IsDeleted = true;
            deletedMemList.push(bookingMem);
          }
        });
        this.bookingMemberList = [];
        this.memberList.forEach(i => {
          this.bookingMemberList.push(i);
        });
        deletedMemList.forEach(i => {
          this.bookingMemberList.push(i);
        });
      } else {
        this.bookingMemberList = [];
        this.memberList.forEach(i => {
          this.bookingMemberList.push(i);
        });
      }
    }
  }

  setBookingResource() {
    if (this.resList) {
      if (this.bookingResourceList) {
        const deletedResList: any = [];
        this.bookingResourceList.forEach(bookingRes => {
          const index: number = this.resList.findIndex(i => i.Id === bookingRes.Id);
          if (index === -1) {
            bookingRes.IsDeleted = true;
            deletedResList.push(bookingRes);
          }
        });

        this.bookingResourceList = [];
        this.resList.forEach(i => {
          this.bookingResourceList.push(i);
        });
        deletedResList.forEach(i => {
          this.bookingResourceList.push(i);
        });
      } else {
        this.bookingResourceList = [];
        this.resList.forEach(i => {
          this.bookingResourceList.push(i);
        });
      }
    }
  }

  closeBooking() {
    this.cancelBookingEvent.emit(this.activeTimeDetail);
  }

  inputFieldChanged(event) {
    this.setTimes(event, 30);
  }

  setTimes(startTime, duration) {
    const date = moment(new Date()).format('YYYY-MM-DD');
  }

  deleteMemberNewBooking(content, item) {
    this.shopPaymentContent = content;
   // this.resourceBookingPaymentCancel(item); check for the shop payment
    const deItem = this.memberList.find(x => x.Id === item.Id);
    if (deItem) {
      // if ( deItem.ItemTypeList.find(x => x === 'NO' || x === 'EML' || x === 'IN')) {
      //    this.exceMessageService.openMessageBox('ERROR', { messageTitle: 'Exceline', messageBody: 'ADMIN.BookingMemberDeletedValidation' });
      //    return;
      // }

      if (this.activeTimeDetail.ArticleId > 0) {
        if (this.activeTimeDetail.IsPunchCard || this.activeTimeDetail.IsContractBooking) {
          this.punchCardContractBookingMemberCancel(deItem);
        } else if (deItem.IsPaid) {
          this.paidBookingMemberCancel(deItem);
        } else {
          const index: number = this.memberList.findIndex(i => i.Id === deItem.Id);
          if (index !== -1) {
            this.memberList.splice(index, 1);
          }
        }
      } else {
        const index: number = this.memberList.findIndex(i => i.Id === deItem.Id);
        if (index !== -1) {
          this.memberList.splice(index, 1);
        }
      }
    } else {
      const index: number = this.memberList.findIndex(i => i.Id === item.Id);
      if (index !== -1) {
        this.memberList.splice(index, 1);
      }
    }
    this.isMemberSelected = false;
  }

  deleteMember(content, item) {
    this.shopPaymentContent = content;
   // this.resourceBookingPaymentCancel(item); check for the shop payment
    const deItem = this.memberList.find(x => x.Id === item.Id);
    if (deItem) {
      if ( deItem.ItemTypeList.find(x => x === 'NO' || x === 'EML' || x === 'IN')) {
         this.exceMessageService.openMessageBox('ERROR', { messageTitle: 'Exceline', messageBody: 'ADMIN.BookingMemberDeletedValidation' });
         return;
      }

      if (this.activeTimeDetail.ArticleId > 0) {
        if (this.activeTimeDetail.IsPunchCard || this.activeTimeDetail.IsContractBooking) {
          this.punchCardContractBookingMemberCancel(deItem);
        } else if (deItem.IsPaid) {
          this.paidBookingMemberCancel(deItem);
        } else {
          const index: number = this.memberList.findIndex(i => i.Id === deItem.Id);
          if (index !== -1) {
            this.memberList.splice(index, 1);
          }
        }
      } else {
        const index: number = this.memberList.findIndex(i => i.Id === deItem.Id);
        if (index !== -1) {
          this.memberList.splice(index, 1);
        }
      }
    } else {
      const index: number = this.memberList.findIndex(i => i.Id === item.Id);
      if (index !== -1) {
        this.memberList.splice(index, 1);
      }
    }
  }

  resourceBookingPaymentCancel(member) {
    this.payMember = member;
    this.shopService.GetSelectedGymSettings({ GymSetColNames: this.selectedGymsettings, IsGymSettings: true }).pipe(takeUntil(this.destroy$)).subscribe(
      res => {
        for (const key in res.Data) {
          if (res.Data.hasOwnProperty(key)) {
            switch (key) {
              case 'EcoNegativeOnAccount':
                this.exceGymSetting.OnAccountNegativeVale = res.Data[key];
                break;
              case 'EcoIsShopOnNextOrder':
                this.exceGymSetting.IsShopOnNextOrder = res.Data[key];
                break;
              case 'EcoIsDailySettlementForAll':
                this.exceGymSetting.IsDailySettlementForAll = res.Data[key];
                break;
              case 'EcoIsOnAccount':
                this.exceGymSetting.IsOnAccount = res.Data[key];
                break;
              case 'EcoIsDefaultCustomerAvailable':
                this.exceGymSetting.IsDefaultCustomerAvailable = res.Data[key];
                break;
              case 'EcoIsPayButtonsAvailable':
                this.exceGymSetting.IsPayButtonsAvailable = res.Data[key];
                break;
              case 'EcoIsPettyCashSameAsUserAllowed':
                this.exceGymSetting.IsPettyCashSameAsUserAllowed = res.Data[key];
                break;
              case 'EcoIsPrintInvoiceInShop':
                this.exceGymSetting.IsPrintInvoiceInShop = res.Data[key];
                break;
              case 'EcoIsPrintReceiptInShop':
                this.exceGymSetting.IsPrintReceiptInShop = res.Data[key];
                break;
              case 'OtherIsShopAvailable':
                this.exceGymSetting.IsShopAvailable = res.Data[key];
                break;
              case 'OtherLoginShop':
                this.exceGymSetting.IsShopLoginNeeded = res.Data[key];
                break;
              case 'HwIsShopLoginWithCard':
                this.exceGymSetting.IsShopLoginWithCard = res.Data[key];
                break;
              case 'ReqMemberListRequired':
                this.exceGymSetting.MemberRequired = res.Data[key];
                break;
              case 'OtherIsLogoutFromShopAfterSale':
                this.exceGymSetting.IsLogoutFromShopAfterSale = res.Data[key];
                break;
              case 'EcoSMSInvoiceShop':
                this.exceGymSetting.IsSMSInvoiceShop = res.Data[key];
                break;
              case 'EcoIsBankTerminalIntegrated':
                this.exceGymSetting.IsBankTerminalIntegrated = res.Data[key];
                break;
              case 'OtherIsValidateShopGymId':
                this.exceGymSetting.IsValidateShopGymId = res.Data[key];
                break;
            }

          }
        }
        this.shopLoginData.GymSetting = this.exceGymSetting;
        this.shopService.GetSalesPoint().pipe(takeUntil(this.destroy$)).subscribe(
          salePoints => {
            this.shopLoginData.SalePoint = salePoints.Data[0];
            this.adminService.getGymSettings('HARDWARE', null).pipe(takeUntil(this.destroy$)).subscribe(
              hwSetting => {
                this.shopLoginData.HardwareProfileList = hwSetting.Data;
                this.shopLoginData.HardwareProfileList.forEach(hwProfile => {
                  if (this.shopLoginData.SalePoint != null &&
                    this.shopLoginData.SalePoint.HardwareProfileId === hwProfile.Id) {
                    this.shopLoginData.SelectedHardwareProfile = hwProfile;
                    this.shopLoginData.HardwareProfileId = hwProfile.Id;
                  } else {
                    this.shopLoginData.SelectedHardwareProfile = null;
                    this.shopLoginData.HardwareProfileId = null;
                  }
                  this.shopHomeService.$shopLoginData = this.shopLoginData;

                });

                 const articleSearch = {
                  ActivityId: this.activeTimeDetail.ActivityId,
                  BranchId: this.branchId
                }
                this.adminService.GetArticleForResouceBooking(articleSearch).pipe(takeUntil(this.destroy$)).subscribe
                  (result => {
                    if (result) {
                      if (this.activeTimeDetail.ArticleId > 0) {
                        const articleItem = result.Data.find(x => x.Id === this.activeTimeDetail.ArticleId && x.CategoryCode === 'BOOKING' && !x.IsPunchCard && !x.IsContractBooking);
                        if (articleItem) {
                          articleItem.DefaultPrice = member.Amount;
                        }
                        this.shopService.GetMemberShopAccountsForEntityType({ memberId: member.Id, entityType: 'MEM' }).pipe(takeUntil(this.destroy$)).subscribe(
                          resShopAccount => {
                            this.shopHomeService.$shopMode = 'BOOKINGCANCELLATION';
                            this.shopHomeService.$shopAccount = res.Data;
                            this.shopHomeService.$isCreditNote = true;
                            this.shopHomeService.$totalPrice = member.Amount;
                            this.shopHomeService.$arItemNo = member.ArItemNo;
                            this.shopHomeService.$articleId = this.activeTimeDetail.ArticleId;
                            this.shopHomeService.$articleName = this.activeTimeDetail.ArticleName;
                            this.shopHomeService.$selectedMember = member;
                            this.shopHomeService.$priceType = 'MEMBER';
                            this.shopPaymentView = this.modalService.open(this.shopPaymentContent);
                            // this.shopHomeService.$shopAccount = resShopAccount.Data;
                            // this.shopHomeService.$isOnAccountPayment = true;
                            // this.shopPaymentView = this.modalService.open(this.shopPaymentContent );
                          });
                      }
                    }
                  });
              },
              error => {

              });
          },
          error => {

          });



      },
      error => {

      }
    )

  }

  closePaymentView(response): void {
    //  this.GetMemberShopAccount();
    const bookingPayment = {
      ActivetimeId: this.activeTimeDetail.Id,
      ArticleId: -1,
      MemberId: this.payMember.Id,
      Paid: false,
      AritemNo: this.payMember.AritemNo,
      Amount: this.payMember.Amount,
      PaymentType: ''
    }
    if (response) {
      this.adminService.SavePaymentBookingMember(bookingPayment).pipe(takeUntil(this.destroy$)).subscribe(
        res => {
          if (res.Data > 0) {
            const index: number = this.memberList.findIndex(i => i.Id === this.payMember.Id);
            if (index !== -1) {
              this.memberList.splice(index, 1);
            }

            if (this.bookingMemberList && this.bookingMemberList.length > 0) {
            const paidMember = this.bookingMemberList.find(x => x.Id === this.payMember.Id);
            if (paidMember) {
              paidMember.IsPaid = false;
            }
            }
          } else {
            this.exceMessageService.openMessageBox('ERROR', { messageTitle: 'Exceline', messageBody: 'ADMIN.PiadBookingDataUpdateFailed' });
          }
        });
      this.shopPaymentView.close();
    }
  }

  punchCardContractBookingMemberCancel(deletedMember) {
    if (deletedMember) {
      let msg: string;
      let msgType: string;
      this.translateService.get('COMMON.Confirm').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => msgType = translatedValue);
      this.translateService.get('ADMIN.Doyouwanttoreturnpunch').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => msg = translatedValue);
      this.exceMessageService.openMessageBox('CONFIRM',
        {
          messageTitle: msgType,
          messageBody: msg,
          msgBoxId: 'DLETE_PUNCHCARD_BOOKING',
          optionalData: deletedMember
        });
    }
  }

  punchCardContractBookingCancelYesHandler(deletedMember) {
    deletedMember.IsBookingCreditback = true;
    const index: number = this.memberList.findIndex(i => i.Id === deletedMember.Id);
    if (index !== -1) {
      this.memberList.splice(index, 1);
    }
  }

  punchCardContractBookingCancelNoHandler(deletedMember) {
    deletedMember.IsBookingCreditback = false;
    const index: number = this.memberList.findIndex(i => i.Id === deletedMember.Id);
    if (index !== -1) {
      this.memberList.splice(index, 1);
    }
  }

  paidBookingCancelYesHandler(deletedMember) {
    this.resourceBookingPaymentCancel(deletedMember);
  }

  paidBookingCancelNoHandler(deletedMember) {
    const index: number = this.memberList.findIndex(i => i.Id === deletedMember.Id);
    if (index !== -1) {
      this.memberList.splice(index, 1);
    }
  }



  paidBookingMemberCancel(deletedMember) {
    if (deletedMember) {
      let msg: string;
      let msgType: string;
      this.translateService.get('COMMON.Confirm').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => msgType = translatedValue);
      this.translateService.get('ADMIN.Doyouwanttocreaditback').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => msg = translatedValue);
      this.exceMessageService.openMessageBox('CONFIRM',
        {
          messageTitle: msgType,
          messageBody: msg,
          msgBoxId: 'DLETE_PAID_BOOKING',
          optionalData: deletedMember
        });
    }
  }

  checkOut(item) {
  }

  openShop(content, item) {
    this.selectedMember = item;
    this.shopService.GetSelectedGymSettings({ GymSetColNames: this.selectedGymsettings, IsGymSettings: true }).pipe(takeUntil(this.destroy$)).subscribe(
      res => {
        for (const key in res.Data) {
          if (res.Data.hasOwnProperty(key)) {
            switch (key) {
              case 'EcoNegativeOnAccount':
                this.exceGymSetting.OnAccountNegativeVale = res.Data[key];
                break;
              case 'EcoIsShopOnNextOrder':
                this.exceGymSetting.IsShopOnNextOrder = res.Data[key];
                break;
              case 'EcoIsDailySettlementForAll':
                this.exceGymSetting.IsDailySettlementForAll = res.Data[key];
                break;
              case 'EcoIsOnAccount':
                this.exceGymSetting.IsOnAccount = res.Data[key];
                break;
              case 'EcoIsDefaultCustomerAvailable':
                this.exceGymSetting.IsDefaultCustomerAvailable = res.Data[key];
                break;
              case 'EcoIsPayButtonsAvailable':
                this.exceGymSetting.IsPayButtonsAvailable = res.Data[key];
                break;
              case 'EcoIsPettyCashSameAsUserAllowed':
                this.exceGymSetting.IsPettyCashSameAsUserAllowed = res.Data[key];
                break;
              case 'EcoIsPrintInvoiceInShop':
                this.exceGymSetting.IsPrintInvoiceInShop = res.Data[key];
                break;
              case 'EcoIsPrintReceiptInShop':
                this.exceGymSetting.IsPrintReceiptInShop = res.Data[key];
                break;
              case 'OtherIsShopAvailable':
                this.exceGymSetting.IsShopAvailable = res.Data[key];
                break;
              case 'OtherLoginShop':
                this.exceGymSetting.IsShopLoginNeeded = res.Data[key];
                break;
              case 'HwIsShopLoginWithCard':
                this.exceGymSetting.IsShopLoginWithCard = res.Data[key];
                break;
              case 'ReqMemberListRequired':
                this.exceGymSetting.MemberRequired = res.Data[key];
                break;
              case 'OtherIsLogoutFromShopAfterSale':
                this.exceGymSetting.IsLogoutFromShopAfterSale = res.Data[key];
                break;
              case 'EcoSMSInvoiceShop':
                this.exceGymSetting.IsSMSInvoiceShop = res.Data[key];
                break;
              case 'EcoIsBankTerminalIntegrated':
                this.exceGymSetting.IsBankTerminalIntegrated = res.Data[key];
                break;
              case 'OtherIsValidateShopGymId':
                this.exceGymSetting.IsValidateShopGymId = res.Data[key];
                break;
            }

          }
        }
        this.shopLoginData.GymSetting = this.exceGymSetting;
        this.shopService.GetSalesPoint().pipe(takeUntil(this.destroy$)).subscribe(
          salePoints => {
            this.shopLoginData.SalePoint = salePoints.Data[0];
            this.adminService.getGymSettings('HARDWARE', null).pipe(takeUntil(this.destroy$)).subscribe(
              hwSetting => {
                this.shopLoginData.HardwareProfileList = hwSetting.Data;
                this.shopLoginData.HardwareProfileList.forEach(hwProfile => {
                  if (this.shopLoginData.SalePoint != null &&
                    this.shopLoginData.SalePoint.HardwareProfileId === hwProfile.Id) {
                    this.shopLoginData.SelectedHardwareProfile = hwProfile;
                    this.shopLoginData.HardwareProfileId = hwProfile.Id;
                  } else {
                    this.shopLoginData.SelectedHardwareProfile = null;
                    this.shopLoginData.HardwareProfileId = null;
                  }
                  this.shopHomeService.$shopLoginData = this.shopLoginData;

                });
                this.setCheoutEvent(content, item);
              },
              error => {
              });
          },
          error => {
          });
      },
      error => {
      }
    )
  }

  setCheoutEvent(content, member) {
    const orderArticles = [];
    this.shopHomeService.$selectedMember = member;
    if (this.activeTimeDetail.ActivityId > 0) {
      const articleSearch = {
        ActivityId: this.activeTimeDetail.ActivityId,
        BranchId: this.branchId
      }
      this.adminService.GetArticleForResouceBooking(articleSearch).pipe(takeUntil(this.destroy$)).subscribe
        (result => {
          if (result) {
            if (this.activeTimeDetail.ArticleId > 0) {
              const articleItem = result.Data.find(x => x.Id === this.activeTimeDetail.ArticleId && x.CategoryCode === 'BOOKING' &&
                !x.IsPunchCard && !x.IsContractBooking);
              if (articleItem) {
                if (articleItem.DefaultPrice <= 0) {
                  this.exceMessageService.openMessageBox('ERROR', { messageTitle: 'Exceline', messageBody: 'ADMIN.ArticlePriceIsZero' });
                } else {
                  articleItem.Price = articleItem.DefaultPrice;
                  orderArticles.push({
                    Id: articleItem.Id,
                    ArticleNo: articleItem.ArticleId,
                    Description: articleItem.Description,
                    SelectedPrice: articleItem.Price,
                    UnitPrice: articleItem.Price,
                    DefaultPrice: articleItem.Price,
                    Price: articleItem.Price,
                    OrderQuantity: 1,
                    Discount: articleItem.Discount,
                    ExpiryDate: articleItem.ExpiryDate,
                    VoucherNumber: articleItem.VoucherNo,
                    IsEditable: false
                  })
                  this.shopHomeService.$priceType = 'MEMBER';
                  this.shopHomeService.$shopMode = 'BOOKING';
                  this.shopHomeService.ShopItems = orderArticles;
                  this.shopHomeService.$isUpdateInstallments = false;
                  this.shopModalReference = this.modalService.open(content);
                }
              } else {
                this.exceMessageService.openMessageBox('ERROR', { messageTitle: 'Exceline', messageBody: 'ADMIN.ArticlesOfPunchcard' });
              }
            } else {
              this.shopHomeService.$priceType = 'MEMBER';
              this.shopHomeService.$shopMode = 'BOOKING';
              this.shopHomeService.$activityId = this.activeTimeDetail.ActivityId;
              this.shopHomeService.$isUpdateInstallments = false;
              this.shopModalReference = this.modalService.open(content);
            }
          }
        }
        )
    }
  }

  getTransType(paymentType) {
    switch (paymentType) {
        case 'CASH':
            return 'CSH';

        case 'BANKTERMINAL':
            return 'TMN';

        case 'OCR':
            return 'LOP';

        case 'BANKSTATEMENT':
            return 'BS';

        case 'DEBTCOLLECTION':
            return 'DC';

        case 'ONACCOUNT':
            return 'OA';

        case 'GIFTCARD':
            return 'GC';

        case 'BANK':
            return 'BNK';

        case 'PREPAIDBALANCE':
            return 'PB';

        case 'EMAILSMSINVOICE':
            return 'EML';

        case 'NEXTORDER':
            return 'NO';

        case 'INVOICE':
            return 'IN';

    }
    return '';
  }

  closeShopAfterSave(response) {
    let paymentType = '';
    response.payModes.forEach( (payMode, index) => {
      if (index === 0) {
        paymentType = this.getTransType(payMode.PaymentTypeCode);
      } else {
        paymentType = ',' + this.getTransType(payMode.PaymentTypeCode);
      }
    });

    const bookingPayment = {
      ActivetimeId: this.activeTimeDetail.Id,
      ArticleId: response.articlesPaid[0].ArticleId,
      MemberId: this.selectedMember.Id,
      Paid: true,
      AritemNo: response.arItemNo,
      Amount: response.totalPrice,
      PaymentType: paymentType
    }
    this.adminService.SavePaymentBookingMember(bookingPayment).pipe(takeUntil(this.destroy$)).subscribe(
      res => {
        if (res.Data > 0) {
          const paidMember = this.memberList.find(x => x.Id === this.selectedMember.Id);
          if (paidMember) {
            paidMember.IsPaid = true;
            paidMember.Amount = response.totalPrice;
            paidMember.ArItemNo = response.arItemNo;
            if (paymentType) {
              const paymentTypeArray = paymentType.split(',');
              paidMember.ItemTypeList = paymentTypeArray;
            }
          }
        }
      });
    this.shopModalReference.close();
  }

  closeShop() {
    this.shopModalReference.close();
  }

  deleteResource(res) {
    const index: number = this.resList.findIndex(i => i.Id === res.Id);
    if (index !== -1) {
      this.resList.splice(index, 1);
    }
  }

  memberSelectionView(content: any, entitySelectionType: string) {
    this.items = [];
    this.columns = [];
    this.entitySelectionType = entitySelectionType;
    if (this.entitySelectionType === 'MEM') {
      if (this.activeTimeDetail.ArticleId > 0 && (this.activeTimeDetail.IsPunchCard || this.activeTimeDetail.IsContractBooking)) {
        this.isAddBtnVisible = false;
      } else {
        this.isAddBtnVisible = true;
      }
      this.isBranchVisible = true;
      this.isRoleVisible = true;
      this.isStatusVisible = true;
      this.isDbPagination = true;
      this.isBasicInforVisible = true;
      this.defaultRole = 'ALL'
      this.multipleRowSelect = true;
      this.clickable = true;

      this.columns = [{ property: 'FirstName', header: 'First Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
      { property: 'LastName', header: 'Last Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
      { property: 'IntCustId', header: 'Number', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
      { property: 'Age', header: 'Age', sortable: false, resizable: false, width: '150px', type: ColumnDataType.DATE },
      { property: 'Mobile', header: 'Mobile', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
      { property: 'StatusName', header: 'Status', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
      { property: 'BranchName', header: 'Gym Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT }
      ];
      PreloaderService.showPreLoader();
      this.adminService.getMembers(this.branchId, '', 10,
        'SEARCH', 'MEM', 1, false,
        false, '').pipe(takeUntil(this.destroy$)).subscribe(result => {
          if (result) {
            PreloaderService.hidePreLoader();
            if (this.memberList) {
             this.memberList.forEach(member => {
             const index: number = result.Data.findIndex(item => item.Id === member.Id);
               if (index !== -1) {
                result.Data.splice(index, 1);
               }
             });
            }
            this.items = result.Data;
            this.itemCount = result.Data.length;
            this.memberSelectView = this.modalService.open(content, {});
          } else {
            PreloaderService.hidePreLoader();
          }
        }
        )
    } else { // If event is 'Extra Resource'
      this.isAddBtnVisible = false;
      this.isBranchVisible = false;
      this.isRoleVisible = false;
      this.isStatusVisible = false;
      this.isDbPagination = false;
      this.clickable = true;
      this.isBasicInforVisible = false;
      this.multipleRowSelect = true;
      this.columns = [{ property: 'Name', header: 'Name', sortable: true, clickable: false, resizable: true, width: '150px', type: ColumnDataType.DEFAULT }
      ];
      PreloaderService.showPreLoader();
      this.adminService.GetResources(this.branchId, '', -1, -1, 2, true).pipe(takeUntil(this.destroy$)).subscribe(result => {
          if (result) {
            const extraResources = result.Data;
            PreloaderService.hidePreLoader();

            const extraResourcesWithoutDupllicate = [];
            // Only add the resources that are not currently selected
            extraResources.forEach(resource => {
              if (resource.Id !== this.resScheduleItem.EntityId) {
                extraResourcesWithoutDupllicate.push(resource)
              }
            })
            // if (this.resList) {
/*               extraResources.forEach(resource => {
                console.log(resource)
                const index: number = extraResources.findIndex(item => item.Id === resource.Id);
                if (index !== -1) {
                  extraResources.splice(index, 1);
                }
              }); */
            // }
            this.items = extraResourcesWithoutDupllicate;
            this.itemCount = extraResourcesWithoutDupllicate.length;
            this.memberSelectView = this.modalService.open(content, {});
          } else {
            PreloaderService.hidePreLoader();
          }
        });
    }
  }

  selectedRowItem(item) {
    if (this.entitySelectionType === EntitySelectionType[EntitySelectionType.MEM]) {
      // item.forEach(i => {
      //   this.memberList.push(i);
      // })
      this.memberList.push(item);
      this.isMemberSelected = true;
    } else if (this.entitySelectionType === EntitySelectionType[EntitySelectionType.RES]) {
      this.resList.push(item)
      this.categorySelectedVal = true;
    }
    this.memberSelectView.close();
  }

  selectedRowItems(item) {
    let breakOutOfLoop = false;
    if (item !== null) {
      this.isMemberSelected = true;
      if (this.entitySelectionType === EntitySelectionType[EntitySelectionType.MEM]) {
        item.forEach(i => {
          if (this.memberList.length === 0) {
            this.memberList.push(i);
          } else {
            this.memberList.forEach(element => {
              if (element.id === i.Id) {
                breakOutOfLoop = true;
                this.exceMessageService.openMessageBox('ERROR', { messageTitle: 'ADMIN.BookingError', messageBody: 'ADMIN.DuplicateMemberInBooking' });
              }
            });
            if (!breakOutOfLoop) {
              this.memberList.push(i);
            }
          }
         })
        this.validateSelectedMembers(item);
      } else if (this.entitySelectionType === EntitySelectionType[EntitySelectionType.RES]) {
        item.forEach(i => {
          this.resList.push(i);
        })
      }
    }
    this.memberSelectView.close();
  }

  validateSelectedMembers(memList: any) {
    let erorMsg = '';
    let tranMsg = '';
    const memberIdLst: any = [];
    // let logbranchAccountNo: number;
    if (this.bookingMemberList) {
      memList.forEach(mem => {
        const index: number = this.bookingMemberList.findIndex(item => item.Id === mem.Id);
        if (index === undefined || index === -1) {
          memberIdLst.push(mem.Id);
        }
      });

      this.memberList.forEach(mem => {
        const index: number = this.bookingMemberList.findIndex(item => item.Id === mem.Id);
        if (index === undefined || index === -1) {
          memberIdLst.push(mem.Id);
        }
      });
    } else {
      memList.forEach(mem => {
        memberIdLst.push(mem.Id);
      });
    }

    if (memberIdLst && this.activeTimeDetail.ArticleId > 0 && this.activeTimeDetail.IsPunchCard) {
      const punchCardValid = {
        ActivityId: this.activeTimeDetail.ActivityId,
        MemberIdLst: memberIdLst,
        StartDate: moment(this.activeTimeDetail.StartDateTime).format('YYYY-MM-DD'),
        AccountNo: this.logbranchAccountNo,
        BranchId: this.branchId
      }
      this.adminService.ValidatePunchcardContractMember(punchCardValid).pipe(takeUntil(this.destroy$)).subscribe(
        res => {
          if (res.Data > 0) {
            let memberValidPunch: any = [];
            let memberValidAccount: any = [];
            const listDiff: any = [];
            const listDiffAccountNo: any = [];
            // for (const key in res.Data) {
            //   if (res.Data.hasOwnProperty(key)) {
            memberValidPunch = res.Data['validMem'];
            memberValidAccount = res.Data['validMemAcc'];
            memberIdLst.forEach(memId => {
              const index: number = memberValidPunch.findIndex(item => item === memId);
              if (index === undefined || index === -1) {
                listDiff.push(memId);
              }
            });

            memberIdLst.forEach(memId => {
              const index: number = memberValidAccount.findIndex(item => item === memId);
              if (index === undefined || index === -1) {
                listDiffAccountNo.push(memId);
              }
            });
            //   }
            // }

            if (listDiff && listDiff.length > 0) {  // validate member same branch
              listDiff.forEach(i => {
                const item = memList.find(x => x.Id = i);
                if (item) {
                  erorMsg = item.Name;
                }
              });
              this.translateService.get('ADMIN.PunchCardArticleMemberError').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => tranMsg = tranlstedValue);
              erorMsg = erorMsg + '\n' + tranMsg;
            } else if (listDiffAccountNo && listDiffAccountNo.length > 0) {
              listDiffAccountNo.forEach(i => {
                const item = memList.find(x => x.Id = i);
                if (item) {
                  erorMsg = item.Name;
                }
              });
              this.translateService.get('ADMIN.PunchcardArticleErrorWithAccount').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => tranMsg = tranlstedValue);
              erorMsg = erorMsg + '\n' + tranMsg;
            } else {
              this.setMember(memList);
            }
          }
        }
      );
    } else if (memberIdLst && this.activeTimeDetail.ArticleId > 0 && this.activeTimeDetail.IsContractBooking) {
      const contractBookingValid = {
        ActivityId: this.activeTimeDetail.ActivityId,
        MemberIdLst: memberIdLst,
        startDate: this.activeTimeDetail.StartDateTime,
        AccountNo: this.logbranchAccountNo,
        BranchId: this.branchId
      }

      this.adminService.CheckUnusedCancelBooking(contractBookingValid).pipe(takeUntil(this.destroy$)).subscribe(
        res => {
          if (res.Data > 0) {
            let memberValidPunch: any = [];
            let memberValidAccount: any = [];
            const listDiff: any = [];
            const listDiffAccountNo: any = [];
            // for (const key in res.Data) {
            //   if (res.Data.hasOwnProperty(key)) {
            memberValidPunch = res.Data['validMem'];
            memberValidAccount = res.Data['validMemAcc'];
            memberIdLst.forEach(memId => {
              const index: number = memberValidPunch.findIndex(item => item === memId);
              if (index === undefined || index === -1) {
                listDiff.push(memId);
              }
            });

            memberIdLst.forEach(memId => {
              const index: number = memberValidAccount.findIndex(item => item === memId);
              if (index === undefined || index === -1) {
                listDiffAccountNo.push(memId);
              }
            });
            //  }
            //  }

            if (listDiff && listDiff.length > 0) {  // validate member same branch
              listDiff.forEach(i => {
                const item = memList.find(x => x.Id = i);
                if (item) {
                  erorMsg = item.Name;
                }
              });
              this.translateService.get('ADMIN.MemberHasNotUnusedCancelBooking').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => tranMsg = tranlstedValue);
              erorMsg = erorMsg + '\n' + tranMsg;
            } else if (listDiffAccountNo && listDiffAccountNo.length > 0) {
              listDiffAccountNo.forEach(i => {
                const item = memList.find(x => x.Id = i);
                if (item) {
                  erorMsg = item.Name;
                }
              });
              this.translateService.get('ADMIN.ErrorUnusedCancelBookingWithAccountNo').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => tranMsg = tranlstedValue);
              erorMsg = erorMsg + '\n' + tranMsg;
            } else {
              this.setMember(memList);
            }
          }
        });
    } else {
      this.setMember(memList);
    }

  }

  setMember(memList) {
    if (memList) {
      memList.forEach(mem => {
        const index: number = this.memberList.find(item => item.Id === mem.Id);
        if (index === undefined || index === -1) {
          // this.memberList.push(mem);
          this.memberList.push({
            Id: mem.Id, Name: mem.Name, CustId: mem.CustId, MemberRole: mem.Role.toString(), IsPaidBtnVisible: false, IsDeleted: false
            , BranchId: mem.BranchID, GuardianId: mem.GuardianId, IsPaid: false, IsArrived: false
          });
        }
      });
    }
  }

  searchDeatail(val: any) {
    switch (val.entitySelectionType) {
      case EntitySelectionType[EntitySelectionType.MEM]:
        PreloaderService.showPreLoader();
        this.adminService.getMembers(val.branchSelected.BranchId, val.searchVal, val.statusSelected.id,
          'SEARCH', val.roleSelected.id, val.hit, false,
          false, ''
        ).pipe(takeUntil(this.destroy$)).subscribe
          (result => {
            if (result) {
              PreloaderService.hidePreLoader();
              this.memberList.forEach(member => {
                const index: number = result.Data.find(item => item.Id === member.Id);
                if (index !== -1) {
                  result.Data.splice(index, 1);
                }
              });
              this.items = result.Data;
              this.itemCount = result.Data.length;
            }
          }
          );
        break;
      case EntitySelectionType[EntitySelectionType.RES]:
        PreloaderService.showPreLoader();
        this.adminService.GetResources(this.branchId, '', -1, -1, 2, true).pipe(takeUntil(this.destroy$)).subscribe
          (result => {
            if (result) {
              PreloaderService.hidePreLoader();
              this.resList.forEach(res => {
                const index: number = result.Data.find(item => item.Id === res.Id);
                if (index !== -1) {
                  result.Data.splice(index, 1);
                }
              });
              this.items = result.Data;
              this.itemCount = result.Data.length;
            } else {
              PreloaderService.hidePreLoader();
            }
          }
          )
        break;
    }
  }
  addMemberModal(content: any) {
    this.addMemberView = this.modalService.open(content, { width: '900' });
    this.memberSelectView.close();
  }

  chooseContractModal(content: any) { /* in ng-template chooseContract */
    this.chooseContractView = this.modalService.open(content, { width: '600'});
    // this.
  }

  addEntity(item: any) {
    this.memberList.push(item);
    this.addMemberView.close();
  }
  closeView() {
    this.memberSelectView.close();
  }

  closeArticleView() {
    this.articleModel.close();
  }

  closeCategoryView() {
    this.categorySelectedVal = false;
    this.categoryModel.close();
  }

  closeChooseContractView() {
    this.chooseContractView.close();
  }

  punchardSelect(event) {
    if (this.isRepeatable) {
      this.punchcardContracts.forEach(element => {
        if (element.ContractId === event.ContractId) {
          this.punchcardContractId = event.ContractId
        }
      });
      this.adminService.SaveResourceActiveTimeRepeatablePunchcard(this.punchcardContractId, this.activeItemsList).pipe(takeUntil(this.destroy$)).subscribe(
        result => {
        }, error => {
        }, () => {
          this.saveBookingEvent.emit();
          this.closeChooseContractView();
        }
      )
      return
    }
    // check if it's single or group booking of punchard
    if (!this.multiplePunchcardBooking) {
      PreloaderService.showPreLoader();
      this.adminService.SaveResourceActiveTimePunchcard(this.punchCardResBooking, event.ContractId).pipe(takeUntil(this.destroy$)).subscribe(
        res => {
          PreloaderService.hidePreLoader();
          if (res.Data > 0) {
            if (this.punchCardResBooking.Id < 0) {
              this.punchCardResBooking.Id = res.Data;
            }
            this.saveBookingEvent.emit(this.punchCardResBooking);
            this.closeChooseContractView();
          }
        },
        err => {
          PreloaderService.hidePreLoader();
          this.exceMessageService.openMessageBox('ERROR', { messageTitle: 'Exceline', messageBody: 'ADMIN.ErrorSavingBooking' });
        }
      );
    } else {
      // check if punchcard booked is the same as choosen,
      // if not, update availablevisist on contracts and ExceMemberBoking-table contractID
      this.resourceBooking.forEach(element => {
        const x = this.punchcardCustomerSelected.toString()
        if (this.punchcardCustomerSelected.toString() === element.EntityId) {
          const bookingId = element.ID;
          const contractPunched = element.ContractId;
          if (event.ContractId !== contractPunched) {
            this.adminService.UpdateContractVisits(event.ContractId, contractPunched, bookingId).pipe(takeUntil(this.destroy$)).subscribe(
              res => {
                if (res.Status === 'OK') {

                }

              }
            );
          }
        }
      });

      this.closeChooseContractView();
      this.chooseContractOnGroupBooking(this.punchCardResBooking, this.chooseContractContent)
    }
  }

  categorySelected(item) {
    this.bookingForm.patchValue({
      BookingCategoryId: item.Id,
      BookingCategoryName: item.Name
    });
    this.categorySelectedVal = true;
    this.categoryModel.close();
  }

  categorySelectionView(content: any) {
    this.adminService.getCategories('BOOKINGCATEGORY').pipe(takeUntil(this.destroy$)).subscribe(
      res => {
        this.categoryList = res.Data;
        this.categoryModel = this.modalService.open(content, { width: '900' });
      }, err => {
        console.log(err);
      });
  }

  articleSelectionView(content: any) {
    if (this.activeTimeDetail.ActivityId > 0) {
      const articleSearch = {
        ActivityId: this.activeTimeDetail.ActivityId,
        BranchId: this.branchId
      }
      PreloaderService.showPreLoader();
      this.adminService.GetArticleForResouceBooking(articleSearch).pipe(takeUntil(this.destroy$)).subscribe
        (result => {
          if (result) {
            PreloaderService.hidePreLoader();
            this.articleList = result.Data;
            this.articleViewList = this.articleList;
            this.articleModel = this.modalService.open(content, { width: '900' });
          }
        }
        )
    }
  }

  filterInput(accNameValue?: any, accNoValue?: any) {
    let filteredArticlesName = this.articleList;
    filteredArticlesName = this.filterpipe.transform('Description', 'NAMEFILTER', filteredArticlesName, accNameValue);
    let filteredArticles = filteredArticlesName;
    filteredArticles = this.filterpipe.transform('Id', 'NAMEFILTER', filteredArticles, accNoValue);
    this.articleItemSource = new DataTableResource(filteredArticles);
    this.reoaldArticles(filteredArticles);
  }

  reoaldArticles(params) {
    if (this.articleItemSource) {
      this.articleItemSource.query(params).then(items => this.articleViewList = items);
    }
  }

  articleSelected(item) {
    this.adminService.IsArticlePunchcard(item.Id).pipe(takeUntil(this.destroy$)).subscribe(
      res => {
        if (res.Data === 1) {
          this.articleIsPunchcard = true;
        } else {
          this.articleIsPunchcard = false;
        }
        // if (item) {
        //   this.validateArticleSelection(item)
        // }
        // this.closeArticleView();
      }
    )
    if (item) {
      this.validateArticleSelection(item)
    }
    this.closeArticleView();
  }

  validateArticleSelection(article) {
    let erorMsg = '';
    let tranMsg = '';
    if (article) {
      const memberIdLst: any = [];
      // let logbranchAccountNo: number;
      this.memberList.forEach(mem => {
        memberIdLst.push(mem.Id);
      });
      if (article.IsPunchCard && this.resList) {
        const punchCardValid = {
          ActivityId: this.activeTimeDetail.ActivityId,
          MemberIdLst: memberIdLst,
          StartDate: this.activeTimeDetail.StartDateTime,
          AccountNo: this.logbranchAccountNo,
          BranchId: this.branchId
        }
        this.adminService.ValidatePunchcardContractMember(punchCardValid).pipe(takeUntil(this.destroy$)).subscribe(
          res => {
            if (res.Data) {
              let memberValidPunch: any = [];
              let memberValidAccount: any = [];
              const listDiff: any = [];
              const listDiffAccountNo: any = [];
              //  for (const key in res.Data) {
              //  if (res.Data.hasOwnProperty(key)) {
              memberValidPunch = res.Data['validMem'];
              memberValidAccount = res.Data['validMemAcc'];
              memberIdLst.forEach(memId => {
                const index: number = memberValidPunch.find(item => item === memId);
                if (index === undefined || index === -1) {
                  listDiff.push(memId);
                }
              });

              memberIdLst.forEach(memId => {
                const index: number = memberValidAccount.find(item => item === memId);
                if (index === undefined || index === -1) {
                  listDiffAccountNo.push(memId);
                }
              });
              // }
              //  }
              if (listDiff && listDiff.length > 0) {  // validate member same branch

                listDiff.forEach(i => {
                  const item = this.memberList.find(x => x.Id = i);
                  if (item) {
                    erorMsg = item.Name;
                  }
                });
                this.translateService.get('ADMIN.PunchCardArticleMemberError').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => tranMsg = tranlstedValue);
                erorMsg = erorMsg + '\n' + tranMsg;
                this.exceMessageService.openMessageBox('WARNING', { messageTitle: 'Exceline', messageBody: erorMsg });
              } else if (listDiffAccountNo && listDiffAccountNo.length > 0) {
                listDiffAccountNo.forEach(i => {
                  const item = this.memberList.find(x => x.Id = i);
                  if (item) {
                    erorMsg = item.Name;
                  }
                });
                this.translateService.get('ADMIN.PunchcardArticleErrorWithAccount').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => tranMsg = tranlstedValue);
                erorMsg = erorMsg + '\n' + tranMsg;
                this.exceMessageService.openMessageBox('WARNING', { messageTitle: 'Exceline', messageBody: erorMsg });
              } else {
                this.setArticle(article);
              }
            }
          }
        );
      } else if (article.IsContractBooking && this.memberList) {
        const contractBookingValid = {
          ActivityId: this.activeTimeDetail.ActivityId,
          MemberIdLst: memberIdLst,
          startDate: this.activeTimeDetail.StartDateTime,
          AccountNo: this.logbranchAccountNo,
          BranchId: this.branchId
        }
        this.adminService.CheckUnusedCancelBooking(contractBookingValid).pipe(takeUntil(this.destroy$)).subscribe(
          res => {
            if (res.Data) {
              let memberValidPunch: any = [];
              let memberValidAccount: any = [];
              const listDiff: any = [];
              const listDiffAccountNo: any = [];
              //  for (const key in res.Data) {
              //    if (res.Data.hasOwnProperty(key)) {
              memberValidPunch = res.Data['validMem'];
              memberValidAccount = res.Data['validMemAcc'];
              memberIdLst.forEach(memId => {
                const index: number = memberValidPunch.find(item => item === memId);
                if (index === -1) {
                  listDiff.push(memId);
                }
              });

              memberIdLst.forEach(memId => {
                const index: number = memberValidAccount.find(item => item === memId);
                if (index === -1) {
                  listDiffAccountNo.push(memId);
                }
              });
              //    }
              //  }
              if (listDiff && listDiff.length > 0) {  // validate member same branch
                listDiff.forEach(i => {
                  const item = this.memberList.find(x => x.Id = i);
                  if (item) {
                    erorMsg = item.Name;
                  }
                });
                this.translateService.get('ADMIN.MemberHasNotUnusedCancelBooking').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => tranMsg = tranlstedValue);
                erorMsg = erorMsg + '\n' + tranMsg;
              } else if (listDiffAccountNo && listDiffAccountNo.length > 0) {
                listDiffAccountNo.forEach(i => {
                  const item = this.memberList.find(x => x.Id = i);
                  if (item) {
                    erorMsg = item.Name;
                  }
                });
                this.translateService.get('ADMIN.MemberHasNotUnusedCancelBooking').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => tranMsg = tranlstedValue);
                erorMsg = erorMsg + '\n' + tranMsg;
              } else {
                this.setArticle(article);
              }
            }
          });
      } else {
        this.setArticle(article);
      }
    }
  }

  setArticle(article) {
    this.activeTimeDetail.ArticleId = article.Id;
    this.activeTimeDetail.ArticleName = article.Description;
    this.activeTimeDetail.IsContractBooking = article.IsContractBooking;
    this.activeTimeDetail.IsPunchCard = article.IsPunchCard;
    this.activeTimeDetail.ArticleNoOfMinutes = article.NoOfMinutes;
    if (Number(article.NoOfMinutes) > 0) {
      // let hours: number;
      // hours = Math.floor(article.NoOfMinutes / 60);
      const min = article.NoOfMinutes
      const activityDateTime = moment(this.activeTimeDetail.StartDateTime).add(min, 'minutes').format('YYYY-MM-DD HH:mm:ss'); // .add(hours, 'hours')
      // .format('YYYY-MM-DD HH:mm:ss'); // ViewModel.BookingItem.StartDateTime.AddHours(hours).AddMinutes(min);
      if (moment(activityDateTime).format('YYYY-MM-DD HH:mm:ss') < moment(this.activeTimeDetail.EndDateTime).format('YYYY-MM-DD HH:mm:ss')) {
        this.activeTimeDetail.EndDateTime = activityDateTime;
        this.bookingForm.patchValue({
          EndTime: moment(this.activeTimeDetail.EndDateTime).format('HH:mm')
        });
      }
    }
    this.bookingForm.patchValue({
      ArticleId: article.Id,
      ArticleName: article.Description
    });
    this.articleModel.close();
  }

  repetableBookingToggle(event: any) {
    this.isRepeatable = event.target.checked;
  }


  freqencyToggle(val: any) {
    this.frequency = val;
    this.repeatableForm.patchValue({
      'frequency': val,
      'recurEvery': [null],
      'daysFrequencyOptions': [null],
      'monthFrequencyOptions': [null],
      'dayOfMonth': [null],
      'ordinalFreq': 1,
      'daySelected': 1,
      'monthSelected': 0
    })
    this.dayOptions = -1;
    this.monthOptions = -1;
    this.yearOptions = -1;
    if (val === 'm' || val === 'y') {
      this.repeatableForm.patchValue({
        'weeksdays': [['Sunday', false], ['Monday', false], ['Tuesday', false], ['Wednesday', false], ['Thursday', false], ['Friday', false], ['Saturday', false]]

      })
      this.weekDays = [['Sunday', false], ['Monday', false], ['Tuesday', false], ['Wednesday', false], ['Thursday', false], ['Friday', false], ['Saturday', false]];
    } else {
      this.weekDays = this.repeatableForm.get('weeksdays').value;
    }
  }

  weekDayToggle(event) {
    this.weekDays.forEach(element => {
      if (element[0] === event.target.name) {
        element[1] = event.target.checked;
      }
    });
    this.repeatableForm.patchValue({
      'weeksdays': this.weekDays
    });
  }

  dayOptionsToggle(event) {
    this.repeatableForm.patchValue({
      'daysFrequencyOptions': event.target.value
    })
    this.dayOptions = event.target.value
  }

  monthOptionsToggle(event) {
    this.repeatableForm.patchValue({
      'monthFrequencyOptions': event.target.value
    })
    this.monthOptions = event.target.value;
  }

  recurEveryToggle(event) {
    this.repeatableForm.patchValue({
      'recurEvery': event.target.value
    })
  }

  dayOfMonthToggle(event) {
    this.repeatableForm.patchValue({
      'dayOfMonth': event.target.value
    })
  }

  ordinalFrequencyToggle(event) {
    this.repeatableForm.patchValue({
      'ordinalFreq': event.target.value
    })
  }

  daySelectedToggle(event) {
    this.repeatableForm.patchValue({
      'daySelected': event.target.value
    });
  }

  monthSelectToggle(event) {
    // value represent month number
    this.repeatableForm.patchValue({
      'monthSelected': event.target.value
    })
  }

  recurrenceOptionsToggle(event) {
    this.repeatableForm.patchValue({
      'recurrenceOption': event.target.value
    })
    this.recurrenceOption = event.target.value
  }

  occurenceToggle(event) {
    this.repeatableForm.patchValue({
      'occurences': event.target.value
    })
  }

  endByToggle(event) {
    const date = event.date.year + '-' + event.date.month + '-' + event.date.day + ' 00:00.00.000';
    this.repeatableForm.patchValue({
      'endDate': date
    })
  }

  yearOptionsToggle(event) {
    this.repeatableForm.patchValue({
      'yearFrequencyOptions': event.target.value
    })
    this.yearOptions = event.target.value;
  }

  getDatesAndTimeOnRepeatable(form, resbooking) {
    const occurences = Number(form.get('occurences').value)

    let dates = [];
    const freq = form.get('frequency').value;

    let time = resbooking.StartDateTime.split(' ');
    const startTime = time[1];
    time = resbooking.EndTime.split(' ');
    const endTime = time[1];

    const startDateSelected = resbooking.StartDateTime.split(' ');

    const date = startDateSelected[0];

    const dateSplit = date.split('-');

    const dateDate =  new Date(dateSplit[0], parseInt(dateSplit[1], 10) - 1, dateSplit[2])

    if (form.get('endDate').value) {
      this.endDateSelect = form.get('endDate').value;
      const split = this.endDateSelect.split(' ');
      const splitDate = split[0];
      const splitList = splitDate.split('-');
      this.endDateSelect = new Date(splitList[0], Number(splitList[1]) - 1, splitList[2]);
    }
    const today = new Date(); // restriction to pluss 20 years when no end date is selected
    const endDate  = new Date(today.getFullYear() + 20, 11, 31);

    const rr = form.get('recurEvery').value // recur rate

    const daysSelected = form.get('weeksdays').value

    switch (freq) {
      case 'd':
      // begin

      const currentDate = new Date(dateDate);

      if (form.get('daysFrequencyOptions').value === '0') {

        dates.push(dateDate); // add selected date  to datelist, NB! scheduleItemId known for this date

        if (form.get('recurrenceOption').value === '0') {
          dates = this.getDatesOnDaysInterval(currentDate, dates, rr, endDate, Infinity)
        } else if (form.get('recurrenceOption').value === '1') {
          dates = this.getDatesOnDaysInterval(currentDate, dates, rr, endDate, occurences)
        } else if (form.get('recurrenceOption').value === '2') {
          dates = this.getDatesOnDaysInterval(currentDate, dates, rr, this.endDateSelect, Infinity)
        }

      } else if (form.get('daysFrequencyOptions').value === '1') {
        if (form.get('recurrenceOption').value === '0') {
          dates = this.getDaysOnDayCheckboxes(currentDate, dates, rr, Infinity, freq, endDate, form.get('weeksdays').value);
        } else if (form.get('recurrenceOption').value === '1') {
          dates = this.getDaysOnDayCheckboxes(currentDate, dates, rr, occurences, freq, endDate, form.get('weeksdays').value);
        } else if (form.get('recurrenceOption').value === '2') {
          dates = this.getDaysOnDayCheckboxes(currentDate, dates, rr, Infinity, freq, this.endDateSelect, form.get('weeksdays').value);
        }

      }
      // end
      break

      case 'w':
      // begin
      const currentDateWeek = new Date(dateDate);
      if (form.get('recurrenceOption').value === '0') {
        dates = this.getDaysOnDayCheckboxes(currentDateWeek, dates, rr, Infinity, freq, endDate, form.get('weeksdays').value);
      } else if (form.get('recurrenceOption').value === '1') {
        dates = this.getDaysOnDayCheckboxes(currentDateWeek, dates, rr, occurences, freq, endDate, form.get('weeksdays').value);
      } else if (form.get('recurrenceOption').value === '2') {
        dates = this.getDaysOnDayCheckboxes(currentDateWeek, dates, rr, Infinity, freq, this.endDateSelect, form.get('weeksdays').value);
      }
      // end
      break

      case 'm':
      // begin

      const currentDate2 = new Date(dateDate);
      // const day = form.get('daySelected').value;
      const ordinal = form.get('ordinalFreq').value;
      const rr2 = form.get('recurEvery').value;

      if (form.get('monthFrequencyOptions').value === '0') {
        if (form.get('recurrenceOption').value === '0') {
          dates = this.getDatesOnMonthlyInterVall(currentDate2, dates, form.get('dayOfMonth').value, Infinity, endDate, form.get('recurEvery').value);
        } else if (form.get('recurrenceOption').value === '1') {
          dates = this.getDatesOnMonthlyInterVall(currentDate2, dates, form.get('dayOfMonth').value, occurences, endDate, form.get('recurEvery').value);
        } else if (form.get('recurrenceOption').value === '2') {
          dates = this.getDatesOnMonthlyInterVall(currentDate2, dates, form.get('dayOfMonth').value, Infinity, this.endDateSelect, form.get('recurEvery').value);
        }
      } else if (form.get('monthFrequencyOptions').value === '1') {
        if (form.get('recurrenceOption').value === '0') {
          dates = this.getDayOfMonth(currentDate2, rr2, endDate, Infinity, form.get('daySelected').value, ordinal)
        } else if (form.get('recurrenceOption').value === '1') {
          dates = this.getDayOfMonth(currentDate2, rr2, endDate, form.get('occurences').value, form.get('daySelected').value, ordinal)
        } else if (form.get('recurrenceOption').value === '2') {
          dates = this.getDayOfMonth(currentDate2, rr2, this.endDateSelect, Infinity, form.get('daySelected').value, ordinal)
        }
      }
      // end
      break

      case 'y':
      // begin
      const month = form.get('monthSelected').value;
      const dom = form.get('dayOfMonth').value;
      const day = form.get('daySelected').value;
      const ordinal2 = form.get('ordinalFreq').value;
      const currentDateY = new Date(dateDate);

      if (form.get('yearFrequencyOptions').value === '0') {
        if (form.get('recurrenceOption').value === '0') {
          dates = this.getDatesOnYearlyFreq(0, rr, dom, month, null, null, currentDateY, endDate, Infinity);
        } else if (form.get('recurrenceOption').value === '1') {
          dates = this.getDatesOnYearlyFreq(0, rr, dom, month, null, null, currentDateY, endDate, form.get('occurences').value);
        } else if (form.get('recurrenceOption').value === '2') {
          dates = this.getDatesOnYearlyFreq(0, rr, dom, month, null, null, currentDateY, this.endDateSelect, Infinity);
        }
      } else if (form.get('yearFrequencyOptions').value === '1') {
        if (form.get('recurrenceOption').value === '0') {
          dates = this.getDatesOnYearlyFreq(1, rr, null, month, day, ordinal2, currentDateY, endDate, Infinity);
        } else if (form.get('recurrenceOption').value === '1') {
          dates = this.getDatesOnYearlyFreq(1, rr, null, month, day, ordinal2, currentDateY, endDate, form.get('occurences').value);
        } else if (form.get('recurrenceOption').value === '2') {
          dates = this.getDatesOnYearlyFreq(1, rr, null, month, day, ordinal2, currentDateY, this.endDateSelect, Infinity);
        }
      }
      // end
    }
    return dates;
  }

  findNextDayOfWeek(i, weekDays, freq, intervall) {
    let k = -1;
    for (let j = i; j < 7; j++) {
      k++
      const day = weekDays[j];
      if (day[1] === true) {
        return k;
      }
      if (j === 6) {
        if (freq === 'w') {
          k = k +  (7 * (intervall - 1));
        }
        j = -1
      }
    }
    return k;
  }

  getDaysOnDayCheckboxes(datePicked, dates, recurRate, maxlen, freq, endDate, weekDays) {
    const currentDate = new Date(datePicked);
    const dow = currentDate.getDay();
    if (freq === 'w') {
      let i = dow;
      while (currentDate.getTime() <= endDate.getTime() && dates.length < maxlen) {
        i = this.findNextDayOfWeek(i, weekDays, 'w', recurRate)
        const n = i + Number(currentDate.getDate())
        currentDate.setDate(n);
        const nDate = new Date(currentDate);
        if (nDate.getTime() <= endDate.getTime()) {
          dates.push(nDate);
        }
        i = currentDate.getDay() + 1;
        currentDate.setDate(Number(currentDate.getDate()) + 1)
      }
    } else { // freq = d
      let i = dow;
      while (currentDate.getTime() <= endDate.getTime() && dates.length < maxlen) {
        i = this.findNextDayOfWeek(i, weekDays, 'd', null)
        const num = i + Number(currentDate.getDate())
        currentDate.setDate(num);
        const newDate = new Date(currentDate);
        if (newDate.getTime() <= endDate.getTime()) {
          dates.push(newDate);
        }
        i = currentDate.getDay() + 1;
        if (i === 7) {
          i = 0;
        }
        currentDate.setDate(Number(currentDate.getDate()) + 1)
      }
    }
    return dates;
  }

  getDatesOnDaysInterval(currentDate, dates, recurRate, endDate, maxLen) {
    while (currentDate.getTime() < endDate.getTime() && dates.length < maxLen) {
      const num = Number(currentDate.getDate()) + Number(recurRate)
      currentDate.setDate(num);
      const date = new Date(currentDate);
      if (date.getTime() > endDate.getTime()) {
        return dates
      } else {
        dates.push(date);
      }
    }
    return dates;
  }

  getDayOfMonth(pickedDate, recurRate, endDate, maxLen, day, ordinalNum) {
    const dates = [];
    let currentDate = new Date(pickedDate);
    while (currentDate.getTime() < endDate.getTime() && dates.length < maxLen) {
      const tempDate = new Date(currentDate);
      const dim = this.getDaysInMonth(tempDate.getMonth(), tempDate.getFullYear()); // days in month
      let i = 0;
      let dayOccurence = 0;

      for (i = 1; i < dim + 1; i++) {
        tempDate.setDate(i);
        const dow = tempDate.getDay(); // day of week
        if (dow === Number(day)) {
          dayOccurence = dayOccurence + 1;
          if (dayOccurence === Number(ordinalNum)) {
            break;
          }
        }
      }

      // currentDate = tempDate;
      if (tempDate.getTime() >= pickedDate.getTime()) {
        dates.push(tempDate);
        currentDate = new Date(Number(tempDate.getFullYear()), Number(tempDate.getMonth()) + Number(recurRate), 1);
      } else {
        currentDate = new Date(Number(tempDate.getFullYear()), Number(tempDate.getMonth()) + 1, 1);
      }
    }
    return dates
  }

  getDatesOnYearlyFreq(option, recurRate, dayOfMonth, month, day, ordinal, pickedDate, endDate, maxLen) {
    let dates = [] ;

    if (option === 0) {
      dates = this.yearlyOnDate(pickedDate, endDate, maxLen, dayOfMonth, recurRate, month);
    } else if (option === 1) {
      dates = this.findDay(month, day, ordinal, pickedDate, endDate, maxLen, recurRate);
    }
    return dates;
  }

  yearlyOnDate(pickedDate, endDate, maxLen, dom, recurRate, month) {
    const dates = [];
    let currentDate = new Date(pickedDate)
    if (Number(month) < currentDate.getMonth() && Number(dom) < currentDate.getDate()) {
      currentDate = new Date(Number(currentDate.getFullYear()) + 1, month, dom);
    } else {
      currentDate = new Date(Number(currentDate.getFullYear()), month, dom);
    }
    while (currentDate.getTime() < endDate.getTime() && dates.length < maxLen) {
      dates.push(currentDate);
      currentDate = new Date(Number(currentDate.getFullYear()) + Number(recurRate), month, dom);
    }
    return dates;
  }

  findDay(month, day, ordinalNum, pickedDate, endDate, maxLen, recurRate) {
    const dates = [];
    let i = 0;
    let dayOccurence = 0;
    let tempDate = new Date(Number(pickedDate.getFullYear()), month, 1);

    if (tempDate.getTime() < pickedDate.getTime()) {
      tempDate = new Date(Number(tempDate.getFullYear()) + 1, month, 1)
    }
    let k = 0;
    while (tempDate.getTime() < endDate.getTime() && dates.length < maxLen) {
      k++;
      const daysInMonth = this.getDaysInMonth(tempDate.getMonth(), tempDate.getFullYear());
      for (i = 1; i < daysInMonth; i++) {
        tempDate.setDate(i);
        const dow = tempDate.getDay(); // day of week
        if (dow === Number(day)) {
          dayOccurence = dayOccurence + 1;
          if (dayOccurence === Number(ordinalNum)) {
            dates.push(tempDate)
            break;
          }
        }
      }
      tempDate = new Date(Number(tempDate.getFullYear()) + Number(recurRate), month, 1);
      dayOccurence = 0;
    }
    return dates;
  }

  getDatesOnMonthlyInterVall(selecetedDate, dates, dayOfMonth, maxLen, endDate, recurRate) {
    let currentDate = new Date(selecetedDate);
    const daysOfMonth =  this.getDaysInMonth(currentDate.getMonth(), currentDate.getFullYear());
    if (currentDate.getDate() > dayOfMonth) {
      currentDate = new Date(currentDate.getFullYear(), currentDate.getMonth() + 1, dayOfMonth);
    } else {
      if (daysOfMonth < dayOfMonth) {
        currentDate = new Date(currentDate.getFullYear(), currentDate.getMonth() + 1, 1);
      } else {
        currentDate = new Date (currentDate.getFullYear(), currentDate.getMonth(), dayOfMonth)
      }
    }
    while ( currentDate.getTime() < endDate.getTime() && dates.length < maxLen) {
      const dom = this.getDaysInMonth(Number(currentDate.getMonth()), Number( currentDate.getFullYear()));
      let year =  currentDate.getFullYear();
      let month = Number( currentDate.getMonth());
      if (dom < dayOfMonth) {
        year = currentDate.getFullYear();
        month = Number( currentDate.getMonth()) + 1;
        currentDate = new Date(year, month, 1);
      } else {
        currentDate.setDate(dayOfMonth)
        dates.push(currentDate);
        year =  currentDate.getFullYear();
        month = Number( currentDate.getMonth()) + Number(recurRate);
        currentDate = new Date(year, month, dayOfMonth)
      }
    }
    return dates;
  }

  getDaysInMonth(month, year) {
   return new Date(year, month + 1, 0).getDate();
  };
}
