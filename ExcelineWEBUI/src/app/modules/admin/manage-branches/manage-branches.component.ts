import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { AdminService } from '../services/admin.service'
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ExceError } from '../../../shared/directives/exce-error/exce-error';
import { DataTableResource } from '../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from '../../../shared/components/us-data-table/tools/data-table-filter-pipe'
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { UsbModal } from '../../../shared/components/us-modal/us-modal';
import { BankAccountValidator } from '../../../shared/directives/exce-error/util/validators/bank-account-validator';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { IMyDpOptions } from '../../../shared/components/us-date-picker/interfaces';
import { Directive, Input, HostListener } from '@angular/core';
import { ExceService } from '../../../shared/directives/exce-error/exce-error'
import { ExceLoginService } from '../../../modules/login/exce-login/exce-login.service';
import { ExceBreadcrumbService } from '../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

const now = new Date();

@Component({
  selector: 'app-manage-branches',
  templateUrl: './manage-branches.component.html',
  styleUrls: ['./manage-branches.component.scss'],
})

export class ManageBranchesComponent implements OnInit, OnDestroy {
  private branches: any;
  private itemResource: any;
  private closeResult: string;
  private isAddNew: boolean = false;
  private modalReference?: any;
  private branchesForm: any;
  private filteredBranches: any;
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  private branchId: number;
  private nameValue: string;
  private typeValue: string;
  private countryList: any;
  private currentCountryId: string;
  private regions: any;
  private addNew: any;
  private savedId: any;
  private editBranch: any;
  private editBranchClone: any;
  private today: NgbDateStruct;
  private endMinDate: NgbDateStruct;
  private destroy$ = new Subject<void>();
  items = [];
  itemCount = 0;
  branchGroups: any;

  @ViewChild(ExceError)
  private localExceError: ExceError

  private fromDatePickerOptions: IMyDpOptions = {
    disableSince: { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() },
  };

  private currentDate: string;
  constructor(private adminService: AdminService,
    private modalService: UsbModal,
    private fb: FormBuilder,
    private exceService: ExceService,
    private exceLoginService: ExceLoginService,
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.pipe(
      takeUntil(this.destroy$)
    ).subscribe((branch) => {
        this.branchId = branch.BranchId;
      }, null, null
    );

    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ADMIN.DepartmentsC',
      url: '/adminstrator/manage-branches'
    });

    this.today = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
    this.endMinDate = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
    this.branchesForm = this.fb.group({
      'BranchName': [null, Validators.compose([Validators.pattern(".*\\S.*"), Validators.required])],
      'IsExpressGym': [null],
      'RegisteredDate': [{ date: this.today }],
      'Addr1': [''],
      'Addr2': [''],
      'Addr3': [''],
      'ZipCode': ['', [Validators.pattern("^[0-9]*$")]],
      'ZipName': [null],
      'CountryId': [null, [Validators.required]],
      'CreditorCollectionId': [null, [Validators.required]],
      'BankAccountNo': ['', [BankAccountValidator.BankAccount]],
      'TelWork': [null, [Validators.pattern("^[0-9]*$")]],
      'Fax': ['', [Validators.pattern("^[0-9]*$")]],
      'Email': [null, [Validators.pattern("^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$")]],
      'RegisteredNo': [''],
      'RegionId': [null]
    });
  }

  ngOnInit() {
    this.getBranchForTable()
    this.adminService.getBranchGroups().pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.branchGroups = result.Data;
      } else {
      }
    }, null, null);

    this.adminService.getCountryDetails().pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.countryList = result.Data;
        this.currentCountryId = this.countryList[0].Id
      }
    }, null, null);
  }

  ngOnDestroy() {
    if (this.modalReference) {
      this.modalReference.close();
    }
    this.destroy$.next();
    this.destroy$.complete();
  }

  getBranchForTable() {
    this.adminService.getBranches(-1).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.branches = result.Data;
        this.itemResource = new DataTableResource(result.Data);
        this.itemResource.count().then(count => this.itemCount = count);
        this.items = this.branches;
        this.itemCount = Number(result.Data.length);
      } else {
      }
    }, null, null);
  }

  loadRegions(countryId: string) {
    this.regions = null;
    this.branchesForm.controls['RegionId'].setValue('');
    this.adminService.getRegions(countryId, -1).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.regions = result.Data;
      } else {
      }
    }, null, null);
  }

  openBranchesModel(content: any, isAddNew: boolean) {
    this.isAddNew = isAddNew;
    this.modalReference = this.modalService.open(content, { width: '1000' });
  }

  reloadItems(params): void {
    if (this.itemResource) {
      this.itemResource.query(params).then(items => this.items = items);
    }
  }

  rowDoubleClick(rowEvent) {
    this.editBranch = rowEvent.row.item;
    this.branchesForm.patchValue(this.editBranch);
    const regDate = new Date(this.editBranch.RegisteredDate)
    this.branchesForm.patchValue({
      RegisteredDate: {
        date: {
          year: regDate.getFullYear(),
          month: regDate.getMonth() + 1,
          day: regDate.getDate()
        }
      }
    });
  }

  filterInput(nameValue?: any, typeValue?: any) {
    if (typeValue === "ALL") {
      this.filteredBranches = this.filterpipe.transform("BranchName", "NAMEFILTER", this.branches, nameValue);
    } else {
      this.filteredBranches = this.filterpipe.transform("BranchName", "NAMEFILTER", this.branches, nameValue);
      this.filteredBranches = this.filterpipe.transform("GroupId", "SELECT", this.filteredBranches, typeValue);
    }
    this.itemResource = new DataTableResource(this.filteredBranches);
    this.itemResource.count().then(count => this.itemCount = count);
    this.reloadItems(this.filteredBranches);
    this.nameValue = nameValue;
    this.typeValue = typeValue;
  }

  saveBranchDetails(value) {
    value.branchId = this.branchId;
    value.IsEnabled = true;
    let month: number = now.getMonth() + 1
    value.RegisteredDate = now.getFullYear() + "-" + month + "-" + now.getDate()
    this.addNew = value;
    this.adminService.saveBranchDetails(value).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result.Status == 'OK') {
        this.savedId = result.Data;
        this.closeForm();
        this.branches.push({
            "Id": this.savedId,
            "EntNo": this.savedId,
            "BranchName": this.savedId + "_" + this.addNew.BranchName,
            "GroupId": 1,
            "GroupName": "Default",
            "RegisteredDate": now.getFullYear() + "-" + month + "-" + now.getDate(),
            "Addr1": this.addNew.Addr1,
            "Addr2": this.addNew.Addr2,
            "Addr3": this.addNew.Addr3,
            "ZipCode": this.addNew.ZipCode,
            "ZipName": this.addNew.ZipName,
            "CreditorCollectionId": this.addNew.CreditorCollectionId,
            "CountryId": this.addNew.CountryId,
            "Region": this.addNew.Region,
            "TelWork": this.addNew.TelWork,
            "Email": this.addNew.Email,
            "Fax": this.addNew.Fax,
            "BankAccountNo": this.addNew.BankAccountNo,
            "KidSwapAccountNo": "",
            "CollectionAccountNo": "",
            "RegisteredNo": this.addNew.RegisteredNo,
            "IsParentGroup": false,
            "IsCheck": false,
            "IsEnabled": true,
            "IsExpressGym": this.addNew.IsExpressGym,
            "RegionId": this.addNew.RegionId
          });
        this.getBranchForTable();
      }
    }, null, null);
  }

  editBranches(value) {
    this.editBranchClone = this.editBranch;
    this.editBranch.BranchName = value.BranchName;
    this.editBranch.IsExpressGym = value.IsExpressGym;
    this.editBranch.Addr1 = value.Addr1;
    this.editBranch.Addr2 = value.Addr2;
    this.editBranch.Addr3 = value.Addr3;
    this.editBranch.ZipCode = value.ZipCode;
    this.editBranch.ZipName = value.ZipName;
    this.editBranch.CountryId = value.CountryId;
    this.editBranch.CreditorCollectionId = value.CreditorCollectionId;
    this.editBranch.BankAccountNo = value.BankAccountNo;
    this.editBranch.Fax = value.Fax;
    this.editBranch.TelWork = value.TelWork;
    this.editBranch.Email = value.Email;
    this.editBranch.RegisteredNo = value.RegisteredNo;
    this.editBranch.RegionId = value.RegionId;
    this.adminService.updateBranchDetails(this.editBranch).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      this.branches = this.branches.filter(item => item !== this.editBranchClone);
      this.filteredBranches = this.branches.filter(item => item !== this.editBranch);
      this.branches.push(this.editBranch);
      this.filteredBranches = this.branches;
      this.closeForm();
      this.filterResults();
    }, null, null);
  }

  filterResults() {
    this.branches = this.sortByKey(this.branches, "BranchName");
    if (this.typeValue === "ALL" || this.typeValue == null) {
      if (this.nameValue != "") {
        this.filteredBranches = this.filterpipe.transform("BranchName", "NAMEFILTER", this.branches, this.nameValue);
      }
    } else {
      if (this.nameValue != "") {
        this.filteredBranches = this.filterpipe.transform("BranchName", "NAMEFILTER", this.branches, this.nameValue);
        this.filteredBranches = this.filterpipe.transform("GroupId", "SELECT", this.filteredBranches, this.typeValue);
      } else {
        this.filteredBranches = this.filterpipe.transform("GroupId", "SELECT", this.branches, this.typeValue);
      }
    }
    if ((this.typeValue === "ALL" || this.typeValue == null) && (this.nameValue == "")) {
      this.itemResource = new DataTableResource(this.branches);
      this.itemResource.count().then(count => this.itemCount = count);
      this.reloadItems(this.branches);
    } else {
      this.filteredBranches = this.sortByKey(this.filteredBranches, "Name");
      this.itemResource = new DataTableResource(this.filteredBranches);
      this.itemResource.count().then(count => this.itemCount = count);
      this.reloadItems(this.filteredBranches);
    }
  }

  closeForm() {
    this.branchesForm.reset();
    this.modalReference.close();
  }

  sortByKey(array: any, key: any) {
    return array.sort(function (a, b) {
      var x = a[key]; var y = b[key];
      return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
  }

  onStartDateChange(date) {

  }

  creditCollector(control: FormControl): { [s: string]: boolean } {
    try {
      if (control.value != null) {
        const creditCollectorId: string = control.value;
        this.adminService.ValidateCreditorNo(creditCollectorId).pipe(
          takeUntil(this.destroy$)
        ).subscribe(result => {
          if (result) {
            if (result['Data']) {
              return null;
            } else {
              this.branchesForm.controls['CreditorCollectionId'].setErrors({
                "creditCollector": false
              });
              return { creditCollector: false };
            }
          } else {
            this.branchesForm.controls['CreditorCollectionId'].setErrors({
              "creditCollector": false
            });
            return { creditCollector: false };
          }
        }, null, null);
      }
    } catch (e) {
      this.branchesForm.controls['CreditorCollectionId'].setErrors({
        "creditCollector": false
      });
      return { creditCollector: false };
    }
  }

  onBlurMethod(val: any) {
    if (this.isAddNew) {
      this.validateCreditCollector(val);
    } else {
      if (this.editBranch.CreditorCollectionId != val) {
        this.validateCreditCollector(val);
      }
    }
  }

  validateCreditCollector(val: any) {
    try {
      if (val != null) {
        let creditCollectorId: string = val;
        this.adminService.ValidateCreditorNo(creditCollectorId).pipe(
          takeUntil(this.destroy$)
        ).subscribe(result => {
          if (result) {
            if (result['Data']) {
              return null;
            } else {
              this.branchesForm.controls['CreditorCollectionId'].setErrors({
                "creditCollector": false
              });
              this.exceService.callComponent(this.branchesForm.controls['CreditorCollectionId']);
            }
          } else {
            this.branchesForm.controls['CreditorCollectionId'].setErrors({
              "creditCollector": false
            });
            this.exceService.callComponent(this.branchesForm.controls['CreditorCollectionId']);
          }
        }, null, null);
      }
    } catch (e) {
      this.branchesForm.controls['CreditorCollectionId'].setErrors({
        "creditCollector": false
      });
      this.exceService.callComponent(this.branchesForm.controls['CreditorCollectionId']);
    }
  }
}
