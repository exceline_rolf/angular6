import { Component, OnInit, OnDestroy } from '@angular/core';
import { ExceService } from '../../../../shared/directives/exce-error/exce-error'
import { ExceLoginService } from '../../../../modules/login/exce-login/exce-login.service';
import {GymSettingsService } from '../gym-settings.service'
import { AdminService } from '../../services/admin.service'
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'required-settings',
  templateUrl: './required-settings.component.html',
  styleUrls: ['./required-settings.component.scss']
})
export class RequiredSettingsComponent implements OnInit, OnDestroy {
  private branch: any;
  private requiredSettings: any;
  public requiredSettingsForm: any;
  private type: string
  private isShowInfo = false;
  private message = 'OK';
  private destroy$ = new Subject<void>();

  constructor(
    private adminService: AdminService,
    private translateService: TranslateService,
    private exceMessageService: ExceMessageService,
    private exceLoginService: ExceLoginService,
    private gymListService: GymSettingsService,
    private fb: FormBuilder) {
      this.requiredSettingsForm = this.fb.group({
        'MemberRequired': [null],
        'MobileRequired': [null],
        'AddressRequired': [null],
        'ZipCodeRequired': [null],
      });
  }

  ngOnInit() {
    if (this.gymListService.SelectedGym) {
      this.fetchData(this.gymListService.SelectedGym);
    }
    this.gymListService.selectGym.pipe(
      takeUntil(this.destroy$)
    ).subscribe((value) => {
      this.fetchData(value.some)
    }, null, null);
  }

  fetchData(branch) {
    this.branch = branch;
    this.adminService.getGymSettings('REQUIRED', null, this.branch.Id).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.requiredSettings = result.Data;
        this.requiredSettingsForm.patchValue(this.requiredSettings[0]);
      } else {
        this.translateService.get('MEMBERSHIP.DataFetchNotSuccessful').pipe(
          takeUntil(this.destroy$)
        ).subscribe(translatedValue => this.message = translatedValue);
        this.type = 'DANGER';
        this.isShowInfo = true;
        setTimeout(() => {
          this.isShowInfo = false;
        }, 4000);
      }
    }, null, null);
  }

  saveSettings(value) {
    this.requiredSettings[0].MemberRequired = value.MemberRequired
    this.requiredSettings[0].MobileRequired = value.MobileRequired
    this.requiredSettings[0].AddressRequired = value.AddressRequired
    this.requiredSettings[0].ZipCodeRequired = value.ZipCodeRequired
    this.adminService.SaveGymRequiredSettings(this.requiredSettings[0]).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        if (result.Status === 'OK') {
          this.translateService.get('MEMBERSHIP.SaveSuccessful').pipe(
            takeUntil(this.destroy$)
          ).subscribe(translatedInfo => this.message = translatedInfo);
          this.type = 'SUCCESS';
          this.isShowInfo = true;
          setTimeout(() => {
            this.isShowInfo = false;
          }, 4000);
        }
      } else {
        this.translateService.get('MEMBERSHIP.SaveNotSuccessful').pipe(
          takeUntil(this.destroy$)
        ).subscribe(translatedValue => this.message = translatedValue);
        this.type = 'DANGER';
        this.isShowInfo = true;
        setTimeout(() => {
          this.isShowInfo = false;
        }, 4000);
      }
    }, null, null);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
