import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PosHwProfileComponent } from './pos-hw-profile.component';

describe('PosHwProfileComponent', () => {
  let component: PosHwProfileComponent;
  let fixture: ComponentFixture<PosHwProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PosHwProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PosHwProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
