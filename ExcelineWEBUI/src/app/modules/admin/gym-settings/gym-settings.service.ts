import { Injectable, EventEmitter, ElementRef } from '@angular/core';
import { Subject } from 'rxjs';


@Injectable()
export class GymSettingsService {
  private selectedGym: any;

  get SelectedGym() {
    return this.selectedGym;
  }

  set SelectedGym(selectedGym) {
    this.selectedGym = selectedGym;
  }

  public selectGym: Subject<any> = new Subject();

  public EventEmitter
  constructor() {

  }

  callComponent(value) {
    this.selectGym.next({ some: value })
  }



}
