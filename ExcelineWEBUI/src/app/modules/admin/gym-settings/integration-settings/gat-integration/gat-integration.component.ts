
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ExceService } from '../../../../../shared/directives/exce-error/exce-error'
import { ExceLoginService } from '../../../../../modules/login/exce-login/exce-login.service';
import { GymSettingsService } from '../../gym-settings.service'
import { AdminService } from '../../../services/admin.service'
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { DataTableResource } from '../../../../../shared/components/us-data-table/tools/data-table-resource';
import { UsbModal } from '../../../../../shared/components/us-modal/us-modal';
import { DataTableFilterPipe } from '../../../../../shared/components/us-data-table/tools/data-table-filter-pipe'
import { ExceMessageService } from '../../../../../shared/components/exce-message/exce-message.service';
import { AccessControlTypeService } from '../access-control-type-service';
import { UsErrorService } from 'app/shared/directives/us-error/us-error.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'gat-integration',
  templateUrl: './gat-integration.component.html',
  styleUrls: ['./gat-integration.component.scss']
})
export class GatIntegrationComponent implements OnInit, OnDestroy {
  private branch: any;
  GATItems = [];
  GATForm: any;
  GATCount = 0;
  private GATResource: any;
  activities = []
  private accessProfiles = []
  accessProfileItems = [];
  private accessProfileForm: any;
  accessProfileCount = 0;
  private accessProfileResource: any;
  private modalReference: any;
  terminalTypes = []
  isPaid = false;
  private isAddNew = true
  private articleItems = [];
  private articleCount = 0;
  private articleResource: any;
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  private deletingItem: any
  private model: any;
  private typeString: string;
  private popOver: any;
  formSubmited = false;
  private rowItem: any;
  private destroy$ = new Subject<void>();

  formErrors = {
    'TerminalName': '',
    'Location': '',
    'Port': '',
    'ActivityId': ''
  };

  validationMessages = {
    'TerminalName': {
      'required': 'ADMIN.InvalidTerminalName'
    },
    'Location': {
      'required': 'ADMIN.InvalidIpAddress',
      'pattern': 'ADMIN.InvalidIpAddress'
    },
    'Port': {
      'required': 'ADMIN.InvalidPort',
      'pattern': 'ADMIN.InvalidPort'
    },
    'ActivityId': {
      'required': 'ADMIN.PleaseSpecify'
    },

}

  constructor(private adminService: AdminService,
    private modalService: UsbModal,
    private exceLoginService: ExceLoginService,
    private gymListService: GymSettingsService, private fb: FormBuilder,
    private exceMessageService: ExceMessageService,
    private accessControlTypeService: AccessControlTypeService) {

    this.gymListService.selectGym.pipe(
      takeUntil(this.destroy$)
    ).subscribe((value) => {
      this.fetchData(value.some)
    }, null, null);
    if (this.gymListService.SelectedGym) {
      this.fetchData(this.gymListService.SelectedGym);
    }

    this.exceMessageService.yesCalledHandle.pipe(
      takeUntil(this.destroy$)
    ).subscribe((value) => {
      this.deleteHandler();
    }, null, null);
    this.accessControlTypeService.accessProfileStringLoaded.pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        this.typeString = result
      }, null, null);

  }

  ngOnInit() {
    this.GATForm = this.fb.group({
      'TerminalName': [null, [Validators.required]],
      'TerminalTypeId': [null, [Validators.required]],
      'IsOnline': [null],
      'IsSecondIdentification': [null],
      'IsPaid': [null],
      'TimeOutAfter': [null],
      // tslint:disable-next-line:max-line-length ;pattern not working properly with line breaks
      'Location': [null, [Validators.pattern('^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])$'), Validators.required]],
      'Port': [null, [Validators.pattern('^[0-9]*$'), Validators.required]],
      'ArticleName': [null],
      'ActivityId': [null, [Validators.required]],

    });
    this.GATForm.statusChanges.pipe(
      takeUntil(this.destroy$)
    ).subscribe(_ => {
      UsErrorService.onValueChanged(this.GATForm, this.formErrors, this.validationMessages)
    }, null, null);
  }

  ngOnDestroy() {
    if (this.modalReference) {
      this.modalReference.close();
    }
    this.destroy$.next();
    this.destroy$.complete();
  }

  fetchData(branch) {
    this.branch = branch;
    this.fetchGATTable()
    this.adminService.getActivities(this.branch.Id).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        if (result) {
          this.activities = result.Data
        }
      }, null, null)

    this.adminService.GetTerminalTypes().pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        if (result) {
          this.terminalTypes = result.Data;
        }
      }, null, null);

    this.adminService.GetAccessProfiles().pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        if (result) {
          this.accessProfileItems = result.Data
          this.accessProfileResource = new DataTableResource(this.accessProfileItems);
          this.accessProfileResource.count().then(count => this.accessProfileCount = count);
          this.accessProfileCount = Number(this.accessProfileItems.length);
        }
      }, null, null);
  }

  submitForm(value) {
    this.formSubmited = true;
    if (this.GATForm.valid) {
      this.saveSettings(value)
      this.formSubmited = false;
    } else {
      UsErrorService.validateAllFormFields(this.GATForm, this.formErrors, this.validationMessages);
    }
  }

  saveSettings(value) {
    const AccessProfileList = []
    this.accessProfileItems.forEach(element => {
      if (element.isChecked) {
        AccessProfileList.push(element)
      }
    });
    let TerminalType: any
    this.terminalTypes.forEach(element => {
      if (element.TypeId === value.TerminalTypeId) {
        TerminalType = element
      }
    });
    value.TerminalType = TerminalType
    value.AccessProfileList = AccessProfileList
    value.BranchId = this.branch.Id
    value.AccessContolTypes = this.typeString

    if (this.isAddNew) {
      this.adminService.SaveGymIntegrationExcSettings(value).pipe(
        takeUntil(this.destroy$)
      ).subscribe(
        result => {
          this.fetchGATTable()
          this.isAddNew = true
          this.clearForm()
        }, null, null);
    } else {
      for (const key in value) {
        if (this.rowItem[key]) {
          this.rowItem[key] = value[key]
        }
      }

      this.adminService.SaveGymIntegrationSettings(this.rowItem).pipe(
        takeUntil(this.destroy$)
      ).subscribe(
        result => {
          this.fetchGATTable()
          this.isAddNew = true
          this.clearForm()
        }, null, null);
    }
  }

  fetchGATTable() {
    this.adminService.getGymSettings('INTEGRATION', null, this.branch.Id).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        if (result) {
          this.GATItems = result.Data
          if (this.GATItems.length > 0) {
            this.GATResource = new DataTableResource(this.GATItems);
            this.GATResource.count().then(count => this.GATCount = count);
            this.GATCount = Number(this.GATItems.length);
            this.typeString = this.GATItems[0].AccessContolTypes
            this.accessControlTypeService.SelectedType = this.GATItems[0].AccessContolTypes

            this.GATItems.forEach(element => {
              element.TerminalTypeName = element.TerminalType.TypeName
              element.TerminalTypeId = element.TerminalType.TypeId
            });
          }
        }
      }, null, null);
  }

  gatRowItemLoad(rowItem) {
    this.isAddNew = false
    this.rowItem = rowItem
    if (rowItem.IsPaid) {
      this.isPaid = true
    } else {
      this.isPaid = false
    }
    this.accessProfileItems.forEach(element => {
      element.isChecked = false;
      element.IsDrawPuncheSelect = false
    })
    this.accessProfileItems.forEach(element => {
      this.rowItem.AccessProfileList.forEach(item => {
        if (item.Id === element.Id) {
          element.isChecked = true

          if (item.IsDrawPuncheSelect) {
            element.IsDrawPuncheSelect = true
          }
        }
      })
    });
    this.GATForm.patchValue(this.rowItem)

  }

  viewLoad(rowItem, popOver) {
    if (this.popOver) {
      this.popOver.close();
    }
    this.popOver = popOver;
    this.popOver.open();
    this.accessProfiles = []
    rowItem.AccessProfileList.forEach(element => {
      this.accessProfiles.push(element)
    });
  }

  paidChange(event) {
    if (event.target.checked) {
      this.isPaid = true
    } else {
      this.isPaid = false
    }
  }

  clearForm() {
    this.rowItem = null
    this.isPaid = false
    this.GATForm.reset()
    this.accessProfileItems.forEach(element => {
      element.isChecked = false
      element.IsDrawPuncheSelect = false
    })
  }

  openArticleSelectionModel(content: any) {
    this.adminService.getArticles('ITEM', '', null, -1, true, false, this.branch.Id).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        this.articleItems = result.Data
        this.articleResource = new DataTableResource(this.accessProfileItems);
        this.articleResource.count().then(count => this.articleCount = count);
        this.articleCount = Number(this.articleItems.length);
        this.modalReference = this.modalService.open(content, { width: '1000' });
      }, null, null);
  }

  filterInput(nameValue?: string) {
    let filteredArticleItems = []
    filteredArticleItems = this.filterpipe.transform('Description', 'NAMEFILTER', this.articleItems, nameValue);
    this.articleResource = new DataTableResource(filteredArticleItems);
    this.articleResource.count().then(count => this.articleCount = count);
    this.articleCount = Number(filteredArticleItems.length);
    this.reloadArticleItems(filteredArticleItems);
  }

  reloadArticleItems(params): void {
    if (this.articleResource) {
      this.articleResource.query(params).then(items => this.articleItems = items);
    }
  }

  articleRowDoubleClick(event) {
    if (this.rowItem) {
      this.rowItem.ArticleName = event.row.item.Description
    }
    this.GATForm.controls['ArticleName'].setValue(event.row.item.Description);
    this.closeForm()
  }

  closeForm() {
    this.modalReference.close();
  }


  deleteSetting(deletingItem) {
    this.deletingItem = deletingItem;
    this.openExceMessage('Confirm', 'Do you want to delete this hardware profile')
  }

  openExceMessage(title, body) {
    this.model = this.exceMessageService.openMessageBox('CONFIRM',
      {
        messageTitle: title,
        messageBody: body
      }
    );
  }

  deleteHandler() {
    this.adminService.DeleteIntegrationSettingById(this.deletingItem.Id).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        if (result) {
          this.GATItems = this.GATItems.filter(item => item !== this.deletingItem);
        }
      }, null, null)
  }
  closePopOver() {
    this.popOver.close();
  }

}
