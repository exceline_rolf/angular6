
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ExceService } from '../../../../../shared/directives/exce-error/exce-error'
import { ExceLoginService } from '../../../../../modules/login/exce-login/exce-login.service';
import { GymSettingsService } from '../../gym-settings.service'
import { AdminService } from '../../../services/admin.service'
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { DataTableResource } from '../../../../../shared/components/us-data-table/tools/data-table-resource';
import { AccessControlTypeService } from '../access-control-type-service'
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';


@Component({
  selector: 'exorlive',
  templateUrl: './exorlive.component.html',
  styleUrls: ['./exorlive.component.scss']
})
export class ExorliveComponent implements OnInit, OnDestroy {
  private branch: any;
  private exorLiveObj: any
  ExorLiveForm: any;
  private typeString: string;
  private destroy$ = new Subject<void>();

  constructor(private adminService: AdminService,
    private exceLoginService: ExceLoginService,
    private gymListService: GymSettingsService, private fb: FormBuilder,
    private accessControlTypeService: AccessControlTypeService
  ) {
    if (this.gymListService.SelectedGym) {
      this.fetchData(this.gymListService.SelectedGym);
    }
    this.gymListService.selectGym.pipe(
      takeUntil(this.destroy$)
    ).subscribe((value) => {
      this.fetchData(value.some)
    }, null, null);
    if (this.accessControlTypeService.SelectedType) {
      this.typeString = this.accessControlTypeService.SelectedType;
    }
    this.accessControlTypeService.accessProfileStringLoaded.pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        this.typeString = result
      }, null, null);
  }

  ngOnInit() {
    this.ExorLiveForm = this.fb.group({
      'ApiKey': [null],
    });
  }

  fetchData(branch) {
    this.branch = branch;
    this.adminService.getGymSettings('OTHERINTEGRATION', 'Exorlive', this.branch.Id).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        if (result) {
          this.exorLiveObj = result.Data
          this.ExorLiveForm.patchValue(result.Data)
        }
      }, null, null)
  }

  saveSettings(value) {
    this.exorLiveObj.ApiKey = value.ApiKey
    this.exorLiveObj.SystemName = 'Exorlive'
    this.exorLiveObj.AccessContolTypes = this.typeString;
    this.adminService.AddUpdateOtherIntegrationSettings({
      'BranchId': this.gymListService.SelectedGym.Id,
      'OtherIntegrationSettings': this.exorLiveObj
    }).pipe(
      takeUntil(this.destroy$)
    ).subscribe(null, null, null)
  }

  clearForm() {
    this.ExorLiveForm.reset()
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
