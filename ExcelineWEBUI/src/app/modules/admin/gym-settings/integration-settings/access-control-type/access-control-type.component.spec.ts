import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessControlTypeComponent } from './access-control-type.component';

describe('AccessControlTypeComponent', () => {
  let component: AccessControlTypeComponent;
  let fixture: ComponentFixture<AccessControlTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccessControlTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccessControlTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
