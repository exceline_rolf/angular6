import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageOperationsComponent } from './manage-operations.component';

describe('ManageOperationsComponent', () => {
  let component: ManageOperationsComponent;
  let fixture: ComponentFixture<ManageOperationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageOperationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageOperationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
