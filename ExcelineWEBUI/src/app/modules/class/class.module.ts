import { ClassHomeService } from './class-home/class-home.service';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { Observable, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { ClassRoutingModule } from './class-routing.module';
import { ClassRoutingComponents } from './class-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ClassHomeComponent } from './class-home/class-home.component';
import { ClassListComponent } from './class-home/class-list/class-list.component';
import { ClassCalendarComponent } from './class-home/class-calendar/class-calendar.component';
import { AddNewCalssComponent } from './class-home/add-new-calss/add-new-calss.component';
import { ClassScheduleComponent } from './class-home/class-schedule/class-schedule.component';
import { AgGridModule } from 'ag-grid-angular';


export function translateLoader(http: HttpClient) {

  return new MultiTranslateHttpLoader(http, [
    { prefix: './assets/i18n/', suffix: '.json' },
    { prefix: './assets/i18n/modules/class/', suffix: '.json' }
  ]);
}

export class MultiTranslateHttpLoader implements TranslateLoader {

  constructor(private http: HttpClient,
    public resources: { prefix: string, suffix: string }[] = [
      {
        prefix: '/assets/i18n/',
        suffix: '.json'
      }
    ]) { }

  /**
   * Gets the translations from the server
   * @param lang
   * @returns {any}
   */
  public getTranslation(lang: string): any {

    return forkJoin(this.resources.map(config => {
      return this.http.get(`${config.prefix}${lang}${config.suffix}`);
    })).pipe(
      map(response => {
        return response.reduce((a, b) => {
          return Object.assign(a, b);
        });
      })
    )
  }
}

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ClassRoutingModule,
    AgGridModule.withComponents(null),
    FormsModule,
    ReactiveFormsModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: (translateLoader),
        deps: [HttpClient]
      },
      isolate: false
    })
  ],
  declarations: [
    ClassRoutingComponents,
    ClassHomeComponent,
    ClassListComponent,
    ClassCalendarComponent,
    AddNewCalssComponent,
    ClassScheduleComponent
  ],
  exports: [
    ClassRoutingComponents,
    ClassHomeComponent,
    ClassListComponent,
    AddNewCalssComponent,
    ClassScheduleComponent
  ],
  providers: [

  ]
})
export class ClassModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: ClassModule,
      providers: []
    };
  }
}
