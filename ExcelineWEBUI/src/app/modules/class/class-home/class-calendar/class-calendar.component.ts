import { UsbModalRef } from '../../../../shared/components/us-modal/modal-ref';
import { PreloaderService } from '../../../../shared/services/preloader.service';
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
import { ExceClassService } from '../../services/exce-class.service';
import { ClassHomeService } from '../class-home.service';
import { Component, ViewChild, TemplateRef, Output, EventEmitter, OnInit, Input } from '@angular/core';
import * as $ from 'jquery';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import * as moment from 'moment';
import * as _ from 'lodash';

import { NgbPopover } from '@ng-bootstrap/ng-bootstrap';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { ExceMessageService } from 'app/shared/components/exce-message/exce-message.service';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';




@Component({
  selector: 'class-calendar',
  templateUrl: './class-calendar.component.html',
  styleUrls: ['./class-calendar.component.scss']
})
export class ClassCalendarComponent implements OnInit, OnDestroy {

  selectedStartTime: any;
  deleteEventId: any;
  translatedMsgs: any;
  updateClassActiveTimeHelper: any = {};
  newEndTime: Date;
  resourceIdList: any[] = [];
  instructorIdList: any[] = [];
  selectedInstructors: any;
  newStartTime: Date;
  schedule: any;
  yeshandlerSubs: Subscription;
  nohandlerSubs: Subscription;
  model: any;
  classBookings: any;
  selectedActiveTime: any;
  isNewBooking: any;
  resourceLabelText: string;
  resourceAreaWidth: string;

  addNewClassModel: UsbModalRef;
  dayOfWeek: number;
  fromDate: { date: { year: number; month: number; day: number; }; };
  toDate: { date: { year: number; month: number; day: number; }; };
  startTime: string;
  tooltipY: number;
  tooltipX: number;
  isShowTooltip: boolean;
  eventContextmenuY: any;
  eventontextmenuX: any;
  contextmenuY: any;
  contextmenuX: any;
  scheduler: any;
  calandarDateRange: any;
  isShowTimeDuration: boolean;
  calandarDate: any;
  classActiveTimes: any;
  isShowDayContextMenu: boolean;
  isShowEventContextMenu: boolean;
  isFixed = true;
  private self;

  public dateFilterForm: FormGroup;
  today: { year: number; month: number; day: number; };
  tommorow: any;

  buttonText: { prev: string; next: string; };
  buttonIcons: { prev: string; next: string; };
  theame: boolean;
  events: any[] = [];
  resources: any[] = [];

  @ViewChild('p') public popover: NgbPopover;

  header: any;
  event: any;
  dialogVisible = false;
  idGen = 100;
  viewType: string;
  Currentevent: string;
  views: any;
  scrollTime: string;

  constructor(
    private classHomeService: ClassHomeService,
    private exceClassService: ExceClassService,
    private modalService: UsbModal,
    private exceMessageService: ExceMessageService,
    private fb: FormBuilder,
    private translate: TranslateService

  ) {
    this.self = this;
    this.scrollTime = moment().format('hh:mm:ss');
    this.isNewBooking = true;

    this.yeshandlerSubs = this.exceMessageService.yesCalledHandle.subscribe((value) => {
      if (value.id === 'CalenderChangeConfirmation') {
        this.yesHandler();

      } else if (value.id === 'ClassEntityProcessMsg') {
      } else if (value.id === 'DeleteConfirm') {
        this.exceClassService.DeleteClass({ classId: this.deleteEventId }).subscribe(
          res => {
            this.searchClasses();
          }
        )
      }
    });

    this.nohandlerSubs = this.exceMessageService.noCalledHandle.subscribe((value) => {
      this.noHandler();
    });

    translate.get([
      'CLASS.Instructor',
      'CLASS.Resource',
      'CLASS.ScheduleOverlapWithInstructor',
      'CLASS.ScheduleOverlapWithResource',
      'CLASS.ClassEntityProcessMsg'
    ]).subscribe((res) => {
      this.translatedMsgs = res;
    });

  }

  ngOnInit() {
    const now = new Date();
    this.isShowDayContextMenu = false;
    this.isShowEventContextMenu = false;
    this.isShowTooltip = false;
    this.isShowTimeDuration = false;
    this.calandarDate = new Date();
    this.today = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
    this.dateFilterForm = this.fb.group({
      fromDate: [{ date: this.today }, [Validators.required]],
      toDate: [{ date: this.today }, [Validators.required]]
    });

    this.theame = false;
    this.viewType = 'agendaDayWithoutResources';

    this.header = {
      right: '',
      center: '',
      left: ''
    };

    this.resourceAreaWidth = '20%';
    this.resourceLabelText = 'Classes';

    this.views = {
      agendaDayWithoutResources: {
        type: 'agenda',
        duration: { days: 1 },
        groupByResource: false,
        groupByDateAndResource: false
      }
    };


    this.dateFilterForm.statusChanges.subscribe(__ => {
      this.searchClasses();
    });

    this.createClassResources();

    this.classHomeService.classActiveTimesLoaded.subscribe(
      res => {
        this.classActiveTimes = res;
        this.createClassResources();
      }
    );

  }

  private createClassResources() {
    this.events = [];
    this.resources = [];
    if (this.classHomeService.ClassActiveTimes.length > 0) {
      this.classActiveTimes = this.classHomeService.ClassActiveTimes;
      this.classActiveTimes.forEach((activeTimes, index) => {

        this.resources.push({
          id: activeTimes.ClassTypeId,
          activeTimesId: activeTimes.Id,
          title: activeTimes.ClassType.Name,
          eventColor: activeTimes.Color
        });


        this.events.push({
          id: index,
          resourceId: activeTimes.ClassTypeId,
          activeTimesId: activeTimes.Id,
          start: activeTimes.StartDateTime,
          end: activeTimes.EndDateTime,
          title: activeTimes.ClassType.Name,
          color: activeTimes.Color,
          activeTimeDetail: activeTimes
        });

      });

    }
  }

  loadResourceView(scheduler) {
    scheduler.changeView('timelineDay');
  }

  selectDateRange(val, scheduler) {
    this.scheduler = scheduler;
    switch (val) {
      case 'TODAY':
        this.dateFilterForm.patchValue({
          fromDate: { date: this.today },
          toDate: { date: this.today }
        });
        scheduler.changeView('agendaDayWithoutResources');
        scheduler.today();
        this.generateTitle(scheduler);

        break;

      case 'DAYVIEW':
        this.dateFilterForm.patchValue({
          fromDate: { date: this.today },
          toDate: { date: this.today }
        });
        scheduler.changeView('agendaDayWithoutResources');
        scheduler.today();
        this.generateTitle(scheduler);

        break;
      case 'WEEKVIEW':

        const startOfWeek = moment().startOf('week').toDate();
        const endOfWeek = moment().endOf('week').toDate();

        this.dateFilterForm.patchValue({
          fromDate: { date: { year: startOfWeek.getFullYear(), month: startOfWeek.getMonth() + 1, day: startOfWeek.getDate() } },
          toDate: { date: { year: endOfWeek.getFullYear(), month: endOfWeek.getMonth() + 1, day: endOfWeek.getDate() } }
        });
        scheduler.changeView('agendaWeek', startOfWeek, endOfWeek);
        this.generateTitle(scheduler);

        break;
      case 'MONTHVIEW':
        const startOfMonth = moment().startOf('month').toDate();
        const endOfMonth = moment().endOf('month').toDate();

        this.dateFilterForm.patchValue({
          fromDate: { date: { year: startOfMonth.getFullYear(), month: startOfMonth.getMonth() + 1, day: startOfMonth.getDate() } },
          toDate: { date: { year: endOfMonth.getFullYear(), month: endOfMonth.getMonth() + 1, day: endOfMonth.getDate() } }
        });
        scheduler.changeView('month');
        this.generateTitle(scheduler);

        break;
      default:
        break;
    }

  }

  onDateChanged(event, scheduler) {
    scheduler.changeView('agendaDayWithoutResources');
    scheduler.gotoDate(event.jsdate);
    this.generateTitle(scheduler);

  }

  loadPriviousDay(scheduler) {
    scheduler.prev();
    this.generateTitle(scheduler);
  }

  loadNextDay(scheduler) {
    scheduler.next();
    this.generateTitle(scheduler);
  }


  private generateTitle(scheduler: any) {
    if (scheduler.getView() === 'agendaWeek' || scheduler.getView() === 'month') {
      this.isShowTimeDuration = true;
      this.calandarDateRange = scheduler.getCalendarDateRange();
      const start = { year: this.calandarDateRange.start.getFullYear(), month: this.calandarDateRange.start.getMonth() + 1, day: this.calandarDateRange.start.getDate() };
      const end = { year: this.calandarDateRange.end.getFullYear(), month: this.calandarDateRange.end.getMonth() + 1, day: this.calandarDateRange.end.getDate() };
      this.dateFilterForm.patchValue({
        fromDate: { date: start },
        toDate: { date: end }
      });

    } else {
      this.calandarDate = scheduler.getDate();
      this.isShowTimeDuration = false;
      const date = new Date(scheduler.getDate());
      const newDate = { year: date.getFullYear(), month: date.getMonth() + 1, day: date.getDate() };
      this.dateFilterForm.patchValue({
        fromDate: { date: newDate },
        toDate: { date: newDate }
      });
    }
  }

  private searchClasses() {
    const fromDate = this.dateFilterForm.value.fromDate.date.year + '-' +
      this.dateFilterForm.value.fromDate.date.month + '-' +
      this.dateFilterForm.value.fromDate.date.day;
    const toDate = this.dateFilterForm.value.toDate.date.year + '-' +
      this.dateFilterForm.value.toDate.date.month + '-' +
      this.dateFilterForm.value.toDate.date.day;
    PreloaderService.showPreLoader();
    this.exceClassService.GetEntityActiveTimes({
      StartDate: fromDate,
      EndDate: toDate,
      EntityList: this.classHomeService.ClassIds,
      EntityRoleType: 'CLS'
    }).subscribe(activeTimes => {

      activeTimes.Data.forEach(times => {
        const type = this.classHomeService.ClassTypeList.filter(x => x.Id === times.ClassTypeId);
        times.ClassType = type[0];
        times.Color = type[0].Colour;
        const now1 = new Date();
        const startDateTime = new Date(times.StartDateTime);
        if (startDateTime > now1) {
          times.IsDeleted = true;
        }
      });
      this.classHomeService.ClassActiveTimes = activeTimes.Data;

      PreloaderService.hidePreLoader();
    }, err => {
      PreloaderService.hidePreLoader();
    });
  }

  onSelectResource(event) {
  }

  handleDayClick(event) {
    debugger
    this.isShowDayContextMenu = false;
    this.isShowEventContextMenu = false;
  }

  onRightClick(event) {
    this.selectedStartTime = event.date.format();
    this.contextmenuX = event.jsEvent.pageX;
    this.contextmenuY = event.jsEvent.pageY;
    this.isShowDayContextMenu = true;
    this.isShowEventContextMenu = false;
  }

  openAddNewClassModel(content, isNewBooking) {
    this.isNewBooking = isNewBooking;
    this.isShowDayContextMenu = false;
    this.isShowEventContextMenu = false;
    const today = new Date();
    this.startTime = moment(this.selectedStartTime).format('HH:mm');
    this.toDate = { date: this.today };
    this.fromDate = { date: this.today };
    this.dayOfWeek = today.getDay();
    this.addNewClassModel = this.modalService.open(content, { width: '1000' });
  }

  openViewBookingsModel(content) {
    this.isShowDayContextMenu = false;
    this.isShowEventContextMenu = false;
    this.addNewClassModel = this.modalService.open(content, { width: '700' });
    PreloaderService.showPreLoader();
    this.exceClassService.GetMembersByActiveTimeId({ activeTimeId: this.selectedActiveTime.SheduleItemId }).subscribe(
      res => {
        this.classBookings = res.Data;
        PreloaderService.hidePreLoader();
      }, err => {
        PreloaderService.hidePreLoader();

      }
    )
  }

  hideContextMenu() {
    debugger
    this.isShowDayContextMenu = false;
    this.isShowEventContextMenu = false;

  }

  handleEventClick(e) {
    debugger
    const self = this;
    this.isShowDayContextMenu = false;
    this.isShowEventContextMenu = false;

    this.event = {};
    this.event.title = e.calEvent.title;

    const start = e.calEvent.start;
    const end = e.calEvent.end;
    if (e.view.name === 'month' || e.view.name === 'basicWeek' || e.view.name === 'basicDay') {
      start.stripTime();
    }

    if (end) {
      end.stripTime();
      this.event.end = end.format();
    }

    this.event.id = e.calEvent.id;
    this.event.start = start.format();
    this.dialogVisible = true;
  }


  onEventDragStart(event) {
    debugger
    this.isShowDayContextMenu = false;
    this.isShowEventContextMenu = false;
  }

  eventResizeStop(event) {
    this.newStartTime = new Date(event.event.start.format());
    this.newEndTime = new Date(event.event.end.format());

    this.schedule = event.event.activeTimeDetail;
    this.model = this.exceMessageService.openMessageBox('CONFIRM',
      {
        messageTitle: 'Exceline',
        messageBody: 'CLASS.CalenderChangeConfirmation',
        msgBoxId: 'CalenderChangeConfirmation'
      }
    );

  }

  handleOnDrop(event) {

    this.newStartTime = new Date(event.event.start.format());
    this.newEndTime = new Date(event.event.end.format());

    this.schedule = event.event.activeTimeDetail;
    this.model = this.exceMessageService.openMessageBox('CONFIRM',
      {
        messageTitle: 'Exceline',
        messageBody: 'CLASS.CalenderChangeConfirmation',
        msgBoxId: 'CalenderChangeConfirmation'
      }
    );
  }

  SaveScheduleItem() {
    this.exceClassService.UpdateClassActiveTime(this.updateClassActiveTimeHelper).subscribe(
      res => {
        PreloaderService.hidePreLoader();
        if (res.Data === 0) {
          this.model = this.exceMessageService.openMessageBox('WARNING',
            {
              messageTitle: 'Exceline',
              messageBody: 'CLASS.UpdatedSuccessfully'
            }
          );
          // this.updateClassEvent.emit(res);
        } else if (res.Data === -1) {
          this.model = this.exceMessageService.openMessageBox('WARNING',
            {
              messageTitle: 'Exceline',
              messageBody: 'CLASS.ScheduleOverlapWithInstructor'
            }
          );
          this.createClassResources();

        } else if (res.Data === -2) {
          this.model = this.exceMessageService.openMessageBox('WARNING',
            {
              messageTitle: 'Exceline',
              messageBody: 'CLASS.ScheduleOverlapWithResource'
            }
          );
          this.createClassResources();

        }
      }, err => {
        PreloaderService.hidePreLoader();
        this.createClassResources();

      });
  }

  dragStop(event, scheduler) {
    this.scheduler = scheduler;
  }

  yesHandler() {
    this.schedule.InstructorList.forEach(ins => {
      this.instructorIdList.push(ins.Id);
    });

    this.schedule.ResourceLst.forEach(res => {
      this.resourceIdList.push(res.Id);
    });

    PreloaderService.showPreLoader();

    const sheduleItem = {
      Id: this.schedule.SheduleItemId,
      ActiveTimeID: this.schedule.Id,
      ActiveTimes: {
        StartDate: moment(this.schedule.StartDate).format('YYYY-MM-DD'),
        EndDate: moment(this.schedule.EndDate).format('YYYY-MM-DD'),
        StartDateTime: moment(this.newStartTime).format('YYYY-MM-DD HH:mm'),
        EndDateTime: moment(this.newEndTime).format('YYYY-MM-DD HH:mm')
      },
      InstructorIdList: this.instructorIdList,
      ResourceIdList: this.resourceIdList

    };

    this.updateClassActiveTimeHelper.StartDateTime = moment(this.newStartTime).format('YYYY-MM-DD HH:mm');
    this.updateClassActiveTimeHelper.EndDateTime = moment(this.newEndTime).format('YYYY-MM-DD HH:mm');
    this.updateClassActiveTimeHelper.ActiveTimeId = this.schedule.Id;
    this.updateClassActiveTimeHelper.AllINSIDList = this.instructorIdList;
    this.updateClassActiveTimeHelper.AllRESIDList = this.resourceIdList;
    this.updateClassActiveTimeHelper.MaxNumberOfBookings = this.schedule.MaxNoOfBooking;
    this.updateClassActiveTimeHelper.NoOfParticipants = this.schedule.NumberOfMembers;

    if (this.instructorIdList.length > 0 || this.resourceIdList.length > 0) {

      this.exceClassService.CheckActiveTimeOverlapWithClass(sheduleItem).subscribe(
        res => {
          const sTextArray = res.Data.split(':');

          if (sTextArray.length > 0) {
            if (Number(sTextArray[0]) === 1) {
              this.SaveScheduleItem();
            } else if (Number(sTextArray[0]) === -2) {
              let entityMsg = '';
              if (sTextArray.length > 1) {

                const entityArray1 = sTextArray[1].split(',');

                if (entityArray1[0] === 'INS') {
                  entityMsg = this.translatedMsgs['CLASS.Instructor'] + ':' + entityArray1[1];
                } else if (entityArray1[0] === 'RES') {
                  entityMsg = this.translatedMsgs['CLASS.Resource'] + ':' + entityArray1[1];
                }

                if (sTextArray.length > 2) {
                  const entityArray2 = sTextArray[2].split(',');
                  if (entityArray2[0] === 'RES') {
                    entityMsg = entityMsg + ' ' + this.translatedMsgs['CLASS.Resource'] + ':' + entityArray2[1];
                  }
                }

                const confirmMsg = entityMsg + ' ' + this.translatedMsgs['CLASS.ClassEntityProcessMsg'];
                this.model = this.exceMessageService.openMessageBox('CONFIRM',
                  {
                    messageTitle: 'Exceline',
                    messageBody: confirmMsg,
                    msgBoxId: 'ClassEntityProcessMsg'

                  }
                );

              }
            }
          }
        }, err => {

        });

    } else {
      this.SaveScheduleItem();
    }
  }

  noHandler() {
    this.createClassResources();
  }

  dayRender(date, cell) {

  }

  onEventRightclick(event) {
    debugger
    this.selectedActiveTime = event.date.activeTimeDetail;
    this.eventontextmenuX = event.jsEvent.pageX;
    this.eventContextmenuY = event.jsEvent.pageY;
    this.isShowEventContextMenu = true;
    this.isShowDayContextMenu = false;
  }

  eventRender(this, event, element) {
    const self = this;
    element.find('.fc-content').append(`<span class="badge badge-pill badge-light fc-exe-badge">`
      + event.activeTimeDetail.NumberOfMembers + `/` + event.activeTimeDetail.MaxNoOfBookings +
      `</span><span class="icon-calendar-changed text-light exc-cal-changed-icon" title="Changed calendar"></span>
      <i  class=\'icon-close float-right fc-close\' id=\'` + event.activeTimeDetail.Id + `\'></i>`);

  }

  deleteEvent(eventId): void {
    this.deleteEventId = eventId;
    if (!this.model) {
      this.model = this.exceMessageService.openMessageBox('CONFIRM',
        {
          messageTitle: 'Exceline',
          messageBody: 'CLASS.DeleteConfirm',
          msgBoxId: 'DeleteConfirm'
        }
      );
    }


  }


  mouseOver(event, scheduler) {
    debugger
    const self = this;
    this.scheduler = scheduler;
    let instructerListHtml = '';
    let resourceLstHtml = '';
    if (event.calEvent.activeTimeDetail.InstructorList && event.calEvent.activeTimeDetail.InstructorList.length > 0) {
      instructerListHtml = '<ul class="m-0 p-0 fc-exc-list fc-exc-list-instructor">';
      event.calEvent.activeTimeDetail.InstructorList.forEach(ins => {
        instructerListHtml = instructerListHtml + '<li>' + ins.DisplayName + '</li>';
      });
      instructerListHtml = instructerListHtml + '</ul>';
    }
    if (event.calEvent.activeTimeDetail.ResourceLst && event.calEvent.activeTimeDetail.ResourceLst.length > 0) {
      resourceLstHtml = '<ul class="m-0 p-0 fc-exc-list fc-exc-list-location">';
      event.calEvent.activeTimeDetail.ResourceLst.forEach(ins => {
        resourceLstHtml = resourceLstHtml + '<li>' + ins.DisplayName + '</li>';
      });
      resourceLstHtml = resourceLstHtml + '</ul>';
    }

    $('#' + event.calEvent.activeTimeDetail.Id).click(function () {
      self.deleteEvent(event.calEvent.activeTimeDetail.SheduleItemId);
      // self.deleteEvent(event.calEvent.activeTimeDetail.Id);
    });


    const tooltip =
      `<div class="event-tooltip">
      <div class="mb-1">
        <strong>` + event.calEvent.title + `</strong>
      </div>
      <div><span class="text-muted">Day :</span>
        <span>` + moment(event.calEvent.start._d).format('dddd') + `</span>
        <span> | </span>
        <span>` + moment(event.calEvent.start._d).format('h:mm a') + `
        <span> - </span>` + moment(event.calEvent.end._d).format('h:mm a') + `
      </span>
      </div>
      <div><span class="text-muted">Max No of Bookings :</span>
        <span>
          ` + event.calEvent.activeTimeDetail.MaxNoOfBookings + `
        </span>
      </div>
      <div><span class="badge badge-pill badge-secondary fc-exe-badge">` + event.calEvent.activeTimeDetail.NumberOfMembers + `/` + event.calEvent.activeTimeDetail.MaxNoOfBookings +
      `</span> <span class="icon-calendar-changed text-dark exc-cal-changed-icon" title="Changed calendar"></span> </div>
      <div class="dropdown-divider"></div>
      <div><span class="text-muted">Instructors</span></div>
      <div>` + instructerListHtml + `
      </div>
      <div class="dropdown-divider"></div>
      <div ><span class="text-muted">Location</span></div>
      <div>` + resourceLstHtml + `
      </div>
    </div>`;

    const $tooltip = $(tooltip).appendTo('body');
    let clientHeight = 0;
    let clientWidth = 0;
    $(event.jsEvent.currentTarget).mouseover(function (e) {
      $(event.jsEvent.currentTarget).css('z-index', 10000);
      $tooltip.fadeIn('500');
      $tooltip.fadeTo('10', 1.9);
      clientHeight = document.body.clientHeight;
      clientWidth = document.body.clientWidth;

    }).mousemove(function (e) {

      this.isShowTooltip = true;


      if ((e.pageX + $('.event-tooltip').width() + 70) > clientWidth) {
        this.tooltipX = e.pageX - $('.event-tooltip').width();
        $tooltip.css('left', e.pageX - ($('.event-tooltip').width() + 40));
      } else {
        this.tooltipX = e.pageX + 20;
        $tooltip.css('left', e.pageX + 20);
      }

      if ((e.pageY + $('.event-tooltip').height() + 30) > clientHeight) {
        this.tooltipY = e.pageY - ($('.event-tooltip').height() + 30);
        $tooltip.css('top', e.pageY - ($('.event-tooltip').height() + 30));

      } else {
        $tooltip.css('top', e.pageY + 10);
        this.tooltipY = e.pageY + 10;

      }



    });
  }

  mouseOut(event) {
    $(event.jsEvent.currentTarget).css('z-index', 8);
    $('.event-tooltip').remove();
  }



  ViewRender(view) {

  }

  onButtonGroupClick($event) {
    const clickedElement = $event.target || $event.srcElement;
    if (clickedElement.nodeName === 'BUTTON') {

      const isCertainButtonAlreadyActive = clickedElement.parentElement.querySelector('.active');
      // if a Button already has Class: .active

      if (isCertainButtonAlreadyActive) {
        isCertainButtonAlreadyActive.classList.remove('active');
      }
      clickedElement.className += ' active';
    }

  }

  saveNewClassSuccess(event) {
    PreloaderService.showPreLoader();

    this.exceClassService.SaveScheduleItem({ ScheduleItem: event }).subscribe(
      res => {
        PreloaderService.hidePreLoader();
        if (res.Data > 0) {
          // this.saveNewClassEvent.emit(res);
          this.searchClasses();

        } else if (res.Data === -1) {
          this.model = this.exceMessageService.openMessageBox('WARNING',
            {
              messageTitle: 'Exceline',
              messageBody: 'CLASS.ScheduleOverlapWithInstructor'
            }
          );
        } else if (res.Data === -2) {
          this.model = this.exceMessageService.openMessageBox('WARNING',
            {
              messageTitle: 'Exceline',
              messageBody: 'CLASS.ScheduleOverlapWithResource'
            }
          );
        }
      }, err => {
        PreloaderService.hidePreLoader();

      }
    );
    this.addNewClassModel.close();
  }

  cancelAddingNewCls() {
    this.addNewClassModel.close();
  }

  updateClass(): void {
    this.searchClasses();
    this.addNewClassModel.close();
  }

  cancelUpdatingCls(): void {
    this.addNewClassModel.close();
  }

  ngOnDestroy(): void {
    if (this.addNewClassModel) {
      this.addNewClassModel.close();
    }
    if (this.yeshandlerSubs) {
      this.yeshandlerSubs.unsubscribe();
    }
    if (this.nohandlerSubs) {
      this.nohandlerSubs.unsubscribe();
    }

  }

}



