import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { PreloaderService } from '../../../../shared/services/preloader.service';
import { ExceClassService } from '../../services/exce-class.service';
import { DataTableResource } from '../../../../shared/components/us-data-table/tools/data-table-resource';
import { ClassHomeService } from '../class-home.service';
import { IMyDpOptions } from '../../../../shared/components/us-date-picker/interfaces/my-options.interface';
import { Component, OnInit, OnDestroy, Renderer2, Input, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { UsbModalRef } from '../../../../shared/components/us-modal/modal-ref';
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { Subscription } from 'rxjs';
import { ExceToolbarService } from 'app/modules/common/exce-toolbar/exce-toolbar.service';
import { ExceLoginService } from '../../../login/exce-login/exce-login.service';
import { DomSanitizer } from '@angular/platform-browser';

const now = new Date();
@Component({
  selector: 'class-list',
  templateUrl: './class-list.component.html',
  styleUrls: ['./class-list.component.scss']
})
export class ClassListComponent implements OnInit, OnDestroy {
  yesDeletehandler: Subscription;

  @Input() isDisplayDateNavPanel: boolean;
  @Input() isDisableDateChange: boolean;
  @Input() isDisableToTimeChange: boolean;
  @Input() isFixed: boolean;
  @Input() inputFromDate: any;
  @Input() inputToDate: any;
  @Input() seasonId: number;

  @Output() informationHandler: EventEmitter<object> = new EventEmitter();

  filterText: any;
  model: void;
  selectedClass: any;
  monthCount: number;
  weekCount: number;
  displayType: string;
  startTime: any;
  fromDate: any;
  toDate: any;
  dayOfWeek: number;
  clsDetailModel: UsbModalRef;
  clsTimeDetailModel: UsbModalRef;
  alterationDetailModel: UsbModalRef;
  classIds: any;
  // itemResource: DataTableResource<{}>;
  // itemCount: number;
  tommorow: any;
  addNewClassModel: UsbModalRef;
  public dateFilterForm: FormGroup;
  public radioGroupForm: FormGroup;
  classActiveTimes: any[];
  filteredActiveTimes: any[];
  unFilteredActiveTimes: any[];
  classTimeDetailDataList: any[];
  resetClassActiveTimes: any[];
  branchId: any;
  type: string;
  isShowInfo: boolean;
  message: string;
  testData: any;
  ListOfClassInstances: any[];
  alterationMessage: string;
  testFilteredList: any[];
  ScheduleData: any;
  instructors = [];
  htmltext: any;

  names = {
    ClassType: 'Klassetype',
    MaxNoOfBookings: 'Plasser',
    Day: 'Dag',
    StartDateTime: 'Start',
    EndDateTime: 'Slutt',
    StartDate: 'Startdato',
    EndDate: 'Sluttdato',
    InstructorNameListStr: 'Instruktør',
    LocationNameListStr: 'Sal'
  }

  colNamesNo = {
    ClassType: 'Klassetype',
    MaxNoOfBookings: 'Plasser',
    Day: 'Dag',
    StartDateTime: 'Start',
    EndDateTime: 'Slutt',
    StartDate: 'Startdato',
    EndDate: 'Sluttdato',
    InstructorNameListStr: 'Instruktør',
    LocationNameListStr: 'Sal'
  }

  colNamesEn = {
    ClassType: 'Class type',
    MaxNoOfBookings: 'Capacity',
    Day: 'Day',
    StartDateTime: 'Start time',
    EndDateTime: 'End time',
    StartDate: 'Start date',
    EndDate: 'End date',
    InstructorNameListStr: 'Insctructor',
    LocationNameListStr: 'Location'
  }

  classDefs: any;
  @ViewChild('classDetailModal') classDetailModal: ElementRef;
  @ViewChild('classTimeDetailModal') classTimeDetailModal: ElementRef;
  @ViewChild('alterationDetailModal') alterationDetailModal: ElementRef;

  today: any;
  formErrors = {
    'fromDate': '',
    'toDate': ''
  };

  validationMessages = {
    'fromDate': {
      'required': 'From Date is required.'
    },
    'toDate': {
      'required': 'To Date is required.'
    }
  };
  fromDatePickerOptions: any;
  // private fromDatePickerOptions: IMyDpOptions = {
  //   disableSince: { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() },
  // };
  toDatePickerOptions: any;
  // private toDatePickerOptions: IMyDpOptions = {
  //   disableSince: { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() }
  // };

  private radioButtonModel = 'Maserati';
  gridApi: any;



  constructor(
    private fb: FormBuilder,
    private renderer: Renderer2,
    private classHomeService: ClassHomeService,
    private exceMessageService: ExceMessageService,
    private exceClassService: ExceClassService,
    private modalService: UsbModal,
    private sanitizer: DomSanitizer,
    private toolBarService: ExceToolbarService,
    private translateService: TranslateService,
    private exceLoginService: ExceLoginService
  ) {

    this.yesDeletehandler = this.exceMessageService.yesCalledHandle.subscribe((value) => {
      this.deleteClassActiveTimeOk();
      this.translateService.get('CLASS.DeletionSuccessful').subscribe(deleteMessage => this.message = deleteMessage);
      this.type = 'SUCCESS';
      // Emit event to be shown in 'class-home' (parent component)
      this.informationHandler.emit({
        type: this.type,
        message: this.message
      });
    });

    this.classHomeService.headerFilter.subscribe(
      res => {


        // this.classActiveTimes = this.classHomeService.ClassActiveTimes;

          this.classActiveTimes = this.resetClassActiveTimes;
          // type filter
        if (!this.isFixed && !((res.type === 'ALL' || res.type === ''))) {
          this.classActiveTimes = this.classActiveTimes.filter(z => z.ClassTypeId === Number(res.type)
        );
        } else if (this.isFixed && !((res.type === 'ALL' || res.type === '') ) ) {
          this.classActiveTimes = this.classActiveTimes.filter(x => x.ClassTypeId === Number(res.type));
        }

        // instructor filter

        if ((!this.isFixed && !(res.instructor === 'ALL' || res.instructor === ''))) {
          this.classActiveTimes = this.classActiveTimes.filter(z => {
            const insList = z.InstructorList.filter(y => y.Id === Number(res.instructor));
            return insList.length > 0 ? true : false; });
          }else if ((this.isFixed && !(res.instructor === 'ALL' || res.instructor === '')) ) {
          this.classActiveTimes = this.classActiveTimes.filter(x => {
            const insList = x.InstructorList.filter(y => y.Id === Number(res.instructor));
            return insList.length > 0 ? true : false;
          });
        }
        // location filter
        if ((!this.isFixed && !(res.location === 'ALL' || res.location === ''))) {
          this.classActiveTimes = this.classActiveTimes.filter(x => {
            const insList = x.ResourceLst.filter(y => y.Id === Number(res.location));
            return insList.length > 0 ? true : false});
        } else if ((this.isFixed && !(res.location === 'ALL' || res.location === ''))) {
          this.classActiveTimes = this.classActiveTimes.filter(x => {
            const insList = x.ResourceLst.filter(y => y.Id === Number(res.location));
            return insList.length > 0 ? true : false;
          });
        }
        // group filter
        if ((!this.isFixed && !(res.group === 'ALL' || res.group === ''))) {
          this.classActiveTimes = this.classActiveTimes.filter(z => z.ClassGroupId === Number(res.group));
        } else if ((this.isFixed && !(res.group === 'ALL' || res.group === ''))) {
          this.classActiveTimes = this.classActiveTimes.filter(x => x.ClassGroupId === Number(res.group));
        }
        // level filter
        if ((!this.isFixed && !(res.level === 'ALL' || res.level === ''))) {
          this.classActiveTimes = this.classActiveTimes.filter(z => z.ClassLevelId === Number(res.level));
        } else if ((this.isFixed && !(res.level === 'ALL' || res.level === ''))) {
          this.classActiveTimes = this.classActiveTimes.filter(x => x.ClassLevelId === Number(res.level));
        }
        this.gridApi.redrawRows();
      });

  }

  ngOnInit() {
    const language = this.toolBarService.getSelectedLanguage();
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;

    switch (language.id) {
      case 'no':
        this.names = this.colNamesNo;
        break;
      case 'en':
        this.names = this.colNamesEn;
        break;
    }

    this.displayType = 'DAY';
    this.weekCount = 0;
    this.monthCount = 0;
    this.today = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
    this.fromDate = this.today;
    this.toDate = this.today;
    if (this.inputFromDate && this.inputToDate) {
      let startD = this.inputFromDate.split('T');
      startD = startD[0].split('-');
      const sd = { year: Number(startD[0]), month:  Number(startD[1]), day:  Number(startD[2]) };
      let endD = this.inputToDate.split('T');
      endD = endD[0].split('-');
      const td = { year: Number(endD[0]), month:  Number(endD[1]), day:  Number(endD[2]) };
      this.dateFilterForm = this.fb.group({
        fromDate: [{ date: sd }, [Validators.required]],
        toDate: [{ date: td }, [Validators.required]]
      });
      if (!this.isFixed) {
        const disUn = new Date(sd.year, sd.month - 1, sd.day)
        disUn.setDate(disUn.getDate() - 1)
        const disSi = new Date(td.year, td.month - 1, td.day)
        disSi.setDate(disSi.getDate() + 1)
        this.fromDatePickerOptions = {
          disableUntil: {year: disUn.getFullYear(), month: disUn.getMonth() + 1, day: disUn.getDate()},
          disableSince: {year: disSi.getFullYear(), month: disSi.getMonth() + 1, day: disSi.getDate()}
        }
        this.toDatePickerOptions = {
          disableUntil: {year: disUn.getFullYear(), month: disUn.getMonth() + 1, day: disUn.getDate()},
          disableSince: {year: disSi.getFullYear(), month: disSi.getMonth() + 1, day: disSi.getDate()}
        }
      }
      this.searchClasses();
    } else {
      this.dateFilterForm = this.fb.group({
        fromDate: [{ date: this.today }, [Validators.required]],
        toDate: [{ date: this.today }, [Validators.required]]
      });
      this.searchClasses();
    }
    // this.dateFilterForm = this.fb.group({
    //   fromDate: [{ date: this.today }, [Validators.required]],
    //   toDate: [{ date: this.today }, [Validators.required]]
    // });

    this.radioGroupForm = this.fb.group({
      'model': 1
    });

    // this.dateFilterForm.statusChanges.subscribe(_ => {
    //   this.searchClasses();
    // });

    this.classDefs = [
      {headerName: this.names.ClassType, field: 'ClassTypeName', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
        filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function name(params) {
        return params.data.ClassType.Name;
      }},
      {headerName: this.names.MaxNoOfBookings, field: 'MaxNoOfBookings', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
        filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true},
      {headerName: this.names.Day, field: 'Day', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        let theDay;
        switch (params.data.Day) {
        case -1 : {theDay = 'Søndag'; break; }
        case 0 : {theDay = 'Mandag'; break; }
        case 1 : {theDay = 'Tirsdag'; break; }
        case 2 : {theDay = 'Onsdag'; break; }
        case 3 : {theDay = 'Torsdag'; break; }
        case 4 : {theDay = 'Fredag'; break; }
        case 5 : {theDay = 'Lørdag'; break; }
        case 6 : {theDay = 'Søndag'; break; }
        case 7 : {theDay = 'Mandag'; break; }
      }
        return theDay;
        }},
      {headerName: this.names.StartDateTime, field: 'StartDateTime', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        const time = params.value.split('T');
        return time[1].slice(0, 5);
        }
      },
      {headerName: this.names.EndDateTime, field: 'EndDateTime', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        const time = params.value.split('T');
        return time[1].slice(0, 5);
      }},
      {headerName: this.names.StartDate, field: 'StartDate', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        const time = params.value.split('T');
        const date = time[0].split('-');
        return date[2] + '.' + date[1] + '.' + date[0];
      }},
      {headerName: this.names.EndDate, field: 'EndDate', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        const time = params.value.split('T');
        const date = time[0].split('-');
        return date[2] + '.' + date[1] + '.' + date[0];
      }},
      {headerName: this.names.InstructorNameListStr, field: 'InstructorNameListStr', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        let returnString = '';
        params.data.InstructorList.forEach(instructor => {
          returnString = returnString + '<li>' +   instructor.DisplayName  + '</li>'
        })
        return returnString;
      }
      },
      {headerName: this.names.LocationNameListStr, field: 'LocationNameListStr', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        let returnString = '';
        params.data.ResourceLst.forEach(resource => {
          returnString = returnString + '<li>' +  resource.DisplayName  + '</li>'
        })
        return returnString;
      }
      },
      {headerName: '', field: 'Detaljer', suppressMenu: true, width: 50, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'center'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        return '<button class="btn btn-primary btn-icon-min" title="Detaljer"><span class="icon-detail"></span></button>';
      }
      },
      {headerName: '', field: 'Slett', suppressMenu: true, width: 50, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'center'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        if (!params.data.IsDeleted) {
          return '<button disabled class="btn btn-danger btn-icon-min" title="Slett"><span class="icon-cancel"></span></button>';
        } else {
          return '<button class="btn btn-danger btn-icon-min" title="Slett"><span class="icon-cancel"></span></button>';
        }
      }
      },
    ]
  }

  onGridReady(params) {
    this.gridApi = params.api;
    // this.gridColumnApi = params.columnApi;
    // auto fits columns to fit screen
    this.gridApi.sizeColumnsToFit();
    // hiding loading message on load
    this.gridApi.gridOptionsWrapper.gridOptions.api.hideOverlay();
  }

  cellClicked(event) {
    const classItem = event.data;
    const column = event.column.colDef.field;
    switch (column) {
      case 'Detaljer':
      if (this.isFixed) {
      this.openClassDetailModel(this.classDetailModal, classItem);
      }else {
      this.openClassTimeDetailModal(this.classTimeDetailModal, classItem);
    }
      break;
      case 'Slett':
      if (classItem.IsDeleted) {
        this.deleteClassActiveTime(classItem);
      }
    }
  }

  selectRowDblClick(item) {
    this.openClassDetailModel(this.classDetailModal, item);
  }

  filterByClassType() {
    if (this.isFixed) {
    if (this.filterText.length > 0) {
      this.classActiveTimes = this.classHomeService.ClassActiveTimes.filter(
        x => x.ClassType.Name.toLowerCase().startsWith(this.filterText.trim().toLowerCase()));
    } else {
      this.classActiveTimes = this.classHomeService.ClassActiveTimes;
    }
  }else {
    if (this.filterText.length > 0) {
      this.classActiveTimes = this.classActiveTimes.filter(
        x => x.ClassType.Name.toLowerCase().startsWith(this.filterText.trim().toLowerCase()));
    } else {
      this.classActiveTimes = this.resetClassActiveTimes;
    }
  }
  }

  openAddNewModal(content) {
    const today = new Date();
    const nowTime = new Date();
    this.startTime = moment().format('HH:mm');
    // *** set todate to selected todate in view ***
    this.toDate = this.dateFilterForm.get('toDate').value;
    this.fromDate = this.dateFilterForm.get('fromDate').value;
    this.dayOfWeek = today.getDay();
    this.addNewClassModel = this.modalService.open(content, { width: '1000' });
  }

  openClassDetailModel(content, item) {
    let itemToPick = item;
    item.ClassType = this.classHomeService.ClassTypeList.find(x => x.Id === item.ClassTypeId);
    if (!this.isFixed) {
     itemToPick = this.unFilteredActiveTimes.find(x => x.ClassTypeId === item.ClassTypeId);
    const replacevalue = this.ScheduleData.ClassList.find(z => z.ScheduleItemId === itemToPick.SheduleItemId)
    itemToPick.StartDate = replacevalue.StartDate;
    itemToPick.EndDate =  replacevalue.EndDate; }
    this.selectedClass = (this.isFixed) ? item : itemToPick
    this.clsDetailModel = this.modalService.open(content, { width: '1000' });
  }

  openClassTimeDetailModal(content, item) {
    const detailedTimeListForClass = [];
    const filteredClassInstanceList  = this.testData.find(x => x.ScheduleItemId === item.ScheduleItemId )
    this.ListOfClassInstances = filteredClassInstanceList.ClassInstanceList
    this.ListOfClassInstances.forEach(Time => {
      const data = {
        Dato: Time.Date.slice(8, 10) + '.' + Time.Date.slice(5, 7) + '.' + Time.Date.slice(0, 4) ,
       Start: Time.StartTime.slice(11, 16),
       Slutt: Time.EndTime.slice(11, 16),
       Altered: Time.Altered,
       AlterationDetails: {
        NewStartTime: Time.AlterationSpecs.NewStartTime.slice(11, 16),
        OldStartTime: Time.AlterationSpecs.OldStartTime.slice(11, 16),
        StartTimeChanged: Time.AlterationSpecs.StartTimeChanged,
        NewEndTime: Time.AlterationSpecs.NewEndTime.slice(11, 16),
        OldEndTime: Time.AlterationSpecs.OldEndTime.slice(11, 16),
        EndTimeChanged: Time.AlterationSpecs.EndTimeChanged,
        OldDate: Time.AlterationSpecs.OldDate.slice(8, 10) + '.' + Time.AlterationSpecs.OldDate.slice(5, 7) + '.' + Time.AlterationSpecs.OldDate.slice(5, 7) ,
        NewDate: Time.AlterationSpecs.NewDate.slice(8, 10) + '.' + Time.AlterationSpecs.NewDate.slice(5, 7) + '.' + Time.AlterationSpecs.NewDate.slice(5, 7) ,
        DateChanged: Time.AlterationSpecs.DateChanged,
        OldNumberOfSpots: Time.AlterationSpecs.OldMaxAmountOfBookings,
        NewNumberOfSpots: Time.AlterationSpecs.NewMaxAmountOfBookings,
        NumberOfSeatsChanged: Time.AlterationSpecs.MaxAmountOfBookingsChanged,
        InstructorListChanged: Time.AlterationSpecs.InstructorListChanged,
        RemovedInstructorsList: Time.AlterationSpecs.RemovedInstructorsList,
        AddedInstructorsList: Time.AlterationSpecs.AddedInstructorsList,
        ResourceListChanged: Time.AlterationSpecs.ResourceListChanged,
        RemovedResourceList: Time.AlterationSpecs.RemovedResourceList,
        AddedResourceList: Time.AlterationSpecs.AddedResourceList
        },
        LastModifiedDate: (Time.AlterationSpecs.LastModifiedDateTime === null) ? '' :  Time.AlterationSpecs.LastModifiedDateTime.slice(8, 10) + '.'
         + Time.AlterationSpecs.LastModifiedDateTime.slice(5, 7)
        + '.' + Time.AlterationSpecs.LastModifiedDateTime.slice(5, 7),
       LastModifiedTime: (Time.AlterationSpecs.LastModifiedDateTime === null) ? '' : Time.AlterationSpecs.LastModifiedDateTime.slice(11, 16),
       LastModifiedUser: Time.AlterationSpecs.LastModifiedUser
     }
     detailedTimeListForClass.push(data)
    })



   /*
    this.unFilteredActiveTimes.forEach(Time => {
    if (Time.SheduleItemId === item.SheduleItemId) {
      const data = {
        Dato: Time.StartDate.slice(8, 10) + '.' + Time.StartDate.slice(5, 7) + '.' + Time.StartDate.slice(0, 4) ,
        Start: Time.StartDateTime.slice(11, 16),
        Slutt: Time.EndDateTime.slice(11, 16)
      }*/
     // detailedTimeListForClass.push(data)
   // }});
    this.classTimeDetailDataList =  detailedTimeListForClass;
    item.ClassType = this.classHomeService.ClassTypeList.find(x => x.Id === item.ClassTypeId);
    this.selectedClass = item;
    this.clsTimeDetailModel = this.modalService.open(content, {width: '600'});
  }

  onButtonGroupClick($event) {
    const clickedElement = $event.target || $event.srcElement;
    if (clickedElement.nodeName === 'BUTTON') {
      const isCertainButtonAlreadyActive = clickedElement.parentElement.querySelector('.active');
      if (isCertainButtonAlreadyActive) {
        isCertainButtonAlreadyActive.classList.remove('active');
      }
      clickedElement.className += ' active';
    }

  }

  selectDateRange(val) {
    const fromDate = new Date(
      this.dateFilterForm.value.fromDate.date.year + '-' +
      this.dateFilterForm.value.fromDate.date.month + '-' +
      this.dateFilterForm.value.fromDate.date.day
    );
    const toDate = new Date(
      this.dateFilterForm.value.toDate.date.year + '-' +
      this.dateFilterForm.value.toDate.date.month + '-' +
      this.dateFilterForm.value.toDate.date.day
    );
    const originalFromDate = fromDate;
    const originalToDate = toDate;
    switch (val) {
      case 'TODAY':
        this.dateFilterForm.patchValue({
          fromDate: { date: this.today },
          toDate: { date: this.today }
        });
        this.displayType = 'DAY';
        this.weekCount = 0;
        this.monthCount = 0;

        break;
      case 'NEXT':
        switch (this.displayType) {
          case 'DAY':
            const newFrom = moment(fromDate).add(1, 'days').toDate();
            const newTo = moment(toDate).add(1, 'days').toDate();
            this.fromDate = { year: newFrom.getFullYear(), month: newFrom.getMonth() + 1, day: newFrom.getDate() };
            this.toDate = { year: newTo.getFullYear(), month: newTo.getMonth() + 1, day: newTo.getDate() };
            this.dateFilterForm.patchValue({
              fromDate: { date: this.fromDate },
              toDate: { date: this.toDate }
            });
            break;

          case 'WEEK':
            this.weekCount = this.weekCount + 1;
            const newFromW = moment().add(this.weekCount, 'weeks').startOf('isoWeek').toDate();
            const newToW = moment().add(this.weekCount, 'weeks').endOf('isoWeek').toDate();
            this.fromDate = { year: newFromW.getFullYear(), month: newFromW.getMonth() + 1, day: newFromW.getDate() };
            this.toDate = { year: newToW.getFullYear(), month: newToW.getMonth() + 1, day: newToW.getDate() };
            this.dateFilterForm.patchValue({
              fromDate: { date: this.fromDate },
              toDate: { date: this.toDate }
            });
            break;

          case 'MONTH':
            this.monthCount = this.monthCount + 1;
            const newFromM = moment().add(this.monthCount, 'months').startOf('month').toDate();
            const newToM = moment().add(this.monthCount, 'months').endOf('month').toDate();
            this.fromDate = { year: newFromM.getFullYear(), month: newFromM.getMonth() + 1, day: newFromM.getDate() };
            this.toDate = { year: newToM.getFullYear(), month: newToM.getMonth() + 1, day: newToM.getDate() };
            this.dateFilterForm.patchValue({
              fromDate: { date: this.fromDate },
              toDate: { date: this.toDate }
            });
            break;

        }
        break;
      case 'BACK':
        switch (this.displayType) {
          case 'DAY':
            const newFrom = moment(fromDate).add(-1, 'days').toDate();
            const newTo = moment(toDate).add(-1, 'days').toDate();
            this.fromDate = { year: newFrom.getFullYear(), month: newFrom.getMonth() + 1, day: newFrom.getDate() };
            this.toDate = { year: newTo.getFullYear(), month: newTo.getMonth() + 1, day: newTo.getDate() };
            this.dateFilterForm.patchValue({
              fromDate: { date: this.fromDate },
              toDate: { date: this.toDate }
            });
            break;

          case 'WEEK':

            this.weekCount = this.weekCount - 1;
            const newFromW = moment().add(this.weekCount, 'weeks').startOf('isoWeek').toDate();
            const newToW = moment().add(this.weekCount, 'weeks').endOf('isoWeek').toDate();
            this.fromDate = { year: newFromW.getFullYear(), month: newFromW.getMonth() + 1, day: newFromW.getDate() };
            this.toDate = { year: newToW.getFullYear(), month: newToW.getMonth() + 1, day: newToW.getDate() };
            this.dateFilterForm.patchValue({
              fromDate: { date: this.fromDate },
              toDate: { date: this.toDate }
            });
            break;

          case 'MONTH':
            this.monthCount = this.monthCount - 1;
            const newFromM = moment().add(this.monthCount, 'months').startOf('month').toDate();
            const newToM = moment().add(this.monthCount, 'months').endOf('month').toDate();
            this.fromDate = { year: newFromM.getFullYear(), month: newFromM.getMonth() + 1, day: newFromM.getDate() };
            this.toDate = { year: newToM.getFullYear(), month: newToM.getMonth() + 1, day: newToM.getDate() };
            this.dateFilterForm.patchValue({
              fromDate: { date: this.fromDate },
              toDate: { date: this.toDate }
            });
            break;

        }
        break;
      case 'DAYVIEW':
        this.dateFilterForm.patchValue({
          fromDate: { date: this.today },
          toDate: { date: this.today }
        });
        this.displayType = 'DAY';
        this.weekCount = 0;
        this.monthCount = 0;
        break;
      case 'WEEKVIEW':

        const startOfWeek = moment().startOf('week').toDate();
        const endOfWeek = moment().endOf('week').toDate();

        this.dateFilterForm.patchValue({
          fromDate: { date: { year: startOfWeek.getFullYear(), month: startOfWeek.getMonth() + 1, day: startOfWeek.getDate() } },
          toDate: { date: { year: endOfWeek.getFullYear(), month: endOfWeek.getMonth() + 1, day: endOfWeek.getDate() } }
        });
        this.displayType = 'WEEK';
        this.weekCount = 0;
        this.monthCount = 0;
        break;
      case 'MONTHVIEW':
        const startOfMonth = moment().startOf('month').toDate();
        const endOfMonth = moment().endOf('month').toDate();

        this.dateFilterForm.patchValue({
          fromDate: { date: { year: startOfMonth.getFullYear(), month: startOfMonth.getMonth() + 1, day: startOfMonth.getDate() } },
          toDate: { date: { year: endOfMonth.getFullYear(), month: endOfMonth.getMonth() + 1, day: endOfMonth.getDate() } }
        });
        this.displayType = 'MONTH';
        this.weekCount = 0;
        this.monthCount = 0;
        break;
    }

    console.log(this.dateFilterForm.get('fromDate').value)
    console.log(this.dateFilterForm.get('toDate').value)

    if (!this.isFixed) {
      let useDate = this.dateFilterForm.get('fromDate').value;
      const fromDateCompare = new Date(useDate.date.year, useDate.date.month - 1, useDate.date.day);
      const fromDateSeason = new Date(this.fromDatePickerOptions.disableUntil.year, this.fromDatePickerOptions.disableUntil.month - 1, this.fromDatePickerOptions.disableUntil.day);
      fromDateSeason.setDate(fromDateSeason.getDate() + 1)
      useDate = this.dateFilterForm.get('toDate').value;
      const toDateCompare = new Date(useDate.date.year, useDate.date.month - 1, useDate.date.day);
      const toDateSeason = new Date(this.fromDatePickerOptions.disableSince.year, this.fromDatePickerOptions.disableSince.month - 1, this.fromDatePickerOptions.disableSince.day);
      toDateSeason.setDate(toDateSeason.getDate() - 1)
      if (fromDateCompare < fromDateSeason) {
        this.dateFilterForm.patchValue({
          fromDate: { date: { year: fromDateSeason.getFullYear(), month: fromDateSeason.getMonth() + 1, day: fromDateSeason.getDate() } },
          toDate: { date: { year: originalToDate.getFullYear(), month: originalToDate.getMonth() + 1, day: originalToDate.getDate() } }
        });
      } else if (toDateCompare > toDateSeason) {
        this.dateFilterForm.patchValue({
          toDate: { date: { year: toDateSeason.getFullYear(), month: toDateSeason.getMonth() + 1, day: toDateSeason.getDate() } },
          fromDate: { date: { year: originalFromDate.getFullYear(), month: originalFromDate.getMonth() + 1, day: originalFromDate.getDate() } }
        });
      } else {
        this.searchClasses();
      }
    } else {
      this.searchClasses();
    }
  }

  searchClasses() {
    const fromDate = this.dateFilterForm.value.fromDate.date.year + '-' +
      this.dateFilterForm.value.fromDate.date.month + '-' +
      this.dateFilterForm.value.fromDate.date.day;
    const toDate = this.dateFilterForm.value.toDate.date.year + '-' +
      this.dateFilterForm.value.toDate.date.month + '-' +
      this.dateFilterForm.value.toDate.date.day;
    PreloaderService.showPreLoader();
    this.exceClassService.GetEntityActiveTimes({
      StartDate: fromDate,
      EndDate: toDate,
      EntityList: this.classHomeService.ClassIds,
      EntityRoleType: 'CLS'
    }).subscribe(activeTimes => {
      activeTimes.Data.forEach(times => {
        const type = this.classHomeService.ClassTypeList.filter(x => x.Id === times.ClassTypeId);
        times.ClassType = type[0];
        times.Color = type[0].Colour;
        const now1 = new Date();
        const startDateTime = new Date(times.StartDateTime);
        if (startDateTime > now1) {
          times.IsDeleted = true;
        }
        times.ClassTypeName = times.ClassType.Name
      });
      this.classHomeService.ClassActiveTimes = activeTimes.Data;
      this.unFilteredActiveTimes =  activeTimes.Data;
      //this.classActiveTimes =  activeTimes.Data;
      this.instructors = [];
      activeTimes.Data.forEach(cls => {
        cls.InstructorList.forEach(inst => {
          let add = true;
          this.instructors.forEach(instAdded => {
            if (instAdded.Id === inst.Id) {
              add = false;
            }
          });
          if (add) {
            this.instructors.push(inst)
          }
        });
      });
      // add instructors to class-home
      this.classHomeService.InstructorList = this.instructors;
      PreloaderService.hidePreLoader();
    }, err => {
      PreloaderService.hidePreLoader();
    },
  () => {
  this.filteredActiveTimes = this.classActiveTimes
  const filterList = [];
  const excludelist = [];
  let runOnlyOnce  = 0;
  const supportlist = this.unFilteredActiveTimes;
  this.unFilteredActiveTimes.forEach(activeElment => {
    let firstDate = (activeElment.StartDateTime);
    let lastDate = (activeElment.EndDateTime);
    const something = supportlist.filter(item =>
      (item.SheduleItemId === activeElment.SheduleItemId)
    )
    if (something.length > 1) {
      something.forEach(item => {
        if ((item.StartDateTime) < firstDate) {

          firstDate = (item.StartDateTime);
        }
        if ( (item.EndDateTime) > lastDate) {
          lastDate = (item.EndDateTime);

        }
        item.StartDateTime = firstDate
        item.EndDateTime = lastDate
        if (runOnlyOnce === 0 ) {
        filterList.push(item)
        this.filteredActiveTimes = this.filteredActiveTimes.filter(p => p.SheduleItemId !== item.SheduleItemId)
        excludelist.push(item.SheduleItemId)
        runOnlyOnce++
      }

          if ( excludelist.indexOf(item.SheduleItemId) === -1) {
            filterList.push(item)
            excludelist.push(item.SheduleItemId)
            this.filteredActiveTimes = this.filteredActiveTimes.filter(p => p.SheduleItemId !== item.SheduleItemId)
          }
      })
    }
    })
    filterList.forEach(elem => {
      this.filteredActiveTimes.push(elem)
    })
  })


  let someData;

  if (!this.isFixed) {
  this.exceClassService.GetSeasonAndClassInfo(this.branchId,  this.seasonId ).subscribe(res => {
    this.testData = res.Data.ClassList
    this.ScheduleData = res.Data;
 }, err => { }, () => {
    let i = 0
    const filterList = [];
    const now1 = new Date();
      this.testData.forEach(element => {
        const startDateTime = new Date(element.StartTime);
        let dotw;
        this.translateService.get('DAY.' + (element.DayOfTheWeek - 1)).subscribe(dow => dotw = dow);
       someData = {
         ClassType : {Name: element.NameOfClass},
         MaxNoOfBookings: element.MaxNoOfBookings,
         Day: (element.DayOfTheWeek - 2),
         StartDateTime: element.StartTime,
         EndDateTime: element.EndTime,
         StartDate: element.StartDate,
         EndDate: element.EndDate,
         InstructorList: element.InstructorList,
         ResourceLst: element.ResourceList,
         ScheduleItemId: element.ScheduleItemId,
         IsDeleted: (startDateTime > now1) ? true : false,
         ClassTypeId: element.ClassTypeId,
         ClassGroupId: element.ClassInfo.GroupId,
         ClassGroupLevel: element.ClassInfo.ClassLevelId
       }
       filterList.push(someData);
       i++;
    })
    this.testFilteredList = filterList;

  this.classActiveTimes =  this.testFilteredList;
  const templist = this.classActiveTimes;
  this.resetClassActiveTimes = templist;
  this.classHomeService.ClassActiveTimes = this.classActiveTimes
    });
  } else { this.classActiveTimes =  this.unFilteredActiveTimes
    this.resetClassActiveTimes = this.classActiveTimes;
    this.classHomeService.ClassActiveTimes = this.classActiveTimes
  }

}

// something.forEach(item => {
// filterList.push(item)
// })

  ngOnDestroy(): void {
    if (this.addNewClassModel) {
      this.addNewClassModel.close();
    }
    if (this.clsDetailModel) {
      this.clsDetailModel.close();
    }
    if (this.yesDeletehandler) {
      this.yesDeletehandler.unsubscribe();
    }

    if (this.alterationDetailModel) {
      this.alterationDetailModel.close();
    }

    if (this.clsTimeDetailModel) {
      this.clsTimeDetailModel.close();
    }
  }

  deleteClassActiveTime(item) {
    this.selectedClass = item;
    this.model = this.exceMessageService.openMessageBox('CONFIRM', {
      messageTitle: 'Exceline',
      messageBody: 'CLASS.DeleteConfirm'
    });
  }

  private deleteClassActiveTimeOk() {
    this.unFilteredActiveTimes.forEach(element => {
      if ((this.selectedClass.SheduleItemId === element.SheduleItemId) || (this.selectedClass.ScheduleItemId === element.SheduleItemId ) ) {
      this.exceClassService.DeleteClassActiveTime({ actTimeId: element.Id }).subscribe(
        res => {
        },
        () => {this.classActiveTimes = this.classActiveTimes.filter(x => x.Id !== this.selectedClass.Id)
        this.unFilteredActiveTimes = this.unFilteredActiveTimes.filter(x => x.SheduleItemId !== element.SheduleItemId)
        this.filteredActiveTimes = this.filteredActiveTimes.filter(x => x.SheduleItemId !== element.SheduleItemId)
        }
      )
    }
    });
    this.searchClasses();
    this.gridApi.redrawRows({});
  }

  cancelAddingNewCls() {
    this.addNewClassModel.close();
  }

  saveNewClassSuccess(event) {
    PreloaderService.showPreLoader();

    this.exceClassService.SaveScheduleItem({ ScheduleItem: event }).subscribe(
      res => {
        PreloaderService.hidePreLoader();
        if (res.Data > 0) {
          // this.saveNewClassEvent.emit(res);
          this.searchClasses();
        } else if (res.Data === -1) {
          this.model = this.exceMessageService.openMessageBox('WARNING',
            {
              messageTitle: 'Exceline',
              messageBody: 'CLASS.ScheduleOverlapWithInstructor'
            }
          );
        } else if (res.Data === -2) {
          this.model = this.exceMessageService.openMessageBox('WARNING',
            {
              messageTitle: 'Exceline',
              messageBody: 'CLASS.ScheduleOverlapWithResource'
            }
          );
        }
      }, err => {
        PreloaderService.hidePreLoader();

      }
    );

    this.addNewClassModel.close();
  }

  cancelUpdatingCls() {
    this.clsDetailModel.close();
  }

  updateClassEvent(infoEvent): void {
    // Send emit further up to parent
    this.informationHandler.emit({
      type: infoEvent.type,
      message: infoEvent.message
    });
    this.clsDetailModel.close();
  }

  fromDateChanged(event) {
    const date = event.date
    const fromDate = new Date(date.year, date.month - 1, date.day)
    const toDate = new Date(this.dateFilterForm.value.toDate.date.year,
      this.dateFilterForm.value.toDate.date.month - 1,
      this.dateFilterForm.value.toDate.date.day)
  /*  if (fromDate > toDate) {
      this.toDate = { year: date.year, month: date.month, day: date.day };
      this.dateFilterForm.patchValue({
        toDate: { date: this.toDate }
      });
    }*/
  }

  openAlterationDetailsWindow(content , input) {
    this.alterationMessage = '';
    if (input.AlterationDetails.EndTimeChanged) {
    this.alterationMessage +=
    'Tidspunkt for timen er blitt endret fra ' + input.AlterationDetails.OldStartTime
    + ' - ' + input.AlterationDetails.OldEndTime + ' til ' + input.AlterationDetails.NewStartTime + ' - ' + input.AlterationDetails.NewEndTime + '<br>'; }

    if (input.AlterationDetails.DateChanged) {
    this.alterationMessage +=
    'Dato for timen er blitt endret fra ' + input.AlterationDetails.OldDate
    + ' til ' + input.AlterationDetails.NewDate + '<br>'; }

    if (input.AlterationDetails.NumberOfSeatsChanged) {
    this.alterationMessage +=
    'Antall plasser for timen ble endret fra ' + input.AlterationDetails.OldNumberOfSpots
    + ' til ' + input.AlterationDetails.NewNumberOfSpots + '<br>' }

    if (input.AlterationDetails.InstructorListChanged) {
     this.alterationMessage +=
      '<br><div style="display: inline-block" >'
      + 'Instruktør oppsettet ble endret:<br><b>Fjernet:</b>'
      + '<ul>';
      input.AlterationDetails.RemovedInstructorsList.forEach(element => {
        this.alterationMessage +=  '<li>' + element.DisplayName + '</li>';
      });
      this.alterationMessage += '</ul>';

      this.alterationMessage +=
      '<b>Lagt til:</b>'
      + '<ul>';
      input.AlterationDetails.AddedInstructorsList.forEach(element => {
        this.alterationMessage +=  '<li>' + element.DisplayName + '</li>';
      });
      this.alterationMessage += '</ul></div>';
    }
   if (input.AlterationDetails.ResourceListChanged) {
     this.alterationMessage += '<div>Sal oppsettet ble endret:</div>' +
    '<b>Fjernet:</b><ul>'
     input.AlterationDetails.RemovedResourceList.forEach(element => {
       this.alterationMessage +=  '<li>' + element.DisplayName + '</li>';
     });
     this.alterationMessage += '</ul>';

     this.alterationMessage +=
     '<br><b>Lagt til:</b>'
     + '<ul>'
     input.AlterationDetails.AddedResourceList.forEach(element => {
       this.alterationMessage +=  '<li>' + element.DisplayName + '</li>';
     });
     this.alterationMessage += '</ul></div>'; }

      this.htmltext = this.sanitizer.bypassSecurityTrustHtml(this.alterationMessage)

  this.alterationDetailModel = this.modalService.open(content, {width: '400'})
  }

  exportClassList() {
    return this.ScheduleData;
  }

}
