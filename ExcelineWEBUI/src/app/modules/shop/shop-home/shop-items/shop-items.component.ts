import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { DataTableResource } from '../../../../shared/components/us-data-table/tools/data-table-resource';
import { ShopHomeService } from '../shop-home.service';
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { ExceToolbarService } from '../../../../modules/common/exce-toolbar/exce-toolbar.service';
import { ShopArticlesService } from '../shop-articles/shop-articles.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsErrorService } from 'app/shared/directives/us-error/us-error.service';
import { CategoryTypes } from '../../../../shared/enums/category-types';
import { TranslateService } from '@ngx-translate/core';
import { ColumnDataType } from '../../../../shared/enums/us-enum';
import { PreloaderService } from '../../../../shared/services/preloader.service';
import { ExceMemberService } from '../../../membership/services/exce-member.service';
import { ExceShopService } from '../../services/exce-shop.service';
import { AdminService } from 'app/modules/admin/services/admin.service';


@Component({
  selector: 'shop-items',
  templateUrl: './shop-items.component.html',
  styleUrls: ['./shop-items.component.scss']
})
export class ShopItemsComponent implements OnInit, OnDestroy {
  campaignEvent: Subscription;
  curserPosition: any;

  items = [];
  shopItems = [];
  itemResource?: DataTableResource<any>;
  itemCount = 0;
  discountComment: string;
  modalReference?: any;
  discountItem: any;
  selectedShopItemSubscribe: any;
  clearShopItemSubscribe: any;
  messageBoxOkSubscribe: any;
  translateSubscription: any;
  saleItemTotal = 0;
  isItemReturn: Boolean;
  isEmployee = false;
  mssageBody: string;
  returnItemCommentModel: any;
  public returnItemForm: FormGroup;
  public discountForm: FormGroup;
  newShopItem: any;
  returnItemFormSubmited = false;
  priceType: string;
  locale: string;

  addedCampaignList = [];
  campaignArticle: any;
  isShopItemDisabled = false;
  discountFormSubmited = false;

  returnformErrors = {
    'returnItemComment': ''
  }

  returnItemValidationMsgs = {
    'returnItemComment': {
      'required': 'COMMON.MsgPlzEnterComment',
    }
  }

  discountFormErrors = {
    'discountComment': ''
  }

  discountFormValidationMsgs = {
    'discountComment': {
      'required': 'COMMON.MsgPlzEnterComment',
    }
  }

  @ViewChild('returnCommentModal') returnCommentModal: ElementRef;
  selectedMemberSubscribe: any;
  selectedMember: any;
  isSelectedMember = false;

  entitySelectionModel: any;

  auotocompleteItems = [{ id: 'NAME', name: 'NAME :', isNumber: false }, { id: 'ID', name: 'Id :', isNumber: true }];

  isSaveShopItems: any;
  entityItems = [];
  isBasicInforVisible = true;
  isAddBtnVisible = true;
  isBranchVisible = true;
  isStatusVisible = true;
  isRoleVisible = true;
  isDbPagination = true;
  entitySearchType: string;
  columns = [{ property: 'FirstName', header: 'First Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
  { property: 'LastName', header: 'Last Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
  { property: 'IntCustId', header: 'Number', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
  { property: 'Age', header: 'Age', sortable: false, resizable: false, width: '150px', type: ColumnDataType.DEFAULT },
  { property: 'TelMobile', header: 'Mobile', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
  { property: 'StatusName', header: 'Status', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
  { property: 'BranchName', header: 'Gym Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT }
  ];
  isSelectedMemberSubscribe: Subscription;
  quantitySubscribe: Subscription;
  CustId = '';
  Name = '';
  memberListGetType = 'MEMBER';
  columnsToShowMemberModal = [];


  constructor(
    private modalService: UsbModal,
    private shopHomeService: ShopHomeService,
    private exceMessageService: ExceMessageService,
    private translateService: TranslateService,
    private fb: FormBuilder,
    private shopArticlesService: ShopArticlesService,
    private toolbarService: ExceToolbarService,
    private memberService: ExceMemberService,
    private adminService: AdminService,
    private shopService: ExceShopService
  ) {

    this.isItemReturn = false;
    this.itemResource = new DataTableResource([]);
    this.itemResource.count().then(count => this.itemCount = count);

    this.returnItemForm = fb.group({
      'returnItemComment': [null, Validators.required]
    })

    this.discountForm = fb.group({
      'discountComment': [null, Validators.required]
    });
    this.discountForm.statusChanges.subscribe(_ => {
      UsErrorService.onValueChanged(this.discountForm, this.discountFormErrors, this.discountFormValidationMsgs)
    });

    this.returnItemForm.statusChanges.subscribe(_ => UsErrorService.onValueChanged(this.returnItemForm, this.returnformErrors, this.returnItemValidationMsgs));

    this.campaignEvent = this.shopHomeService.campaignAddEvent.subscribe(
      campaign => {
        this.campaignArticle = null;
        campaign.ArticleList.forEach(element => {
          const findItem = this.shopItems.find(shopItem => shopItem.Id === element.Id);
          if (findItem) {
            this.campaignArticle = findItem;
          }
        });
        if (this.campaignArticle) {
          const findcampaign = this.addedCampaignList.find(citem => citem.Id === campaign.Id);
          if (findcampaign) {
            this.exceMessageService.openMessageBox('WARNING',
              {
                messageTitle: 'Exceline',
                messageBody: 'COMMON.CampaignUserBefore'
              });
          } else {
            if (this.campaignArticle.OrderQuantity >= campaign.MinNumberOfArticles) {
              campaign.OrderQuantity = this.campaignArticle.OrderQuantity;
              const discountCount = campaign.OrderQuantity / campaign.MinNumberOfArticles;
              if (campaign.CategoryCode === 'FIXEDAMOUNT') {
                campaign.Price = campaign.DiscountAmount * discountCount * -1;
              } else if (campaign.CategoryCode === 'PERCENTATGE') {
                campaign.Price = this.campaignArticle.Price * campaign.OrderQuantity * campaign.DiscountPercentage / 100 * -1;
              } else if (campaign.CategoryCode === 'LOWESTPRICE') {
                campaign.Price = this.campaignArticle.Price * discountCount * campaign.NumberOfFreeItems * -1;
              }
              this.addedCampaignList.push(campaign);
              this.calcTotalShopItem();
            } else {
              this.exceMessageService.openMessageBox('WARNING',
                {
                  messageTitle: 'Exceline',
                  messageBody: 'COMMON.BuyMoreArticles'
                });
            }

          }
          if (this.addedCampaignList && this.addedCampaignList.length > 0) {
            this.isShopItemDisabled = true;
            this.shopItems.forEach(element => {
              element.isPromoAdded = true;
            });
          }
        } else {
          this.exceMessageService.openMessageBox('WARNING',
            {
              messageTitle: 'Exceline',
              messageBody: 'COMMON.ArticlesNorRelatedToCampaign'
            });
        }

      });
    if (this.shopHomeService.$isReturn) {
      this.isItemReturn = true;
    }
  }

  ngOnInit() {
    // **Language selecton and subscription - dateformat and amountconverter uses this **
    const language = this.toolbarService.getSelectedLanguage();
    this.locale = language.culture;
    // this._dateFormat = 'DD.MM.YYYY';

    this.toolbarService.langUpdated.subscribe(
      (lang) => {
        if (lang) {
          this.locale = lang.culture;
        }
      }
    );
    // **END**

    // this.shopService.refreshArticleListEvent.next(true);
    if (this.shopHomeService.getShopItems() && this.shopHomeService.$priceType) {
      this.shopItems = this.shopHomeService.getShopItems();
      this.itemResource = new DataTableResource(this.shopItems);
      this.itemResource.count().then(count => this.itemCount = count);
      this.shopHomeService.getShopStatusEvent.next(
        {
          items: this.shopItems,
          priceType: this.shopHomeService.$priceType,
          shopLoginData: this.shopHomeService.$shopLoginData
        });
      this.calcTotalShopItem();
    } else {
      // localStorage.setItem('shopItems', JSON.stringify([]));
      this.shopHomeService.ShopItems = [];
      this.itemResource = new DataTableResource(this.shopItems);
      this.itemResource.count().then(count => this.itemCount = count);
    }

    this.selectedShopItemSubscribe = this.shopHomeService.shopItemChanged
      .subscribe(
      (item) => {
        if (this.addedCampaignList && this.addedCampaignList.length > 0) {
          this.exceMessageService.openMessageBox('WARNING',
            {
              messageTitle: 'Exceline',
              messageBody: 'COMMON.NeedToAddMoreArticles'
            });
        } else {
          if (item) {
            this.newShopItem = {};
            this.items = this.shopHomeService.getShopItems();
            if (item.Category !== 'Gavekort') {
              item.Discount = 0;
            }

            if (item.StockLevel < 1 && item.ArticleType !== 'SERVICE' && !this.isItemReturn && item.StockStatus) {
              this.exceMessageService.openMessageBox('WARNING',
                {
                  messageTitle: 'Exceline',
                  messageBody: 'COMMON.MsgItemUnAvailable'
                }
              );
              item.IsSelected = false;
            } else {
              if (!this.isEmployee) {
                this.priceType = CategoryTypes[CategoryTypes.MEMBER];
                this.shopHomeService.$priceType = this.priceType;
                item.SelectedPrice = item.DefaultPrice;
                item.Price = item.Category === 'Gavekort' ? item.PurchasedPrice : item.DefaultPrice;
              } else {
                this.priceType = CategoryTypes[CategoryTypes.EMPLOYEE];
                this.shopHomeService.$priceType = this.priceType;
                item.SelectedPrice = item.EmployeePrice;
                item.Price = item.Category === 'Gavekort' ? item.PurchasedPrice : item.EmployeePrice;
                item.DefaultPrice = item.EmployeePrice;
              }
              if (this.isItemReturn) {
                this.newShopItem = item;
                if (this.items.length > 0) {
                  const findItem = this.items.find(shopItem => shopItem.Id === item.Id);
                  if (findItem) {
                    if (findItem.OrderQuantity > 0) {
                      findItem.OrderQuantity = 0;
                    }
                    findItem.OrderQuantity = findItem.OrderQuantity - 1;
                  } else {
                    // this.returnItemCommentModel = this.modalService.open(this.returnCommentModal, { width: '400' });
                    // fjerner modal, flyttes til savepayment i shop-payment
                    this.addReturnItemComment();
                    item.OrderQuantity = -1;
                  }
                } else {
                  // this.returnItemCommentModel = this.modalService.open(this.returnCommentModal, { width: '400' });
                  // fjerner modal, flyttes til savepayment i shop-payment
                  this.addReturnItemComment();
                  item.OrderQuantity = -1;
                }

              } else {
                if (this.items.length > 0) {
                  const findItem = this.items.find(shopItem => shopItem.Id === item.Id);
                  if (findItem && findItem.Category !== 'Gavekort') {
                    if (findItem.OrderQuantity < 0) {
                      findItem.OrderQuantity = 0;
                    }
                    if (findItem.ArticleType !== 'SERVICE') {
                      if (!findItem.StockStatus || findItem.StockLevel >= findItem.OrderQuantity) {
                        findItem.OrderQuantity = findItem.OrderQuantity + 1;
                      } else if (findItem.StockLevel < findItem.OrderQuantity) {
                        findItem.OrderQuantity = 1;
                      }
                    } else {
                      findItem.OrderQuantity = findItem.OrderQuantity + 1;
                    }
                  } else {
                    this.items.push(item);
                  }
                } else {
                  this.items.push(item);
                }
              }
            }
            this.shopHomeService.ShopItems = this.items;
            this.shopItems = [];
            this.shopItems = this.items;
            this.calcTotalShopItem();
          }
          this.itemResource = new DataTableResource(this.shopItems);
        }
      });

    this.clearShopItemSubscribe = this.shopHomeService.clearShopItemsEvent.subscribe(
      clear => {
        this.shopService.refreshArticleListEvent.next(true);
        this.shopItems = [];
        this.saleItemTotal = 0;
      });

    this.messageBoxOkSubscribe = this.exceMessageService.okCalledHandle.subscribe((value) => {

    });

    if (this.shopHomeService.$selectedMember) {
      this.selectedMember = this.shopHomeService.$selectedMember;
      this.isSelectedMember = true;
      this.CustId = this.selectedMember.CustId;
      this.Name = this.selectedMember.Name;
    }


    this.selectedMemberSubscribe = this.shopHomeService.memberSelectedEvent.subscribe(
      (member) => {
        if (member) {
          // this.shopHomeService.$isSelectedMember = true;
          // this.isSelectedMember = true;
          // this.selectedMember = member;

          let hasValue;
          try {
            hasValue = member.Id
          } catch (err) {
            hasValue = false;
          }
          if (hasValue) {
            this.isSelectedMember = true;
            // this.shopHomeService.$isSelectedMember = true;
            this.selectedMember = member;
            this.CustId = this.selectedMember.CustId;
            this.Name = this.selectedMember.Name;
          } else {
            if (this.isEmployee) {
              this.isEmployee = false;
            }
            this.isSelectedMember = false;
            this.selectedMember = null;
            this.CustId = '';
            this.Name = '';
            // this.shopHomeService.$selectedMember = null;
          }
        } else {
          this.isEmployee = false;
          this.isSelectedMember = false;
          this.selectedMember = null;
          this.CustId = '';
          this.Name = '';
        }

      }
    );

    this.isSelectedMemberSubscribe = this.shopHomeService.isSelectedMemberEvent.subscribe(
      (value) => {
        if (value) {
          this.isSelectedMember = value;
          this.selectedMember = this.shopHomeService.$selectedMember;
        }
      }
    );

    this.quantitySubscribe = this.shopHomeService.changeQuantityEvent.subscribe(
      (item) => {
        const shopItems = this.shopHomeService.getShopItems();
        const q = shopItems.find(shopItem => shopItem.Id === item.Id);
        if (this.isItemReturn) {
          this.OrderQuantityChanged(item, q.OrderQuantity - 1);
        } else {
          this.OrderQuantityChanged(item, q.OrderQuantity + 1);
        }
      }
    );

    if (this.shopHomeService.$shopOpenedFromOrders) {
      this.selectedMember = this.shopHomeService.$selectedMember;
      this.Name = this.selectedMember.Name;
      this.CustId = this.selectedMember.CustId;
    }

  }

  removeMember() {
    if (this.isEmployee) {
      this.shopHomeService.clearShopItems();
    }
    this.isEmployee = false;
    this.shopHomeService.$selectedMember = null;
    this.Name = '';
    this.CustId = '';

  }

  reloadItems(params) {
    if (this.itemResource) {
      this.itemResource.query(params).then(items => this.shopItems = items);
    }
  }

  removeShopItem(item) {
    this.shopItems = this.shopItems.filter(ele => ele.Id !== item.Id);
    this.itemResource = new DataTableResource(this.shopItems);
    this.itemResource.count().then(count => this.itemCount = count);
    this.shopHomeService.removeShopItem(this.shopItems);
    this.calcTotalShopItem();
  }

  discountModelOpen(content, item) {
    this.discountFormSubmited = false;
    this.discountItem = item;
    if ((item.DiscountPercentage > 0) || (item.Discount > 0)) {
      this.discountComment = '';
      this.modalReference = this.modalService.open(content, { width: '400' })
    }
  }

  submitDiscountComment() {
    this.discountFormSubmited = true;
    if (this.discountForm.valid) {
      this.discountFormSubmited = false;
      this.discountItem.Comment = this.discountForm.value.discountComment;
      this.calcTotalShopItem();
      this.modalReference.close();
      this.discountForm.reset();
    } else {
      UsErrorService.validateAllFormFields(this.discountForm, this.discountFormErrors, this.discountFormValidationMsgs)
    }
  }

  closeDiscountModel() {
    if (!this.discountComment) {
      this.discountItem.DiscountPercentage = 0;
      this.discountItem.Discount = 0;
    }
    this.shopHomeService.ShopItems = this.shopItems;
    this.modalReference.close();
  }

  discountChanged(item, event, isPercentage, content) {
    this.discountItem = item;
    if (isPercentage) {
      this.discountItem.Discount = (this.discountItem.DefaultPrice * Number(event)) / 100;
      this.discountItem.DiscountPercentage = Number(event);
    } else {
      this.discountItem.DiscountPercentage = (Number(event) / this.discountItem.DefaultPrice) * 100;
      this.discountItem.Discount = Number(event);
    }
    this.shopHomeService.ShopItems = this.shopItems;
    this.discountFormSubmited = false;
    if ((Number(item.DiscountPercentage) > 0) || (Number(item.Discount) > 0)) {
      this.discountComment = '';
      this.modalReference = this.modalService.open(content, { width: '400' })
    }
    this.calculatePrice(this.discountItem);
    this.calcTotalShopItem();
  }

  OrderQuantityChanged(item, event) {
    const findItem = this.shopItems.find(shopItem => shopItem.Id === item.Id);
    if (findItem) {
      findItem.OrderQuantity = Number(event);
      this.calculatePrice(findItem);
      this.calcTotalShopItem();
      this.shopHomeService.ShopItems = this.shopItems;
    }
  }

  changeModel(inputDP) {
    setTimeout(() => {
      inputDP.selectionEnd = this.curserPosition;
    }, 100);
  }

  onUnitPriceChanged(saleItem, event) {
    const item = this.shopItems.find(X => X.Id === saleItem.Id);
    if (item) {
      item.DefaultPrice = Number(event);
      this.calculatePrice(item);
      this.calcTotalShopItem();
      this.shopHomeService.ShopItems = this.shopItems;
    }
  }

  ngOnDestroy(): void {
    if (this.campaignEvent) {
        this.campaignEvent.unsubscribe();
    }
    this.selectedShopItemSubscribe.unsubscribe();
    this.selectedShopItemSubscribe.complete();
    this.clearShopItemSubscribe.unsubscribe();
    this.clearShopItemSubscribe.complete();
    this.messageBoxOkSubscribe.unsubscribe();
    this.messageBoxOkSubscribe.complete();
    if (this.returnItemCommentModel) {
      this.returnItemCommentModel.close();
    }
    if (this.entitySelectionModel) {
      this.entitySelectionModel.close();
    }
    if (this.modalReference) {
      this.modalReference.close();
    }
    if (this.selectedMemberSubscribe) {
      this.selectedMemberSubscribe.unsubscribe();
    }
    // clearItems
    this.shopItems = [];
    this.saleItemTotal = 0;
    this.shopHomeService.ShopItems = [];
    this.shopHomeService.shopItemReturn(false);
    this.isEmployee = false;
    this.shopHomeService.$priceType = 'MEMBER';
  }

  itemReturn() {
    if (this.shopHomeService.$paymentAdded) {
      alert("Gjennomfør kjøp eller begynn på nytt")
      return;
    }
    this.isItemReturn = !this.isItemReturn;
    this.shopHomeService.shopItemReturn(this.isItemReturn);
    // if (this.isItemReturn) {
    //   this.shopItems.forEach(shopItem => {
    //     if (!(Number(shopItem.UnitPrice) < 0)) {
    //       shopItem.UnitPrice = -shopItem.UnitPrice;
    //     }
    //     if (!(Number(shopItem.Price) < 0)) {
    //       shopItem.Price = -shopItem.Price;
    //     }
    //     if (!(Number(shopItem.OrderQuantity) < 0)) {
    //       shopItem.OrderQuantity = -shopItem.OrderQuantity;
    //     }
    //     shopItem.isReturn = true;
    //   })
    // } else {
    //   this.shopItems.forEach(shopItem => {
    //     if (Number(shopItem.UnitPrice) < 0) {
    //       shopItem.UnitPrice = -shopItem.UnitPrice;
    //     }
    //     if (Number(shopItem.Price) < 0) {
    //       shopItem.Price = -shopItem.Price;
    //     }
    //     if (Number(shopItem.OrderQuantity) < 0) {
    //       shopItem.OrderQuantity = -shopItem.OrderQuantity;
    //     }
    //     shopItem.isReturn = false;
    //   })
    // }
    // this.calcTotalShopItem();
    this.shopHomeService.clearShopItems();
  }

  addReturnItemComment() {
    this.returnItemFormSubmited = true;
    this.newShopItem.ReturnComment = this.returnItemForm.value.returnItemComment;
    this.newShopItem.isReturn = true;
    if (this.isItemReturn) {
      if (!(Number(this.newShopItem.UnitPrice) < 0)) {
        this.newShopItem.UnitPrice = -this.newShopItem.UnitPrice;
      }
      if (!(Number(this.newShopItem.Price) < 0)) {
        this.newShopItem.Price = -this.newShopItem.Price;
      }
      if (!(Number(this.newShopItem.OrderQuantity) < 0)) {
        this.newShopItem.OrderQuantity = -this.newShopItem.OrderQuantity;
      }
    } else {

      if (Number(this.newShopItem.UnitPrice) < 0) {
        this.newShopItem.UnitPrice = -this.newShopItem.UnitPrice;
      }
      if (Number(this.newShopItem.Price) < 0) {
        this.newShopItem.Price = -this.newShopItem.Price;
      }
      if (Number(this.newShopItem.OrderQuantity) < 0) {
        this.newShopItem.OrderQuantity = -this.newShopItem.OrderQuantity;
      }

    }
    this.items.push(this.newShopItem);
    this.shopHomeService.ShopItems = this.items;
    this.shopItems = [];
    this.shopItems = this.items;
    this.calcTotalShopItem();
    this.returnItemFormSubmited = false;
    this.returnItemForm.reset();
    // this.returnItemCommentModel.close();
  }

  retunItemCancel() {
    this.returnItemForm.reset();
    this.returnItemCommentModel.close();
    this.returnItemFormSubmited = false;
  }

  calculatePrice(item) {
    if (item.DiscountPercentage > 0) {
      item.Price = item.OrderQuantity * item.DefaultPrice - (item.DiscountPercentage * item.OrderQuantity * item.DefaultPrice / 100);
    } else if (item.Discount > 0) {
      item.Price = (item.DefaultPrice - item.Discount) * item.OrderQuantity;
    } else {
      item.Price = item.DefaultPrice * item.OrderQuantity;
    }
  }

  private calcTotalShopItem() {
    // this.saleItemTotal = Number(this.shopItems.reduce((a, b) => a + b.Price
    //   , 0));
    if (this.isEmployee) {
      let totalSales = 0;
      this.shopItems.forEach(item => {
        if (item.EmployeePrice > 0) {
          totalSales = totalSales + (item.EmployeePrice * item.OrderQuantity);
          // item.DefaultPrice = item.EmployeePrice;
        } else {
          totalSales = totalSales + (item.Price * item.OrderQuantity);
          // item.DefaultPrice = item.Price;
        }
      });
      this.saleItemTotal = totalSales;
    } else {
      this.saleItemTotal = Number(this.shopItems.reduce((a, b) => a + b.Price
      , 0));
    }
    this.shopHomeService.$totalPrice = this.saleItemTotal;
    this.shopHomeService.totalPriceChangeEvent.next(this.saleItemTotal)
  }

  removeCampaign(item) {
    this.addedCampaignList = this.addedCampaignList.filter(i => i.Id !== item.Id);
    if (!this.addedCampaignList || this.addedCampaignList.length === 0) {
      this.isShopItemDisabled = false;
      this.shopItems.forEach(element => {
        element.isPromoAdded = false;
      });
    }
  }

  onKeyDown(event) {
    this.curserPosition = event.target.selectionEnd;
  }

  searchDeatail(val: any) {
    switch (val.entitySelectionType) {
      case CategoryTypes[CategoryTypes.MEMBER]:
        PreloaderService.showPreLoader();
        this.memberService.getMembers(val.branchSelected.BranchId, val.searchVal, val.statusSelected.id,
          'SHOP', val.roleSelected.id, val.hit, false,
          false, ''
        ).subscribe
          (result => {
            PreloaderService.hidePreLoader();
            if (result) {
              this.entityItems = result.Data;
              this.itemCount = result.Data.length;
            } else {
            }
          }, err => {
            PreloaderService.hidePreLoader();
          });
        break;
      case CategoryTypes[CategoryTypes.EMPLOYEE]:
        PreloaderService.showPreLoader();
        this.shopService.GetGymEmployees({ searchText: val.searchVal, isActive: true })
          .subscribe
          (result => {
            this.entityItems = result.Data;
            this.itemCount = result.Data.length;
            PreloaderService.hidePreLoader();
          }, err => {
            PreloaderService.hidePreLoader();
          });
        break;
    }
  }

  loadShopAccount(memberId: Number, entityType: string) {
    // this.checkEmployeeByEntNo
    this.shopService.GetMemberShopAccountsForEntityType({ memberId: memberId, entityType: entityType }).subscribe(
      res => {
        this.shopHomeService.$shopAccount = res.Data;
        // this.router.navigate(['shop/shop-payment']);
        //  this.loadShopHome = false;
      });
  }

  closeMemberSelection() {
    this.entitySelectionModel.close();
  }

  selecteMember(item) {
    if (this.isEmployee) {
      this.shopHomeService.$priceType = 'EMP';
    } else {
      this.shopHomeService.$priceType = 'MEMBER';
    }
    const member = item.data;
    member.Id = member.ID;
    member.Name = member.FirstName + ' ' + member.LastName;
    this.shopHomeService.$selectedMember = item.data;
    this.shopHomeService.$isCashCustomerSelected = false;
    if (this.isSaveShopItems) {
      // TODO implement generating invoice methos
      if (this.entitySelectionModel) {
        this.entitySelectionModel.close();
      }
    } else {
      if (this.shopHomeService.$priceType === CategoryTypes[CategoryTypes.EMPLOYEE]) {
        this.loadShopAccount(member.Id, 'EMP');
      } else if (this.shopHomeService.$priceType === CategoryTypes[CategoryTypes.MEMBER]) {
        this.loadShopAccount(member.Id, 'MEM');
      }
    }
    this.shopHomeService.memberSelectedEvent.next(member);
    this.shopHomeService.isSelectedMemberEvent.next(true);
    this.entitySelectionModel.close();
  }

  closeEntitySelection() {
    if (this.CustId === '' && this.Name === '' && this.isEmployee) {
      this.isEmployee = false;
      this.shopHomeService.clearShopItems();
    }
    this.entitySelectionModel.close();
  }

  openMemberModal(content, type) {
    this.memberListGetType = type;
    if (type === 'EMPLOYEE') {
      this.isEmployee = this.isEmployee ? false : true;
      if (this.isEmployee) {
        this.columnsToShowMemberModal = ['CustId', 'LastName', 'FirstName'];
        this.entitySelectionModel = this.modalService.open(content, {width: '600'} );
        this.shopHomeService.clearShopItems();
      }
    } else {
      this.columnsToShowMemberModal = [];
      this.entitySelectionModel = this.modalService.open(content, {width: '1200'} );
    }
    // this.memberListGetType = type;
  }
}
