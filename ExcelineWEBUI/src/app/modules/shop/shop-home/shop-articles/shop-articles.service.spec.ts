import { TestBed, inject } from '@angular/core/testing';

import { ShopArticlesService } from './shop-articles.service';

describe('ShopArticlesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ShopArticlesService]
    });
  });

  it('should be created', inject([ShopArticlesService], (service: ShopArticlesService) => {
    expect(service).toBeTruthy();
  }));
});
