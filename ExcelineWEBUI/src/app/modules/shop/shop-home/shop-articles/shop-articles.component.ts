import { UsErrorService } from '../../../../shared/directives/us-error/us-error.service';
import { ShopHomeService } from '../shop-home.service';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
import { ExceShopService } from '../../services/exce-shop.service';
import { CategoryTypes } from '../../../../shared/enums/category-types';
import { ShopArticlesService } from './shop-articles.service';
import { FormGroup, FormBuilder, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { PreloaderService } from '../../../../shared/services/preloader.service';

import { IMyDpOptions } from '../../../../shared/components/us-date-picker/interfaces';
import { TabContentComponent } from 'app/shared/components/us-tab/tab-content/tab-content.component';
import { UsTabComponent } from 'app/shared/components/us-tab/us-tab.component';
const now = new Date();
const seqId = 'GIFTV';
const subSeqId = 'GIFTVNO';

@Component({
  selector: 'shop-articles',
  templateUrl: './shop-articles.component.html',
  styleUrls: ['./shop-articles.component.scss']
})
export class ShopArticlesComponent implements OnInit, OnDestroy {

  textValue = '';
  articleCategoryList: any[];
  activityCategoryList: any[];
  articleCategory = 'ALL';
  articleType = 'ALL';
  activityCategory = 'ALL';
  filteringName = '';
  isDisableActivityChange = false;
  searchText: string;
  searchVal: string;
  giftVoucherArticle: any;

  articleSearchForm: FormGroup;
  addGiftVoucherForm: FormGroup;

  nextGiftVoucherNumber: Number;
  articleNumber: Number;
  articleName: Number;
  articleId: Number;
  voucherModalReference: any;

  VoucherNotEditable =  false;

  giftFormSubmited: boolean;

  auotocompleteItems = [
    { id: 'ArticleNo', name: 'Article No :', isNumber: true },
    { id: 'Name', name: 'Name :', isNumber: false },
    { id: 'Barcode', name: 'Barcode :', isNumber: false }
  ];

  config: any = { 'placeholder': '', 'sourceField': ['searchText'] };
  fromDatePickerOptions: IMyDpOptions = {
    disableUntil: { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() + 1 },
  };

  giftVErrors = {
    'Price': '',
    'PurchasedPrice': ''
  };

  validationMessages = {
    'Price': {
      'priceRequired': 'COMMON.VoucherPriceRequired'
    },
    'PurchasedPrice': {
      'discountValidateExpre': 'COMMON.DiscountValidateExpre'
    }
  };

  @ViewChild('autobox') public autocomplete;
  @ViewChild('articleList') public TabContentComponent: TabContentComponent;
  @ViewChild('tabController') public tabController: UsTabComponent;
  clearShopItemSubscribe: any;
  clearSearchEvent: any;
  tabSub: any;

  constructor(
    private shopService: ExceShopService,
    private modalService: UsbModal,
    private shopArticlesService: ShopArticlesService,
    private fb: FormBuilder,
    private shopHomeService: ShopHomeService
  ) {
    this.giftFormSubmited = false;
    this.articleSearchForm = fb.group({
      'searchVal': [this.searchText]
    });

    // this.textValue = 'Barcode : ';

    this.addGiftVoucherForm = fb.group({
      'VoucherNumber': [null],
      'Price': [null, [], [this.priceRequired.bind(this)]],
      'PurchasedPrice': [null, [], [this.discountValidateExpre.bind(this)]],
      'ExpiryDate': [null],
    });

    this.shopArticlesService.$isCustomerPrice = true;
    this.addGiftVoucherForm.statusChanges.subscribe(_ => {
      UsErrorService.onValueChanged(this.addGiftVoucherForm, this.giftVErrors, this.validationMessages)
    });
  }

  ngOnInit() {
    // this.autocomplete.setSearchValue(this.auotocompleteItems);
    this.autocomplete.nativeElement.focus();
    this.shopService.GetCategories({ type: CategoryTypes[CategoryTypes.ACTIVITY] }).subscribe(
      categories => {
        this.activityCategoryList = categories.Data;
      }
    );

    this.shopService.GetCategories({ type: CategoryTypes[CategoryTypes.ITEM] }).subscribe(
      itemCategories => {
        this.shopService.GetCategories({ type: CategoryTypes[CategoryTypes.SERVICE] }).subscribe(
          serviceCategories => {
            this.articleCategoryList = itemCategories.Data.concat(serviceCategories.Data);
          });
      }
    );

    this.shopHomeService.VoucherNotEditableEvent.subscribe( VouchVal => {
      this.VoucherNotEditable = VouchVal
    })

    this.clearShopItemSubscribe = this.shopHomeService.clearShopItemsEvent.subscribe(
      clear => {
        this.clearSearch();
      });

    this.clearSearchEvent = this.shopHomeService.clearSearchEvent.subscribe(
      clearSearchField => {
        this.clearSearch();
      }
    )

    this.tabSub = this.shopHomeService.changeTabEvent.subscribe( event => {
      // ** this one changing the tabs **
      this.tabController.selectTab(this.TabContentComponent, 1)
    })
  }

  ngOnDestroy() {
    if (this.voucherModalReference) {
      this.voucherModalReference.close();
    }
    if (this.clearShopItemSubscribe) {
      this.clearShopItemSubscribe.unsubscribe();
    }
    if (this.tabSub) {
      this.tabSub.unsubscribe();
    }
    if (this.clearSearchEvent) {
      this.clearSearchEvent.unsubscribe();
    }
  }

  VoucherPriceChanged(event) {
    if (this.VoucherNotEditable) {
      this.addGiftVoucherForm.patchValue({
        'PurchasedPrice': event
      })
    }
  }

  openGiftVoucherModel(content) {
    this.giftFormSubmited = false;
    const today = new Date();
    today.setMonth(today.getMonth() + 12);
    PreloaderService.showPreLoader();
    this.shopService.GetNextGiftVoucherNumber({ seqId: seqId, subSeqId: subSeqId }).subscribe(
      data => {
        this.nextGiftVoucherNumber = data.Data;
        this.addGiftVoucherForm.patchValue({
          'VoucherNumber': this.nextGiftVoucherNumber,
          'Price': 0.00,
          'PurchasedPrice': 0.00,
          'ExpiryDate': {
            date: {
              year: today.getFullYear(),
              month: today.getMonth() + 1,
              day: today.getDate()
            }
          }
        });
        this.addGiftVoucherForm.get('VoucherNumber').disable();
        this.shopService.GetGiftVoucherDetail().subscribe(
          detail => {
            this.articleNumber = detail.Data.ArticleID;
            this.articleName = detail.Data.ArticleName;
            this.articleId = detail.Data.ArticleNo;
            this.voucherModalReference = this.modalService.open(content, { width: '300' });
            PreloaderService.hidePreLoader();
          },
          error => {
            PreloaderService.hidePreLoader();
          }
        )
      },
      error => {
        PreloaderService.hidePreLoader();
      }
    );

  }

  onArticleTypeChange(value) {
    this.isDisableActivityChange = false;
    this.shopArticlesService.$articlelType = value;
    this.activityCategory = 'ALL';
    if (value === 'ITEM') {
      this.isDisableActivityChange = true;
    }
  }

  onArtleCategoryChange(value) {
    this.shopArticlesService.$artleCategory = value;
  }

  onActivityChange(value) {
    this.shopArticlesService.$activity = value;
  }

  onPriceTypeChange(value) {
    this.shopArticlesService.$isCustomerPrice = (value === '0') ? true : false;
    if (this.shopArticlesService.$isCustomerPrice) {
      this.shopHomeService.$priceType = CategoryTypes[CategoryTypes.MEMBER];
    } else {
      this.shopHomeService.$priceType = CategoryTypes[CategoryTypes.EMPLOYEE];
    }
  }

  filterByName(value) {
    this.shopArticlesService.$articleName = value;
  }

  filterByVendor(value) {
    this.shopArticlesService.$vendorName = value;
  }

  onSelectSearchItem(item: any, value: any) {
    this.shopArticlesService.$searchText = item.name + item.searchVal;

    // ** this one changing the tabs **
    // this.tabController.selectTab(this.TabContentComponent, 1)


    // this.shopArticlesService.$searchText = 'Barcode : ' + item.searchText;
    // this.clearSearch();


  }

  searchArticles(value: any) {
    this.shopArticlesService.$searchText = value;
  }

  getSearchText(searchName: string) {
    const searchArray = searchName.split(':');
    if (searchArray.length > 1) {
      return this.auotocompleteItems.find(x => x.name.split(':')[0].trim() === searchArray[0].trim()).id + ' :' + searchArray[1];
    }
    return searchName;
  }

  clearSearch() {
    this.searchVal = '';
    this.searchText = '';
    this.autocomplete.nativeElement.value = '';
    this.shopArticlesService.$searchText = this.searchText;
    this.autocomplete.nativeElement.focus();
  }

  addGiftVoucher(value: any) {
    this.giftFormSubmited = true;

    if (this.addGiftVoucherForm.valid) {
      this.giftVoucherArticle = value;
      this.giftVoucherArticle.VoucherNumber = this.nextGiftVoucherNumber;
      this.giftVoucherArticle.Category = 'Gavekort';
      this.giftVoucherArticle.ArticleNo = this.articleNumber;
      this.giftVoucherArticle.Id = Number(this.articleId);
      this.giftVoucherArticle.Description = this.articleName;
      this.giftVoucherArticle.VatRate = 0;
      this.giftVoucherArticle.IsVoucher = true;
      this.giftVoucherArticle.DefaultPrice = Number(this.giftVoucherArticle.Price);
      this.giftVoucherArticle.Price = Number(this.giftVoucherArticle.PurchasedPrice);
      this.giftVoucherArticle.PurchasedPrice = Number(this.giftVoucherArticle.PurchasedPrice);
      this.giftVoucherArticle.SelectedPrice = Number(this.giftVoucherArticle.PurchasedPrice);
      this.giftVoucherArticle.EmployeePrice = Number(this.giftVoucherArticle.DefaultPrice);
      this.giftVoucherArticle.Discount = (Number(this.giftVoucherArticle.DefaultPrice) - Number(this.giftVoucherArticle.PurchasedPrice));
      this.giftVoucherArticle.DiscountPercentage = (((Number(this.giftVoucherArticle.DefaultPrice) - Number(this.giftVoucherArticle.PurchasedPrice)) /
        Number(this.giftVoucherArticle.DefaultPrice)) * 100);
      this.shopHomeService.addShopItem(this.giftVoucherArticle);
      this.voucherModalReference.close();
    } else {
      UsErrorService.validateAllFormFields(this.addGiftVoucherForm, this.giftVErrors, this.validationMessages);
    }
  }

  discountValidateExpre(control: AbstractControl) {
    return new Promise(resolve => {
      setTimeout(() => {
        if (Number(this.addGiftVoucherForm.value.Price) < Number(control.value)) {
          resolve({
            'discountValidateExpre': true
          })
        } else {
          resolve(null);
        }
      }, 10);
    });
  }

  // hvis prisen er mindre eller lik 0, blir 'priceRequired' satt til true
  priceRequired(control: AbstractControl) {
    return new Promise(resolve => {
      setTimeout(() => {
        if (Number(this.addGiftVoucherForm.value.Price) <= 0) {
          resolve({
            'priceRequired': true
          })
        } else {
          resolve(null);
        }
      }, 10);
    });
  }


}
