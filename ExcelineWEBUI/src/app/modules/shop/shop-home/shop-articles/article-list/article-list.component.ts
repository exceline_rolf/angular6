import { ExceToolbarService } from '../../../../common/exce-toolbar/exce-toolbar.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ExceShopService } from '../../../services/exce-shop.service';
import { DataTableResource } from '../../../../../shared/components/us-data-table/tools/data-table-resource';
import { ShopArticlesService } from '../shop-articles.service';
import { ShopHomeService } from '../../shop-home.service';
import { PreloaderService } from '../../../../../shared/services/preloader.service';
import { ExceMessageService } from '../../../../../shared/components/exce-message/exce-message.service';

@Component({
  selector: 'app-article-list',
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.scss']
})
export class ArticleListComponent implements OnInit, OnDestroy {

  articleList: any[];
  articleListBase: any[];
  itemCount = 0;
  itemResource?: DataTableResource<any>;
  isCustomerPrice: Boolean;

  articleType = 'ALL';
  articleCategory = 'ALL';
  activity = 'ALL';

  filterSubscription: any;
  priceTypeSubscription: any;
  searchByKeywordSubscription: any;

  locale: string;
  articleRefreshSubscribe: any;
  gridApi: any;
  articleDefs: any;

  names = {
    ArticleNo: 'Artikkelnummer',
    Description: 'Navn',
    Category: 'Kategori',
    StockLevel: 'Antall tilgjengelig',
    DefaultPrice: 'Pris',
    EmployeePrice: 'Ansattpris',
  }

  colNamesNo = {

  }

  colNamesEn = {

  }


  constructor(
    private shopService: ExceShopService,
    private shopArticlesService: ShopArticlesService,
    private shopHomeService: ShopHomeService,
    private toolbarService: ExceToolbarService,
    private exceMessageService: ExceMessageService,
  ) {
    this.isCustomerPrice = true;
    this.filterSubscription = this.shopArticlesService.filterArticles.subscribe(
      params => {
        if (this.articleListBase) {
          this.articleList = this.articleListBase;

          this.articleType = params.articlelType;
          this.articleCategory = params.artleCategory;
          this.activity = params.activity;

          if (this.articleType === 'ALL') {
            if ((this.articleCategory === 'ALL') && (this.activity === 'ALL')) {
              this.articleList = this.articleListBase;
            }
            if ((this.articleCategory !== 'ALL') && (this.activity === 'ALL')) {
              this.articleList = this.articleListBase.filter(
                item => ((item.CategoryCode === this.articleCategory)));
            }
            if ((this.articleCategory === 'ALL') && (this.activity !== 'ALL')) {
              this.articleList = this.articleListBase.filter(
                item => ((item.ActivityName === this.activity)));
            }
            if ((this.articleCategory !== 'ALL') && (this.activity !== 'ALL')) {
              this.articleList = this.articleListBase.filter(
                item => ((item.CategoryCode === this.articleCategory) && (item.ActivityName === this.activity)));
            }

          } else {
            if ((this.articleCategory === 'ALL') && (this.activity === 'ALL')) {
              this.articleList = this.articleListBase.filter(
                item => (item.ArticleType === this.articleType));
            }
            if ((this.articleCategory !== 'ALL') && (this.activity === 'ALL')) {
              this.articleList = this.articleListBase.filter(
                item => ((item.ArticleType === this.articleType) && (item.CategoryCode === this.articleCategory)));
            }
            if ((this.articleCategory === 'ALL') && (this.activity !== 'ALL')) {
              this.articleList = this.articleListBase.filter(
                item => ((item.ArticleType === this.articleType) && (item.ActivityName === this.activity)));
            }
            if ((this.articleCategory !== 'ALL') && (this.activity !== 'ALL')) {
              this.articleList = this.articleListBase.filter(
                item => ((item.ArticleType === this.articleType) && (item.CategoryCode === this.articleCategory) && (item.ActivityName === this.activity)));
            }
          }
          if (params.articleName) {
            this.articleList = this.articleList.filter(o =>
              o.Description.toLowerCase().includes(params.articleName.toLowerCase())
            );
          }

          if (params.vendorName) {
            this.articleList = this.articleList.filter(o =>
              o.VendorName.toLowerCase().includes(params.vendorName.toLowerCase())
            );
          }
        }
      }
    );

    this.priceTypeSubscription = this.shopArticlesService.priceTypeChange.subscribe(
      isCustomerPrice => {
        if (typeof isCustomerPrice !== 'undefined') {
          this.isCustomerPrice = isCustomerPrice;
        }
      }
    )

    this.searchByKeywordSubscription = this.shopArticlesService.searchByKeyword.subscribe(
      keyword => {
        this.articleList = [];
        PreloaderService.showPreLoader();
        if (!keyword || keyword.length === 0) {
          this.shopService.GetArticles({ articleType: 'ITEM', keyWord: '', isActive: true, isFilterByBranch: true }).subscribe(
            result => {
            this.shopService.GetArticles({ articleType: 'SERVICE', keyWord: '', isActive: true, isFilterByBranch: true }).subscribe(
              res => {
              this.articleList = result.Data.concat(res.Data);
              if (this.shopHomeService.$shopMode === 'BOOKING' && this.shopHomeService.$activityId > 0) {
                this.articleList = this.articleList.filter(x => x.ActivityId === this.shopHomeService.$activityId && x.CategoryCode === 'BOOKING');
              }
              this.itemCount = this.articleList.length;
              this.itemResource = new DataTableResource(this.articleList);
              this.itemResource.count().then(count => this.itemCount = count);
              this.shopArticlesService.setArticleLists(this.articleList);
              this.articleListBase = this.articleList;
              PreloaderService.hidePreLoader();
            },
              error => {
            //    PreloaderService.hidePreLoader();
              });
          },
            error => {
           //   PreloaderService.hidePreLoader();
            });
        } else {
          this.shopService.GetArticlesSearch(keyword).subscribe(result => {
          // this.shopService.GetArticles({ articleType: 'SERVICE', keyWord: keyword, isActive: true, isFilterByBranch: true }).subscribe(res => {
            this.articleList = result.Data;
            this.itemCount = this.articleList.length;
            this.itemResource = new DataTableResource(this.articleList);
            this.itemResource.count().then(count => this.itemCount = count);
            this.shopArticlesService.setArticleLists(this.articleList);
            this.articleListBase = this.articleList;
            if (this.itemCount === 1) {
              this.shopHomeService.addShopItem(this.articleList[0]);
            } else if (this.itemCount > 1 && keyword.length > 0) {
              this.shopHomeService.changeTabEvent.next(true);
            }
            PreloaderService.hidePreLoader();
          },
            error => {
              PreloaderService.hidePreLoader();
            });

          }
      }
    )
  }

  ngOnInit() {
     // **Language selecton and subscription - dateformat and amountconverter uses this **
     const language = this.toolbarService.getSelectedLanguage();
     this.locale = language.culture;
     // this._dateFormat = 'DD.MM.YYYY';

     this.toolbarService.langUpdated.subscribe(
       (lang) => {
         if (lang) {
           this.locale = lang.culture;
         }
       }
     );
     // **END**

    if (this.shopHomeService.$shopMode !== 'INVOICE') {
      this.articleList = [];
   //   PreloaderService.showPreLoader();
      this.shopService.GetArticles({ articleType: 'ITEM', keyWord: '', isActive: true, isFilterByBranch: true }).subscribe(
        result => {
        this.shopService.GetArticles({ articleType: 'SERVICE', keyWord: '', isActive: true, isFilterByBranch: true }).subscribe(
          res => {
          this.articleList = result.Data.concat(res.Data);
          if (this.shopHomeService.$shopMode === 'BOOKING' && this.shopHomeService.$activityId > 0) {
            this.articleList = this.articleList.filter(x => x.ActivityId === this.shopHomeService.$activityId && x.CategoryCode === 'BOOKING');
          }
          this.itemCount = this.articleList.length;
          this.itemResource = new DataTableResource(this.articleList);
          this.itemResource.count().then(count => this.itemCount = count);
          this.shopArticlesService.setArticleLists(this.articleList);
          this.articleListBase = this.articleList;
   //       PreloaderService.hidePreLoader();
        },
          error => {
        //    PreloaderService.hidePreLoader();
          });
      },
        error => {
       //   PreloaderService.hidePreLoader();
        });
    }
    this.articleRefreshSubscribe =  this.shopService.refreshArticleListEvent.subscribe(
      (value) => {
        if (value === true) {
          if (this.shopHomeService.$shopMode !== 'INVOICE') {
            this.articleList = [];
           // PreloaderService.showPreLoader();
            this.shopService.GetArticles({ articleType: 'ITEM', keyWord: '', isActive: true, isFilterByBranch: true }).subscribe(
              result => {
              this.shopService.GetArticles({ articleType: 'SERVICE', keyWord: '', isActive: true, isFilterByBranch: true }).subscribe(
                res => {
                this.articleList = result.Data.concat(res.Data);
                if (this.shopHomeService.$shopMode === 'BOOKING' && this.shopHomeService.$activityId > 0) {
                  this.articleList = this.articleList.filter(x => x.ActivityId === this.shopHomeService.$activityId && x.CategoryCode === 'BOOKING');
                }
                this.itemCount = this.articleList.length;
                this.itemResource = new DataTableResource(this.articleList);
                this.itemResource.count().then(count => this.itemCount = count);
                this.shopArticlesService.setArticleLists(this.articleList);
                this.articleListBase = this.articleList;
            //    PreloaderService.hidePreLoader();
              },
                error => {
            //      PreloaderService.hidePreLoader();
                });
            },
              error => {
             //   PreloaderService.hidePreLoader();
              });
          }
        }
      }
    )

    this.articleDefs = [
      {headerName: this.names.ArticleNo, field: 'ArticleNo', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
        filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true},
      {headerName: this.names.Description, field: 'Description', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true},
      {headerName: this.names.Category, field: 'Category', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
      sortAscending: '<i class="icon-sort-ascending"/>',
      sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true},
      {headerName: this.names.StockLevel, field: 'StockLevel', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
      sortAscending: '<i class="icon-sort-ascending"/>',
      sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true},
      {headerName: this.names.DefaultPrice, field: 'DefaultPrice', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
      sortAscending: '<i class="icon-sort-ascending"/>',
      sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true},
      {headerName: this.names.EmployeePrice, field: 'EmployeePrice', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
      sortAscending: '<i class="icon-sort-ascending"/>',
      sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true},
    ]
  }

  onGridReady(params) {
    this.gridApi = params.api;
    // this.gridColumnApi = params.columnApi;
    // auto fits columns to fit screen
    this.gridApi.sizeColumnsToFit();
    // hiding loading message on load
    this.gridApi.gridOptionsWrapper.gridOptions.api.hideOverlay();
  }

  articleListRowDoubleClick(rowEvent) {
    if (this.shopHomeService.$paymentAdded === true) {
      this.exceMessageService.openMessageBox('ERROR',
          {
            messageTitle: 'COMMON.PaymentError',
            messageBody: 'COMMON.FinsihPurchase'
          });
    } else {
      this.shopHomeService.addShopItem(rowEvent.data);
    }
  }

  ngOnDestroy(): void {
    this.filterSubscription.unsubscribe();
    this.priceTypeSubscription.unsubscribe();
    this.searchByKeywordSubscription.unsubscribe();

  }

}
