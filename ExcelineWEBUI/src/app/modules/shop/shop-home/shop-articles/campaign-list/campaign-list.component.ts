import { ShopHomeService } from '../../shop-home.service';
import { ExceToolbarService } from '../../../../common/exce-toolbar/exce-toolbar.service';
import { ExceShopService } from '../../../services/exce-shop.service';
import { Component, OnInit } from '@angular/core';
import { DataTableResource } from 'app/shared/components/us-data-table/tools/data-table-resource';
import { DiscountType } from 'app/shared/enums/discount-type';
const today = new Date();

@Component({
  selector: 'app-campaign-list',
  templateUrl: './campaign-list.component.html',
  styleUrls: ['./campaign-list.component.scss']
})
export class CampaignListComponent implements OnInit {
  campaignItems: any = [];
  itemResource = new DataTableResource(this.campaignItems);
  itemCount = 0;
  campArticleList: any[];
  listOfItemsView: any;
  locale: string;

  constructor(
    private shopService: ExceShopService,
    private toolbarService: ExceToolbarService,
    private shopHomeService: ShopHomeService
  ) {  }

  ngOnInit() {
    // **Language selecton and subscription - dateformat and amountconverter uses this **
    const language = this.toolbarService.getSelectedLanguage();
    this.locale = language.culture;
    // this._dateFormat = 'DD.MM.YYYY';

    this.toolbarService.langUpdated.subscribe(
      (lang) => {
        if (lang) {
          this.locale = lang.culture;
        }
      }
    );
    // **END**
    today.setHours(0, 0, 0, 0);

    this.shopService.GetDiscountList({ discountType: DiscountType.SHOP }).subscribe(
      res => {
        this.campaignItems = res.Data.filter(item => {
          const startDate = new Date(item.StartDate);
          const endDate = new Date(item.EndDate);
          startDate.setHours(0, 0, 0, 0)
          endDate.setHours(0, 0, 0, 0)
          return (endDate >= today && startDate <= today)
        });

        this.itemCount = this.campaignItems.length;
        this.itemResource = new DataTableResource(this.campaignItems);
        this.itemResource.count().then(count => this.itemCount = count);
      },
      error => {

      }
    )
  }

  campaignDoubleClick(item) {
    this.shopHomeService.$campaignItem = item;
  }

  reloadItems(params) {
    this.itemResource.query(params).then(items => this.campaignItems = items);
  }

  openPopOver(item, listOfItemsView) {
    if (this.listOfItemsView) {
      this.listOfItemsView.close();
    }
    this.listOfItemsView = listOfItemsView;
    this.campArticleList = item.ArticleList;
    listOfItemsView.open();
  }

  closePopOver() {
    this.listOfItemsView.close();
  }

}
