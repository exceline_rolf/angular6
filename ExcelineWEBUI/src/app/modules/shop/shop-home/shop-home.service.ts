import { Injectable, EventEmitter } from '@angular/core';
import { Subject } from 'rxjs';
// import value from '*.json';

@Injectable()
export class ShopHomeService {
    salePoint: any;
  private shopItems = [];
  private shopItem: any;
  private priceType: string;
  private campaignItem: any;
  private selectedMember: any;
  private shopLoginData: any;
  private totalPrice: any;
  private isCashCustomerSelected: boolean;
  private shopAccount: any;
  private isOnAccountPayment: boolean;
  private isCreditNote: boolean;
  private isBookingPayment: boolean;
  private custRoleType: string;
  private giftVoucher: any;
  private isReturn: any;
  private isCancelBtnHide: boolean;
  private isUpdateInstallments: boolean;
  private selectedOder: any;
  private selectedInvoice: any;
  private isShopLoginNeed: boolean;
  private isLogoutAfterSale: boolean;
  private isCardLogin: boolean;
  private hasUserLoggedShop: boolean;
  private isSaveShopItems: boolean;
  private shopMode: string;
  private arItemNo: number;
  private activityId: number;
  private articleId: number;
  private articleName: string;
  private isSelectedMember = false;
  private fromMcOrders: any;
  private paymentAdded: boolean;
  private IsCashSalesActivated = false;

  private VoucherNotEditable = false;

  private sessionKey: any;
  private shopOpenedFromOrders = false;
  private shopOpenedFromVisitRegistration =
  {
    StatusCode: 0,
    ArticleId: -1,
    ItemName: ''}
  private visitRegistrationChanged: EventEmitter<number> = new EventEmitter

  public isSelectedMemberEvent = new Subject<any>();
  public shopItemChanged = new Subject<any>();
  public shopItemReturnEvent = new Subject<any>();
  public clearShopItemsEvent = new Subject<any>();
  public getShopStatusEvent = new Subject<any>();
  public campaignAddEvent = new Subject<any>();
  public shopLoginSuccessEvent = new Subject<any>();
  public salePointAddedEvent =  new Subject<any>();
  public closeShopEvent = new Subject<any>();
  public memberSelectedEvent = new Subject<any>();
  public totalPriceChangeEvent = new Subject<any>();
  public changeQuantityEvent = new Subject<any>();
  public paymentAddedEvent = new Subject<any>();
  public sessionKeyGymCodeEvent = new Subject<any>();
  public savePaymentEvent = new Subject<any>();
  public savePaymentFromOrdersEvent = new Subject<any>();
  public openCashDrawerEvent = new Subject<any>();
  public isBankTerminalActiveEvent = new Subject<any>();
  public VoucherNotEditableEvent = new Subject<any>();
  public IsCashSalesActivatedEvent = new Subject<any>();
  public showMessageEvent = new Subject<any>();
  public clearSearchEvent = new Subject<any>();
  public changeTabEvent = new Subject<any>();

  isBankTerminalActive = false;



  constructor() {
    this.isUpdateInstallments = false;
  }

  get $paymentAdded(): boolean {
    return this.paymentAdded;
  }

  set $paymentAdded(value: boolean) {
    this.paymentAddedEvent.next(value);
    this.paymentAdded = value;
  }

  get $IsCashSalesActivated() {
    return this.IsCashSalesActivated;
  }

  set $IsCashSalesActivated(value: boolean) {
    this.IsCashSalesActivatedEvent.next(value)
    this.IsCashSalesActivated = value;
  }

  get $VoucherNotEditable(): any {
    return this.VoucherNotEditable;
  }

  set $VoucherNotEditable(value: any) {
    this.VoucherNotEditable = value;
    this.VoucherNotEditableEvent.next(value);
  }

  get $sessionKey(): any {
    return this.sessionKey;
  }

  set $sessionKey(value: any) {
    this.sessionKey = value;
    this.sessionKeyGymCodeEvent.next(value);
  }

  get $fromMcOrders(): boolean {
    return this.fromMcOrders;
  }

  set $fromMcOrders(value: boolean) {
    this.fromMcOrders = value;
  }

  get $isSaveShopItems(): boolean {
    return this.isSaveShopItems;
  }

  set $isSaveShopItems(value: boolean) {
    this.isSaveShopItems = value;
  }

  get $isCashCustomerSelected(): boolean {
    return this.isCashCustomerSelected;
  }

  set $isCashCustomerSelected(value: boolean) {
    this.isCashCustomerSelected = value;
  }

  get $isSelectedMember(): boolean {
    return this.isSelectedMember;
  }

  set $isSelectedMember(value: boolean) {
    this.isSelectedMemberEvent.next(value)
    this.isSelectedMember = value;
  }

  get $shopAccount(): any {
    return this.shopAccount;
  }

  set $shopAccount(value: any) {
    this.shopAccount = value;
  }

  get $totalPrice(): any {
    return this.totalPrice;
  }

  set $totalPrice(value: any) {
    this.totalPriceChangeEvent.next(value)
    this.totalPrice = value;
  }

  get $shopLoginData(): any {
    return this.shopLoginData;
  }

  set $shopLoginData(value: any) {
    this.shopLoginData = value;
  }

  get $selectedMember(): any {
    return this.selectedMember;
  }

  set $selectedMember(value: any) {
    let hasValue;
    try {
      hasValue = value.Id
    } catch (err) {
      hasValue = false;
    }
    if (hasValue) {
      this.isSelectedMember = true;
      // this.shopHomeService.$isSelectedMember = true;
      this.selectedMember = value;
      this.memberSelectedEvent.next(value)
    } else {
      this.isSelectedMember = false;
      this.selectedMember = null;
      this.memberSelectedEvent.next(false)
      // this.shopHomeService.$selectedMember = null;
    }
    // this.selectedMember = value;
  }

  get $isCreditNote(): boolean {
    return this.isCreditNote;
  }

  set $isCreditNote(value: boolean) {
    this.isCreditNote = value;
  }

  get $isBankTerminalActive(): boolean {
    return this.isBankTerminalActive;
  }

  set $isBankTerminalActive(val: boolean) {
    // this.isBankTerminalActiveEvent.next(true)
    this.isBankTerminalActive = val;
  }

  get $isBookingPayment(): boolean {
    return this.isBookingPayment;
  }

  set $isBookingPayment(value: boolean) {
    this.isBookingPayment = value;
  }

  get $giftVoucher(): any {
    return this.giftVoucher;
  }

  set $giftVoucher(value: any) {
    this.giftVoucher = value;
  }


  get $isOnAccountPayment(): boolean {
    return this.isOnAccountPayment;
  }

  set $isOnAccountPayment(value: boolean) {
    this.isOnAccountPayment = value;
  }
  get $campaignItem(): any {
    return this.campaignItem;
  }

  set $campaignItem(value: any) {
    this.campaignItem = value;
    this.campaignAddEvent.next(this.campaignItem);
  }

  get $priceType(): string {
    return this.priceType;
  }

  set $priceType(value: string) {
    this.priceType = value;
  }

  get $isReturn(): any {
    return this.isReturn;
  }

  set $isReturn(value: any) {
    this.isReturn = value;
  }

  get $isCancelBtnHide(): boolean {
    return this.isCancelBtnHide;
  }

  set $isCancelBtnHide(value: boolean) {
    this.isCancelBtnHide = value;
  }

  get $isUpdateInstallments(): boolean {
    return this.isUpdateInstallments;
  }

  set $isUpdateInstallments(value: boolean) {
    this.isUpdateInstallments = value;
  }

  get $selectedOder(): any {
    return this.selectedOder;
  }

  set $selectedOder(value: any) {
    this.selectedOder = value;
  }

  get $selectedInvoice(): any {
    return this.selectedInvoice;
  }

  set $selectedInvoice(value: any) {
    this.selectedInvoice = value;
  }

  get $shopMode(): any {
    return this.shopMode;
  }

  set $shopMode(value: any) {
    this.shopMode = value;
  }

  get $isShopLoginNeed(): boolean {
    return this.isShopLoginNeed;
  }

  set $isShopLoginNeed(value: boolean) {
    this.isShopLoginNeed = value;
  }

  get $isLogoutAfterSale(): boolean {
    return this.isLogoutAfterSale;
  }

  set $isLogoutAfterSale(value: boolean) {
    this.isLogoutAfterSale = value;
  }

  get $isCardLogin(): boolean {
    return this.isCardLogin;
  }

  set $isCardLogin(value: boolean) {
    this.isCardLogin = value;
  }

  get $hasUserLoggedShop(): boolean {
    return this.hasUserLoggedShop;
  }

  set $hasUserLoggedShop(value: boolean) {
    this.hasUserLoggedShop = value;
    this.shopLoginSuccessEvent.next(value);
  }

  set $salePointAdded(value: boolean){
      this.salePointAddedEvent.next(value);
  }

  get $arItemNo(): number {
    return this.arItemNo;
  }

  set $arItemNo(value: number) {
    this.arItemNo = value;
  }

  get $articleId(): number {
    return this.articleId;
  }

  set $articleId(value: number) {
    this.articleId = value;
  }

  get $articleName(): string {
    return this.articleName;
  }

  set $articleName(value: string) {
    this.articleName = value;
  }
  get $activityId(): number {
    return this.activityId;
  }

  set $activityId(value: number) {
    this.activityId = value;
  }

  set $salePoint(value: any){
    this.salePoint = value;
  }

  get $salePoint(): any {
    return this.salePoint;
  }

  set $shopOpenedFromOrders(value: any) {
    this.shopOpenedFromOrders = value;
  }

  get $shopOpenedFromOrders(): any {
    return this.shopOpenedFromOrders;
  }

  set $shopOpenedFromVisitRegistration(value: any) {
    this.shopOpenedFromVisitRegistration = value;
    this.visitRegistrationChanged.emit(value);
  }

  get $shopOpenedFromVisitRegistration(): any {
    return this.shopOpenedFromVisitRegistration;
  }
  visitRegistrationChangedEmitter() {
    return this.visitRegistrationChanged;
  }
  getShopItems() {
    // this.shopItems = JSON.parse(localStorage.getItem('shopItems'));
    return this.shopItems ? this.shopItems : [];
  }

  set ShopItems(shopItems) {
    this.shopItems = shopItems;
    // localStorage.setItem('shopItems', JSON.stringify(this.shopItems));
    this.getShopStatusEvent.next({ items: this.getShopItems(), priceType: this.priceType, shopLoginData: this.shopLoginData });
  }

  addShopItem(item) {
    let included = false;

    this.shopItems.forEach(shopitem => {
      if (shopitem.Id === item.Id) {
        included = true;
      }
    });

    if (included) {
      // quantity event
      this.changeQuantityEvent.next(item);
    } else {
    item.OrderQuantity = 1;
    if (!item.Discount) {
      item.Discount = 0;
    }
    if (!item.DiscountPercentage) {
      item.DiscountPercentage = 0;
    }
    this.shopItemChanged.next(item);
    this.getShopStatusEvent.next({ items: this.getShopItems(), priceType: this.priceType, shopLoginData: this.shopLoginData });
    }
    this.clearSearchEvent.next(true);
  }

  removeShopItem(items) {
    this.ShopItems = items;
    this.getShopStatusEvent.next({ items: this.getShopItems(), priceType: this.priceType, shopLoginData: this.shopLoginData });
  }

  clearShopItems() {
    this.shopItems = [];
    localStorage.setItem('shopItems', JSON.stringify(this.shopItems));
    this.clearShopItemsEvent.next({});
  }

  shopItemReturn(isReturn) {
    this.$isReturn = isReturn;
    this.shopItemReturnEvent.next(isReturn);
  }

  savePayment() {
    this.savePaymentEvent.next(true)
  }

}
