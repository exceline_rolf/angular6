import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
// import { AngularSplitModule } from 'angular-split';
import { AngularSplitModule } from 'angular-split-ng6';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { ShopRoutingModule } from './shop-routing.module';
import { ShopRoutingComponents } from './shop-routing.module';

import { ExceShopService } from './services/exce-shop.service';
import { ShopArticlesService } from './shop-home/shop-articles/shop-articles.service';
import { ShopHomeService } from './shop-home/shop-home.service';
import { ShopPaymentComponent } from './shop-home/shop-payment/shop-payment.component';
import { ShopLoginComponent } from './shop-home/shop-login/shop-login.component';
import { TranslateModule, TranslateLoader, TranslateParser } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateCompiler } from '@ngx-translate/core';
import { Observable, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { ShopHomeLayoutComponent } from './shop-home/shop-home-layout/shop-home-layout.component';
import { AgGridModule } from 'ag-grid-angular';


export function translateLoader(http: HttpClient) {

  return new MultiTranslateHttpLoader(http, [
    { prefix: './assets/i18n/', suffix: '.json' },
    { prefix: './assets/i18n/modules/shop/', suffix: '.json' },
    { prefix: './assets/i18n/modules/membership/', suffix: '.json' }
  ]);
}

export class MultiTranslateHttpLoader implements TranslateLoader {

  constructor(private http: HttpClient,
    public resources: { prefix: string, suffix: string }[] = [
      {
        prefix: '/assets/i18n/',
        suffix: '.json'
      }
    ]) { }

  /**
   * Gets the translations from the server
   * @param lang
   * @returns {any}
   */
  public getTranslation(lang: string): any {

    return forkJoin(this.resources.map(config => {
      return this.http.get(`${config.prefix}${lang}${config.suffix}`);
    })).pipe(
      map(response => {
        return response.reduce((a, b) => {
          return Object.assign(a, b);
        });
      })
    )
  }
}


@NgModule({
  imports: [
    NgbModule,
    CommonModule,
    SharedModule,
    ShopRoutingModule,
    AgGridModule.withComponents(null),
    FormsModule,
    ReactiveFormsModule,
    AngularSplitModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (translateLoader),
        deps: [HttpClient]
      }
    })
  ],
  exports: [
    ShopRoutingComponents
  ],
  declarations: [
    ShopRoutingComponents,
    ShopLoginComponent,
    ShopHomeLayoutComponent
  ],
  providers: [
    ExceShopService,
    ShopArticlesService,
    ShopHomeService
  ]
})
export class ShopModule {
}
