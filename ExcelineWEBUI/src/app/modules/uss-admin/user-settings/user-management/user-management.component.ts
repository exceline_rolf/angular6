import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { DataTableResource } from '../../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from '../../../../shared/components/us-data-table/tools/data-table-filter-pipe'
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
import { ExceMemberService } from '../../../membership/services/exce-member.service'
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { USSAdminService } from '../../services/uss-admin-service'
import { ExceLoginService } from '../../../../modules/login/exce-login/exce-login.service';
import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { ExceBreadcrumbService } from '../../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.scss']
})

export class UserManagementComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private branchId: number;
  private uspUsers: any;
  private filteredUsers: any;
  private itemResource: any = 0;
  private inactVal = false;
  private actvVal = true;
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  private modalReference: any;
  private rowItem: any;
  private isAddNew = true
  private selectedIndex = 1;

  items = [];
  itemCount = 0;

  constructor(private exceMemberService: ExceMemberService,
    private modalService: UsbModal,
    private fb: FormBuilder,
    private ussAdminService: USSAdminService,
    private exceLoginService: ExceLoginService,
    private exceMessageService: ExceMessageService,
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      (branch) => {
        this.branchId = branch.BranchId;
      });

    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ADMIN.UsersC',
      url: '/uss-admin/user-management'
    });
  }

  ngOnInit() {
    this.getUsers('', true);
  }

  getUsers(searchText?: any, isActive?: boolean) {
    this.ussAdminService.GetUspUsers(searchText, isActive).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.uspUsers = result.Data;
        this.itemResource = new DataTableResource(this.uspUsers);
        this.itemResource.count().then(count => this.itemCount = count);
        this.items = this.uspUsers;
        this.itemCount = Number(result.Data.length);
      }
    });
  }

  search(searchTxt: string) {
    if (this.actvVal) {
      this.getUsers(searchTxt, true)
    } else {
      this.getUsers(searchTxt, false)
    }
  }

  filterInput(nameValue: any) {
    this.filteredUsers = this.uspUsers
    if (nameValue && nameValue !== '') {
      this.filteredUsers = this.filterpipe.transform('UserName', 'NAMEFILTER', this.filteredUsers, nameValue);
    }
    this.itemResource = new DataTableResource(this.filteredUsers);
    this.itemResource.count().then(count => this.itemCount = count);
    this.reloadItems(this.filteredUsers);
  }

  radioValue(event) {
    if (event.target.value === 'ACTIVE') {
      this.inactVal = false;
      this.actvVal = true
    } else {
      this.inactVal = true;
      this.actvVal = false
    }
  }

  reloadItems(params): void {
    if (this.itemResource) {
      this.itemResource.query(params).then(items => this.items = items);
    }
  }

  openModel(content: any, isAddNew: any) {
    this.isAddNew = isAddNew
    this.modalReference = this.modalService.open(content);
  }

  closeForm() {
    this.modalReference.close();
  }

  rowItemLoad(event, model) {
    this.rowItem = event.row.item
    this.openModel(model, false)
  }

  viewUserDetails() {
    this.selectedIndex = 2;
  }

  saveSuccess() {
    this.getUsers('', true);
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
