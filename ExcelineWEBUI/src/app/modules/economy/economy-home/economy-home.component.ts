import { Component, OnInit } from '@angular/core';
import { ExceLoginService } from '../../login/exce-login/exce-login.service';
import { UsTileMenuService } from '../../common/us-tile-menu/us-tile-menu.service';
import { UsBreadcrumbService } from '../../../shared/components/us-breadcrumb/us-breadcrumb.service';
import { ExceBreadcrumbService } from '../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';

@Component({
  selector: 'app-economy-home',
  templateUrl: './economy-home.component.html',
  styleUrls: ['./economy-home.component.scss']
})
export class EconomyHomeComponent implements OnInit {

  selectedModule: any;
  modules: any;
  constructor(
    private exceLoginService: ExceLoginService,
    private usTileMenuService: UsTileMenuService,
    private breadcrumbService: UsBreadcrumbService,
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {
    // breadcrumbService.hideRoute('/economy/home');
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ECONOMY.EconomyC',
      url: '/economy/home'
    });
  }

  ngOnInit() {
    if (this.usTileMenuService.SelectedModule) {
      this.selectedModule = this.usTileMenuService.SelectedModule;
    } else {
      this.usTileMenuService.getUserInfo(this.exceLoginService.CurrentUser.username)
        .subscribe(result => {

          this.modules = result.Data.ModuleList;
          this.selectedModule = this.modules.find(x => x.ID === 'ExceEconomy');
          this.breadcrumbService.addFriendlyNameForRoute('/economy', this.selectedModule.DisplayName);

        },
          error => {

          });
    }


  }

}
