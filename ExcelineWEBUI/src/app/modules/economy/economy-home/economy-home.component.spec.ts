import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EconomyHomeComponent } from './economy-home.component';

describe('EconomyHomeComponent', () => {
  let component: EconomyHomeComponent;
  let fixture: ComponentFixture<EconomyHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EconomyHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EconomyHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
