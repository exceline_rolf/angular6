import { Component, OnInit } from '@angular/core';
import { ExceBreadcrumbService } from '../../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { ShopHomeService } from '../../../shop/shop-home/shop-home.service';
import { ExceLoginService } from '../../../login/exce-login/exce-login.service';
import {ExceEconomyService} from '../../services/exce-economy.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import * as moment from 'moment';
import { Extension } from 'app/shared/Utills/Extensions';


@Component({
  selector: 'app-ledger',
  templateUrl: './Ledger.component.html',
  styleUrls: ['./Ledger.component.scss']
})
export class LedgerComponent implements OnInit {

  branchId: number;
  salePointId: number;
  datalist: any;
  searchForm: FormGroup;
  startDate: any;
  endDate: any;

  constructor(
    private exceBreadcrumbService: ExceBreadcrumbService,
    private shopHomeService: ShopHomeService,
    private exceLoginService: ExceLoginService,
    private fb: FormBuilder,
    private exceEconomyService: ExceEconomyService
  ) {
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ECONOMY.LedgerC',
      url: '/economy/Ledger'
    });
  }

  fetchledger() {
  const fromDate: any = this.manipulatedate(this.searchForm.value.startDate);
  const toDate: any = this.manipulatedate(this.searchForm.value.endDate);
  this.exceEconomyService.GetTransactionData(fromDate, toDate , this.branchId).subscribe(
    result => {
      if (result && result.Data) {
        this.datalist = result.Data;
      }
    });
  }

  manipulatedate(dateInput: any) {

    if (dateInput.date !== undefined) {
      const tempDate: Date = new Date(dateInput.date.year, dateInput.date.month - 1, dateInput.date.day);
      return moment(tempDate).format('YYYY-MM-DD').toString();
    }
  }

  dateAdd(startDate: any, count: any, interval: string): Date {

    return moment(startDate).add(count, interval).toDate()
  }

  ngOnInit() {
    this.branchId =  this.exceLoginService.SelectedBranch.BranchId;
    this.searchForm = this.fb.group({
      'startDate': [null],
      'endDate': [null]
    });
    this.searchForm.patchValue({
      startDate: { date: Extension.GetFormatteredDate(this.dateAdd(new Date(), -1, 'days')) },
      endDate: { date: Extension.GetFormatteredDate(new Date()) }
    });
  }

}
