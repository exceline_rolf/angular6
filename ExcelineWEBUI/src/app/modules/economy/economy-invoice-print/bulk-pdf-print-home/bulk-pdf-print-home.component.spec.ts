import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BulkPdfPrintHomeComponent } from './bulk-pdf-print-home.component';

describe('BulkPdfPrintHomeComponent', () => {
  let component: BulkPdfPrintHomeComponent;
  let fixture: ComponentFixture<BulkPdfPrintHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BulkPdfPrintHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BulkPdfPrintHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
