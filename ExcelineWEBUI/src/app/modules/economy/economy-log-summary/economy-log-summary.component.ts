import { Component, OnInit } from '@angular/core';
import { UsbModal } from 'app/shared/components/us-modal/us-modal';
import { ExceBreadcrumbService } from '../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';

@Component({
  selector: 'app-economy-log-summary',
  templateUrl: './economy-log-summary.component.html',
  styleUrls: ['./economy-log-summary.component.scss']
})
export class EconomyLogSummaryComponent implements OnInit {
  econLogDetailModal: any;
  constructor(
    private modalService: UsbModal,
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ECONOMY.EconomyLogC',
      url: '/economy/economy-log-summary'
    });
  }


  ngOnInit() {
  }

  // openEconLogDetailModal(content: any) {
  //   this.econLogDetailModal = this.modalService.open(content);
  // }
}
