import { element } from 'protractor';
import { ExceToolbarService } from 'app/modules/common/exce-toolbar/exce-toolbar.service';
import { UsbModalRef } from './../../../shared/components/us-modal/modal-ref';
import { ClassHomeService } from './../../class/class-home/class-home.service';
import { UsbModal } from './../../../shared/components/us-modal/us-modal';
import { FormGroup } from '@angular/forms';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { FormBuilder } from '@angular/forms';
import { ExceEconomyService } from './../services/exce-economy.service';
import { Component, OnInit } from '@angular/core';
import { Extension } from '../../../shared/Utills/Extensions';
import * as moment from 'moment';
import { ExceMessageService } from '../../../shared/components/exce-message/exce-message.service';
import { error } from 'util';
import { ExceBreadcrumbService } from '../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';

@Component({
  selector: 'app-price-update',
  templateUrl: './price-update.component.html',
  styleUrls: ['./price-update.component.scss']
})
export class PriceUpdateComponent implements OnInit, OnDestroy {
  langSubscription: any;
  _dateFormat: string;
  locale: string;
  messageRef: void;
  priceUpdateContracts: any[] = [];
  ContractCount = 0
  selectedGyms: any[];
  modelRef: UsbModalRef;
  TemplateId: any;
  updateDataForm: FormGroup;
  branches: any[] = [];
  constructor(
    private economyService: ExceEconomyService,
    private modalService: UsbModal,
    private fb: FormBuilder,
    private messageService: ExceMessageService,
    private toolBarService: ExceToolbarService,
    private exceBreadcrumbService: ExceBreadcrumbService
  ) {
    exceBreadcrumbService.addBreadCumbItem.next({
      name: 'ECONOMY.PriceChangesC',
      url: '/economy/price-update'
    });
  }

  ngOnInit() {
    this.economyService.getBranches().subscribe(
      result => {
        if (result) {
          this.branches = result.Data;
        }
      }
    );

    const language = this.toolBarService.getSelectedLanguage();
    this.locale = language.culture;
    this._dateFormat = 'DD.MM.YYYY';

    this.toolBarService.langUpdated.subscribe(
      (lang) => {
        if (lang) {
          this.locale = lang.culture;
        }
      }
    );

    this.updateDataForm = this.fb.group({
      'dueDate': [null],
      'lockInperiod': [null],
      'priceGuarantee': [null],
      'changeToNextTemplate': [null],
      'minimumAmount': [null],
      'templateNo': [null],
      'templateName': [null]
    });

    this.updateDataForm.patchValue({
      dueDate: { date: Extension.GetFormatteredDate(new Date()) },
      minimumAmount: Number(100)
    })
  }

  rowsSelected(event: any) {
    if (event.selected) {
      event.item.IsSelected = true;
    } else {
      event.item.IsSelected = false;
    }
  }

  openContractTemplates(content: any) {
    this.modelRef = this.modalService.open(content);
  }

  templateSelected(event: any) {
    const template = event;
    this.modelRef.close();
    this.TemplateId = template.PackageId;

    // update the data
    this.updateDataForm.patchValue({
      templateNo: template.TemplateNumber,
      templateName: template.PackageName
    });

  }

  removeTemplate() {
    this.TemplateId = -1;
    // update the data
    this.updateDataForm.patchValue({
      templateNo: '',
      templateName: ''
    });
  }

  getContractCount() {
    this.selectedGyms = this.branches.filter(X => X.IsSelected).map(B => B.BranchId);
    if (this.selectedGyms.length > 0) {
      let priceUpdateDetails = {
        dueDate: this.manipulatedate(this.updateDataForm.value.dueDate),
        lockInPeriod: this.updateDataForm.value.lockInperiod,
        priceGuarantyPeriod: this.updateDataForm.value.priceGuarantee,
        minimumAmount: this.updateDataForm.value.minimumAmount,
        templateID: this.TemplateId,
        gyms: this.selectedGyms,
        changeToNextTemplate: this.updateDataForm.value.changeToNextTemplate,
        hit: 1
      }

      this.economyService.getPriceChangeContractCount(priceUpdateDetails).subscribe(
        result => {
          if (result) {
            this.ContractCount = result.Data;
          }
        }
      );
    }
  }

  getUpdateContracts(content: any) {
    this.selectedGyms = this.branches.filter(X => X.IsSelected).map(B => B.BranchId);
    if (this.selectedGyms.length > 0) {
      let priceUpdateDetails = {
        dueDate: this.manipulatedate(this.updateDataForm.value.dueDate),
        lockInPeriod: this.updateDataForm.value.lockInperiod,
        priceGuarantyPeriod: this.updateDataForm.value.priceGuarantee,
        minimumAmount: this.updateDataForm.value.minimumAmount,
        templateID: this.TemplateId,
        gyms: this.selectedGyms,
        changeToNextTemplate: this.updateDataForm.value.changeToNextTemplate,
        hit: 1
      }

      this.economyService.getPriceUpdateContracts(priceUpdateDetails).subscribe(
        result => {
          if (result) {
            this.priceUpdateContracts = result.Data;
            //popup the contract view
            this.modelRef = this.modalService.open(content);
          }
        }
      );
    }
  }

  closeContracts() {
    if (this.modelRef) {
      this.modelRef.close();
    }
  }

  updatePrice(isfrompopUp: boolean) {
    if (isfrompopUp) {
      //close the popup
      if (this.modelRef) {
        this.modelRef.close();
      }
    }
    this.selectedGyms = this.branches.filter(X => X.IsSelected).map(B => B.BranchId);
    if (this.selectedGyms.length > 0) {
      let priceUpdateDetails = {
        dueDate: this.manipulatedate(this.updateDataForm.value.dueDate),
        lockInPeriod: this.updateDataForm.value.lockInperiod,
        priceGuarantyPeriod: this.updateDataForm.value.priceGuarantee,
        minimumAmount: this.updateDataForm.value.minimumAmount,
        templateID: this.TemplateId,
        gyms: this.selectedGyms,
        changeToNextTemplate: this.updateDataForm.value.changeToNextTemplate,
        hit: 1
      }

      this.economyService.updatePrice(priceUpdateDetails).subscribe(
        result => {
          if (result) {
            if (result.Data) {
              this.messageService.openMessageBox('WARNING', {
                messageTitle: 'Exceline',
                messageBody: 'MEMBERSHIP.ChangePriceSuccess'
              });
            } else {
              this.messageService.openMessageBox('WARNING', {
                messageTitle: 'Exceline',
                messageBody: 'MEMBERSHIP.ChangePriceError'
              });
            }
          }
        },
        error => {
          this.messageService.openMessageBox('WARNING', {
            messageTitle: 'Exceline',
            messageBody: 'MEMBERSHIP.ChangePriceError'
          });
        }
      );
    }
  }

  manipulatedate(dateInput: any) {
    if (dateInput.date != undefined) {
      let tempDate: Date = new Date(dateInput.date.year, dateInput.date.month - 1, dateInput.date.day);
      return moment(tempDate).format('YYYY-MM-DD').toString();
    }
  }

  ngOnDestroy() {
    if (this.modelRef) {
      this.modelRef.close();
    }

  }
}
