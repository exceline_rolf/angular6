import { Cookie } from 'ng2-cookies/ng2-cookies';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { ExceLoginService } from './../../login/exce-login/exce-login.service';
import { ConfigService } from '@ngx-config/core';
import { CommonHttpService } from './../../../shared/services/common-http.service';
import { Injectable } from '@angular/core';

@Injectable()
export class ExceEconomyService {

  private memberApiURL: string;
  private economyApiURL: string;
  private branchId: number;

  constructor(private commonHttpService: CommonHttpService, private config: ConfigService, private exceLoginService: ExceLoginService, private http: HttpClient) {
    this.memberApiURL = this.config.getSettings('EXCE_API.MEMBER');
    this.economyApiURL = this.config.getSettings('EXCE_API.ECONOMY');
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.subscribe(
      (branch) => {
        this.branchId = branch.BranchId;
      }
    );
  }

  memberFeeConfirmation(memberFeegenerationDetails: any): Observable<any> {
    const url = this.memberApiURL + '/MemberFeeConfirmation';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: memberFeegenerationDetails
    });
  }

  validateMemberFee(memberFeegenerationDetails: any): Observable<any> {
    const url = this.memberApiURL + '/ValidateMemberFee';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: memberFeegenerationDetails
    });
  }

  generateMemberfee(memberfeeGenerationDetails: any): Observable<any> {
    const url = this.memberApiURL + '/GenerateMemberFee';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: memberfeeGenerationDetails
    });
  }

  getBranches(): Observable<any> {
    const url = this.memberApiURL + '/GetUserSelectedBranches'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  getPriceChangeContractCount(priceUpdateDetails: any): Observable<any> {
    const url = this.memberApiURL + '/Contract/PriceChangeContractCount';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: priceUpdateDetails
    });
  }


  updatePrice(priceUpdateDetails: any): Observable<any> {
    const url = this.memberApiURL + '/Contract/UpdatePrice';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: priceUpdateDetails
    });
  }

  getPriceUpdateContracts(priceUpdateDetails: any): Observable<any> {
    const url = this.memberApiURL + '/Contract/PriceUpdateContracts';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: priceUpdateDetails
    });
  }

  getErrorPaymentsByType(from: any, to: any, errorPaymentType: any, branchId: any, batchId: any): Observable<any> {
    const url = this.economyApiURL + 'payment/GetErrorPaymentsByType?from=' + from + '&to=' + to + '&errorPaymentType=' + errorPaymentType + '&branchId=' + branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  removeErrorPayment(errorPaymentDetails: any): Observable<any> {
    const url = this.economyApiURL + 'payment/RemovePayment'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: errorPaymentDetails
    });
  }

  movePayment(moveingDetails): Observable<any> {
    const url = this.economyApiURL + 'payment/MovePayment'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: moveingDetails
    });
  }

  getApportionments(KID: any, creditorNo: any, amount: any, arNo: any, caseNo: any): Observable<any> {
    const url = this.economyApiURL + 'Apportionment/GetApportionmentData?amount=' + amount + '&arNo=' + arNo + '&caseNo=' + caseNo
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  printDeviations(fromDate: any, toDate: any, branchId: any): Observable<any> {
    const url = this.economyApiURL + 'payment/DeviationPrint?fromDate=' + fromDate + '&toDate=' + toDate + '&branchId=' + branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  searchInvoices(searchValue: any, invoiceType: any, fieldType: any, searchMode: any, constValue: any, paymentID): Observable<any> {
    const url = this.economyApiURL + 'invoice/GeneralInvoiceSearch?searchValue=' + searchValue + '&invoiceType=' + invoiceType
    + '&fieldType=' + fieldType + '&searchMode=' + searchMode + '&constValue=' + constValue + '&paymentID=' + paymentID;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  addApportionments(transactionList: any): Observable<any> {
    const url = this.economyApiURL + 'Apportionment/AddApportionmentData';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: transactionList
    });
  }

  getOCRImportHistory(startDate: any, endDate: any): Observable<any> {
    const url = this.economyApiURL + 'payment/GetOCRImportHistory?startDate=' + startDate + '&endDate=' + endDate;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  getATGSummary(fileType: any, startDate: any, endDueDate: any, sendingNo: any): Observable<any> {
    const url = this.economyApiURL + 'Economy/GetATGSummary?fileType=' + fileType + '&startDate=' + startDate + '&endDueDate=' + endDueDate + '&sendingNo=' + sendingNo;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  generateATGFile(fileType: any, startDate: any, endDueDate: any, sendingNo: any): Observable<any> {
    const url = this.economyApiURL + 'Economy/GenerateATGFile?fileType=' + fileType + '&startDate=' + startDate + '&endDueDate=' + endDueDate + '&sendingNo=' + sendingNo;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  getFile(filePath: string): Observable<any> {
    const path = this.economyApiURL + 'Economy/DownloadATGFile?filePath=' + filePath;
    return this.commonHttpService.downLoadFile(path);
  }

  uploadFile(fileData) {
    const url = this.economyApiURL + 'payment/UploadOCRFile';
    return this.commonHttpService.uploadFile(url, fileData);
  }

  getOCRSummary(filePath: any): Observable<any> {
    const url = this.economyApiURL + 'payment/GetOCRSummary?filePath=' + filePath;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  importOCR(filePath: any, fileType: any): Observable<any> {
    const url = this.economyApiURL + 'payment/ImportOCR?filePath=' + filePath + '&fileType=' + fileType;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  getErrorFiles(filePath: any): Observable<any> {
    const url = this.economyApiURL + 'payment/GetErrorFiles?filePath=' + filePath;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  getaccountnames(gymCode: any): Observable<any> {
    const url = this.economyApiURL + 'Economy/GetAccountNames?gymCode=' + gymCode;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }


  addaccountnumber(description: string, accnum: number ): Observable<any> {
    const url = this.economyApiURL + 'Economy/AddAccountNumber?description=' + description + '&accnum=' + accnum ;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true ,
      body: accnum
    });
  }

  runaccountfile(fromtime: any , totime: any, branchId: any, selectall: any): Observable<any> {
    const url = this.economyApiURL + 'Economy/RunAccountFile?fromtime=' + fromtime + '&totime=' + totime + '&branchId=' + branchId + '&selectall=' + selectall;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }


  getKeyForSalePoint(salePointId: number, branchId: number): Observable<any> {
    const url = this.economyApiURL + 'Economy/getKeyForSalePoint?salePointId=' + salePointId  + '&branchId=' + branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }




  getPublicPEMKeyForSalePoint(): Observable<any> {
   const url = this.economyApiURL + 'Economy/getPublicPEMKeyForSalePoint?';
   return this.commonHttpService.makeHttpCall(url, {
     method: 'GET',
     auth: true
   });
  }

  GetSalePointsByBranch( branchId: number): Observable<any> {
    const url = this.economyApiURL + 'Economy/GetSalePointsByBranch?branchId=' + branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  GetTransactionData( fromDate: Date, toDate: Date, branchId: number): Observable<any> {
    const url = this.economyApiURL + 'Economy/GetTransactionData?fromDate=' + fromDate + '&toDate=' + toDate + '&branchId=' + branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  GetInvoice(orderno: String, isBris: boolean, branchId: number , sponsorARItemNo: number = 0 , anonymizeNames: boolean = false): Observable<any> {
    const url = this.economyApiURL + 'invoice/GetInvoice?orderno=' + orderno + '&isBris=' + isBris + '&branchId=' + branchId + '&sponsorAritemNo=' + sponsorARItemNo +
    '&anonymizeNames=' + anonymizeNames;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

}
