import { ExceLoginService } from '../../login/exce-login/exce-login.service';
import { Injectable } from '@angular/core';
import { CommonHttpService } from '../../../shared/services/common-http.service';
import { ConfigService } from '@ngx-config/core';
import { PostalArea } from '../../../shared/SystemObjects/Common/PostalArea';
import { Observable } from 'rxjs';
import { Cookie } from 'ng2-cookies/ng2-cookies';
@Injectable()
export class ExceMemberService {
  private memberApiURL: string;
  private branchId: number;

  constructor(private commonHttpService: CommonHttpService, private config: ConfigService, private exceLoginService: ExceLoginService) {
    this.memberApiURL = this.config.getSettings('EXCE_API.MEMBER');
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.subscribe(
      (branch) => {
        this.branchId = branch.BranchId;
      }
    );
  }


  getMemberDetails(memberID: number, branchID: number, memberRole: string): Observable<any> {
    const url = this.memberApiURL + '/GetMemberDetails?branchID=' + branchID + '&memberID=' + memberID + '&memberRole=' + memberRole
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  getMemberCount(branchId: number): Observable<any> {
    const url = this.memberApiURL + '/GetMemberCount?branchId=' + Number(branchId);
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  getMembers(branchID: number, searchText: string, statuse: number, searchType: any, memberRole: string, hit: number, isHeaderClick: boolean,
    isAscending: boolean, sortName: string): Observable<any> {
    const url = this.memberApiURL + '/GetMembers?branchID=' + branchID + '&searchText=' + searchText + '&statuse=' + statuse
      + '&searchType=' + searchType + '&memberRole=' + memberRole
      + '&hit=' + hit + '&isHeaderClick=' + isHeaderClick + '&isAscending=' + isAscending + '&sortName=' + sortName
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  membercardVisit(memberID: any): Observable<any> {
    const url = this.memberApiURL + '/MembercardVisit?memberID=' + memberID;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true
    })
  }

  getVisitedMembers(): Observable<any> {
    const url = this.memberApiURL + '/GetVisitedMembers';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  getMembersList(parameters: any): Observable<any> {
    const url = this.memberApiURL + '/GetMemberList';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: parameters
    })
  }

  getBranches(): Observable<any> {
    const url = this.memberApiURL + '/GetUserSelectedBranches'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  test(branchID: number, searchText: string, statuse: number, searchType: any): Observable<any> {
    // tslint:disable-next-line:max-line-length
    const url = this.memberApiURL + '/Test?branchID=' + branchID + '&searchText=' + searchText + '&statuse=' + statuse
      + '&searchType=' + searchType
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  getMemberContractSummeries(memberId: number, branchId: number, isFreezeDetailsNeeded: boolean, isFreezeView: boolean): Observable<any> {
    // tslint:disable-next-line:max-line-length
    const url = this.memberApiURL + '/Contract/GetContractSummeries?memberId=' + memberId + '&branchId=' + branchId
      + '&isFreezDetailNeeded=' + isFreezeDetailsNeeded
      + '&isFreezdView=' + isFreezeView;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  getCountries(): Observable<any> {
    const url = this.memberApiURL + '/GetCountries';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  getCategories(type: string): Observable<any> {
    const url = this.memberApiURL + '/GetCategories?type=' + type;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  getMemberStatus(getAllStatuses): Observable<any> {
    const url = this.memberApiURL + '/GetMemberStatus?getAllStatuses=' + getAllStatuses;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  savePostalArea(postalArea: PostalArea): Observable<any> {
    const url = this.memberApiURL + '/SavePostalArea';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: postalArea
    })
  }

  updateMember(member: any): Observable<any> {
    const url = this.memberApiURL + '/UpdateMember';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: member
    })
  }

  AddEventlogForImangeChange(notification: any): Observable<any> {
    const url = this.memberApiURL + '/AddEventlogForImangeChange';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: notification
    })
  }

  getMemberSearchCategory(branchId: number): Observable<any> {
    const url = this.memberApiURL + '/GetMemberSearchCategory?branchId=' + branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  getCityForPostalCode(postalCode: string): Observable<any> {
    const url = this.memberApiURL + '/GetCityForPostalCode?postalCode=' + postalCode;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  validateMemberwithStatus(memberID: number, statusID: number): Observable<any> {
    const url = this.memberApiURL + '/ValidateMemberStatus?memberId=' + memberID + '&statusId=' + statusID;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  getMemberPaymentsHistory(branchId: number, hit: number, memberId: number, paymentType: string): Observable<any> {
    const url = this.memberApiURL + '/GetMemberPaymentsHistory?branchId=' + branchId + '&hit=' + hit
      + '&memberId=' + memberId + '&paymentType=' + paymentType;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  getSMSNotificationTemplateByEntity(): Observable<any> {
    const url = this.memberApiURL + '/GetSMSNotificationTemplateByEntity';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  sendSMSFromMemberCard(smsData: any): Observable<any> {
    const url = this.memberApiURL + '/SendSMSFromMemberCard';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: smsData
    })
  }

  sendEmailFromMemberCard(emailData: any): Observable<any> {
    const url = this.memberApiURL + '/SendEmailFromMemberCard';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: emailData
    })
  }

  getMemberBooking(memberId: number): Observable<any> {
    const url = this.memberApiURL + '/GetMemberBooking?memberId=' + memberId
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  getResources(branchId: number, searchText: string, categoryId: number, activityId: number, isActive: boolean): Observable<any> {
    const url = this.memberApiURL + '/GetResources?branchId=' + branchId + '&searchText=' + searchText + '&categoryId=' + categoryId
      + '&activityId=' + activityId + '&isActive=' + isActive
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  getActivityCategories(branchId: number): Observable<any> {
    const url = this.memberApiURL + '/GetCategories?type=ACTIVITY&branchId=' + branchId
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  getResourceCategories(branchId: number): Observable<any> {
    const url = this.memberApiURL + '/GetCategories?type=RESOURCE&&branchId=' + branchId
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  getOtherMembersForBooking(scheduleId: string): Observable<any> {
    const url = this.memberApiURL + '/GetOtherMembersForBooking?scheduleId=' + scheduleId
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  PrintMemberBooking(memberId: number, branchId: number, fromDate: any, toDate: any): Observable<any> {
    const url = this.memberApiURL + '/PrintMemberBooking?memberId=' + memberId + '&branchId=' + branchId + '&fromDate=' + fromDate + '&toDate=' + toDate
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  getClassByMemberId(branchId: number, memberId: number, classDate?: any): Observable<any> {
    const url = this.memberApiURL + '/GetClassByMemberId?branchId=' + branchId + '&memberId=' + memberId + '&classDate=' + classDate
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  PrintClassListByMember(branchId: number, memberId: number, fromDate: any, toDate: any): Observable<any> {
    const url = this.memberApiURL + '/PrintClassListByMember?branchId=' + branchId + '&memberId=' + memberId + '&fromDate=' + fromDate + '&toDate=' + toDate
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  getSelectedGymSettings(settingParams): Observable<any> {
    const url = this.memberApiURL + '/GetSelectedGymSettings?gymSetColNames=' + settingParams + '&isGymSettings=' + true + '&branchId=' + this.branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: settingParams
    })
  }

  cancelPayment(body: any): Observable<any> {
    const url = this.memberApiURL + '/CancelPayment';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }

  updateUserRoutine(body: any): Observable<any> {
    const url = this.memberApiURL + '/UpdateUserRoutine';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }

  getClaimExportStatusForMember(memberId: number, hit: number, type: string, transferDate: any): Observable<any> {
    const url = this.memberApiURL + '/GetClaimExportStatusForMember?memberId=' + memberId + '&&hit=' + hit + '&&type=' + type + '&&transferDate=' + transferDate
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  getMemberBasicInfo(memberId: number): Observable<any> {
    const url = this.memberApiURL + '/GetMemberBasicInfo?memberId=' + memberId
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  getIntroducedMembers(memberId: number, branchId: number): Observable<any> {
    const url = this.memberApiURL + '/GetIntroducedMembers?memberId=' + memberId + '&&branchId=' + branchId
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  updateIntroducedMembers(body: any): Observable<any> {
    const url = this.memberApiURL + '/UpdateIntroducedMembers';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }

  getNotificationByNotifiyMethod(memberId: number, branchId: number, fromDate: any, toDate: any, notifyMethod: any): Observable<any> {
    const url = this.memberApiURL + '/GetNotificationByNotifiyMethod?memberId=' + memberId + '&&branchId=' + branchId + '&&fromDate=' + fromDate +
      '&&toDate=' + toDate + '&&notifyMethod=' + notifyMethod
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  updateNotificationStatus(body: any): Observable<any> {
    const url = this.memberApiURL + '/UpdateNotificationStatus';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }

  addCountryDetails(body: any): Observable<any> {
    const url = this.memberApiURL + '/AddCountryDetails';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }

  getSessionValue(sessionKey: string): Observable<any> {
    const url = this.memberApiURL + '/GetSessionValue?sessionKey=' + sessionKey
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  getInterestCategoryByMember(memberId: number): Observable<any> {
    const url = this.memberApiURL + '/GetInterestCategoryByMember?memberId=' + memberId + '&&branchId=' + this.branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  saveInterestCategoryByMember(body: any): Observable<any> {
    const url = this.memberApiURL + '/SaveInterestCategoryByMember';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: body
    })
  }

  getCategoryTypes(name: string, branchId: number): Observable<any> {
    const url = this.memberApiURL + '/GetCategoryTypes?name=' + name + '&&branchId=' + branchId
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  saveCategory(newCategory: any): Observable<any> {
    const url = this.memberApiURL + '/SaveCategory'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: newCategory
    });
  }

  getContractDetails(contractId: number, branchId?: number): Observable<any> {
    const url = this.memberApiURL + '/Contract/GetContractDetails?contractId=' + contractId + '&branchId=' + branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  getAccessProfiles(gender: any): Observable<any> {
    const url = this.memberApiURL + '/Contract/GetAccessProfiles?gender=' + gender;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  getInstallments(branchId: number, memberContractId: number): Observable<any> {
    const url = this.memberApiURL + '/Contract/GetInstallments?branchId=' + branchId + '&memberContractId=' + memberContractId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }


  getMemberVisits(memberId: number, selectedDate, branchId): Observable<any> {
    const url = this.memberApiURL + '/GetMemberVisits?memberId=' + memberId + '&selectedDate=' + selectedDate + '&branchId=' + branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  PrintMemberVisits(memberId: number, branchId: number, fromDate: any, toDate: any): Observable<any> {
    const url = this.memberApiURL + '/PrintVisitsListByMember?branchId=' + branchId + '&memberId=' + memberId + '&fromDate=' + fromDate + '&toDate=' + toDate
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetContractSummaries(memberId: number, branchId: number, isFreezDetailNeeded: boolean, isFreezdView: boolean): Observable<any> {
    const url = this.memberApiURL + '/GetContractSummaries?memberId=' + memberId + '&&branchId=' + branchId +
      '&&isFreezDetailNeeded=' + isFreezDetailNeeded + '&&isFreezdView=' + isFreezdView
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetVisitCount(memberId: number, startDate: any, endDate: any): Observable<any> {
    const url = this.memberApiURL + '/GetVisitCount?memberId=' + memberId + '&&startDate=' + startDate + '&&endDate=' + endDate
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  DeleteMemberVisit(deleteVisit: any): Observable<any> {
    const url = this.memberApiURL + '/DeleteMemberVisit';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: deleteVisit
    })
  }

  SaveMemberVisit(memberVisit: any): Observable<any> {
    const url = this.memberApiURL + '/SaveMemberVisit';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: memberVisit
    })
  }

  getInstructors(searchText: string, isActive: boolean): Observable<any> {
    const url = this.memberApiURL + '/GetInstructors?branchId=' + this.branchId + '&searchText=' + searchText + '&isActive=' + isActive;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  getEmployeeCategoryBySponsorId(branchId, sponsorId: number): Observable<any> {
    const url = this.memberApiURL + '/GetEmployeeCategoryBySponsorId?branchId=' + branchId + '&sponsorId=' + sponsorId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  checkEmailExits(email: string) {
    if (email === 'nuwan@gmail.com') {
      return true;
    } else {
      return false;
    }

  }

  checkMemberCardExits(memberCard: string) {
    if (memberCard === '12345') {
      return true;
    } else {
      return false;
    }
  }

  validateEmail(email: string): Observable<any> {
    const url = this.memberApiURL + '/ValidateEmail?email=' + email;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  validateMobile(mobile: string, mobilePrifix: string): Observable<any> {
    const url = this.memberApiURL + '/ValidateMobile?mobile=' + mobile + '&mobilePrefix=' + mobilePrifix;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  validateMemberCard(memberCard: string, memberId: number): Observable<any> {
    const url = this.memberApiURL + '/ValidateMemberCard?memberCard=' + memberCard + '&memberId=' + memberId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  saveMember(member: any): Observable<any> {
    const url = this.memberApiURL + '/SaveMember'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: member
    });
  }

  validateGatCardNo(gatCardNo: string): Observable<any> {
    const url = this.memberApiURL + '/ValidateGatCardNo?gatCardNo=' + gatCardNo;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  getFollowUpTemplates(branchId: number): Observable<any> {
    const url = this.memberApiURL + '/GetFollowUpTemplates?branchId=' + branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  getFollowUpTaskByTemplateIds(followUpTemplateId: number): Observable<any> {
    const url = this.memberApiURL + '/GetFollowUpTaskByTemplateIds?followUpTemplateId=' + followUpTemplateId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetDocumentData(isActive: boolean, custId: string, searchText: string, documentType: number, branchId?: number): Observable<any> {
    const url = this.memberApiURL + '/GetDocumentData?isActive=' + isActive + '&&custId=' + custId + '&&searchText=' +
      searchText + '&&documentType=' + documentType + '&&branchId=' + branchId
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  DeleteDocumentData(deleteDocument: any): Observable<any> {
    const url = this.memberApiURL + '/DeleteDocumentData';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: deleteDocument
    })
  }

  SaveDocumentData(document: any): Observable<any> {
    const url = this.memberApiURL + '/SaveDocumentData';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: document
    })
  }

  addDocument(document: any): Observable<any> {
    const url = this.memberApiURL + '/AddDocument';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: document
    })
  }

  addFile(document: any): Observable<any> {
    const url = this.memberApiURL + '/addFile';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: document
    })
  }

  GetMemberIntegrationSettings(branchId: number, memberId: number, classDate?: any): Observable<any> {
    const url = this.memberApiURL + '/GetMemberIntegrationSettings?branchId=' + branchId + '&&memberId=' + memberId
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetExorLiveClientAppUrl(): Observable<any> {
    const url = this.memberApiURL + '/GetExorLiveClientAppUrl'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetEncryptKey(custId: string): Observable<any> {

    const url = this.memberApiURL + '/GetEncryptKey?custId=' + custId
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetSessionValue(session: any): Observable<any> {
    const url = this.memberApiURL + '/GetSessionValue';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: session
    })
  }

  AddSessionValue(session: any): Observable<any> {
    const url = this.memberApiURL + '/AddSessionValue';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: session
    })
  }

  GetGymCompanySettings(settingType: string): Observable<any> {
    const url = this.memberApiURL + '/Utill/GetGymCompanySettings?gymCompanySettingType=' + settingType
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  // to get member order list
  getMemberOrders(memberId: number, type: string): Observable<any> {
    const url = this.memberApiURL + '/GetMemberOrders?memberId=' + memberId + '&&type=' + type;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  deleteInstallment(deleteInstallment: any): Observable<any> {
    const url = this.memberApiURL + '/DeleteInstallment';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: deleteInstallment
    })
  }

  WellnessIntegrationOperation(wellnessIntegration): Observable<any> {
    const url = this.memberApiURL + '/WellnessIntegrationOperation';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: wellnessIntegration
    })
  }

  InfoGymRegisterMember(InfoGymRegister): Observable<any> {
    const url = this.memberApiURL + '/InfoGymRegisterMember';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: InfoGymRegister
    })
  }

  InfoGymUpdateMember(InfoGymUpdate): Observable<any> {
    const url = this.memberApiURL + '/InfoGymUpdateMember';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: InfoGymUpdate
    })
  }

  // GenerateInvoice
  generateInvoice(generateInvoice: any): Observable<any> {
    const url = this.memberApiURL + '/GenerateInvoice';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: generateInvoice
    })
  }

  // ---------------------------Sponsor  Setting  ----------------------
  getSponsorEmployeeCategoryList(sponsorId: number, branchId: number): Observable<any> {
    const url = this.memberApiURL + '/Sponsor/GetEmployeeLevelList?sponsorId=' + sponsorId + '&branchId=' + branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  // Same as GetGroupDiscountByType()
/*   getSponsorDiscount(branchId: number, discountTypeId: number, sponsorId: number): Observable<any> {
    const url = this.memberApiURL + '/Sponsor/GetSponsorDiscount?branchId=' + branchId + '&discountTypeId=' + discountTypeId + '&sponsorId=' + sponsorId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  } */

  // GetNotificationTemplateListByMethod
  getNotificationTemplateListByMethod(method: any, temp: any, branchId: number): Observable<any> {
    const url = this.memberApiURL + '/GetNotificationTemplateListByMethod?method=' + method + '&&temp=' + temp + '&&branchId=' + branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  // GenerateAndSendInvoiceSMSANDEmail
  generateAndSendInvoiceSMSANDEmail(sendInvoiceSMSANDEmail: any): Observable<any> {
    const url = this.memberApiURL + '/GenerateAndSendInvoiceSMSANDEmail';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: sendInvoiceSMSANDEmail
    })
  }

  // SendInvoiceSMSANDEmail
  sendInvoiceSMSANDEmail(sendInvoiceSMSANDEmail: any): Observable<any> {
    const url = this.memberApiURL + '/SendInvoiceSMSANDEmail';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: sendInvoiceSMSANDEmail
    })
  }

  getSponsorSetting(branchId: number, sponsorId: number): Observable<any> {
    const url = this.memberApiURL + '/Sponsor/GetSponsorSetting?branchId=' + branchId + '&sponsorId=' + sponsorId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  getFollowUps(memberId: number, followUpId: number): Observable<any> {
    const url = this.memberApiURL + '/GetFollowUps?memberId=' + memberId + '&followUpId=' + followUpId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  saveFollowUp(followUpList: any): Observable<any> {
    const url = this.memberApiURL + '/SaveFollowUp';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: followUpList
    })
  }

  GetContractFreezeItems(memberContractId: number): Observable<any> {
    const url = this.memberApiURL + '/GetContractFreezeItems?memberContractId=' + memberContractId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetContractFreezeInstallments(freezeItemId: number): Observable<any> {
    const url = this.memberApiURL + '/GetContractFreezeInstallments?freezeItemId=' + freezeItemId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetInstallmentsToFreeze(memberContractId: number): Observable<any> {
    const url = this.memberApiURL + '/GetInstallmentsToFreeze?memberContractId=' + memberContractId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  SaveContractFreezeItem(ContractFreezeItem: any): Observable<any> {
    const url = this.memberApiURL + '/SaveContractFreezeItem';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: ContractFreezeItem
    })
  }

  ExtendContractFreezeItem(ContractFreezeItem: any): Observable<any> {
    const url = this.memberApiURL + '/ExtendContractFreezeItem';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: ContractFreezeItem
    })
  }

  UnfreezeContract(ContractFreezeItem: any): Observable<any> {
    const url = this.memberApiURL + '/UnfreezeContract';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: ContractFreezeItem
    })
  }

  UpdateMemberInstallment(installment: any): Observable<any> {
    const url = this.memberApiURL + '/Contract/UpdateMemberInstallment';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: installment
    })
  }

  ResignContract(resignDetails: any): Observable<any> {
    const url = this.memberApiURL + '/Contract/ResignContract';
    resignDetails.BranchId = this.branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: resignDetails
    })
  }

  GetGroupMembers(groupId: number): Observable<any> {
    const url = this.memberApiURL + '/GetGroupMembers?groupId=' + groupId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetFamilyMembers(familyId: number): Observable<any> {
    const url = this.memberApiURL + '/GetFamilyMembers?memberId=' + familyId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetContractTemplates(branchId: Number, contractType: Number): Observable<any> {
    const url = this.memberApiURL + '/GetContractTemplates?branchId=' + branchId + '&contractType=' + contractType;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  SaveMemberInfoWithNotes(inforDetail: any): Observable<any> {
    const url = this.memberApiURL + '/SaveMemberInfoWithNotes';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: inforDetail
    })
  }

  validateChangeGymInMember(currentBranchID: number, newBranchID: number, memberID: number): Observable<any> {
    const url = this.memberApiURL + '/ValidateChangeGymInMember?currentBranchID=' + currentBranchID + '&newBranchID=' + newBranchID + '&branchID=' + this.branchId
      + '&memberID=' + memberID;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }

  GetMemberInfoWithNotes(branchId: number, memberId: number): Observable<any> {
    const url = this.memberApiURL + '/GetMemberInfoWithNotes?branchId=' + branchId + '&memberId=' + memberId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }
  GetGymsforAccountNumber(accountNumber: number): Observable<any> {
    const url = this.memberApiURL + '/GetGymsforAccountNumber?accountNumber=' + accountNumber;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    })
  }
  getMemberDetailsByMemberId(memberID: Number, memberRole: string): Observable<any> {
    const url = this.memberApiURL + '/GetMemberDetails?branchId=' + this.branchId + '&memberID=' + memberID + '&memberRole=' + memberRole;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  getEconomyDetails(memberID: Number): Observable<any> {
    const url = this.memberApiURL + '/GetEconomyDetails?memberID=' + memberID;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  changePPAccount(increase: any, amount: any, memberId: any, branchId: any): Observable<any> {
    const url = this.memberApiURL + '/ChangePrePaidAccount?increase=' + increase + '&amount=' + amount + '&memberId=' + memberId + '&branchId=' + branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
    });
  }

  getListOfGuardian(guardianId: Number): Observable<any> {
    const url = this.memberApiURL + '/GetListOfGuardian?guardianId=' + guardianId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  saveEconomyDetails(MemberEconomyDetails: any): Observable<any> {
    const url = this.memberApiURL + '/SaveEconomyDetails';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: MemberEconomyDetails
    });
  }

  GetMemberInvoicesForContract(memberId: number, contractId: number, hit: number): Observable<any> {
    const url = this.memberApiURL + '/Contract/GetMemberInvoicesForContract?memberId=' + memberId + '&contractId=' + contractId + '&hit=' + hit;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  ViewCreditNoteContent(arItemNo: number): Observable<any> {
    const url = this.memberApiURL + '/ViewCreditNoteDetails?branchId=' + this.branchId + '&arItemNo=' + arItemNo;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  ViewInvoiceDetails(arItemNo: number): Observable<any> {
    const url = this.memberApiURL + '/ViewInvoiceDetails?branchId=' + this.branchId + '&arItemNo=' + arItemNo;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  ViewMemberATGContract(memberId: number): Observable<any> {
    const url = this.memberApiURL + '/ViewMemberATGContract?branchId=' + this.branchId + '&memberId=' + memberId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  GetMemberInvoices(memberId: number, hit: number): Observable<any> {
    const url = this.memberApiURL + '/GetMemberInvoices?memberId=' + memberId + '&hit=' + hit;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  GetInvoiceDetails(invoiceId: number): Observable<any> {
    const url = this.memberApiURL + '/GetInvoiceDetails?invoiceId=' + invoiceId + '&branchId=' + this.branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  UpdateInvoice(updateInvoice: any): Observable<any> {
    const url = this.memberApiURL + '/UpdateInvoice';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: updateInvoice
    });
  }

  GetCreditNoteDetails(aritemNo: number): Observable<any> {
    const url = this.memberApiURL + '/GetCreditNoteDetails?arItemno=' + aritemNo;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  AddCreditNote(creditNote: any): Observable<any> {
    const url = this.memberApiURL + '/AddCreditNote';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: creditNote
    })
  }
  getEmployeeRoles(): Observable<any> {
    const url = this.memberApiURL + '/GetEmpRole'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  printGroupMemberListByMember(memberId: number): Observable<any> {
    const url = this.memberApiURL + '/ViewMemberATGContract?branchId=' + this.branchId + '&memberId=' + memberId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  printInvoicePaidReceipt(arItemNo: number): Observable<any> {
    const url = this.memberApiURL + '/PrintInvoicePaidReceipt?branchId=' + this.branchId + '&arItemNo=' + arItemNo;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  viewSponsorInvoiceDetails(arItemNo: number): Observable<any> {
    const url = this.memberApiURL + '/ViewSponsorInvoiceDetails?branchId=' + this.branchId + '&arItemNo=' + arItemNo;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  GetUserRole(): Observable<any> {
    const url = this.memberApiURL + '/GetUserRole'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  SaveMemberContract(membercontract: any): Observable<any> {
    const url = this.memberApiURL + '/Contract/SaveContract'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: membercontract
    });
  }

  getContractActivities(): Observable<any> {
    const url = this.memberApiURL + '/Utill/GetActivities?branchID=' + this.branchId
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  getContractBookings(memberContractId: any): Observable<any> {
    const url = this.memberApiURL + '/Contract/ContractBookings?branchId=' + this.branchId + '&membercontractId=' + memberContractId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  addInstallment(installment: any): Observable<any> {
    const url = this.memberApiURL + '/Contract/AddInstallment';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: installment
    });
  }

  addBlancInstallment(installment: any): Observable<any> {
    const url = this.memberApiURL + '/Contract/AddBlancInstallment?memberId=' + installment.MemberId + '&branchId=' + installment.branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true
    });
  }

  mergeOrders(mergeDetails: any): Observable<any> {
    const url = this.memberApiURL + '/Contract/MergeOrders';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: mergeDetails
    });
  }

  updateContractOrders(orderUpdateDetails: any): Observable<any> {
    const url = this.memberApiURL + '/Contract/UpdateContractOrder';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: orderUpdateDetails
    });
  }

  DeleteEmployeeCategory(categoryID): Observable<any> {
    const url = this.memberApiURL + '/Sponsor/DeleteEmployeeCategory?categoryID=' + categoryID;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  GetGroupDiscountByType(branchId: number, discountTypeId: number, sponsorId: number): Observable<any> {
    const url = this.memberApiURL + '/Sponsor/GetSponsorDiscount?branchId=' + branchId + '&discountTypeId=' + discountTypeId + '&sponsorId=' + sponsorId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  RemoveSponsorshipOfMember(sponsoredRecordId): Observable<any> {
    const url = this.memberApiURL + '/Sponsor/RemoveSponsorshipOfMember?sponsoredRecordId=' + sponsoredRecordId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  DeleteDiscountCategory(categoryID): Observable<any> {
    const url = this.memberApiURL + '/Sponsor/DeleteDiscountCategory?categoryID=' + categoryID;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  SaveSponsorSetting(sponsorSetting: any): Observable<any> {
    const url = this.memberApiURL + '/Sponsor/SaveSponsorSetting';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: sponsorSetting
    });
  }

  removeResignation(resignRemoveDetails: any): Observable<any> {
    const url = this.memberApiURL + '/Contract/RemoveResign';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: resignRemoveDetails
    });
  }

  // GenerateAndSendInvoiceSMSANDEmail
  generateOrderAndSMSANDEmail(sendInvoiceSMSANDEmail: any): Observable<any> {
    const url = this.memberApiURL + '/Contract/GenerateOrderAndSMSANDEmail';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: sendInvoiceSMSANDEmail
    })
  }

  updateMemerContract(memberContract: any): Observable<any> {
    const url = this.memberApiURL + '/Contract/UpdateContract';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: memberContract
    });
  }

  deleteOrder(deleteOrderDetails: any): Observable<any> {
    const url = this.memberApiURL + '/Contract/DeleteOrder';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: deleteOrderDetails
    });
  }

  getLastInstallment(membercontractID: any): Observable<any> {
    const url = this.memberApiURL + '/Contract/GetLastInstallment?membercontractID=' + membercontractID;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  getRenewTemplate(templateID: any, filterType: any): Observable<any> {
    const url = this.memberApiURL + '/Contract/GetRenewTemplate?templateID=' + templateID + '&filterType=' + filterType;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  renewMemberContract(contractRenewDetails: any): Observable<any> {
    const url = this.memberApiURL + '/Contract/RenewMemberContract';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: contractRenewDetails
    });
  }

  IsBrisIntegration(): Observable<any> {
    const url = this.memberApiURL + '/BrisIntegration';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  PrintContractDocument(contractId: number): Observable<any> {
    const url = this.memberApiURL + '/ViewMemberContractDetail?branchId=' + this.branchId + '&contractId=' + contractId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  printGroupContratMembers(contractId: number, memberType: any): Observable<any> {
    const url = this.memberApiURL + '/PrintGroupContractMemberList?branchId=' + this.branchId + '&contractID=' + contractId + '&memberType=' + memberType;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  printOrder(orderId: any): Observable<any> {
    const url = this.memberApiURL + '/PrintOrder?orderId=' + orderId + '&branchId=' + this.branchId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  printPayments(memberId: any, fromDate: any, toDate: any): Observable<any> {
    const url = this.memberApiURL + '/PrintPaymentHistory?branchId=' + this.branchId + '&memberId=' + memberId + '&fromDate=' + fromDate + '&toDate=' + toDate;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  deleteCreditNote(credidNoteId: number): Observable<any> {
    const url = this.memberApiURL + '/DeleteCreditNote?creditNoteId=' + credidNoteId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  getContractPrint(contractId: number): Observable<any> {
    const url = this.memberApiURL + '/Contract/PrintOfMemberContract?contractId=' + contractId;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  registerChangeOfInvoiceDueDate(duedatechangedata: any): Observable<any> {
    const url = this.memberApiURL + '/RegisterChangeOfInvoiceDueDate';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: duedatechangedata
    });
  }

}

