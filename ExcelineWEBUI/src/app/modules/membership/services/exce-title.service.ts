import { Injectable, Output, EventEmitter } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class ExceTitleService {
  private _title = new BehaviorSubject<string>('Default title');

  public currentTitle = this._title.asObservable();

  @Output() titleChangeEvent: EventEmitter<any> =  new EventEmitter();

  constructor() { }

  public setTitle(title: string) {
    this._title.next(
      title
    )
  }
}
