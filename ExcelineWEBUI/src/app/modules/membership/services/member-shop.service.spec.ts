import { TestBed, inject } from '@angular/core/testing';

import { MemberShopService } from './member-shop.service';

describe('MemberShopService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MemberShopService]
    });
  });

  it('should be created', inject([MemberShopService], (service: MemberShopService) => {
    expect(service).toBeTruthy();
  }));
});
