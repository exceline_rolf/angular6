import { Component, OnInit, OnDestroy } from '@angular/core';
import { McBasicInfoService } from './../mc-basic-info/mc-basic-info.service';
import { ExceMemberService } from '../../services/exce-member.service';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { DataTableResource } from '../../../../shared/components/us-data-table/tools/data-table-resource';
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { ExceLoginService } from '../../../../modules/login/exce-login/exce-login.service';
import { Subject } from 'rxjs';
import { takeUntil, mergeMap } from 'rxjs/operators';

const now = new Date();

@Component({
  selector: 'app-mc-interests',
  templateUrl: './mc-interests.component.html',
  styleUrls: ['./mc-interests.component.scss']
})
export class McInterestsComponent implements OnInit, OnDestroy {

  private selectedMember: any;
  SelectedBranchId: number = JSON.parse(Cookie.get('selectedBranch')).BranchId;
  interestCategories?: any[] = [];
  itemCount = 0;
  private itemResource: DataTableResource<any>;
  private allInterests?: any[] = [];
  memberInterests = [];
  private selectedMemberInterests = [];
  private newCategoryRegisterView: any;
  items = [];
  branchId: number;
  private destroy$ = new Subject<void>();

  constructor(
    private basicInfoService: McBasicInfoService,
    private exceMemberService: ExceMemberService,
    private modalService: UsbModal,
    private exceLoginService: ExceLoginService
  ) {
    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.pipe(takeUntil(this.destroy$)).subscribe(
      (branch) => {
        this.branchId = branch.BranchId;
      }
    );
  }

  ngOnInit() {
    // get the selected member and load to the data to the table

    this.basicInfoService.currentMember.pipe(
      mergeMap(currentMember => {
        this.selectedMember = currentMember;
        return this.exceMemberService.getCategories('INTERESTS');
      }),
      mergeMap(category => {
        this.allInterests = category.Data;
        return this.exceMemberService.getInterestCategoryByMember(this.selectedMember.Id);
      }),
      takeUntil(this.destroy$)
    ).subscribe(resultInterest => {
      if (resultInterest) {
        this.interestCategories = resultInterest.Data;
        for (const x of this.allInterests) {
          for (const y of this.interestCategories) {
            if (x.Id === y.Id) {
              x.IsCheck = true;
            }
          }
        }
      }
      this.memberInterests = this.allInterests;
      this.itemResource = new DataTableResource(this.memberInterests);
      this.itemResource.count().then(count => this.itemCount = count);
      this.items = this.memberInterests;
      this.itemCount = Number(this.allInterests.length);
    })

    // this.basicInfoService.currentMember.mergeMap(currentMember => {
    //   this.selectedMember = currentMember;
    //   return this.exceMemberService.getCategories('INTERESTS');
    // }).mergeMap(category => {
    //   this.allInterests = category.Data;
    //   return this.exceMemberService.getInterestCategoryByMember(this.selectedMember.Id).pipe(
    //     takeUntil(this.destroy$)
    //   )
    // }).pipe(
    //   takeUntil(this.destroy$)
    //   ).subscribe(resultInterest => {
    //     if (resultInterest) {
    //       this.interestCategories = resultInterest.Data;
    //       for (const x of this.allInterests) {
    //         for (const y of this.interestCategories) {
    //           if (x.Id === y.Id) {
    //             x.IsCheck = true;
    //           }
    //         }
    //       }
    //     }
    //     this.memberInterests = this.allInterests;
    //     this.itemResource = new DataTableResource(this.memberInterests);
    //     this.itemResource.count().then(count => this.itemCount = count);
    //     this.items = this.memberInterests;
    //     this.itemCount = Number(this.allInterests.length);
    //   })



/*     this.basicInfoService.currentMember.pipe(takeUntil(this.destroy$)).subscribe(
      member => {
        if (member) {
          this.exceMemberService.getCategories('INTERESTS').pipe(takeUntil(this.destroy$)).subscribe(result => {
              this.allInterests = result.Data;

              if (result.Data) {
                this.selectedMember = member;
                // get all interest list
                this.exceMemberService.getInterestCategoryByMember(this.selectedMember.Id).pipe(takeUntil(this.destroy$)).subscribe(resultInterest => {
                    if (resultInterest) {
                      this.interestCategories = resultInterest.Data;
                      for (const x of this.allInterests) {
                        for (const y of this.interestCategories) {
                          if (x.Id === y.Id) {
                            x.IsCheck = true;
                          }
                        }
                      }
                    }
                    this.memberInterests = this.allInterests;
                    this.itemResource = new DataTableResource(this.memberInterests);
                    this.itemResource.count().then(count => this.itemCount = count);
                    this.items = this.memberInterests;
                    this.itemCount = Number(result.Data.length);
                  });
              }
            },
              error => {
                console.log(error);
              });
        }
      }); */

  }

  ngOnDestroy() {
    if (this.newCategoryRegisterView) {
      this.newCategoryRegisterView.close();
    }
    this.destroy$.next();
    this.destroy$.complete();
  }

  // save the interest category by member to the database
  saveInterestCategoryByMember(): any {
    this.selectedMemberInterests = this.memberInterests.filter(x => x.IsCheck === true);
    const memberInterestsObject = {
      interestCategoryList: this.selectedMemberInterests,
      memberId: this.selectedMember.Id,
      branchId: this.selectedMember.BranchId    // SelectedBranchId
    }

    this.exceMemberService.saveInterestCategoryByMember(memberInterestsObject).pipe(takeUntil(this.destroy$)).subscribe(result => {
    },
      error => {
        console.log(error);
      });
  }

  newCategoryModal(content: any) {
    this.newCategoryRegisterView = this.modalService.open(content, { width: '800' });
  }

  closeView() {
    this.newCategoryRegisterView.close();
  }

  getData(value) {
    this.allInterests.push(value);
    this.reloadItems(this.allInterests);
  }

  reloadItems(params): void {
    if (this.itemResource) {
      this.itemResource.query(params).then(items => this.items = items);
    }
  }

}
