import { TranslateService } from '@ngx-translate/core';
import { Extension } from './../../../../shared/Utills/Extensions';
import { filter } from 'rxjs/operators';
import { UsbModalRef } from './../../../../shared/components/us-modal/modal-ref';
import { ShopHomeService } from './../../../shop/shop-home/shop-home.service';
import { ExcePdfViewerService } from './../../../../shared/components/exce-pdf-viewer/exce-pdf-viewer.service';
import { PreloaderService } from './../../../../shared/services/preloader.service';
import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { DataTableResource } from '../../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from '../../../../shared/components/us-data-table/tools/data-table-filter-pipe'
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
import { ExceMemberService } from '../../services/exce-member.service'
import { McBasicInfoService } from './../mc-basic-info/mc-basic-info.service';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { ExceLoginService } from '../../../../modules/login/exce-login/exce-login.service';
import { AdminService } from '../../../admin/services/admin.service'
import { UsErrorService } from '../../../../shared/directives/us-error/us-error.service'
import * as moment from 'moment';
import { error } from 'util';
// import value from '*.json';
import { Subscription, Subject } from 'rxjs';
import { takeUntil, mergeMap } from 'rxjs/operators';
const now = new Date();

@Component({
  selector: 'app-mc-visit',
  templateUrl: './mc-visit.component.html',
  styleUrls: ['./mc-visit.component.scss']
})
export class McVisitComponent implements OnInit, OnDestroy {
  trailDate: any;
  countVisitAfter: any;
  shopModal: any;
  shopView: UsbModalRef;
  private filteredVisits: any;
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  private modalReference?: any;
  private memberBookingForm: any;
  private printVisitsForm: any;
  private printResult: any;
  private selectedMember: any;
  private today: NgbDateStruct;
  private monthbefore: NgbDateStruct;
  activities: any
  branches: any
  private todayString: string;
  public visits: any;
  public visitCount = 0;
  private visitResource: any;
  private visitList: any;
  private visitsTaken = 0
  private countForm: any
  private deletingItem: any
  private model: any;
  private addVisitForm: any;
  private contracts: any;
  private selectedContract: any;
  private branchId: number
  private gymSettings: any;
  public disableSince: any;
  private tomorrow: any
  public timeConfig;
  private selectedDate: any
  public formSubmited = false;
  private accessTimeObj: any
  selectedActivity: any;
  private destroy$ = new Subject<void>();
  formErrors = {
    'VisitDate': '',
    'InTime': ''
  };

  validationMessages = {
    'VisitDate': {
      'required': 'MEMBERSHIP.Required',
    },

    'InTime': {
      'required': 'Time is required.',
      'inTime': 'MEMBERSHIP.VisitVWCantAddFutureDates'
    }
  };
  shopvisitsubscribe: Subscription;
  yeasSubscribe: Subscription;
  visitSub: any;

  constructor(private exceMemberService: ExceMemberService,
    private basicInfoService: McBasicInfoService,
    private modalService: UsbModal,
    private fb: FormBuilder,
    private exceMessageService: ExceMessageService,
    private exceLoginService: ExceLoginService,
    private adminService: AdminService,
    private excePdfViewerService: ExcePdfViewerService,
    private shopHomeService: ShopHomeService,
    private translationService: TranslateService
  ) {

    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.pipe(takeUntil(this.destroy$)).subscribe(
      (branch) => {
        this.branchId = branch.BranchId;
      });

    this.timeConfig = { 'is24hourFormat': true }
    this.today = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
    this.tomorrow = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() + 1 };
    const lastMonth = this.dateAdd(now, -1, 'months');
    this.monthbefore = { year: lastMonth.getFullYear(), month: lastMonth.getMonth() + 1, day: lastMonth.getDate() };
    this.disableSince = { disableSince: this.tomorrow }
    let hours, minuites, seconds;

    if (now.getHours().toString().length < 2) {
      hours = '0' + now.getHours()
    } else {
      hours = now.getHours()
    }

    if (now.getMinutes().toString().length < 2) {
      minuites = '0' + now.getMinutes()
    } else {
      minuites = now.getMinutes()
    }

    if (now.getSeconds().toString().length < 2) {
      seconds = '0' + now.getSeconds()
    } else {
      seconds = now.getSeconds()
    }

    this.todayString = this.today.year + '-' + this.today.month + '-' + this.today.day + 'T' + hours + ':' + minuites + ':' + seconds;
    // this.addVisitForm = this.fb.group({
    //   'VisitDate': [{ date: this.today }, [Validators.required]],
    //   'InTime': [nowString, [Validators.required]],
    //   'Activity': [this.selectedActivity]
    // });

    //this.addVisitForm.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(_ => UsErrorService.onValueChanged(this.addVisitForm, this.formErrors, this.validationMessages));

    this.printVisitsForm = this.fb.group({
      'fromDate': [{ date: this.monthbefore }],
      'toDate': [{ date: this.today }],
    });

    this.countForm = this.fb.group({
      'fromDate': [{ date: this.monthbefore }],
      'toDate': [{ date: this.today }],
    });

    this.exceMessageService.yesCalledHandle.pipe(
      mergeMap(val => {
        if (val.Id === 'DELETE_VISIT') {
          return this.exceMemberService.DeleteMemberVisit({
            'memberVisitId': this.deletingItem.Id,
            'memberContractId': this.deletingItem.MemberContractId
          })
        } else if (val.Id === 'SHOPVISITS') {
          this.openShopMOdal();
        }
      }),
      mergeMap(memberVisits => this.exceMemberService.getMemberVisits(this.selectedMember.Id, this.todayString, this.selectedMember.BranchId)),
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.visitList = result.Data.VisitList;
        this.visits = result.Data.VisitList;
        this.trailDate = result.Data.TrailDate;
        this.visitResource = new DataTableResource(this.visits);
        this.visitResource.count().then(count => this.visitCount = count);
        this.visitCount = Number(this.visits.length);
      }
    });

    // this.exceMessageService.yesCalledHandle.mergeMap( val => {
    //   if (val.Id === 'DELETE_VISIT') {
    //     return this.exceMemberService.DeleteMemberVisit({
    //       'memberVisitId': this.deletingItem.Id,
    //       'memberContractId': this.deletingItem.MemberContractId
    //     })
    //   } else if (val.Id === 'SHOPVISITS') {
    //     this.openShopMOdal();
    //   }
    // }).mergeMap(memberVisits => {
    //   return this.exceMemberService.getMemberVisits(this.selectedMember.Id, this.todayString, this.selectedMember.BranchId).pipe(
    //     takeUntil(this.destroy$)
    //   )
    // }).pipe(
    //   takeUntil(this.destroy$)
    //   ).subscribe(result => {
    //     if (result) {
    //       this.visitList = result.Data.VisitList;
    //       this.visits = result.Data.VisitList;
    //       this.trailDate = result.Data.TrailDate;
    //       this.visitResource = new DataTableResource(this.visits);
    //       this.visitResource.count().then(count => this.visitCount = count);
    //       this.visitCount = Number(this.visits.length);
    //     }
    //   });

/*     this.exceMessageService.yesCalledHandle.pipe(takeUntil(this.destroy$)).subscribe((val) => {
      if (val.id === 'DELETE_VISIT') {
        this.deleteHandler();
      } else if (val.id === 'SHOPVISITS') {
        // opern the shop
        this.openShopMOdal();
      }
    }); */

    this.exceMessageService.okCalledHandle.pipe(takeUntil(this.destroy$)).subscribe((val) => {
      if (val.id === 'COUNT_VISITS') {
      } else if (val.id === 'NO_VISITS') {
      }
    });

  }

  ngOnInit() {
    const VisitNow = moment();
    this.addVisitForm = this.fb.group({
      'VisitDate': [{ date: { year: VisitNow.format('YYYY'), month: VisitNow.format('M'), day: VisitNow.format('D') } }],
      'InTime': [VisitNow.format('HH:mm'), [Validators.required]],
      'Activity': [this.selectedActivity]
    });
    this.addVisitForm.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(_ => UsErrorService.onValueChanged(this.addVisitForm, this.formErrors, this.validationMessages));
    this.basicInfoService.currentMember.pipe(takeUntil(this.destroy$)).subscribe(
      member => {
        if (member) {
          this.selectedMember = member;
          this.getMemberVisits();
          this.getGymSettings();
          this.exceMemberService.getBranches().pipe(takeUntil(this.destroy$)).subscribe(result => {
            if (result) {
              this.branches = result.Data;
            } else {
            }
          });
          this.exceMemberService.getActivityCategories(this.branchId).pipe(takeUntil(this.destroy$)).subscribe(result => {
            if (result) {
              this.activities = result.Data;
              this.selectedActivity = this.activities.filter(X => X.Code === 'Trening')[0];
            } else {
            }
          });
          this.exceMemberService.GetContractSummaries(this.selectedMember.Id, this.selectedMember.BranchId, false, false).pipe(takeUntil(this.destroy$)).subscribe(result => {
            if (result) {
              this.contracts = result.Data;
            } else {
            }
          })
        }
      });
  }

  ngOnDestroy () {
    this.destroy$.next();
    this.destroy$.complete();
    if (this.shopView) {
      this.shopView.close();
    }
    if (this.visitSub) {
      this.visitSub.unsubscribe();
    }
    if (this.modalReference) {
      this.modalReference.close();
    }
  }

  getMemberVisits() {
    this.exceMemberService.getMemberVisits(this.selectedMember.Id, this.todayString, this.selectedMember.BranchId).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          this.visitList = result.Data.VisitList;
          this.visits = result.Data.VisitList;
          this.trailDate = result.Data.TrailDate;
          this.visitResource = new DataTableResource(this.visits);
          this.visitResource.count().then(count => this.visitCount = count);
          this.visitCount = Number(this.visits.length);
        } else {
        }
      })
  }

  openShopMOdal() {
    this.shopView = this.modalService.open(this.shopModal);
    this.shopHomeService.$isCancelBtnHide = true;
    this.selectedMember.BranchID = this.selectedMember.BranchId;
    this.shopHomeService.$selectedMember = this.selectedMember;
    this.shopHomeService.$isCancelBtnHide = true;
    this.selectedMember.RoleId = this.selectedMember.Role;
    this.shopHomeService.memberSelectedEvent.next(this.selectedMember);
    const shopTrialData = {
      StatusCode: 1,
      ArticleId: -1,
      ItemName: ''
    };
    this.shopHomeService.$shopOpenedFromVisitRegistration = shopTrialData;


    this.shopHomeService.visitRegistrationChangedEmitter().pipe(takeUntil(this.destroy$)).subscribe(
      val => {
        Object.assign(shopTrialData, val);
        // shopTrialData = val;
      if (shopTrialData.StatusCode === 2 ) {
        this.shopView.close()
        this.visitSub.unsubscribe();
        const formValue = this.addVisitForm.value;
        const addVisitObj: any = {};
        const dataString = formValue.VisitDate.date.year + '-' + formValue.VisitDate.date.month + '-' + formValue.VisitDate.date.day
          addVisitObj.BranchId = this.branchId;
          addVisitObj.MemberContractId = -1;
          addVisitObj.ItemName = shopTrialData.ItemName;
          addVisitObj.ActivityId = formValue.ActivityId;
          addVisitObj.ArticleID = shopTrialData.ArticleId;
          addVisitObj.VisitType = 'TRAIL';
          addVisitObj.CountVist = true;
          addVisitObj.InTime = dataString + 'T' + formValue.InTime;
          addVisitObj.VisitDate = dataString;
          addVisitObj.EntityId = this.selectedMember.Id;
          this.saveVisit(addVisitObj, 'TRAIL');
          setTimeout(() => {
            this.shopHomeService.$shopOpenedFromVisitRegistration = {
              StatusCode: 0,
              ArticleId: -1,
              ItemName: ''
            }
          }, 2000)
          this.shopView.close();
        ;
      }
      if (shopTrialData.StatusCode === 3 ) {
        const formValue = this.addVisitForm.value;
        const addVisitObj: any = {};
        const dataString = formValue.VisitDate.date.year + '-' + formValue.VisitDate.date.month + '-' + formValue.VisitDate.date.day
          addVisitObj.BranchId = this.branchId;
          addVisitObj.MemberContractId = -1;
          addVisitObj.ItemName = shopTrialData.ItemName;
          addVisitObj.ActivityId = formValue.ActivityId;
          addVisitObj.ArticleID = shopTrialData.ArticleId;
          addVisitObj.VisitType = 'DROPIN';
          addVisitObj.CountVist = true;
          addVisitObj.InTime = dataString + 'T' + formValue.InTime;
          addVisitObj.VisitDate = dataString;
          addVisitObj.EntityId = this.selectedMember.Id;
          this.saveVisit(addVisitObj, 'DROPIN');
          setTimeout(() => {
            this.shopHomeService.$shopOpenedFromVisitRegistration = {
              StatusCode: 0,
              ArticleId: -1,
              ItemName: ''
            }
          }, 2000)
        this.shopView.close();
      } else if (shopTrialData.StatusCode === 4) {
        this.model = this.exceMessageService.openMessageBox('ERROR',
        {
          messageTitle: 'Besøk ble ikke registrert',
          messageBody: 'Ingen av varene var av typen TRIAL eller DROPIN',
          msgBoxId: ' NO_TRIAL_OR_DROPIN'
        });
        this.shopView.close();
        setTimeout(() => {
          this.shopHomeService.$shopOpenedFromVisitRegistration = {
            StatusCode: 0,
            ArticleId: -1,
            ItemName: ''
          }
        }, 2000)
      }else {
      }
    }
    );

  }


  getGymSettings() {
    const gymSettingColumnList = []
    gymSettingColumnList.push('EcoNegativeOnAccount');
    gymSettingColumnList.push('EcoSMSInvoiceShop');
    gymSettingColumnList.push('EcoIsPrintInvoiceInShop');
    gymSettingColumnList.push('EcoIsPrintReceiptInShop');
    gymSettingColumnList.push('EcoIsShopOnNextOrder');
    gymSettingColumnList.push('EcoIsOnAccount');
    gymSettingColumnList.push('EcoIsDefaultCustomerAvailable');
    gymSettingColumnList.push('EcoIsPayButtonsAvailable');
    gymSettingColumnList.push('EcoIsPettyCashSameAsUserAllowed');
    gymSettingColumnList.push('OtherIsShopAvailable');
    gymSettingColumnList.push('OtherLoginShop');
    gymSettingColumnList.push('HwIsShopLoginWithCard');
    gymSettingColumnList.push('ReqMemberListRequired');
    gymSettingColumnList.push('OtherIsLogoutFromShopAfterSale');
    gymSettingColumnList.push('EcoIsBankTerminalIntegrated');
    gymSettingColumnList.push('EcoIsDailySettlementForAll');
    gymSettingColumnList.push('OtherIsValidateShopGymId');
    gymSettingColumnList.push('AccCountVisitAfter');

    this.adminService.GetSelectedGymSettingsPost({
      'GymSetColNames': gymSettingColumnList,
      'IsGymSettings': true,
      'BranchId': this.branchId
    }).pipe(takeUntil(this.destroy$)).subscribe(result => {
        if (result) {
          this.gymSettings = result.Data;
          this.countVisitAfter = this.gymSettings.AccCountVisitAfter;
        } else {
        }
      });
  }

  filterInput(branchValue?: any) {
    branchValue = branchValue.trim();
    this.filteredVisits = this.visitList
    if (branchValue !== 'ALL') {
      this.filteredVisits = this.filterpipe.transform('GymName', 'SELECT', this.filteredVisits, branchValue.split('_')[1]);
    }
    this.visitResource = new DataTableResource(this.filteredVisits);
    this.visitResource.count().then(count => this.visitCount = count);
    this.reloadItems(this.filteredVisits);
  }

  reloadItems(params): void {
    if (this.visitResource) {
      this.visitResource.query(params).then(items => this.visits = items);
    }
  }
  openVisitsModel(content: any) {
    this.modalReference = this.modalService.open(content, { width: '400' });
  }

  openPrintModel(content: any) {
    this.modalReference = this.modalService.open(content, { width: '300' });
  }

  closeForm() {
    this.modalReference.close();
  }

  printVisits(printData: any) {
    const fromDate = printData.fromDate.date.year + '-' + printData.fromDate.date.month + '-' + printData.fromDate.date.day;
    const toDate = printData.toDate.date.year + '-' + printData.toDate.date.month + '-' + printData.toDate.date.day;
    this.exceMemberService.PrintMemberVisits(this.selectedMember.Id, this.branchId, fromDate, toDate).pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        PreloaderService.hidePreLoader();
        if (result.Data.FileParth.trim() !== '') {
          this.excePdfViewerService.openPdfViewer('Exceline', result.Data.FileParth);
        }
        this.closeForm();
      }
    });
  }

  count(countValue: any) {
    const fromDate = countValue.fromDate.date.year + '-' + countValue.fromDate.date.month + '-' + countValue.fromDate.date.day;
    const toDate = countValue.toDate.date.year + '-' + countValue.toDate.date.month + '-' + countValue.toDate.date.day;
    this.exceMemberService.GetVisitCount(this.selectedMember.Id, fromDate, toDate).pipe(takeUntil(this.destroy$)).subscribe(
    result => {
      if (result) {
        this.visitsTaken = result.Data
      } else {
      }
    });
  }

  deleteSetting(deletingItem) {
    this.deletingItem = deletingItem;
    this.openExceDeleteMessage('Confirm', 'MEMBERSHIP.VisitVWConfirmDeletion')
  }

  openExceDeleteMessage(title, body) {
    this.model = this.exceMessageService.openMessageBox('CONFIRM', {
      messageTitle: 'COMMON.ViewAccounts',
      messageBody: body,
      msgBoxId: 'DELETE_VISIT'
    });
  }

  deleteHandler() {
    this.exceMemberService.DeleteMemberVisit({
      'memberVisitId': this.deletingItem.Id,
      'memberContractId': this.deletingItem.MemberContractId
    }).pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.getMemberVisits()
      } else {

      }
    });
  }

  addLogic(logicValue) {
    logicValue.ActivityId = this.selectedActivity.Id;
    const dataString = logicValue.VisitDate.date.year + '-' + logicValue.VisitDate.date.month + '-' + logicValue.VisitDate.date.day
    const vdateString = logicValue.VisitDate.date.year + '/' + logicValue.VisitDate.date.month +
      '/' + logicValue.VisitDate.date.day
    const visitDateObj = Date.parse(vdateString + ' 00:00:00')
    const addVisitObj = {
      'BranchId': -1, 'MemberContractId': '', 'ItemName': '', 'ActivityId': '', 'VisitType': '', 'CountVist': false, 'InTime': '', 'VisitDate': '', 'EntityId': ''
    };
    for (const contract of this.contracts) {
      if (contract) {
        const startDateObj = Date.parse(contract.StartDate.replace('T', ' ').replace(new RegExp('-', 'g'), '/'))
        const endDateObj = Date.parse(contract.EndDate.replace('T', ' ').replace(new RegExp('-', 'g'), '/'))
        contract.IsFreezed = false;
        contract.IsFreezed = false;
        contract.ContractFreezList.forEach(contractfreez => {
          const contractFreezFromDate = Date.parse(contractfreez.FromDate.replace('T', ' ').replace(new RegExp('-', 'g'), '/'))
          const contractFreezToDate = Date.parse(contractfreez.ToDate.replace('T', ' ').replace(new RegExp('-', 'g'), '/'))
          if (contractFreezFromDate <= logicValue.VisitDate && contractFreezToDate >= logicValue.VisitDate) {
            contract.IsFreezed = true;
          }
        });
        if (contract.AccessProfileId > 0) {
          if (!contract.IsFreezed && startDateObj <= visitDateObj && endDateObj >= visitDateObj
            && logicValue.ActivityId != null && contract.ActivityId === logicValue.ActivityId) {
            this.GetAccessTime(contract, logicValue.VisitDate);
            if (this.accessTimeObj != null) {
              if ((Date.parse('01/01/2011 ' + logicValue.InTime) >= Date.parse('01/01/2011 ' + this.accessTimeObj.InTime.split('T')[1])) &&
                (Date.parse('01/01/2011 ' + logicValue.InTime) < Date.parse('01/01/2011 ' + this.accessTimeObj.OutTime.split('T')[1]))
              ) {
                this.selectedContract = contract;
                break;
              }
            }
          }
        } else {
          if (!contract.IsFreezed && startDateObj <= visitDateObj && endDateObj >= visitDateObj
            && logicValue.ActivityId != null && contract.ActivityId === logicValue.ActivityId) {
            this.selectedContract = contract;
            break;
          }
        }
      }
    }
    if (!this.selectedContract) {
      for (const contract of this.contracts) {
        if (contract) {
          const startDateObj = Date.parse(contract.StartDate.replace('T', ' ').replace(new RegExp('-', 'g'), '/'))
          const endDateObj = Date.parse(contract.EndDate.replace('T', ' ').replace(new RegExp('-', 'g'), '/'))
          contract.IsFreezed = false;
          contract.ContractFreezList.forEach(contractfreez => {
            const contractFreezFromDate = Date.parse(contractfreez.FromDate.replace('T', ' ').replace(new RegExp('-', 'g'), '/'))
            const contractFreezToDate = Date.parse(contractfreez.ToDate.replace('T', ' ').replace(new RegExp('-', 'g'), '/'))
            if (contractFreezFromDate <= visitDateObj && contractFreezToDate >= visitDateObj) {
              contract.IsFreezed = true;
            }
          });
          if (contract.AccessProfileId === 0 || contract.AccessProfileId < 0) {
            if (!contract.IsFreezed && startDateObj <= visitDateObj && endDateObj >= visitDateObj
              && logicValue.ActivityId != null && contract.ActivityId === logicValue.ActivityId) {
              this.GetAccessTime(contract, logicValue.VisitDate);
              if (this.accessTimeObj != null) {

                if ((Date.parse('01/01/2011 ' + logicValue.InTime) >= Date.parse('01/01/2011 ' + this.accessTimeObj.InTime.split('T')[1])) &&
                  (Date.parse('01/01/2011 ' + logicValue.InTime) < Date.parse('01/01/2011 ' + this.accessTimeObj.OutTime.split('T')[1]))
                ) {
                  this.selectedContract = contract;
                  break;
                }
              }
            }
          }
        }
      }
    }

    if (this.selectedContract) {
      addVisitObj.BranchId = this.branchId;
      addVisitObj.MemberContractId = this.selectedContract.Id;
      addVisitObj.ItemName = this.selectedContract.TemplateName;
      addVisitObj.ActivityId = this.selectedContract.ActivityId;
      addVisitObj.VisitType = 'CON';
      addVisitObj.CountVist = true;
      addVisitObj.InTime = dataString + 'T' +  logicValue.InTime.substring(0, 8);
      addVisitObj.VisitDate = dataString;
      addVisitObj.EntityId = this.selectedMember.Id;
      if (this.selectedContract.ContractTypeCode === 'PUNCHCARD') {
        if (this.selectedContract.RemainingVisits <= 0) {
          this.openRemainingVisitsMessage('Confirm', 'MEMBERSHIP.MsgNovisits')
        } else {
          this.saveVisit(addVisitObj, 'CON');
        }
      } else {
        this.saveVisit(addVisitObj, 'CON');
      }
    } else {
      this.openShopOpenMessage('Confirm', 'MEMBERSHIP.VisitConfirmWithShop')
    }
  }

  openRemainingVisitsMessage(title, body) {
    this.model = this.exceMessageService.openMessageBox('WARNING', {
      messageTitle: 'title',
      messageBody: body,
      msgBoxId: ' NO_VISITS'
    });
  }

  openShopOpenMessage(title, body) {
    this.model = this.exceMessageService.openMessageBox('CONFIRM', {
      messageTitle: 'title',
      messageBody: body,
      msgBoxId: 'SHOPVISITS'
    });
  }

  manipulatedate(dateInput: any) {
    if (dateInput.date !== undefined) {
      const tempDate: Date = new Date(dateInput.date.year, dateInput.date.month - 1, dateInput.date.day);
      return moment(tempDate).format('YYYY-MM-DD').toString();
    }
  }

  GetAccessTime(contract, VisitDate) {
    const visitDate = this.manipulatedate(VisitDate);
    const dayOfWeek = moment(visitDate).weekday(); //   VisitDate.jsdate.toString().split(' ')[0]
    let aTime: any;
    switch (dayOfWeek) {
      case 1:
        contract.AccesTimes.forEach(element => {
          if (element.Day === 'Monday') {
            aTime = element;
          }
        });
        break;

      case 2:
        contract.AccesTimes.forEach(element => {
          if (element.Day === 'Tuesday') {
            aTime = element;
          }
        });
        break;

      case 3:
        contract.AccesTimes.forEach(element => {
          if (element.Day === 'Wednesday') {
            aTime = element;
          }
        });
        break;

      case 4:
        contract.AccesTimes.forEach(element => {
          if (element.Day === 'Thursday') {
            aTime = element;
          }
        });
        break;
      case 5:
        contract.AccesTimes.forEach(element => {
          if (element.Day === 'Friday') {
            aTime = element;
          }
        });
        break;
      case 6:
        contract.AccesTimes.forEach(element => {
          if (element.Day === 'Saturday') {
            aTime = element;
          }
        });
        break;
      case 7:
        contract.AccesTimes.forEach(element => {
          if (element.Day === 'Sunday') {
            aTime = element;
          }
        });
        break;
    }
    this.accessTimeObj = aTime
    return aTime;
  }

  saveVisit(addObj, visitType) {
    let isValid = true;
    const entryD: string = addObj.InTime.replace('T', ' ');

    if (this.visits) {
      const todayList = []
      this.visits.forEach(element => {
        if (moment(element.VisitDate).isSame(moment(addObj.VisitDate))) {
          todayList.push(element);
        }
      });
      todayList.forEach(element => {
        const visitD: string = element.InTime.replace('T', ' ');
        const diffInMs: number = Date.parse(entryD) - Date.parse(visitD);
        const diffInMins: number = diffInMs / 1000 / 60;
        if (diffInMins > 0) {
          if (diffInMins <= (this.gymSettings.AccCountVisitAfter * 60)) {
          // isValid = false;
          }
        } else if (diffInMins < 0) {
          if ((diffInMins * -1) <= (this.gymSettings.AccCountVisitAfter * 60)) {
          //  isValid = false;
          }
        }
      });
    }

    if (isValid) {
      this.exceMemberService.SaveMemberVisit(addObj).pipe(takeUntil(this.destroy$)).subscribe(result => {
        if (result) {
          this.getMemberVisits();
          if (this.selectedContract.ContractTypeCode !== undefined) {
            if (this.selectedContract.ContractTypeCode === 'PUNCHCARD') {
              this.contracts = this.contracts.filter(item => item !== this.selectedContract);
              this.selectedContract.RemainingVisits -= 1;
              this.contracts.push(this.selectedContract)
          }
        }
          this.selectedContract = ''
        } else {
        }
      })
    } else {
      let transError: string;
      let hrs: string;
      this.translationService.get('MEMBERSHIP.MsgVisitRangeError').pipe(takeUntil(this.destroy$)).subscribe(transValue => transError = transValue);
      this.translationService.get('MEMBERSHIP.Hrs').pipe(takeUntil(this.destroy$)).subscribe(transValue => hrs = transValue);
      this.countVisitAfterMessage('Error', error + ' ' + this.gymSettings.AccCountVisitAfter + ' ' + hrs);
    }
  }

  countVisitAfterMessage(title, body) {
    this.model = this.exceMessageService.openMessageBox('WARNING', {
        messageTitle: title,
        messageBody: body,
        msgBoxId: ' COUNT_VISITS'
      }
    );
  }

  onActivitySelected(activity) {
    this.selectedActivity = activity;
  }

  countVisitHandler() {
  }

  timeValidator(dateValue) {
    const datenow = new Date();
    if (dateValue.VisitDate && dateValue.InTime) {
      const dateString = dateValue.VisitDate.day + '/' + dateValue.VisitDate.month + '/' + dateValue.VisitDate.year
      const noWString = datenow.getDate() + '/' + (datenow.getMonth() + 1) + '/' + datenow.getFullYear()

      if (dateValue.VisitDate.date.toString() === this.today.toString()) {
        const nowTime = datenow.getHours() + ':' + datenow.getMinutes() + ':' + datenow.getSeconds()
        const selectedTime = dateValue.InTime + ':00'

        if (Date.parse('01/01/2017 ' + nowTime) < Date.parse('01/01/2017 ' + selectedTime)) {
          this.addVisitForm.controls['InTime'].setErrors({
            'inTime': false
          });
        }
      }
    }
  }

  submitForm(formValue, content) {
    this.shopModal = content;
    this.formSubmited = true;
    if (this.addVisitForm.valid) {
      this.addLogic(formValue)
      this.formSubmited = false;
    } else {
      UsErrorService.validateAllFormFields(this.addVisitForm, this.formErrors, this.validationMessages);
    }
  }

  dateAdd(startDate: any, count: any, interval: string): Date {
    return moment(startDate).add(count, interval).toDate()
  }

  closeShop(event) {
    this.shopView.close();
 /*
    if (this.shopView) {
      // this.shopView.dismiss()
      this.shopView.close();
      if (event) {
        if (event.saleStatus === true) {
          let dataString;
          let value = this.addVisitForm.value;
          value.ActivityId = this.selectedActivity.Id;
          let addVisitObj: any = {};
          dataString = value.VisitDate.date.year + '-' + value.VisitDate.date.month + '-' + value.VisitDate.date.day
          if (event.articlesPaid[0].CategoryCode == 'TRAIL') {
            addVisitObj.BranchId = this.branchId;
            addVisitObj.MemberContractId = -1;
            addVisitObj.ItemName = event.articlesPaid[0].ItemName;
            addVisitObj.ActivityId = value.ActivityId;
            addVisitObj.ArticleID = event.articlesPaid[0].ArticleId;
            addVisitObj.VisitType = 'TRAIL';
            addVisitObj.CountVist = true;
            addVisitObj.InTime = dataString + 'T' + value.InTime;
            addVisitObj.VisitDate = dataString;
            addVisitObj.EntityId = this.selectedMember.Id;
            this.saveVisit(addVisitObj, 'TRAIL');
          } else if (event.articlesPaid[0].CategoryCode == 'DROPIN') {
            addVisitObj.BranchId = this.branchId;
            addVisitObj.MemberContractId = -1;
            addVisitObj.ItemName = event.articlesPaid[0].ItemName;
            addVisitObj.ActivityId = value.ActivityId;
            addVisitObj.ArticleID = event.articlesPaid[0].ArticleId;
            addVisitObj.VisitType = 'DROPIN';
            addVisitObj.CountVist = true;
            addVisitObj.InTime = dataString + 'T' + value.InTime;
            addVisitObj.VisitDate = dataString;
            addVisitObj.EntityId = this.selectedMember.Id;
            addVisitObj.InvoiceRerefence = event.arItemNo;
            this.saveVisit(addVisitObj, 'DROPIN');
          }
        }
      }
    }*/
  }
}
