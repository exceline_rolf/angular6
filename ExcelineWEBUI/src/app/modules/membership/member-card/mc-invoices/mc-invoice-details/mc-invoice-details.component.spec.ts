import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { McInvoiceDetailsComponent } from './mc-invoice-details.component';

describe('McInvoiceDetailsComponent', () => {
  let component: McInvoiceDetailsComponent;
  let fixture: ComponentFixture<McInvoiceDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ McInvoiceDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(McInvoiceDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
