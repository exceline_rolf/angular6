import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { McEventLogComponent } from './mc-event-log.component';

describe('McEventLogComponent', () => {
  let component: McEventLogComponent;
  let fixture: ComponentFixture<McEventLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ McEventLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(McEventLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
