import { Component, ViewChild, OnInit, OnDestroy, AfterContentInit } from '@angular/core';
import { McBasicInfoService } from './mc-basic-info.service';
import { FormGroup, FormBuilder, AbstractControl, Validators, ValidationErrors } from '@angular/forms';
import { ExceMemberService } from '../../services/exce-member.service';
import { DataGridSampleComponent } from '../../../sample/data-grid-sample/data-grid-sample.component';
import { PostalArea } from '../../../../shared/SystemObjects/Common/PostalArea';
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
import { configFactory } from '../../../../app.module';
import { IMyDpOptions } from '../../../../shared/components/us-date-picker/interfaces';
import { ExcelineMember } from '../../models/ExcelineMember';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { UsErrorService } from 'app/shared/directives/us-error/us-error.service';
import { TranslateService } from '@ngx-translate/core';
import { MemberCardService } from 'app/modules/membership/member-card/member-card.service';
import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { AdminService } from '../../../admin/services/admin.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MemberRole } from 'app/shared/enums/us-enum';
import { ExceBreadcrumbService } from '../../../../shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { Subject } from 'rxjs'
import { takeUntil, flatMap, mergeMap } from 'rxjs/operators';


@Component({
  selector: 'mc-basic-info',
  templateUrl: './mc-basic-info.component.html',
  styleUrls: ['./mc-basic-info.component.scss']
})
export class McBasicInfoComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private selectedStatus: any;
  private memberBirthdate: any;
  private loggedBranchId: number;
  private popOver: any;
  private imageUploaderModal: any;
  private isMobileNumberValid = true;
  private isPrivateNumberValid = true;
  private isWorkNumberValid = true;

  isMemberCardBlur = false;
  emailEditorValue: any;
  emailEditorModalReference: any;
  emailEditorContent: any;
  noCallHandler: any;
  dataUpdateMember: any;
  selectedMember: ExcelineMember;
  postalData?: PostalArea = new PostalArea();
  public memberForm: FormGroup;
  public postalDataForm: FormGroup;
  myFormSubmited = false;
  countries: any;
  isEditMode: boolean;
  notifiMethods: any;
  memberStatus: any;
  postalDataView: any;
  smsConfirmationView: any;
  smsErrorView: any;
  smsTemplateView: any;
  _editImage = false;
  memberCategoies: any;
  smsTemplates: any;
  smsText = '';
  deleteButtonEnabled = true;
  gymSettings: any;
  isMobileBlur = false;
  isPrivateBlur = false;
  isWorkBlur = false;
  phoneNumberDetail: string;
  mobileExitMsg: string;
  listOfCountryId = [];
  smsSettings: any;
  currentId: any;
  currentName: any;
  intlOptions: { initialCountry: string; formatOnDisplay: boolean; separateDialCode: boolean; onlyCountries: string[]; autoPlaceholder: string; };
  isUsingBrisIntegration: boolean;
  countrIdMatch = 'no';
  type: string
  message = 'OK';
  private _componentName = 'mc-basic-info-component';

  selectedGymsettings: string[] = [
    'ReqAddressRequired',
    'ReqMobileRequired',
    'ReqZipCodeRequired',
    'OtherIsShopAvailable',
    'OtherPhoneCode',
    'EcoNoOfOrdersResigning',
    'EcoIsMemberFee']

  @ViewChild('message') public takenMSG;

  formErrors = {
    'Status': '',
    'LastName': '',
    'FirstName': '',
    'CompanyName': '',
    'Address1': '',
    'Address2': '',
    'PostCode': '',
    'PostPlace': '',
    'country': '',
    'gender': '',
    'MemberCardNo': '',
    'MobilePrefix': '',
    'Mobile': '',
    'PrivateTeleNoPrefix': '',
    'PrivateTeleNo': '',
    'WorkTeleNoPrefix': '',
    'WorkTeleNo': '',
    'Email': '',
    'AccountingEmail': '',
    'VatNumber': '',
    'BirthDate': '',
    'Role': ''
  };

  validationMessages = {
    'gender': {
      'required' : 'MEMBERSHIP.MsgMemGenderReq'
    },
    'LastName': {
      'required': 'MEMBERSHIP.MsgMemLastNamerequied'
    },
    'FirstName': {
      'required': 'MEMBERSHIP.MsgMemFirstNamerequired'
    },
    'CompanyName': {
      'required': 'MEMBERSHIP.MsgComapnyNameRequired'
    },
    'Email': {
      'email': 'MEMBERSHIP.MsgInvalidEmail',
      'pattern': 'MEMBERSHIP.EmailPattern',
      'required': 'MEMBERSHIP.EmailReq'
    },
    'AccountingEmail': {
      'email': 'MEMBERSHIP.MsgInvalidEmail'
    },
    'Address1': {
      'required': 'MEMBERSHIP.MsgMemAddressrequied'
    },
    'PostCode': {
      'required': 'MEMBERSHIP.crPostalCodeReq'
    },
    'Mobile': {
      'required': 'MEMBERSHIP.crMobileReq',
      'pattern': 'MEMBERSHIP.numOnlynums',
      'mobileTaken': 'MEMBERSHIP.NumberOccupied'
    },
    'MemberCardNo': {
      'pattern': 'MEMBERSHIP.onlynums',
      'memberCardTaken': 'MEMBERSHIP.NumberOccupied'
    }
  };
  _memberID: any;
  _branchID: any;
  _memberRole: any;
  isShowInfo: boolean;


  constructor(
    private basicInfoService: McBasicInfoService,
    private memberService: ExceMemberService,
    private fb: FormBuilder,
    private modalService: UsbModal,
    private translateService: TranslateService,
    private memberCardService: MemberCardService,
    private exceMessageService: ExceMessageService,
    private route: ActivatedRoute,
    private adminService: AdminService,
    private router: Router,
    private exceBreadCrumbService: ExceBreadcrumbService,
    private exceMemberService: ExceMemberService
  ) {
    this._memberID = this.route.snapshot.params['Id'];
    this._branchID = this.route.snapshot.params['BranchId'];
    this._memberRole = this.route.snapshot.params['Role'].trim();

    this.memberCardService.guardianDetails.pipe(
      takeUntil(this.destroy$)
      ).subscribe(res => {
      this.dataUpdateMember = res;
      this.selectedMember.GuardianCustId = this.dataUpdateMember.GuardianCustId;
      this.selectedMember.GuardianName = this.dataUpdateMember.GuardianName;
    })

    this.exceMessageService.yesCalledHandle.pipe(
      takeUntil(this.destroy$)
      ).subscribe((value) => {
        if (value.id === 'MSG_EMAIL') {
          this.yesHandler();
        }
    });
    this.exceMessageService.noCalledHandle.pipe(
      takeUntil(this.destroy$)
      ).subscribe((value) => {
        if (value.id === 'MSG_EMAIL') {
          this.noHandler();
        }
    });
  }

  ngOnInit() {
    this.loggedBranchId = JSON.parse(Cookie.get('selectedBranch')).BranchId;
    this.adminService.GetGymCompanySettings('other').pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
        this.isUsingBrisIntegration = result.Data.IsBrisIntegrated;
    });

    this.memberService.getMemberDetails(this._memberID, this._branchID, this._memberRole).pipe(
      flatMap(
        membD => {
          this.basicInfoService.setSelectedMember(membD.Data);
          return  this.basicInfoService.currentMember;
      })
    ).pipe(takeUntil(this.destroy$)).subscribe(member => {
      if (member) {
        this.selectedMember = member;
        this.getCountryData();
        this.manipulateInfoMethods();
        this.setMemberCategory();
        this.validateMemberWithStatus(this.selectedMember.Id, this.selectedMember.MemberStatuse, true);
        if (this.selectedMember.BirthDate) {
          const bDate = new Date(this.selectedMember.BirthDate);
          this.memberBirthdate = { year: bDate.getFullYear(), month: bDate.getMonth() + 1, day: bDate.getDate() }
          this.selectedMember.Age = this.basicInfoService.getMemberAge(bDate);
        }
        this.memberForm = this.fb.group({
          'Status': [this.selectedStatus],
          'LastName': [this.selectedMember.LastName, Validators.required],
          'FirstName': [this.selectedMember.FirstName, Validators.required],
          'CompanyName': [this.selectedMember.CompanyName],
          'Address1': [this.selectedMember.Address1],
          'Address2': [this.selectedMember.Address2],
          'PostCode': [this.selectedMember.PostCode],
          'PostPlace': [this.selectedMember.PostPlace],
          'country': [ this.selectedMember.CountryId],
          'gender': [this.selectedMember.Gender, Validators.required],
          'CompanyEmail': [null],
          'MemberCardNo': [this.selectedMember.MemberCardNo, Validators.pattern('^[0-9]*$')],
          'MobilePrefix': [this.selectedMember.MobilePrefix],
          'Mobile': [this.selectedMember.Mobile, Validators.pattern('^[0-9]{8,12}$')],
          'PrivateTeleNoPrefix': [this.selectedMember.PrivateTeleNoPrefix],
          'PrivateTeleNo': [this.selectedMember.PrivateTeleNo],
          'WorkTeleNoPrefix': [this.selectedMember.WorkTeleNoPrefix],
          'WorkTeleNo': [this.selectedMember.WorkTeleNo],
          'Email': [this.selectedMember.Email, [this.emailValidator.bind(this), Validators.pattern('^[A-Åa-å0-9._%+-]+@[A-Åa-å0-9.-]+\.[A-Åa-å]{2,4}$')]],
          'AccountingEmail': [this.selectedMember.AccountingEmail],
          'VatNumber': [this.selectedMember.VatNumber],
          'BirthDate': [{ date: this.memberBirthdate }],
          'Role': [this.selectedMember.Role]
        });

        this.memberForm.statusChanges.pipe(
          takeUntil(this.destroy$)
        ).subscribe(_ => UsErrorService.onValueChanged(this.memberForm, this.formErrors, this.validationMessages));
        this.fetchData(this.loggedBranchId);
     }
    }, null, null);

    this.postalDataForm = this.fb.group({
      'postalCode': [this.postalData.postalCode, Validators.required],
      'postalName': [this.postalData.postalName, Validators.required],
      'population': [this.postalData.population],
      'houseHold': [this.postalData.houseHold]
    });
    this.basicInfoService.getEditMode().pipe(
      takeUntil(this.destroy$)
      ).subscribe(mode => this.isEditMode = mode);
    this.basicInfoService.getImageEditMode().pipe(
      takeUntil(this.destroy$)
      ).subscribe(imageMode => {
      this._editImage = imageMode;
    });

    this.intlOptions = {
      initialCountry: this.countrIdMatch,
      formatOnDisplay: true,
      separateDialCode: true,
      onlyCountries: this.listOfCountryId,
      autoPlaceholder: 'off'
    };


    this.basicInfoService.getMemberStatus().pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      status => this.memberStatus = status
      );
    this.basicInfoService.getNotifyMethods().pipe(
      takeUntil(this.destroy$)
    ).subscribe(methods => {
      this.notifiMethods = methods;
      this.manipulateInfoMethods();
    }
    );
    this.basicInfoService.getMemberCategories().pipe(
      takeUntil(this.destroy$)
    ).subscribe(categories => {
      this.memberCategoies = categories;
      this.setMemberCategory();
    })

    const getAllStatuses = true;
    this.memberService.getMemberStatus(getAllStatuses).pipe(
      takeUntil(this.destroy$)
    ).subscribe( result => {
        if (result) {
          this.memberStatus = result.Data;
          this.basicInfoService.setMemberStatus(this.memberStatus);
        }
      }, null, () => {

      }
    )

    this.memberService.getCountries().pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      result => {
        if (result) {
          this.countries = result.Data;
          this.basicInfoService.setCountries(this.countries);
        }
      }
    )

    this.memberService.getCategories('NOTIFYMETHOD').pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      result => {
        if (result) {
          this.notifiMethods = result.Data;
          this.basicInfoService.setNotifyMethods(this.notifiMethods);
        }
      }
    )

    this.memberService.getCategories('MEMBER').pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      result => {
        if (result) {
          this.memberCategoies = result.Data;
          this.basicInfoService.setMemberCategoires(this.memberCategoies);
        }
      }
    )

    this.memberService.getSelectedGymSettings({
      settingNames: this.selectedGymsettings,
      isGymSetting: true,
      branchId: this.loggedBranchId
    }).pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      result => {
        if (result) {
          this.gymSettings = result.Data;
          this.basicInfoService.setGymSettings(this.gymSettings);
        }
      }
    )

    // this.basicInfoService.gymSettingEvent.pipe(takeUntil(this.destroy$)).subscribe(
    //   settings => {
    //     this.gymSettings = settings;
    //     if (settings) {
    //       if (this.gymSettings.ReqAddressRequired === true) {
    //         this.memberForm.get('Address1').setValidators(Validators.required);
    //         this.memberForm.get('Address1').updateValueAndValidity();
    //       }
    //       if (this.gymSettings.ReqZipCodeRequired === true) {
    //         this.memberForm.get('PostCode').setValidators(Validators.required);
    //         this.memberForm.get('PostCode').updateValueAndValidity();
    //       }
    //       if (this.gymSettings.ReqMobileRequired === true) {
    //         this.memberForm.get('Mobile').setValidators(Validators.required);
    //         this.memberForm.get('Mobile').updateValueAndValidity();
    //       }
    //     }
    //   });

      // this.fetchData(this.loggedBranchId);

  }

  defaultPrefixSet(type) {
    switch (type) {
      case 'MobilePrefix':
      this.memberForm.patchValue({ MobilePrefix: '+47'})
      break;
      case 'PrivateTeleNoPrefix':
      this.memberForm.patchValue({ PrivateTeleNoPrefix: '+47'})
      break;
      case 'WorkTeleNoPrefix':
      this.memberForm.patchValue({ WorkTeleNoPrefix: '+47'})
      break;
    }
  }

  prefixChange(val, type) {
    if (val.length > 0) {
      const index = val.indexOf('+')
      let valu = val;
      let length = 0;
      if (index === 0) {
        if (val.split('+').length === 2) {
          // nothin to see here, move along
        } else {
          this.defaultPrefixSet(type);
          // this.translateService.get('MEMBERSHIP.PreFixError').pipe(takeUntil(this.destroy$)).subscribe(trans => this.saveMessage = trans);
          // this.type = 'DANGER';
          // this.isShowSaveInfo = true;
          // setTimeout(() => {
          //   this.isShowSaveInfo = false;
          // }, 4000);
        }
        length = val.split('+')[1];
        valu = index;
      } else if (index > 0) {
        this.defaultPrefixSet(type);
        // this.translateService.get('MEMBERSHIP.PreFixError').pipe(takeUntil(this.destroy$)).subscribe(trans => this.saveMessage = trans);
        // this.type = 'DANGER';
        // this.isShowSaveInfo = true;
        // setTimeout(() => {
        //   this.isShowSaveInfo = false;
        // }, 4000);
      } else {
        // missing +
        // this.memberForm.patchValue({ MobilePrefix: '+' + val})
        switch (type) {
          case 'MobilePrefix':
          this.memberForm.patchValue({ MobilePrefix: '+' + val})
          break;
          case 'PrivateTeleNoPrefix':
          this.memberForm.patchValue({ PrivateTeleNoPrefix: '+' + val})
          break;
          case 'WorkTeleNo':
          this.memberForm.patchValue({ WorkTeleNo: '+' + val})
          break;
        }
        // this.translateService.get('MEMBERSHIP.PreFixError').pipe(takeUntil(this.destroy$)).subscribe(trans => this.saveMessage = trans);
        // this.type = 'WARNING';
        // this.isShowSaveInfo = true;
        // setTimeout(() => {
        //   this.isShowSaveInfo = false;
        // }, 4000);

      }
      if (length > 0) {

      }
    } else {
      this.memberForm.patchValue({ MobilePrefix: '+47'})
    }
  }



onCountryChange() {
 // this.selectedMember.CountryName = this.memberForm.getRawValue().country.DisplayName
 this.selectedMember.CountryId = this.memberForm.getRawValue().country
}

fetchData(branch) {
  this.adminService.getGymSettings('SMS', null, branch).pipe(
    takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.smsSettings = result.Data;
        this.currentId = this.smsSettings[0].Id;
        this.currentName = this.smsSettings[0].SenderName;
      }
    });

  this.adminService.getGymSettings('REQUIRED', null, branch).pipe(
    takeUntil(this.destroy$)
  ).subscribe(res => {
    if (res.Data[0].MobileRequired) {
      this.memberForm.get('Mobile').setValidators([Validators.required]);
      this.memberForm.get('Mobile').updateValueAndValidity();
    }
    if (res.Data[0].AddressRequired) {
      this.memberForm.get('Address1').setValidators([Validators.required]);
      this.memberForm.get('Address1').updateValueAndValidity();
    }
    if (res.Data[0].ZipCodeRequired) {
      this.memberForm.get('PostCode').setValidators([Validators.required]);
      this.memberForm.get('PostCode').updateValueAndValidity();
    }
  })
}

  manipulateInfoMethods() {
    if (this.selectedMember && this.notifiMethods) {
      if (this.selectedMember.InfoCategoryList) {
        for (const cat of this.notifiMethods) {
          const foundCat = this.selectedMember.InfoCategoryList.filter(x => x.Id === cat.Id);
          if (foundCat && foundCat.length > 0) {
            cat.IsCheck = true;
          }
        }
      }
    }
  }

  removeleadingzeroes() {
    const k = (this.memberForm.get('MemberCardNo').value);
    let p: string;
    if (k && k.length > 0) {
      for (let i = 0; i < k.length; i++) {
        if (k[i] !== '0') {
          p = k.slice(i, k.length);
          i = k.length;
        }
      }
    }
    this.memberForm.patchValue({MemberCardNo: p});
  }

  cancelUpdate() {
    this.basicInfoService.updateEditMode(false, 'CARD');
    this.myFormSubmited = false;
  }

  openPostalCodeModal(content: any) {
    this.postalDataView = this.modalService.open(content, { width: '300' });
  }

  getCountryData() {
    this.memberService.getCountries().pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
        if (result) {
          this.countries = result.Data;
          this.countries.forEach(element => {
            if ((element.CountryCode === null ? '+47' : element.CountryCode ) === this.selectedMember.MobilePrefix
          || (element.CountryCode === null ? '+47' : element.CountryCode ) === ('+' + this.selectedMember.MobilePrefix)) {
            this.countrIdMatch =  (element.CountryCode === '+47' || '47') ? 'NO' : element.Id.toLowerCase();
            this.intlOptions.initialCountry = this.countrIdMatch;
            }
            let tempName;
            this.listOfCountryId.push(element.Id);
            const translateExpression = 'COUNTRIES.' + element.Id;
            this.translateService.get(translateExpression).pipe(takeUntil(
              this.destroy$)
            ).subscribe(tranlstedValue => tempName = tranlstedValue);
            element.DisplayName = tempName;
          });
        }
      }
    )
  }

  openMobileSendSmsModal(content: any) {
    if (this.currentName === '' || this.currentName === null || this.currentName.length === 0) {
      let messageTitle = '';
      let messageBody = '';
      this.translateService.get('ADMIN.ErrorGettingGymSmsSetting').pipe(
        takeUntil(this.destroy$)
      ).subscribe(tranlstedValue => messageTitle = tranlstedValue);
      this.translateService.get('ADMIN.SenderNameMissing').pipe(
        takeUntil(this.destroy$)
      ).subscribe(tranlstedValue => messageBody = tranlstedValue);
      this.exceMessageService.openMessageBox('ERROR',
      {
        messageTitle: messageTitle,
        messageBody: messageBody,
        msgBoxId: 'MSG_EMAIL'
      });
    } else {
      this.memberService.getSMSNotificationTemplateByEntity().pipe(
        takeUntil(this.destroy$)
      ).subscribe(result => {
          if (result) {
            this.smsTemplates = result.Data;
            this.smsTemplateView = this.postalDataView = this.modalService.open(content, { width: '600' });
          }
        }
      )
    }
  }

  closeMobileSendSmsModal() {
    if (this.smsTemplateView) {
      this.smsTemplateView.close();
    }
  }

  memberCardBlur(val: any) {
    if (val) {
      this.removeleadingzeroes();
    // if (this.memberForm.controls['Email'].valid) { // email is exits check after valid email
    this.memberService.validateMemberCard(val, this.selectedMember.Id).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
        if (result) {
          if (!result.Data) {
            this.isMemberCardBlur = true;
            this.memberForm.controls['MemberCardNo'].setErrors({
              'memberCardTaken': true
            });
          } else {
          }
        }
      }
    )
    }
  }

  memberCardFocus() {
    this.isMemberCardBlur = false
  }

  setGender(genderType: number) {
    this.selectedMember.Gender = genderType;
    this.memberForm.patchValue({
      gender: genderType
    })
  }

  setMemberType(memberType: number) {
    this.selectedMember.Role = memberType;
    if (memberType === 0) {
      this.selectedMember.MemberCategory = this.memberCategoies.filter(x => x.Code === 'COMPANY');
    } else if (memberType === 1) {
      this.selectedMember.MemberCategory = this.memberCategoies.filter(x => x.Code === 'MEMBER');
    }
  }

  savePostalData() {
    const postalData = this.postalDataForm.value;
    this.memberService.savePostalArea(postalData).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        if (result) {
          if (result.Data > 0) {
            this.memberForm.controls['postalCode'].setValue(postalData.postalCode);
            this.memberForm.controls['city'].setValue(postalData.postalName);
            this.postalDataView.close();
          }
        }
      })
  }

  changeInforCats(popOver: any) {
    for (const cat of this.notifiMethods) {
      if (cat.IsCheck) {
        const selectedCat = this.selectedMember.InfoCategoryList.filter(x => x.Id === cat.Id);
        if (selectedCat.length === 0) {
          this.selectedMember.InfoCategoryList.push(cat);
        }
      }
    }
    // popOver.close();
  }

  getSelectedStatus(value) {
    const selectedStatus = this.memberStatus.filter(x => x.StatusName === value);
    if (selectedStatus && selectedStatus.length > 0) {
      if (selectedStatus[0].StatusID === 8) {
        this.validateMemberWithStatus(this.selectedMember.Id, selectedStatus[0].StatusID, false);
      }
      this.selectedMember.MemberStatuse = selectedStatus[0].StatusID;
      this.selectedMember.StatusName = selectedStatus[0].StatusName;
      this.selectedMember.StatusNo = selectedStatus[0].StatusID;
    }
  }



  onbirthdayDateChange(value) {
    this.selectedMember.BirthDate = value.date.year + '-' + value.date.month + '-' + value.date.day;
  }

  setMemberCategory() {
    if (this.memberCategoies && this.selectedMember) {
      let selectedcategory: any;
      if (this.selectedMember.Role === 0) {
        selectedcategory = this.memberCategoies.filter(x => x.Code === 'COMPANY');
      } else if (this.selectedMember.Role === 1) {
        selectedcategory = this.memberCategoies.filter(x => x.Code === 'MEMBER');
      }
      if (selectedcategory.length > 0) {
        this.selectedMember.MemberCategory = selectedcategory[0];
      }
    }
  }

  getCityforPostalCode(postalCode: any) {
    if (postalCode && postalCode.length >= 4) {
      this.memberService.getCityForPostalCode(postalCode).pipe(
        takeUntil(this.destroy$)
      ).subscribe(result => {
          if (result) {
            this.selectedMember.PostCode = postalCode;
            this.selectedMember.PostPlace = result.Data;
            this.memberForm.patchValue({ PostPlace: this.selectedMember.PostPlace });
          }
        }
      )
    }
  }

  validateMemberWithStatus(memberID: number, selectedStatus: any, initialLoad: boolean) {
    let tempMessage = '';
    this.memberService.validateMemberwithStatus(memberID, selectedStatus).pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      result => {
        if (result) {
          if (result.Data > 0) {
            if (initialLoad === true) {
              this.deleteButtonEnabled = false;
            }
            switch (result.Data) {
              case result.Data >= 8: {
                this.translateService.get('MEMBERSHIP.Group').pipe(
                  takeUntil(this.destroy$)
                ).subscribe(tranlstedValue => tempMessage = tranlstedValue + ',');
                break;
              }
              case result.Data % 8 >= 4: {
                this.translateService.get('MEMBERSHIP.ParentName').pipe(
                  takeUntil(this.destroy$)
                ).subscribe(tranlstedValue => tempMessage = tranlstedValue + ',');
                break;
              }
              case result.Data % 4 >= 2: {
                this.translateService.get('MEMBERSHIP.Sponsor').pipe
                (takeUntil(this.destroy$)
                ).subscribe(tranlstedValue => tempMessage = tranlstedValue + ',');
                break;
              }
              case result.Data % 2 >= 1: {
                this.translateService.get('MEMBERSHIP.Employee').pipe(
                  takeUntil(this.destroy$)
                ).subscribe(tranlstedValue => tempMessage = tranlstedValue + ',');
                break;
              }
            }
          } else {
            if (initialLoad === true) {
              this.deleteButtonEnabled = true;
            }
            this.translateService.get('MEMBERSHIP.DiedMemberMgs1').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => tempMessage = tranlstedValue + ',');
          }
        }
      })
  }

  updateNameFormat(name: string, nameType: string) {
    let formatedName = '';
    if (typeof name !== 'undefined' && name) {
      for (let i = 0; i < name.length; i++) {
        if (i > 0) {
          formatedName = name[i - 1] === '-' || name[i - 1] === ' ' || name[i - 1] === '&' || name[i - 1] === '/' ?
            formatedName + name[i].toUpperCase() : formatedName + name[i].toLowerCase();
        } else {
          formatedName += name[i].toUpperCase();
        }
        if (nameType === 'FNAME') {
          this.memberForm.patchValue({ FirstName: formatedName.trim() });
        } else if (nameType === 'LNAME') {
          this.memberForm.patchValue({ LastName: formatedName.trim() });
        }
      }
    }
  }

  onPhoneNumberChange(type: string, value: any) {
    switch (type) {
      case 'MOBILE': {
        this.selectedMember.Mobile = value;
        break;
      }
      case 'PRIVATE': {
        this.selectedMember.PrivateTeleNo = value;
        break;
      }
      case 'WORK': {
        this.selectedMember.WorkTeleNo = value;
        break;
      }
      case 'MOBILEPRE': {
        this.selectedMember.MobilePrefix = value;
        break;
      }
      case 'PRIVATEPRE': {
        this.selectedMember.PrivateTeleNoPrefix = value;
        break;
      }
      case 'WORKPRE': {
        this.selectedMember.WorkTeleNoPrefix = value;
        break;
      }
    }
  }

  getText(event) {
    this.smsText = event.target.value;
  }

  setSMSText(templateName: string) {
    const selectedTemplate = this.smsTemplates.filter(x => x.Name === templateName);
    if (selectedTemplate && selectedTemplate.length > 0) {
      this.smsText = selectedTemplate[0].Text;
    }
  }

  sendSMS(content: any, content2: any) {
    let errorMessage = '';
    this.translateService.get('MEMBERSHIP.Error').pipe(
      takeUntil(this.destroy$)
    ).subscribe(translatedValue => errorMessage = translatedValue);
    const smsData = {
      memberID: this.selectedMember.Id,
      branchId: this.selectedMember.BranchId,
      message: this.smsText,
      mobileNo: this.selectedMember.MobilePrefix + this.selectedMember.Mobile
    }
    this.memberService.sendSMSFromMemberCard(smsData).pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
        if (result) {
          if (result.Data === false) {
          this.smsTemplateView.close();
          this.smsErrorView = this.modalService.open(content, {width: '300'});
            // alert(errorMessage);
          } else {
            this.smsTemplateView.close();
            this.smsConfirmationView = this.modalService.open(content, { width: '300' });
          }
        }
      },
      error => {
        // alert(errorMessage);
      })

      // ***********************************
      // next lines removed when sms working - there is no result now, therefore view is never closed
      this.closeMobileSendSmsModal();
      this.smsConfirmationView = this.modalService.open(content, { width: '300' });
  }


  mobileFocus(type: string) {
    switch (type) {
      case 'MOBILE':
        this.isMobileBlur = false
        break;
      case 'PRIVATE':
        this.isPrivateBlur = false
        break;
      case 'WORK':
        this.isWorkBlur = false
        break;
    }
  }

  mobileBlur(val: any, type: string) {
    const msg =  this.takenMSG.nativeElement.innerHTML;
    switch (type) {
      case 'MOBILE':
        this.isMobileBlur = true;
        if (this.memberForm.controls['Mobile'].errors == null || (!this.memberForm.controls['Mobile'].errors.required &&
          !this.memberForm.controls['Mobile'].errors.phoneNumber)) {
          this.memberService.validateMobile(this.memberForm.value.Mobile, this.memberForm.value.MobilePrefix).pipe(
            takeUntil(this.destroy$)
            ).subscribe(
            result => {
              const mobileTakenInfo = result.Data;
              if (mobileTakenInfo != null && mobileTakenInfo.Item1 !== -1 && mobileTakenInfo.Item2 !== this.selectedMember.Name/* MemberId is set to -1 if no duplicate is found */) {
                const outputMemberId = mobileTakenInfo.Item1;
                const outputName = mobileTakenInfo.Item2;
                const errorMsg = outputMemberId + ' - ' + outputName + ' ' + msg;
                this.mobileExitMsg = errorMsg;
                this.memberForm.controls['Mobile'].setErrors({
                  'mobileTaken': true
                });
                this.formErrors.Mobile = errorMsg;
              }
            }
          )
        }
        break;
      case 'PRIVATE':
        this.isPrivateBlur = true;
        break;
      case 'WORK':
        this.isWorkBlur = true;
        break;
    }

  }

  fullValueChange(event: any, type: string) {
    this.phoneNumberDetail = event;
    if (type === 'MOBILE') {
      this.memberForm.patchValue({
        Mobile: event.value.replace(' ', ''),
      });
      this.memberForm.patchValue({
        MobilePrefix: event.extension
      });
      this.isMobileNumberValid = event.numberType === 1 && event.valid;
    } else if (type === 'PRIVATE') {
      this.memberForm.patchValue({
        PrivateTeleNo: event.value.replace(' ', ''),
      });
      this.memberForm.patchValue({
        PrivateTeleNoPrefix: event.extension
      });
      this.isPrivateNumberValid = event.value === '' || event.valid;
    } else if (type === 'WORK') {
      this.memberForm.patchValue({
        WorkTeleNo: event.value.replace(' ', ''),
      });
      this.memberForm.patchValue({
        WorkTeleNoPrefix: event.extension
      });
      this.isWorkNumberValid = event.value === '' || event.valid;
    }
  }

  sendEmail(value: any, content: any) {
    this.emailEditorContent = content;
    let messageTitle = '';
    let messageBody = '';
    this.translateService.get('Confirm').pipe(
      takeUntil(this.destroy$)
    ).subscribe(tranlstedValue => messageTitle = tranlstedValue);

    this.translateService.get('MEMBERSHIP.emailFromExceline').pipe(
      takeUntil(this.destroy$)
    ).subscribe(tranlstedValue => messageBody = tranlstedValue);


    this.exceMessageService.openMessageBox('CONFIRM', {
      messageTitle: messageTitle,
      messageBody: messageBody,
      msgBoxId: 'MSG_EMAIL'
    });
  }

  yesHandler() {
    this.emailEditorModalReference = this.modalService.open(this.emailEditorContent, { width: '600' });
  }

  noHandler() {
    window.open('mailto:' + this.selectedMember.Email);
  }

  sendFromEmailEditor(to, subject) {
    subject = subject.trim();
    const emailObj = {
      memberID: this.selectedMember.Id,
      branchId: this.selectedMember.BranchId,
      message: this.emailEditorValue,
      email: to,
      subject: subject
    }

    if (subject !== undefined && subject !== '') {
      this.memberService.sendEmailFromMemberCard(emailObj).pipe(
        takeUntil(this.destroy$)
        ).subscribe(result => {
        if (result) {
          this.emailEditorModalReference.close();
        }
      })
    } else {
      this.exceMessageService.openMessageBox('ERROR', { messageTitle: 'Exceline', messageBody: 'MEMBERSHIP.SendWithoutSubject' });
    }
    // this.emailEditorModalReference.close();

  }

  private emailValidator(control: AbstractControl): ValidationErrors {
    if (!control.value) {
      return null;
    }
    return Validators.email(control);
  }

  saveMemberUpdates() {
    this.updateMember();
  }

  routeToGuardian() {
    if ( this.selectedMember.GuardianId === -1 ) {
      return;
    }
    if (this.selectedMember.GuardianId === this.selectedMember.Id || this.selectedMember.GuardianId === 0) {
      this.translateService.get('MEMBERSHIP.canNotRouteToSelf').pipe(
        takeUntil(this.destroy$)
        ).subscribe(trans => this.message = trans);
      this.type = 'INFO';
      this.isShowInfo = true;
      setTimeout(() => {
        this.isShowInfo = false;
      }, 4000);
      return;
    }
    const url = '/membership/card/' + this.selectedMember.GuardianId + '/' + this.selectedMember.GuardianBranchId + '/' + 'MEM';
    this.router.navigate([url]);
    this.exceBreadCrumbService.addBreadCumbItem.next({
      name: this.selectedMember.Name,
      url: url
    });
  }

  updateMember() {
    this.memberCardBlur(this.memberForm.get('MemberCardNo').value)
    this.memberForm.get('Email').setValidators(this.emailValidator.bind(this));
    this.myFormSubmited = true;
    if (this.memberForm.valid) {
      let memberID = -1;
      let errorMessage = '';
      const member = this.memberForm.value;
      member.Id = this.selectedMember.Id;
      member.Gender = this.selectedMember.Gender;
      member.InfoCategoryList = this.selectedMember.InfoCategoryList;
      member.MemberStatuse = this.selectedMember.MemberStatuse;
      member.StatusName = this.selectedMember.StatusName;
      member.StatusNo = this.selectedMember.StatusNo;
      member.CountryId = this.selectedMember.CountryId;
     // member.CountryName = this.selectedMember.CountryName;
      member.BirthDate = this.selectedMember.BirthDate;
      member.MemberCategory = this.selectedMember.MemberCategory[0];
      member.PostPlace = this.selectedMember.PostPlace;
      member.postCode = this.selectedMember.PostCode;
      member.IsBrisIntegrated = this.selectedMember.IsBrisIntegrated;
      member.Role = this.selectedMember.Role;
      member.Name = member.FirstName + ' ' + member.LastName;
      member.CustId = this.selectedMember.CustId;

      if (this.selectedMember.Role === 0) {
        const MemberCategory = this.memberCategoies.filter(x => x.Code === 'COMPANY');
        member.MemberCategory = MemberCategory[0];
      } else if (this.selectedMember.Role === 1) {
        const MemberCategory = this.memberCategoies.filter(x => x.Code === 'MEMBER');
        member.MemberCategory = MemberCategory[0];
      }

      this.translateService.get('MEMBERSHIP.Error').pipe(
        takeUntil(this.destroy$)
      ).subscribe(translatedValue => errorMessage = translatedValue);
      let errorSaveMag: string;
      if (member.Role === 0) {
        this.translateService.get('MEMBERSHIP.MsgComSaveError').pipe(
          takeUntil(this.destroy$)
        ).subscribe(translatedValue => errorSaveMag = translatedValue);
      } else if (member.Role === 1) {
        this.translateService.get('MEMBERSHIP.MsgMemSaveError').pipe(takeUntil(
          this.destroy$)
        ).subscribe(translatedValue => errorSaveMag = translatedValue);
      }
      this.memberService.updateMember(member).pipe(takeUntil(this.destroy$)).subscribe(
        result => {
          if (result) {
            if (result.Data.length > 0) {
              memberID = result.Data[0];
              this.translateService.get('MEMBERSHIP.SaveSuccessful').pipe(
                takeUntil(this.destroy$)
              ).subscribe(value => this.message = value);
              this.type = 'SUCCESS';
              this.isShowInfo = true;
              setTimeout(() => {
                this.isShowInfo = false;
              }, 4000);
            }
            if (memberID > 0) {
              this.memberService.getMemberDetails(Number(this._memberID), Number(this._branchID), MemberRole[member.Role]).pipe(takeUntil(this.destroy$)).subscribe( membD => {
                this.basicInfoService.setSelectedMember(membD.Data);
                this.basicInfoService.updateEditMode(false, 'CARD');
              })
            } else {
              if (memberID === -1) {
                // show message
              } else if (memberID === -2) {
                this.translateService.get('MEMBERSHIP.crCardNumberExist').pipe(takeUntil(this.destroy$)).subscribe(value => errorSaveMag = value);
                // show the message
              } else if (memberID === -4) {
                const mobileExitMemName = result.Data[1];
                this.translateService.get('MEMBERSHIP.ExitMobileNumber').pipe(takeUntil(this.destroy$)).subscribe(value => errorSaveMag = mobileExitMemName + ' ' + value);
              } else if (memberID === -5) {
                let emailExitMemName = '';
                const typeOfEmail = result.Data[1];
                if (result.Data.length > 2) {
                  emailExitMemName = result.Data[2];
                  this.translateService.get('MEMBERSHIP.ExitEmailAddress').pipe(takeUntil(this.destroy$)).subscribe(value => errorSaveMag = emailExitMemName + ' ' + value);
                }
                if (member.Role === 1) { // member
                  // update the member email
                } else {
                  if (typeOfEmail === 1) {
                    // update the company email
                  } else {
                    // update the company accounting email
                  }
                }
              }
            }
          }
        }, error => {
          this.translateService.get('MEMBERSHIP.SaveNotSuccessful').pipe(takeUntil(this.destroy$)).subscribe(value => this.message = value);
          this.type = 'DANGER';
          this.isShowInfo = true;
          setTimeout(() => {
            this.isShowInfo = false;
          }, 4000);
         // alert(errorMessage);
        })
    } else {
      UsErrorService.validateAllFormFields(this.memberForm, this.formErrors, this.validationMessages);
    }
  }

  viewLoad(popOver) {
    if (this.popOver) {
      this.popOver.close();
    }
    this.popOver = popOver;
    this.popOver.open();
  }

  closePopOver() {
    this.popOver.close();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();

    if (this.postalDataView) {
      this.postalDataView.close();
    }
    if (this.smsTemplateView) {
      this.smsTemplateView.close();
    }
    if (this.smsErrorView) {
      this.smsErrorView.close();
    }
    if (this.smsConfirmationView) {
      this.smsConfirmationView.close();
    }
    if (this.emailEditorModalReference) {
      this.emailEditorModalReference.close();
    }
    this.destroy$.next();
    this.destroy$.complete();
  }

}
