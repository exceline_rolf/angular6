import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { McDocumentComponent } from './mc-document.component';

describe('McDocumentComponent', () => {
  let component: McDocumentComponent;
  let fixture: ComponentFixture<McDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ McDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(McDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
