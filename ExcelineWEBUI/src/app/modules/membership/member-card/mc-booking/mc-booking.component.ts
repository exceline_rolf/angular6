import { ExcePdfViewerService } from './../../../../shared/components/exce-pdf-viewer/exce-pdf-viewer.service';
import { PreloaderService } from './../../../../shared/services/preloader.service';
import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { DataTableResource } from '../../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from '../../../../shared/components/us-data-table/tools/data-table-filter-pipe'
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
import { ExceMemberService } from '../../services/exce-member.service'
import { McBasicInfoService } from './../mc-basic-info/mc-basic-info.service';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

const now = new Date();

@Component({
  selector: 'app-mc-booking',
  templateUrl: './mc-booking.component.html',
  styleUrls: ['./mc-booking.component.scss']
})

export class McBookingComponent implements OnInit, OnDestroy {
  private memberBookings: any;
  private filteredMemberBookings: any;
  private itemResource: any = 0;
  public items = [];
  public itemCount = 0;
  public resources: any;
  public activityCategories: any;
  public resourceCategories: any;
  private filterpipe: DataTableFilterPipe = new DataTableFilterPipe();
  private dateValue: any = '';
  private resourceValue: string;
  private activityValue: string;
  private categoryValue: string;
  private modalReference?: any;
  private memberBookingForm: any;
  private otherMembers: any;
  private rowItem: any;
  private printBookingForm: any;
  private printResult: any;
  rowColors: any;
  private selectedMember: any;
  private today: NgbDateStruct;
  private monthbefore: NgbDateStruct;

  private destroy$ = new Subject<void>();

  constructor(
    private exceMemberService: ExceMemberService,
    private basicInfoService: McBasicInfoService,
    private modalService: UsbModal,
    private fb: FormBuilder,
    private excePdfViewerService: ExcePdfViewerService
  ) {
    this.today = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
    this.monthbefore = { year: now.getFullYear(), month: now.getMonth(), day: now.getDate() };

    this.memberBookingForm = this.fb.group({
      'otherMembers': [''],
      'OtherResource': [''],
      'ActivityName': [''],
      'Comment': [''],
      'Article': [''],
      'StartTime': [''],
      'EndTime': [''],
      'CategoryName': [''],
      'IsSmsRemindered': [''],
    });

    this.printBookingForm = this.fb.group({
      'fromDate': [{ date: this.monthbefore }],
      'toDate': [{ date: this.today }],
    });
  }

  ngOnInit() {
    this.basicInfoService.currentMember.pipe(takeUntil(this.destroy$)).subscribe(
      member => {
        if (member) {
          this.selectedMember = member;
          this.exceMemberService.getMemberBooking(this.selectedMember.Id).pipe(takeUntil(this.destroy$)).subscribe
            (result => {
              if (result) {
                this.memberBookings = result.Data;
                for (let i of this.memberBookings) {
                  if (i.ActiveStatus == false) {
                    i.IsError = true;
                  }
                  if (i.ActiveStatus == false && i.ReferenceId > 0) {
                    const item = this.memberBookings.find(x => x.ActiveTimeId == i.ReferenceId);
                    if (item != null) {
                      i.CancelNumber = item.Number;
                    }
                  }
                }
                this.rowColors = this.getRowColors.bind(this);
                this.itemResource = new DataTableResource(this.memberBookings);
                this.itemResource.count().then(count => this.itemCount = count);
                this.items = this.memberBookings;
                this.itemCount = Number(result.Data.length);
                //  this.reloadItems({offset: 0, limit: 10});
              } else {
              }
            })

          this.exceMemberService.getResources(this.selectedMember.BranchId, '', -1, -1, true).pipe(takeUntil(this.destroy$)).subscribe
            (
            result => {
              if (result) {
                this.resources = result.Data;
              } else {
              }
            })

          this.exceMemberService.getCategories('ACTIVITY').pipe(takeUntil(this.destroy$)).subscribe
            (result => {
              if (result) {
                this.activityCategories = result.Data;
              } else {
              }
            })

          this.exceMemberService.getCategories('RESOURCE').pipe(takeUntil(this.destroy$)).subscribe
            (result => {
              if (result) {
                this.resourceCategories = result.Data;
              } else {
              }
            })
        }
      });
  }

  ngOnDestroy() {
    if (this.modalReference) {
      this.modalReference.close();
    }
    this.destroy$.next();
    this.destroy$.complete();
  }

  filterInput(dateValue?: any, resourceValue?: any, activityValue?: any, categoryValue?: any, statusValue?: any) {
    this.filteredMemberBookings = this.memberBookings
    if (dateValue && dateValue !== '') {
      dateValue = this.dateConverter(dateValue);
      this.filteredMemberBookings = this.filterpipe.transform('Date', 'DATE_FILTER', this.filteredMemberBookings, dateValue);
    }
    if (resourceValue !== 'ALL') {
      this.filteredMemberBookings = this.filterpipe.transform('ResourceName', 'SELECT', this.filteredMemberBookings, resourceValue);
    }
    if (activityValue !== 'ALL') {
      this.filteredMemberBookings = this.filterpipe.transform('ActivityName', 'SELECT', this.filteredMemberBookings, activityValue);
    }
    if (categoryValue !== 'ALL') {
      this.filteredMemberBookings = this.filterpipe.transform('CategoryName', 'SELECT', this.filteredMemberBookings, categoryValue);
    }
    if (statusValue !== 'ALL') {
      this.filteredMemberBookings = this.filterpipe.transform('ArrivalDateTime', 'NULL_SELECT', this.filteredMemberBookings, statusValue);
    }
    this.itemResource = new DataTableResource(this.filteredMemberBookings);
    this.itemResource.count().then(count => this.itemCount = count);
    this.reloadItems(this.filteredMemberBookings);
    this.resourceValue = resourceValue;
    this.dateValue = dateValue;
    this.categoryValue = categoryValue;
    this.activityValue = activityValue;
  }

  // convert date object in to string
  dateConverter(dateObj: any) {
    let dateS = '';
    if (dateObj) {
      if (dateObj) {
        const splited = dateObj.split('/')
        if (splited.length > 1) {
          dateS = splited[2] + '-' + splited[1] + '-' + splited[0] + 'T00:00:00'
        }
      }
    }
    return dateS;
  }

  getRowColors(membookings) {
    if (membookings.CancelNumber !== '') {
      return 'rgb(255, 255, 197)';
    }
  }

  reloadItems(params): void {
    if (this.itemResource) {
      this.itemResource.query(params).then(items => this.items = items);
    }
  }

  openBookingModel(content: any) {
    this.modalReference = this.modalService.open(content, { width: '700' });
  }

  openPrintBookingModel(content: any) {
    this.modalReference = this.modalService.open(content, { width: '300' });
  }

  rowItemLoad(rowItem) {
    this.rowItem = rowItem
    this.memberBookingForm.patchValue(this.rowItem);
    this.exceMemberService.getOtherMembersForBooking(this.rowItem.ScheduleItemId).pipe(takeUntil(this.destroy$)).subscribe
      (result => {
        if (result) {
          this.otherMembers = result.Data;
        } else {
        }
      })
  }

  closeForm() {
    this.memberBookingForm.reset();
    this.modalReference.close();
  }

  printBookings(printData: any) {
    const fromDate = printData.fromDate.date.year + '-' + printData.fromDate.date.month + '-' + printData.fromDate.date.day;
    const toDate = printData.toDate.date.year + '-' + printData.toDate.date.month + '-' + printData.toDate.date.day;
    this.exceMemberService.PrintMemberBooking(this.selectedMember.Id, this.selectedMember.BranchId, fromDate, toDate).pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
        if (result) {
          this.printResult = result.Data;
          this.modalReference.close();
          PreloaderService.hidePreLoader();
          if (result.Data.FileParth.trim() !== '') {
            this.excePdfViewerService.openPdfViewer('Exceline', result.Data.FileParth);
          }
        } else {
          alert('print error')
        }
      });
  }
}
