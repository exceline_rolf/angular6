import { UsErrorService } from './../../../../../shared/directives/us-error/us-error.service';
import { element } from 'protractor';
import { filter } from 'rxjs/operators';
import { Extension } from './../../../../../shared/Utills/Extensions';
import { Subscription } from 'rxjs';
import { ExceToolbarService } from './../../../../common/exce-toolbar/exce-toolbar.service';
import { ExceMessageService } from './../../../../../shared/components/exce-message/exce-message.service';
import { UsbModalRef } from './../../../../../shared/components/us-modal/modal-ref';
import { UsbModal } from './../../../../../shared/components/us-modal/us-modal';
import { Router } from '@angular/router';
import { MemberRole } from './../../../../../shared/enums/us-enum';
import { FormGroup, Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { ExceLoginService } from './../../../../login/exce-login/exce-login.service';
import { ConfigService } from '@ngx-config/core';
import { McContractService } from './../mc-contract.service';
import { ExceTitleService } from './../../../services/exce-title.service';
import { ExceMemberService } from './../../../services/exce-member.service';
import { McBasicInfoService } from './../../mc-basic-info/mc-basic-info.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import * as moment from 'moment';
import { forEach } from '@angular/router/src/utils/collection';
import { error } from 'util';
import deepcopy from 'ts-deepcopy';
import { AdminService } from 'app/modules/admin/services/admin.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-mc-contract-renew',
  templateUrl: './mc-contract-renew.component.html',
  styleUrls: ['./mc-contract-renew.component.scss']
})
export class McContractRenewComponent implements OnInit, OnDestroy {

  private destroy$ = new Subject<void>();
  private _lastInsallment: any = {};
  private _lastInstallmentNo = 0;
  private _dateFormat: string;
  private _installmentDate = {};

  modalReference: UsbModalRef;
  selectedOrder: any;
  _deletingOrder: any;
  public validationEnabled: boolean;

  _trainingEndDate: any;
  _trainingStartDate: any;
  installmentList: any[] = [];
  _serviceAmount: number;
  isSaveEnabled: boolean;
  _lastInstallmentDueDate: any;
  TrainingStartDate: Date;
  RenewToDate: any;
  RenewFromDate: Date;
  _yesHandlerSubcription: Subscription;
  locale: string;
  templateModel: UsbModalRef;
  contractform: FormGroup;
  _branchId: any;
  _user: any;
  _selectedMember: any;
  IsBrisIntegrated: boolean;
  selectedContract: any;
  SelectedRenewTemplate: any;
  RenewInstallmentList: any[] = [];
  startDueDate: Date;
  selectedContractOriginal: any;


  formErrors = {
    'StartDueDate': ''
  }

  validationMessages = {
    'StartDueDate': {
      'required': 'MEMBERSHIP.DueDateReq'
    }
  }


  constructor(
    private basicInfoService: McBasicInfoService,
    private loginservice: ExceLoginService,
    private memberService: ExceMemberService,
    private titleService: ExceTitleService,
    private contractService: McContractService,
    private config: ConfigService,
    private fb: FormBuilder,
    private router: Router,
    private modalService: UsbModal,
    private messageService: ExceMessageService,
    private toolbarService: ExceToolbarService,
    private adminService: AdminService,
  ) { }

  ngOnInit() {
    this.adminService.GetGymCompanySettings('other').pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
      this.IsBrisIntegrated = result.Data.IsBrisIntegrated;
    });
    this.titleService.setTitle('MEMBERSHIP.Renew');
    this._branchId = this.loginservice.SelectedBranch.BranchId;
    this.basicInfoService.currentMember.pipe(
      takeUntil(this.destroy$)
    ).subscribe( currentMember => {this._selectedMember = currentMember});

    this._user = this.loginservice.CurrentUser.username;
    this.selectedContract = deepcopy<any>(this.contractService.getSelectedContract());
    this.selectedContractOriginal = deepcopy<any>(this.selectedContract);
    this.contractform = this.fb.group(
      {
        'TemplateNo': [null, Validators.required],
        'TemplateName': [null, Validators.required],
        'NoOfMonths': [null],
        'NoOfOrders': [null],
        'LockInPeriod': [null],
        'PriceGuaranteePeriod': [null],
        'StartDueDate': [null, Validators.required],
        'LockInDate': [null],
        'PriceGuranteeDate': [null],
        'TrainingStartDate': [null]
      }
    );

    // get the last installment
    if (this.selectedContract.InstalllmentList.length > 0) {
      this._lastInsallment = this.selectedContract.InstalllmentList[this.selectedContract.InstalllmentList.length - 1];
      this._lastInstallmentNo = this._lastInsallment.InstallmentNo + 1;
    } else {
      this.memberService.getLastInstallment(this.selectedContract.Id).pipe(takeUntil(this.destroy$)).subscribe(
        result => {
          if (result.Data) {
            if (result.Data.Id > 0) {
              this._lastInsallment = result.Data;
              this._lastInstallmentNo = this._lastInsallment.InstallmentNo + 1;
            } else {
              this._lastInstallmentNo = 1;
            }
          }
        }
      );
    }

    if (this.selectedContract.NextTemplateId > 0) {
      this.memberService.getRenewTemplate(this.selectedContract.NextTemplateId, 'ID').pipe(takeUntil(this.destroy$)).subscribe(
        result => {
          if (result) {
            this.SelectedRenewTemplate = result.Data;
            this.updateData();
            this.PrepareArticles();
          }
        }
      );
    } else {
      this.memberService.getRenewTemplate(this.selectedContract.ContractId, 'ID').pipe(takeUntil(this.destroy$)).subscribe(
        result => {
          if (result) {
            this.SelectedRenewTemplate = result.Data;
            this.updateData()
            this.PrepareArticles();
          }
        }
      );
    }

    const language = this.toolbarService.getSelectedLanguage();
    this.locale = language.culture;
    this._dateFormat = 'DD.MM.YYYY';

    this.toolbarService.langUpdated.pipe(takeUntil(this.destroy$)).subscribe(
      (lang) => {
        if (lang) {
          this.locale = lang.culture;
        }
      }
    );
    this.messageService.yesCalledHandle.pipe(takeUntil(this.destroy$)).subscribe((value) => {
      switch (value.id) {
        case 'DATEUP':
          this.moveOrders('UP');
          break;
        case 'DATEDOWN':
          this.moveOrders('DOWN');
          break;
        case 'GENINS':
          this.generateInstallments();
          break;
        case 'DELORDER':
          this.removeOrder();
          break;
      }
    });
  }

  updateData() {
    this.contractform.patchValue(
      {
        TemplateNo: this.SelectedRenewTemplate.TemplateNumber,
        TemplateName: this.SelectedRenewTemplate.PackageName,
        NoOfMonths: this.SelectedRenewTemplate.NoOfMonths,
        NoOfOrders: this.SelectedRenewTemplate.NumOfInstallments,
      }
    );
  }

  openContractTemplates(content: any) {
    this.templateModel = this.modalService.open(content);
  }

  templateSelected(event: any) {
    this.SelectedRenewTemplate = event;
    if (this.SelectedRenewTemplate.MaxAge <= this._selectedMember.Age && this._selectedMember.Age > 0 && this.SelectedRenewTemplate.MaxAge > 0) {
      this.messageService.openMessageBox('WARNING', {
        messageTitle: 'Exceline',
        messageBody: 'MEMBERSHIP.MsgAgeNotMatch'
      });
    } else {
      if (this.SelectedRenewTemplate.PackageCategory.Code == this.selectedContract.ContractTypeCode) {
        this.SelectedRenewTemplate = this.SelectedRenewTemplate;
        this.updateData();
        this.PrepareArticles();
      } else {
        this.messageService.openMessageBox('WARNING', {
          messageTitle: 'Exceline',
          messageBody: 'MEMBERSHIP.MsgSelectSameContractType'
        });
      }
    }
    this.templateModel.close();
  }

  PrepareArticles() {
    if (this.SelectedRenewTemplate) {
      this.selectedContract = deepcopy<any>(this.selectedContractOriginal);
      if (this.SelectedRenewTemplate.FixDateOfContract) {
        if (moment(this.SelectedRenewTemplate.FixDateOfContract).isBefore(moment(), 'day')) {
          this.messageService.openMessageBox('WARNING', {
            messageTitle: 'Exceline',
            messageBody: 'MEMBERSHIP.TemplateExpired'
          });
        } else {
          this.RenewToDate = this.SelectedRenewTemplate.FixDateOfContract;
        }
      } else {
        this.RenewToDate = this.dateAdd(this.selectedContract.ContractEndDate, this.SelectedRenewTemplate.NoOfMonths, 'months');
      }

      // this.RenewFromDate = moment(this.selectedContract.ContractEndDate).toDate(); // this.dateAdd(this.selectedContract.ContractEndDate, 1, 'days');
      this.RenewFromDate = this.dateAdd(this.selectedContract.ContractEndDate, 1, 'days');
      this.TrainingStartDate = this.RenewFromDate;
      this._installmentDate = this.RenewFromDate;
      //set the training date

      if (this._lastInsallment) {
        this._lastInstallmentDueDate = this._lastInsallment.DueDate;
      }
      if (moment(this.selectedContract.ContractEndDate).isSameOrAfter(moment(), 'day')) {
        let noofDaysInthisMonth = moment().date();
        let daysInPreviousMonth = this.getNumberOfDaysInMonth(moment(this.dateAdd(this._lastInstallmentDueDate, -1, 'months')).year(), moment(this.dateAdd(this._lastInstallmentDueDate, -1, 'months')).month() + 1);
        let coutStartDate: Date = this.dateAdd(new Date(), ((daysInPreviousMonth + noofDaysInthisMonth - 1) * -1), 'days');

        if (moment(this._lastInstallmentDueDate).isSameOrAfter(coutStartDate, 'day')) {
          if (this.selectedContract.FixedDueDay > 0)
            this._lastInstallmentDueDate = new Date(moment(this.dateAdd(this._lastInstallmentDueDate, 1, 'months')).year(), moment(this.dateAdd(this._lastInstallmentDueDate, 1, 'months')).month(), this.selectedContract.FixedDueDay);
          else
            this._lastInstallmentDueDate = new Date(moment(this.dateAdd(this._lastInstallmentDueDate, 1, 'months')).year(), moment(this.dateAdd(this._lastInstallmentDueDate, 1, 'months')).month(), moment(this._lastInstallmentDueDate).date());
        } else {
          if (this.selectedContract.FixedDueDay > 0)
            this._lastInstallmentDueDate = new Date(moment(this.dateAdd(this.selectedContract.ContractEndDate, 1, 'months')).year(), moment(this.dateAdd(this.selectedContract.ContractEndDate, 1, 'months')).month(), this.selectedContract.FixedDueDay);
          else
            this._lastInstallmentDueDate = new Date(moment(this.dateAdd(this.selectedContract.ContractEndDate, 1, 'months')).year(), moment(this.dateAdd(this.selectedContract.ContractEndDate, 1, 'months')).month(), moment(this._lastInstallmentDueDate).date());
        }

      } else {
        if (this.selectedContract.FixedDueDay > 0) {
          this._lastInstallmentDueDate = new Date(moment(this.dateAdd(new Date(), 1, 'months')).year(), moment(this.dateAdd(new Date(), 1, 'months')).month(), this.selectedContract.FixedDueDay);
        }
        else {
          this._lastInstallmentDueDate = new Date(moment(this.dateAdd(new Date(), 1, 'months')).year(), moment(this.dateAdd(new Date(), 1, 'months')).month(), moment(this._lastInstallmentDueDate).date());
        }
        this.RenewToDate = this.dateAdd(this.TrainingStartDate, this.SelectedRenewTemplate.NoOfMonths, 'months');
      }

      if (moment(this._lastInstallmentDueDate).isBefore(moment(), 'day')) {
        this._lastInstallmentDueDate = this.dateAdd(this._lastInstallmentDueDate, 1, 'months');
      }

      this.selectedContract.RenewedTemplateID = this.SelectedRenewTemplate.PackageId;
      this.selectedContract.RenewEffectiveDate = this.RenewFromDate;
      this.selectedContract.NoOfInstallments = this.SelectedRenewTemplate.NumOfInstallments;
      this.startDueDate = this._lastInstallmentDueDate;
      this.contractform.patchValue({
        TrainingStartDate: { date: Extension.GetFormatteredDate(this.TrainingStartDate) },
        StartDueDate: { date: Extension.GetFormatteredDate(this.startDueDate) }
      });
      let candidateitem: any;
      let itemsWithoutEndOrder: any = [];
      if (this.SelectedRenewTemplate.PackageId != this.selectedContract.ContractId) {
        if (this.SelectedRenewTemplate.EveryMonthItemList.length > 0) {
          //set old item as ended
          this.selectedContract.EveryMonthItemList.forEach(oldItem => {
            if (oldItem.EndOrder < 1) {
              if (this._lastInstallmentNo > 1) {
                oldItem.EndOrder = this._lastInstallmentNo - 1;
              } else {
                oldItem.EndOrder = 1;
              }
            }
          });

          if (this._lastInstallmentNo === 1) {
            this._lastInstallmentNo = 2; // this is to take the next order start with no 2
          }

          itemsWithoutEndOrder = this.SelectedRenewTemplate.EveryMonthItemList.filter(X => X.EndOrder < 1);
          if (itemsWithoutEndOrder.length > 1) { //more activity articles found with out end date
            //check for a activity article
            let activityAritlce = itemsWithoutEndOrder.filter(X => X.ActivityID === this.selectedContract.ActivityId && X.EndOrder < 1)
            if (!activityAritlce) { // no activity article found
              this.SelectedRenewTemplate.EveryMonthItemList.forEach(element => {
                if (element.ActivityID === this.selectedContract.ActivityId) {
                  if (candidateitem) {
                    if (candidateitem.EndOrder < element.EndOrder) {
                      candidateitem = element;
                    }
                  } else {
                    candidateitem = element;
                  }
                }
              });
            }

            if (candidateitem) {
              let candidateIndex = itemsWithoutEndOrder.indexOf(candidateitem);
              if (candidateIndex > 0) {
                itemsWithoutEndOrder.splice(candidateIndex, 1);
              }

              candidateitem.EndOrder - 1;
              itemsWithoutEndOrder.push(candidateitem);
            }

            itemsWithoutEndOrder.forEach(newItem => {
              newItem.StartOrder = this._lastInstallmentNo;
              this.selectedContract.EveryMonthItemList.push(newItem);
            });
          } else {// only one activity article found

            this.SelectedRenewTemplate.EveryMonthItemList.forEach(element => {
              if (element.ActivityID === this.selectedContract.ActivityId) {
                if (candidateitem) {
                  if (candidateitem.EndOrder < element.EndOrder) {
                    candidateitem = element;
                  }
                } else {
                  candidateitem = element;
                }
              }
            });

            if (candidateitem) {
              candidateitem.EndOrder = -1;
              candidateitem.StartOrder = this._lastInstallmentNo;
              this.selectedContract.EveryMonthItemList.push(candidateitem);
            }
          }
        }
      } else { // use the same contract temaplate
        if (this.SelectedRenewTemplate.EveryMonthItemList.length > 0) {
          itemsWithoutEndOrder = this.SelectedRenewTemplate.EveryMonthItemList.filter(X => X.EndOrder < 1);
          if (itemsWithoutEndOrder.length > 1) {
            //more activity articles found with out end date
            //check for a activity article
            let activityAritlce = itemsWithoutEndOrder.filter(X => X.ActivityID === this.selectedContract.ActivityId && X.EndOrder < 1)
            if (!activityAritlce) { // no activity article found
              this.SelectedRenewTemplate.EveryMonthItemList.forEach(element => {
                if (element.ActivityID === this.selectedContract.ActivityId) {
                  if (candidateitem) {
                    if (candidateitem.EndOrder < element.EndOrder) {
                      candidateitem = element;
                    }
                  } else {
                    candidateitem = element;
                  }
                }
              });
            }

            if (candidateitem) {
              candidateitem.EndOrder = -1;
            }

            this.selectedContract.EveryMonthItemList = [];
            this.SelectedRenewTemplate.EveryMonthItemList.forEach(newItem => {
              this.selectedContract.EveryMonthItemList.push(newItem);
            });

          } else { //only one Item
            this.SelectedRenewTemplate.EveryMonthItemList.forEach(element => {
              if (element.ActivityID === this.selectedContract.ActivityId) {
                if (candidateitem) {
                  if (candidateitem.EndOrder < element.EndOrder) {
                    candidateitem = element;
                  }
                } else {
                  candidateitem = element;
                }
              }
            });

            if (candidateitem) {
              candidateitem.EndOrder = -1;
            }

            this.selectedContract.EveryMonthItemList = [];
            this.SelectedRenewTemplate.EveryMonthItemList.forEach(newItem => {
              this.selectedContract.EveryMonthItemList.push(newItem);
            });
          }
        }
      }
      //disable the save Button sincce article are updated
      this.isSaveEnabled = false;
    }
  }

  noOfOrdersChange(val) {
    if (!isNaN(val)) {
      this.SelectedRenewTemplate.NumOfInstallments = Number(val);
    }
  }

  generateInstallments() {
    if (this.SelectedRenewTemplate) {
      this.startDueDate = new Date(this.manipulatedate(this.contractform.value.StartDueDate));
      this.TrainingStartDate = new Date(this.manipulatedate(this.contractform.value.TrainingStartDate));
      this.RenewInstallmentList = [];
      let contractServicePrice = 0;
      if (this._lastInsallment) {
        this._serviceAmount = 0;
        if (moment(this.RenewFromDate).isBefore(moment(), 'day')) {
          this.RenewToDate = this.dateAdd(this.TrainingStartDate, this.SelectedRenewTemplate.NoOfMonths, 'months');
        } else {
          this.RenewToDate = this.dateAdd(this.RenewFromDate, this.SelectedRenewTemplate.NoOfMonths, 'months');
        }
        this.selectedContract.ContractEndDate = this.dateAdd(this.RenewToDate, -1, 'days');
        this._lastInstallmentDueDate = this.startDueDate;
        if (this.SelectedRenewTemplate.NumOfInstallments > 0) {
          this._trainingStartDate = this.TrainingStartDate;
          let insCount = 1;
          let i = 0;
          for (i = this._lastInstallmentNo; i < this.SelectedRenewTemplate.NumOfInstallments + this._lastInstallmentNo; i++) {
            if (this.SelectedRenewTemplate.FixDateOfContract) {
              if (moment(this.SelectedRenewTemplate.FixDateOfContract).isBefore(moment(this._lastInstallmentDueDate))) {
                continue;
              }
            }

            let installment: any = {};
            installment.AddOnList = [];
            installment.IsATG = this.selectedContract.IsATG;
            installment.DueDate = moment(this._lastInstallmentDueDate).format('YYYY-MM-DD').toString();
            installment.OriginalDueDate = moment(this._lastInstallmentDueDate).format('YYYY-MM-DD').toString();
            installment.CreatedUser = this._user;
            installment.BillingDate = moment(new Date()).format('YYYY-MM-DD').toString();
            installment.TransferDate = moment(this.dateAdd(this._lastInstallmentDueDate, -6, 'days')).format('YYYY-MM-DD').toString();
            installment.MemberId = this.selectedContract.MemberId;
            installment.MemberContractId = this.selectedContract.Id;
            installment.MemberContractNo = this.selectedContract.MemberContractNo;
            installment.InstallmentId = i;
            installment.InstallmentNo = i;
            installment.InstallmentDate = moment(this._installmentDate).format('YYYY-MM-DD').toString();
            installment.TrainingPeriodStart = moment(this._trainingStartDate).format('YYYY-MM-DD').toString();
            this._trainingEndDate = this.dateAdd(this.dateAdd(this._trainingStartDate, 1, 'months'), -1, 'days');
            installment.TrainingPeriodEnd = moment(this._trainingEndDate).format('YYYY-MM-DD').toString();
            installment.OriginalCustomer = this._lastInsallment.OriginalCustomer;
            installment.PayerId = this._lastInsallment.PayerId;
            installment.Text = moment(installment.TrainingPeriodStart).format(this._dateFormat) + ' - ' + moment(installment.TrainingPeriodEnd).format(this._dateFormat);
            installment.ContractName = this.SelectedRenewTemplate.PackageName;// template name
            installment.PayerName = this._lastInsallment.PayerName;
            installment.AdonPrice = 0;
            installment.Amount = 0;
            installment.ItemAmount = 0;
            installment.ActivityPrice = 0;
            //adding monthly items
            if (this.selectedContract.EveryMonthItemList != null) {
              let priotityIndex = 1;
              this.selectedContract.EveryMonthItemList.forEach(monthItem => {
                monthItem.Priority = priotityIndex;
                if (monthItem.ActivityID == this.selectedContract.ActivityId) {
                  monthItem.IsActivityArticle = true;
                }

                if (monthItem.EndOrder > 0) {
                  if (installment.InstallmentNo >= monthItem.StartOrder && installment.InstallmentNo <= monthItem.EndOrder) {
                    installment.Amount += monthItem.Price;
                    if (!monthItem.IsActivityArticle) {
                      installment.AdonPrice += monthItem.Price;
                    }
                    else {
                      installment.ActivityPrice += monthItem.Price;
                    }

                    installment.ServiceAmount += monthItem.Price;
                    contractServicePrice += monthItem.Price;
                    installment.AddOnList.push(monthItem);
                    priotityIndex++;
                  }
                }
                else if (installment.InstallmentNo >= monthItem.StartOrder) // no end order found
                {
                  installment.Amount += monthItem.Price;
                  if (!monthItem.IsActivityArticle)
                    installment.AdonPrice += monthItem.Price;
                  else
                    installment.ActivityPrice += monthItem.Price;

                  installment.ServiceAmount += monthItem.Price;
                  contractServicePrice += monthItem.Price;
                  installment.AddOnList.push(monthItem);
                  priotityIndex++;
                }
              });
            }//end of adding monthyl Items

            installment.EstimatedOrderAmount = installment.Amount;
            installment.TemplateId = this.SelectedRenewTemplate.PackageId;
            installment.Balance = installment.Amount;
            installment.PriceListItem = this.SelectedRenewTemplate.PackageName;
            this._lastInstallmentDueDate = this.dateAdd(this._lastInstallmentDueDate, 1, 'months');
            this._trainingStartDate = this.dateAdd(this._trainingEndDate, 1, 'days');
            this._installmentDate = this.dateAdd(this._installmentDate, 1, 'months');
            this.RenewInstallmentList.push(installment);
            insCount++;
          }
          this.selectedContract.CreatedUser = this._user;
          this.selectedContract.EveryMonthItemPrice += contractServicePrice;
          this.selectedContract.ContractPrize += contractServicePrice;

          if (this.selectedContract.FixDateOfContract) {
            let noofMOnths = moment(this.selectedContract.ContractEndDate).diff(moment(this.TrainingStartDate), 'months');
            if (noofMOnths > 0) {
              this.SelectedRenewTemplate.NoOfMonths = noofMOnths;
              this.SelectedRenewTemplate.NumOfInstallments = this.RenewInstallmentList.length;
            }
          }
          this.SetTrainingDates();
        }//end of no of installments
      }//end of last installment
      this.isSaveEnabled = true;
    }//end of renew template
  }

  SetTrainingDates() {
    let generatedOrderscount = this.RenewInstallmentList.length;
    let traininfStartDate: Date = this.TrainingStartDate;
    let trainingEndDate: Date = this.selectedContract.ContractEndDate;

    if (generatedOrderscount < this.SelectedRenewTemplate.NoOfMonths && generatedOrderscount > 0) {
      let noofSections: number = this.SelectedRenewTemplate.NoOfMonths / generatedOrderscount;
      let lastInstallment: any = this.RenewInstallmentList[this.RenewInstallmentList.length - 1];

      this.RenewInstallmentList.forEach(installment => {
        installment.TrainingPeriodStart = moment(traininfStartDate).format('YYYY-MM-DD').toString();
        if (lastInstallment === installment) {
          trainingEndDate = this.selectedContract.ContractEndDate;
          installment.TrainingPeriodEnd = moment(trainingEndDate).format('YYYY-MM-DD').toString();
          if (moment(installment.TrainingPeriodEnd).isBefore(moment(installment.TrainingPeriodStart))) {
            installment.TrainingPeriodEnd = moment(installment.TrainingPeriodStart).format('YYYY-MM-DD').toString();
          }
          installment.Text = moment(installment.TrainingPeriodStart).format(this._dateFormat) + ' - ' + moment(installment.TrainingPeriodEnd).format(this._dateFormat);
        } else {
          trainingEndDate = this.dateAdd(traininfStartDate, 1, 'months');
          trainingEndDate = this.dateAdd(trainingEndDate, -1, 'days');
          installment.TrainingPeriodEnd = moment(trainingEndDate).format('YYYY-MM-DD').toString();;
          if (moment(installment.TrainingPeriodEnd).isBefore(moment(installment.TrainingPeriodStart))) {
            installment.TrainingPeriodEnd = moment(installment.TrainingPeriodStart).format('YYYY-MM-DD').toString();
          }
          installment.Text = moment(installment.TrainingPeriodStart).format(this._dateFormat) + ' - ' + moment(installment.TrainingPeriodEnd).format(this._dateFormat);
          traininfStartDate = this.dateAdd(trainingEndDate, 1, 'days');
        }
      });
    } else {
      let lastInstallment: any = this.RenewInstallmentList[this.RenewInstallmentList.length - 1];
      this.RenewInstallmentList.forEach(installment => {
        if (lastInstallment === installment) {
          trainingEndDate = this.selectedContract.ContractEndDate;
          installment.TrainingPeriodEnd = moment(trainingEndDate).format('YYYY-MM-DD').toString();
          if (moment(installment.TrainingPeriodEnd).isBefore(moment(installment.TrainingPeriodStart))) {
            installment.TrainingPeriodEnd = moment(installment.TrainingPeriodStart).format('YYYY-MM-DD').toString();
          }
          installment.Text = moment(installment.TrainingPeriodStart).format(this._dateFormat) + ' - ' + moment(installment.TrainingPeriodEnd).format(this._dateFormat);
        }
      });
    }
  }

  createInstallments() {
    if (this.contractform.valid) {
      if (this._selectedMember.Age <= 0 && this.SelectedRenewTemplate.MaxAge > 0) {
        this.messageService.openMessageBox('CONFIRM',
          {
            messageTitle: 'Exceline',
            messageBody: 'MEMBERSHIP.ContractTemplateConfirmation',
            msgBoxId: 'GENINS'
          });
      } else if (this.SelectedRenewTemplate.MaxAge < this._selectedMember.Age && this._selectedMember.Age > 0 && this.SelectedRenewTemplate.MaxAge > 0) {
        this.messageService.openMessageBox('WARNING',
          {
            messageTitle: 'Exceline',
            messageBody: 'MEMBERSHIP.MsgAgeNotMatch'
          });
      } else {
        this.generateInstallments();
      }
    } else {
      this.validationEnabled = true;
      UsErrorService.validateAllFormFields(this.contractform, this.formErrors, this.validationMessages);
    }
  }

  moveOrdersUp() {
    this.messageService.openMessageBox('CONFIRM',
      {
        messageTitle: 'Exceline',
        messageBody: 'MEMBERSHIP.ConfirmDateUp',
        msgBoxId: 'DATEUP'
      });
  }

  moveOrdersDown() {
    this.messageService.openMessageBox('CONFIRM',
      {
        messageTitle: 'Exceline',
        messageBody: 'MEMBERSHIP.ConfirmDatedwn',
        msgBoxId: 'DATEDOWN'
      });
  }

  moveOrders(direction: string) {
    if (direction === 'UP') {
      this.RenewInstallmentList.forEach(order => {
        order.DueDate = moment(this.dateAdd(order.DueDate, -1, 'months')).format('YYYY-MM-DD').toString();
        if (this.getNumberOfDaysInMonth(moment(order.TrainingPeriodEnd).year(), moment(order.TrainingPeriodEnd).month() + 1) === moment(order.TrainingPeriodEnd).date()) {
          order.TrainingPeriodStart = moment(this.dateAdd(order.TrainingPeriodStart, -1, 'months')).format('YYYY-MM-DD').toString();
          order.TrainingPeriodEnd = this.dateAdd(order.TrainingPeriodEnd, -1, 'months');
          let numberOfDaysinNewMonth = this.getNumberOfDaysInMonth(moment(order.TrainingPeriodEnd).year(), moment(order.TrainingPeriodEnd).month() + 1)
          let newEndDate: Date = new Date(moment(order.TrainingPeriodEnd).toDate().getFullYear(), moment(order.TrainingPeriodEnd).toDate().getMonth(), numberOfDaysinNewMonth);
          order.TrainingPeriodEnd = newEndDate;
          order.TrainingPeriodEnd = moment(order.TrainingPeriodEnd).format('YYYY-MM-DD').toString();
        } else {
          order.TrainingPeriodStart = moment(this.dateAdd(order.TrainingPeriodStart, -1, 'months')).format('YYYY-MM-DD').toString();
          order.TrainingPeriodEnd = moment(this.dateAdd(order.TrainingPeriodEnd, -1, 'months')).format('YYYY-MM-DD').toString();
        }
        order.Text = moment(order.TrainingPeriodStart).format(this._dateFormat) + ' - ' + moment(order.TrainingPeriodEnd).format(this._dateFormat);

      });
    } else if (direction === 'DOWN') {
      this.RenewInstallmentList.forEach(order => {
        order.DueDate = moment(this.dateAdd(order.DueDate, 1, 'months')).format('YYYY-MM-DD').toString();
        if (this.getNumberOfDaysInMonth(moment(order.TrainingPeriodEnd).year(), moment(order.TrainingPeriodEnd).month() + 1) === moment(order.TrainingPeriodEnd).date()) {
          order.TrainingPeriodStart = moment(this.dateAdd(order.TrainingPeriodStart, 1, 'months')).format('YYYY-MM-DD').toString();
          order.TrainingPeriodEnd = this.dateAdd(order.TrainingPeriodEnd, 1, 'months');
          let numberOfDaysinNewMonth = this.getNumberOfDaysInMonth(moment(order.TrainingPeriodEnd).year(), moment(order.TrainingPeriodEnd).month() + 1)
          let newEndDate: Date = new Date(moment(order.TrainingPeriodEnd).toDate().getFullYear(), moment(order.TrainingPeriodEnd).toDate().getMonth(), numberOfDaysinNewMonth);
          order.TrainingPeriodEnd = newEndDate;
          order.TrainingPeriodEnd = moment(order.TrainingPeriodEnd).format('YYYY-MM-DD').toString();
        } else {
          order.TrainingPeriodStart = moment(this.dateAdd(order.TrainingPeriodStart, 1, 'months')).format('YYYY-MM-DD').toString();
          order.TrainingPeriodEnd = moment(this.dateAdd(order.TrainingPeriodEnd, 1, 'months')).format('YYYY-MM-DD').toString();
        }
        order.Text = moment(order.TrainingPeriodStart).format(this._dateFormat) + ' - ' + moment(order.TrainingPeriodEnd).format(this._dateFormat);
      });
    }
  }

  onTemplateNoChange(value: any) {
    if (!isNaN(value)) {
      this.memberService.getRenewTemplate(this.selectedContract.ContractId, 'ID').pipe(takeUntil(this.destroy$)).subscribe(
        result => {
          if (result) {
            this.SelectedRenewTemplate = result.Data;
            this.PrepareArticles();
          } else {
            this.SelectedRenewTemplate = {};
          }
        }
      );
    }
  }

  lockInPeriodChange(value: any) {
    if (!isNaN(value)) {
      if (Number(value) > 0) {
        this.selectedContract.LockInPeriodUntilDate = this.dateAdd(this.RenewFromDate, Number(value), 'months');
        this.contractform.patchValue({ LockInDate: { date: Extension.GetFormatteredDate(this.selectedContract.LockInPeriodUntilDate) } });
      }
    }
  }

  priceGuranteePeriodChange(value: any) {
    if (!isNaN(value)) {
      if (Number(value) > 0) {
        this.selectedContract.PriceGuarantyUntillDate = this.dateAdd(this.RenewFromDate, Number(value), 'months');
        this.contractform.patchValue({ PriceGuranteeDate: { date: Extension.GetFormatteredDate(this.selectedContract.PriceGuarantyUntillDate) } });
      }
    }
  }

  noOfMonthsChange(value: any) {
    if (!isNaN(value)) {
      if (this.RenewFromDate) {
        this.RenewToDate = this.dateAdd(this.RenewFromDate, Number(value), 'months');
        this.SelectedRenewTemplate.NoOfMonths = Number(value)
      }
    }
  }

  dateAdd(startDate: any, count: any, interval: string): Date {
    return moment(startDate).add(count, interval).toDate()
  }

  getNumberOfDaysInMonth(year: number, month: number): number {
    return moment(year + '-' + month, 'YYYY-MM').daysInMonth();
  }

  closeContractRenew() {
    let base = '/membership/card/' + this._selectedMember.Id + '/' + this._selectedMember.BranchId + '/' + MemberRole[this._selectedMember.Role] + '/contracts/';
    this.router.navigate([base + '/' + this.selectedContract.Id]);
  }

  deleteOrder(item: any) {
    this._deletingOrder = item;
    this.messageService.openMessageBox('CONFIRM',
      {
        messageTitle: 'Exceline',
        messageBody: 'MEMBERSHIP.ConfirmDatedwn',
        msgBoxId: 'DELORDER'
      });
  }

  removeOrder() {
    let deletingIndex = this.RenewInstallmentList.indexOf(this._deletingOrder);
    this.RenewInstallmentList.splice(deletingIndex, 1);
  }

  openDetailView(content, selectedOrder) {
    this.selectedOrder = selectedOrder;
    const dueDate = new Date(selectedOrder.DueDate);
    this.selectedOrder = selectedOrder;
    this.selectedOrder.DueDateFormated = { year: dueDate.getFullYear(), month: dueDate.getMonth() + 1, day: dueDate.getDate() };


    if (!selectedOrder.isOpened || selectedOrder.isFormatedDates) {
      const traningStart = new Date(selectedOrder.TrainingPeriodStart);
      this.selectedOrder.TrainingPeriodStart = { year: traningStart.getFullYear(), month: traningStart.getMonth() + 1, day: traningStart.getDate() };
      const traningEnd = new Date(selectedOrder.TrainingPeriodEnd);
      this.selectedOrder.TrainingPeriodEnd = { year: traningEnd.getFullYear(), month: traningEnd.getMonth() + 1, day: traningEnd.getDate() };
      const installmentDate = new Date(selectedOrder.InstallmentDate);
      this.selectedOrder.InstallmentDate = { year: installmentDate.getFullYear(), month: installmentDate.getMonth() + 1, day: installmentDate.getDate() };
      const originalDueDate = new Date(selectedOrder.OriginalDueDate);
      this.selectedOrder.OriginalDueDate = { year: originalDueDate.getFullYear(), month: originalDueDate.getMonth() + 1, day: originalDueDate.getDate() };
      const estimatedInvoiceDate = new Date(selectedOrder.EstimatedInvoiceDate);
      this.selectedOrder.EstimatedInvoiceDate = { year: estimatedInvoiceDate.getFullYear(), month: estimatedInvoiceDate.getMonth() + 1, day: estimatedInvoiceDate.getDate() };
    }

    selectedOrder.isOpened = true;
    this.modalReference = this.modalService.open(content);
  }

  closeOderDetailModal() {
    this.modalReference.close();
  }

  saveContractRenewal() {

    let contractRenewDetails = {
      renewedContract: this.selectedContract,
      installmentList: this.RenewInstallmentList,
      branchId: this._branchId
    }
    this.memberService.renewMemberContract(contractRenewDetails).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          this.closeContractRenew();
        }
      },
      error => {
        this.messageService.openMessageBox('WARNING',
          {
            messageTitle: 'Exceline',
            messageBody: 'MEMBERSHIP.SaveError'
          });
      }
    );
  }

  manipulatedate(dateInput: any) {
    if (dateInput.date !== undefined) {
      const tempDate: Date = new Date(dateInput.date.year, dateInput.date.month - 1, dateInput.date.day);
      return moment(tempDate).format('YYYY-MM-DD').toString();
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();

    if (this.modalReference) {
      this.modalReference.close();
    }
    if (this.templateModel) {
      this.templateModel.close();
    }
  }
}
