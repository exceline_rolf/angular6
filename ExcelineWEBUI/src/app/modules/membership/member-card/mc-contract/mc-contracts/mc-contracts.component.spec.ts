import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { McContractsComponent } from './mc-contracts.component';


describe('McContractsComponent', () => {
  let component: McContractsComponent;
  let fixture: ComponentFixture<McContractsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ McContractsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(McContractsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
