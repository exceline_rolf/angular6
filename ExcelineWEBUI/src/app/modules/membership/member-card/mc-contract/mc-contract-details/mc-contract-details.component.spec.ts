import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { McContractDetailsComponent } from './mc-contract-details.component';

describe('McContractDetailsComponent', () => {
  let component: McContractDetailsComponent;
  let fixture: ComponentFixture<McContractDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ McContractDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(McContractDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
