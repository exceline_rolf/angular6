import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { ExceMemberService } from 'app/modules/membership/services/exce-member.service';
import { ExceToolbarService } from 'app/modules/common/exce-toolbar/exce-toolbar.service';
import { DataTableFilterPipe } from 'app/shared/components/us-data-table/tools/data-table-filter-pipe';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-mc-contract-templates',
  templateUrl: './mc-contract-templates.component.html',
  styleUrls: ['./mc-contract-templates.component.scss']
})
export class McContractTemplatesComponent implements OnInit, AfterViewInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private _locale: string;
  private _brachId: Number;
  private _contractType: Number;
  private _originalTemplateList: any;
  private _filterPipe: DataTableFilterPipe = new DataTableFilterPipe();

  _contractTemplates: any;
  constructor(
    private memebrService: ExceMemberService,
    private toolbarService: ExceToolbarService
  ) { }

  ngOnInit() {
    const language = this.toolbarService.getSelectedLanguage();
    this._locale = language.culture;
    // this._dateFormat = 'DD.MM.YYYY';

  this.toolbarService.langUpdated.pipe(takeUntil(this.destroy$)).subscribe(
      (lang) => {
        if (lang) {
          this._locale = lang.culture;
        }
      }
    );
  }

  ngAfterViewInit(): void {
    this.memebrService.GetContractTemplates(1, 1).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          this._originalTemplateList = result.Data
          this._contractTemplates = this._originalTemplateList;
        }
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  filterTemplates(numberValue: any, nameValue: any, type: any) {
    if (type === 'NUMBER') {
      if (numberValue) {
        this._contractTemplates = this._filterPipe.transform('TemplateNumber', 'TEXT', this._originalTemplateList, numberValue);
      }
      if (nameValue) {
        this._contractTemplates = this._filterPipe.transform('PackageName', 'TEXT', this._contractTemplates, nameValue);
      }
    } else if (type === 'NAME') {
      if (nameValue) {
        this._contractTemplates = this._filterPipe.transform('PackageName', 'TEXT', this._contractTemplates, nameValue);
      }
      if (numberValue) {
        this._contractTemplates = this._filterPipe.transform('TemplateNumber', 'TEXT', this._originalTemplateList, numberValue);
      }
    }
  }

}
