import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { UsbModal } from '../../../../../shared/components/us-modal/us-modal';
import { UsbModalRef } from 'app/shared/components/us-modal/modal-ref';
import { ExceMemberService } from 'app/modules/membership/services/exce-member.service';
import { McBasicInfoService } from 'app/modules/membership/member-card/mc-basic-info/mc-basic-info.service';
import { FormBuilder, FormGroup, Validators, AbstractControl, ValidationErrors } from '@angular/forms';
import { Extension } from 'app/shared/Utills/Extensions';
import { ExceLoginService } from 'app/modules/login/exce-login/exce-login.service';
import * as moment from 'moment';
import { UsErrorService } from 'app/shared/directives/us-error/us-error.service';
import { ExceMessageService } from 'app/shared/components/exce-message/exce-message.service';
import { ExceToolbarService } from 'app/modules/common/exce-toolbar/exce-toolbar.service';
import { filter } from 'rxjs/operators';
import deepcopy from 'ts-deepcopy';
import { EntitySelectionType, ColumnDataType, MemberRole } from 'app/shared/enums/us-enum';
import { PreloaderService } from 'app/shared/services/preloader.service';
import { TranslateService } from '@ngx-translate/core';
import { Route, RouterModule, Router } from '@angular/router';
import { ExceTitleService } from '../../../services/exce-title.service';
import { error } from 'util';
import { AdminService } from '../../../../../modules/admin/services/admin.service';
import { GymSettingsService } from '../../../../admin/gym-settings/gym-settings.service';
import { ExceContracttmpService } from 'app/shared/components/exce-contract-tmp-select/exce-contracttmp.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';


@Component({
  selector: 'app-mc-contract-reg',
  templateUrl: './mc-contract-reg.component.html',
  styleUrls: ['./mc-contract-reg.component.scss']
})
export class McContractRegComponent implements OnInit, OnDestroy {
  [name: string]: any;
  modalReference: UsbModalRef;
  orderlineModel: UsbModalRef;
  groupModalRef: UsbModalRef;
  addNewMemberModalRef: UsbModalRef;
  selectedOrder: any;
  public multipleRowSelect = true;
  public autocompleteItems = [{ id: 'NAME', name: 'NAMEE :', isNumber: false }, { id: 'ID', name: 'Idd :', isNumber: true }];
  public columns = [
    { property: 'FirstName', header: 'First Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
    { property: 'LastName', header: 'Last Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
    { property: 'IntCustId', header: 'Number', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
    { property: 'Age', header: 'Age', sortable: false, resizable: false, width: '150px', type: ColumnDataType.DATE },
    { property: 'Mobile', header: 'Mobile', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
    { property: 'StatusName', header: 'Status', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
    { property: 'BranchName', header: 'Gym Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT }
  ];
  public entitySelectionType = 'MEM';
  public defaultRoleType = 'ALL';
  public isSaveVisible = false;
  public isSaveVisible2 = false;
  locale: string;
  _deletingBooking: any;
  public itemCount: number;
  public entityItems = [];

  nextStepDisabled: boolean;

  public basicForm: FormGroup
  public bookingForm: FormGroup;
  public economyForm: FormGroup;
  public enableNext: boolean;
  public selectedCategory: any = null;
  public validationEnabled = false;
  public bookingValidationEnabled = false;
  public economyValidationEnabled = false;
  public selectedSignCategory: any = null;
  public selectedAccessProfile: any = null
  public showAccessprofile = false;
  public resources: any;
  public selectedDay: any;
  public bookingDay: any;
  public startTime: any;
  public endTime: any;
  public costPerBooking: number;
  public _memberContract: any = {};
  public bookingList: any[] = [];
  economyValid = true;
  private _selectedTemplate: any;
  private _templateModal: UsbModalRef;
  _contractCategories: any;
  public _signUpCategories: any;
  private _accessProfiles: any;
  private _activities: any;
  private _selectedMember: any;
  private _branchID: number;
  private _user: string;
  private _hasFixedDate = false;
  private _dueDay: any;
  private _originalMonthlyItemPrice: number;
  private _originalContractAmount: number;
  private _isBasicActivity = false;
  private _gymSettings: any = {};
  private _contractDueDate: Date;
  private _contractActivity: any;
  private _installmentList: any[] = [];
  private _contractBooking: any = {};
  private _selectedResource: any;
  private _yesHandlerSubcription: any;
  private _deletedItem: any;
  private _dateFormat: string;
  private _deletingOrder: any;
  private _movingOrder: any;
  private _movingDirection: string;
  private _numberOforders: number;
  private _orderlineType: string;
  private _deletingGroupmember: any;
  private _groupMembers: any = [];
  selectionType: string;
  private _title: string;
  public itemSelectionTitle: string;
  public skipNextState = false;
  public isBookingActivity: boolean;
  public _isMemberFee: boolean;
  public _isMemberFeeMonth: boolean;

  skipNumber: any;
  private modalOpen = false;
  private postpay: boolean;
  private creditPeriod: any;
  private fixedDueDate: any;
  private _contractConditions: any;
  private conditionRef: any;
  private ServiceAmount: any;

  private destroy$ = new Subject<void>();

  @ViewChild('contractWiz') public contractWiz;

  // private newMem: boolean;
  // private newMemId: any;
  // private newMemRole: any;
  // private newMemBranchId: any;

  public isPunchCard = false;

  private _selectedGymSettings: string[] = [
    'OtherPhoneCode',
    'ReqMobileRequired',
    'EcoIsMemberFee',
    'EcoIsMemberFeeMonth'
  ]

  formErrors = {
    'TemplateNo': '',
    'ContractName': '',
    'ContractCategoryName': '',
    'ActivityName': '',
    'CreatedDate': '',
    'NoOfMonths': '',
    'ContractStartDate': '',
    'ContractEndDate': '',
    'IsATG': '',
    'AutoRenew': '',
    'IsInvoiceDetails': '',
    'LockInPeriod': '',
    'LockInPeriodUntilDate': '',
    'GuarantyPeriod': '',
    'PriceGuarantyUntillDate': '',
    'NumberOfVisits': '',
    'NoofDays': '',
    'BranchName': '',
    'bookingDay': '',
    'startTime': '',
    'endTime': '',
    'costPerBooking': '',
    'dueDate': '',
    'numberOfOrders': '',
    'MemberFee': '',
    'MemberFeeMonth': ''
  }

  validationMessages = {
    'TemplateNo': {
      'required': 'MEMBERSHIP.MsgConTemplateReq'
    },
    'ContractName': {
      'required': 'MEMBERSHIP.MsgConTemplateReq'
    },
    'ContractCategoryName': {
      'required': 'MEMBERSHIP.MsgContypeReq'
    },
    'ActivityName': {
      'required': 'MEMBERSHIP.GymEmActiviytReq'
    },
    'NoOfMonths': {
      'min': 'MEMBERSHIP.MsgNoOfMonthsReq'
    },
    'ContractStartDate': {
      'required': 'MEMBERSHIP.MsgContracteStartDateReq'
    },
    'ContractEndDate': {
      'required': 'MEMBERSHIP.MsgContractendDateReq',
      'compare' : 'MEMBERSHIP.ContractStartEndDateCompare'
    },
    'BranchName': {
      'required': 'MEMBERSHIP.plsSelectGym'
    },
    'bookingDay': {
      'required': 'MEMBERSHIP.BookingDayReq'
    },
    'startTime': {
      'required': 'MEMBERSHIP.MsgNoStartStartTime'
    },
    'endTime': {
      'required': 'MEMBERSHIP.MsgNoEndTime'
    },
    'costPerBooking': {
      'required': 'MEMBERSHIP.BookingCostReq'
    },
    'dueDate': {
      'required': 'MEMBERSHIP.DueDateReq'
    },
    'numberOfOrders': {
      'required': 'MEMBERSHIP.NoOrdersReq'
    },
    'MemberFeeMonth': {
      'required': 'MEMBERSHIP.plsSelectMemberFeeMonth'
    }
  }

  months = [
    { key: '1', value: 'MEMBERSHIP.January' },
    { key: '2', value: 'MEMBERSHIP.February' },
    { key: '3', value: 'MEMBERSHIP.March' },
    { key: '4', value: 'MEMBERSHIP.April' },
    { key: '5', value: 'MEMBERSHIP.May' },
    { key: '6', value: 'MEMBERSHIP.June' },
    { key: '7', value: 'MEMBERSHIP.July' },
    { key: '8', value: 'MEMBERSHIP.August' },
    { key: '9', value: 'MEMBERSHIP.September' },
    { key: '10', value: 'MEMBERSHIP.October' },
    { key: '11', value: 'MEMBERSHIP.November' },
    { key: '12', value: 'MEMBERSHIP.December' }
  ]


  constructor(
    private basicInfoService: McBasicInfoService,
    private modalService: UsbModal,
    private templateService: ExceContracttmpService,
    private memberService: ExceMemberService,
    private fb: FormBuilder,
    private loginService: ExceLoginService,
    private messageService: ExceMessageService,
    private toolbarService: ExceToolbarService,
    private translateService: TranslateService,
    private router: Router,
    private titleService: ExceTitleService,
    private adminService: AdminService,
    private exceLoginService: ExceLoginService,
    private gymListService: GymSettingsService
  ) {   }

  ngOnInit() {
    this.basicInfoService.currentMember.pipe(
      takeUntil(this.destroy$)
    ).subscribe(currentMember => this._selectedMember = currentMember);
    this._branchID = this.loginService.SelectedBranch.BranchId;
    this.fetchData(this.loginService.SelectedBranch.BranchId);
    this._user = this.loginService.CurrentUser.username;
    this.titleService.setTitle('MEMBERSHIP.RegisterContract');
    this.basicForm = this.fb.group(
      {
        'TemplateNo': [null, Validators.required],
        'ContractName': [null, Validators.required],
        'ContractCategoryName': [null, Validators.required],
        'ActivityName': [null, Validators.required],
        'CreatedDate': [null],
        'NoOfMonths': [null, Validators.pattern('^[0-9]+$')],
        'ContractStartDate': [null, Validators.required],
        'ContractEndDate': [null, Validators.required],
        'IsATG': [null],
        'AutoRenew': [null],
        'IsInvoiceDetails': [null],
        'LockInPeriod': [null],
        'LockInPeriodUntilDate': [null],
        'GuarantyPeriod': [null],
        'PriceGuarantyUntillDate': [null],
        'NumberOfVisits': [null],
        'NoofDays': [null],
        'BranchName': [null, Validators.required],
        'ContractCondition': [null]
      }
    );

    this.bookingForm = this.fb.group(
      {
        'bookingDay': [null, Validators.required],
        'startTime': [null, Validators.required],
        'endTime': [null, Validators.required],
        'costPerBooking': [null, Validators.required]
      }
    );

    this.economyForm = this.fb.group(
      {
        'dueDate': [null, Validators.required],
        'numberOfOrders': [null, Validators.required],
        'MemberFee': [null],
        'MemberFeeMonth': [null]
      }
    );


    this.basicForm.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(_ => {
      UsErrorService.onValueChanged(this.basicForm, this.formErrors, this.validationMessages);
    });

    this.memberService.getAccessProfiles(this._selectedMember.Gender).pipe(takeUntil(this.destroy$)).subscribe(
      accessresult => {
        if (accessresult) {
          this._accessProfiles = accessresult.Data;
        }
      }
    );

    this.memberService.getCategories('PACKAGE').pipe(takeUntil(this.destroy$)).subscribe(
      packageresult => {
        if (packageresult) {
          this._contractCategories = packageresult.Data;
        }
      }
    );

    this.memberService.getCategories('SIGNUP').pipe(takeUntil(this.destroy$)).subscribe(
      signupresult => {
        if (signupresult) {
          this._signUpCategories = signupresult.Data;
        }
      }
    );

    // ** GET POSTPAY SETTING FROM BRANCH-SETTINGS **
    this.adminService.getGymSettings('ECONOMY', null,  this._branchID).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if ( result) {
          this.postpay = result.Data.PostPay
        }
      }
    )

    // ** GET CreditPeriod **
    this.adminService.GetGymCompanySettings('Economy').pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          this.creditPeriod = Number(result.Data.CreditPeriod)
          this.fixedDueDate = Number(result.Data.FixedDueDay)
        }
      }
    );


    this.memberService.getSelectedGymSettings({ settingNames: this._selectedGymSettings, isGymSetting: true, branchId: this._branchID }).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          this._gymSettings = result.Data;
        }
      }
    );

    this.memberService.getContractActivities().pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          this._activities = result.Data;
        }
      }
    );

    this.memberService.GetGymCompanySettings('CONTRACT').pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          this._contractConditions = result.Data;
        }
      });

    // get group member for adding to the group contract
    this.memberService.GetGroupMembers(this._selectedMember.Id).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          this._groupMembers = result.Data;
        }
      }
    )

    // subscribe for the messges
    this.messageService.yesCalledHandle.pipe(takeUntil(this.destroy$)).subscribe((value) => {
      switch (value.id) {
        case 'BOOKING':
          this.deleteBooking();
          break;
        case 'MONTHLY':
          this.deleteMonthlyItem();
          break;
        case 'ITEM':
          this.deleteStartUpItem();
          break;
        case 'ORDER':
          this.deleteOrder();
          break;
        case 'MOVEORDER':
          this.moveOrders();
          break;
        case 'GROUPMEMBER':
          this.deleteGroupmember();
          break;
        case 'ONEXIT':
          this.exitContractView();
          break;
      }

    });

    this.economyForm.get('MemberFeeMonth').valueChanges.pipe(takeUntil(this.destroy$)).subscribe(
      form => {
        if (this.economyForm.valid) {
          if (form) {
            this.economyValid = true;
          }
        } else {
          if (form) {
            this.economyValid = true;
          }
        }
      }
      // this is done to handle the function
    );

    const language = this.toolbarService.getSelectedLanguage();
    this.locale = language.culture;
    this._dateFormat = 'DD.MM.YYYY';

    this.toolbarService.langUpdated.pipe(takeUntil(this.destroy$)).subscribe(
      (lang) => {
        if (lang) {
          this.locale = lang.culture;
        }
      }
    );
  }

  selectTemplate(content: any) {
    this.validationEnabled = false;
    // tslint:disable-next-line:no-unused-expression
    this._templateModal = this.modalService.open(content);
    this.modalOpen = true;
  }

  getContractTemplateById(val) {
    this.templateService.getContractTemplates(this.loginService.SelectedBranch.BranchId, 0).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        PreloaderService.hidePreLoader();
        if (result) {
          const templates = result.Data;
          templates.forEach(templ => {
              if (Number(templ.TemplateNumber === val)) {
                this.templateSelected(templ)
                return
              }
          });
      }
    })
  }

  templateSelected(event: any) {
    // validate article avila bility
    this._selectedTemplate = event;
    if (this.modalOpen) {
      this._templateModal.close();
      this.modalOpen = false;
    }
    let unavailableConteractItemCount = 0;
    const unavailableStartUpItemCount = 0;
    const contractItems: any = this._selectedTemplate.EveryMonthItemList.concat(this._selectedTemplate.StartUpItemList);
    unavailableConteractItemCount = contractItems.filter(x => x.IsAvailableForGym === false).length;

    if (unavailableConteractItemCount > 0) {

    }

    this._memberContract.IsATG = this._selectedTemplate.IsATG;
    this._memberContract.MaxATGAmount = this._selectedTemplate.AmountForATG;
    this._memberContract.MemberId = this._selectedMember.Id;
    this._memberContract.BranchName = this._selectedTemplate.BranchName;
    if (!this._memberContract.BranchName) {
      let branch = this.loginService.SelectedBranch;
      // branch = branch.BranchName.split('_')
      this._memberContract.BranchName = branch.BranchName.split('_')[1];
      this._memberContract.BranchName = branch[1]
    }
    this._memberContract.ContractId = this._selectedTemplate.PackageId;
    this._memberContract.NumberOfVisits = this._selectedTemplate.NumOfVisits;
    this._memberContract.ContractName = this._selectedTemplate.PackageName;
    this._memberContract.BranchId = this._branchID;
    this._memberContract.EnrollmentFee = this._selectedTemplate.EnrollmentFee;
    this._memberContract.NoOfInstallments = this._selectedTemplate.NumOfInstallments;
    this._memberContract.NoofDays = this._selectedTemplate.NoOfDays;
    this._memberContract.ContractPrize = this._selectedTemplate.PackagePrice;
    this._memberContract.CreatedDate = new Date();
    this._memberContract.NoOfMonths = this._selectedTemplate.NoOfMonths;
    this._memberContract.IsBookingActivity = this._selectedTemplate.IsBookingActivity;
    this.isBookingActivity = this._memberContract.IsBookingActivity;
    this._memberContract.ContractCondition = this._selectedTemplate.ContractCondition;
    this._memberContract.ContractConditionId = this._selectedTemplate.ContractConditionId;
    this._memberContract.CreatedUser = this._user;

    if (!this.isBookingActivity) {
      this.skipNumber = 2;
    }

    // Manipulating start date
    this._memberContract.ContractStartDate = this._selectedTemplate.FixStartDateOfContract !== this._selectedTemplate.FixStartDateOfContract ? null :  new Date() ;
    let endDate;
    if (this._selectedTemplate.FixDateOfContract != null) {
      this._memberContract.ContractEndDate = this._selectedTemplate.FixDateOfContract;
      this._hasFixedDate = true;
      this._memberContract.NoOfMonths = moment(this._memberContract.ContractStartDate).diff(moment(this._memberContract.ContractEndDate), 'months');
      endDate = Extension.GetFormatteredDate(this._selectedTemplate.FixDateOfContract)
    } else if (this._selectedTemplate.RestPlusMonth) {
      const periodLimit = Number(this._selectedTemplate.NoOfMonths);
      const noofDaysForMonth = this.getNumberOfDaysInMonth(moment(this._memberContract.ContractStartDate).year(), moment(this._memberContract.ContractStartDate).month() + 1);
      const restDays = (noofDaysForMonth - moment(this._memberContract.ContractStartDate).date());
      this._memberContract.ContractEndDate = this.dateAdd(this._memberContract.ContractStartDate, restDays, 'days');
      this._memberContract.ContractEndDate = this.dateAdd(this._memberContract.ContractEndDate, periodLimit, 'months');
      endDate = Extension.GetFormatteredDate(this._memberContract.ContractEndDate)
    } else {
      // this._memberContract.ContractEndDate = new Date(new Date().setFullYear(new Date().getFullYear() + 1));
      this._memberContract.ContractEndDate = this.dateAdd(this._memberContract.ContractStartDate, this._selectedTemplate.NoOfMonths, 'months')
      this._memberContract.ContractEndDate.setDate(this._memberContract.ContractEndDate.getDate() - 1);
      this._hasFixedDate = false;
      endDate = Extension.GetFormatteredDate(this._memberContract.ContractEndDate)
    }

    this._memberContract.ActivityId = this._selectedTemplate.ActivityId;
    this._memberContract.ActivityName = this._selectedTemplate.ActivityName;
    this._memberContract.ContractTypeId = this._selectedTemplate.ContractTypeValue.Id;
    this._memberContract.RestPlusMonth = this._selectedTemplate.RestPlusMonth;
    this._memberContract.LockInPeriod = this._selectedTemplate.LockInPeriod;
    this._memberContract.GuarantyPeriod = this._selectedTemplate.PriceGuaranty;

    this._memberContract.AutoRenew = this._selectedTemplate.AutoRenew;
    this._memberContract.IsInvoiceDetails = this._selectedTemplate.IsInvoiceDetail;
    this._memberContract.ArticleId = this._selectedTemplate.ArticleNo;
    this._memberContract.ArticleName = this._selectedTemplate.ArticleText;
    this._memberContract.AccessProfileId = this._selectedTemplate.AccessProfileId;

    this._memberContract.FixDateOfContract = this._selectedTemplate.FixDateOfContract;
    this._memberContract.FirstDueDate = this._selectedTemplate.FirstDueDate;
    this._memberContract.SecondDueDate = this._selectedTemplate.SecondDueDate;
    this._memberContract.CreditoDueDays = this._selectedTemplate.CreditDueDays;

    if (this._dueDay > 0) {
      this._memberContract.DueDateSetting = this._dueDay;
    } else {
      this._memberContract.DueDateSetting = this.fixedDueDate;
    }
    this._memberContract.TemplateNo = this._selectedTemplate.TemplateNumber;
    this._memberContract.ContractCategoryName = this._selectedTemplate.PackageCategory.Name;
    this._memberContract.NoOfMonths = this._selectedTemplate.NoOfMonths;
    this._memberContract.ActivityName = this._selectedTemplate.ActivityName;

    this._memberContract.StartUpItemPrice = this._selectedTemplate.StartUpItemPrice;
    this._memberContract.MemberName = this._selectedMember.FirstName + ' ' + this._selectedMember.LastName;
    this._memberContract.EveryMonthItemPrice = this._selectedTemplate.EveryMonthItemsPrice;

    this._memberContract.StartUpItemList = this._selectedTemplate.StartUpItemList;
    this._memberContract.EveryMonthItemList = this._selectedTemplate.EveryMonthItemList;
    this._memberContract.EveryMonthItemPrice = this._selectedTemplate.EveryMonthItemsPrice;
    this._memberContract.ServiceAmount = this._selectedTemplate.ServicePrice;
    // this._memberContract.MemberFeeMonth = this._selectedTemplate.servicePrice;

    this._originalMonthlyItemPrice = this._selectedTemplate.EveryMonthItemsPrice;
    this._memberContract.ContractTypeName = this._selectedTemplate.ContractTypeValue.Code;
    this._memberContract.AmountPerInstallment = this._selectedTemplate.RatePerInstallment;

    this._memberContract.OrderPrice = this._selectedTemplate.OrderPrice;
    this._originalContractAmount = this._selectedTemplate.PackagePrice;
    this._memberContract.ContractPrize = this._selectedTemplate.PackagePrice;
    this._memberContract.ContractDescription = this._selectedTemplate.ContractDescritpion;
    this._memberContract.AccountNo = this._selectedMember.AccountNo;
    this._memberContract.TemplateNo = this._selectedTemplate.TemplateNumber;
    this._memberContract.CategoryId = this._selectedTemplate.ContractTypeValue.Id;
    this._memberContract.ContractCategoryName = this._selectedTemplate.PackageCategory.Name;
    this._memberContract.ContractTypeCode = this._selectedTemplate.PackageCategory.Code;

    this._memberContract.UseTodayAsDueDate = this._selectedTemplate.UseTodayAsDueDate;

    this._memberContract.NextTemplateId = this._selectedTemplate.NextTemplateId;
    this._memberContract.NextTemplateName = this._selectedTemplate.NextContractTemplateName;
    this._memberContract.NextTemplateNo = this._selectedTemplate.NextTemplateNo;
    this._memberContract.CreditPeriod = this.creditPeriod;
    this._isBasicActivity = this._selectedTemplate.IsBasicActivity;
    this._memberContract.PostPay = this.postpay;
    this._memberContract.BrisID = this._selectedMember.UserId;
    this._memberContract.AgressoID = this._selectedMember.AgressoId !== undefined ? this._selectedMember.AgressoId : -1;
    this._memberContract.ContractBookingList = [];
    this._memberContract.GroupMembers = [];


    if (this._memberContract.LockInPeriod > 0) {
      this.onContractLimitChange(this._memberContract.LockInPeriod, 'LOCK');
    }

    if (this._memberContract.GuarantyPeriod > 0) {
      this.onContractLimitChange(this._memberContract.GuarantyPeriod, 'PRICE');
    }

    if (this._memberContract.NoOfMonths > 0) {
      this.onNoOfPeriodChange(this._memberContract.NoOfMonths, 'MONTHS');
    } else if (this._memberContract.NoofDays) {
      this.onNoOfPeriodChange(this._memberContract.NoofDays, 'DAYS');
    }

    // handling member fee
    if (this._selectedTemplate.IsBasicActivity && this._gymSettings.EcoIsMemberFee) {
      if (this._selectedMember.MemberFeeArticle !== undefined) {
        this._memberContract.MemberFeeArticleID = this._selectedMember.MemberFeeArticle.ArticleId;
        this._memberContract.MemberFeeArticleName = this._selectedMember.MemberFeeArticle.ItemName;

        if (this._selectedTemplate.MemberFeeArticle.ArticleId > 0) {
          this._selectedTemplate.MemberFeeArticle.IsStartUpItem = true;
          this._memberContract.StartUpItemList.push(this._selectedTemplate.MemberFeeArticle);
          this._memberContract.StartUpItemPrice += this._selectedTemplate.MemberFeeArticle.Price;
        }
      }
    }

    // handle due date
    this.setDueDate();
    this._memberContract.OrderPrice = this._memberContract.OrderPrice === 0 ? this._selectedTemplate.PackagePrice : this._memberContract.OrderPrice;
    this.selectedCategory = this._contractCategories.filter(cat => cat.Id === this._memberContract.CategoryId)[0];

    // manipulate access profile
    this._contractActivity = this._activities.filter(act => act.Id === this._memberContract.ActivityId)[0];
    if (this._contractActivity !== undefined && this._contractActivity.ActivitySetting !== undefined) {
      if ((this._contractActivity.ActivitySetting.IsContractTimeLimited && this._memberContract.ContractTypeCode === 'CONTRACT') ||
      (this._contractActivity.ActivitySetting.IsPunchCardLimited && this._memberContract.ContractTypeCode === 'PUNCHCARD')) {
        this.showAccessprofile = true;
        this.selectedAccessProfile = this._accessProfiles.filter(acc => acc.Id === this._memberContract.AccessProfileId);
      } else {
        this.showAccessprofile = false;
      }

      if (this._memberContract.ContractTypeCode !== 'PUNCHCARD') {
        if (!this._contractActivity.ActivitySetting.IsContractBooking) {
          this.skipNumber = '2';
        }
      }
    }

    // get the resources
    this.memberService.getResources(this._branchID, '', -1, this._memberContract.ActivityId, true).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          this.resources = result.Data;
        }
      }
    )

    // et the UI state
    if (this._memberContract.ContractTypeCode === 'PUNCHCARD') {
      this.isPunchCard = true;
    }

    this.basicForm.patchValue(
      {
        ContractName: this._memberContract.ContractName,
        TemplateNo: this._memberContract.TemplateNo,
        ContractCategoryName: this._memberContract.ContractCategoryName,
        ActivityName: this._memberContract.ActivityName,
        CreatedDate: { date: Extension.GetFormatteredDate(new Date()) },
        NoOfMonths: this._memberContract.NoOfMonths,
        ContractStartDate: { date: this._selectedTemplate.FixStartDateOfContract !== null ?
          Extension.GetFormatteredDate(this._selectedTemplate.FixStartDateOfContract) : Extension.GetFormatteredDate(new Date()) },
        ContractEndDate: { date: endDate },
        IsATG: this._memberContract.IsATG,
        AutoRenew: this._memberContract.AutoRenew,
        IsInvoiceDetails: this._memberContract.IsInvoiceDetails,
        LockInPeriod: this._memberContract.LockInPeriod,
        LockInPeriodUntilDate: { date: Extension.GetFormatteredDate(this._memberContract.LockInPeriodUntilDate) },
        GuarantyPeriod: this._memberContract.GuarantyPeriod,
        PriceGuarantyUntillDate: { date: Extension.GetFormatteredDate(this._memberContract.PriceGuarantyUntillDate) },
        NumberOfVisits: this._memberContract.NumberOfVisits,
        NoofDays: this._memberContract.NoofDays,
        BranchName: this._memberContract.BranchName,
        NoOfVisits: this._memberContract.NumberOfVisits
      }
    );
    this.validationEnabled = true;
    UsErrorService.validateAllFormFields(this.basicForm, this.formErrors, this.validationMessages);

    // update Economy form
    this.economyForm.patchValue(
      {
        dueDate: {
          date: {
            year: this._contractDueDate.getFullYear(),
            month: this._contractDueDate.getMonth() + 1,
            day: this._contractDueDate.getDate()
          }
        },
        numberOfOrders: this._memberContract.NoOfInstallments,
        MemberFeeMonth: this._selectedTemplate.ServicePrice
      }
    );

    // add validations to economy form
    if (this._isBasicActivity && this._gymSettings.EcoIsMemberFeeMonth) {
      this.economyForm.get('MemberFeeMonth').setValidators(Validators.required);
      this.economyForm.get('MemberFeeMonth').updateValueAndValidity();
    }
  }

  cancelContractSelection() {
    this._templateModal.close();
    this.modalOpen = false;
  }

  onNoOfPeriodChange(value: any, periodType: string) {
    const period = Number(value);
    if (!isNaN(period)) {
      if (periodType === 'MONTHS') {
        if (!this._memberContract.NoofDays || this._memberContract.NoofDays <= 0) {
          this.setContractEndDate(period, 0);
        } else {
          this.basicForm.patchValue({
            NoOfMonths: 0
          });
        }
      } else if (periodType === 'DAYS') {
        if (!this._memberContract.NoOfMonths || this._memberContract.NoOfMonths <= 0) {
          this.setContractEndDate(period, 0);
        } else {
          this.basicForm.patchValue({
            NoofDays: 0
          });
        }
      }
    }
  }

  contractCategoryChanged(selectedCategoryId: any) {
    if (selectedCategoryId) {
      this._memberContract.CategoryId = selectedCategoryId;
    }
  }

  onSignCatChange(selecteSignCatId: any) {
    if (selecteSignCatId) {
      this._memberContract.SignUpCategoryId = selecteSignCatId;
    }
  }

  onAccessprofileChange(accessprofileId: any) {
    if (accessprofileId) {
      this._memberContract.AccessProfileId = accessprofileId;
    }
  }

  onContractLimitChange(period: any, limitType: string) {
    const periodLimit = Number(period);
    const noofDaysForMonth = this.getNumberOfDaysInMonth(moment(this._memberContract.ContractStartDate).year(), moment(this._memberContract.ContractStartDate).month() + 1);
    const restDays = (noofDaysForMonth - moment(this._memberContract.ContractStartDate).date());

    if (this._memberContract.RestPlusMonth) {

      if (limitType === 'LOCK') {
        this._memberContract.LockInPeriodUntilDate = this.dateAdd(this._memberContract.ContractStartDate, restDays, 'days');
        this._memberContract.LockInPeriodUntilDate = this.dateAdd(this._memberContract.LockInPeriodUntilDate, periodLimit, 'months');
        // let noOfDaysForEndMonth = this.getNumberOfDaysInMonth(moment(this._memberContract.LockInPeriodUntilDate).year(), moment(this._memberContract.LockInPeriodUntilDate).month() + 1);
        // if (noOfDaysForEndMonth != moment(this._memberContract.LockInPeriodUntilDate).date()) {
        //   this._memberContract.LockInPeriodUntilDate =
        // this.dateAdd(this._memberContract.LockInPeriodUntilDate, (noOfDaysForEndMonth - moment(this._memberContract.LockInPeriodUntilDate).date()), 'days');
        // }

        this.basicForm.patchValue(
          {
            LockInPeriodUntilDate: { date: Extension.GetFormatteredDate(this._memberContract.LockInPeriodUntilDate) }
          }
        );
      } else if (limitType === 'PRICE') {
        this._memberContract.PriceGuarantyUntillDate = this.dateAdd(this._memberContract.ContractStartDate, restDays, 'days');
        this._memberContract.PriceGuarantyUntillDate = this.dateAdd(this._memberContract.PriceGuarantyUntillDate, periodLimit, 'months');
        // let noOfDaysForEndMonth = this.getNumberOfDaysInMonth(moment(this._memberContract.PriceGuarantyUntillDate).year(), moment(this._memberContract.PriceGuarantyUntillDate).month() + 1);
        // if (noOfDaysForEndMonth != moment(this._memberContract.PriceGuarantyUntillDate).date()) {
        // this._memberContract.PriceGuarantyUntillDate =
        // this.dateAdd(this._memberContract.PriceGuarantyUntillDate, (noOfDaysForEndMonth - moment(this._memberContract.PriceGuarantyUntillDate).date()), 'days');
        // }

        this.basicForm.patchValue(
          {
            PriceGuarantyUntillDate: { date: Extension.GetFormatteredDate(this._memberContract.PriceGuarantyUntillDate) }
          }
        );
      }
    } else {
      if (limitType === 'LOCK') {
        this._memberContract.LockInPeriodUntilDate = this.dateAdd(this._memberContract.ContractStartDate, periodLimit, 'months');
        this._memberContract.LockInPeriodUntilDate.setDate(this._memberContract.LockInPeriodUntilDate.getDate() - 1);
        const date = this._memberContract.LockInPeriodUntilDate;
        if (this.isLeapYear(date) && date.getDate() === 29 && date.getMonth() === 1) {
          this._memberContract.LockInPeriodUntilDate.setDate(this._memberContract.LockInPeriodUntilDate.getDate() + 1)
        }

        // this._memberContract.LockInPeriodUntilDate = this.dateAdd(this._memberContract.ContractStartDate, periodLimit, 'months');
        this.basicForm.patchValue(
          {
            LockInPeriodUntilDate: { date: Extension.GetFormatteredDate(this._memberContract.LockInPeriodUntilDate) }
          }
        )
      } else if (limitType === 'PRICE') {
        this._memberContract.PriceGuarantyUntillDate = this.dateAdd(this._memberContract.ContractStartDate, periodLimit, 'months');
        this._memberContract.PriceGuarantyUntillDate.setDate(this._memberContract.PriceGuarantyUntillDate.getDate() - 1);
        const date = this._memberContract.PriceGuarantyUntillDate;

        if (this.isLeapYear(date) && date.getDate() === 29 && date.getMonth() === 1) {
          this._memberContract.PriceGuarantyUntillDate.setDate(this._memberContract.PriceGuarantyUntillDate.getDate() + 1)
        }

        this.basicForm.patchValue(
          {
            PriceGuarantyUntillDate: { date: Extension.GetFormatteredDate(this._memberContract.PriceGuarantyUntillDate) }
          }
        );
      }
    }
  }

  setContractEndDate(noOfMonths: Number, noOfDays: Number) {
    if (noOfMonths > 0) {
      if (this._memberContract.RestPlusMonth) {
        const noofDaysForMonth = this.getNumberOfDaysInMonth(moment(this._memberContract.ContractStartDate).year(), moment(this._memberContract.ContractStartDate).month() + 1);
        const restDays = (noofDaysForMonth - moment(this._memberContract.ContractStartDate).date());
        this._memberContract.ContractEndDate = this.dateAdd(this._memberContract.ContractStartDate, restDays, 'days');
        this._memberContract.ContractEndDate = this.dateAdd(this._memberContract.ContractEndDate, noOfMonths, 'months');
        this._memberContract.ContractEndDate.setDate(this._memberContract.ContractEndDate.getDate());

        // let noOfDaysForEndMonth = this.getNumberOfDaysInMonth(moment(this._memberContract.ContractEndDate).year(), moment(this._memberContract.ContractEndDate).month() + 1);
        // if (noOfDaysForEndMonth != moment(this._memberContract.ContractEndDate).date()) {
        //   this._memberContract.ContractEndDate = this.dateAdd(this._memberContract.ContractEndDate, (noOfDaysForEndMonth - moment(this._memberContract.ContractEndDate).date()), 'days');
        // }
      } else {
        this._memberContract.ContractEndDate = this.dateAdd(this._memberContract.ContractStartDate, noOfMonths, 'months');
        this._memberContract.ContractEndDate.setDate(this._memberContract.ContractEndDate.getDate() - 1);
        const date = this._memberContract.ContractStartDate;
        if (this.isLeapYear(date) && date.getDate() === 29 && date.getMonth() === 1) {
          this._memberContract.ContractEndDate.setDate(this._memberContract.ContractEndDate.getDate() + 1)
        }
      }
    } else if (noOfDays > 0) {
      this._memberContract.ContractEndDate = this.dateAdd(this._memberContract.ContractStartDate, noOfMonths, 'days');
      this._memberContract.ContractEndDate.setDate(this._memberContract.ContractEndDate.getDate() - 1);
    }

    this.basicForm.patchValue({
      ContractEndDate: { date: Extension.GetFormatteredDate(this._memberContract.ContractEndDate) }
    })

    if (this._memberContract.LockInPeriod > 0) {
      this.onContractLimitChange(this._memberContract.LockInPeriod, 'LOCK')
    }
  }

  selectContractCondition(content: any) {
    this.conditionRef = this.modalService.open(content, { width: '800' });
  }

  contractConditionSelected(condition) {
    if (condition) {
      this._memberContract.ContractCondition = condition.Name;
      this._memberContract.ContractConditionId = condition.Id;

      this.basicForm.patchValue({
        ContractCondition: this._memberContract.ContractCondition
      })
    }
    this.conditionRef.close();
  }

  closeContractContions() {
    this.conditionRef.close();
  }

  onLeavebasicInfo(event: any) {
    this.dateValidator();
    if (this.stepValidation(1)) {
      UsErrorService.validateAllFormFields(this.basicForm, this.formErrors, this.validationMessages);
      this.contractWiz.nextStepDisabled = true;
      this.validationEnabled = true;
    } else {
      this.validationEnabled = false;
      this.contractWiz.nextStepDisabled = false;
      UsErrorService.validateAllFormFields(this.basicForm, this.formErrors, this.validationMessages);
    }
    this.manipulateBookingPrice()
    this.updateEconomy();
  }


  stepValidation(step: number) {
    switch (step) {
      case 1:
        return (this.basicForm.controls['TemplateNo'].invalid || this.basicForm.controls['ContractName'].invalid || this.basicForm.controls['ContractCategoryName'].invalid
          || this.basicForm.controls['ActivityName'].invalid || this.basicForm.controls['NoOfMonths'].invalid || this.basicForm.controls['ContractStartDate'].invalid
          || this.basicForm.controls['ContractEndDate'].invalid)
      // case 2:
      //   return this.memberForm.controls['BankAccount'].invalid
    }
  }

  addBooking() {
    if (this.bookingForm.valid) {
      if (this._selectedResource !== undefined) {
        const booking = this.bookingForm.value;
        this._contractBooking.BookingAmount = booking.costPerBooking;
        this._contractBooking.BookingStartTime = booking.startTime;
        this._contractBooking.BookingEndTime = booking.endTime;
        this._contractBooking.Day = booking.bookingDay;
        this._contractBooking.ResourceName = this._selectedResource.Name;
        this._contractBooking.ResourceId = this._selectedResource.Id;
        this._memberContract.ContractBookingList.push(this._contractBooking);
      }
    } else {
      this.bookingValidationEnabled = true;
      UsErrorService.validateAllFormFields(this.bookingForm, this.formErrors, this.validationMessages);
    }
  }

  removeBooking(item) {
    this._deletingBooking = item;
    this.messageService.openMessageBox('CONFIRM',
      {
        messageTitle: 'Exceline',
        messageBody: 'MEMBERSHIP.ConfirmDelete',
        msgBoxId: 'BOOKING'
      });
  }

  deleteBooking() {
    const deleteingBookingIndex = this._memberContract.ContractBookingList.indexOf(this._deletingBooking);
    this._memberContract.ContractBookingList.splice(deleteingBookingIndex, 1);
  }

  manipulateBookingPrice() {
    let bookingAmount = 0;
    this._memberContract.ContractBookingList.forEach(booking => {
      const dateCount: number = this.CountDays(this._memberContract.ContractStartDate, this._memberContract.ContractEndDate, booking.Day);
      bookingAmount += booking.BookingAmount * dateCount;
    });

    this._memberContract.BookingPrice += bookingAmount;
    const existingArticle = this._memberContract.EveryMonthItemList.filter(X => X.ArticleId === -1)[0];
    if (existingArticle !== undefined) {
      const bokingArticleIndex = this._memberContract.EveryMonthItemList.indexOf(existingArticle);
      this._memberContract.EveryMonthItemList.splice(bokingArticleIndex, 1);
    }

    if (bookingAmount > 0) {
      let bookingName: string;
      this.translateService.get('MEMBERSHIP.BookingFee').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => bookingName = translatedValue);
      const amountPerInstallment: number = Number(Math.round(bookingAmount / this._memberContract.NoOfInstallments).toFixed(2));
      const bookingItem: any = {};
      bookingItem.ArticleId = -1;
      bookingItem.IsBookingArticle = true;
      bookingItem.Discount = 0;
      bookingItem.IsActivityArticle = false;
      bookingItem.IsSelected = true;
      bookingItem.IsStartUpItem = false;
      bookingItem.ItemName = bookingName;
      bookingItem.NumberOfOrders = this._memberContract.NoOfInstallments;
      bookingItem.Price = amountPerInstallment;
      bookingItem.Quantity = 1;
      bookingItem.UnitPrice = amountPerInstallment;
      bookingItem.StartOrder = 1;
      bookingItem.EndOrder = this._memberContract.NoOfInstallments;
      this._memberContract.EveryMonthItemList.push(bookingItem);
    }
  }

  CountDays(startDate: any, endDate: any, bookingDay: string): number {
    let count = 0;
    while (moment(startDate).toDate() <= moment(endDate).toDate()) {
      if (moment(startDate).format('dddd') === bookingDay) {
        count++;
      }
      startDate = this.dateAdd(startDate, 1, 'days');
    }
    return count;
  }

  onSelectionChange(item) {
    this._selectedResource = item;
  }

  onBookingLeave(event: any) {
    this.isSaveVisible = false;
    if (!this.economyForm.valid) {
      this.economyValid = false;
      this.economyValidationEnabled = true;
      UsErrorService.validateAllFormFields(this.economyForm, this.formErrors, this.validationMessages);
    } else {
      this.economyValid = true;
    }
    this.manipulateBookingPrice()
    this.updateEconomy();
  }

  // remove Monthly items
  removeMonthlyItem(item: any) {
    this._deletedItem = item;
    const removeditemIndex = this._memberContract.EveryMonthItemList.indexOf(this._deletedItem);
    if (removeditemIndex > -1) {
      if (this._deletedItem.ActivityID === this._memberContract.ActivityId) {
        if (this._memberContract.EveryMonthItemList.filter(x => x.ActivityID === this._memberContract.ActivityId).length === 1) {
          this.messageService.openMessageBox('WARNING',
            {
              messageTitle: 'Exceline',
              messageBody: 'MEMBERSHIP.MsgActivtyCntDelete'
            }
          )
        } else {
          this.messageService.openMessageBox('CONFIRM',
            {
              messageTitle: 'Exceline',
              messageBody: 'MEMBERSHIP.ConfirmDelete',
              msgBoxId: 'MONTHLY'
            });
        }
      } else {
        this.messageService.openMessageBox('CONFIRM',
          {
            messageTitle: 'Exceline',
            messageBody: 'MEMBERSHIP.ConfirmDelete',
            msgBoxId: 'MONTHLY'
          });
      }
    }
  }

  deleteMonthlyItem() {
    const deleteingIndex = this._memberContract.EveryMonthItemList.indexOf(this._deletedItem);
    this._memberContract.EveryMonthItemList.splice(deleteingIndex, 1);
    this.updateEconomy();
  }

  // remove startup items
  removeStartUpItem(item: any) {
    this._deletedItem = item;
    const removeditemIndex = this._memberContract.StartUpItemList.indexOf(this._deletedItem);
    if (removeditemIndex > -1) {

      this.messageService.openMessageBox('CONFIRM',
        {
          messageTitle: 'Exceline',
          messageBody: 'MEMBERSHIP.ConfirmDelete',
          msgBoxId: 'ITEM'
        });
    }
  }

  deleteStartUpItem() {
    const deleteingIndex = this._memberContract.StartUpItemList.indexOf(this._deletedItem);
    this._memberContract.StartUpItemList.splice(deleteingIndex, 1);
    this.updateEconomy();
  }

  updateEconomy() {
    this._memberContract.InstalllmentList = [];
    this.GenerateOrders();
  }

  onEconomyLeave(event: any) {
    this.isSaveVisible = false;
    if (!this.economyForm.valid) {

    }
  }

  setDueDate(): void {
    if (this._memberContract.FirstDueDate) {
      this._contractDueDate = this._memberContract.FirstDueDate;
    } else if (this._memberContract.CreditoDueDays > 0) {
      this._contractDueDate = moment(this._memberContract.ContractStartDate).add(this._memberContract.CreditoDueDays, 'days').toDate();
    } else if (this._memberContract.UseTodayAsDueDate) {
      this._contractDueDate = moment().toDate();
    } else if (this._memberContract.DueDateSetting > 0) {
      const startDate: Date = moment(this._memberContract.ContractStartDate).add(1, 'months').toDate();
      this._contractDueDate = new Date(startDate.getFullYear(), startDate.getMonth(), this._memberContract.DueDateSetting);
    } else {
      this._contractDueDate = this._memberContract.ContractStartDate;
    }
  }

  GenerateOrders() {
    let ContractServicePrice = 0;
    let ContractItemPrice = 0;
    let trainingStartDate: Date = this._memberContract.ContractStartDate;
    let restplusAmount = 0;
    let trainingEndDate: Date = moment(this._memberContract.ContractStartDate).add(1, 'months').add(-1, 'days').toDate();
    this._installmentList = [];
    const installment1: any = {};
    installment1.AddOnList = [];
    if (this._memberContract !== undefined) {
      if (this._memberContract.ContractTypeCode !== 'PUNCHCARD') {
        let installmentAmount: number = this._memberContract.OrderPrice / this._memberContract.NoOfInstallments;
        installment1.IsATG = this._memberContract.IsATG;
        installment1.OrderOperationsEnabled = false;
        installment1.InstallmentNo = 1;
        installment1.InstallmentId = 1;
        installment1.CreatedUser = this.loginService.CurrentUser.UserName;
        installment1.OriginalCustomer = this._selectedMember.CustId + ' - ' + this._selectedMember.Name;
        installment1.BranchName = this.loginService.SelectedBranch.BranchName;
        installment1.MemberId = this._selectedMember.Id;
        installment1.MemberName = this._selectedMember.Name;
        installment1.MemberContractId = this._memberContract.Id;
        installment1.MemberContractNo = this._memberContract.MemberContractNo;
        installment1.InstallmentDate = this._memberContract.ContractStartDate;
        installment1.TemplateId = this._memberContract.ContractId;
        installment1.AdonPrice = 0;
        installment1.Amount = 0;
        installment1.ItemAmount = 0;
        installment1.ActivityPrice = 0;
        if (this._contractDueDate != null) {
          installment1.OriginalDueDate = this._contractDueDate;
          installment1.DueDate = moment(this._contractDueDate).format('YYYY-MM-DD').toString();
        } else {
          installment1.OriginalDueDate = this._memberContract.ContractStartDate;
          installment1.DueDate = this._memberContract.ContractStartDate;
        }
        if (moment(installment1.DueDate).isBefore(new Date(), 'day')) {
          installment1.DueDate = moment(installment1.DueDate).add(1, 'months').format('YYYY-MM-DD').toString();
        }
        installment1.BillingDate = moment(new Date()).format('YYYY-MM-DD').toString();
        installment1.TrainingPeriodStart = trainingStartDate;
        installment1.TrainingPeriodEnd = trainingEndDate;

        if (installment1.DueDate.Date === moment(new Date()).toDate) {
          installment1.EstimatedInvoiceDate = moment(new Date()).format('YYYY-MM-DD').toString();
        } else {
          if (this._selectedMember.CreditPeriod > 0) {
            installment1.EstimatedInvoiceDate = moment(installment1.DueDate).add(this._selectedMember.CreditPeriod * -1, 'days').format('YYYY-MM-DD').toString();
          } else if (this._memberContract.CreditPeriod > 0) {
            installment1.EstimatedInvoiceDate = moment(installment1.DueDate).add(this._memberContract.CreditPeriod * -1, 'days').format('YYYY-MM-DD').toString();
          }
        }

        if (moment(installment1.EstimatedInvoiceDate).toDate() <= moment(new Date).toDate() && moment(installment1.DueDate).toDate() > moment(new Date()).toDate()) {
          installment1.EstimatedInvoiceDate = moment(installment1.EstimatedInvoiceDate).add(1, 'days').format('YYYY-MM-DD').toString();
        }
        if (this._selectedMember.GuardianId !== -1) {
          installment1.PayerId = this._selectedMember.GuardianId;
          installment1.PayerName = this._selectedMember.GuardianFirstName;
        }

        // handle the rest plus month value
        if (this._memberContract.RestPlusMonth) {
          const noofDaysForMonth: number = moment(moment(this._memberContract.ContractStartDate).year + '-' + moment(this._memberContract.ContractStartDate).month, 'YYYY-MM').daysInMonth();
          const restDays = (noofDaysForMonth - moment(this._memberContract.ContractStartDate).date()) + 1;
          const activityArticles: any = this._memberContract.EveryMonthItemList.filter(x => x.ActivityID === this._memberContract.ActivityId)[0];
          if (activityArticles !== undefined) {
            if (moment(this._memberContract.ContractStartDate).date() === 1) {
              restplusAmount = activityArticles.Price;
            } else {
              const amountPerDay: number = Number(Math.round(activityArticles.Price / 30).toFixed(2));
              restplusAmount = restDays * amountPerDay;
            }
          } else {
            restplusAmount = 0;
          }
          this._memberContract.ContractPrize = this._originalContractAmount + restplusAmount;
          this._memberContract.EveryMonthItemPrice = this._originalMonthlyItemPrice + restplusAmount;
          this._memberContract.RestPlusMonthAmount = restplusAmount;
          trainingEndDate = moment(this._memberContract.ContractStartDate).add(restDays - 1, 'days').toDate();
          const nextMonth = this.dateAdd(trainingEndDate, 1, 'days');
          const noofDaysForNextMonth: number = this.getNumberOfDaysInMonth(moment(nextMonth).year(), moment(nextMonth).month() + 1);
          trainingEndDate = moment(trainingEndDate).add(noofDaysForNextMonth, 'days').toDate();
          installment1.TrainingPeriodEnd = trainingEndDate;
          if (this._memberContract.NoOfMonths > 0) {
            this._memberContract.ContractEndDate = moment(trainingEndDate).add(1, 'days').add(this._memberContract.NoOfMonths - 1, 'months').add(-1, 'days').toDate();
          } else if (this._memberContract.NoofDays > 0) {
            this._memberContract.ContractEndDate = moment(trainingEndDate).add(this._memberContract.NoofDays, 'days').toDate();
          }
        } // end of rest plus month

        installment1.Text = moment(installment1.TrainingPeriodStart).format(this._dateFormat) + ' - ' + moment(installment1.TrainingPeriodEnd).format(this._dateFormat);
        let Priority = 1;
        if (this._memberContract.StartUpItemList !== undefined) {
          (this._memberContract.StartUpItemList).forEach(element => {
            ContractItemPrice += element.Price;
            if (!element.IsMemberFee) {
              element.Priority = Priority;
              installment1.AddOnList.push(element);
              installment1.AdonPrice += element.Price;
              installment1.Amount += element.Price;
              installment1.ItemAmount += element.Price;
              Priority++;
            }
          });
        }
        if (this._memberContract.EveryMonthItemList !== undefined) {
          this._memberContract.EveryMonthItemList.forEach(monththlyItem => {
            const monthItem = deepcopy<any>(monththlyItem);
            monthItem.Priority = Priority;

            if (monthItem.ActivityID === this._memberContract.ActivityId) {
              monthItem.IsActivityArticle = true;
              // monthItem.Price += restplusAmount;
            }

            if (monthItem.EndOrder > 0) {
              if (installment1.InstallmentNo >= monthItem.StartOrder && installment1.InstallmentNo <= monthItem.EndOrder) {
                if (!monthItem.IsActivityArticle) {
                  installment1.AdonPrice += monthItem.Price;
                  installment1.ServiceAmount += monthItem.Price;
                  ContractServicePrice += monthItem.Price;
                  installment1.Amount += monthItem.Price;
                } else {
                  monthItem.Price = monthItem.Price + restplusAmount;
                  installment1.ActivityPrice += monthItem.Price;
                  installment1.ServiceAmount += monthItem.Price;
                  ContractServicePrice += monthItem.Price;
                  installment1.Amount += monthItem.Price;
                }

                installment1.AddOnList.push(monthItem);
                Priority++;
              }
            } else if (installment1.InstallmentNo >= monthItem.StartOrder) {
              if (!monthItem.IsActivityArticle) {
                installment1.AdonPrice += monthItem.Price;
                installment1.ServiceAmount += monthItem.Price;
                installment1.Amount += monthItem.Price;
                ContractServicePrice += monthItem.Price;
              } else {
                monthItem.Price = monthItem.Price + restplusAmount;
                installment1.ActivityPrice += monthItem.Price;
                installment1.ServiceAmount += monthItem.Price;
                ContractServicePrice += monthItem.Price;
                installment1.Amount += monthItem.Price;
              }
              installment1.AddOnList.push(monthItem);
              Priority++;
            }
          });

          installment1.Balance = installment1.Amount;
          installment1.EstimatedOrderAmount = installment1.Amount;
          installment1.PriceListItem = this._memberContract.ContractName;
          this._installmentList.push(installment1);
        }

        if (this._memberContract.RestPlusMonth) {
          let dueDate: Date = moment(installment1.DueDate).add(1, 'months').toDate();
          let installmentRegisterDate = this._memberContract.ContractStartDate;
          const monofDaysForMonth: number = this.getNumberOfDaysInMonth(moment(this._memberContract.ContractStartDate).year(), moment(this._memberContract.ContractStartDate).month() + 1);
          const restDays: number = monofDaysForMonth - moment(this._memberContract.ContractStartDate).date();

          // preapre the installment date
          if (new Date().getDate() > 1) {
            installmentRegisterDate = moment(this._memberContract.ContractStartDate).add(restDays, 'days');
            const nextMonth: Date = moment(installmentRegisterDate).add(1, 'days').toDate();
            const noofDaysForNextMonth: number = this.getNumberOfDaysInMonth(nextMonth.getFullYear(), nextMonth.getMonth() + 1);
          } else {
            installmentRegisterDate = moment(this._memberContract.ContractStartDate).add(1, 'months');
          }
          let i: number;
          for (i = 2; i < Number(this.economyForm.get('numberOfOrders').value) + 1; i++) {
            if (this._hasFixedDate) {
              if (this._memberContract.ContractEndDate < dueDate) {
                continue;
              }
            }

            if (this._memberContract.SecondDueDate != null && i === 2) {
              if (this._memberContract.SecondDueDate > new Date() && installment1.DueDate < this._memberContract.SecondDueDate) {
                dueDate = this._memberContract.SecondDueDate;
              } else if (this._memberContract.PostPay) {
                dueDate = moment(this.dateAdd(installment1.DueDate, 2, 'months').getFullYear().toString() + '-'
                + (this.dateAdd(installment1.DueDate, 2, 'months').getMonth() + 1).toString() + '-' + this._memberContract.DueDateSetting).toDate();
              } else {
                dueDate = moment(this.dateAdd(installment1.DueDate, 1, 'months').getFullYear().toString() + '-'
                + (this.dateAdd(installment1.DueDate, 1, 'months').getMonth() + 1).toString() + '-' + this._memberContract.DueDateSetting).toDate();
              }
            } else if (i === 2) {
              if (this._memberContract.PostPay) {
                dueDate = moment(this.dateAdd(installment1.DueDate, 2, 'months').getFullYear().toString() + '-'
                + (this.dateAdd(installment1.DueDate, 2, 'months').getMonth() + 1).toString() + '-' + this._memberContract.DueDateSetting).toDate();
              } else {
                dueDate = moment(this.dateAdd(installment1.DueDate, 1, 'months').getFullYear().toString() + '-'
                + (this.dateAdd(installment1.DueDate, 1, 'months').getMonth() + 1).toString() + '-' + this._memberContract.DueDateSetting).toDate();
              }
            }

            if (moment(dueDate).isBefore(new Date(), 'day')) {
              dueDate = moment(dueDate).add(1, 'months').toDate();
            }

            trainingStartDate = this.dateAdd(trainingEndDate, 1, 'days');
            const tempNDate: Date = this.dateAdd(trainingEndDate, 1, 'days');
            const noofDaysInNextMonth: number = this.getNumberOfDaysInMonth(tempNDate.getFullYear(), tempNDate.getMonth() + 1);
            trainingEndDate = this.dateAdd(trainingEndDate, noofDaysInNextMonth, 'days');

            const installment: any = {};
            installment.AddOnList = [];
            installment.OrderOperationsEnabled = false;
            installment.IsATG = this._memberContract.IsATG;
            installment.DueDate = moment(dueDate).format('YYYY-MM-DD').toString();
            installment.OriginalDueDate = moment(dueDate).format('YYYY-MM-DD').toString();
            installment.BranchName = this.loginService.SelectedBranch.BranchName;
            installment.CreatedUser = this._user;
            installment.BillingDate = installmentRegisterDate;
            installment.InstallmentDate = installmentRegisterDate;
            installment.OriginalCustomer = this._selectedMember.CustId + ' - ' + this._selectedMember.Name;
            installment.TransferDate = this.dateAdd(dueDate, -6, 'days');
            installment.MemberId = this._selectedMember.Id;
            installment.MemberName = this._selectedMember.Name;
            installment.MemberContractId = this._memberContract.Id;
            installment.MemberContractNo = this._memberContract.MemberContractNo;
            installment.InstallmentId = i;
            installment.InstallmentNo = i;
            installment.TrainingPeriodStart = trainingStartDate;
            installment.TrainingPeriodEnd = trainingEndDate;
            installment.TemplateId = this._memberContract.ContractId;
            installment.AdonPrice = 0;
            installment.Amount = 0;
            installment.ItemAmount = 0;
            installment.ActivityPrice = 0;
            installment.Text = moment(installment.TrainingPeriodStart).format(this._dateFormat) + ' - ' + moment(installment.TrainingPeriodEnd).format(this._dateFormat);
            if (this._selectedMember.GuardianId !== -1) {
              installment.PayerId = this._selectedMember.GuardianId;
              installment.PayerName = this._selectedMember.GuardianFirstName;
            }

            // adding monthly items
            if (this._memberContract.EveryMonthItemList != null) {
              const priotityIndex = 1;
              this._memberContract.EveryMonthItemList.forEach(monthItem => {
                monthItem.Priority = priotityIndex;
                if (monthItem.ActivityID === this._memberContract.ActivityId) {
                  monthItem.IsActivityArticle = true;
                }
                if (monthItem.EndOrder > 0) {
                  if (installment.InstallmentNo >= monthItem.StartOrder && installment.InstallmentNo <= monthItem.EndOrder) {
                    installment.Amount += monthItem.Price;
                    if (!monthItem.IsActivityArticle) {
                      installment.AdonPrice += monthItem.Price;
                    } else {
                      installment.ActivityPrice += monthItem.Price;
                    }
                    installment.ServiceAmount += monthItem.Price;
                    ContractServicePrice += monthItem.Price;
                    installment.AddOnList.push(monthItem);
                    Priority++;
                  }
                  // no end order found
                } else if (installment.InstallmentNo >= monthItem.StartOrder) {
                  installment.Amount += monthItem.Price;
                  if (!monthItem.IsActivityArticle) {
                    installment.AdonPrice += monthItem.Price;
                  }else {
                    installment.ActivityPrice += monthItem.Price;
                  }
                  installment.ServiceAmount += monthItem.Price;
                  ContractServicePrice += monthItem.Price;
                  installment.AddOnList.push(monthItem);
                  Priority++;
                }
              });
            }

            if (this._memberContract.EveryMonthItemList.length === 0) {
              installment.Amount = installmentAmount;
            }

            installment.EstimatedOrderAmount = installment.Amount;
            if (this._selectedMember.CreditPeriod > 0) {
              installment.EstimatedInvoiceDate = moment(this.dateAdd(installment.DueDate, this._selectedMember.CreditPeriod * -1, 'days')).format('YYYY-MM-DD').toString();
            } else if (this._memberContract.CreditPeriod > 0) {
              installment.EstimatedInvoiceDate = moment(this.dateAdd(installment.DueDate, this._memberContract.CreditPeriod * -1, 'days')).format('YYYY-MM-DD').toString();
            }
            if (this._memberContract.DueDateSetting > 0) {
              dueDate = this.dateAdd(dueDate, 1, 'months');
              dueDate = new Date(dueDate.getFullYear(), dueDate.getMonth(), this._memberContract.DueDateSetting);
            } else {
              dueDate = this.dateAdd(dueDate, 1, 'months');
            }

            const noOfDays: number = this.getNumberOfDaysInMonth(moment(installmentRegisterDate).year(), moment(installmentRegisterDate).month() + 1);
            installmentRegisterDate = this.dateAdd(moment(installmentRegisterDate).toDate(), noOfDays, 'days');
            this._installmentList.push(installment);

          }// end of for
        } else {
          let dueDate: Date = moment(installment1.DueDate).add(1, 'months').toDate();
          if (this._memberContract.EveryMonthItemList.length === 0) {
            installmentAmount = this._memberContract.OrderPrice / this._memberContract.NoOfInstallments;
          }

          let installmentRegisterDate: Date = moment(this._memberContract.ContractStartDate).add(1, 'months').toDate();
          // bruker antall ordre for framfor antall måneder
          for (let i = 2; i < Number(this.economyForm.get('numberOfOrders').value) + 1; i++) {
            if (this._hasFixedDate) {
              if (this._memberContract.ContractEndDate < dueDate) {
                continue;
              }
            }

            if (this._memberContract.SecondDueDate !== undefined && (i === 2)) {
              if (this._memberContract.SecondDueDate > new Date()) {
                dueDate = this._memberContract.SecondDueDate;
              } else {
                const nextDueMonth: Date = this.dateAdd(installment1.DueDate, 1, 'months');
                dueDate = new Date(nextDueMonth.getFullYear(), nextDueMonth.getMonth(), this._memberContract.DueDateSetting);
              }
            } else if (i === 2) {
              const nextDueMonth: Date = this.dateAdd(installment1.DueDate, 1, 'months');
              dueDate = new Date(nextDueMonth.getFullYear(), nextDueMonth.getMonth(), this._memberContract.DueDateSetting);
            }

            if (moment(dueDate).isBefore(new Date(), 'day')) {
              dueDate = this.dateAdd(dueDate, 1, 'months');
            }

            trainingStartDate = this.dateAdd(trainingEndDate, 1, 'days');
            trainingEndDate = this.dateAdd(this.dateAdd(trainingStartDate, 1, 'months'), -1, 'days');
            const installment: any = {};
            installment.OrderOperationsEnabled = false;
            installment.AddOnList = [];
            installment.IsATG = this._memberContract.IsATG;
            installment.DueDate = moment(dueDate).format('YYYY-MM-DD').toString();
            installment.OriginalDueDate = moment(dueDate).format('YYYY-MM-DD').toString();
            installment.BranchName = this.loginService.SelectedBranch.BranchName;
            installment.CreatedUser = this._user;
            installment.BillingDate = installmentRegisterDate;
            installment.InstallmentDate = installmentRegisterDate;
            installment.OriginalCustomer = this._selectedMember.CustId + ' - ' + this._selectedMember.Name;
            installment.TransferDate = this.dateAdd(dueDate, -6, 'days');
            installment.MemberId = this._selectedMember.Id;
            installment.MemberName = this._selectedMember.Name;
            installment.MemberContractId = this._memberContract.Id;
            installment.MemberContractNo = this._memberContract.MemberContractNo;
            installment.InstallmentId = i;
            installment.InstallmentNo = i;
            installment.TrainingPeriodStart = trainingStartDate;
            installment.TrainingPeriodEnd = trainingEndDate;
            installment.TemplateId = this._memberContract.ContractId;
            installment.AdonPrice = 0;
            installment.Amount = 0;
            installment.ItemAmount = 0;
            installment.ActivityPrice = 0;
            installment.Text = moment(installment.TrainingPeriodStart).format(this._dateFormat) + ' - ' + moment(installment.TrainingPeriodEnd).format(this._dateFormat);
            if (this._selectedMember.GuardianId !== -1) {
              installment.PayerId = this._selectedMember.GuardianId;
              installment.PayerName = this._selectedMember.GuardianFirstName;
            }

            if (this._memberContract.EveryMonthItemList != null) {
              const priotityIndex = 1;
              this._memberContract.EveryMonthItemList.forEach(monthItem => {
                monthItem.Priority = priotityIndex;
                if (monthItem.ActivityID === this._memberContract.ActivityId) {
                  monthItem.IsActivityArticle = true;
                }
                if (monthItem.EndOrder > 0) {
                  if (installment.InstallmentNo >= monthItem.StartOrder && installment.InstallmentNo <= monthItem.EndOrder) {
                    installment.Amount += monthItem.Price;
                    if (!monthItem.IsActivityArticle) {
                      installment.AdonPrice += monthItem.Price;
                    } else {
                      installment.ActivityPrice += monthItem.Price;
                    }

                    installment.ServiceAmount += monthItem.Price;
                    ContractServicePrice += monthItem.Price;
                    installment.AddOnList.push(monthItem);
                    Priority++;
                  }
                  // no end order found
                } else if (installment.InstallmentNo >= monthItem.StartOrder) {
                  installment.Amount += monthItem.Price;
                  if (!monthItem.IsActivityArticle) {
                    installment.AdonPrice += monthItem.Price;
                  } else {
                    installment.ActivityPrice += monthItem.Price;
                  }
                  installment.ServiceAmount += monthItem.Price;
                  ContractServicePrice += monthItem.Price;
                  installment.AddOnList.push(monthItem);
                  Priority++;
                }
              });
            }

            if (this._memberContract.EveryMonthItemList.Count === 0) {
              installment.Amount = installmentAmount;
            installment.Balance = installment.Amount;
            installment.EstimatedOrderAmount = installment.Amount;
            }
            if (this._selectedMember.CreditPeriod > 0) {
              installment.EstimatedInvoiceDate = moment(this.dateAdd(installment.DueDate, this._selectedMember.CreditPeriod * -1, 'days')).format('YYYY-MM-DD').toString();
            } else if (this._memberContract.CreditPeriod > 0) {
              installment.EstimatedInvoiceDate = moment(this.dateAdd(installment.DueDate, this._memberContract.CreditPeriod * -1, 'days')).format('YYYY-MM-DD').toString();
            }
            if (this._memberContract.DueDateSetting > 0) {
              dueDate = this.dateAdd(dueDate, 1, 'months');
              dueDate = new Date(dueDate.getFullYear(), dueDate.getMonth(), this._memberContract.DueDateSetting);
            } else {
              dueDate = this.dateAdd(dueDate, 1, 'months');
            }

            installmentRegisterDate = this.dateAdd(installmentRegisterDate, 1, 'months');
            this._installmentList.push(installment);
          }
        }

      } else { // handling punch card contracts
        let dueDate: Date = this._memberContract.ContractStartDate;
        let tSdate: Date = this._memberContract.ContractStartDate;
        let tEDate: Date = this.dateAdd(this.dateAdd(this._memberContract.ContractStartDate, 1, 'months'), -1, 'days');
        let installmentRegisterDate: Date = moment(this._memberContract.ContractStartDate).toDate();

        for (let i = 1; i < Number(this._memberContract.NoOfInstallments) + 1; i++) {
          if (this._hasFixedDate) {
            if (this._memberContract.ContractEndDate < dueDate) {
              continue;
            }
          }

          const installment: any = {};
          installment.OrderOperationsEnabled = false;
          installment.AddOnList = [];
          installment.IsATG = this._memberContract.IsATG;
          installment.DueDate = moment(dueDate).format('YYYY-MM-DD').toString();
          installment.OriginalDueDate = moment(dueDate).format('YYYY-MM-DD').toString();
          installment.BranchName = this.loginService.SelectedBranch.BranchName;
          installment.CreatedUser = this._user;
          installment.OriginalCustomer = this._selectedMember.CustId + ' - ' + this._selectedMember.Name;
          installment.TransferDate = this.dateAdd(dueDate, -6, 'days');
          installment.MemberId = this._selectedMember.Id;
          installment.MemberName = this._selectedMember.Name;
          installment.MemberContractId = this._memberContract.Id;
          installment.MemberContractNo = this._memberContract.MemberContractNo;
          installment.InstallmentId = i;
          installment.InstallmentNo = i;
          installment.TrainingPeriodStart = moment(tSdate).format('YYYY-MM-DD').toString();
          installment.TrainingPeriodEnd = moment(tEDate).format('YYYY-MM-DD').toString();
          installment.TemplateId = this._memberContract.ContractId;
          installment.AdonPrice = 0;
          installment.Amount = 0;
          installment.ItemAmount = 0;
          installment.ActivityPrice = 0;
          installment.InstallmentDate = moment(installmentRegisterDate).format('YYYY-MM-DD').toString();
          if (this._selectedMember.GuardianId !== -1) {
            installment.PayerId = this._selectedMember.GuardianId;
            installment.PayerName = this._selectedMember.GuardianFirstName;
          }
          // setting the training period
          if (i === this._memberContract.NoOfInstallments) {
            installment.TrainingPeriodStart = tSdate;
            installment.TrainingPeriodEnd = this._memberContract.ContractEndDate;
          } else {
            installment.TrainingPeriodStart = tSdate;
            installment.TrainingPeriodEnd = tEDate;
          }

          let Priority = 1;
          if (i === 1) {
            if (this._contractDueDate != null) {
              dueDate = this._contractDueDate;
            } else {
              dueDate = this._memberContract.ContractStartDate;
            }

            // adding startup items
            if (this._memberContract.StartUpItemList !== undefined) {
              (this._memberContract.StartUpItemList).forEach(element => {
                ContractItemPrice += element.Price;
                if (!element.IsMemberFee) {
                  element.Priority = Priority;
                  installment1.AddOnList.push(element);
                  installment1.AdonPrice += element.Price;
                  installment1.Amount += element.Price;
                  installment1.ItemAmount += element.Price;
                  Priority++;
                }

              });
            }
          }

          // adding monthly items
          if (this._memberContract.EveryMonthItemList != null) {
            const priotityIndex = 1;
            this._memberContract.EveryMonthItemList.forEach(monthItem => {
              monthItem.Priority = priotityIndex;
              if (monthItem.ActivityID === this._memberContract.ActivityId) {
                monthItem.IsActivityArticle = true;
              }
              if (monthItem.EndOrder > 0) {
                if (installment.InstallmentNo >= monthItem.StartOrder && installment.InstallmentNo <= monthItem.EndOrder) {
                  installment.Amount += monthItem.Price;
                  if (!monthItem.IsActivityArticle) {
                    installment.AdonPrice += monthItem.Price;
                  } else {
                    installment.ActivityPrice += monthItem.Price;
                  }

                  installment.ServiceAmount += monthItem.Price;
                  ContractServicePrice += monthItem.Price;
                  installment.AddOnList.push(monthItem);
                  Priority++;
                }
                // no end order found
              } else if (installment.InstallmentNo >= monthItem.StartOrder) {
                installment.Amount += monthItem.Price;
                if (!monthItem.IsActivityArticle) {
                  installment.AdonPrice += monthItem.Price;
                } else {
                  installment.ActivityPrice += monthItem.Price;
                }
                installment.ServiceAmount += monthItem.Price;
                ContractServicePrice += monthItem.Price;
                installment.AddOnList.push(monthItem);
                Priority++;
              }
            });
          }

          if (this._memberContract.SecondDueDate !== undefined && (i === 2)) {
            if (this._memberContract.SecondDueDate > new Date()) {
              dueDate = this._memberContract.SecondDueDate;
            } else {
              dueDate = new Date(dueDate.getFullYear(), dueDate.getMonth(), this._memberContract.DueDateSetting);
            }
          } else if (i === 2) {
            dueDate = new Date(dueDate.getFullYear(), dueDate.getMonth(), this._memberContract.DueDateSetting);
          }

          installment.DueDate = dueDate;
          installment.OriginalDueDate = dueDate;

          installment.BillingDate = new Date();
          installment.PriceListItem = this._memberContract.ContractName;
          installment.Balance = installment.Amount;
          installment.EstimatedOrderAmount = installment.Amount;

          if (this._selectedMember.CreditPeriod > 0) {
            installment.EstimatedInvoiceDate = moment(this.dateAdd(installment.DueDate, this._selectedMember.CreditPeriod * -1, 'days')).format('YYYY-MM-DD').toString();
          } else if (this._memberContract.CreditPeriod > 0) {
            installment.EstimatedInvoiceDate = moment(this.dateAdd(installment.DueDate, this._memberContract.CreditPeriod * -1, 'days')).format('YYYY-MM-DD').toString();
          }
          installment.Text = moment(installment.TrainingPeriodStart).format(this._dateFormat) + ' - ' + moment(installment.TrainingPeriodEnd).format(this._dateFormat);
          this._installmentList.push(installment);

          if (this._memberContract.DueDateSetting > 0) {
            dueDate = this.dateAdd(dueDate, 1, 'months');
            dueDate = new Date(dueDate.getFullYear(), dueDate.getMonth(), this._memberContract.DueDateSetting);
          } else {
            dueDate = this.dateAdd(dueDate, 1, 'months');
          }

          tSdate = this.dateAdd(tSdate, 1, 'days');
          tEDate = this.dateAdd(this.dateAdd(tSdate, 1, 'months'), -1, 'days');
          installmentRegisterDate = moment(installmentRegisterDate).add(1, 'months').toDate();
        }
      }// end of puch card handling
      this._memberContract.EveryMonthItemPrice = ContractServicePrice;
      this._memberContract.StartUpItemPrice = ContractItemPrice;
      this._memberContract.ContractPrize = ContractServicePrice + ContractItemPrice;
      this._memberContract.InstalllmentList = this._installmentList;
      // Set Training dates
      this.SetTrainingDates();
    }
  }

  SetTrainingDates() {
    const generatedOrderscount = this._memberContract.InstalllmentList.length;
    let traininfStartDate: Date = this._memberContract.ContractStartDate;
    let trainingEndDate: Date = this._memberContract.ContractEndDate;

    if (generatedOrderscount < this._memberContract.NoOfMonths && generatedOrderscount > 0) {
      const noofSections: number = this._memberContract.NoOfMonths / generatedOrderscount;
      const lastInstallment: any = this._memberContract.InstalllmentList[this._memberContract.InstalllmentList.length - 1];

      this._memberContract.InstalllmentList.forEach(installment => {
        installment.TrainingPeriodStart = moment(traininfStartDate).format('YYYY-MM-DD').toString();
        if (lastInstallment === installment) {
          trainingEndDate = this._memberContract.ContractEndDate;
          installment.TrainingPeriodEnd = moment(trainingEndDate).format('YYYY-MM-DD').toString();
          if (moment(installment.TrainingPeriodEnd).isBefore(moment(installment.TrainingPeriodStart))) {
            installment.TrainingPeriodEnd = moment(installment.TrainingPeriodStart).format('YYYY-MM-DD').toString();
          }
          installment.Text = moment(installment.TrainingPeriodStart).format(this._dateFormat) + ' - ' + moment(installment.TrainingPeriodEnd).format(this._dateFormat);
        } else {
          trainingEndDate = this.dateAdd(traininfStartDate, 1, 'months');
          trainingEndDate = this.dateAdd(trainingEndDate, -1, 'days');
          installment.TrainingPeriodEnd = moment(trainingEndDate).format('YYYY-MM-DD').toString();
          if (moment(installment.TrainingPeriodEnd).isBefore(moment(installment.TrainingPeriodStart))) {
            installment.TrainingPeriodEnd = moment(installment.TrainingPeriodStart).format('YYYY-MM-DD').toString();
          }
          installment.Text = moment(installment.TrainingPeriodStart).format(this._dateFormat) + ' - ' + moment(installment.TrainingPeriodEnd).format(this._dateFormat);
          traininfStartDate = this.dateAdd(trainingEndDate, 1, 'days');
        }
      });
    } else {
      const lastInstallment: any = this._memberContract.InstalllmentList[this._memberContract.InstalllmentList.length - 1];
      this._memberContract.InstalllmentList.forEach(installment => {
        if (lastInstallment === installment) {
          trainingEndDate = this._memberContract.ContractEndDate;
          installment.TrainingPeriodEnd = moment(trainingEndDate).format('YYYY-MM-DD').toString();
          if (moment(installment.TrainingPeriodEnd).isBefore(moment(installment.TrainingPeriodStart))) {
            installment.TrainingPeriodEnd = moment(installment.TrainingPeriodStart).format('YYYY-MM-DD').toString();
          }
          installment.Text = moment(installment.TrainingPeriodStart).format(this._dateFormat) + ' - ' + moment(installment.TrainingPeriodEnd).format(this._dateFormat);
        }
      });
    }
  }

  getNumberOfDaysInMonth(year: number, month: number): number {
    return moment(year + '-' + month, 'YYYY-MM').daysInMonth();
  }


  onStepChanged(event: any) {
    if (event.number === 3) {
      this.isSaveVisible = true;
    } else {
      this.isSaveVisible = false;
    }
  }

  openDetailView(content, selectedOrder) {
    this.selectedOrder = selectedOrder;
    const dueDate = new Date(selectedOrder.DueDate);
    this.selectedOrder = selectedOrder;
    this.selectedOrder.DueDateFormated = { year: dueDate.getFullYear(), month: dueDate.getMonth() + 1, day: dueDate.getDate() };


    if (!selectedOrder.isOpened || selectedOrder.isFormatedDates) {
      const traningStart = new Date(selectedOrder.TrainingPeriodStart);
      this.selectedOrder.TrainingPeriodStart = { year: traningStart.getFullYear(), month: traningStart.getMonth() + 1, day: traningStart.getDate() };
      const traningEnd = new Date(selectedOrder.TrainingPeriodEnd);
      this.selectedOrder.TrainingPeriodEnd = { year: traningEnd.getFullYear(), month: traningEnd.getMonth() + 1, day: traningEnd.getDate() };
      const installmentDate = new Date(selectedOrder.InstallmentDate);
      this.selectedOrder.InstallmentDate = { year: installmentDate.getFullYear(), month: installmentDate.getMonth() + 1, day: installmentDate.getDate() };
      const originalDueDate = new Date(selectedOrder.OriginalDueDate);
      this.selectedOrder.OriginalDueDate = { year: originalDueDate.getFullYear(), month: originalDueDate.getMonth() + 1, day: originalDueDate.getDate() };
      const estimatedInvoiceDate = new Date(selectedOrder.EstimatedInvoiceDate);
      this.selectedOrder.EstimatedInvoiceDate = { year: estimatedInvoiceDate.getFullYear(), month: estimatedInvoiceDate.getMonth() + 1, day: estimatedInvoiceDate.getDate() };

    }

    selectedOrder.isOpened = true;
    this.modalReference = this.modalService.open(content);
  }

  addOrder() {
    const lastInstallment: any = this._memberContract.InstalllmentList[this._memberContract.InstalllmentList.length - 1];
    if (lastInstallment) {
      const newInstallment: any = {};
      newInstallment.MemberId = lastInstallment.MemberId;
      newInstallment.MemberContractId = lastInstallment.MemberContractId;
      newInstallment.InstallmentDate = this.dateAdd(lastInstallment.InstallmentDate, 1, 'months')
      newInstallment.DueDate = this.dateAdd(lastInstallment.DueDate, 1, 'months')
      newInstallment.Amount = 0;
      newInstallment.BillingDate = new Date();
      newInstallment.AdonPrice = 0;
      newInstallment.InstallmentNo = lastInstallment.InstallmentNo + 1;
      newInstallment.InstallmentType = 'M';
      newInstallment.Balance = 0;
      newInstallment.TrainingPeriodStart = this.dateAdd(lastInstallment.TrainingPeriodEnd, 1, 'days');
      newInstallment.TrainingPeriodEnd = this.dateAdd(newInstallment.TrainingPeriodStart, 1, 'months');
      newInstallment.IsATG = lastInstallment.IsATG;
      newInstallment.OriginalDueDate = this.dateAdd(lastInstallment.DueDate, 1, 'months')
      newInstallment.ActivityPrice = 0;
      newInstallment.PayerId = lastInstallment.MemberId;
      newInstallment.TemplateId = lastInstallment.v;
      newInstallment.ServiceAmount = 0;
      newInstallment.ItemAmount = 0;
      newInstallment.MemberContractNo = lastInstallment.MemberContractNo;
      newInstallment.Text = moment(newInstallment.TrainingPeriodStart).format(this._dateFormat) + ' - ' + moment(newInstallment.TrainingPeriodEnd).format(this._dateFormat);
      this._memberContract.InstalllmentList.push(newInstallment);
    }
  }

  closeOderDetailModal() {
    this.modalReference.close();
  }

  removeOrder(item) {
    this._deletingOrder = item;
    this.messageService.openMessageBox('CONFIRM',
      {
        messageTitle: 'Exceline',
        messageBody: 'MEMBERSHIP.ConfirmDelete',
        msgBoxId: 'ORDER'
      });
  }

  deleteOrder() {
    const deletingIndex = this._memberContract.InstalllmentList.indexOf(this._deletingOrder);
    this._memberContract.InstalllmentList.splice(deletingIndex, 1);
  }

  moveDueDates(selectedOrder: any, direction: string) {
    this._movingOrder = selectedOrder;
    this._movingDirection = direction;
    if (direction === 'UP') {
      this.messageService.openMessageBox('CONFIRM',
        {
          messageTitle: 'Exceline',
          messageBody: 'MEMBERSHIP.ConfirmDatedwn', // this is changed to be more meaningfull
          msgBoxId: 'MOVEORDER'
        });
    } else if (direction === 'DOWN') {
      this.messageService.openMessageBox('CONFIRM',
        {
          messageTitle: 'Exceline',
          messageBody: 'MEMBERSHIP.ConfirmDateUp', // this is changed to be more meaningfull
          msgBoxId: 'MOVEORDER'
        });
    }
  }

  moveOrders() {
    if (this._movingDirection === 'UP') {
      this._memberContract.InstalllmentList.forEach(order => {
        if (order.InstallmentNo >= this._movingOrder.InstallmentNo) {
          order.DueDate = this.dateAdd(order.DueDate, -1, 'months');
        }
      });
    } else if (this._movingDirection === 'DOWN') {
      this._memberContract.InstalllmentList.forEach(order => {
        if (order.InstallmentNo >= this._movingOrder.InstallmentNo) {
          order.DueDate = this.dateAdd(order.DueDate, 1, 'months');
        }
      });
    }
  }

  onNoOfOrdersChanged() {
    const noOfOrders: number = Number(this.economyForm.get('numberOfOrders').value);
    if (noOfOrders > 0) {
      this._memberContract.EveryMonthItemList.forEach(monthltItem => {
        if (monthltItem.IsActivityArticle) {
          monthltItem.NumberOfOrders = noOfOrders;
          const servicePrice: number = Number(this._memberContract.EveryMonthItemPrice) / noOfOrders;
          monthltItem.Price = servicePrice;
        } else {
          if (monthltItem.NumberOfOrders > noOfOrders) {
            monthltItem.NumberOfOrders = noOfOrders;
          }
        }
      });
      this._memberContract.NoOfInstallments = noOfOrders;
      this.updateEconomy();
    }
  }

  selectArticles(content: any, type: string) {
    this._orderlineType = type;

    switch (this._orderlineType) {
      case 'MEMBERFEE':
        this.itemSelectionTitle = 'MEMBERSHIP.SelectMemberFeeItems'
        this.selectionType = 'ITEM';
        break;
      case 'MONTHLY':
        this.itemSelectionTitle = 'MEMBERSHIP.SelectMItems'
        this.selectionType = 'SERVICE';
        break;
      case 'STARTUP':
        this.itemSelectionTitle = 'MEMBERSHIP.SelectSItems'
        this.selectionType = 'ITEM';
        break;
    }
    this.orderlineModel = this.modalService.open(content);
  }

  orderlineModelClose() {
    this.orderlineModel.close();
  }

  selectItemByDoubleClick(article) {
    let add = true;
    this._memberContract.EveryMonthItemList.forEach(item => {
      if (item.ActivityId === article.ActivityId && this._orderlineType === 'MONTHLY' && item.ActivityId === 188) {
        let messageBody: string;
        this.translateService.get('MEMBERSHIP.MsgMonthlyItemActivity').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => messageBody = translatedValue);
        this.messageService.openMessageBox('WARNING', {
          messageTitle: '',
          messageBody: messageBody
        })
        add = false;
      } else if (item.ActivityID === article.ActivityId && this._orderlineType === 'MONTHLY' && item.ActivityID === 188) {
        let messageBody: string;
        this.translateService.get('MEMBERSHIP.MsgMonthlyItemActivity').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => messageBody = translatedValue);
        this.messageService.openMessageBox('WARNING', {
          messageTitle: 'Exceline',
          messageBody: messageBody
        })
        add = false;
      }
    });
    if (add) {
      switch (this._orderlineType) {
        case 'MEMBERFEE':
          this.handleMemberFee(article);
          break;
        case 'MONTHLY':
        const monthlyArticles = [];
        const contractItem: any = {
            ArticleId: article.Id,
            ItemName: article.Description,
            Price: article.DefaultPrice,
            StartOrder: 0,
            EndOrder: 0,
            NumberOfOrders: 0,
            Quantity: article.Quantity,
            UnitPrice: article.DefaultPrice
          }
          monthlyArticles.push(contractItem);
          this.handleContractItems(monthlyArticles, 'MONTHLY');
          break;
        case 'STARTUP':
        const startUpArticles = [];
        const startUpItem: any = {
            ArticleId: article.Id,
            ItemName: article.Description,
            Price: article.DefaultPrice,
            Quantity: article.Quantity,
            UnitPrice: article.DefaultPrice
          }
          startUpArticles.push(startUpItem);
          this.handleContractItems(startUpArticles, 'STARTUP');
          break;
      }
    }
    this.orderlineModel.close();
  }

  selectMultipleArticles(articles) {
    if (articles.length > 0) {
      switch (this._orderlineType) {
        case 'MEMBERFEE':
          this.handleMemberFee(articles[0]);
          break;
        case 'MONTHLY':
        const monthlyArticles = [];
          articles.forEach(article => {
            const contractItem: any = {
              ArticleId: article.Id,
              ItemName: article.Description,
              Price: article.DefaultPrice,
              StartOrder: 0,
              EndOrder: 0,
              NumberOfOrders: 0,
              Quantity: article.Quantity,
              UnitPrice: article.DefaultPrice
            }
            if (this._memberContract.ActivityId === 188 && article.ActivityId === this._memberContract.ActivityId) {
              let messageBody: string;
              this.translateService.get('MEMBERSHIP.MsgMonthlyItemActivity').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => messageBody = translatedValue);
              this.messageService.openMessageBox('WARNING', {
                messageTitle: 'Exceline',
                messageBody: messageBody
              })
            } else {
              monthlyArticles.push(contractItem);
            }
            // monthlyArticles.push(contractItem);
          });
          this.handleContractItems(monthlyArticles, 'MONTHLY');
          break;
        case 'STARTUP':
        const startUpArticles = [];
          articles.forEach(article => {
            const startUpItem: any = {
              ArticleId: article.Id,
              ItemName: article.Description,
              Price: article.DefaultPrice,
              Quantity: article.Quantity,
              UnitPrice: article.DefaultPrice
            }
            startUpArticles.push(startUpItem);
          });
          this.handleContractItems(startUpArticles, 'STARTUP');
          break;
      }
    }
    this.orderlineModel.close();
  }

  handleContractItems(contractitems: any, itemType: string) {
    if (itemType === 'MONTHLY') {
      contractitems.forEach(monthlyItem => {
        monthlyItem.StartOrder = 1;
        monthlyItem.EndOrder = this._memberContract.NoOfInstallments;
        monthlyItem.NumberOfOrders = this._memberContract.NoOfInstallments;
        this._memberContract.EveryMonthItemList.push(monthlyItem);
        this.updateEconomy();
      });
    } else if (itemType === 'STARTUP') {
      contractitems.forEach(strtupItem => {
        this._memberContract.StartUpItemList.push(strtupItem);
        this.updateEconomy();
      });
    }
  }

  handleMemberFee(memberFeeArticle: any) {
    if (memberFeeArticle) {

      const contractItem: any = {
        ArticleId: memberFeeArticle.Id,
        ItemName: memberFeeArticle.Description,
        Price: memberFeeArticle.Price,
        Quantity: 1
      }

      this._memberContract.MemberFeeArticleID = contractItem.ArticleId;
      this._memberContract.MemberFeeArticleName = contractItem.ItemName;

      // update the economy form
      this.economyForm.patchValue({
        MemberFee: this._memberContract.MemberFeeArticleName
      });

    }
  }

  orderUpdateSuccess() {

  }

  onMonthlyItemChange() {
    this.updateEconomy();
  }

  onMonthlyItemPriceChange(monthlyItem: any, event) {
    monthlyItem.Price = Number(monthlyItem.Price);
    if (monthlyItem.ActivityID === this._memberContract.ActivityId) {
      this.ServiceAmount = Number(event);
      this._memberContract.ServiceAmount = monthlyItem.Price;
    } else if (monthlyItem.ActivityId === this._memberContract.ActivityId) {
      this.ServiceAmount = Number(event);
      this._memberContract.ServiceAmount = monthlyItem.Price;
    }
    this.updateEconomy();
  }

  startDueDateChanged(event: any) {
    this._contractDueDate = new Date(event.date.year, event.date.month - 1, event.date.day);
    this.updateEconomy();
  }

  onStartUpItemQuantityChange(startUpitem: any) {
    startUpitem.Price = startUpitem.UnitPrice * startUpitem.Quantity;
    this.updateEconomy();
  }

  onOrdersLeave() {
    this._groupMembers.forEach(gMember => {
      gMember.GmStartDate = moment(this._memberContract.ContractStartDate).format('DD/MM/YYYY');
      gMember.StartDate = { date: { year: moment(this._memberContract.ContractStartDate).year(),
        month: moment(this._memberContract.ContractStartDate).month() + 1, day: moment(this._memberContract.ContractStartDate).date() } };
      this._memberContract.GroupMembers.push(gMember);
    });
    this.isSaveVisible = true;
  }

  addGroupMembers(content: any) {
    this.groupModalRef = this.modalService.open(content);
  }

  // Search a member in member selection
  searchMember(val: any): void {
    PreloaderService.showPreLoader();
    this.memberService.getMembers(val.branchSelected.BranchId, val.searchVal, val.statusSelected.id,
      'SEARCH', val.roleSelected.id, val.hit, false,
      false, '').pipe(takeUntil(this.destroy$)).subscribe(result => {
        PreloaderService.hidePreLoader();
        if (result) {
          this.entityItems = result.Data;
          this.itemCount = result.Data.length;
        } else {
        }
      }, err => {
        PreloaderService.hidePreLoader();
      });
  }

  closeMemberSelectionView() {
    this.groupModalRef.close();
  }

  MemberSelected(selectedmembers: any) {
    let duplicateFound = false;
    let message = '';
    selectedmembers.forEach(member => {
      const existingmembers = this._memberContract.GroupMembers.filter(X => X.Id === member.Id)
      if (existingmembers.length > 0) {
        duplicateFound = true;
        message += existingmembers[0].CustId + ', ';
      } else {
        member.GmStartDate = moment(this._memberContract.ContractStartDate).format('DD/MM/YYYY');
        member.StartDate = { date: { year: moment(this._memberContract.ContractStartDate).year(),
          month: moment(this._memberContract.ContractStartDate).month() + 1, day: moment(this._memberContract.ContractStartDate).date() } };
        this._memberContract.GroupMembers.push(member);
      }
    });

    if (duplicateFound) {
      let messageBody: string;
      this.translateService.get('MEMBERSHIP.MsgDuplicateGroupMember').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => messageBody = translatedValue);
      this.messageService.openMessageBox('WARNING', {
        messageTitle: 'Exceline',
        messageBody: message + ' ' + messageBody
      })
    }
    this.groupModalRef.close();
  }

  groupDateChanged(event, item: any, dateType: string) {
    if (dateType === 'START') {
      item.StartDate = event;
    } else if (dateType === 'END') {
      item.EndDate = event;
    }
  }

  // get Add Member View
  addMemberView(content: any) {
    this.addNewMemberModalRef = this.modalService.open(content, { width: '800' });
    this.groupModalRef.close();
  }

  addNewMember(member: any) {
    member.GmStartDate = moment(this._memberContract.ContractStartDate).format('DD/MM/YYYY');
    this._memberContract.GroupMembers.push(member);
    this.addNewMemberModalRef.close();
  }

  closeAddMemberView() {
    this.addNewMemberModalRef.close();
  }
  removeGroupmMember(member: any) {
    this._deletingGroupmember = member;
    this.messageService.openMessageBox('CONFIRM',
      {
        messageTitle: 'Exceline',
        messageBody: 'MEMBERSHIP.ConfirmDelete',
        msgBoxId: 'GROUPMEMBER'
      });
  }

  deleteGroupmember() {
    const itemIndex = this._memberContract.GroupMembers.indexOf(this._deletingGroupmember);
    this._memberContract.GroupMembers.splice(itemIndex, 1);


  }

  fetchData(branch) {
    this.adminService.getGymSettings('ECONOMY', null, branch).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          this._isMemberFee = result.Data[0].IsMemberFee;
          this._isMemberFeeMonth = result.Data[0].IsMemberFeeMonth;
        }
      }
    )
  }

  saveContract(event) {
    // get data from basic from
    this._memberContract.ContractName = this.basicForm.value.ContractName,
    this._memberContract.TemplateNo = this.basicForm.value.TemplateNo,
    this._memberContract.ContractCategoryName = this.basicForm.value.ContractCategoryName,
    this._memberContract.ActivityName = this.basicForm.value.ActivityName,
    this._memberContract.CreatedDate = this.manipulatedate(this.basicForm.value.CreatedDate);
    this._memberContract.NoOfMonths = this.basicForm.value.NoOfMonths,
    this._memberContract.ContractStartDate = this.manipulatedate(this.basicForm.value.ContractStartDate),
    this._memberContract.ContractEndDate = this.manipulatedate(this.basicForm.value.ContractEndDate),
    this._memberContract.IsATG = this.basicForm.value.IsATG,
    this._memberContract.AutoRenew = this.basicForm.value.AutoRenew,
    this._memberContract.IsInvoiceDetails = this.basicForm.value.IsInvoiceDetails,
    this._memberContract.LockInPeriod = this.basicForm.value.LockInPeriod,
    this._memberContract.LockInPeriodUntilDate = this.manipulatedate(this.basicForm.value.LockInPeriodUntilDate),
    this._memberContract.GuarantyPeriod = this.basicForm.value.GuarantyPeriod,
    this._memberContract.PriceGuarantyUntillDate = this.manipulatedate(this.basicForm.value.PriceGuarantyUntillDate),
    this._memberContract.NumberOfVisits = this.basicForm.value.NumberOfVisits,
    this._memberContract.NoofDays = this.basicForm.value.NoofDays,
    this._memberContract.BranchName = this.basicForm.value.BranchName,
    this._memberContract.NoOfVisits = this.basicForm.value.NumberOfVisits

    // get data from ecnomy form
    this._memberContract.NoOfInstallments = this.economyForm.value.numberOfOrders;
    if (this.economyForm.value.MemberFeeMonth != null) {
      this._memberContract.MemberFeeMonth = Number(this.economyForm.value.MemberFeeMonth);
    }

    this._memberContract.GroupMembers.forEach(gropMember => {
      gropMember.GmStartDate = this.manipulatedate(gropMember.StartDate)
      gropMember.GmEndDate = gropMember.EndDate ? this.manipulatedate(gropMember.EndDate) : null;
    });
    // save the contract
    this.memberService.SaveMemberContract(this._memberContract).pipe(takeUntil(this.destroy$)).subscribe(
      result => {
        if (result) {
          switch (result.Data.MemberContractId) {
            case -2:
              this.messageService.openMessageBox('WARNING',
                {
                  messageTitle: 'Exceline',
                  messageBody: 'MEMBERSHIP.MsgDuplicateBasicContracts'
                }
              );
              break;

            case -3:
               this.messageService.openMessageBox('WARNING',
                {
                  messageTitle: 'Exceline',
                  messageBody: 'MEMBERSHIP.ReturnShopAccountBalance'
                }
              );
              break;

            case -4:
              this.messageService.openMessageBox('WARNING',
                {
                  messageTitle: 'Exceline',
                  messageBody: 'MEMBERSHIP.ReturnPrepaisAccountBalace'
                }
              );
              break;

            case -5:
               this.messageService.openMessageBox('WARNING',
                {
                  messageTitle: 'Exceline',
                  messageBody: 'MEMBERSHIP.MsgPaytheShopBalance'
                }
              );
              break;
          }

          if (result.Data.MemberContractId > 0) {
            const base = '/membership/card/' + this._selectedMember.Id + '/' + this._selectedMember.BranchId + '/' + MemberRole[this._selectedMember.Role] + '/contracts/';
            this.router.navigate([base + '/' + result.Data.MemberContractId]);
          }

        }
      },
      error => {
        if (error) {
          this.messageService.openMessageBox('WARNING',
            {
              messageTitle: 'Exceline',
              messageBody: 'MEMBERSHIP.MsgConSaveError'
            }
          );
        }
      }
    );
  }

  manipulatedate(dateInput: any) {
    if (dateInput.date !== undefined) {
      const tempDate: Date = new Date(dateInput.date.year, dateInput.date.month - 1, dateInput.date.day);
      return moment(tempDate).format('YYYY-MM-DD').toString();
    }
  }

  onCancel() {
    if (this._selectedTemplate) {
      this.messageService.openMessageBox('CONFIRM',
        {
          messageTitle: 'Exceline',
          messageBody: 'MEMBERSHIP.ConfrimSaveContract',
          msgBoxId: 'ONEXIT'
        });
    } else {
      this.exitContractView();
    }
  }

  exitContractView() {
    this.router.navigate(['/membership/card/' + this._selectedMember.Id + '/' + this._selectedMember.BranchId + '/' + MemberRole[this._selectedMember.Role] + '/contracts'])
  }

  dateValidator() {
    const startDate = moment();
    startDate.year(this.basicForm.value.ContractStartDate.date.year);
    startDate.month(this.basicForm.value.ContractStartDate.date.month);
    startDate.day(this.basicForm.value.ContractStartDate.date.day);

    const endDate = moment();
    endDate.year(this.basicForm.value.ContractEndDate.date.year);
    endDate.month(this.basicForm.value.ContractEndDate.date.month);
    endDate.day(this.basicForm.value.ContractEndDate.date.day);

    if ((this.basicForm.value.ContractStartDate && this.basicForm.value.ContractEndDate) &&
        (endDate.diff(startDate, 'days') < 0)) {
        this.basicForm.controls['ContractEndDate'].setErrors({
          'compare' : true
        });
        this.translateService.get('MEMBERSHIP.IllogicalDate').pipe(takeUntil(this.destroy$)).subscribe(translatedValue =>
          this.formErrors.ContractEndDate = translatedValue);
      }
  }

  startDateChanged(event) {
    this._memberContract.ContractStartDate = event.jsdate;
    this.onNoOfPeriodChange(Number(this.basicForm.get('NoOfMonths').value), 'MONTHS')
  }

  dateAdd(startDate: any, count: any, interval: string): Date {
    return moment(startDate).add(count, interval).toDate()
  }

  isLeapYear(date) {
    const year = date.getFullYear();
    return (year % 100 === 0) ? (year % 400 === 0) : (year % 4 === 0);
  }

  monthsChanged(val) {
    this.economyForm.patchValue( {
      'numberOfOrders': val
    })
    this._memberContract.EveryMonthItemList.forEach(element => {
      element.NumberOfOrders = Number(val);
      element.EndOrder = Number(val);
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();

    if (this.modalReference) {
      this.modalReference.close();
    }

    if (this.orderlineModel) {
      this.orderlineModel.close();
    }

    if (this.conditionRef) {
      this.conditionRef.close();
    }

    if (this._templateModal) {
      this._templateModal.close();
      this.modalOpen = false;
    }

    if (this.groupModalRef) {
      this.groupModalRef.close();
    }

    if (this.addNewMemberModalRef) {
      this.addNewMemberModalRef.close();
    }
  }
}
