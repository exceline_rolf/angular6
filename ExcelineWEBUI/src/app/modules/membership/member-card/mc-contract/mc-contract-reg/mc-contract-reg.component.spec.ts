import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { McContractRegComponent } from './mc-contract-reg.component';

describe('McContractRegComponent', () => {
  let component: McContractRegComponent;
  let fixture: ComponentFixture<McContractRegComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ McContractRegComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(McContractRegComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
