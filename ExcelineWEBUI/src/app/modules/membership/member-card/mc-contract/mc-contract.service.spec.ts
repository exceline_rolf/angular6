import { TestBed, inject } from '@angular/core/testing';

import { McContractService } from './mc-contract.service';

describe('McContractService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [McContractService]
    });
  });

  it('should be created', inject([McContractService], (service: McContractService) => {
    expect(service).toBeTruthy();
  }));
});
