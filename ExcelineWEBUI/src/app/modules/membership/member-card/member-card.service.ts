import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class MemberCardService {

  private isleftPanelCollapsed = false;
  public leftPanelCollaped = new Subject<any>();

  public guardianDetails = new Subject<any>();
  private selectedGuardian: any;

  get SelectedGuardian(): any {
    return this.selectedGuardian;
  }

  set SelectedGuardian(selectedGuardian) {
    this.guardianDetails.next(selectedGuardian);
    this.selectedGuardian = selectedGuardian;
  }

  constructor() { }

  get $isleftPanelCollapsed(): boolean {
    return this.isleftPanelCollapsed;
  }

  set $isleftPanelCollapsed(value: boolean) {
    this.isleftPanelCollapsed = value;
    this.leftPanelCollaped.next(this.isleftPanelCollapsed);
  }



}
