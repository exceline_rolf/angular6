import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectOrderlinesComponent } from './select-orderlines.component';

describe('SelectOrderlinesComponent', () => {
  let component: SelectOrderlinesComponent;
  let fixture: ComponentFixture<SelectOrderlinesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectOrderlinesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectOrderlinesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
