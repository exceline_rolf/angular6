import { ExcePdfViewerService } from './../../../../../../../shared/components/exce-pdf-viewer/exce-pdf-viewer.service';
import { PreloaderService } from './../../../../../../../shared/services/preloader.service';
import { NotificationMethodType } from './../../../../../../../shared/enums/us-enum';
// import { element } from 'protractor';
import { ExceToolbarService } from './../../../../../../common/exce-toolbar/exce-toolbar.service';
import { Extension } from 'app/shared/Utills/Extensions';
import { Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter, OnDestroy, DoCheck, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { DataTableResource } from '../../../../../../../shared/components/us-data-table/tools/data-table-resource';
import { DataTableFilterPipe } from '../../../../../../../shared/components/us-data-table/tools/data-table-filter-pipe'
import { UsbModal } from '../../../../../../../shared/components/us-modal/us-modal';
import { ExceMemberService } from '../../../../../services/exce-member.service'
import { TranslateService } from '@ngx-translate/core';
import { McBasicInfoService } from '../../../../mc-basic-info/mc-basic-info.service';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { ExceLoginService } from '../../../../../../../modules/login/exce-login/exce-login.service';
import { UsErrorService } from '../../../../../../../shared/directives/us-error/us-error.service'
import * as moment from 'moment';
import value from '*.json';
import { ExceMessageService } from 'app/shared/components/exce-message/exce-message.service';
import { error } from 'util';
import { RowNode } from 'ag-grid-community';
// import { Observable, Subscription } from 'rxjs';
import { Subject } from 'rxjs'
import { Observable } from 'rxjs';
import { Subscription } from 'rxjs';
import { takeUntil, distinctUntilChanged, mergeMap } from 'rxjs/operators';


const now = new Date();

@Component({
  selector: 'app-new-freeze',
  templateUrl: './new-freeze.component.html',
  styleUrls: ['./new-freeze.component.scss']
})
export class NewFreezeComponent implements OnInit, OnDestroy {
  originalFreezeMonths: any;
  locale: string;

  private insItems: any;
  private itemResource: any = 0;
  public items = [];
  public itemCount = 0;
  private resources: any;
  private modalReference?: any;
  private rowItem: any;
  private selectedMember: any;
  private today: NgbDateStruct;
  private monthbefore: NgbDateStruct;
  private fromdate: any;
  private toDate: any;
  private selectedIndex = 1;
  freezCat: any
  newFreezeForm: FormGroup;
  formSubmited = false;
  private saveItem: any;
  private fromDate: any;
  private dateObj: NgbDateStruct;
  private fromDateValue: any
  isExtendedVal: boolean = true
  private branch: any;
  private newInsList = []
  freezMonthsNo = 0;
  private isTablepopulated: boolean = false;
  private shiftOrders = 0;
  private ShiftStartInstallmentId = 0;
  isDisabled = false;
  // moveOrdersDisabled = true;
  bgColor1: any = '#FFFF00';
  private toDateValue: any;
  originalEnddate: any;
  selectedCategoryId: number = -1;
  originalShiftCount: any;
  shiftDisabled = false;

   // Variables in this block are intentionally left ambiguous, as they are used for various backend confirmations.
  type: string;
  isShowInfo: boolean;
  message: String;
  // -----
  theDankestCategoryId = new FormControl();


  @Input() selectedItem: any;
  @Input() selectedContract: number;
  @Input() contract: any;
  @Input() isEdit = false;
  @Input() editType: any;
  @Input() insItemsToFreeze: any[];

  @Output() canceled = new EventEmitter();
  @Output() saveSuccess = new EventEmitter();
  formErrors = {
    'CategoryId': '',
    'FreezeMonths': '',
    'FromDate': '',
    'ToDate': '',
    'IsExtended': '',
    'ShiftCount': '',
    'FreezeDays': '',
    'ContractEndDate': '',
    'Note': ''
  };

  validationMessages = {
    'CategoryId': {
      'required': 'MEMBERSHIP.Required',
    },

    'FreezeMonths': {
      'required': 'MEMBERSHIP.Required',
    },
    'FromDate': {
      'required': 'MEMBERSHIP.Required',
    },
    'ToDate': {
      'required': 'MEMBERSHIP.Required',
    },

    'IsExtended': {
      'required': 'MEMBERSHIP.Required',
    },
    'ShiftCount': {
      'required': 'MEMBERSHIP.Required',
    },
    'FreezeDays': {
      'required': 'MEMBERSHIP.Required',
    },
    'ContractEndDate': {
      'required': 'MEMBERSHIP.Required',
    }
  };

  names = {
    OrderNr: 'Ordrenummer',
    DueDate: 'Forfall',
    ActivityPrice: 'Aktivitetspris',
    TrainingPeriodStart: 'Treningsperiode start',
    TrainingPeriodEnd: 'Treningsperiode slutt',
    NewDueDate: 'Nytt forfall',
    NewTrainingPeriodStart: 'Ny Treningsperiode start',
    NewTrainingPeriodEnd: 'Ny Treningsperiode slutt'
  }

  gridApi: any;
  gridColumnApi: any;
  getRowHeight: (params: any) => any;
  gridIsSized = false;
  rowClassRules: any;
  orderlistDefs: any;
  isNextDueDateHighlight = true;
  phDate: string;

  private destroy$ = new Subject<void>();
  currentHighlightedRow: any;


  constructor(private exceMemberService: ExceMemberService,
    private basicInfoService: McBasicInfoService,
    private modalService: UsbModal,
    private fb: FormBuilder,
    private exceLoginService: ExceLoginService,
    private translateService: TranslateService,
    private toolbarService: ExceToolbarService,
    private messageService: ExceMessageService,
    private excePdfViewerService: ExcePdfViewerService
  ) {

    this.today = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
    this.monthbefore = { year: now.getFullYear(), month: now.getMonth(), day: now.getDate() };
    this.branch = this.exceLoginService.SelectedBranch;
    this.exceLoginService.branchSelected.pipe(takeUntil(this.destroy$)).subscribe(
      (branch) => {
        this.branch = branch;
      }
    );
  }

  ngOnInit() {
    const language = this.toolbarService.getSelectedLanguage();
    this.locale = language.culture;
    // this._dateFormat = 'DD.MM.YYYY';
    this.toolbarService.langUpdated.pipe(takeUntil(this.destroy$)).subscribe(
      (lang) => {
        if (lang) {
          this.locale = lang.culture;
        }
      }
    );
    this.newFreezeForm = this.fb.group({
      'CategoryId': [null, [Validators.required]],
      'FreezeMonths': [null, [Validators.required]],
      'FromDate': [null, [Validators.required]],
      'ToDate': [null, [Validators.required]],
      'IsExtended': [null, [Validators.required]],
      'ShiftCount': [null, [Validators.required]],
      'FreezeDays': [null, [Validators.required]],
      'ContractEndDate': [null, [Validators.required]],
      'Note': [null]
    });

    if (this.editType === 'EXTEND') {
      this.theDankestCategoryId.setValue(this.selectedItem.CategoryId);
    }

    this.basicInfoService.currentMember.pipe(
      mergeMap(currentMember => {
        this.selectedMember = currentMember;
        return this.exceMemberService.getCategories('FREEZE');
      }),
      mergeMap(freezeCategory => {
        if (freezeCategory) {
          this.freezCat = freezeCategory.Data;
          if (this.editType === 'NEW') {
            this.theDankestCategoryId.enable();
            this.theDankestCategoryId.setValue(this.freezCat[0].Id);
            this.newFreezeForm.controls['FreezeMonths'].disable({
              onlySelf: true,
              emitEvent: false
            });
            this.newFreezeForm.controls['ShiftCount'].disable({
              onlySelf: true,
              emitEvent: false
            });
          } else {
            this.theDankestCategoryId.disable();
          }

          this.newFreezeForm.patchValue({
            'CategoryId': this.theDankestCategoryId.value
          });

          if (!this.isEdit) {
            this.originalEnddate = this.contract.ContractEndDate;
            this.newFreezeForm.patchValue({
              IdExtended: true,
              ContractEndDate: {
                date: Extension.GetFormatteredDate(this.contract.ContractEndDate)
              }
            });
          }

          if (this.editType !== 'NEW') {
            if (this.selectedItem) {
              this.originalShiftCount = this.selectedItem.ShiftCount;
              this.originalFreezeMonths = this.selectedItem.FreezeMonths;
              if (this.selectedItem.IsExtended) {
                this.isExtendedVal = true
                this.newFreezeForm.controls['ShiftCount'].enable({
                  onlySelf: true,
                  emitEvent: false
                });
              } else {
                this.isExtendedVal = false
                this.newFreezeForm.controls['ShiftCount'].disable({
                  onlySelf: true,
                  emitEvent: false
                });
              }
              this.newFreezeForm.patchValue({
                FreezeMonths: this.selectedItem.FreezeMonths,
                FromDate: { date: Extension.GetFormatteredDate(this.selectedItem.FromDate) },
                ToDate: { date: Extension.GetFormatteredDate(this.selectedItem.ToDate) },
                ShiftCount: this.selectedItem.ShiftCount,
                FreezeDays: this.selectedItem.FreezeDays,
                ContractEndDate: { date: Extension.GetFormatteredDate(this.selectedItem.ContractEndDate) },
                Note: this.selectedItem.Note,
                CategoryId: this.selectedItem.CategoryId
              })
              this.shiftOrders = this.selectedItem.ShiftCount;
            }
          }
        }
        return this.exceMemberService.GetInstallmentsToFreeze(this.selectedContract);
      }),
      takeUntil(this.destroy$)
    ).subscribe(installmentToFreeze => {
      if (installmentToFreeze) {
        this.insItems = installmentToFreeze.Data;
        this.items = this.insItems;
        this.newInsList = [];
        this.items.forEach(element => {
          /* Transforming from T00:00:00 to T12:00:00*/
          element.DueDate = this.setNoonTransform(element.DueDate);
          element.OriginalDueDate = this.setNoonTransform(element.OriginalDueDate);
          element.PaidDate = this.setNoonTransform(element.PaidDate);
          element.PrintedDate = this.setNoonTransform(element.PrintedDate);
          element.DebtCollectionDate = this.setNoonTransform(element.DebtCollectionDate);
          element.BillingDate = this.setNoonTransform(element.BillingDate);
          element.TransferDate = this.setNoonTransform(element.TransferDate);
          if (this.editType !== 'NEW') {
            if (element.TableId >= this.insItemsToFreeze[0].InstallmentId) { /* First item is start of shift */
            // if (element.TableId >= this.selectedItem.ShiftStartInstallmentId) {
              element.isFreeze = true;
              element.NewInstallmentDueDate = element.DueDate;
              element.NewTrainingPeriodStart = element.TrainingPeriodStart;
              element.NewTrainingPeriodEnd = element.TrainingPeriodEnd;
              this.newInsList.push(element);
            }
          } else {
            element.isFreeze = false;
            element.CannotEditAmount = true;
          }
        });
      }
    })

    // this.basicInfoService.currentMember.mergeMap(currentMember => {
    //   this.selectedMember = currentMember;
    //   return this.exceMemberService.getCategories('FREEZE');
    // }).mergeMap(freezeCategory => {
    //   if (freezeCategory) {
    //     this.freezCat = freezeCategory.Data;
    //     if (this.editType === 'NEW') {
    //       this.theDankestCategoryId.enable();
    //       this.theDankestCategoryId.setValue(this.freezCat[0].Id)
    //       this.newFreezeForm.controls['FreezeMonths'].disable({
    //         onlySelf: true,
    //         emitEvent: false
    //     });
    //       this.newFreezeForm.controls['ShiftCount'].disable({
    //         onlySelf: true,
    //         emitEvent: false
    //     });
    //     } else {
    //       this.theDankestCategoryId.disable();
    //     }
    //     this.newFreezeForm.patchValue({
    //       'CategoryId': this.theDankestCategoryId.value
    //     })
    //     if (!this.isEdit) {
    //       this.originalEnddate = this.contract.ContractEndDate;
    //       this.newFreezeForm.patchValue({
    //         IsExtended: true,
    //         ContractEndDate: { date: Extension.GetFormatteredDate(this.contract.ContractEndDate) }
    //       });
    //     }
    //     if (this.editType !== 'NEW') {
    //       if (this.selectedItem) {
    //         this.originalShiftCount = this.selectedItem.ShiftCount;
    //         this.originalFreezeMonths = this.selectedItem.FreezeMonths;
    //         if (this.selectedItem.IsExtended) {
    //           this.isExtendedVal = true
    //           this.newFreezeForm.controls['ShiftCount'].enable({
    //             onlySelf: true,
    //             emitEvent: false
    //           });
    //         } else {
    //           this.isExtendedVal = false
    //           this.newFreezeForm.controls['ShiftCount'].disable({
    //             onlySelf: true,
    //             emitEvent: false
    //           });
    //         }
    //         this.newFreezeForm.patchValue({
    //           FreezeMonths: this.selectedItem.FreezeMonths,
    //           FromDate: { date: Extension.GetFormatteredDate(this.selectedItem.FromDate) },
    //           ToDate: { date: Extension.GetFormatteredDate(this.selectedItem.ToDate) },
    //           ShiftCount: this.selectedItem.ShiftCount,
    //           FreezeDays: this.selectedItem.FreezeDays,
    //           ContractEndDate: { date: Extension.GetFormatteredDate(this.selectedItem.ContractEndDate) },
    //           Note: this.selectedItem.Note,
    //           CategoryId: this.selectedItem.CategoryId
    //         })
    //         this.shiftOrders = this.selectedItem.ShiftCount;
    //       }
    //     }
    //   }
    //   return this.exceMemberService.GetInstallmentsToFreeze(this.selectedContract).pipe(
    //     takeUntil(this.destroy$)
    //   )
    // }).pipe(
    //   takeUntil(this.destroy$)
    // ).subscribe(installmentToFreeze => {
    //   if (installmentToFreeze) {
    //     this.insItems = installmentToFreeze.Data;
    //     this.items = this.insItems;
    //     this.newInsList = [];
    //     this.items.forEach(element => {
    //       /* Transforming from T00:00:00 to T12:00:00*/
    //       element.DueDate = this.setNoonTransform(element.DueDate);
    //       element.OriginalDueDate = this.setNoonTransform(element.OriginalDueDate);
    //       element.PaidDate = this.setNoonTransform(element.PaidDate);
    //       element.PrintedDate = this.setNoonTransform(element.PrintedDate);
    //       element.DebtCollectionDate = this.setNoonTransform(element.DebtCollectionDate);
    //       element.BillingDate = this.setNoonTransform(element.BillingDate);
    //       element.TransferDate = this.setNoonTransform(element.TransferDate);
    //       if (this.editType !== 'NEW') {
    //         if (element.TableId >= this.insItemsToFreeze[0].InstallmentId) { /* First item is start of shift */
    //         // if (element.TableId >= this.selectedItem.ShiftStartInstallmentId) {
    //           element.isFreeze = true;
    //           element.NewInstallmentDueDate = element.DueDate;
    //           element.NewTrainingPeriodStart = element.TrainingPeriodStart;
    //           element.NewTrainingPeriodEnd = element.TrainingPeriodEnd;
    //           this.newInsList.push(element);
    //         }
    //       } else {
    //         element.isFreeze = false;
    //         element.CannotEditAmount = true;
    //       }
    //     });
    //   }
    // })


    // this.basicInfoService.currentMember.pipe(takeUntil(this.destroy$)).subscribe(member => {
    //   if (member) {
    //     this.selectedMember = member;
    //     this.exceMemberService.getCategories('FREEZE').pipe(takeUntil(this.destroy$)).subscribe( result => {
    //         if (result) {
    //           this.freezCat = result.Data;
    //           if (this.editType === 'NEW') {
    //             this.theDankestCategoryId.enable();
    //             this.theDankestCategoryId.setValue(this.freezCat[0].Id)
    //             this.newFreezeForm.controls['FreezeMonths'].disable({
    //               onlySelf: true,
    //               emitEvent: false
    //           });
    //             this.newFreezeForm.controls['ShiftCount'].disable({
    //               onlySelf: true,
    //               emitEvent: false
    //           });
    //           } else {
    //             this.theDankestCategoryId.disable();
    //           }
    //           this.newFreezeForm.patchValue({
    //             'CategoryId': this.theDankestCategoryId.value
    //           })
    //           if (!this.isEdit) {
    //             this.originalEnddate = this.contract.ContractEndDate;
    //             this.newFreezeForm.patchValue({
    //               IsExtended: true,
    //               ContractEndDate: { date: Extension.GetFormatteredDate(this.contract.ContractEndDate) }
    //             });
    //           }
    //           if (this.editType !== 'NEW') {
    //             if (this.selectedItem) {
    //               this.originalShiftCount = this.selectedItem.ShiftCount;
    //               this.originalFreezeMonths = this.selectedItem.FreezeMonths;
    //               if (this.selectedItem.IsExtended) {
    //                 this.isExtendedVal = true
    //                 this.newFreezeForm.controls['ShiftCount'].enable({
    //                   onlySelf: true,
    //                   emitEvent: false
    //                 });
    //               } else {
    //                 this.isExtendedVal = false
    //                 this.newFreezeForm.controls['ShiftCount'].disable({
    //                   onlySelf: true,
    //                   emitEvent: false
    //                 });
    //               }
    //               this.newFreezeForm.patchValue({
    //                 FreezeMonths: this.selectedItem.FreezeMonths,
    //                 FromDate: { date: Extension.GetFormatteredDate(this.selectedItem.FromDate) },
    //                 ToDate: { date: Extension.GetFormatteredDate(this.selectedItem.ToDate) },
    //                 ShiftCount: this.selectedItem.ShiftCount,
    //                 FreezeDays: this.selectedItem.FreezeDays,
    //                 ContractEndDate: { date: Extension.GetFormatteredDate(this.selectedItem.ContractEndDate) },
    //                 Note: this.selectedItem.Note,
    //                 CategoryId: this.selectedItem.CategoryId
    //               })
    //               this.shiftOrders = this.selectedItem.ShiftCount;
    //             }
    //           }
    //         }
    //       }
    //     );

    //     this.exceMemberService.GetInstallmentsToFreeze(this.selectedContract).pipe(takeUntil(this.destroy$)).subscribe(result => {
    //         if (result) {
    //           this.insItems = result.Data;
    //           this.items = this.insItems;
    //           this.newInsList = [];
    //           this.items.forEach(element => {
    //             /* Transforming from T00:00:00 to T12:00:00*/
    //             element.DueDate = this.setNoonTransform(element.DueDate);
    //             element.OriginalDueDate = this.setNoonTransform(element.OriginalDueDate);
    //             element.PaidDate = this.setNoonTransform(element.PaidDate);
    //             element.PrintedDate = this.setNoonTransform(element.PrintedDate);
    //             element.DebtCollectionDate = this.setNoonTransform(element.DebtCollectionDate);
    //             element.BillingDate = this.setNoonTransform(element.BillingDate);
    //             element.TransferDate = this.setNoonTransform(element.TransferDate);
    //             if (this.editType !== 'NEW') {
    //               if (element.TableId >= this.insItemsToFreeze[0].InstallmentId) { /* First item is start of shift */
    //               // if (element.TableId >= this.selectedItem.ShiftStartInstallmentId) {
    //                 element.isFreeze = true;
    //                 element.NewInstallmentDueDate = element.DueDate;
    //                 element.NewTrainingPeriodStart = element.TrainingPeriodStart;
    //                 element.NewTrainingPeriodEnd = element.TrainingPeriodEnd;
    //                 this.newInsList.push(element);
    //               }
    //             } else {
    //               element.isFreeze = false;
    //               element.CannotEditAmount = true;
    //             }
    //           });
    //         }
    //       }
    //     );
    //   }
    // });

    // this.newFreezeForm.get('FreezeMonths').valueChanges.pipe(takeUntil(this.destroy$)).subscribe(numberOfMonths =>  {
    // this.newFreezeForm.get('FreezeMonths').valueChanges.distinctUntilChanged().pipe(takeUntil(this.destroy$)).subscribe(numberOfMonths =>  {
    this.newFreezeForm.get('FreezeMonths').valueChanges.pipe(
      distinctUntilChanged(),
      takeUntil(this.destroy$)
    ).subscribe(numberOfMonths => {
      if (this.newFreezeForm.value.FromDate) {
      // if (numberOfMonths) {
        this.OnShiftOrderschanged(numberOfMonths);
        this.newFreezeForm.patchValue({
          ['ShiftCount']: numberOfMonths
        });
        this.onFreezeMonthsChanged(numberOfMonths);
        if (this.editType === 'NEW') {
          this.setHighlight();
        }
      }
    });

      this.newFreezeForm.get('FromDate').valueChanges.pipe(takeUntil(this.destroy$)).subscribe(date => {
      if (date) {
        // this.moveOrdersDisabled = true;
        this.newFreezeForm.controls['ShiftCount'].enable();
        this.newFreezeForm.controls['FreezeMonths'].enable();
        this.removeFreezelight();
        this.removeHighlight(); // remove previous highlight
        this.setHighlight();
      } else {
      this.newFreezeForm.controls['ShiftCount'].disable();
      this.newFreezeForm.controls['FreezeMonths'].disable();
      this.removeHighlight();
      if (this.isFreezeMonthsOrShiftOrderEmpty()) {
        this.removeFreezelight();
      }
    }
    });

    if (this.editType !== 'NEW') {
      this.isDisabled = true;
    }


    this.orderlistDefs = [{
      headerName: this.names.OrderNr,
      field: 'TableId',
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true
      }, filterParams: {
          newRowsAction: 'keep'
        }, cellStyle: {
          'cursor': 'pointer'
        }, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      },
      suppressMovable: true,
      autoHeight: true
    }, {
      headerName: this.names.DueDate,
      field: 'DueDate',
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true
      }, filterParams: {
          newRowsAction: 'keep'
      }, cellStyle: {
          'cursor': 'pointer'
      }, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
    },
    suppressMovable: true,
    autoHeight: true,
    cellRenderer: function(params) {
      if (params.value) {
        const d = params.value.split('T');
        const dDate = d[0].split('-');
        return dDate[2] + '.' + dDate[1] + '.' + dDate[0];
      } else {
        return
      }
    }
  }, {
      headerName: this.names.ActivityPrice,
      field: 'ActivityPrice',
      suppressMenu: true,
      suppressSorting: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true
      }, filterParams: {
          newRowsAction: 'keep'
      }, cellStyle: {
          'cursor': 'pointer',
          'textAlign': 'center'
        }, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      },
      suppressMovable: true,
      autoHeight: true
  },
    {
      headerName: this.names.TrainingPeriodStart,
      field: 'TrainingPeriodStart',
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true},
      filterParams: {
        newRowsAction: 'keep'
      }, cellStyle: {
        'cursor': 'pointer'
      }, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      },
      suppressMovable: true,
      autoHeight: true,
      cellRenderer: function(params) {
        if (params.value) {
          const d = params.value.split('T');
          const dDate = d[0].split('-');
          return dDate[2] + '.' + dDate[1] + '.' + dDate[0];
        } else {
          return
        }
      }
  },
    {
      headerName: this.names.TrainingPeriodEnd,
      field: 'TrainingPeriodEnd',
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true
      },
      filterParams: {
        newRowsAction: 'keep'
      },
        cellStyle: {
          'cursor': 'pointer'
      }, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
    },
    suppressMovable: true,
    autoHeight: true,
    cellRenderer: function(params) {
      if (params.value) {
        const d = params.value.split('T');
        const dDate = d[0].split('-');
        return dDate[2] + '.' + dDate[1] + '.' + dDate[0];
      } else {
        return
      }
    }
  },
    {
      headerName: this.names.NewDueDate,
      field: 'NewInstallmentDueDate',
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true
      },
      filterParams: {
        newRowsAction: 'keep'
      }, cellStyle: {
        'cursor': 'pointer'
      }, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
    },
    suppressMovable: true,
    autoHeight: true,
    cellRenderer: function(params) {
      if (params.value) {
        // If params.value is a moment obj
        /* console.log('params: ', params.value)
        const q = params.value;
        const a = [ q.date(), q.month() + 1, q.year()]
        console.log(a[0] + '.' + a[1] + '.' + a[2])
        return a[0] + '.' + a[1] + '.' + a[2]; */
        const d = params.value.split('T');
        const dDate = d[0].split('-');
        return dDate[2] + '.' + dDate[1] + '.' + dDate[0];
      } else {
        return
      }
    }
  },
    {
      headerName: this.names.NewTrainingPeriodStart,
      field: 'NewTrainingPeriodStart',
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true
      },
      filterParams: {
        newRowsAction: 'keep'
      }, cellStyle: {
        'cursor': 'pointer'
      }, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
    },
    suppressMovable: true,
    autoHeight: true,
    cellRenderer: function(params) {
      if (params.value) {
        const d = params.value.split('T');
        const dDate = d[0].split('-');
        return dDate[2] + '.' + dDate[1] + '.' + dDate[0];
      } else {
        return
      }
    }
  },
    {
      headerName: this.names.NewTrainingPeriodEnd,
      field: 'NewTrainingPeriodEnd',
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true
      },
    filterParams: {
      newRowsAction: 'keep'
    }, cellStyle: {
      'cursor': 'pointer'
    }, icons: {
      sortAscending: '<i class="icon-sort-ascending"/>',
      sortDescending: '<i class="icon-sort-descending"/>'
  },
  suppressMovable: true,
  autoHeight: true,
  cellRenderer: function(params) {
    if (params.value) {
      const d = params.value.split('T');
      const dDate = d[0].split('-');
      return dDate[2] + '.' + dDate[1] + '.' + dDate[0];
    } else {
      return
      }
    }
  }
    ];

    this.rowClassRules = {
      'freeze-is-active' : (params => {
        return params.data.isFreeze
      }),
      'highlighted' : () => {
        // Do not change style if freezemonths or fromdate is empty, or if highlighting is set to false.
        if ( this.editType === 'EXTEND' || !this.newFreezeForm.value.FreezeMonths || !this.newFreezeForm.value.FromDate || !this.isNextDueDateHighlight) {
          return false;
        } else {
        return this.isNextDueDateHighlight;
        }
      }
  };

  this.getRowHeight = function(params) {
    return 30;
  };

  this.phDate = 'dd.MM.YYYY';

} // end ngOnInit



onGridReady(params) {
  this.gridApi = params.api;
  this.gridColumnApi = params.columnApi;
  // auto fits columns to fit screen
  params.api.sizeColumnsToFit();
  // hiding loading message on load
  this.gridApi.gridOptionsWrapper.gridOptions.api.hideOverlay();
}

  onFromDateChanged(event: any) {
    const freezeMonth = this.newFreezeForm.value.FreezeMonths == null ? 0 : this.newFreezeForm.value.FreezeMonths;
    // if (!isNaN(freezeMonth)) {
    // this.moveOrdersDisabled = false;
    if (this.newFreezeForm.value.FreezeMonths == null) {
      this.newFreezeForm.patchValue({
        'ShiftCount' : '',
        'FreezeMonths' : ''
      });

    const endDate: any = this.dateAdd(this.manipulatedate(event), freezeMonth, 'months')
    this.newFreezeForm.patchValue({
      ToDate: { date: Extension.GetFormatteredDate(endDate) },
      FreezeDays: moment(endDate).diff(moment(this.manipulatedate(event)), 'days')
    });

    if (this.isExtendedVal) {
      this.contract.ContractEndDate = this.dateAdd(this.contract.ContractEndDate, freezeMonth, 'months')
      this.newFreezeForm.patchValue({
        ContractEndDate: { date: Extension.GetFormatteredDate(this.contract.ContractEndDate) }
      });
    }
    // this.updateOrders(this.manipulatedate(event));
    }
  }

  setHighlight() {
    const nextChronoId = this.getNextChronoDateId();
    // const row = this.gridApi.getDisplayedRowAtIndex(nextChronoId);
    this.currentHighlightedRow = this.gridApi.getDisplayedRowAtIndex(nextChronoId);
    this.isNextDueDateHighlight = true;
    this.gridApi.redrawRows({
      rowNodes: [this.currentHighlightedRow]
    });
  }

  removeHighlight() {
    this.isNextDueDateHighlight = false;
    this.gridApi.redrawRows({
      rowNodes: [this.currentHighlightedRow]
    })
  }

  getNextChronoDateId() {
    let feasibleId: number;
    let isContinueCheckOrderList = true;
    const fromDate: Date = this.newFreezeForm.value.FromDate.date;
    const fromDateAsMoment = moment(fromDate).subtract(1, 'month');  /* As Date objects are 0 based, must subtract 1 month. */
    let isOneOrderHasFeasibleDuedate = false; // Ensure that atleast one orders date is lower than fromDate. If not, no need to run search
    for (let index = 0; index < this.items.length; index++) {
      const elem = this.items[index];
      if (fromDateAsMoment.isBefore(moment(elem.TrainingPeriodStart))) {
        isOneOrderHasFeasibleDuedate = true;
        break;
      }
    }
    if (!isOneOrderHasFeasibleDuedate) {
      return null;
    } else {
    // Minimum one feasible order exists.
      while (isContinueCheckOrderList) {
        this.items.forEach(element => {
        const dateFromList = moment(element.TrainingPeriodStart);
        if (fromDateAsMoment.isSame(dateFromList, 'day')) {
          feasibleId = element.Id;
          isContinueCheckOrderList = false;
          element.isHighlight = true; // alternative version
        } else {
          element.isHighlight = false;
        }
        });
      if (isContinueCheckOrderList) {
        fromDateAsMoment.add(1, 'days');
        } else {
          return feasibleId;
        }
      }
    }
  }

  onFreezeMonthsChanged(value) {
    // this.selectedItem = this.contract;
    /* Do nothing if freezeMonth or ShiftOrder is empty */
    if (this.isFreezeMonthsOrShiftOrderEmpty() && this.editType === 'NEW') {
      this.removeFreezelight();
    }
    if (!isNaN(value)) {
      if (this.newFreezeForm.value.FromDate) {
        const fromDate = this.newFreezeForm.value.FromDate;
        const formatedFromDate = this.manipulatedate(fromDate);
        /* If FromDate is the first of the month, subtract one day. E.g: 2019-08-01 --> 2019-07-31 */
        const endDate = (Number(fromDate.date.day) === 1) ? this.dateAddFirstOfPrevMonth(formatedFromDate, value, 'months') : this.dateAdd(formatedFromDate, value, 'months');
        this.newFreezeForm.patchValue({
          ToDate: {
            date: Extension.GetFormatteredDate(endDate)
          },
          FreezeDays: moment(endDate).diff(moment(this.manipulatedate(fromDate)), 'days')
        });
        if (this.isExtendedVal) {
          if (this.editType === 'NEW') {
            this.contract.ContractEndDate = this.dateAdd(this.originalEnddate, value, 'months')
          } else {
            const monthsDiff = value - this.originalFreezeMonths;
            this.contract.ContractEndDate = this.dateAdd(this.originalEnddate, monthsDiff, 'months')
          }

          this.newFreezeForm.patchValue({
            ContractEndDate: {
              date: Extension.GetFormatteredDate(this.contract.ContractEndDate) }
          });
        }
        // this.updateOrders(this.manipulatedate(this.newFreezeForm.value.FromDate));
      }
    }
  }

/*   updateOrders(startDate: any) {
    this.ShiftStartInstallmentId = -1;
    for (let element of this.items) {
      if (moment(element.TrainingPeriodStart).isSameOrAfter(moment(startDate))) {
        element.isFreeze = true;
        if (this.ShiftStartInstallmentId < 0) {
          this.ShiftStartInstallmentId = element.TableId;
        }
      } else {
        element.isFreeze = false;
      }
    };
  } */

  removeNewPeriodData (element: any) {
    element.NewTrainingPeriodStart = '';
    element.NewTrainingPeriodEnd = '';
    element.NewInstallmentDueDate = '';
  }

  OnShiftOrderschanged(value: any) {
    value = value == null ? 0 : Number(value);
    if (this.isExtendedVal) {
      if (!isNaN(value)) {
        this.newInsList = [];
        if (this.editType === 'NEW') {
          this.items.forEach(element => {
            if (element.isFreeze) {
              element.NewTrainingPeriodStart = this.getPrepareDate(element.TrainingPeriodStart, value); // this.dateAdd(element.TrainingPeriodStart, value, 'months');
              element.NewTrainingPeriodEnd = this.getPrepareDate(element.TrainingPeriodEnd, value); // this.dateAdd(element.TrainingPeriodEnd, value, 'months');
              element.NewInstallmentDueDate = this.getNextDueDate(element.DueDate, value, 'months');
              this.newInsList.push(element);
            } else {
              this.removeNewPeriodData(element);
            }
          });
        } else {
          const diff = value - this.originalShiftCount;
          this.items.forEach(element => {
            if (element.isFreeze) {
              element.NewTrainingPeriodStart = this.getPrepareDate(element.TrainingPeriodStart, diff); // this.dateAdd(element.TrainingPeriodStart, value, 'months');
              element.NewTrainingPeriodEnd = this.getPrepareDate(element.TrainingPeriodEnd, diff); // this.dateAdd(element.TrainingPeriodEnd, value, 'months');
              element.NewInstallmentDueDate = this.getNextDueDate(element.DueDate, diff, 'months');
              this.newInsList.push(element);
            } else {
              this.removeNewPeriodData(element);
            }
          });
        }
      }
      // Refresh rows in ag-grid. use refreshCells as style is not directly changed
      const refreshRows = [];
      this.newInsList.forEach( element => {
        const id = element.Id;
        const row = this.gridApi.getDisplayedRowAtIndex(id); // ag-rows are 0 indexed.
        refreshRows.push(row)
      });
      this.gridApi.refreshCells({
        rowNodes: refreshRows
      });
    }
  }

  pad(num: number, size: number): string {
    let s = num + '';
    while (s.length < size) {
      s = '0' + s;
    }
    return s;
  }

  getNextDueDate(date, delta, category) {
    const preProsDate = this.dateAdd(date, delta, category);
    const q = Extension.GetFormatteredDate(preProsDate);
    q.month = (q.month < 10 ) ? this.pad(q.month, 2) : q.month;
    q.day = (q.day < 10 ) ? this.pad(q.day, 2) : q.day;
    const returnDate = q.year + '-' + q.month + '-' + q.day + 'T' + '12:00:00';
    return returnDate
  }

  setNoonTransform(date) {
    const q = Extension.GetFormatteredDate(date);
    q.month = (q.month < 10 ) ? this.pad(q.month, 2) : q.month;
    q.day = (q.day < 10 ) ? this.pad(q.day, 2) : q.day;
    return (q.year + '-' + q.month + '-' + q.day + 'T' + '12:00:00');
  }

  getPrepareDate(dateF: any, count: number) {
    const q: any = this.prepareDate(dateF, count);
    q.month = (q.month < 10 ) ? this.pad(q.month, 2) : q.month;
    q.day = (q.day < 10 ) ? this.pad(q.day, 2) : q.day;
    const returnDate = q.year + '-' + q.month + '-' + q.day + 'T' + '12:00:00';
    return returnDate;
  }

  prepareDate(dateF: any, count: any) {
    const date = moment(dateF);
    const noOfDaysInMonth = Extension.getNumberOfDaysInMonth(date.year(), date.month());
    let newDate: Date = this.dateAdd(date, count, 'months');
    if (noOfDaysInMonth === date.date()) {
      const noOfDaysInNextMonth = Extension.getNumberOfDaysInMonth(moment(newDate).year(), moment(newDate).month());
      newDate = new Date(moment(newDate).toDate().getFullYear(), moment(newDate).toDate().getMonth(), noOfDaysInNextMonth);
    }
    const returnDate = Extension.GetFormatteredDate(newDate);
    return returnDate;
  }

  removeFreezelight () {
    // Remove freeze selection from view
    const tempFreezeItemsForRemoval = [];
    this.newInsList.forEach(element => {
      element.isFreeze = false;
      this.removeNewPeriodData(element);
      const row = this.gridApi.getDisplayedRowAtIndex(element.Id)
      tempFreezeItemsForRemoval.push(row);
    });
    this.gridApi.redrawRows({
      rowNodes: tempFreezeItemsForRemoval
    });
    // Reinitialize freezeItems
    this.newInsList = [];
  }

  isFreezeMonthsOrShiftOrderEmpty() {
    if (!this.newFreezeForm.value.FreezeMonths || !this.newFreezeForm.value.FromDate  ) {
      return true;
    }
    return false;
  }

  onCellClicked(order: any) {
    // Do nothing if freezeMonths and fromDate is empty
    if (this.isFreezeMonthsOrShiftOrderEmpty()) {
      return
    }
    // Disable highlighting of freezemonth if field contains valid data.
    if (this.newFreezeForm.value.FreezeMonths >= 1) {
      this.isNextDueDateHighlight = false;
    }
    if (this.editType === 'NEW' || this.editType === 'EXTEND') {
      const addFreezeRows = [];
      const removeFreezeRows = [];
      this.newInsList = [];
      this.items.forEach(element => {
        if (element.TableId >= order.data.TableId) {
          element.isFreeze = true;
          const row = this.gridApi.getDisplayedRowAtIndex(element.Id);
          addFreezeRows.push(row)
          this.newInsList.push(element);
        } else {
          element.isFreeze = false;
          const row = this.gridApi.getDisplayedRowAtIndex(element.Id);
          removeFreezeRows.push(row);
        }
      });
      const shiftCount = this.newFreezeForm.value.ShiftCount == null ? 0 : this.newFreezeForm.value.ShiftCount;
      this.OnShiftOrderschanged(shiftCount);
      this.isNextDueDateHighlight = false;
      if ((addFreezeRows.length !== 0)) {
        // Must use redrawRows, as refreshCells do not update styling.
        this.gridApi.redrawRows({
          rowNodes: addFreezeRows
        });
      }
      if (removeFreezeRows.length !== 0) {
        this.gridApi.redrawRows({
          rowNodes: removeFreezeRows
        })
      }
    }
  }

  cancel() {
    this.canceled.emit();
  }

  ngOnDestroy() {
    if (this.modalReference) {
      this.modalReference.close();
    }

    this.destroy$.next();
    this.destroy$.complete();
  }

  submitForm(channel) {
    this.formSubmited = true;
    if (this.newFreezeForm.valid) {
      this.saveItem = this.newFreezeForm.value;
      this.openSelectChannelModal(channel);
      this.formSubmited = false;
    } else {
      UsErrorService.validateAllFormFields(this.newFreezeForm, this.formErrors, this.validationMessages);
    }
  }

  openSelectChannelModal(modal) {
    this.modalReference = this.modalService.open(modal, { width: '300' });
  }

  saveContract(channel: string) {
    let senderDes: string;
    const freezeInstallments: any[] = [];
    if (channel === 'SMS') {
      senderDes = this.selectedMember.Mobile
    } else if (channel === 'EMAIL') {
      senderDes = this.selectedMember.Email
    } else {
      senderDes = ''
    }

    if (this.editType === 'NEW') {
      this.newInsList.forEach(element => {
        const freezeIns = {
          InstallmentId: element.TableId,
          InstallmentDueDate: element.DueDate,
          NewInstallmentDueDate: element.NewInstallmentDueDate,
          TrainingPeriodStart: element.NewTrainingPeriodStart,
          TrainingPeriodEnd: element.NewTrainingPeriodEnd,
          InstallmentAmount: element.Amount,
          ExtendTrainingPeriodDays: this.isExtendedVal === true ? this.newFreezeForm.value.FreezeDays : 0,
          EstimatedInvoiceDate: element.MemberCreditPeriod > 0 ? this.dateAdd(element.NewInstallmentDueDate,
            (element.MemberCreditPeriod * -1), 'days') : this.dateAdd(element.NewInstallmentDueDate, (element.CreditPeriod * -1), 'days')
        };
        freezeInstallments.push(freezeIns);
      });

      const freezeDetails = {
        freezeItem: {
          NotifyMethod: NotificationMethodType[channel],
          MemberId: this.contract.MemberId,
          MemberContractid: this.contract.Id,
          CategoryId: this.theDankestCategoryId.value,
          FromDate: this.manipulatedate(this.newFreezeForm.value.FromDate),
          ToDate: this.manipulatedate(this.newFreezeForm.value.ToDate),
          Note: this.newFreezeForm.value.Note,
          FreezeMonths: this.newFreezeForm.value.FreezeMonths,
          FreezeDays: this.newFreezeForm.value.FreezeDays,
          ShiftCount: this.newFreezeForm.value.ShiftCount,
          ShiftStartInstallmentId: ((this.ShiftStartInstallmentId === 0) ? this.insItems[0].TableId : this.ShiftStartInstallmentId),
          ContractEndDate: this.manipulatedate(this.newFreezeForm.value.ContractEndDate),
          IsExtended: this.isExtendedVal,
          MemberContractNo: this.contract.MemberContractNo
        },
        freezeInstallments: freezeInstallments,
        branchId: this.selectedMember.BranchId,
        notificationTitle: '',
        notifyMethod: channel,
        senderDescription: senderDes,
        gymName: this.branch.BranchName
      };
      this.exceMemberService.SaveContractFreezeItem(freezeDetails).pipe(takeUntil(this.destroy$)).subscribe(result => {
        if (result) {
          PreloaderService.hidePreLoader();
           if (channel === 'PRINT') {
            if (result.Data === 'PDFERROR') {
              this.messageService.openMessageBox('WARNING',
                {
                  messageTitle: 'Exceline',
                  messageBody: 'MEMBERSHIP.MsgFreezeDocGenFailed',
                });
            } else {
              if (result.Data.trim() !== '') {
                this.excePdfViewerService.openPdfViewer('Exceline', result.Data);
              }
            }
          } else if (result.Data === 'NERROR') {
            this.messageService.openMessageBox('WARNING',
              {
                messageTitle: 'Exceline',
                messageBody: 'MEMBERSHIP.NotifyError',
              });
          }
          this.saveSuccess.emit();

          this.translateService.get('MEMBERSHIP.SaveSuccessful').subscribe(trans => this.message = trans);
          this.type = 'SUCCESS';
          this.isShowInfo = true;
          setTimeout(() => {
            this.isShowInfo = false;
          }, 4000);
        }
      },
        error => {
          if (error) {
            this.translateService.get('MEMBERSHIP.frzSaveFail').pipe(takeUntil(this.destroy$)).subscribe(trans =>
              this.message = trans);
            this.type = 'DANGER';
            this.isShowInfo = true;
            setTimeout(() => {
              this.isShowInfo = false;
            }, 4000);
          }
        });
    } else {
      // Extending
        this.newInsList.forEach(element => {
        const freezeIns = {
          InstallmentId: element.TableId,
          InstallmentDueDate: element.DueDate,
          NewInstallmentDueDate: element.NewInstallmentDueDate,
          TrainingPeriodStart: element.NewTrainingPeriodStart,
          TrainingPeriodEnd: element.NewTrainingPeriodEnd,
          InstallmentAmount: element.Amount,
          ExtendTrainingPeriodDays: this.isExtendedVal === true ? this.newFreezeForm.value.FreezeDays : 0,
          EstimatedInvoiceDate: element.MemberCreditPeriod > 0 ? this.dateAdd(element.NewInstallmentDueDate,
            (element.MemberCreditPeriod * -1), 'days') : this.dateAdd(element.NewInstallmentDueDate, (element.CreditPeriod * -1), 'days')
        }
        freezeInstallments.push(freezeIns);
      });

      const freezeDetails = {
        freezeItem: {
          Id: this.selectedItem.Id,
          NotifyMethod: NotificationMethodType[channel],
          MemberId: this.selectedItem.MemberId,
          MemberContractid: this.selectedItem.MemberContractid,
          CategoryId: this.theDankestCategoryId.value,
          FromDate: this.manipulatedate(this.newFreezeForm.value.FromDate),
          ToDate: this.manipulatedate(this.newFreezeForm.value.ToDate),
          Note: this.newFreezeForm.value.Note,
          FreezeMonths: this.newFreezeForm.value.FreezeMonths,
          FreezeDays: this.newFreezeForm.value.FreezeDays,
          ShiftCount: this.newFreezeForm.value.ShiftCount,
          ShiftStartInstallmentId: ((this.ShiftStartInstallmentId === 0) ? this.insItems[0].TableId : this.ShiftStartInstallmentId),
          ContractEndDate: this.manipulatedate(this.newFreezeForm.value.ContractEndDate),
          IsExtended: this.isExtendedVal,
          MemberContractNo: this.selectedItem.MemberContractNo,
          ActiveStatus: this.selectedItem.ActiveStatus
        },
        freezeInstallments: freezeInstallments,
        branchId: this.selectedMember.BranchId,
        notificationTitle: '',
        notifyMethod: channel,
        senderDescription: senderDes,
        gymName: this.branch.BranchName
      }

      this.exceMemberService.ExtendContractFreezeItem(freezeDetails).pipe(takeUntil(this.destroy$)).subscribe(result => {
        if (result) {
          PreloaderService.hidePreLoader();
          if (channel == 'PRINT') {
            if (result.Data == 'PDFERROR') {
              this.messageService.openMessageBox('WARNING',
                {
                  messageTitle: 'Exceline',
                  messageBody: 'MEMBERSHIP.MsgFreezeDocGenFailed',
                });
            } else {
              if (result.Data.trim() !== '') {
                this.excePdfViewerService.openPdfViewer('Exceline', result.Data);
              }
            }
          } else if (result.Data == 'NERROR') {
            this.messageService.openMessageBox('WARNING',
              {
                messageTitle: 'Exceline',
                messageBody: 'MEMBERSHIP.NotifyError',
              });
          }
          this.saveSuccess.emit();
          this.translateService.get('MEMBERSHIP.ChangeSuccessful').pipe(takeUntil(this.destroy$)).subscribe(trans =>
            this.message = trans);
          this.type = 'SUCCESS';
          this.isShowInfo = true;
          setTimeout(() => {
            this.isShowInfo = false;
          }, 4000);
        }
      },
        error => {
          if (error) {
            this.translateService.get('MEMBERSHIP.ChangeNotSuccessful').pipe(takeUntil(this.destroy$)).subscribe(trans =>
              this.message = trans);
            this.type = 'DANGER';
            this.isShowInfo = true;
            setTimeout(() => {
              this.isShowInfo = false;
            }, 4000);
          }
        });
    }
  }

  changeRadio(event) {
    if (event.target.value.toString() === 'true') {
      this.newFreezeForm.controls['ShiftCount'].enable();
      if (moment(this.contract.ContractEndDate).isSame(this.originalEnddate, 'day')) {
        let freezeMonth = this.newFreezeForm.value.FreezeMonths == null ? 0 : this.newFreezeForm.value.FreezeMonths;
        this.contract.ContractEndDate = this.dateAdd(this.contract.ContractEndDate, freezeMonth, 'months')
        this.newFreezeForm.patchValue({
          ContractEndDate: { date: Extension.GetFormatteredDate(this.contract.ContractEndDate) }
        });
      }
      this.isExtendedVal = true;
      this.items.forEach(element => {
        element.CannotEditAmount = true;
      });

    } else {
      this.newFreezeForm.controls['ShiftCount'].disable();
      this.isExtendedVal = false
      this.contract.ContractEndDate = this.originalEnddate;
      this.newFreezeForm.patchValue({
        IsExtended: true,
        ContractEndDate: { date: Extension.GetFormatteredDate(this.contract.ContractEndDate) }
      });

      this.items.forEach(element => {
        element.CannotEditAmount = false;
      });
    }
  }

  onAmountChanged(value: any, order: any) {
    order.Amount = value;
  }

  manipulatedate(dateInput: any) {
    if (dateInput.date != undefined) {
      let tempDate: Date = new Date(dateInput.date.year, dateInput.date.month - 1, dateInput.date.day);
      return moment(tempDate).format('YYYY-MM-DD').toString();
    }
  }

  dateAdd(startDate: any, count: any, interval: string): any {
    return moment(startDate).add(count, interval).toDate();
  }

  dateAddFirstOfPrevMonth(startDate: any, count: any, interval: string): any {
    return moment(startDate).add(count, interval).subtract(1, 'days').toDate();
  }

/*   getNumberOfDaysInMonth(year: number, month: number): number {
    return moment(year + '-' + month, 'YYYY-MM').daysInMonth();
  } */


}
