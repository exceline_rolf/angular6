import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { McShopComponent } from './mc-shop.component';

describe('McShopComponent', () => {
  let component: McShopComponent;
  let fixture: ComponentFixture<McShopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ McShopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(McShopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
