import { Component, OnInit, OnDestroy, ViewChild, ElementRef, ViewChildren, AfterViewInit   } from '@angular/core';
import { McBasicInfoService } from './../mc-basic-info/mc-basic-info.service';
import { ExceMemberService } from '../../services/exce-member.service';
import { Cookie } from 'ng2-cookies/src/services';
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsErrorService } from 'app/shared/directives/us-error/us-error.service';
import { TranslateService } from '@ngx-translate/core';
import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { ExceLoginService } from '../../../login/exce-login/exce-login.service';
import { MemberSearchType, ColumnDataType, EntitySelectionType, MemberRole } from 'app/shared/enums/us-enum';
import { IMyDpOptions } from 'app/shared/components/us-date-picker/interfaces';
import { ExceToolbarService } from '../../../common/exce-toolbar/exce-toolbar.service';
import { element } from 'protractor';
import * as _ from 'lodash';
import { Router } from '@angular/router';
import { Extension } from 'app/shared/Utills/Extensions';
import { UsDatePickerComponent } from 'app/shared/components/us-date-picker/us-date-picker.component';
import { ExcePostalcodeComponent } from 'app/shared/components/exce-postalcode/exce-postalcode.component';
import { formattedError } from '@angular/compiler';
import { takeUntil, concatMap, mergeMap } from 'rxjs/operators';
import { Subject, of } from 'rxjs';
import { ExcelineMember } from '../../models/ExcelineMember';


const now = new Date();
interface ISponsoredMember {
  AmountPerVisit: number;
  BranchId: number;
  Id: number;
  IsAmountEnable: boolean;
  IsPercentageEnable: boolean;
  LevelId: number;
  LevelName: string;
  MinVisits: number;
  MonthlyAmount: number;
  Name: string;
  SponsorId: number;
  SponsoringPercentage: number;
  IsDefaultCategory: boolean;
}
@Component({
  selector: 'app-mc-sponsoring',
  templateUrl: './mc-sponsoring.component.html',
  styleUrls: ['./mc-sponsoring.component.scss']
})

export class McSponsoringComponent implements OnInit, OnDestroy, AfterViewInit {
  private destroy$ = new Subject<void>();
  locale: string;
  DuplicateMsgBoxModal: any;
  // selectedMembersList?: any[] = [];
  enrollmentFeePayer: any;
  msgBoxModalDeleteDiscount: any;
  msgBoxModalDeleteCategory: any;
  msgBoxModalDeleteMember: any;
  deletedDisCategoryId: any;
  deletedDisCategory: any;
  IsAddDiscountCategoryVisible: boolean;
  deletedMemberId: any;
  deletedMember: any;
  deletedCatId: any;
  employeeCategoryList?: any[] = [];
  selectedCategory: any;
  selectDisCatModalRef: any;
  branchId: number;
  deletedCat: any;
  public isSponsor = false;
  private sponsorId = 0;
  sponsorLevelList: ISponsoredMember[] = [];
  sponsorDiscountList?: any[] = [];
  discountCategoryList?: any[] = [];
  private auotocompleteItems = [{ id: 'NAME', name: 'NAME :', isNumber: false }, { id: 'ID', name: 'Id :', isNumber: true }]
  sponsorSetting: any;
  private disCategoryName: any;
  private disCategory: any;
  sponsoredMemberList: any[] = [];
  private isEnrolFeeNone: any;
  private isEnrolFeeSponsor: any;
  private isEnrolFeeMember: any;
  private members = [];
  private itemCount: number;
  private entitySelectionViewTitle: string;
  private entitySelectionType: string;
  private entitySearchType: string;
  private newMemberRegisterView: any;
  private columns: any;
  private defaultRole = 'ALL';
  private spMembersModelRef: any;
  public sponsorSettingForm: FormGroup;
  public sponsoredMembersForm: FormGroup;
  sponsorDiscountListContainer: any[];
  sponsorLevelListContainer: any[];
  // sponosorDiscountListContainer: any[];
  // sponosorLevelListContainer: any[];
  public changeMade = false;
  public inputFormSubmitted = false;
  isShowInfo = false;
  type: string;
  message: string;
  columnsToShowExceMemberlist = ['CustId', 'LastName', 'FirstName', 'Age', 'Mobile'];

  @ViewChild('content') content: ElementRef;
  @ViewChild('SponsorDueDay') SponsorDueDay: ElementRef;
  @ViewChild('InvoiceText') InvoiceText: ElementRef;
  @ViewChild('OrderRegardless') OrderRegardless: ElementRef;
  @ViewChild('IsEmailInvoice') IsEmailInvoice: ElementRef;
  @ViewChild('LastSponsoredMembers') LastSponsoredMembers: ElementRef;
  @ViewChild('SponsoredMembers') SponsoredMembers: ElementRef;
  @ViewChild('Select') Select: ElementRef;
  @ViewChild('Add') Add: ElementRef;
  @ViewChild('minVisits') minVisits: ElementRef;
  @ViewChild('SponsorCode') SponsorCode: ElementRef;
  @ViewChild('isDisplayVisitNo') isDisplayVisitNo: ElementRef;
  @ViewChild('isDisplayRemain') isDisplayRemain: ElementRef;
  @ViewChild('invoicingPeriod') invoicingPeriod: ElementRef;
  @ViewChild('enrollmentFee') enrollmentFee: ElementRef;
  @ViewChild('enrollmentFee2') enrollmentFee2: ElementRef;
  @ViewChild('enrollmentFee3') enrollmentFee3: ElementRef;
  @ViewChild('addSponsorLevel') addSponsorLevelBtn: ElementRef;
  @ViewChild('selectSponsoredMembersBtn') selectSponsoredMembersBtn: ElementRef;

  formErrors = {
    'startDate' : '',
    'endDate' : '',
    'sponsorDueDay' : ''
  };
  validationMessages = {
    'startDate': {
      'required': 'MEMBERSHIP.StartDateReq'
    },
    'endDate' : {
      'required': 'MEMBERSHIP.EndDateReq'
    },
    'sponsorDueDay': {
      'required': 'MEMBERSHIP.SponsorDueDayReq',
      'pattern': 'MEMBERSHIP.SponsorDueDayPattern'
    }
  }

  startDatePickerOptions: IMyDpOptions = {
    // disableSince: { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() + 1 }
  };

  endDatePickerOptions: IMyDpOptions = {
    disableUntil: { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() - 1 }
  };
  tempForm: FormGroup;
  selectedMember: ExcelineMember;


  constructor(
    private basicInfoService: McBasicInfoService,
    private modalService: UsbModal,
    private translateService: TranslateService,
    private router: Router,
    private exceMemberService: ExceMemberService,
    private fb: FormBuilder,
    private _exceMessageService: ExceMessageService,
    private exceLoginService: ExceLoginService,
    private toolbarService: ExceToolbarService
  ) {

    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this._exceMessageService.yesCalledHandle.pipe(
      takeUntil(this.destroy$)
    ).subscribe((value) => {
      if (value.id === 'DELETE_CATEGORY') {
        this.categoryDelYesHandler();
      }
      if (value.id === 'DELETE_MEMBER') {
        this.memberDelYesHandler();
      }
      if (value.id === 'DELETE_DISCOUNT') {
        this.discountDelYesHandler();
      }
    }, error => {}, () => console.error('0'));
  }

  ngOnInit() {
    const language = this.toolbarService.getSelectedLanguage();
    this.locale = language.culture;
    // this._dateFormat = 'DD.MM.YYYY';

    this.toolbarService.langUpdated.pipe(takeUntil(this.destroy$)).subscribe(
      (lang) => {
        if (lang) {
          this.locale = lang.culture;
        }
      }, null, null);

    this.sponsorSettingForm = this.fb.group({
      'startDate': [],
      'endDate': [],
      'sponsorDueDay': [null, [Validators.required, Validators.pattern('^[0-9]+?$')]],
      'invoiceText': [],
      'isSponsorWithoutVist': [],
      'isEmailInvoice': [],
      'lastSponsoredMembersCount': [],
      'sponsoredMembersCount': [],
      'minVisits': [],
      'minAmountAfterDiscount': [],
      'pinCode': [],
      'invoicingPeriod': [],
      'isDisplayVisitNo': [],
      'isDisplayRemain': [],
      'discountCategoryName': [],
      'enrollmentFee': [this.enrollmentFeePayer]
    });

    this.basicInfoService.currentMember.pipe(
      concatMap(currentMember => {
        this.selectedMember = currentMember;
        this.sponsorId = this.selectedMember.Id;
        return this.exceMemberService.getSponsorEmployeeCategoryList(this.sponsorId, this.selectedMember.BranchId);
      }),
      mergeMap(sponsorEmployeeCategoryList => {
        if (sponsorEmployeeCategoryList) {
          this.sponsorLevelList = sponsorEmployeeCategoryList.Data;
        }
        return this.exceMemberService.getSponsorSetting(this.branchId, this.sponsorId);
      }),
      mergeMap(sponsorSetting => {
        this.sponsorSetting = sponsorSetting.Data;
        this.isSponsor = sponsorSetting.Data.IsSponsor;
        this.disableAll(this.isSponsor);
        if (!this.sponsorSetting.DiscountTypeName) {
          this.IsAddDiscountCategoryVisible = true;
        }
        this.enrollmentFeePayer = this.sponsorSetting.EnrollmentFeePayer;
        this.sponsoredMemberList = this.sponsorSetting.SponsoredMembers;
        this.sponsoredMemberList.forEach(spMember => {
          spMember.StartDate = new Date(spMember.StartDate);
          spMember.StartDate = { year: spMember.StartDate.getFullYear(), month: (spMember.StartDate.getMonth() + 1), day: spMember.StartDate.getDate() }
          spMember.EndDate = new Date(spMember.EndDate);
          if (spMember.EndDate > '19000101') {
            spMember.EndDate = { year: spMember.EndDate.getFullYear(), month: (spMember.EndDate.getMonth() + 1), day: spMember.EndDate.getDate() }
          } else {
            spMember.EndDate = '1900-01-01T00:00:00';
          }
        });
        return this.exceMemberService.getCategories('GROUPDISCOUNT');
      }),
      mergeMap(category => {
        if (category) {
          this.discountCategoryList = category.Data;
          this.disCategory = (this.discountCategoryList.filter(data => data.Id === this.sponsorSetting.DiscountTypeId));
          if (this.disCategory.length > 0) {
            this.disCategoryName = this.disCategory[0].Name;
          }
        }
        this.patchSponsorSetting(this.sponsorSetting);
        return this.exceMemberService.GetGroupDiscountByType(this.branchId, this.sponsorSetting.DiscountTypeId, this.sponsorId);
      }),
      takeUntil(this.destroy$)
    ).subscribe(res => {
      this.changeMade = false;
    }, null, null);

    // this.basicInfoService.currentMember.concatMap(currentMember => {
    //   this.selectedMember = currentMember;
    //   this.sponsorId = this.selectedMember.Id;
    //   return this.exceMemberService.getSponsorEmployeeCategoryList(this.sponsorId, this.selectedMember.BranchId)
    // }).mergeMap(sponsorEmployeeCategoryList  => {
    //     if (sponsorEmployeeCategoryList) {
    //       this.sponsorLevelList = sponsorEmployeeCategoryList.Data;
    //     }
    //     return this.exceMemberService.getSponsorSetting(this.branchId, this.sponsorId)
    //   }).mergeMap(sponsorSetting => {
    //     this.sponsorSetting = sponsorSetting.Data;
    //     this.isSponsor = sponsorSetting.Data.IsSponsor;
    //     this.disableAll(this.isSponsor);
    //     if (!this.sponsorSetting.DiscountTypeName) {
    //       this.IsAddDiscountCategoryVisible = true;
    //     }
    //     this.enrollmentFeePayer = this.sponsorSetting.EnrollmentFeePayer;
    //     this.sponsoredMemberList = this.sponsorSetting.SponsoredMembers;
    //     this.sponsoredMemberList.forEach(spMember => {
    //       spMember.StartDate = new Date(spMember.StartDate);
    //       spMember.StartDate = { year: spMember.StartDate.getFullYear(), month: (spMember.StartDate.getMonth() + 1), day: spMember.StartDate.getDate() }
    //       spMember.EndDate = new Date(spMember.EndDate);
    //       if (spMember.EndDate > '19000101') {
    //         spMember.EndDate = { year: spMember.EndDate.getFullYear(), month: (spMember.EndDate.getMonth() + 1), day: spMember.EndDate.getDate() }
    //       } else {
    //         spMember.EndDate = '1900-01-01T00:00:00';
    //       }
    //     });
    //     return this.exceMemberService.getCategories('GROUPDISCOUNT')
    //     }).mergeMap(category => {
    //       if (category) {
    //         this.discountCategoryList = category.Data;
    //         this.disCategory = (this.discountCategoryList.filter(data => data.Id === this.sponsorSetting.DiscountTypeId));
    //         if (this.disCategory.length > 0) {
    //           this.disCategoryName = this.disCategory[0].Name;
    //         }
    //       }
    //       this.patchSponsorSetting(this.sponsorSetting);
    //       return this.exceMemberService.GetGroupDiscountByType(this.branchId, this.sponsorSetting.DiscountTypeId, this.sponsorId)
    //     }).pipe(
    //       takeUntil(this.destroy$)
    //     ).subscribe(res => {
    //       this.changeMade = false;
    //     }, null, null);

      /* this.getDiscountcategories();
      if (this.sponsorSetting.DiscountTypeId > 0) {
        this.GetGroupDiscountByType(this.sponsorSetting.DiscountTypeId);
    this.basicInfoService.currentMember.pipe(takeUntil(this.destroy$)).subscribe(
      member => {
        if (member) {
          this.sponsorId = member.Id;
          this.selectedMember = member;
          this.getSponsorEmployeeCategoryList(this.sponsorId, member.BranchId);
          this.exceMemberService.getSponsorSetting(this.branchId, this.sponsorId).pipe(takeUntil(this.destroy$)).subscribe(result => {
            if (result) {
              this.sponsorSetting = result.Data;
              this.isSponsor = result.Data.IsSponsor;
              this.disableAll(this.isSponsor);
              if (!this.sponsorSetting.DiscountTypeName) {
                this.IsAddDiscountCategoryVisible = true;
              }
              this.enrollmentFeePayer = this.sponsorSetting.EnrollmentFeePayer;
              this.sponsoredMemberList = this.sponsorSetting.SponsoredMembers;
              this.sponsoredMemberList.forEach(spMember => {
                spMember.StartDate = new Date(spMember.StartDate);
                spMember.StartDate = { year: spMember.StartDate.getFullYear(), month: (spMember.StartDate.getMonth() + 1), day: spMember.StartDate.getDate() }
                spMember.EndDate = new Date(spMember.EndDate);
                if (spMember.EndDate > '19000101') {
                  spMember.EndDate = { year: spMember.EndDate.getFullYear(), month: (spMember.EndDate.getMonth() + 1), day: spMember.EndDate.getDate() }
                } else {
                  spMember.EndDate = '1900-01-01T00:00:00';
                }
                // Matches the sponsoredLevelId to the list of sponsorCategorys
                spMember.SponsoredLevel = this.sponsorLevelList.find(cat => cat.Id === spMember.SponsoredLevelId);
              });
              this.getDiscountcategories();
              if (this.sponsorSetting.DiscountTypeId > 0) {
                this.GetGroupDiscountByType(this.sponsorSetting.DiscountTypeId);
              }
            }
            this.changeMade = false;
          }, error => {}, () => console.error('2'));
        }
      }, error => {}, () => console.error('3')); */
  }

  ngAfterViewInit() {
    this.sponsorSettingForm.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(
      val => {
        if (val && this.isSponsor) {
          this.tempForm = this.sponsorSettingForm;
        }
      }, null, null)
  };

  backToInfoWithNotes() {
    const parentRoute = '/membership/card/' + this.selectedMember.Id + '/' + this.selectedMember.BranchId + '/' + MemberRole[this.selectedMember.Role];
    this.router.navigate([parentRoute]);
  }

  cancelChanges() {
    this.changeMade = false;
    this.isSponsor = this.selectedMember.IsSponsor;
    this.sponsorId = this.selectedMember.Id;
    this.disableAll(this.isSponsor);

    this.exceMemberService.getSponsorEmployeeCategoryList(this.sponsorId, this.selectedMember.BranchId).pipe(
      mergeMap(res => this.exceMemberService.getSponsorSetting(this.branchId, this.sponsorId)),
      mergeMap(sponsorSetting => {
        this.sponsorSetting = sponsorSetting.Data;
        this.enrollmentFeePayer = this.sponsorSetting.EnrollmentFeePayer;
        this.sponsoredMemberList = this.sponsorSetting.SponsoredMembers;
        this.sponsoredMemberList.forEach(spMember => {
          spMember.StartDate = new Date(spMember.StartDate);
          spMember.StartDate = { year: spMember.StartDate.getFullYear(), month: (spMember.StartDate.getMonth() + 1), day: spMember.StartDate.getDate() }
          spMember.EndDate = new Date(spMember.EndDate);
          if (spMember.EndDate > '19000101') {
            spMember.EndDate = { year: spMember.EndDate.getFullYear(), month: (spMember.EndDate.getMonth() + 1), day: spMember.EndDate.getDate() }
          } else {
            spMember.EndDate = '1900-01-01T00:00:00';
          }
        });
        return this.exceMemberService.getCategories('GROUPDISCOUNT');
      }),
      mergeMap(categories => {
        if (categories) {
          this.discountCategoryList = categories.Data;
          this.disCategory = (this.discountCategoryList.filter(data => data.Id === this.sponsorSetting.DiscountTypeId));
          if (this.disCategory.length > 0) {
            this.disCategoryName = this.disCategory[0].Name;
          }
          this.patchSponsorSetting(this.sponsorSetting);
        }
        if (this.sponsorSetting.DiscountTypeId > 0) {
          return this.exceMemberService.GetGroupDiscountByType(this.branchId, this.sponsorSetting.DiscountTypeId, this.sponsorId)
        } else {
          return of('DiscountTypeId < 0')
        }
      }),
      takeUntil(this.destroy$)
    ).subscribe( result => {
      if (result) {
        // this.sponsorDiscountList = [];
        const sponsorCategories = result.Data;
        sponsorCategories.forEach(cat => {
          this.sponsorDiscountList.push(cat)
        });
        // this.sponsorDiscountList.push(result.Data[0]);
      }
    });

    // this.exceMemberService.getSponsorEmployeeCategoryList(this.sponsorId, this.selectedMember.BranchId).mergeMap(res => {
    //   return this.exceMemberService.getSponsorSetting(this.branchId, this.sponsorId)
    // }).mergeMap(sponsorSetting => {
    //   this.sponsorSetting = sponsorSetting.Data;
    //   this.enrollmentFeePayer = this.sponsorSetting.EnrollmentFeePayer;
    //   this.sponsoredMemberList = this.sponsorSetting.SponsoredMembers;
    //   this.sponsoredMemberList.forEach(spMember => {
    //     spMember.StartDate = new Date(spMember.StartDate);
    //     spMember.StartDate = { year: spMember.StartDate.getFullYear(), month: (spMember.StartDate.getMonth() + 1), day: spMember.StartDate.getDate() }
    //     spMember.EndDate = new Date(spMember.EndDate);
    //     if (spMember.EndDate > '19000101') {
    //       spMember.EndDate = { year: spMember.EndDate.getFullYear(), month: (spMember.EndDate.getMonth() + 1), day: spMember.EndDate.getDate() }
    //     } else {
    //       spMember.EndDate = '1900-01-01T00:00:00';
    //     }
    //   });
    //   return this.exceMemberService.getCategories('GROUPDISCOUNT')
    // }).mergeMap(categories => {
    //     if (categories) {
    //       this.discountCategoryList = categories.Data;
    //       this.disCategory = (this.discountCategoryList.filter(data => data.Id === this.sponsorSetting.DiscountTypeId));
    //       if (this.disCategory.length > 0) {
    //         this.disCategoryName = this.disCategory[0].Name;
    //       }
    //       this.patchSponsorSetting(this.sponsorSetting);
    //     }
    //     if (this.sponsorSetting.DiscountTypeId > 0) {
    //       return this.exceMemberService.GetGroupDiscountByType(this.branchId, this.sponsorSetting.DiscountTypeId, this.sponsorId)
    //     } else {
    //       return of('DiscountTypeId < 0')
    //     }
    // }).subscribe( result => {
    //   if (result) {
    //     // this.sponsorDiscountList = [];
    //     const sponsorCategories = result.Data;
    //     sponsorCategories.forEach(cat => {
    //       this.sponsorDiscountList.push(cat)
    //     });
    //     // this.sponsorDiscountList.push(result.Data[0]);
    //   }
    // });
    //

/*     this.changeMade = false;
    this.basicInfoService.currentMember.pipe(takeUntil(this.destroy$)).subscribe(
      member => {
        if (member) {
          this.isSponsor = member.IsSponsor;
          this.sponsorId = member.Id;
          this.disableAll(this.isSponsor)
          this.getSponsorEmployeeCategoryList(this.sponsorId, member.BranchId);

          this.exceMemberService.getSponsorSetting(this.branchId, this.sponsorId).pipe(takeUntil(this.destroy$)).subscribe(result => {
            if (result) {
              this.sponsorSetting = result.Data;
              this.enrollmentFeePayer = this.sponsorSetting.EnrollmentFeePayer;
              this.sponsoredMemberList = this.sponsorSetting.SponsoredMembers;
              this.sponsoredMemberList.forEach(spMember => {
                spMember.StartDate = new Date(spMember.StartDate);
                spMember.StartDate = { year: spMember.StartDate.getFullYear(), month: (spMember.StartDate.getMonth() + 1), day: spMember.StartDate.getDate() }
                spMember.EndDate = new Date(spMember.EndDate);
                if (spMember.EndDate > '19000101') {
                  spMember.EndDate = { year: spMember.EndDate.getFullYear(), month: (spMember.EndDate.getMonth() + 1), day: spMember.EndDate.getDate() }
                } else {
                  spMember.EndDate = '1900-01-01T00:00:00';
                }
              });
              this.getDiscountcategories();
              if (this.sponsorSetting.DiscountTypeId > 0) {
                this.GetGroupDiscountByType(this.sponsorSetting.DiscountTypeId);
              }
            }
          }, error => {}, () => console.error('5'))
        }
      }, error => {}, () => console.error('6')) */
  }

  // get Employee Category
  getSponsorEmployeeCategoryList(sponsorId, branchId, ) {
    this.exceMemberService.getSponsorEmployeeCategoryList(sponsorId, branchId).pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.sponsorLevelList = result.Data;
        // this.sponsorLevelList = tempSponsorLevels.filter((sponsorLevel: ISponsoredMember) => !sponsorLevel.IsDefaultCategory);
      }
    }, null, null);
  }

  getDiscountcategories() {
    this.exceMemberService.getCategories('GROUPDISCOUNT').pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        this.discountCategoryList = result.Data;

        this.disCategory = (this.discountCategoryList.filter(data => data.Id === this.sponsorSetting.DiscountTypeId));
        if (this.disCategory.length > 0) {
          this.disCategoryName = this.disCategory[0].Name;
        }
        this.patchSponsorSetting(this.sponsorSetting);
      } else {
      }
    }, null, null);
  }

  patchSponsorSetting(sponsSetting: any) {
    const sDate = new Date(sponsSetting.StartDate);
    const eDate = new Date(sponsSetting.EndDate);
    if (this.isSponsor) {
      this.sponsorSettingForm.patchValue({
        startDate: {
          date: {
            year: sDate.getFullYear(),
            month: sDate.getMonth() + 1,
            day: sDate.getDate()
          }
        },
        endDate: {
          date: {
            year: eDate.getFullYear(),
            month: eDate.getMonth() + 1,
            day: eDate.getDate()
          }
        },
      })
    } else {
      // not adding any startdate
    }
    this.sponsorSettingForm.patchValue({
      invoiceText: sponsSetting.InvoiceText,
      sponsorDueDay: sponsSetting.SponsorDueDay,
      isSponsorWithoutVist: sponsSetting.IsSponsorWithoutVist,
      isEmailInvoice: sponsSetting.IsEmailInvoice,
      lastSponsoredMembersCount: sponsSetting.LastSponsoredMembersCount,
      sponsoredMembersCount: sponsSetting.SponsoredMembers.length,
      minVisits: sponsSetting.MinVisits,
      minAmountAfterDiscount: sponsSetting.MinAmountAfterDiscount,
      pinCode: sponsSetting.PinCode,
      invoicingPeriod: sponsSetting.InvoicingPeriod,
      isDisplayVisitNo: sponsSetting.IsDisplayVisitNo,
      isDisplayRemain: sponsSetting.IsDisplayRemain,
      discountCategoryName: this.disCategoryName,
    });
    if (this.sponsorSettingForm.value.discountCategoryName === undefined) {
      this.IsAddDiscountCategoryVisible = false;
    }
  }

  // get from date change
  startDateChange(changedStartDate: {date, epoc, formatted, jsdate}, sponsoredMemberStartDate: UsDatePickerComponent, i: number): void {
    this.endDatePickerOptions = {
      disableUntil: changedStartDate.date
    };
    sponsoredMemberStartDate.showSelector = true;
    this.sponsoredMemberList[i].StartDate = changedStartDate.date;
  /*   this.sponsoredMemberList.forEach(spMemb => {
      if (spMemb.Id === member.Id) {
        spMemb.StartDate = changedStartDate.date
      }
    }) */
  }

  endDateChange(changedEndDate: {date, epoc, formatted, jsdate}, i: number): void {
    console.log(changedEndDate)
    this.sponsoredMemberList[i].EndDate = changedEndDate.date;
    /* this.sponsoredMemberList.forEach(spMemb => {
      if (spMemb.Id === member.Id) {
        console.log('changing endDate to: including inner date: ', changedEndDate.date)
        spMemb.EndDate = changedEndDate.date;
      }
    }) */
  }

  GetGroupDiscountByType(discountTypeID: number) {
    this.exceMemberService.GetGroupDiscountByType(this.branchId, discountTypeID, this.sponsorId).pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        // this.sponsorDiscountList = [];
        const sponsorCategories = result.Data;
        sponsorCategories.forEach(cat => {
          this.sponsorDiscountList.push(cat)
        });
        // this.sponsorDiscountList.push(result.Data[0]);
      } else {
      }
    }, null, null);
  }

  selectDisCategory(content) {
    this.selectDisCatModalRef = this.modalService.open(content, { width: '450' });
  }

  // Sponsored Member Modal
  selectSponsoredMembers(content) {
    this.spMembersModelRef = this.modalService.open(content);
  }

  // remove selected sponsored Member
  deleteSelectedSponsoredMember(value) {
    this.deletedMember = value;
    this.deletedMemberId = value.Id
    let messageTitle = '';
    let messageBody = '';
    this.translateService.get('Confirm').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageTitle = tranlstedValue);
    this.translateService.get('MEMBERSHIP.MsgDeleteSponsoredMemberConfirm').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageBody = tranlstedValue);
    this.msgBoxModalDeleteMember = this._exceMessageService.openMessageBox('CONFIRM', {
        messageTitle: messageTitle,
        messageBody: messageBody,
        msgBoxId: 'DELETE_MEMBER'
      });
  }

  // delete member
  memberDelYesHandler() {
    const index: number = this.sponsoredMemberList.indexOf(this.deletedMember);
    if (index !== -1) {
      this.sponsoredMemberList.splice(index, 1);
    }
    if (this.deletedMemberId > 0) {
      this.exceMemberService.RemoveSponsorshipOfMember(this.deletedMemberId).pipe(takeUntil(this.destroy$)).subscribe(result => {
        if (result) {
          this.translateService.get('MEMBERSHIP.DeletionSuccessful').pipe(takeUntil(this.destroy$)).subscribe(trans =>
            this.message = trans);
          this.type = 'INFO';
          this.isShowInfo = true;
          setTimeout(() => {
            this.isShowInfo = false;
          }, 4000);
        }
      },
        error => {
          this.translateService.get('MEMBERSHIP.DeletionNotSuccessful').pipe(takeUntil(this.destroy$)).subscribe(trans =>
            this.message = trans);
          this.type = 'DARK';
          this.isShowInfo = true;
          setTimeout(() => {
            this.isShowInfo = false;
          }, 4000);
        });
    }
  }

  newMemberModal(content: any, searchType: any) {
    this.translateService.get('COMMON.MemberSelection').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => this.entitySelectionViewTitle = tranlstedValue);
    this.entitySearchType = searchType;
    this.defaultRole = 'ALL';
    this.members = [];
    this.columns = [];
    this.columns = [{ property: 'FirstName', header: 'First Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
    { property: 'LastName', header: 'Last Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
    { property: 'IntCustId', header: 'Number', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
    { property: 'Age', header: 'Age', sortable: false, resizable: false, width: '150px', type: ColumnDataType.DATE },
    { property: 'Mobile', header: 'Mobile', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
    { property: 'StatusName', header: 'Status', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT },
    { property: 'BranchName', header: 'Gym Name', sortable: true, resizable: true, width: '150px', type: ColumnDataType.DEFAULT }
    ];
    this.newMemberRegisterView = this.modalService.open(content, {});
  }

  closeMemberSelectionView() {
    this.newMemberRegisterView.close();
  }

  searchDeatail(val: any) {
    this.exceMemberService.getMembers(val.branchSelected.BranchId, val.searchVal, val.statusSelected.id,
      this.entitySearchType, val.roleSelected.id, val.hit, false,
      false, ''
    ).pipe(takeUntil(this.destroy$)).subscribe(result => {
        if (result) {
          this.members = result.Data;

          if (this.sponsorSetting.SponsoredMembers.length > 0) {
            for (let i = 0; i < this.sponsorSetting.SponsoredMembers.length; i++) {
              for (let j = 0; j < result.Data.length; j++) {
                if (this.sponsorSetting.SponsoredMembers[i].MemberNo === result.Data[j].CustId) {
                  const index: number = this.members.indexOf(result.Data[j]);
                  if (index !== -1) {
                    this.members.splice(index, this.sponsorSetting.SponsoredMembers.length);
                  }
                }
              }
            }
          }
          this.itemCount = this.members.length;
        } else {
        }
      });
  }

  // Sponsored members
  selectedRowItems(selectedMembers: any): void {
    let isFoundDuplicate: boolean = false;
    const sDate = new Date();
    if (selectedMembers.length > 0) {
      selectedMembers.forEach(member => {
        if (this.sponsoredMemberList.findIndex(spMember => selectedMembers.includes(spMember.Id) !== -1)) {
        const _member = {
          'Id': -1,
          'SponsorId': this.selectedMember.Id,
          'MemberId': member.Id,
          'MemberNo': member.CustId,
          'MemberName': member.Name,
          'BranchName': member.BranchName,
          'StartDate': { year: sDate.getFullYear(), month: (sDate.getMonth() + 1), day: sDate.getDate() },
          'EndDate': { /* year: sDate.getFullYear() + 100, month: (sDate.getMonth() + 1), day: sDate.getDate() */},
          // 'EmployeeCategory': [],
          // 'Role': '',
          // 'SponsorName': '',
          'SponsoredLevel': this.sponsorLevelList[0], // Set default empty category as initial category
          // 'SponsoredLevelId': -1
          // 'SponsoredLevelName': '',

          // 'StartDate': sDate.getFullYear() + '-' + sDate.getMonth() + '-' + sDate.getDate() + 'T00:00:00',
          // // 'StartDate': { year: sDate.getFullYear(), month: (sDate.getMonth() + 1), day: sDate.getDate() },
          // 'EndDate': year + '-' + sDate.getMonth() + '-' + sDate.getDate() + 'T00:00:00',
        };
        this.sponsoredMemberList.push(_member);
      } else {
        isFoundDuplicate = true;
        // Inform that member is already in the list
        this.translateService.get('MEMBERSHIP.MemberAlreadyInList').subscribe(trans =>
          this.message = trans);
        this.type = 'INFO';
        this.isShowInfo = true;
        setTimeout(() => {
          this.isShowInfo = false;
        }, 4000);
      }
    });
    if (!isFoundDuplicate) {
      this.newMemberRegisterView.close();
    }
    }
  }

  /**
   * @description Update sponsoredMemberList when changing selector. Must have seperate ngModelChange function, as two fields needs to be updated per entry in sponsoredMemberList.
   * @param deltaSponsMemb SponsoredMember type
   * @param i Index of sponsoredMemberList
   */
  changeSPCategory(deltaSponsMemb: ISponsoredMember, i: number): void {
    this.sponsoredMemberList[i].SponsoredLevelId = deltaSponsMemb.Id;
    this.sponsoredMemberList[i].SponsoredLevel = deltaSponsMemb;
  }
  // sponsored Members ok button
  selectSPMembersOk() {
    this.changeMade = true;
    this.spMembersModelRef.close();
    this.sponsorSettingForm.patchValue({
      sponsoredMembersCount: this.sponsoredMemberList.length
    });
  }

  selectSPMembersCancel() {
    this.spMembersModelRef.close();
  }

  // Add Employee Category
  addSponsorLevelToView() {
    const sponsorLevelItem = {
      'AmountPerVisit': 0,
      'BranchId': this.sponsorSetting.BranchId,
      'Id': -1,
      'IsAmountEnable': true,
      'IsPercentageEnable': true,
      'LevelId': 0,
      'LevelName': '',
      'MinVisits': 0,
      'MonthlyAmount': 0,
      'Name': '',
      'SponsorId': this.sponsorSetting.SponsorId,
      'SponsoringPercentage': 0,
      'IsDefaultCategory': false
    };
    this.sponsorLevelList.push(sponsorLevelItem);
  }

  // Discount Category
  addSponsorDiscount() {
    this.changeMade = true;
    const customSponsorDiscount = {
      'Id': -1,
      'GroupDiscountName': '',
      'MinMemberNo': 0,
      'HighAmount': 0,
      'HighPercentage': 0,
      'LowAmount': 0,
      'LowPercentage': 0,
    };
    this.sponsorDiscountList.push(customSponsorDiscount);
  }

  disableAll(isSponsorBool) {
    this.SponsorDueDay.nativeElement.disabled = !isSponsorBool;
    this.InvoiceText.nativeElement.disabled = !isSponsorBool;
    this.OrderRegardless.nativeElement.disabled = !isSponsorBool;
    this.IsEmailInvoice.nativeElement.disabled = !isSponsorBool;
    this.LastSponsoredMembers.nativeElement.disabled = !isSponsorBool;
    // this.SponsoredMembers.nativeElement.disabled = !isSponsorBool; // Commented out to permanently disable (gray out) the 'Sponsored members' field
    this.Select.nativeElement.disabled = !isSponsorBool;
    // this.Add.nativeElement.disabled = !isSponsorBool;
    this.IsAddDiscountCategoryVisible ? this.Add.nativeElement.disabled = false : this.Add.nativeElement.disabled = true;
    this.minVisits.nativeElement.disabled = !isSponsorBool;
    this.SponsorCode.nativeElement.disabled = !isSponsorBool;
    this.isDisplayVisitNo.nativeElement.disabled = !isSponsorBool;
    this.isDisplayRemain.nativeElement.disabled = !isSponsorBool;
    this.invoicingPeriod.nativeElement.disabled = !isSponsorBool;
    this.enrollmentFee.nativeElement.disabled = !isSponsorBool;
    this.enrollmentFee2.nativeElement.disabled = !isSponsorBool;
    this.enrollmentFee3.nativeElement.disabled = !isSponsorBool;
    this.addSponsorLevelBtn.nativeElement.disabled = !isSponsorBool;
    this.selectSponsoredMembersBtn.nativeElement.disabled = !isSponsorBool;
    if (isSponsorBool) {
      this.content.nativeElement.style = 'opacity: 1;';
    } else if (!isSponsorBool) {
      this.content.nativeElement.style = 'opacity: 0.4;';
    }
  }


  isSponsorChange(checkSponsor: any) {
    this.changeMade = true;
    if (checkSponsor.target.checked) {
      this.isSponsor = true;
      this.disableAll(this.isSponsor)
      const startdate = new Date()
      this.sponsorSettingForm.patchValue({
        startDate: {
          date: {
            year: startdate.getFullYear(),
            month: startdate.getMonth() + 1,
            day: startdate.getDate()
          }
        },
        endDate: {
          date: {
            year: startdate.getFullYear() + 100,
            month: startdate.getMonth() + 1,
            day: startdate.getDate()
          }
        }
      })
      this.sponsorSettingForm.get('startDate').setValidators(Validators.required);
      this.sponsorSettingForm.get('startDate').updateValueAndValidity();
      this.sponsorSettingForm.get('endDate').setValidators(Validators.required);
      this.sponsorSettingForm.get('endDate').updateValueAndValidity();
     } else {
      this.isSponsor = false;
      this.disableAll(this.isSponsor)
      this.sponsorSettingForm.patchValue({
        startDate: {
          date: {
            year: '',
            month: '',
            day: ''
          }
        },
        endDate: {
          date: {
            year: '',
            month: '',
            day: ''
          }
        }
      })
      // this.sponosorDiscountListContainer = this.sponosorDiscountList;
      this.sponsorLevelListContainer = this.sponsorLevelList;
      this.sponsorSettingForm.get('startDate').setValidators(null);
      this.sponsorSettingForm.get('startDate').updateValueAndValidity();
      this.sponsorSettingForm.get('endDate').setValidators(null);
      this.sponsorSettingForm.get('endDate').updateValueAndValidity();
    }
  }

  deleteEmployeeCategory(cat) {
    this.deletedCat = cat;
    this.deletedCatId = cat.Id
    let messageTitle = '';
    let messageBody = '';
    (this.translateService.get('MEMBERSHIP.Confirm').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => messageTitle = translatedValue));
    (this.translateService.get('MEMBERSHIP.ConfirmDeletionOfEmployeeCategory').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => messageBody = translatedValue));
    this.msgBoxModalDeleteCategory = this._exceMessageService.openMessageBox('CONFIRM',
      {
        messageTitle: messageTitle,
        messageBody: messageBody,
        msgBoxId: 'DELETE_CATEGORY'
      });
  }

  categoryDelYesHandler() {
    const index: number = this.sponsorLevelList.indexOf(this.deletedCat);
    if (index !== -1) {
      this.sponsorLevelList.splice(index, 1);
    }
    if (this.deletedCatId > 0) {
      this.exceMemberService.DeleteEmployeeCategory(this.deletedCatId).pipe(takeUntil(this.destroy$)).subscribe(result => {
        if (result) {
        }
      },
        error => {
        });
    }
  }

  // selected Discount Category
  onSelectionChange(value) {
    this.selectedCategory = value;
    if (this.selectedCategory) {
      this.sponsorSetting.DiscountTypeId = this.selectedCategory.Id;
    }
  }

  getSelectedDiscountCategory() {
    if (this.selectedCategory) {
      this.GetGroupDiscountByType(this.selectedCategory.Id);
    }

    this.sponsorSettingForm.controls['discountCategoryName'].setValue(this.selectedCategory.Name);
    this.IsAddDiscountCategoryVisible = true;
/*     this.exceMemberService.GetGroupDiscountByType(this.selectedCategory.BranchId, this.selectedCategory.Id, this.sponsorSetting.SponsorId).pipe(takeUntil(this.destroy$)).subscribe(res => {
      if (res) {
      }
    },
      error => {
        console.log(error);
      }); */
  }

  deleteDiscountCategory(value) {
    this.changeMade = true;
    const categoryForDeleteion = value; // CFD
    categoryForDeleteion.isActive = false;
    const CFDId = categoryForDeleteion.Id;

    // Inform user that category will be deleted when SAVE is pressed
/*     this.translateService.get('MEMBERSHIP.confirmCategoryDeletion').pipe(takeUntil(this.destroy$)).subscribe(trans =>
      this.message = trans);
    this.type = 'INFO';
    this.isShowInfo = true;
    setTimeout(() => {
      this.isShowInfo = false;
    }, 4000); */

    this.exceMemberService.DeleteDiscountCategory(CFDId).pipe(
      takeUntil(this.destroy$)
      ).subscribe(disc => {
        const index: number = this.sponsorDiscountList.indexOf(categoryForDeleteion);
        if (index !== -1) {
          this.sponsorDiscountList.splice(index, 1);
        }
      }, null, null);
  }

  categorySelected(value) {
    this.changeMade = true;
    this.selectedCategory = value;
    if (this.selectedCategory) {
      this.sponsorSetting.DiscountTypeId = this.selectedCategory.Id;
      this.IsAddDiscountCategoryVisible = true;
    }
    this.getSelectedDiscountCategory();
    this.selectDisCatModalRef.close();
  }

  discountDelYesHandler() {
    this.exceMemberService.DeleteDiscountCategory(this.deletedDisCategoryId).pipe(takeUntil(this.destroy$)).subscribe(disc => {
      const index: number = this.sponsorDiscountList.indexOf(this.deletedDisCategory);
      if (index !== -1) {
        this.sponsorDiscountList.splice(index, 1);
      }
    }, null, null)
  }

  /**
   * @description Concatinates the date with a default time stamp, left zero-padding month and day if before the 10th
   * @returns Concatinated string with time stamp set to noon
   * @param date Object with the attributes: day, month, year
   */
  dateNormalizedConverter(date: { day: number, month: number, year: number}) {
    const day = (date.day < 10) ? Extension.pad(date.day, 2) : date.day;
    const month = (date.month < 10) ? Extension.pad(date.month, 2) : date.month;
    const year = date.year;
    return (year + '-' + month + '-' + day + 'T12:00:00');
  }


  // convert date js object in to string
  dateFromTimePickerConverter(rawDateObject: { date: { day: number, month: number, year: number}}) {
    // Do nothing if not sponsor
    if (!this.isSponsor) {
      return
    }
    const day = (rawDateObject.date.day < 10) ? Extension.pad(rawDateObject.date.day, 2) : rawDateObject.date.day;
    const month = (rawDateObject.date.month < 10) ? Extension.pad(rawDateObject.date.month, 2) : rawDateObject.date.month;
    const year = (rawDateObject.date.year);
    return (year + '-' + month + '-' + day + 'T12:00:00');
  }

  // This function is sort of a hack. As us-amount-input is a custom component, the problem av showing/hiding the save/reset button should be handled there.
  saveUSAmountForm(value, htmlNameElement) {
      Object.getOwnPropertyNames(this.sponsorSetting).forEach(prop => {
        const htmlNameElementCapFirst = this.capitalizeFirstLetter(htmlNameElement);
        if (prop === htmlNameElementCapFirst) {
          this.isOldAndNewSame(this.sponsorSetting[prop], value[htmlNameElement]) ? this.disableSaveAndResetBtn(false) : this.disableSaveAndResetBtn(true);
        }
      });
  }

  checkRadioButton(clickEvent, value) {;
    this.isOldAndNewSame(clickEvent, this.sponsorSetting.EnrollmentFeePayer) ? this.disableSaveAndResetBtn(false) : this.disableSaveAndResetBtn(true);
    /* //The conversion between int and string is done on the backend.
    if (this.enrollmentFeePayer === 1) {
      this.enrollmentFeePayer = 'MEMBER'
    } else if (this.enrollmentFeePayer === 2) {
      this.enrollmentFeePayer = 'SPONSOR'
    } else {
      this.enrollmentFeePayer = 'NONE'
    }
    */
    this.sponsorSettingForm.patchValue({
      enrollmentFeePayer: this.enrollmentFeePayer
    });
  }

  saveTempForm(value, event) {
    if (event !== 'SAVE') {
      // Only run this code if save button is not pressed
      const htmlNameElement = event.srcElement.attributes.formcontrolname.nodeValue;
      switch (htmlNameElement) {
        case 'invoiceText': {
          this.checkText(value, htmlNameElement);
        break;
        }
        case 'isEmailInvoice': {
        this.checkBoolBtn(value, htmlNameElement);
        break;
        }
        case 'isSponsorWithoutVist': {
          this.checkBoolBtn(value, htmlNameElement);
        break;
        }
        case 'lastSponsoredMembersCount': {
          this.checkValue(value, htmlNameElement);
        break;
        }
        case 'sponsoredMembersCount': {
          this.checkValue(value, htmlNameElement);
        break;
        }
        case 'sponsorDueDay': {
          this.checkText(value, htmlNameElement);
          break;
        }
        case 'minVisits': {
          this.checkText(value, htmlNameElement);
          break;
        }
        case 'minAmountAfterDiscount': {
          this.checkText(value, htmlNameElement);
          break;
        }
        case 'pinCode': {
          this.checkText(value, htmlNameElement);
          break;
        }
        case 'invoicingPeriod': {
          this.checkText(value, htmlNameElement);
          break;
        }
        case 'isDisplayVisitNo': {
          this.checkBoolBtn(value, htmlNameElement);
          break;
        }
        case 'isDisplayRemain': {
          this.checkBoolBtn(value, htmlNameElement);
          break;
        }
        case 'enrollmentFee': {
          // enrollmentFee (radio button) is handled in checkRadioButton, as srcElement used in htmlNameElement creation does not exists for radio button
          break;
        }
        default: {
        console.error('Default case. Unknown variable assigned to htmlNameElement')
          break;
        }
      }
      /*
    if (this.enrollmentFeePayer === 1) {
      this.enrollmentFeePayer = 'MEMBER'
    } else if (this.enrollmentFeePayer === 2) {
      this.enrollmentFeePayer = 'SPONSOR'
    } else {
      this.enrollmentFeePayer = 'NONE'
    }
    */
  } else {
      const smList = this.sponsoredMemberList.map(x => Object.assign({}, x)); // Do a deep copy
      smList.forEach( memb => {
        memb.StartDate = this.dateNormalizedConverter(memb.StartDate);
        memb.EndDate = this.dateNormalizedConverter(memb.EndDate);
      });
      this.sponsorSetting = {
        Id: this.sponsorSetting.Id,
        LastSponsoredMembersCount: value.lastSponsoredMembersCount,
        // currentMembersCount <-- pressent in SponsorSettingDC
        IsSponsor: this.isSponsor,
        StartDate: this.dateFromTimePickerConverter(value.startDate),
        EndDate: this.dateFromTimePickerConverter(value.endDate),
        SponsorDueDay: value.sponsorDueDay,
        MinAmountAfterDiscount: value.minAmountAfterDiscount,
        MinVisits: value.minVisits,
        IsDisplayVisitNo: value.isDisplayVisitNo,
        IsDisplayRemain: value.isDisplayRemain,
        // minPayment  <-- pressent in SponsorSettingDC
        InvoiceText: value.invoiceText,
        DiscountCategoryList: this.sponsorDiscountList,
        EmployeeCategoryList: this.sponsorLevelList,
        InvoicingPeriod: value.invoicingPeriod,
        PinCode: value.pinCode,
        BranchId: this.selectedMember.BranchId,
        IsSponsorWithoutVist: value.isSponsorWithoutVist,
        IsEmailInvoice: value.isEmailInvoice,
        DiscountTypeId: this.sponsorSetting.DiscountTypeId,
        // discountType <-- pressent in SponsorSettingDC
        // packageId <-- pressent in SponsorSettingDC
        // activetyId <-- pressent in SponsorSettingDC
        // isActive <-- pressent in SponsorSettingDC
        SponsorId: this.sponsorSetting.SponsorId,
        // DiscountTypeName: value.discountCategoryName, <-- pressent in SponsorSettingDC
        EnrollmentFeePayer: this.enrollmentFeePayer,
        // percentageModeId <-- pressent in SponsorSettingDC
        // amountModeId <-- pressent in SponsorSettingDC
        SponsoredMembers: smList, // Test 1
      }
    }
  }

  checkText(value, htmlNameElement) {
    Object.getOwnPropertyNames(this.sponsorSetting).forEach(prop => {
      const htmlNameElementCapFirst = this.capitalizeFirstLetter(htmlNameElement);
      if (prop === htmlNameElementCapFirst) {
        this.isOldAndNewSame(this.sponsorSetting[prop], value[htmlNameElement]) ? this.disableSaveAndResetBtn(false) : this.disableSaveAndResetBtn(true);
      }
    });
  }

  checkValue(value, htmlNameElement) {
    Object.getOwnPropertyNames(this.sponsorSetting).forEach(prop => {
      const htmlNameElementCapFirst = this.capitalizeFirstLetter(htmlNameElement);
      if (prop === htmlNameElementCapFirst) {
        this.isOldAndNewSame(this.sponsorSetting[prop], value[htmlNameElement]) ? this.disableSaveAndResetBtn(false) : this.disableSaveAndResetBtn(true);
      }
    });
  }

  capitalizeFirstLetter (string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  lowercaseFirstLetter (string) {
    return string.charAt(0).toLowerCase() + string.slice(1);
  }

  checkBoolBtn(value, htmlNameElement) {
    Object.getOwnPropertyNames(this.sponsorSetting).forEach(prop => {
      const htmlNameElementCapFirst = this.capitalizeFirstLetter(htmlNameElement);
      if (prop === htmlNameElementCapFirst) {
        this.isOldAndNewSame(this.sponsorSetting[prop], value[htmlNameElement]) ? this.disableSaveAndResetBtn(false) : this.disableSaveAndResetBtn(true);
      }
    })
  }

  disableSaveAndResetBtn(isEnabled: boolean) {
    // Only apply change if change is not already enabled
    if (!this.changeMade) {
      this.changeMade = isEnabled;
    }
  }

  saveSponsoring(value): void {
    this.changeMade = false;
    this.inputFormSubmitted = true;
    if (this.sponsorSettingForm.valid) {
      this.saveTempForm(value, 'SAVE');
      this.exceMemberService.SaveSponsorSetting(this.sponsorSetting).pipe(
        takeUntil(this.destroy$)
        ).subscribe(result => {
        if (result.Data) {
          this.changeMade = false;
          this.translateService.get('MEMBERSHIP.SaveSuccessful').pipe(takeUntil(this.destroy$)).subscribe(trans =>
            this.message = trans);
          this.type = 'SUCCESS';
          this.isShowInfo = true;
          setTimeout(() => {
            this.isShowInfo = false;
          }, 4000);
        }
      },
        error => {
          this.translateService.get('MEMBERSHIP.SaveNotSuccessful').pipe(takeUntil(this.destroy$)).subscribe(trans =>
            this.message = trans);
          this.type = 'DANGER';
          this.isShowInfo = true;
          setTimeout(() => {
            this.isShowInfo = false;
          }, 4000);
        });
    } else {
      UsErrorService.validateAllFormFields(this.sponsorSettingForm, this.formErrors, this.validationMessages);
    }
  }

  isOldAndNewSame(oldC: any,  newC: any) {
    return (oldC === newC);
  }

  /*
  setValidation() {
    if (this.isSponsor) {
      this.sponsorSettingForm.get('endDate').setValidators(Validators.required);
      this.sponsorSettingForm.get('endDate').updateValueAndValidity();
    }
    this.sponsorSettingForm.statusChanges.pipe(takeUntil(this.destroy$)).subscribe(() =>
      UsErrorService.onValueChanged(this.sponsorSettingForm, this.formErrors, this.validationMessages));
  }
  */

  levelNameChanged(value: any, level: any) {
    this.changeMade = true;
    level.LevelName = value;
  }

  onMonthlyAmountChanged(value: any, level: any) {
    this.changeMade = true;
    level.MonthlyAmount = Number(value);
    if (level.MonthlyAmount > 0) {
      level.SponsoringPercentage = 0;
    }
  }

  sponsoringPercentageChanged(value: any, level: any) {
    this.changeMade = true;
    level.SponsoringPercentage = Number(value);
    if (level.SponsoringPercentage > 0) {
      level.MonthlyAmount = 0;
    }
  }

  amountPerVisitChanged(value: any, level: any) {
    level.AmountPerVisit = Number(value);
    this.changeMade = true;
  }

  minVisitsChanged(value: any, level: any) {
    level.MinVisits = Number(value);
    this.changeMade = true;
  }

  nameChanged(value: any, discount: any) {
    discount.GroupDiscountName = value;
    this.changeMade = true;
  }

  minMemberNoChanged(value: any, discount: any) {
    discount.MinMemberNo = Number(value);
    this.changeMade = true;
  }

  highAmountChanged(value: any, discount: any) {
    discount.HighAmount = Number(value);
    this.changeMade = true;
  }

  highPercentageChanged(value: any, discount: any) {
    discount.HighPercentage = Number(value);
    this.changeMade = true;
  }

  lowAmountChanged(value: any, discount: any) {
    discount.LowAmount = Number(value);
    this.changeMade = true;
  }

  lowPercentageChanged(value: any, discount: any) {
    discount.LowPercentage = Number(value);
    this.changeMade = true;
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    if (this.msgBoxModalDeleteCategory) {
      this.msgBoxModalDeleteCategory.unsubscribe();
    }
    if (this.msgBoxModalDeleteDiscount) {
      this.msgBoxModalDeleteDiscount.unsubscribe();
    }
    if (this.msgBoxModalDeleteMember) {
      this.msgBoxModalDeleteMember.unsubscribe();
    }
    if (this.selectDisCatModalRef) {
      this.selectDisCatModalRef.close();
    }
    if (this.spMembersModelRef) {
      this.spMembersModelRef.close();
    }
    if (this.newMemberRegisterView) {
      this.newMemberRegisterView.close();
    }
  }

}
