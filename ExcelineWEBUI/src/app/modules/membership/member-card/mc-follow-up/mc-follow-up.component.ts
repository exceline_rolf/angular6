import { Component, OnInit, OnDestroy, ViewChild, EventEmitter } from '@angular/core';
import { McBasicInfoService } from './../mc-basic-info/mc-basic-info.service';
import { ExceMemberService } from '../../services/exce-member.service';
import { UsbModal } from '../../../../shared/components/us-modal/us-modal';
import { ExceMessageService } from '../../../../shared/components/exce-message/exce-message.service';
import { TranslateService } from '@ngx-translate/core';
import { ExceAddFollowUpService } from 'app/shared/components/exce-followup/exce-add-follow-up.service';
import { UsbModalRef } from 'app/shared/components/us-modal/modal-ref';
import { ExceFollowupCommonService } from 'app/shared/components/exce-followup/exce-followup-common.service';
import { Subject } from 'rxjs';
import { takeUntil, mergeMap } from 'rxjs/operators';

@Component({
  selector: 'app-mc-follow-up',
  templateUrl: './mc-follow-up.component.html',
  styleUrls: ['./mc-follow-up.component.scss']
})

export class McFollowUpComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  selectedFollowupMember: any;
  updateFixedFollowUpTemplateView: UsbModalRef;
  followUpList = [];
  followUpDetailList?: any[] = [];
  addFixedFollowUpTemplateView: any;
  addFixedFollowUpView: any;
  firstName: any;
  lastName: any;
  Email: any;
  Mobile: any;
  MobilePrefix: string;
  Name: string;
  selectedMember: any;
  followupDefinition: any[];
  @ViewChild('followUpProfile') public followUpProfile;
  @ViewChild('updateMember') public updateMember;

  columnTitle = {
    PlannedDate: 'Planlagt dato',
    Text: 'Tekst',
    AssignedEmpName: 'Tildelt til ansatt',
    RoleType: 'Rolletype',
    CompletedDate: 'Utførtdato',
    CompletedEmpName: 'Utført av',
    TaskCategoryName: 'Oppgavekategori'
  }

  rowData: any;
  gridApi: any;
  gridColumnApi: any;
  getRowHeight: (params: any) => any;
  gridIsSized = false;
  rowClassRules: any;
  rowSelection: any;
  deleteFollowUpItem: any;
  // Variables in this block are intentionally left ambiguous, as they are used for various backend confirmations.
  type: string;
  isShowInfo: boolean;
  message: string;
  // -----

  constructor(
    private basicInfoService: McBasicInfoService,
    private modalService: UsbModal,
    private exceMemberService: ExceMemberService,
    private translateService: TranslateService,
    private exceMessageService: ExceMessageService,
    private exceAddFollowUpService: ExceAddFollowUpService,
    private exceFollowupCommonService: ExceFollowupCommonService
  ) {
    this.exceMessageService.yesCalledHandle.pipe(takeUntil(this.destroy$)).subscribe((value) => {
      this.yesHandler(value);
    });
  }

  ngOnInit() {
    this.basicInfoService.currentMember.pipe(
      mergeMap(currentMember => {
        this.selectedMember = currentMember;
        this.firstName = this.selectedMember.FirstName;
        this.lastName = this.selectedMember.LastName;
        this.Email = this.selectedMember.Email;
        this.Mobile = this.selectedMember.Mobile;
        this.MobilePrefix = this.selectedMember.MobilePrefix;
        this.Name = this.selectedMember.Name;

        return this.exceMemberService.getFollowUps(this.selectedMember.Id, -1);
      }),
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        const rowData = [];
        this.followUpList = [];
        this.followUpDetailList = [];
        this.followUpList = result.Data;
        this.followUpList.forEach(followUp => {
          followUp.FollowUpDetailList.forEach(followupDet => {
            followupDet.followUpId = followUp.Id;
            // Create row object for ag-grid
            const el = {
              PlannedDate: followupDet.PlannedDate,
              Name: followupDet.Name,
              AssignedEmpName: followupDet.AssignedEmpName,
              RoleType: followupDet.RoleType,
              CompletedDate: followupDet.CompletedDate,
              CompletedEmpName: followupDet.CompletedEmpName,
              TaskCategoryName: followupDet.TaskCategoryName,
              Id: followupDet.followUpId
            }
            this.followUpDetailList.push(followupDet);
            rowData.push(el);
          });
            this.rowData = rowData;
        });
      }
    })
    // load the data to the table
    // this.basicInfoService.currentMember.mergeMap(currentMember => {
    //   this.selectedMember = currentMember;
    //   this.firstName = this.selectedMember.FirstName;
    //   this.lastName = this.selectedMember.LastName;
    //   this.Email = this.selectedMember.Email;
    //   this.Mobile = this.selectedMember.Mobile;
    //   this.MobilePrefix = this.selectedMember.MobilePrefix;
    //   this.Name = this.selectedMember.Name;

    //   return this.exceMemberService.getFollowUps(this.selectedMember.Id, -1).pipe(
    //     takeUntil(this.destroy$)
    //   )
    // }).pipe(
    //   takeUntil(this.destroy$)
    // ).subscribe(result => {
    //   if (result) {
    //     const rowData = [];
    //     this.followUpList = [];
    //     this.followUpDetailList = [];
    //     this.followUpList = result.Data;
    //     this.followUpList.forEach(followUp => {
    //       followUp.FollowUpDetailList.forEach(followupDet => {
    //         followupDet.followUpId = followUp.Id;
    //         // Create row object for ag-grid
    //         const el = {
    //           PlannedDate: followupDet.PlannedDate,
    //           Name: followupDet.Name,
    //           AssignedEmpName: followupDet.AssignedEmpName,
    //           RoleType: followupDet.RoleType,
    //           CompletedDate: followupDet.CompletedDate,
    //           CompletedEmpName: followupDet.CompletedEmpName,
    //           TaskCategoryName: followupDet.TaskCategoryName,
    //           Id: followupDet.followUpId
    //         }
    //         this.followUpDetailList.push(followupDet);
    //         rowData.push(el);
    //       });
    //         this.rowData = rowData;
    //     });
    //   }
    // }, error => {
    //   console.log(error)
    // });

/*     this.basicInfoService.currentMember.pipe(takeUntil(this.destroy$)).subscribe(
      member => {
        if (member) {
          this.selectedMember = member;
          this.firstName = this.selectedMember.FirstName;
          this.lastName = this.selectedMember.LastName;
          this.Email = this.selectedMember.Email;
          this.Mobile = this.selectedMember.Mobile;
          this.MobilePrefix = this.selectedMember.MobilePrefix;
          this.Name = this.selectedMember.Name;
          this.loadFollowUps(this.selectedMember)
        }
      },
      error => {
        console.log(error);
      }
    ); */

    this.followupDefinition = [{
      headerName: this.columnTitle.PlannedDate,
      field: 'PlannedDate',
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true
      }, filterParams: {
          newRowsAction: 'keep'
        }, cellStyle: {
          'cursor': 'pointer'
        }, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      },
      suppressMovable: true,
      autoHeight: true,
      cellRenderer: function(params) {
        if (params.value) {
          const d = params.value.split('T');
          const dDate = d[0].split('-');
          return dDate[2] + '.' + dDate[1] + '.' + dDate[0];
        } else {
          return
        }
      }
    }, {
      headerName: this.columnTitle.Text,
      field: 'Name',
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true
      }, filterParams: {
          newRowsAction: 'keep'
      }, cellStyle: {
          'cursor': 'pointer'
      }, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
    },
    suppressMovable: true,
    autoHeight: true,
    cellRenderer: null
  }, {
      headerName: this.columnTitle.AssignedEmpName,
      field: 'AssignedEmpName',
      suppressMenu: true,
      suppressSorting: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true
      }, filterParams: {
          newRowsAction: 'keep'
      }, cellStyle: {
          'cursor': 'pointer',
          'textAlign': 'center'
        }, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      },
      suppressMovable: true,
      autoHeight: true
  },
    {
      headerName: this.columnTitle.RoleType,
      field: 'RoleType',
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true},
      filterParams: {
        newRowsAction: 'keep'
      }, cellStyle: {
        'cursor': 'pointer'
      }, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      },
      suppressMovable: true,
      autoHeight: true,
      cellRenderer: null
  },
    {
      headerName: this.columnTitle.CompletedDate,
      field: 'CompletedDate',
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true
      },
      filterParams: {
        newRowsAction: 'keep'
      },
        cellStyle: {
          'cursor': 'pointer'
      }, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
    },
    suppressMovable: true,
    autoHeight: true,
    cellRenderer: function(params) {
      if (params.value) {
        const d = params.value.split('T');
        const dDate = d[0].split('-');
        return dDate[2] + '.' + dDate[1] + '.' + dDate[0];
      } else {
        return
      }
    }
  },
    {
      headerName: this.columnTitle.CompletedEmpName,
      field: 'CompletedEmpName',
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true
      },
      filterParams: {
        newRowsAction: 'keep'
      }, cellStyle: {
        'cursor': 'pointer'
      }, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
    },
    suppressMovable: true,
    autoHeight: true,
    cellRenderer: null
  },
    {
      headerName: this.columnTitle.TaskCategoryName,
      field: 'TaskCategoryName',
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true
      },
      filterParams: {
        newRowsAction: 'keep'
      }, cellStyle: {
        'cursor': 'pointer'
      }, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      },
      suppressMovable: true,
      autoHeight: true,
      cellRenderer: null
    },
    {headerName: '', field: 'Delete', suppressMenu: true, width: 40, floatingFilterComponentParams: {suppressFilterButton: true},
    filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'center'}, icons: {
      sortAscending: '<i class="icon-sort-ascending"/>',
      sortDescending: '<i class="icon-sort-descending"/>'
    }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
      return  '<button class="btn btn-danger btn-icon btn-icon-min" title="Slett"><span class="icon-cancel"></span>'
      }
    }
  ];
    this.getRowHeight = function(params) {
      return 30;
    };
  }

  // to delete followup item from the database
  deleteFollowUp(item) {
    let msgType = '';
    let msg = '';
    this.translateService.get('COMMON.Confirm').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => msgType = translatedValue);
    this.translateService.get('ADMIN.ConfirmMessage').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => msg = translatedValue);
    this.exceMessageService.openMessageBox('CONFIRM', {
        messageTitle: msgType,
        messageBody: msg,
        msgBoxId: 'Delete_Followup',
        optionalData: item
      }
    );
  }

  loadFollowUps(member) {
    this.exceMemberService.getFollowUps(member.Id, -1).pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        const rowData = [];
        this.followUpList = [];
        this.followUpDetailList = [];
        this.followUpList = result.Data;
        this.followUpList.forEach(followUp => {
          followUp.FollowUpDetailList.forEach(followupDet => {
            followupDet.followUpId = followUp.Id;
            // Create row object for ag-grid
            const el = {
              PlannedDate: followupDet.PlannedDate,
              Name: followupDet.Name,
              AssignedEmpName: followupDet.AssignedEmpName,
              RoleType: followupDet.RoleType,
              CompletedDate: followupDet.CompletedDate,
              CompletedEmpName: followupDet.CompletedEmpName,
              TaskCategoryName: followupDet.TaskCategoryName,
              Id: followupDet.followUpId
            }
            this.followUpDetailList.push(followupDet);
            rowData.push(el);
          });
            this.rowData = rowData;
        });
      }
    }, error => {
      console.log(error)
    });
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    // auto fits columns to fit screen
    params.api.sizeColumnsToFit();
    // hiding loading message on load
    this.gridApi.gridOptionsWrapper.gridOptions.api.hideOverlay();
  }

  yesHandler(followUp: any) {
    this.deleteFollowUpItem = followUp;
    const selectedFollowUp = followUp.optionalData
    let removedFollowUp: any;
    let followUpListHost: any;
    // If undoItem is null, continue to check Ids
    this.followUpDetailList.forEach(followUpDetail => {
      if (followUpDetail.FollowUpId === selectedFollowUp.Id) {
        followUpListHost = this.followUpList.find(x => x.Id === followUpDetail.FollowUpId)
        removedFollowUp = followUpListHost;
        removedFollowUp.FollowUpDetailList = null;
        removedFollowUp.IsInsert = false;
      }
    })
    /* Deleting followUp */
    this.exceMemberService.saveFollowUp([removedFollowUp]).pipe(takeUntil(this.destroy$)).subscribe(result => {
        if (result) {
          // Remove item from view, as it is successfully deleted on backend
          this.rowData = this.rowData.filter(data => data !== this.deleteFollowUpItem.optionalData);
          this.translateService.get('MEMBERSHIP.DeletionSuccessful').pipe(takeUntil(this.destroy$)).subscribe(trans => this.message = trans);
          this.type = 'SUCCESS';
          this.isShowInfo = true;
          setTimeout(() => {
            this.isShowInfo = false;
          }, 4000);
        }
      },
        error => {
          this.translateService.get('MEMBERSHIP.DeletionNotSuccessful').pipe(takeUntil(this.destroy$)).subscribe(trans => this.message = trans);
          this.type = 'DANGER';
          this.isShowInfo = true;
          setTimeout(() => {
            this.isShowInfo = false;
          }, 4000);
    });
  }

  onCellClicked(event) {
    const followUp = event.data;
    const column = event.column.colDef.field;
    switch (column) {
      case 'Delete':
      this.deleteFollowUp(followUp)
    }
  }

  addNewFollowUpToView(event) {
    const newFollowUp = event;
    const newFollowUpDetail = newFollowUp.FollowUpDetailList[0];
    newFollowUpDetail.FollowUpId = newFollowUp.Id;
    this.followUpList.push(newFollowUp)
    this.followUpDetailList.push(newFollowUpDetail);

    const el = {
      PlannedDate: newFollowUpDetail.PlannedDate,
      Name: newFollowUpDetail.Name,
      AssignedEmpName: newFollowUpDetail.AssignedEmpName,
      RoleType: newFollowUpDetail.RoleType,
      CompletedDate: newFollowUpDetail.CompletedDate,
      CompletedEmpName: newFollowUpDetail.CompletedEmpName,
      TaskCategoryName: newFollowUpDetail.TaskCategoryName,
      Id: newFollowUp.Id
    }
    this.rowData.push(el);
    this.gridApi.updateRowData({
     add: [el]
    });
    this.addFixedFollowUpTemplateView.close();
  }

  // for Follow Up
  addFixedFollowUpTemplateModal(content: any) {
    this.addFixedFollowUpTemplateView = this.modalService.open(content, { width: '1500' });
  }

  // close addFixedFollowUpView
  closeAddFixedFollowUpView() {
    this.addFixedFollowUpView.close();
  }

  // For Follow Up Profile
  addFixedFollowUpModal(content: any) {

    this.exceAddFollowUpService.isDisableFollowupProfileList = true;
    this.addFixedFollowUpView = this.modalService.open(content, { width: '1500' });
  }

  // close addFixedFollowUpTemplateView
  closeAddFixedFollowUpTemplateView() {
    this.addFixedFollowUpTemplateView.close();
  }

  // for Update Follow Up
  updateFixedFollowUpTemplateModal(content: any) {
    this.exceAddFollowUpService.IsDisableFollowupProfileList = true;
    this.updateFixedFollowUpTemplateView = this.modalService.open(content, { width: '1500' });
  }

  // close updateFixedFollowUpTemplateView
  closeUpdateFixedFollowUpTemplate() {
    this.updateFixedFollowUpTemplateView.close();
  }

  // Row Double Click
  rowDoubleClick(rowEvent) {
    this.selectedFollowupMember = rowEvent.row.item;
    this.exceFollowupCommonService.SelectedFollowupMember = this.selectedFollowupMember;
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();

    if (this.addFixedFollowUpTemplateView) {
      this.addFixedFollowUpTemplateView.close();
    }
    if (this.addFixedFollowUpView) {
      this.addFixedFollowUpView.close();
    }
    if (this.updateFixedFollowUpTemplateView) {
      this.updateFixedFollowUpTemplateView.close();
    }
  }

/*   deleteConfirmMsg(rowEvent: any) {
    let msgType = '';
    let msg = '';
    this.translateService.get('COMMON.Confirm').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => msgType = translatedValue);
    this.translateService.get('ADMIN.ConfirmMessage').pipe(takeUntil(this.destroy$)).subscribe(translatedValue => msg = translatedValue);
    this.exceMessageService.openMessageBox('CONFIRM', {
        messageTitle: msgType,
        messageBody: msg,
        msgBoxId: 'Delete_Followup',
        optionalData: rowEvent
      }
    );
  } */

}

