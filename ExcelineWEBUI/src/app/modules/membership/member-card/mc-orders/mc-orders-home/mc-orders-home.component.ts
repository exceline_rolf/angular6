import { AdminService } from '../../../../admin/services/admin.service';
import { ExceGymSetting } from '../../../../../shared/SystemObjects/Common/ExceGymSetting';
import { ExceShopService } from '../../../../shop/services/exce-shop.service';
import { ShopHomeService } from '../../../../shop/shop-home/shop-home.service';
import { UsbModalRef } from '../../../../../shared/components/us-modal/modal-ref';
import { ExceToolbarService } from '../../../../common/exce-toolbar/exce-toolbar.service';
import { PreloaderService } from '../../../../../shared/services/preloader.service';
import { Component, OnInit, EventEmitter, OnDestroy, ViewChild, enableProdMode, ViewChildren, QueryList, ElementRef } from '@angular/core';
import { ExceMemberService } from '../../../services/exce-member.service';
import { McBasicInfoService } from '../../mc-basic-info/mc-basic-info.service';
import { DataTableResource } from '../../../../../shared/components/us-data-table/tools/data-table-resource';
import { ExceMessageService } from '../../../../../shared/components/exce-message/exce-message.service';
import { UsbModal } from '../../../../../shared/components/us-modal/us-modal';
import { ExceLoginService } from '../../../../login/exce-login/exce-login.service';
import { PaymentTypes, MemberRole } from 'app/shared/enums/us-enum';
import { NotificationMethodType } from 'app/shared/enums/us-enum';
import { TextTemplateEnum } from 'app/shared/enums/us-enum';
import { FormGroup, FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { PlatformLocation } from '@angular/common';
import { ConfigService } from '@ngx-config/core';
import { ExcePdfViewerService } from '../../../../../shared/components/exce-pdf-viewer/exce-pdf-viewer.service';
import * as moment from 'moment';
import { forEach } from '@angular/router/src/utils/collection';
import { ExceEconomyService } from '../../../../economy/services/exce-economy.service';
import { DomSanitizer, SafeHtml, SafeStyle, SafeScript, SafeUrl, SafeResourceUrl } from '@angular/platform-browser';
import { saveAs } from 'file-saver/FileSaver';
import { MemberCardService } from '../../member-card.service';
import { Subject } from 'rxjs';
import { takeUntil, mergeMap, tap, distinctUntilChanged } from 'rxjs/operators';
const now = new Date();

@Component({
  selector: 'app-mc-orders-home',
  templateUrl: './mc-orders-home.component.html',
  styleUrls: ['./mc-orders-home.component.scss']
})
export class McOrdersHomeComponent implements OnInit, OnDestroy {
  btnDeleteOrderVisible = true;
  btnDeleteOrderEnable = true;
  IsBrisIntegration: any;
  branchId: any;
  shopModalReference: UsbModalRef;
  modalReference: UsbModalRef;
  limit: any;
  locale: string;

  private destroy$ = new Subject<void>();
  private selectedMember: any;
  private items = [];
  public orderItems?: any[] = [];
  private deletedItemId: any;
  private deletedItem: any;
  private selectedOrderItem: any;
  private selectedOrder: any;
  private paymentDetails: any;
  private memberBranchID: any;
  private editTemplateForm: FormGroup;
  private SelectedSMSTemplate: any;
  private SelectedEmailTemplate: any;
  private SMSTemplate: any;
  private EmailTemplate: any;
  private payingOrder: any;
  private IsSMSNo = true;
  private IsEmailAddress = true;
  private SMSContent: any;
  private EmailContent: any;
 // private editTemplateModal: any;
  private isSelected = {
    'SMS' : false,
    'EMAIL' : false
  };
  private text = {
    'SMS' : String,
    'EMAIL' : String
  };
  private senderDetail = {
    'SMS' : String,
    'EMAIL' : String
  };
  private SmsNo: any;
  private EmailAddress: any;
  private SMSChecked: any;
  private EmailChecked: any;
  private exceGymSetting: ExceGymSetting = new ExceGymSetting();
  private shopLoginData?: any = {};
  blobUrl: any;
  blob: any;
  currentInvoice: any;
  generatePdfView: UsbModalRef;
  generateSmsView: UsbModalRef;
  addInvoiceFee = false
  private selectedGymsettings = [
    'EcoNegativeOnAccount',
    'EcoSMSInvoiceShop',
    'EcoIsPrintInvoiceInShop',
    'EcoIsPrintReceiptInShop',
    'EcoIsShopOnNextOrder',
    'EcoIsOnAccount',
    'EcoIsDefaultCustomerAvailable',
    'EcoIsPayButtonsAvailable',
    'EcoIsPettyCashSameAsUserAllowed',
    'OtherIsShopAvailable',
    'OtherLoginShop',
    'HwIsShopLoginWithCard',
    'ReqMemberListRequired',
    'OtherIsLogoutFromShopAfterSale',
    'EcoIsBankTerminalIntegrated',
    'EcoIsDailySettlementForAll',
    'OtherIsValidateShopGymId'
  ];

  @ViewChild('Pdfwindow') Pdfwindow: ElementRef;
  @ViewChild('editTemplateModal1') editTemplateModal1: ElementRef;
  @ViewChild('shopHomeModel') shopHomeModel: ElementRef;
  @ViewChild('orderDetails') detailView: ElementRef;

  private isOrderCheckOutEnable = true;
  private isOrderCheckOutvisible = true;
  private isOrderSMSEmailEnable = true;
  private isOrderSMSEmailvisible = true;
  private isOrderInvoiceEnable = true;
  private isOrderInvoicevisible = true;

  _user: any;
  _dateFormat = 'DD/MM/YYYY';
  saveEvent: any;

  orderDefs: any;

  names = {
    MemberContractNo: 'Kontrakt',
    No: 'Ordrenummer',
    Periode: 'Periode',
    Duedate: 'Forfall',
    ActivityPrice: 'Aktivitetspris',
    AdonPrice: 'Tilleggspris',
    Amount: 'Ordrebeløp',
    Content: 'Innhold',
    Type: 'Type',
    Status: 'Status'
  }

  colNamesNo = {
    MemberContractNo: 'Kontrakt',
    No: 'Ordrenummer',
    Periode: 'Periode',
    Duedate: 'Forfall',
    ActivityPrice: 'Aktivitetspris',
    AdonPrice: 'Tilleggspris',
    Content: 'Innhold',
    Amount: 'Ordrebeløp',
    Type: 'Type',
    Status: 'Status'
  }

  colNamesEn = {
    MemberContractNo: 'Contract',
    No: 'Order no',
    Periode: 'Periode',
    Duedate: 'Due date',
    ActivityPrice: 'Activity price',
    AdonPrice: 'Addon price',
    Content: 'Content',
    Amount: 'Order amount',
    Type: 'Type',
    Status: 'Status'
  }
  gridApi: any;
  sub: any;

  constructor(
    private basicInfoService: McBasicInfoService,
    private exceMemberService: ExceMemberService,
    private translateService: TranslateService,
    private exceMessageService: ExceMessageService,
    private adminService: AdminService,
    private modalService: UsbModal,
    private location: PlatformLocation,
    private toolbarService: ExceToolbarService,
    private loginservice: ExceLoginService,
    private shopHomeService: ShopHomeService,
    private shopService: ExceShopService,
    private exceLoginService: ExceLoginService,
    private excePdfViewerService: ExcePdfViewerService,
    private config: ConfigService,
    private economyService: ExceEconomyService,
    private sanitizer: DomSanitizer,
    private formbuilder: FormBuilder,
    private membercardservice: MemberCardService
  ) {

    this.branchId = this.exceLoginService.SelectedBranch.BranchId;
    this.exceLoginService.branchSelected.pipe(takeUntil(this.destroy$)).subscribe(
      (branch) => {
        this.branchId = branch.BranchId;
      }
    );

    this.editTemplateForm = formbuilder.group({
      'IsSMSChecked': [this.SMSChecked],
      'IsEmailChecked': [this.EmailChecked],
      'SmsNo': [{ value: this.SmsNo, disabled: false }, [Validators.pattern('^[0-9 ]*$')]], // { value: null, disabled: true }
      'SelectedSMSTemplate': [this.SMSTemplate],
      'SMSContent': [null],
      'EmailAddress': [{ value: this.EmailAddress, disabled: false }, [Validators.email]],
      'SelectedEmailTemplate': [this.EmailTemplate],
      'EmailContent': [null],
    });

    this.exceMessageService.yesCalledHandle.pipe(takeUntil(this.destroy$)).subscribe((value) => {
      if (value.id === 'DELETE_ORDER') {
        this.deleteYesHandler();
      } else if (value.id === 'INVOICE_WITH_SPONSORING') {
        this.invoiceWithSopnsoringYesHandler();
      } else if (value.id === 'MSG_INVOICE') {
        this.msgInvoiceYesHandler();
      } else if (value.id === 'ADD_INVOICE_FEE') {
        this.msgAddInvoiceFeeYesHandler();
      } else if (value.id === 'MSG_INVOICE_SMS') {
        this.msgInvoiceSMSYesHandler(value.optionalData);
      } else if (value.id === 'ADD_INVOICE_FEE_SMS') {
        this.msgAddInvoiceFeeSMSYesHandler();
      }
    });

    this.exceMessageService.noCalledHandle.pipe(takeUntil(this.destroy$)).subscribe((value) => {
      if (value.id === 'ADD_INVOICE_FEE') {
        this.msgAddInvoiceFeeNoHandler();
      } else if (value.id === 'ADD_INVOICE_FEE_SMS') {
        this.msgAddInvoiceFeeSMSNoHandler();
      }
    });

    const language = this.toolbarService.getSelectedLanguage();
    this.locale = language.culture;
    this._dateFormat = 'DD.MM.YYYY';

    switch (language.id) {
      case 'no':
        this.names = this.colNamesNo;
        break;
      case 'en':
        this.names = this.colNamesEn;
        break;
    }

    this.toolbarService.langUpdated.pipe(takeUntil(this.destroy$)).subscribe(
      (lang) => {
        if (lang) {
          this.locale = lang.culture;
        }
      }
    );

  }

  manipulateDateObject(date: Date) {
    return moment(date).format('YYYY-MM-DD').toString();
  }

  dateAdd(startDate: any, count: any, interval: string): Date {
    return moment(startDate).add(count, interval).toDate()
  }

  manipulatedate(dateInput: any) {
    if (dateInput.date !== undefined) {
      const tempDate: Date = new Date(dateInput.date.year, dateInput.date.month - 1, dateInput.date.day);
      return moment(tempDate).format('YYYY-MM-DD').toString();
    }
  }

  AddOrderEvent() {
    const newInstallment: any = {};
    newInstallment.MemberId = Number(this.selectedMember.Id);
    newInstallment.branchId = this.branchId;
    // add the installment
    this.exceMemberService.addBlancInstallment(newInstallment).pipe(
      mergeMap(next => {
        if (next.Data === 1) {
          PreloaderService.showPreLoader();
          return this.exceMemberService.getMemberOrders(this.selectedMember.Id, 'ALL');
        }
      }),
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.items = [];
        result.Data.forEach(installment => {
          installment.BtnDeleteOrderEnable = this.btnDeleteOrderEnable;
          installment.BtnDeleteOrderVisible = this.btnDeleteOrderVisible;
          installment.IsOrderCheckOutvisible = true;
          installment.IsOrderSMSEmailvisible = true;
          installment.IsOrderInvoicevisible = true;

          if (installment.GymID !== this.branchId && !this.IsBrisIntegration) {
            installment.OrderOperationsEnabled = false;
            installment.IsCheckOutEnable = false;
            installment.IsLocked = true;
          }
          installment.IsTreat = true;
          installment.OrderNo = Number(installment.OrderNo);
          installment.IsCheckOutEnable = this.isOrderCheckOutEnable ? installment.IsCheckOutEnable : false;
          installment.OrderOperationsEnabled = this.isOrderSMSEmailEnable ? installment.OrderOperationsEnabled : false;
          installment.EnableInvoice = this.isOrderInvoiceEnable ? installment.EnableInvoice : false;

          installment.IsOrderCheckOutvisible = this.isOrderCheckOutvisible ? true : false;
          installment.IsOrderSMSEmailvisible = this.isOrderSMSEmailvisible ? true : false;
          installment.IsOrderInvoicevisible = this.isOrderInvoicevisible ? true : false;
          this.items.push(installment);
        });

        // this.items = result.Data;
        this.orderItems = this.items;

        // this.itemResource = new DataTableResource(this.orderItems);
        // this.itemResource.count().then(count => this.itemCount = count);
        // this.itemCount = Number(result.Data.length);
        // this.limit = this.orderItems.length;
      }
      PreloaderService.hidePreLoader();
    }, err => {
      PreloaderService.hidePreLoader();
    });

/*      this.exceMemberService.addBlancInstallment(newInstallment).pipe(takeUntil(this.destroy$)).subscribe(result => {
        if (result) {
          if (result.Data === 1) {
            // this.selectedContract.NoOfInstallments += 1;
            this.getInstallments();
          }
        }
      }); */

  }
  getInstallments() {
    this.loadMemberOders();
  }

  loadMemberOders() {
    PreloaderService.showPreLoader();
    this.exceMemberService.getMemberOrders(this.selectedMember.Id, 'ALL').pipe(
      takeUntil(this.destroy$)
      ).subscribe(result => {
        if (result) {
          this.items = [];
          result.Data.forEach(installment => {
            installment.BtnDeleteOrderEnable = this.btnDeleteOrderEnable;
            installment.BtnDeleteOrderVisible = this.btnDeleteOrderVisible;
            installment.IsOrderCheckOutvisible = true;
            installment.IsOrderSMSEmailvisible = true;
            installment.IsOrderInvoicevisible = true;

            if (installment.GymID !== this.branchId && !this.IsBrisIntegration) {
              installment.OrderOperationsEnabled = false;
              installment.IsCheckOutEnable = false;
              installment.IsLocked = true;
            }
            installment.IsTreat = true;
            installment.OrderNo = Number(installment.OrderNo);
            installment.IsCheckOutEnable = this.isOrderCheckOutEnable ? installment.IsCheckOutEnable : false;
            installment.OrderOperationsEnabled = this.isOrderSMSEmailEnable ? installment.OrderOperationsEnabled : false;
            installment.EnableInvoice = this.isOrderInvoiceEnable ? installment.EnableInvoice : false;

            installment.IsOrderCheckOutvisible = this.isOrderCheckOutvisible ? true : false;
            installment.IsOrderSMSEmailvisible = this.isOrderSMSEmailvisible ? true : false;
            installment.IsOrderInvoicevisible = this.isOrderInvoicevisible ? true : false;
            this.items.push(installment);
          });

          // this.items = result.Data;
          this.orderItems = this.items;

          // this.itemResource = new DataTableResource(this.orderItems);
          // this.itemResource.count().then(count => this.itemCount = count);
          // this.itemCount = Number(result.Data.length);
          // this.limit = this.orderItems.length;
        }
        PreloaderService.hidePreLoader();
      }, err => {
        PreloaderService.hidePreLoader();
      });
  }

  ngOnInit() {
    this.adminService.GetGymCompanySettings('other').pipe(takeUntil(this.destroy$)).subscribe(result => {
      this.IsBrisIntegration = result.Data.IsBrisIntegrated;
    });
    this.memberBranchID = this.loginservice.SelectedBranch.BranchId;
    this._user = this.loginservice.CurrentUser.username;
        // get the selected member

    this.basicInfoService.currentMember.pipe(
      mergeMap(member => {
        if (member) {
          this.selectedMember = member;
          return this.exceMemberService.getMemberOrders(this.selectedMember.Id, 'ALL');
        }
      }),
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.items = [];
        result.Data.forEach(installment => {
          installment.BtnDeleteOrderEnable = this.btnDeleteOrderEnable;
          installment.BtnDeleteOrderVisible = this.btnDeleteOrderVisible;
          installment.IsOrderCheckOutvisible = true;
          installment.IsOrderSMSEmailvisible = true;
          installment.IsOrderInvoicevisible = true;

          if (installment.GymID !== this.branchId && !this.IsBrisIntegration) {
            installment.OrderOperationsEnabled = false;
            installment.IsCheckOutEnable = false;
            installment.IsLocked = true;
          }
          installment.IsTreat = true;
          installment.OrderNo = Number(installment.OrderNo);
          installment.IsCheckOutEnable = this.isOrderCheckOutEnable ? installment.IsCheckOutEnable : false;
          installment.OrderOperationsEnabled = this.isOrderSMSEmailEnable ? installment.OrderOperationsEnabled : false;
          installment.EnableInvoice = this.isOrderInvoiceEnable ? installment.EnableInvoice : false;

          installment.IsOrderCheckOutvisible = this.isOrderCheckOutvisible ? true : false;
          installment.IsOrderSMSEmailvisible = this.isOrderSMSEmailvisible ? true : false;
          installment.IsOrderInvoicevisible = this.isOrderInvoicevisible ? true : false;
          this.items.push(installment);
        });

        // this.items = result.Data;
        this.orderItems = this.items;

        // this.itemResource = new DataTableResource(this.orderItems);
        // this.itemResource.count().then(count => this.itemCount = count);
        // this.itemCount = Number(result.Data.length);
        // this.limit = this.orderItems.length;
      }
      PreloaderService.hidePreLoader();
    }, err => {
      PreloaderService.hidePreLoader();
    });

    // Get SMS Template List
    this.exceMemberService.getNotificationTemplateListByMethod(NotificationMethodType[NotificationMethodType.SMS],
      TextTemplateEnum[TextTemplateEnum.SMSINVOICE], this.memberBranchID).pipe(takeUntil(this.destroy$)).subscribe(
        result => {
          if (result) {
            this.SelectedSMSTemplate = result.Data;
            this.SMSContent = this.SelectedSMSTemplate[0].Text;
          }
        }
      );

    // Get Email Template List
    this.exceMemberService.getNotificationTemplateListByMethod(NotificationMethodType[NotificationMethodType.EMAIL],
      TextTemplateEnum[TextTemplateEnum.EMAILINVOICE], this.memberBranchID).pipe(takeUntil(this.destroy$)).subscribe(
        result => {
          if (result) {
            this.SelectedEmailTemplate = result.Data;
            this.EmailContent = this.SelectedEmailTemplate[0].Text;
          }
        });

    this.shopService.GetSelectedGymSettings({ GymSetColNames: this.selectedGymsettings, IsGymSettings: true }).pipe(takeUntil(this.destroy$)).subscribe(
      res => {

        for (const key in res.Data) {
          if (res.Data.hasOwnProperty(key)) {
            switch (key) {
              case 'EcoNegativeOnAccount':
                this.exceGymSetting.OnAccountNegativeVale = res.Data[key];
                break;
              case 'EcoIsShopOnNextOrder':
                this.exceGymSetting.IsShopOnNextOrder = res.Data[key];
                break;
              case 'EcoIsDailySettlementForAll':
                this.exceGymSetting.IsDailySettlementForAll = res.Data[key];
                break;
              case 'EcoIsOnAccount':
                this.exceGymSetting.IsOnAccount = res.Data[key];
                break;
              case 'EcoIsDefaultCustomerAvailable':
                this.exceGymSetting.IsDefaultCustomerAvailable = res.Data[key];
                break;
              case 'EcoIsPayButtonsAvailable':
                this.exceGymSetting.IsPayButtonsAvailable = res.Data[key];
                break;
              case 'EcoIsPettyCashSameAsUserAllowed':
                this.exceGymSetting.IsPettyCashSameAsUserAllowed = res.Data[key];
                break;
              case 'EcoIsPrintInvoiceInShop':
                this.exceGymSetting.IsPrintInvoiceInShop = res.Data[key];
                break;
              case 'EcoIsPrintReceiptInShop':
                this.exceGymSetting.IsPrintReceiptInShop = res.Data[key];
                break;
              case 'OtherIsShopAvailable':
                this.exceGymSetting.IsShopAvailable = res.Data[key];
                break;
              case 'OtherLoginShop':
                this.exceGymSetting.IsShopLoginNeeded = res.Data[key];
                break;
              case 'HwIsShopLoginWithCard':
                this.exceGymSetting.IsShopLoginWithCard = res.Data[key];
                break;
              case 'ReqMemberListRequired':
                this.exceGymSetting.MemberRequired = res.Data[key];
                break;
              case 'OtherIsLogoutFromShopAfterSale':
                this.exceGymSetting.IsLogoutFromShopAfterSale = res.Data[key];
                break;
              case 'EcoSMSInvoiceShop':
                this.exceGymSetting.IsSMSInvoiceShop = res.Data[key];
                break;
              case 'EcoIsBankTerminalIntegrated':
                this.exceGymSetting.IsBankTerminalIntegrated = res.Data[key];
                break;
              case 'OtherIsValidateShopGymId':
                this.exceGymSetting.IsValidateShopGymId = res.Data[key];
                break;
            }

          }
        }
    }, null, null);

    this.shopLoginData.GymSetting = this.exceGymSetting;
    this.shopService.GetSalesPoint().pipe(
      tap(value => console.log(value)),
      mergeMap(salePoints => {
        this.shopLoginData.SalePoint = salePoints.Data[0];
        return this.adminService.getGymSettings('HARDWARE', null).pipe(
          takeUntil(this.destroy$)
        )
      }),
      takeUntil(this.destroy$)
      ).subscribe(hwSetting => {
        this.shopLoginData.HardwareProfileList = hwSetting.Data;
        this.shopLoginData.HardwareProfileList.forEach(hwProfile => {
          if (this.shopLoginData.SalePoint != null &&
            this.shopLoginData.SalePoint.HardwareProfileId === hwProfile.Id) {
            this.shopLoginData.SelectedHardwareProfile = hwProfile;
            this.shopLoginData.HardwareProfileId = hwProfile.Id;
        } else {
            this.shopLoginData.SelectedHardwareProfile = null;
            this.shopLoginData.HardwareProfileId = null;
          }
        this.shopHomeService.$shopLoginData = this.shopLoginData;
      });
    }, null, null);

    this.saveEvent = this.shopHomeService.savePaymentFromOrdersEvent.pipe(
      takeUntil(this.destroy$)
      ).subscribe(val => {
        if (val === 0) {
          this.closeShop();
        } else if (val === 1) {
          this.shopHomeService.closeShopEvent.next(true);
        }
      }
    )

    this.orderDefs = [
      {headerName: this.names.MemberContractNo, field: 'MemberContractNo', width: 90, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
    }, suppressMovable: true, autoHeight: true},
      {headerName: this.names.No, field: 'InstallmentNo', width: 90, suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
        filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true},
      {headerName: this.names.Periode, field: 'Text', suppressMenu: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'center'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true},
      {headerName: this.names.Duedate, field: 'DueDate', suppressMenu: true, suppressSorting: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'center'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        if (params.value) {
          const d = params.value.split('T');
          const dDate = d[0].split('-');
          return dDate[2] + '.' + dDate[1] + '.' + dDate[0];
        } else {
          return
        }}},
      {headerName: this.names.ActivityPrice, field: 'ActivityPrice', suppressMenu: true, suppressSorting: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'right'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        if (params.value) {
          const val = Number(params.value)
          return val.toFixed(2);
        } else {
          return;
        }}},
      {headerName: this.names.AdonPrice, field: 'AdonPrice', suppressMenu: true, suppressSorting: true, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'right'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        if (params.value) {
          const val = Number(params.value)
          return val.toFixed(2);
        } else {
          return '0.00';
        }}},
        {headerName: this.names.Amount, field: 'Amount', suppressMenu: true, suppressSorting: true, floatingFilterComponentParams: {suppressFilterButton: true},
        filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'right'}, icons: {
          sortAscending: '<i class="icon-sort-ascending"/>',
          sortDescending: '<i class="icon-sort-descending"/>'
        }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
          if (params.value) {
            const val = Number(params.value)
            return val.toFixed(2);
          } else {
            return;
          }}},
      {headerName: '', field: 'Betaling', suppressMenu: true, suppressSorting: true, suppressFilter: true, width: 40, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'center'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        if (params.data.IsOrderCheckOutvisible) {
          if (params.data.IsCheckOutEnable) {
            return '<button class="btn btn-primary btn-icon-min" title="Betaling"><span class="icon-checkout"></span></button>';
          } else {
            return '<button disabled class="btn btn-primary btn-icon-min" title="Betaling"><span class="icon-checkout"></span></button>';
          }
        } else {
          return
        }
        }
      },
      {headerName: '', field: 'SMS/EPOST', suppressMenu: true, suppressSorting: true, suppressFilter: true, width: 40, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'center'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        if (params.data.IsOrderSMSEmailvisible) {
          if (params.data.OrderOperationsEnabled) {
            return '<button class="btn btn-primary btn-icon-min" title="SMS/Epost faktura"><span class="icon-sms"></span></button>';
          } else {
            return '<button disabled class="btn btn-primary btn-icon-min" title="SMS/Epost faktura"><span class="icon-sms"></span></button>';
          }
        } else {
          return
        }
        }
      },
      {headerName: '', field: 'Faktura', suppressMenu: true, suppressSorting: true, suppressFilter: true, width: 40, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'center'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        if (params.data.IsOrderInvoicevisible) {
          if (params.data.OrderOperationsEnabled) {
            return '<button class="btn btn-primary btn-icon-min" title="Opprett faktura"><span class="icon-invoice"></span></button>';
          } else {
            return '<button disabled class="btn btn-primary btn-icon-min" title="Opprett faktura"><span class="icon-invoice"></span></button>';
          }
        } else {
          return;
        }
        }
      },
      {headerName: '', field: 'Detaljer', suppressMenu: true, suppressSorting: true, suppressFilter: true, width: 40, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'center'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        return '<button class="btn btn-primary btn-icon-min" title="Ordredetaljer"><span class="icon-detail"></span></button>';
        }
      },
      {headerName: '', field: 'Slett', suppressMenu: true, suppressSorting: true, suppressFilter: true, width: 40, floatingFilterComponentParams: {suppressFilterButton: true},
      filterParams: {newRowsAction: 'keep'}, cellStyle: {'cursor': 'pointer', 'textAlign': 'center'}, icons: {
        sortAscending: '<i class="icon-sort-ascending"/>',
        sortDescending: '<i class="icon-sort-descending"/>'
      }, suppressMovable: true, autoHeight: true, cellRenderer: function(params) {
        if (params.data.HasAddonFromShop) {
          return '<button disabled class="btn btn-danger btn-icon-min" title="Slett"><span class="icon-cancel"></span></button>';
        } else {
          return '<button class="btn btn-danger btn-icon-min" title="Slett"><span class="icon-cancel"></span></button>';
        }
        }
      },
    ];
  }

  // to open Edit Template view
  openEditTemplateModal(content: any) {

    this.editTemplateForm.reset();

    if (this.selectedMember.Mobile !== '') {
      this.SMSChecked = true;
      this.editTemplateForm.get('SmsNo').enable();
    } else {
      this.SMSChecked = false;
      this.editTemplateForm.get('SmsNo').disable();
    }
    if (this.selectedMember.Email !== '') {
      this.EmailChecked = true;
      this.editTemplateForm.get('EmailAddress').enable();
    } else {
      this.EmailChecked = false;
      this.editTemplateForm.get('EmailAddress').disable();
    }

    this.SmsNo = this.selectedMember.Mobile;
    this.EmailAddress = this.selectedMember.Email;

    this.SMSTemplate = this.SelectedSMSTemplate[0];
    this.EmailTemplate = this.SelectedEmailTemplate[0];
    this.SMSContent = this.SelectedSMSTemplate[0].Text;
    this.EmailContent = this.SelectedEmailTemplate[0].Text;

    this.generateSmsView = this.modalService.open(this.editTemplateModal1, { width: '800' });

  }

  cellClickedOrder(event) {
    const order = event.data;
    const column = event.column.colDef.field;
    switch (column) {
      case 'Betaling':
        this.openShop(this.shopHomeModel, order)
      break;
      case 'SMS/EPOST':
      this.SMSInvoiceEvent(order, this.editTemplateModal1)
      break;
      case 'Faktura':
      this.generateInvoice(order)
      break;
      case 'Detaljer':
      this.openDetailView(order)
      break;
      case 'Slett':
        if (event.data.HasAddonFromShop) {
          break;
        }
      this.deleteOrder(order)
      break;
    }
  }

  onGridReady(params) {
    this.gridApi = params.api;
    // this.gridColumnApi = params.columnApi;
    // auto fits columns to fit screen
    this.gridApi.sizeColumnsToFit();
    // hiding loading message on load
    this.gridApi.gridOptionsWrapper.gridOptions.api.hideOverlay();
  }

  orderSeleted(event) {
    const order = event.data;
    this.openDetailView(order)
  }


  // ------------------------ SEND SMS/EMAIL -----------------------

  // to send SMS or Email
  SMSInvoiceEvent(item, editTemplateModal) {

    this.payingOrder = item;
    this.editTemplateModal1 = editTemplateModal;

    // MsgInvoice = Are you sure you want to invoice ?
    let messageTitle = '';
    let messageBody = '';
    (this.translateService.get('Confirm').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageTitle = tranlstedValue));
    (this.translateService.get('MEMBERSHIP.MsgInvoice').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageBody = tranlstedValue));
    this.exceMessageService.openMessageBox('CONFIRM',
      {
        messageTitle: messageTitle,
        messageBody: messageBody,
        msgBoxId: 'MSG_INVOICE_SMS',
        optionalData: this.payingOrder.IsInvoiceFeeAdded
      }
    );
  }

  // MSG_INVOICE_SMS
  msgInvoiceSMSYesHandler(data) {
    // this.payingOrder.IsInvoiceFeeAdded = data;
    if (data === false) {
      let messageTitle = '';
      let messageBody = '';
      (this.translateService.get('Confirm').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageTitle = tranlstedValue));
      (this.translateService.get('MEMBERSHIP.MsgaddInvoiceFee').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageBody = tranlstedValue));
      this.exceMessageService.openMessageBox('CONFIRM',
        {
          messageTitle: messageTitle,
          messageBody: messageBody,
          msgBoxId: 'ADD_INVOICE_FEE_SMS',
          optionalData: data
        }
      );
    } else {
      this.PreapreSMSInvoice();
    }
  }

  // ADD_INVOICE_FEE_SMS yes
  msgAddInvoiceFeeSMSYesHandler() {
    this.PreapreSMSInvoice();
  }

  // ADD_INVOICE_FEE_SMS no
  msgAddInvoiceFeeSMSNoHandler() {
    this.payingOrder.IsInvoiceFeeAdded = (this.payingOrder.IsInvoiceFeeAdded === null) ? null : false;
    this.PreapreSMSInvoice();
  }

  PreapreSMSInvoice() {
    this.openEditTemplateModal(this.editTemplateModal1);
  }

  sendSMSOrEmail(value) {

    if (value.IsSMSChecked === null) {
      value.IsSMSChecked = false;
    }
    if (value.IsEmailChecked === null) {
      value.IsEmailChecked = false;
    }

    value.SMSContent = (value.SMSContent === null) ? value.SelectedSMSTemplate.Text : value.SMSContent;
    value.EmailContent = (value.EmailContent === null) ? value.SelectedEmailTemplate.Text : value.EmailContent;

    if ((value.IsSMSChecked === true) || (value.IsEmailChecked === true)) {
      if ((value.IsSMSChecked === true) && (value.SMSContent !== !null) && (value.SmsNo !== null)) {

        const isSelectS = {};
        isSelectS['SMS'] = true;
        this.isSelected['SMS'] = true;

        const txtS = {};
        txtS['SMS'] = value.SMSContent;
        this.text['SMS'] = value.SMSContent;

        const senderDetS = {};
        senderDetS['SMS'] = value.SmsNo;
        this.senderDetail['SMS'] = value.SmsNo;

      } else {
        const isSelectS = {};
        isSelectS['SMS'] = false;
        this.isSelected['SMS'] = false;
      }

      if ((value.IsEmailChecked === true) && (value.EmailContent !== null) && (value.EmailAddress !== null)) {

        const isSelectE = {};
        isSelectE['EMAIL'] = true;
        this.isSelected['EMAIL'] = true;

        const txtE = {};
        txtE['EMAIL'] = value.EmailContent;
        this.text['EMAIL'] = value.EmailContent;

        const senderDetE = {};
        senderDetE['EMAIL'] = value.EmailAddress;
        this.senderDetail['EMAIL'] = value.EmailAddress;

      } else {
        const isSelectE = {};
        isSelectE['EMAIL'] = false;
        this.isSelected['EMAIL'] = false;
      }

      const generateAndSendInvoiceSMSANDEmail = {
        invoiceBranchId: 1,
        selectedOrder: this.payingOrder,
        paymentDetails: { PaymentSource: PaymentTypes[PaymentTypes.INSTALLMENTS] },
        isSelected: this.isSelected,
        text: this.text,
        senderDetail: this.senderDetail,
        memberBranchID: this.memberBranchID
      }

      this.exceMemberService.generateAndSendInvoiceSMSANDEmail(generateAndSendInvoiceSMSANDEmail).pipe(takeUntil(this.destroy$)).subscribe(result => {
        if (result) {
          const Notification = {
            Title: 'Generering av faktura',
            Description: 'Ordre ble manuelt generert til faktura',
            CreatedUser: this.loginservice.CurrentUser.username,
            Role: MemberRole[this.selectedMember.Role],
            TypeId: 4,
            SeverityId: 3,
            StatusId: 1,
            BranchID: this.branchId,
            MemberID: this.selectedMember.Id,
            RecordType: 'ORD'
          }
          // logging event
          this.adminService.AddEventToNotifications(Notification).pipe(takeUntil(this.destroy$)).subscribe( res => {
            // do nothing for now
            if (res) {
            }
          })
          this.getInstallments();
        } else {
        }
      }
      );
    } else {
    }
  }


  // ------------------ INVOICE -------------------

  // to generate invoice
  generateInvoice(item) {

    this.selectedOrder = item;
    const tmpSelectedOrder = item;

    if (tmpSelectedOrder != null && !tmpSelectedOrder.IsSponsored && (tmpSelectedOrder.Discount > 0 || tmpSelectedOrder.SponsoredAmount > 0)) {
      let messageTitle = '';
      let messageBody = '';
      (this.translateService.get('Confirm').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageTitle = tranlstedValue));
      (this.translateService.get('MEMBERSHIP.MsgIvoiceWithSposoring').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageBody = tranlstedValue));

      this.exceMessageService.openMessageBox('CONFIRM',
        {
          messageTitle: messageTitle,
          messageBody: messageBody,
          msgBoxId: 'INVOICE_WITH_SPONSORING'
        }
      );

    } else {
      let messageTitle = '';
      let messageBody = '';
      (this.translateService.get('Confirm').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageTitle = tranlstedValue));
      (this.translateService.get('MEMBERSHIP.MsgInvoice').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageBody = tranlstedValue));
      this.exceMessageService.openMessageBox('CONFIRM',
        {
          messageTitle: messageTitle,
          messageBody: messageBody,
          msgBoxId: 'MSG_INVOICE'
        }
      );
    }
  }

  // INVOICE_WITH_SPONSORING
  invoiceWithSopnsoringYesHandler() {

    if (this.selectedOrder.IsInvoiceFeeAdded === true) {
      let messageTitle = '';
      let messageBody = '';
      (this.translateService.get('Confirm').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageTitle = tranlstedValue));
      (this.translateService.get('MEMBERSHIP.MsgaddInvoiceFee').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageBody = tranlstedValue));
      this.exceMessageService.openMessageBox('CONFIRM',
        {
          messageTitle: messageTitle,
          messageBody: messageBody,
          msgBoxId: 'ADD_INVOICE_FEE'
        }
      );
    } else {
      this.GenerateInvoiceDocument();
    }
  }

  // ADD_INVOICE_FEE yes
  msgAddInvoiceFeeYesHandler() {
    this.addInvoiceFee = true;
    this.GenerateInvoiceDocument();
  }

  // ADD_INVOICE_FEE no
  msgAddInvoiceFeeNoHandler() {
    this.addInvoiceFee = false;
    this.GenerateInvoiceDocument();
  }

  // MSG_INVOICE
  msgInvoiceYesHandler() {

    let messageTitle = '';
    let messageBody = '';
    (this.translateService.get('Confirm').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageTitle = tranlstedValue));
    (this.translateService.get('MEMBERSHIP.MsgaddInvoiceFee').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageBody = tranlstedValue));
    this.exceMessageService.openMessageBox('CONFIRM',
      {
        messageTitle: messageTitle,
        messageBody: messageBody,
        msgBoxId: 'ADD_INVOICE_FEE'
      }
    );
  }

  // to generate the invoice
  GenerateInvoiceDocument() {
    if (this.selectedOrder.AddOnList.length !== 0) {
    this.selectedOrder.IsInvoiceFeeAdded = this.addInvoiceFee;
    const generateInvoice = {
      invoiceBranchId: 1,
      installment: this.selectedOrder,
      paymentDetail: { PaymentSource: PaymentTypes[PaymentTypes.INSTALLMENTS] },
      invoiceType: 'PRINT',
      notificationTitle: 'Invoice was generated',
      memberBranchID: this.memberBranchID
    }

    this.exceMemberService.generateInvoice(generateInvoice).pipe(takeUntil(this.destroy$)).subscribe(result => {
      if (result) {
        if (result.Data.InvoiceNo) {
          const Notification = {
            Title: 'Generering av faktura',
            Description: 'Ordre ble manuelt generert til faktura',
            CreatedUser: this.loginservice.CurrentUser.username,
            Role: MemberRole[this.selectedMember.Role],
            TypeId: 4,
            SeverityId: 3,
            StatusId: 1,
            BranchID: this.branchId,
            MemberID: this.selectedMember.Id,
            RecordType: 'ORD'
          }
          // logging event
          this.adminService.AddEventToNotifications(Notification).pipe(
            distinctUntilChanged(),
            takeUntil(this.destroy$)
          ).subscribe(res => {
            if (res) {
              this.openGenerateConfirmModal(this.Pdfwindow, result.Data.InvoiceNo);
              this.loadMemberOders();
            }
          })
          // this.adminService.AddEventToNotifications(Notification).distinctUntilChanged().pipe(takeUntil(this.destroy$)).subscribe( res => {
          //   if (res) {
          //     this.openGenerateConfirmModal(this.Pdfwindow, result.Data.InvoiceNo);
          //     this.loadMemberOders();
          //   }
          // })
          // this.openGenerateConfirmModal(this.Pdfwindow, result.Data.InvoiceNo);
          // this.loadMemberOders();
        }
      } else {
        //  ================= When generate Invoice Failed ====================
        let messageBody = '';
        (this.translateService.get('MEMBERSHIP.MsgInvoiceDocGenFailed').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageBody = tranlstedValue));
        this.exceMessageService.openMessageBox('WARNING',
          {
            messageTitle: 'Exceline',
            messageBody: messageBody,
            msgBoxId: 'INVOICE_FAILED'
          }
        );
      }
    },
      error => {
        // console.log(error);
      });
    } else {
      let messageBody = '';
      (this.translateService.get('MEMBERSHIP.zeroOrderLineInvoice').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageBody = tranlstedValue));
      this.exceMessageService.openMessageBox('ERROR',
        {
          messageTitle: 'Exceline',
          messageBody: messageBody,
          msgBoxId: 'INVOICE_FAILED'
        });
    }
  }


  // ---------------- DELETE -----------------------

  // to delete an order
  deleteOrder(item) {
    this.deletedItem = item;
    this.deletedItemId = item.Id;

    let messageTitle = '';
    let messageBody = '';
    (this.translateService.get('Confirm').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageTitle = tranlstedValue));
    (this.translateService.get('MEMBERSHIP.ConfirmDelete').pipe(takeUntil(this.destroy$)).subscribe(tranlstedValue => messageBody = tranlstedValue));
    this.exceMessageService.openMessageBox('CONFIRM',
      {
        messageTitle: messageTitle,
        messageBody: messageBody,
        msgBoxId: 'DELETE_ORDER'
      }
    );
  }

  deleteYesHandler() {
    const idList = []
    idList.push(this.deletedItem.Id);

    const deleteInstallment = {
      installmentIdList: idList,
      memberContractId: this.deletedItem.MemberContractId
    }

    this.exceMemberService.deleteInstallment(deleteInstallment).pipe(takeUntil(this.destroy$)).subscribe(
        result => {
          this.orderItems = this.orderItems.filter(item => item !== this.deletedItem);
        });
  }

  changeSmsEvent(event) {
    if (event.target.checked) {
      this.IsSMSNo = false;
      this.SMSContent = this.SelectedSMSTemplate[0].Text;
      this.editTemplateForm.get('SmsNo').enable();
    } else {
      this.IsSMSNo = true;
      this.SMSContent = '';
      this.editTemplateForm.get('SmsNo').disable();
    }
  }

  changeEmailEvent(event) {
    if (event.target.checked) {
      this.IsEmailAddress = false;
      this.EmailContent = this.SelectedEmailTemplate[0].Text;
      this.editTemplateForm.get('EmailAddress').enable();
    } else {
      this.IsEmailAddress = true;
      this.EmailContent = '';
      this.editTemplateForm.get('EmailAddress').disable();
    }
  }

  openDetailView(selectedOrder) {
    this.selectedOrder = selectedOrder;
    const dueDate = new Date(selectedOrder.DueDate);
    this.selectedOrder = selectedOrder;
    this.selectedOrder.DueDateFormated = { year: dueDate.getFullYear(), month: dueDate.getMonth() + 1, day: dueDate.getDate() };


    if (!selectedOrder.isOpened || selectedOrder.isFormatedDates) {
      const traningStart = new Date(selectedOrder.TrainingPeriodStart === null ? moment() : selectedOrder.TrainingPeriodStart );
      this.selectedOrder.TrainingPeriodStart = { year: traningStart.getFullYear(), month: traningStart.getMonth() + 1, day: traningStart.getDate() };
      const traningEnd = new Date(selectedOrder.TrainingPeriodEnd === null ? moment() : selectedOrder.TrainingPeriodEnd);
      this.selectedOrder.TrainingPeriodEnd = { year: traningEnd.getFullYear(), month: traningEnd.getMonth() + 1, day: traningEnd.getDate() };
      const installmentDate = new Date(selectedOrder.InstallmentDate);
      this.selectedOrder.InstallmentDate = { year: installmentDate.getFullYear(), month: installmentDate.getMonth() + 1, day: installmentDate.getDate() };
      const originalDueDate = new Date(selectedOrder.OriginalDueDate);
      this.selectedOrder.OriginalDueDate = { year: originalDueDate.getFullYear(), month: originalDueDate.getMonth() + 1, day: originalDueDate.getDate() };
      const estimatedInvoiceDate = new Date(selectedOrder.EstimatedInvoiceDate);
      this.selectedOrder.EstimatedInvoiceDate = { year: estimatedInvoiceDate.getFullYear(), month: estimatedInvoiceDate.getMonth() + 1, day: estimatedInvoiceDate.getDate() };
    }

    selectedOrder.isOpened = true;

    this.modalReference = this.modalService.open(this.detailView);

  }

  closeOderDetailModal() {
    this.modalReference.close();
    this.loadMemberOders();
  }

  openShop(content, selectedOrder) {
    this.shopHomeService.$shopOpenedFromOrders = true;
    const orderArticles = [];
    this.selectedOrder = selectedOrder;
    this.shopHomeService.$priceType = 'MEMBER';
    // this.shopHomeService.$isCancelBtnHide = true;
    this.selectedMember.BranchID = this.selectedMember.BranchId;
    this.shopHomeService.$selectedMember = this.selectedMember;
    this.shopHomeService.$isUpdateInstallments = true;
    this.shopHomeService.memberSelectedEvent.next(this.selectedMember);
    selectedOrder.AddOnList.forEach(addon => {
      orderArticles.push({
        ArticleNo: addon.ArticleId,
        Id: addon.ArticleId,
        Description: addon.ItemName,
        UnitPrice: addon.UnitPrice,
        DiscountPercentage: addon.Discount,
        OrderQuantity: addon.Quantity,
        DefaultPrice: addon.Price,
        Price: addon.Price,
        isReturn: false,
        EmployeePrice: addon.EmployeePrice,
        SelectedPrice: addon.Price,
        ReturnComment: '',
        Discount: 0,
        VatRate: 0
      })
    });
    this.shopHomeService.ShopItems = orderArticles;
    this.shopHomeService.$selectedOder = selectedOrder;
    this.shopHomeService.$fromMcOrders = true;

    const totalSum = orderArticles.map(article => article.Price).reduce((a, b) => a + b);

    this.shopHomeService.$totalPrice = totalSum.toFixed(2)

    this.shopHomeService.$isCashCustomerSelected = false;

    this.shopModalReference = this.modalService.open(content);
  }

  closeShop() {
    this.shopHomeService.$shopOpenedFromOrders = false;
    this.shopHomeService.$fromMcOrders = false;
    this.loadMemberOders();
    this.shopModalReference.close();
    this.shopHomeService.clearShopItems();
    this.shopHomeService.$selectedMember = null;
  }

  closeShopAfterSave() {
    this.shopHomeService.$shopOpenedFromOrders = false;
    this.shopHomeService.$fromMcOrders = false;
    this.loadMemberOders();
    this.shopModalReference.close();
  }

  orderUpdateSuccess() {
    if (this.modalReference) {
      this.loadMemberOders();
      this.modalReference.close();
    }
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    if (this.modalReference) {
      this.modalReference.close();
    }
    if (this.shopModalReference) {
      this.shopModalReference.close();
    }
    if (this.sub) {
      this.sub.dismiss();
    }
    this.saveEvent.unsubscribe();
  }



  printfunc(invoiceno: any) {
    this.currentInvoice = invoiceno;
    this.economyService.GetInvoice(invoiceno, this.IsBrisIntegration, this.selectedMember.BranchId).subscribe( // takeUntil w/ pipe does not work as expected with Blobs.
      result => {
      if (result) {
      const contentType = 'application/pdf',
      b64Data = result.Data;
      this.blob = this.b64toBlob(b64Data, contentType);
      this.blobUrl = URL.createObjectURL(this.blob); }
      });
  }
  close() {
    this.blobUrl = null;
  }

  b64toBlob = (b64Data, contentType= '', sliceSize= 512) => {
  const byteCharacters = atob(b64Data);
  const byteArrays = [];
  for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    const slice = byteCharacters.slice(offset, offset + sliceSize),
        byteNumbers = new Array(slice.length);
    for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);

    byteArrays.push(byteArray);
  }

const blob = new Blob(byteArrays, {type: contentType});
return blob;
}

openGenerateConfirmModal(content: any, invoicenum: any) {
  this.printfunc(invoicenum);
  this.generatePdfView = this.modalService.open(content, {width: '800'});
 }



PrintPDF() {
  const iframe = document.createElement('iframe');
  iframe.style.display = 'none';
  iframe.src = this.blobUrl;
  document.body.appendChild(iframe);
  iframe.contentWindow.print();
}

SavePDF() {
saveAs(this.blob, this.currentInvoice + '.pdf');
}

}
