import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

export class SubHandlerComponent {
    private subscriptions: Subscription [];

    constructor() {
        this.subscriptions = [];
    }

    push(sub: Subscription) {
        this.subscriptions.push(sub);
    }

    unsubscribe(sub: Subscription) {
        if (this.subscriptions.includes(sub)) {
            const subIndex = this.subscriptions.indexOf(sub, 0);

            if (subIndex > -1) {
                this.subscriptions.splice(subIndex, 1);
            }
        }
    }

    unsubscribeAll() {
        this.subscriptions.forEach((sub) =>
            sub.unsubscribe()
        );
    }

    numberOfActiveSubscriptions() {
        let numberOfActiveSubscriptions = 0;

        this.subscriptions.forEach((sub) => {
            if (!sub.closed) {
                numberOfActiveSubscriptions++;
            }
        });
        return numberOfActiveSubscriptions;
    }

    totalSubscriptions() {
        let totalSubscriptions = 0;

        this.subscriptions.forEach(sub => {
            totalSubscriptions++;
        });
        return totalSubscriptions;
    }

    printAllSubscriptions() {
        this.subscriptions.forEach((sub) =>
            console.log(sub)
        );
    }

    printActiveSubscriptions() {
        this.subscriptions.forEach((sub) => {
            if (!sub.closed) {
                console.log(sub)
            }
        });
    }
}