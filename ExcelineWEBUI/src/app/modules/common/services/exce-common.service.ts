import { Injectable } from '@angular/core';
import { ConfigService } from '@ngx-config/core';
import { Observable } from 'rxjs';

import { CommonHttpService } from '../../../shared/services/common-http.service';
import { ExceLoginService } from '../../login/exce-login/exce-login.service';

@Injectable()
export class ExceCommonService {

  adminServiceUrl: any;
  commonApiUrl: any;
  loginApiUrl: any;
  private branchId: number;

  constructor(
    private commonHttpService: CommonHttpService,
    private config: ConfigService,
    private exceLoginService: ExceLoginService) {
    this.commonApiUrl = this.config.getSettings('EXCE_API.COMMON');
    this.loginApiUrl = this.config.getSettings('EXCE_API.LOGIN');
    this.adminServiceUrl = this.config.getSettings('EXCE_API.USSADMIN');

    this.exceLoginService.branchSelected.subscribe(
      (branch) => {
        this.branchId = branch.BranchId;
      }
    );
  }

  getUserBranches(userName): Observable<any> {
    const url = this.loginApiUrl + 'api/login/GetUserBranches';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  GetVersionNoList(): Observable<any> {
    const url = this.commonApiUrl + 'api/User/GetVersionNoList';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  GetLoginGymByUser(): Observable<any> {
    const url = this.commonApiUrl + 'api/User/GetLoginGymByUser';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  GetUserSelectedBranches(): Observable<any> {
    const url = this.commonApiUrl + 'api/User/GetUserSelectedBranches';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  GetLoggedUserDetails(): Observable<any> {
    const url = this.loginApiUrl + 'api/login/GetLoggedUserDetails';
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  UpdateSettingForUserRoutine(params: any): Observable<any> {
    const url = this.commonApiUrl + 'api/User/UpdateSettingForUserRoutine?key=' + params.key + '&value=' + params.value;
    return this.commonHttpService.makeHttpCall(url, {
      method: 'GET',
      auth: true
    });
  }

  EditUSPUserBasicInfo(basicInfo: any): Observable<any> {
    const url = this.adminServiceUrl + 'EditUSPUserBasicInfo'
    return this.commonHttpService.makeHttpCall(url, {
      method: 'POST',
      auth: true,
      body: basicInfo
    });
  }




}
