import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';

import { ExceToolbarComponent } from './exce-toolbar/exce-toolbar.component';
import { UsTileFeaturesComponent } from './us-tile-menu/us-tile-features/us-tile-features.component';

import { UsTileMenuService } from './us-tile-menu/us-tile-menu.service';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient } from '@angular/common/http';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      },
      isolate: false
    })
  ],
  declarations: [
    ExceToolbarComponent,
    UsTileFeaturesComponent,
  ],
  exports: [
    ExceToolbarComponent,
    UsTileFeaturesComponent,
  ],
  providers: [
    UsTileMenuService,
  ]
})
export class ExceCommonModule {}
