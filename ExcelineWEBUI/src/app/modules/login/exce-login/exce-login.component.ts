import { element, promise } from 'protractor';
import { error } from 'util';
import { Observable, Subscription, timer, of, from, empty } from 'rxjs';
import { ExceSessionService } from './../../../shared/services/exce-session.service';
import { UsErrorService } from '../../../shared/directives/us-error/us-error.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ConfigService } from '@ngx-config/core';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { ExceLoginService } from './exce-login.service';
import { ExceToolbarService } from '../../common/exce-toolbar/exce-toolbar.service';
import { ExceCommonService } from '../../common/services/exce-common.service';
import { CommonEncryptionService } from '../../../shared/services/common-Encryptor-Decryptor';
import { Extension } from 'app/shared/Utills/Extensions';
import { createHash } from 'crypto';
import { IBranch } from 'app/modules/login/interfaces/IBranch';
import { Subject } from 'rxjs';
import { takeUntil, switchMap, map, take, mergeMap, tap, concatMap, delay, concat } from 'rxjs/operators';


@Component({
  selector: 'exce-login',
  templateUrl: './exce-login.component.html',
  styleUrls: ['./exce-login.component.scss']
})
export class ExceLoginComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  timerEvent: Subscription;
  _user: any;
  timer: Observable<number>;
  sessionkey: any;
  gymCode: any;

  loginForm: FormGroup;
  resetPasswordForm: FormGroup;
  resetPasswordFirstTimeForm: FormGroup;
  recoverPasswordForm: FormGroup;
  forgottenPasswordForm: FormGroup;
  branchSelectionForm: FormGroup;
  recoverPasswordFormValidationMessage: FormGroup;

  userBranches: any[];
  selectedBranch: any;

  isLoginSuccess: boolean = false;
  isLoginFailed: boolean = false;
  isLoginFirstTime: boolean = false;
  isForgotPassword: boolean = false;
  isSuccess: boolean = false;
  isEmailFailed: boolean = false;
  isUsernameError: boolean = false;
  loginFormSubmited: boolean = false;

  isForgotPasswordFormSubmitted: boolean = false;
  isResetPasswordFormSubmited: boolean = false;
  isPasswordInfoBoxVisible: boolean = false;
  isEmailLinkInfoVisible: boolean = false;

  // Get query parameter from URL to determine if request is to reset password
  isPasswordResetRouteEngaged: boolean = false;
  fromEmailReturnCode: String;
  fromEmailUserName: String;
  fromEmailBase: String;


  loginFormErrors = {
    'username': '',
    'password': ''
  };
  loginValidationMessages = {
    'username': {
      'required': 'LOGIN.unameRequired'
    },
    'password': {
      'required': 'LOGIN.pwordRequired',
      'loginFail': 'LOGIN.LVPwUnnotCorrect'
    }
  };

  recoverPasswordFormError = {
    'username': ''
  };
  recoverPasswordFormErrorValidationMessage = {
    'username': {
      'required': 'LOGIN.unameRequired'
    }
  }

  resetPasswordFormErrors = {
    'newPassword': '',
    'confirmPassword': ''
  };
  resetPasswordFormValidationMessages = {
    'password': {
      'required': 'LOGIN.pwordRequired'
    },
    'confirmPassword': {
      'required': 'LOGIN.confirmPwordRequired',
      'compare': 'COMMON.FailComparePassword'
    }
  };

  resetPasswordFirstTimeFormErrors = {
    'password' : '',
    'passwordConfirm' : ''
  }
  resetPasswordFirstTimeFormValidationMessages = {
    'newPassword': {
      'required' : 'LOGIN.pwordRequired'
    },
    'rePassword': {
      'required': 'LOGIN.confirmPwordRequired',
      'compare': 'COMMON.FailComparePassword'
    }
  }



  constructor(
    private router: Router,
    private fb: FormBuilder,
    private exceToolbarService: ExceToolbarService,
    private config: ConfigService,
    private exceCommonService: ExceCommonService,
    private exceLoginService: ExceLoginService,
    private commonEncryptionService: CommonEncryptionService,
    private activatedRoute: ActivatedRoute,
    private exceSessionService: ExceSessionService,
    private route: ActivatedRoute // To get query parameters from url

  ) {

    // MOVED TO APP.COMPONENT OR SET TO NULL AT LOGOUT
    // const companyCode = params.get('CompanyCode');
    // if (this.config.getSettings('ENV') === 'DEV') {
    //   this.gymCode = this.config.getSettings('GYMCODE')
    // } else {
    //   this.gymCode = companyCode;
    //   this.exceLoginService.CompanyCode = this.gymCode;
    // }
    this.gymCode = this.exceLoginService.CompanyCode;
    this.loginForm = fb.group({
      'username': [null, [Validators.required]],
      'password': [null, [Validators.required]],
    });
    // this.loginForm = fb.group({
    //   'username': ['exceadmin', [Validators.required]],
    //   'password': ['admin123@exceline', [Validators.required]],
    // });

    /*
    this.resetPasswordForm = fb.group({
      'newPassword': [null, [Validators.required]],
      'rePassword': [null, [Validators.required], [this.comparePassword.bind(this)]],
    });
    */

    this.resetPasswordForm = fb.group({
      'password': [null, [Validators.required]],
      'passwordConfirm': [null, [Validators.required]] // Implement custom compare validator
    })

    this.loginForm.statusChanges.pipe(
      takeUntil(this.destroy$)
      ).subscribe(_ => {
      UsErrorService.onValueChanged(this.loginForm, this.loginFormErrors, this.loginValidationMessages)
    });

    this.resetPasswordForm.statusChanges.pipe(
      takeUntil(this.destroy$)
      ).subscribe(_ => {
      UsErrorService.onValueChanged(this.resetPasswordForm, this.resetPasswordFormErrors, this.recoverPasswordFormValidationMessage)
    });

    this.forgottenPasswordForm = fb.group({
      'username': [ null, [Validators.required]]
    });

    this.resetPasswordFirstTimeForm = fb.group({
      'password': [null, [Validators.required]],
      'passwordConfirm': [null, [Validators.required]]
    })

    this.loginForm.valueChanges.pipe(
      takeUntil(this.destroy$)
      ).subscribe(data => {
      this.isLoginFailed = false;
    });

    this.branchSelectionForm = fb.group({
      'branchSelected': [this.selectedBranch, [Validators.required]],
    })
  }

  ngOnInit() {
    // const params = new URL(location.href).searchParams;
    // const companyCode = params.get('CompanyCode');
    // if (this.config.getSettings('ENV') === 'DEV') {
    //   this.gymCode = this.config.getSettings('GYMCODE')
    // } else {
    //   this.gymCode = companyCode;
    //   this.exceLoginService.CompanyCode = this.gymCode;
    // }
    // console.log(this.gymCode)
    // this.exceLoginService.logout();

    // get the machine name
    // this.sessionkey = Extension.createGuid();
    // if (this.sessionkey) {
    //   this.exceSessionService.AddSession('MACHINE', '1', this.sessionkey, this.gymCode);
      // this.timer = Observable.timer(0, 2000);
      // this.timerEvent = this.timer.pipe(takeUntil(this.destroy$)).subscribe(X => this.getMachineName(X, this.sessionkey));
    // }

    // Get query parameters from url
    this.route.queryParams.pipe(
      takeUntil(this.destroy$)
      ).subscribe(params => {
      if (params['pwrec'] && params['username'] && params['base']) {
        this.fromEmailReturnCode = params['pwrec'];
        this.fromEmailUserName = params['username'];
        this.fromEmailBase = params['base'];
        this.isPasswordResetRouteEngaged = true;
      }
    });
  }

  getMachineName(tick: number, sessionKey: string) {
    this.exceSessionService.getInitialSessionValue(sessionKey, this.manipulateGymCode(), tick).pipe(
      takeUntil(this.destroy$)
      ).subscribe(
      result => {
        if (result) {
          if (result.Data !== 'ERROR' && result.Data !== 'DUPLICATE' && result.Data !== 'SUCCESS') {
            this.timerEvent.unsubscribe();
            this.exceLoginService.MachineName = result.Data;
          } else {
            if (tick === 60) {
              this.timerEvent.unsubscribe();
              this.exceLoginService.MachineName = 'DEFAULT';
            }
          }
        }
      }, error => {
        this.timerEvent.unsubscribe();
        this.exceLoginService.MachineName = 'DEFAULT';
      }, null
    );
  }

  manipulateGymCode() {
    let finalGymCode = '';
    if (this.gymCode) {
      let chars: any[] = [];
      chars = this.gymCode.split('');
      chars.forEach(element => {
        finalGymCode += '@' + element;
      });
    }
    return this.gymCode;
  }

  saveResetPassword(): void {
    this.isResetPasswordFormSubmited = true;
    if (this.resetPasswordFirstTimeForm.valid) {
      // this.commonEncryptionService.encryptValue(value.password)

      const details = this.exceLoginService.LoggedUserDetails;

      details.Password = this.commonEncryptionService.encryptValue(this.resetPasswordFirstTimeForm.value.password);
      details.IsLoginFirstTime = false;
      details.Id = this.exceLoginService.LoggedUserDetails.UserID;


      this.exceCommonService.EditUSPUserBasicInfo({ 'isPasswdChange': true, 'uspUser': details, 'Password': details.Password}).pipe(
        takeUntil(this.destroy$)
        ).subscribe(result => {
        if (result.Data) {
          this.isLoginSuccess = true;
          this.isLoginFirstTime = false;
          this.exceCommonService.getUserBranches(this.exceLoginService.LoggedUserDetails.UserName).pipe(
            takeUntil(this.destroy$)
            ).subscribe(branches => {
            if (branches) {
              this.userBranches = branches.Data;
              this.exceToolbarService.setUserBranches(this.userBranches);
              this.selectedBranch = this.userBranches[0];
              this.exceCommonService.GetLoginGymByUser().pipe(
                takeUntil(this.destroy$)
                ).subscribe(
                res => {
                  this.selectedBranch = this.userBranches.find(x => x.BranchId === res.Data);
                }, null, null
              );
              if (this.userBranches.length === 1) {
                this.exceLoginService.setIsBranchSelected(this.userBranches[0], true);
                this.exceCommonService.UpdateSettingForUserRoutine({ key: 'InitialGym', value: this.userBranches[0].BranchId }).pipe(
                  takeUntil(this.destroy$)
                  ).subscribe(
                  res => {

                  }, null, null
                );
              }
            }
          }, null, null);
        }
      });
    } else {
      UsErrorService.validateAllFormFields(this.resetPasswordForm, this.resetPasswordFormErrors, this.recoverPasswordFormValidationMessage);
    }
  }

  skipResetPassword(): void {
    this.isLoginSuccess = true;
    this.isLoginFirstTime = false;
    this.exceCommonService.getUserBranches(this.exceLoginService.LoggedUserDetails.UserName).pipe(
      takeUntil(this.destroy$)
      ).subscribe(branches => {
      if (branches) {
        this.userBranches = branches.Data;
        this.exceToolbarService.setUserBranches(this.userBranches);
        this.selectedBranch = this.userBranches[0];
        this.exceCommonService.GetLoginGymByUser().pipe(
          takeUntil(this.destroy$)
          ).subscribe(
          res => {
            this.selectedBranch = this.userBranches.find(x => x.BranchId === res.Data);
          }, null, null
        );
      }
    }, null, null);
  }

  /*comparePasswords(): boolean {
    if (this.resetPasswordForm.value.password && this.resetPasswordForm.value.passwordConfirm &&
      (this.resetPasswordForm.value.password === this.resetPasswordForm.value.passwordConfirm)) {
        return true;
      }
  }*/

/*
  comparePassword(control: AbstractControl) {

    return new Promise(resolve => {
      setTimeout(() => {
        if (
          this.resetPasswordForm.value.newPassword && this.resetPasswordForm.value.rePassword &&
          (this.resetPasswordForm.value.newPassword !== this.resetPasswordForm.value.rePassword)) {
          resolve({
            'compare': true
          })
        } else {
          resolve(null);
        }
      }, 10);
    })
  }*/

  login(value: any) {
    this.loginFormSubmited = true;
    if (this.loginForm.valid) {
      this.exceLoginService.login(this.gymCode + '/' + value.username, this.commonEncryptionService.encryptValue(value.password)).pipe(
        concatMap((loginResult: any) => {
          if (loginResult) {
            this.exceLoginService.loginSuccess(loginResult, this.gymCode + '/' + value.username);
            this.isLoginSuccess = true;
            return this.exceCommonService.GetLoggedUserDetails();
          } else {
            /* When login fails, an error is returned from the backend. It will not reach this callback. Hence, this else block is superflious */
            this.loginForm.controls['password'].setErrors({
              'loginFail': true
            });
            this.isLoginSuccess = false;
            this.isLoginFailed = true;
            return empty()
          }
        }),
        concatMap((user: any) => {
          if (!user.Data[0].IsLoginFirstTime) {
            return this.exceCommonService.getUserBranches(value.username)
          }
          this.isLoginFirstTime = true;
          return empty();
        }),
        concatMap((branches: {Data: IBranch[]}) => {
            this.userBranches = branches.Data;
            this.exceToolbarService.setUserBranches(this.userBranches);
            this.selectedBranch = this.userBranches[0];
            return this.exceCommonService.GetLoginGymByUser()
        }),
        concatMap((loggedGymFromUser: any) => {
          this.selectedBranch = this.userBranches.find(x => x.BranchId === loggedGymFromUser.Data);
          if (this.userBranches.length === 1) {
            this.exceLoginService.setIsBranchSelected(this.userBranches[0], true);
            return this.exceCommonService.UpdateSettingForUserRoutine({ key: 'InitialGym', value: this.userBranches[0].BranchId })
          } return empty()
        }),
        takeUntil(this.destroy$)
      ).subscribe(null, () => {
        /* Will be run if user inputs wrong username/password. @Invalid grant */
        this.loginForm.controls['password'].setErrors({
          'loginFail': true
        });
        this.isLoginSuccess = false;
      }, null);
    } else {
      UsErrorService.validateAllFormFields(this.loginForm, this.loginFormErrors, this.loginValidationMessages);
    }

/*       this.exceLoginService.login(this.gymCode + '/' + value.username, this.commonEncryptionService.encryptValue(value.password)).pipe(
        takeUntil(this.destroy$)
        ).subscribe(result => {
          if (result) {
            this.exceLoginService.loginSuccess(result, this.gymCode + '/' + value.username);
            // this.router.navigate(['/']);
            this.isLoginSuccess = true;
            this.exceCommonService.GetLoggedUserDetails().pipe(
              takeUntil(this.destroy$)
              ).subscribe(
              user => {
                this.exceLoginService.LoggedUserDetails = user.Data[0];
                if (!user.Data[0].IsLoginFirstTime) {
                  this.exceCommonService.getUserBranches(value.username).pipe(
                    takeUntil(this.destroy$)
                    ).subscribe(branches => {
                    if (branches) {
                      this.userBranches = branches.Data;
                      this.exceToolbarService.setUserBranches(this.userBranches);
                      this.selectedBranch = this.userBranches[0];
                      this.exceCommonService.GetLoginGymByUser().pipe(
                        takeUntil(this.destroy$)
                        ).subscribe(
                        res => {
                          this.selectedBranch = this.userBranches.find(x => x.BranchId === res.Data);
                        }, null, null
                      );
                      if (this.userBranches.length === 1) {
                        this.exceLoginService.setIsBranchSelected(this.userBranches[0], true);
                        this.exceCommonService.UpdateSettingForUserRoutine({ key: 'InitialGym', value: this.userBranches[0].BranchId }).pipe(
                          takeUntil(this.destroy$)
                          ).subscribe(
                          null, null, null);
                      }
                    }
                  }, null, null);
                } else {
                  this.isLoginFirstTime = true;
                }
              }, null, null);
          } else {
            this.loginForm.controls['password'].setErrors({
              'loginFail': true
            });
            this.isLoginSuccess = false;
            this.isLoginFailed = true;
          }
        },
          error => {
            this.loginForm.controls['password'].setErrors({
              'loginFail': true
            });
            this.isLoginSuccess = false;
          });
    } else {
      UsErrorService.validateAllFormFields(this.loginForm, this.loginFormErrors, this.loginValidationMessages);
    } */

  }

  forgotPassword(): void {
    this.isForgotPassword = true;
  }

  forgotPasswordRequest(PotentialUserName: any): void {
    this.isForgotPasswordFormSubmitted = true;
    if (this.forgottenPasswordForm.valid) {
      this. isEmailLinkInfoVisible = true;
      this.isForgotPassword = false;

      this.exceLoginService.requestNewPassword(this.gymCode, PotentialUserName.username).pipe(
        takeUntil(this.destroy$)
        ).subscribe( null, null, null);
      setTimeout(() => {
        this.isEmailLinkInfoVisible = false;
        this.router.navigate(['/']);
      }, 3000);
    } else {
      UsErrorService.validateAllFormFields(this.forgottenPasswordForm, this.recoverPasswordFormError, this.recoverPasswordFormValidationMessage);
    }
  }

  sendNewPWForConfirmation(passwordFields: any): void {
    if (this.resetPasswordForm.valid) {
      this.isResetPasswordFormSubmited = true;
      this.isPasswordResetRouteEngaged = false;
      this.isPasswordInfoBoxVisible = true;
      this.exceLoginService.sendNewPWForConfirmation({
        'gymCode' : this.gymCode,
        'userName' : this.fromEmailUserName,
        'base': this.fromEmailBase,
        'password' : this.commonEncryptionService.encryptValue(passwordFields.password)
      }).pipe(takeUntil(this.destroy$)).subscribe(() => {
        /*
        if (result.Data) {
          console.log('result from sendNewPWForConfirmation: ', result.Data);
        }
        */
      });
      setTimeout(() => {
        this.isPasswordResetRouteEngaged = false;
        this.isPasswordInfoBoxVisible = false;
        this.isForgotPassword = false;
        this.router.navigate(['/']);
      }, 3000);
    } else {
      // UsErrorService.validateAllFormFields(this.resetPasswordForm, this.resetPasswordFormErrors, this.resetPasswordFormValidationMessages);
    }
  }

  selectBranch(branch: IBranch): void {
    console.log(branch)
    this.exceLoginService.setIsBranchSelected(branch, true);
    this.exceCommonService.UpdateSettingForUserRoutine({ key: 'InitialGym', value: branch.BranchId }).pipe(
      takeUntil(this.destroy$)
    ).subscribe(null, null, null);
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
