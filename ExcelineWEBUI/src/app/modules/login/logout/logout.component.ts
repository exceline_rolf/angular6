import { Component } from '@angular/core';
import { ExceLoginService } from '../exce-login/exce-login.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent {
  constructor(
    private router: Router,
    private exceLoginService: ExceLoginService
  ) {
    exceLoginService.logout();
    this.router.navigate(['/']);
    // window.location.reload();
  }

}
