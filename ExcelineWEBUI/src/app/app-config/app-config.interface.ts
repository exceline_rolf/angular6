export interface IAppConfig {


  EMAIL_REGEXP: string;
  LANGUAGE_DEFAULT: Language,
  LANGUAGES: Language[]

}

interface Language {
  ID: string,
  NAME: string,
  CULTURE: string
}
