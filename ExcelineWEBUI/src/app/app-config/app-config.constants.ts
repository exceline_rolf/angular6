import { InjectionToken } from '@angular/core';
import { IAppConfig } from './app-config.interface';

export const APP_DI_CONFIG: IAppConfig = {


  EMAIL_REGEXP: '[a-z0-9!#$%&\'*+=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?',
  LANGUAGE_DEFAULT: { ID: 'no', NAME: 'Norsk', CULTURE: 'en-US' },
  LANGUAGES: [
    { ID: 'no', NAME: 'Norsk', CULTURE: 'en-US' },
    { ID: 'en', NAME: 'English', CULTURE: 'en-US' }
  ]
};

export let APP_CONFIG = new InjectionToken<IAppConfig>('app.config');

