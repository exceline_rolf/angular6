import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// import { AngularSplitModule } from 'angular-split';
import { AngularSplitModule } from 'angular-split-ng6';
import { Guid } from './shared/services/guid.service'
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { APP_CONFIG, APP_DI_CONFIG } from './app-config/app-config.constants';
import { AuthGuard } from './guards/auth.guard';
import { AppRoutingModule } from './app-routing.module';
import { routeingComponents } from './app-routing.module';
import { UsBreadcrumbService } from './shared/components/us-breadcrumb/us-breadcrumb.service';
import { ExceToolbarService } from './modules/common/exce-toolbar/exce-toolbar.service';
import { CommonHttpService } from './shared/services/common-http.service';
import { ExceLoginService } from './modules/login/exce-login/exce-login.service';
import { ExceCommonModule } from './modules/common/exce-common.module';
import { MembershipModule } from './modules/membership/membership.module';
import { UssAdminModule } from './modules/uss-admin/uss-admin.module'
import { LoginModule } from './modules/login/login.module';
import { ReportingModule } from './modules/reporting/reporting.module';
import { ExceService } from './shared/directives/exce-error/exce-error'
import { ExceMessageService } from './shared/components/exce-message/exce-message.service';
import { UsScrollService } from './shared/directives/us-scroll/us-scroll.service';
import { UsTabService } from './shared/components/us-tab/us-tab.service';
import { PreloaderService } from './shared/services/preloader.service';
import { UsErrorService } from './shared/directives/us-error/us-error.service';
import { AdminService } from './modules/admin/services/admin.service';
import { ExceMemberService } from './modules/membership/services/exce-member.service';
import { APP_BASE_HREF, LocationStrategy, HashLocationStrategy } from '@angular/common';
import { ExceSessionService } from 'app/shared/services/exce-session.service';
import { ExcePdfViewerService } from 'app/shared/components/exce-pdf-viewer/exce-pdf-viewer.service'
import { USSAdminService } from './modules/uss-admin/services/uss-admin-service';
import { CommonEncryptionService } from 'app/shared/services/common-Encryptor-Decryptor';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ConfigModule, ConfigLoader, } from '@ngx-config/core';
import { ConfigHttpLoader } from '@ngx-config/http-loader';
import { ExceAddFollowUpService } from 'app/shared/components/exce-followup/exce-add-follow-up.service';
import { Observable, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { ExceClassService } from 'app/modules/class/services/exce-class.service';
import { ClassHomeService } from 'app/modules/class/class-home/class-home.service';
import { ExceCommonService } from './modules/common/services/exce-common.service';
import { ReportingService } from './modules/reporting/reporting-service';
import { ExceBreadcrumbService } from './shared/components/exce-breadcrumb/exce-breadcrumb.service';
import { AdminModule } from './modules/admin/admin.module';
import { AgGridModule } from 'ag-grid-angular';
import { EconomyModule } from './modules/economy/economy.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouteReuseStrategy } from '@angular/router';
import { CustomRouteReuseStategy } from './CustomRouteReuseStrategy';
import { VersionCheckService } from './version-check.service';
// import { DebounceClickDirective } from './shared/directives/debounce-click/debounce-click.directive';


export function configFactory(http: HttpClient): ConfigLoader {
  return new ConfigHttpLoader(http, './assets/config.json'); // API ENDPOINT
}

export function translateLoader(http: HttpClient) {
  return new MultiTranslateHttpLoader(http, [
    { prefix: './assets/i18n/', suffix: '.json' },
    { prefix: './assets/i18n/modules/membership/', suffix: '.json' },
    { prefix: './assets/i18n/modules/admin/', suffix: '.json' },
    // { prefix: './assets/i18n/modules/reporting/', suffix: '.json' },
    { prefix: './assets/i18n/modules/class/', suffix: '.json' },
    { prefix: './assets/i18n/modules/common-notification/', suffix: '.json' },
    { prefix: './assets/i18n/modules/economy/', suffix: '.json' },
    { prefix: './assets/i18n/modules/uss-admin/', suffix: '.json' }
  ]);
}

export class MultiTranslateHttpLoader implements TranslateLoader {

  constructor(private http: HttpClient,
    public resources: { prefix: string, suffix: string }[] = [
      {
        prefix: '/assets/i18n/',
        suffix: '.json'
      }
    ]) { }

  /**
   * Gets the translations from the server
   * @param lang
   * @returns {any}
   */
  public getTranslation(lang: string): any {

    return forkJoin(this.resources.map(config => {
      return this.http.get(`${config.prefix}${lang}${config.suffix}`);
    })).pipe(
      map(response => {
        return response.reduce((a, b) => {
          return Object.assign(a, b);
        });
      })
    )
  }
}

@NgModule({
  declarations: [
    AppComponent,
    routeingComponents
    // DebounceClickDirective
  ],
  imports: [
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (translateLoader),
        deps: [HttpClient]
      }
    }),
    ConfigModule.forRoot({
      provide: ConfigLoader,
      useFactory: (configFactory),
      deps: [HttpClient]
    }),
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AngularSplitModule,
    ReactiveFormsModule,
    AppRoutingModule,
    MembershipModule,
    ExceCommonModule,
    UssAdminModule,
    AgGridModule.withComponents(null),
    LoginModule,
    AdminModule,
    EconomyModule,
    SharedModule.forRoot(),
    NgbModule.forRoot(),
    BrowserAnimationsModule

  ],
  providers: [
    AuthGuard,
    VersionCheckService,
    UsBreadcrumbService,
    ExceLoginService,
    TranslateService,
    ExceMessageService,
    CommonHttpService,
    ExceToolbarService,
    ExceService,
    UsScrollService,
    UsTabService,
    ExcePdfViewerService,
    ExceCommonService,
    Guid,
    PreloaderService,
    UsErrorService,
    AdminService,
    ReportingService,
    ExceMemberService,
    ExceSessionService,
    CommonEncryptionService,
    ExceAddFollowUpService,
    USSAdminService,
    ExceClassService,
    ClassHomeService,
    ExceBreadcrumbService,
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    {
      provide: APP_CONFIG,
      useValue: APP_DI_CONFIG
    },
    {provide: RouteReuseStrategy, useClass: CustomRouteReuseStategy}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
