import { USExcelineHTMLPage } from './app.po';

describe('us-exceline-html App', () => {
  let page: USExcelineHTMLPage;

  beforeEach(() => {
    page = new USExcelineHTMLPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
