#Problums of running the cli project
If some one ccan not run the project firstly run following command
`npm install -g @angular/cli@latest`
`npm install windows-build-tools -g`
`npm install aglio`

## Application configurations
Please add `config.json` to `asserts` by using `asserts\config.sample.json`

## Rest API hosted URLs
All the rest APIs have been hosted on `unicornexc` server for development
Please use following urls on `asserts\config.json` to avoid run the services localy 
 "EXCE_API": {
    "LOGIN": "http://unicornexc/ExcelineLoginRest/",
    "COMMON": "http://unicornexc/ExcelineCommonRest/",
    "MEMBER": "http://unicornexc/ExcelineMemberRest/api/Member",
    "ADMIN": "http://unicornexc/ExcelineAdminRest/",
    "SHOP": "http://unicornexc/ExcelineShopRest/api/"
  }

## Application constants

App constance configs are in 
-src
 -app-config
  --app-config.constance.ts
  --app-config.interface.ts


## Language Settings

All the translations files are in 
-assets
 -i18n
  -modules
   -module-name
    --en.json
    --no.json
  -en.json `this contains shared module language settings`
  -no.json




## BD changes for module load
USP_AUT_GetModulesForUser
USP_AUT_GetFeaturesForUser
USP_AUT_GetOperationsForUser

USExceGMSCheckActiveTimeOverlapWithClass --added
Table type ExceEntityActiveTime --added

## Production Build

git password - 1qaz2wsx
Create pull request 

run following command to build hosting package on following location
`D:\Exceline\Instances\ExcelineHTML\EXCEWEBUI\usexceline\ExcelineWEBUI`

Build Command
#production build
`ng build --base-href /ExcelineHTML/ExcelineWEBUI/ -prod -aot=false`

#testing build
`ng build --base-href /ExcelineWebUI/ -prod -aot=false`


http://dev.exceline.net/ExcelineWebUI